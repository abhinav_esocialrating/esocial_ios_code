# GoodSpace

GoodSpace:
Welcome to GoodSpace - an exclusive club where you will find the best people to network, collaborate, and become friends with.
GoodSpace allows you to search for and find the best people to connect with to help fulfill your various needs. Whether you’re looking for employment, freelance projects, or just friends in general - GoodSpace will connect you with people who share similar values and interests.

## Architecture

Project uses MVVM Architecture throughout. ViewModel of the any ViewController contains the definitions of the Networking functions, Business logics and the controller is notified through Closures in the functions.

## Dependencies Used

### Networking

Alamofire

### Socket Chat

Socket.IO-Client-Swift

### Firebase

Firebase/Messaging

Firebase/Analytic

Firebase/DynamicLink

GoogleTagManager

Firebase/RemoteConfig


### Social media

GoogleSignIn

FBSDKLoginKit


### Crashlytics

pod 'Fabric', '~> 1.10.2'

pod 'Crashlytics', '~> 3.14.0'


### UI Libraries

DropDown

MBProgressHUD

FAPanels

FloatRatingView

UICircularProgressRing

Koloda

TOCropViewControlle

MDatePickerView

FittedSheets

ViewPager-Swift

FlexiblePageContro

WKVerticalScrollBar

RKMultiUnitRuler


## Modules

### Feedback

Here at GoodSpace, you can get the best opportunities lightning fast. 

By connecting you to all kinds of jobs, good people, and special discounts. 

	1.	You can start by giving quick feedback for their overall goodness out of 10	
	2.	Click on ‘Give more feedback’ for a detailed feedback based on the 7 GoodSpace values
	3.	And you can click again on ‘Give more feedback’ to elaborate how you feel about them - in your own words or ‘View Templates’ for some inspiration.
	4.	Define your relationship and mark how well you know them by clicking on ‘Give more feedback’
	5.	Define the closeness of your relationship and how long you’ve known them. 
	6.	Click on submit to complete the feedback process.
	7.	An important reminder, all the feedback you give is anonymous unless you want to share it personally with them. 
	8.	Let the good people in your life know what they mean to you with GoodSpace.


### Score

Here at GoodSpace, you can get the best opportunities lightning fast.  
By connecting you to all kinds of jobs, good people, and special discounts. 

If you are curious about your Goodness Score and want to know what ‘Your Goodness Page’ is all about then keep watching this video.

Your Goodness Page keeps you updated on your activities, achievements and feedback.

	1.	First, let’s start by tapping on the ‘Goodness Score’ card - you can see the feedback you’ve received based on the 7 GoodSpace values. 
	2.	Filter out feedback based on friends, family, colleagues and so on to go in depth. 
	3.	GMedal’s show the impact you have created in your social circles with GoodSpace.
	4.	Next we have GCoins, you earn GCoins by giving feedback and getting your community to sign up on GoodSpace.
	5.	Use your GCoins within the GoodSpace community or redeem your GCoins to your PayTM wallet as credit.
	6.	Review all the people you have given feedback to and see who has given you feedback.
	7.	Check up on all the invites you have sent and send a reinvite reminder incase they haven’t joined.
	8.	Read through all the detailed feedback you have received and pin the ones you like to your profile so people know all that is good about you.


### Find

Let’s take a look at how the powerful search engine can help you find what you’re looking for.

	1.	In the GoodSpace tab, you can search directly by name 
	2.	Or,  use the convenient filters tab to narrow down your search.
	3.	Think of the find feature as a yellow pages meets social networking.
	4.	Looking for a hair stylist, driver or a trainer from a trustable source? 
	5.	Or maybe you need to connect with a doctor or lawyer?
	6.	Browse from a wide range of services and discover the right kind of people for your personal and professional life

### Profile

Here at GoodSpace, you can get the best opportunities lightning fast.  
By connecting you to all kinds of jobs, good people, and special discounts.

Your profile will get you in touch with like minded people and tailored opportunities. Let’s look at how that works:

	1.	Open your Complete Profile
	2.	Highlight what you are looking for to get optimised results
	3.	Now, if you are looking to make friends, make sure to fill your personal details and interests.
	4.	And if you are looking for any kind of employment or need a service, fill in all three areas of your professional details, skills, and education details
	5.	If you are looking for freelance work, try and mention your hourly engagement. 
	6.	Make the best out of your profile on Goodspace

### Offers

Here at GoodSpace, you can get the best opportunities lightning fast. 

By connecting you to all kinds of jobs, good people, and special discounts.

You can start by filling in an opportunity of your own:

	1.	All you have to do is, go to the ‘Offers’ section.
	2.	Click the plus (+) button 
	3.	Then choose the type of offer based on your requirements.
    4.  Fill in the specific role and add a suitable offer title. 
    5.  Try and keep the description short and simple.
    6.  Are there any skills you prefer for your offer? Fill them in…
    7.  Select a GoodnessScore out of 10 and put your desired location.
    8.  Click on ’Save’ and your offer will be sent to GoodSpace for approval.
    .  Check the status of your offer in the ‘My Offers’ Section.
    10.  Once your offer is approved, it will be visible for everyone in the ‘All’ Section. 
    11.  You will receive a notification when people show interest. 
    12.  And they can directly contact you by clicking here.


## Home Tab

### Interface file

Home.Storyboard

### Directory Structure

```
├── EditName
│   └── EditNameViewController.swift
├── FeedbackSuggestions
│   └── TextFeedbackSuggestionViewController.swift
├── HomeNavigationController.swift
├── HomeViewController.swift
├── HomeViewModel.swift
├── Model
│   ├── RatingParamsAndRelationships
│   │   ├── RatingParameters.swift
│   │   └── Relationship.swift
│   ├── ShareParameters.swift
│   └── TZCard
│       ├── Action.swift
│       ├── Medals.swift
│       ├── Overall.swift
│       ├── Suggestions.swift
│       ├── TZCard.swift
│       └── UserInfo.swift
├── SharePopUp
│   └── SharePopUpViewController.swift
└── View
    ├── AuxCards
    │   ├── AuxCardCell.swift
    │   ├── AuxCardCell.xib
    │   ├── PointsEarnedCell.swift
    │   ├── PointsEarnedCell.xib
    │   ├── WebCardCell.swift
    │   └── WebCardCell.xib
    ├── LinkCards
    │   ├── BackGroundImageCell
    │   │   ├── BackgroundImageCell.swift
    │   │   └── BackgroundImageCell.xib
    │   ├── InfoCardCell
    │   │   ├── InfoCardCell.swift
    │   │   └── InfoCardCell.xib
    │   ├── LinkCardCell1
    │   │   ├── LinkCardCell1.swift
    │   │   └── LinkCardCell1.xib
    │   ├── LinkCardCell2
    │   │   ├── LinkCardCell2.swift
    │   │   └── LinkCardCell2.xib
    │   ├── LinkCardCell3
    │   │   ├── LinkCardCell3.swift
    │   │   └── LinkCardCell3.xib
    │   ├── LinkCardCell4
    │   │   ├── LinkCardCell4.swift
    │   │   └── LinkCardCell4.xib
    │   └── LinkCardCell5
    │       ├── LinkCardCell5.swift
    │       └── LinkCardCell5.xib
    ├── MultipleReviewCards
    │   ├── BaseReviewCell.swift
    │   ├── BaseReviewCell.xib
    │   ├── CardHeaderView.swift
    │   ├── CardHeaderView.xib
    │   ├── FeedbackCardCell.swift
    │   ├── FeedbackCardCell.xib
    │   ├── GoodnessCardCell.swift
    │   ├── GoodnessCardCell.xib
    │   ├── Parameters
    │   │   ├── ParameterBaseCell.swift
    │   │   ├── ParameterBaseCell.xib
    │   │   ├── ParameterCell.swift
    │   │   └── ParameterCell.xib
    │   ├── RelationshipCardCell.swift
    │   └── RelationshipCardCell.xib
    └── ReviewCard
        ├── RelationshipCell.swift
        ├── RelationshipCell.xib
        ├── ReviewCardCell.swift
        ├── ReviewCardCell.xib
        ├── SliderCell.swift
        └── SliderCell.xib
```
### App Screenshot

![RelationshipCard](./Documentation/Screenshots/Home/Cards/RelationshipCard.png)

![ParameterCard](./Documentation/Screenshots/Home/Cards/ParameterCard.png)

![Detailed Feedback Card](./Documentation/Screenshots/Home/Cards/DetailedFeedbackCard.png)

## Score Tab

### Interface file

ScoreCard.Storyboard

### Directory Structure

```
├── Medals
│   ├── MedalsViewController.swift
│   ├── MedalsViewModel.swift
│   ├── Model
│   │   └── MedalsModel+CoreDataClass.swift
│   └── View
│       ├── MedalHeaderView.swift
│       ├── MedalHeaderView.xib
│       └── MedalsCell.swift
├── Network
│   ├── Model
│   │   └── NetworkList.swift
│   ├── NetworkPager
│   │   ├── NetworkContainerViewController.swift
│   │   └── NetworkPagerViewController.swift
│   ├── NetworkViewController.swift
│   ├── NetworkViewModel.swift
│   └── View
│       ├── NetworkCell.swift
│       └── NetworkCell.xib
├── ScoreCardNavigation
│   └── ScoreCardNavigationController.swift
└── ScoreCardVC
    ├── Model
    │   ├── Network.swift
    │   ├── Relationships.swift
    │   └── ScoreCard.swift
    ├── ScoreCardViewController.swift
    ├── ScoreCardViewModel.swift
    └── View
        └── ScoreCardCell.swift
```
### App Screenshot

![ScoreCard](./Documentation/Screenshots/ScoreCard/ScoreCardTab.png)

## Find Tab

### Interface file

Search.Storyboard

### Directory Structure

```
├── SearchContainer
│   └── SearchContainerViewController.swift
├── SearchNavigation
│   └── SearchNavigaitionController.swift
└── SearchResults
    ├── Model
    │   └── SearchResults.swift
    ├── SearchResultsViewController.swift
    ├── SearchResultsViewModel.swift
    └── View
        ├── Search\ Card
        │   ├── SearchCard.swift
        │   └── SearchCard.xib
        ├── SearchCategoriesTagCell.swift
        ├── SearchResultsCell.swift
        ├── SearchResultsCell.xib
        ├── SearchSuggestionsCell.swift
        ├── SearchSwipeCell.swift
        └── SearchSwipeCell.xib
```

### App Screenshot

![search](./Documentation/Screenshots/Search/SearchMainScreen.png)




## Profile Tab

### Interface file

ProfileV2.Storyboard

### Directory Structure
```
├── CertificateLicense
│   ├── CertificateList
│   │   ├── CertificateListViewController.swift
│   │   ├── CertificateListViewModel.swift
│   │   ├── Model
│   │   │   └── Certification.swift
│   │   └── View
│   │       └── CertificationCell.swift
│   └── EditCertificate
│       ├── EditCertificateViewController.swift
│       └── EditCertificateViewModel.swift
├── Compensation
│   ├── CompensationView
│   │   ├── CompensationListViewController.swift
│   │   └── CompensationListViewModel.swift
│   └── EditCompensation
│       ├── CompensationViewController.swift
│       └── CompensationViewModel.swift
├── Competency
│   ├── CompetencyViewController.swift
│   ├── CompetencyViewModel.swift
│   └── Model
│       └── Competency.swift
├── Data
│   └── ProfileV2Data.swift
├── EduExp
│   └── ProfileV2EducationListViewController.swift
├── Feedback
│   ├── TextualFeedback
│   └── TextualFeedbackList
│       ├── TextualFeedbackViewController.swift
│       ├── TextualFeedbackViewModel.swift
│       ├── View
│       │   └── TextualFeedbackCell.swift
│       └── model
│           ├── TextualFeedback.swift
│           └── TextualFeedbackModel+CoreDataClass.swift
├── Friends
│   ├── ProfileV2BasicDetailViewController.swift
│   ├── ProfileV2BioViewController.swift
│   ├── ProfileV2InterestsViewController.swift
│   ├── ProfileV2PersonalDetailViewController.swift
│   └── ProfileV2SkillsViewController.swift
├── MainPage
│   └── ProfileHomeViewController.swift
├── ProfileBaseViewController.swift
├── ProfileCategory
│   ├── Model
│   │   └── ProfileCategories.swift
│   ├── ProfileCategoriesViewController.swift
│   └── View
│       └── ProfileCategoriesCell.swift
├── ProfileDetails
│   ├── Model
│   │   ├── ProfileDetailCategories.swift
│   │   └── ProfileDetails.swift
│   ├── ProfileDetailsViewController.swift
│   ├── ProfileDetailsViewModel.swift
│   └── View
│       ├── CompetencyTagCell.swift
│       ├── ProfileDetailBasicInfoCell.swift
│       └── ProfileDetailHeaderCell.swift
├── ProfileV2NavigationController.swift
├── ProfileV2PagerViewController.swift
├── Projects
│   ├── EditProjects
│   │   ├── EditProjectsViewController.swift
│   │   └── EditProjectsViewModel.swift
│   └── Projects
│       ├── Model
│       │   └── Project.swift
│       ├── ProjectsViewController.swift
│       ├── ProjectsViewModel.swift
│       └── View
│           └── ProjectCell.swift
├── SkillTypes
│   ├── SkillTypesCell.swift
│   ├── SkillTypesViewModel.swift
│   └── SkillsTypesViewController.swift
├── SocialMedia
│   ├── SocialMediaURLViewController.swift
│   └── SocialMediaURLViewModel.swift
├── TagRelations
│   ├── Model
│   │   └── TagRelationship.swift
│   ├── TagRelationsViewController.swift
│   ├── TagRelationsViewModel.swift
│   └── View
│       └── TagRelationsCell.swift
└── Views
    ├── ProfileV2HeaderView.swift
    ├── ProfileV2HeaderView.xib
    └── SkillV2TableViewCell
        ├── SkillV2TableViewCell.swift
        └── SkillV2TableViewCell.xib
```

### App Screenshot

![profile](./Documentation/Screenshots/Profile/ProfileMain.png)
![guest profile](./Documentation/Screenshots/Profile/ProfileOthers.png)

## Offers Tab

### Interface file

Offers.Storyboard

### Directory Structure
```
├── EditOffers
│   ├── EditOffersViewController.swift
│   └── EditOffersViewModel.swift
├── Likes
│   ├── LikesViewController.swift
│   ├── LikesViewModel.swift
│   ├── Model
│   │   └── LikesModel.swift
│   └── View
│       └── LikesCell.swift
├── OfferAPI.swift
├── OfferComment
│   ├── OfferCommentViewController.swift
│   ├── OfferCommentViewModel.swift
│   └── View
│       └── OfferReplyCell.swift
├── OfferDetails
│   ├── Model
│   │   └── OfferComment.swift
│   ├── OfferDetailViewModel.swift
│   ├── OfferDetailsViewController.swift
│   └── View
│       └── OfferCommentsCell.swift
├── OfferFilter
│   ├── OfferFilterViewController.swift
│   └── OfferFilterViewModel.swift
├── Offers
│   ├── Offers
│   │   ├── OffersPagerViewController.swift
│   │   └── OffersViewController.swift
│   └── OffersList
│       ├── Model
│       │   └── Offer.swift
│       ├── OfferListViewModel.swift
│       ├── OffersListViewController.swift
│       └── View
│           ├── OffersCell.swift
│           └── OffersCell.xib
└── OffersNavigationController.swift
```

### App Screenshot

![offer home](./Documentation/Screenshots/Offers/OfferHome.png)
![my offers](./Documentation/Screenshots/Offers/MyOffers.png) 


## Network calls

NetworkManager class is used for intereacting with  remote server. Related files can be found in the 'Networking' group.

Here's how to make a network call:

```
NetworkManager.rateUser(parameters: parameters, method: .post) { (response) in
    switch response.response?.statusCode {
    case 200:
        do {
            let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
            let data = jsonData["data"] as! NSDictionary
        }
        catch {

        }
    }
}
```

Use the data dictionary in the above code snippet to find the requested data.

Here 'parameters' is the request data in the specified format, and
'method' is the type of HTTP method.

URLs of the network calls are listed in the 'AppURLs.swift' file.
