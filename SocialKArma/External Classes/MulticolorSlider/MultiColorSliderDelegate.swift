//
//  MultiColorSliderDelegate.swift
//  MultiColorSlider
//
//  Created by Keisuke Shoji on 2017/03/09.
//
//

import CoreGraphics

public protocol MultiColorSliderDelegate: class {

    /// Called when the MultiColorSlider values are changed
    ///
    /// - Parameters:
    ///   - slider: MultiColorSlider
    ///   - minValue: minimum value
    ///   - maxValue: maximum value
    func rangeSeekSlider(_ slider: MultiColorSlider, didChange minValue: CGFloat, maxValue: CGFloat)

    /// Called when the user has started interacting with the MultiColorSlider
    ///
    /// - Parameter slider: MultiColorSlider
    func didStartTouches(in slider: MultiColorSlider)

    /// Called when the user has finished interacting with the MultiColorSlider
    ///
    /// - Parameter slider: MultiColorSlider
    func didEndTouches(in slider: MultiColorSlider)

    /// Called when the MultiColorSlider values are changed. A return `String?` Value is displayed on the `minLabel`.
    ///
    /// - Parameters:
    ///   - slider: MultiColorSlider
    ///   - minValue: minimum value
    /// - Returns: String to be replaced
    func rangeSeekSlider(_ slider: MultiColorSlider, stringForMinValue minValue: CGFloat) -> String?

    /// Called when the MultiColorSlider values are changed. A return `String?` Value is displayed on the `maxLabel`.
    ///
    /// - Parameters:
    ///   - slider: MultiColorSlider
    ///   - maxValue: maximum value
    /// - Returns: String to be replaced
    func rangeSeekSlider(_ slider: MultiColorSlider, stringForMaxValue: CGFloat) -> String?
}


// MARK: - Default implementation

public extension MultiColorSliderDelegate {

    func rangeSeekSlider(_ slider: MultiColorSlider, didChange minValue: CGFloat, maxValue: CGFloat) {}
    func didStartTouches(in slider: MultiColorSlider) {}
    func didEndTouches(in slider: MultiColorSlider) {}
    func rangeSeekSlider(_ slider: MultiColorSlider, stringForMinValue minValue: CGFloat) -> String? { return nil }
    func rangeSeekSlider(_ slider: MultiColorSlider, stringForMaxValue maxValue: CGFloat) -> String? { return nil }
}
