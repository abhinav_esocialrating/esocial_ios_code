//
//  CountryCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/03/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {
    
    var nameLabel = UILabel()
    var codeLabel = UILabel()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureView() {
        contentView.addSubview(nameLabel)
        contentView.addSubview(codeLabel)
        
        nameLabel.backgroundColor = .white
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.anchor(top: nameLabel.superview?.topAnchor, paddingTop: 8, bottom: nameLabel.superview?.bottomAnchor, paddingBottom: 8, left: nameLabel.superview?.leftAnchor, paddingLeft: 12, right: nameLabel.superview?.rightAnchor, paddingRight: 100, width: 0, height: 0)
        nameLabel.textColor = UIColor.NewTheme.darkGray
        nameLabel.numberOfLines = 0
        
        codeLabel.backgroundColor = .white
        codeLabel.translatesAutoresizingMaskIntoConstraints = false
        codeLabel.anchor(top: codeLabel.superview?.topAnchor, paddingTop: 8, bottom: codeLabel.superview?.bottomAnchor, paddingBottom: 8, left: nil, paddingLeft: 0, right: codeLabel.superview?.rightAnchor, paddingRight: 12, width: 80, height: 0)
        codeLabel.textColor = UIColor.NewTheme.darkGray
        nameLabel.numberOfLines = 0
    }
}
