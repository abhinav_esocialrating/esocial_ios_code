//
//  CountryPickerViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/03/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CountryPickerViewController: UIViewController {
    
    var tableView: UITableView!
    var navView: UIView!
    var backButton: UIButton!
    var safeArea: UILayoutGuide!
    var searchView: UIView!
    var searchTF: UITextField!
    var titleLabel: UILabel!
    // MARK: - Variables
    var list = CountryPhoneCodeAndName()
    var filteredArray = [CountryNameWithCode]()
    var selectionAction: ((_ name: String, _ code: String) -> Void)? = nil // For sending selected data
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        safeArea = view.layoutMarginsGuide
        configureView()
        list.getCountryName()
        tableView.reloadData()
    }
    func configureView() {
        view.backgroundColor = .white
        
        navView = UIView()
        navView.backgroundColor = .white
        navView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(navView)
        navView.anchor(top: safeArea.topAnchor, paddingTop: 0, bottom: nil, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 0, right: safeArea.rightAnchor, paddingRight: 0, width: 0, height: 44)
        
        backButton = UIButton(type: .custom)
        backButton.translatesAutoresizingMaskIntoConstraints = false
        navView.addSubview(backButton)
        backButton.anchor(top: navView.topAnchor, paddingTop: 4, bottom: nil, paddingBottom: 0, left: navView.leftAnchor, paddingLeft: 12, right: nil, paddingRight: 0, width: 36, height: 36)
        backButton.setImage(UIImage(named: "Back-Arrow-icon-Grey"), for: .normal)
        backButton.tintColor = UIColor.NewTheme.darkGray
        backButton.addTarget(self, action: #selector(backTapped(_:)), for: .touchUpInside)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        searchView = UIView()
        searchView.backgroundColor = .white
        searchView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(searchView)
        searchView.anchor(top: navView.bottomAnchor, paddingTop: 0, bottom: nil, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 0, right: view.rightAnchor, paddingRight: 0, width: 0, height: 44)

        titleLabel = UILabel(frame: CGRect(x: 3, y: 3, width: 3, height: 30))
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        navView.addSubview(titleLabel)
        titleLabel.anchor(top: navView.topAnchor, paddingTop: 12, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, width: 0, height: 0)
        let titleConst = NSLayoutConstraint(item: titleLabel!, attribute: .centerX, relatedBy: .equal, toItem: searchView, attribute: .centerX, multiplier: 1.0, constant: 0)
        view.addConstraint(titleConst)
        NSLayoutConstraint.activate([titleConst])
        titleLabel.text = "Select Country Code"
        titleLabel.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 16)
        titleLabel.textColor = UIColor.NewTheme.darkGray
        titleLabel.textAlignment = .center
        


        searchTF = UITextField()
        searchTF.backgroundColor = .white
        searchTF.translatesAutoresizingMaskIntoConstraints = false
        searchView.addSubview(searchTF)
        searchTF.anchor(top: searchView.topAnchor, paddingTop: 0, bottom: nil, paddingBottom: 0, left: searchView.leftAnchor, paddingLeft: 16, right: searchView.rightAnchor, paddingRight: 16, width: 0, height: 44)
        searchTF.placeholder = "Search by country name..."
        searchTF.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 16)
        searchTF.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
        
        tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.anchor(top: searchView.bottomAnchor, paddingTop: 0, bottom: safeArea.bottomAnchor, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 0, right: view.rightAnchor, paddingRight: 0, width: 0, height: 0)
        tableView.register(CountryCell.self, forCellReuseIdentifier: TableViewCells.CountryCell.rawValue)
        tableView.registerFromXib(name: TableViewCells.CountryListCell.rawValue)
        tableView.delegate = self
        tableView.dataSource = self
    }
    // MARK: - Actions
    @objc func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func textChanged(_ sender: UITextField) {
        if sender.text == "" {
            filteredArray = []
        } else {
            filteredArray = list.countryWithCode.filter({$0.countryName.contains(sender.text!)})
        }
        tableView.reloadData()
    }
}
// MARK: - TableView methods
extension CountryPickerViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredArray.count == 0 ? list.countryWithCode.count : filteredArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.CountryListCell.rawValue, for: indexPath) as! CountryListCell
        if filteredArray.count == 0 {
            cell.nameLabel.text = list.countryWithCode[indexPath.row].countryName
            cell.codeLabel.text = list.countryWithCode[indexPath.row].countryCode
        } else {
            cell.nameLabel.text = filteredArray[indexPath.row].countryName
            cell.codeLabel.text = filteredArray[indexPath.row].countryCode
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if filteredArray.count == 0 {
            selectionAction?(list.countryWithCode[indexPath.row].countryName, list.countryWithCode[indexPath.row].countryCode)
        } else {
            selectionAction?(filteredArray[indexPath.row].countryName, filteredArray[indexPath.row].countryCode)
        }
        navigationController?.popViewController(animated: true)
    }
}
