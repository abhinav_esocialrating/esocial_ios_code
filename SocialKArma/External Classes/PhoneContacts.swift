//
//  PhoneContacts.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 14/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import ContactsUI

class PhoneContacts {
    class func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let store = CNContactStore()
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            completionHandler(false)
        case .restricted, .notDetermined:
            store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            }
        @unknown default:
            print("")
        }
    }
    class func getContacts(filter: ContactsFilter = .none) -> [CNContact] { //  ContactsFilter is Enum find it below
        
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName) as CNKeyDescriptor,
            CNContactPhoneNumbersKey as CNKeyDescriptor] //as [Any]
        
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        for container in allContainers {
            
            let request = CNContactFetchRequest(keysToFetch: keysToFetch )
            do {
                try contactStore.enumerateContacts(with: request, usingBlock: { (cnContact, stop) in
                    results.append(cnContact)
                    print(results.count)
//                    chunks.append(contact)
//                    let contact = PhoneContact(contact: cnContact)
//                    for (j,i) in contact.phoneNumber.enumerated() {
//                        var dict = [String:Any]()
//                        dict["name"] = contact.name ?? ""
//                        dict["mobile_number"] = i.extractDecimalDigits()
//                        dict["country_code"] = contact.countryCode[j] == "" ? "" : "+\(contact.countryCode[j])"
//                        contactArray.append(dict)
//                    }
//                    if contactArray.count >= 50 {
//                        print("chunks.count == 50")
//                        CoreDataHelper.shared.createData(contacts: contactArray)
//                        contactArray = []
//                    }
                })
            } catch {
                print("Contact fetch error!")
            }
            
//        CoreDataHelper.shared.createData(contacts: contactArray)
//        contactArray = []
        
//            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
//            do {
//                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
//                results.append(contentsOf: containerResults)
//            } catch {
//                print("Error fetching containers")
//            }
            
        }
        return results
    }
    
    class func getContactsAll(filter: ContactsFilter = .none) -> [[String:Any]] { //  ContactsFilter is Enum find it below
            
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName) as CNKeyDescriptor,
            CNContactPhoneNumbersKey as CNKeyDescriptor] //as [Any]
                
        var results: [[String:Any]] = []
                    
        let request = CNContactFetchRequest(keysToFetch: keysToFetch )
        do {
            try contactStore.enumerateContacts(with: request, usingBlock: { (cnContact, stop) in
                let contact = PhoneContact(contact: cnContact)
                for (j,i) in contact.phoneNumber.enumerated() {
                    if let name = contact.name, name.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
                        
                        var dict = [String:Any]()
                        dict["name"] = name
                        dict["mobile_number"] = i.extractDecimalDigits()
                        dict["country_code"] = contact.countryCode[j] == "" ? "" : "+\(contact.countryCode[j])"
                        results.append(dict)
                    }
                }

            })
        } catch {
            print("Contact fetch error!")
        }
        return results
    }
}
enum ContactsFilter {
    case none
    case mail
    case message
}
