//
//  SendData.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 05/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation

protocol SendData {
    func passData(dataPasser: Any, data: Any)
}
