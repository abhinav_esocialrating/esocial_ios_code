//
//  NotificationsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import FAPanels
import FirebaseAnalytics

class NotificationsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    // MARK: - Variables
    let viewModel = NotificationsViewModel()
    var isFromPush = false
    var notificationType = ""
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 78
        PointsHandler.shared.isNotification = false
        getNotification()
        if isFromPush {
            Analytics.logEvent("promotional_notification_open", parameters: [
            "name": "notification_type" as NSObject,
            "full_text": notificationType as NSObject
            ])
        }
    }
    
    // MARK: - Navigation
    
    @IBAction func backTapped(_ sender: Any) {
//        self.updateNotifications()
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Web Service
    func getNotification() {
        viewModel.getNotification(parameters: [:]) { (success, json, message) in
            if success {
                self.updateUI()
                self.updateNotifications()
            } else {
                self.updateUI()
            }
        }
    }
    func updateNotifications() {
        let parmas = ["userNotificationId": viewModel.fetchNewNotificationIds()]
        viewModel.updateNotification(parameters: parmas) { (success, json, message) in
        }
    }
    func contactSearch(query: String) {
        let params = ["query": query]
        viewModel.contactSearch(parameters: params) { (success, json, message) in
            if success {
                let userInfo = ["contact_user_id": json.intValue, "notif": true] as [String : Any]
                NotificationCenter.default.post(name: Notification.Name.AppNotifications.ReviewBack, object: self, userInfo: userInfo)
            }
        }
    }
    // MARK: - Methods
    func scorerequestResponse(action: Int, indexPath: IndexPath) {
        if action == 10 {
            self.dismiss(animated: true) {
                var name = "0"
                var sourceId = 0
                if indexPath.section == 0 {
                    name = self.viewModel.newNotifications[indexPath.row]?.sourceName ?? ""
                    sourceId = self.viewModel.newNotifications[indexPath.row]?.sourceUserId ?? 0
                } else {
                    name = self.viewModel.earlierNotifications[indexPath.row]?.sourceName ?? ""
                    sourceId = self.viewModel.earlierNotifications[indexPath.row]?.sourceUserId ?? 0
                }
                self.reviewBackFromNotifications(contact_user_id: sourceId)
//                self.contactSearch(query: name)
            }
        } else if action == 11 {
            // Nothing for now
        } else {
            var userId = 0
            if indexPath.section == 0 {
                userId = viewModel.newNotifications[indexPath.row]?.sourceUserId ?? 0
            } else {
                userId = viewModel.earlierNotifications[indexPath.row]?.sourceUserId ?? 0
            }
            let param = ["userId": userId, "status": action]
            viewModel.scorerequestResponse(parameters: param) { (success, json, message) in
                if success {
                    if let points = json.int {
                        PointsHandler.shared.updateTZPoints(points: points, changedToBeShown: true)
                    }
                    self.getNotification()
                }
            }
        }
    }
    func reviewBackFromNotifications(contact_user_id: Int) {
        let userInfo = ["contact_user_id": contact_user_id, "notif": true, "notification_type": self.notificationType] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name.AppNotifications.ReviewBack, object: self, userInfo: userInfo)
    }
    func updateUI() {
        DispatchQueue.main.async {
            if self.viewModel.newNotifications.count == 0 && self.viewModel.earlierNotifications.count == 0 {
                self.tableView.isHidden = true
                self.noDataLabel.isHidden = false
            } else {
                self.tableView.isHidden = false
                self.noDataLabel.isHidden = true
            }
            self.tableView.reloadData()
        }
    }
    func openProfileV2(id: Int) {
        let vc = ProfileHomeViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.otherUserId = id
        vc.userType = 2
        ProfileV2Data.sharedInstance.userType = 2
        ProfileV2Data.sharedInstance.otherUserId = id
        navigationController?.pushViewController(vc, animated: true)
    }
    func openRedeem() {
        let vc = RedeemPopUpViewController.instantiate(fromAppStoryboard: .MainExt)
        vc.redeemInfo = { (number, done) in
            self.remove()
            if done {
                self.redeemPoints(number: number) { _ in
                }
            }
        }
        add(vc)
    }
}
extension NotificationsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? viewModel.newNotifications.count : viewModel.earlierNotifications.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.NotificationCell.rawValue, for: indexPath) as! NotificationCell
        cell.indexPath = indexPath
        cell.notification = indexPath.section == 0 ? viewModel.newNotifications[indexPath.row] : viewModel.earlierNotifications[indexPath.row]
        cell.notificationActions = { action in
            self.scorerequestResponse(action: action, indexPath: indexPath)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return viewModel.newNotifications.count == 0 ? "" : "New"
        default:
            return viewModel.earlierNotifications.count == 0 ? "" : "Earlier"
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return viewModel.newNotifications.count > 0 ? 44 : CGFloat.leastNonzeroMagnitude
        } else {
            return viewModel.earlierNotifications.count > 0 ? 44 : CGFloat.leastNonzeroMagnitude
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let notif = indexPath.section == 0 ?  viewModel.newNotifications[indexPath.row] : viewModel.earlierNotifications[indexPath.row] else {
            return
        }
        if let id = notif.sourceUserId, let ownId = Int(UserDefaults.standard.userId), id != ownId {

            handleNotifications(userInfo: ["type": notif.notificationType ?? ""], type: .didReceiveResponse, id: id, NotificationObj: notif)

        } else {
            handleNotifications(userInfo: ["type": notif.notificationType ?? ""], type: .didReceiveResponse, id: 0, NotificationObj: notif)
        }
        
    }
}

// MARK: - Handling Push Notifications
extension NotificationsViewController {
    func handleNotifications(userInfo: [AnyHashable:Any], type: NotificationHandleType, id: Int, NotificationObj: NotificationModel) {
        let notificationType = userInfo["type"] as? String
        if type == .didReceiveResponse {
            switch notificationType {
            case PushNotificationType.CHAT.rawValue, PushNotificationType.CHAT_UNREAD_MESSAGES.rawValue:
                if let panelVC = appDelegate?.window?.rootViewController as? FAPanelController, let tbc = panelVC.center as? TabBarController {
                    
                    var editedUserInfo = userInfo
                    editedUserInfo["notificationHandleType"] = type.rawValue
                    
                    if let presented = tbc.presentedViewController, presented is ChatNavigationController  {
                        
                        NotificationCenter.default.post(name: Notification.Name.PushReceivedChat, object: nil, userInfo: editedUserInfo)
                        
                    } else if let presented = tbc.presentedViewController {
                        presented.dismiss(animated: false) {
                            self.handleChatNotifications(editedUserInfo: editedUserInfo, tbc: tbc)
                        }
                    } else {
                        self.handleChatNotifications(editedUserInfo: editedUserInfo, tbc: tbc)
                    }
                    
                }
            case PushNotificationType.FEEDBACK_UE.rawValue, PushNotificationType.CONTACT_UE.rawValue, PushNotificationType.PROFILE_UE.rawValue, PushNotificationType.FEEDBACK_UE_2.rawValue, PushNotificationType.INVITE_UE.rawValue:
                
                handlePromotionalNotifications(notificationType: PushNotificationType(rawValue: notificationType!)!)
                
            case PushNotificationType.UE_PERSONAL_NEWS.rawValue, PushNotificationType.UE_FEEDBACK_REMINDER_1.rawValue, PushNotificationType.UE_FEEDBACK_REMINDER_2.rawValue, PushNotificationType.UE_FEEDBACK_REMINDER_3.rawValue, PushNotificationType.UE_FEEDBACK_REMINDER_4.rawValue, PushNotificationType.UE_FEEDBACK_REMINDER_5.rawValue, PushNotificationType.UE_FEEDBACK_REMINDER_6.rawValue, PushNotificationType.UE_FEEDBACK_REMINDER_7.rawValue, PushNotificationType.UE_INFO_1.rawValue, PushNotificationType.UE_PERSONAL_NEWS_2.rawValue, PushNotificationType.UE_PROFILE_COMPLETION_2.rawValue, PushNotificationType.UE_PROFILE_COMPLETION_1.rawValue, PushNotificationType.UE_REINVITE_1.rawValue, PushNotificationType.UE_REINVITE_2.rawValue,  PushNotificationType.OFFER_REJECT.rawValue:
                
                handlePromotionalNotifications(notificationType: PushNotificationType(rawValue: notificationType!)!)
                
            case PushNotificationType.COINS_TO_CASH.rawValue:
                
                openRedeem()
                
            case PushNotificationType.OFFER_ACCEPT.rawValue,  PushNotificationType.OFFER_LIKE_NOTIFICATION.rawValue, PushNotificationType.OFFER_COMMENT_ORGANIC.rawValue, PushNotificationType.OFFER_COMMENT_NOTIFICATION.rawValue:
                
                openOfferDetails(type: notificationType ?? "", offerId: NotificationObj.offerId ?? 0)
                
            case PushNotificationType.SCORE_UPDATE.rawValue:
//                openProfileV2(id: id)
                openOtherUserProfile(userId: id)
            case PushNotificationType.REVIEW.rawValue, PushNotificationType.REQUEST_SCORE.rawValue, PushNotificationType.REQUEST_SCORE_REJECT.rawValue:
                
                dismiss(animated: true) {
                    self.showScoreFromScoreUpdateNotification()
                }
                
            case PushNotificationType.CONTACT_JOINING.rawValue:
                if let panelVC = appDelegate?.window?.rootViewController as? FAPanelController, let tbc = panelVC.center as? TabBarController {

                    if let presented = tbc.presentedViewController {
                        presented.dismiss(animated: false) {
                            self.openProfile(tbc: tbc, id: 0)
                        }
                    } else {
                        self.openProfile(tbc: tbc, id: 0)
                    }
                }
            default:
                if let panelVC = appDelegate?.window?.rootViewController as? FAPanelController, let tbc = panelVC.center as? TabBarController {
                    
                    if let presented = tbc.presentedViewController {
                        presented.dismiss(animated: false) {
                            tbc.selectedIndex = 0
                        }
                    } else {
                        tbc.selectedIndex = 0
                    }
                    if notificationType == "FEEDBACK_REQUEST" {
                        self.reviewBackFromNotifications(contact_user_id: NotificationObj.sourceUserId ?? 0)
                    }
                }
            }
        } else {
        }
    }
    func showBadge() {
        if let panelVC = appDelegate?.window?.rootViewController as? FAPanelController, let tbc = panelVC.center as? TabBarController, let tabItems = tbc.tabBar.items {
            let tabItem = tabItems[2]
            tabItem.badgeColor = UIColor.red
            tabItem.badgeValue = ""
        }
    }
    
    func handlePromotionalNotifications(notificationType: PushNotificationType) {
        if let panelVC = appDelegate?.window?.rootViewController as? FAPanelController, let tbc = panelVC.center as? TabBarController {
            
            if let presented = tbc.presentedViewController {
                presented.dismiss(animated: false) {
                    self.selectTabsOnPromotionalNotifcations(notificationType: notificationType, tbc: tbc)
                }
            } else {
                selectTabsOnPromotionalNotifcations(notificationType: notificationType, tbc: tbc)
            }
        }
    }
    func selectTabsOnPromotionalNotifcations(notificationType: PushNotificationType, tbc: TabBarController) {
        
        switch notificationType {
        case .FEEDBACK_UE, .FEEDBACK_UE_2, .UE_PERSONAL_NEWS, .UE_FEEDBACK_REMINDER_1, .UE_FEEDBACK_REMINDER_2, .UE_FEEDBACK_REMINDER_4, .UE_FEEDBACK_REMINDER_5, .UE_FEEDBACK_REMINDER_7, .UE_FEEDBACK_REMINDER_3, .UE_FEEDBACK_REMINDER_6:
            tbc.selectedIndex = 0
        case .CONTACT_UE, .INVITE_UE:
            tbc.selectedIndex = 2
        case .UE_REINVITE_1, .UE_REINVITE_2:
            tbc.selectedIndex = 1
            if let presented = tbc.presentedViewController {
                presented.dismiss(animated: false) {
                    self.navigatePromotionalNotifForScoreCard(notificationType: notificationType, tbc: tbc)
                }
            } else {
                navigatePromotionalNotifForScoreCard(notificationType: notificationType, tbc: tbc)
            }
        case .COINS_TO_CASH:
            tbc.selectedIndex = 4
        default:  // .PROFILE_UE
            tbc.selectedIndex = 3
            ProfileV2Data.sharedInstance.notificationType = notificationType.rawValue
        }
    }
    func navigatePromotionalNotifForScoreCard(notificationType: PushNotificationType, tbc: TabBarController) {
        switch notificationType {
        case .UE_REINVITE_1, .UE_REINVITE_2:
            tbc.openNetworkVC(screenType: 3, title: "Invitations Sent")
        default:
            AppDebug.shared.consolePrint("Handled in the Home Controller")
        }
    }
    func handleChatNotifications(editedUserInfo: [AnyHashable: Any], tbc: TabBarController) {
        let vc = ChatNavigationController.instantiate(fromAppStoryboard: .Chat)
        vc.modalPresentationStyle = .fullScreen
        tbc.present(vc, animated: true) {
            NotificationCenter.default.post(name: Notification.Name.PushReceivedChat, object: nil, userInfo: editedUserInfo)
        }
    }
    func openProfile(tbc: TabBarController, id: Int) {
        let vc = ProfileDetailsViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.otherUserId = id
        tbc.present(vc, animated: true, completion: nil)
        
//        ProfileV2Data.sharedInstance.reset()
//        let vc = ProfileHomeViewController.instantiate(fromAppStoryboard: .ProfileV2)
//        vc.userType = 2
//        vc.otherUserId = id
//        ProfileV2Data.sharedInstance.userType = 2
//        ProfileV2Data.sharedInstance.otherUserId = id
//        tbc.present(vc, animated: true, completion: nil)
    }
    func openOfferDetails(type: String, offerId: Int) {
        switch type {
        case PushNotificationType.OFFER_ACCEPT.rawValue,  PushNotificationType.OFFER_LIKE_NOTIFICATION.rawValue, PushNotificationType.OFFER_COMMENT_ORGANIC.rawValue, PushNotificationType.OFFER_COMMENT_NOTIFICATION.rawValue:
            
            moveToDetailScreen(id: offerId)
        default:
            AppDebug.shared.consolePrint("")
        }
    }
    func moveToDetailScreen(id: Int) {
        let vc = OfferDetailsViewController.instantiate(fromAppStoryboard: .Offers)
        vc.viewModel.offerId = id
        navigationController?.pushViewController(vc, animated: true)
    }
}
