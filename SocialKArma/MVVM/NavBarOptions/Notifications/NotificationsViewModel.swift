//
//  NotificationsViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationsViewModel: NSObject {
    var notifications = [["name": "Sanjay Sharma", "type": "rated you", "time": "1 hour ago"]]
    var readNotifications = [["name": "Chandini Sharma", "type": "messaged you", "time": "3 hours ago"]]
    
    var newNotifications = [NotificationModel?]()
    var earlierNotifications = [NotificationModel?]()
    
    // MARK: - Web Services
    func getNotification(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.notification(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getNotification")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                self.parseDataAndUpdateDataSource(json: json)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func updateNotification(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.notification(parameters: parameters, method: .put) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("updateNotification")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func scorerequestResponse(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.scorerequestResponse(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("scorerequestResponse")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"]
                completion(true, data, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func contactSearch(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.contactSearch(parameters: parameters, method: .get) { (response) in
            
            switch response.response?.statusCode {
            case 200:
                var contact_user_id = 0
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData.value(forKey: "data") as? NSArray
                    if (data?.count ?? 0) > 0 {
                        let contactsSearchedFromRemote = ContactSearch.modelsFromDictionaryArray(array: data!)
                        contact_user_id = contactsSearchedFromRemote[0].contact_user_id ?? 0
                    }
                } catch {
                
                }
                completion(true, JSON(contact_user_id), "")
            default:
                let json = JSON(response.result.value ?? JSON.init())
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    // MARK: - Methods
    func parseDataAndUpdateDataSource(json: JSON) {
        let data = json["data"].dictionaryValue
        self.newNotifications = []
        self.earlierNotifications = []
        var noti = data["new"]?.arrayValue
        noti?.forEach({ (item) in
            let notif = NotificationModel(dictionary: item.dictionaryValue)
            self.newNotifications.append(notif)
        })
        noti = data["earlier"]?.arrayValue
        noti?.forEach({ (item) in
            let notif = NotificationModel(dictionary: item.dictionaryValue)
            self.earlierNotifications.append(notif)
        })
    }
    
    func fetchNewNotificationIds() -> [Int] {
        return newNotifications.map({$0?.userNotificationId ?? 1})
    }
}
