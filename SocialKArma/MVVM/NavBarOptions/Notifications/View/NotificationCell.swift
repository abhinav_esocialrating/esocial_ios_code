//
//  NotificationCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var notificationLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var userImage: UIImageView!
            
    @IBOutlet weak var actionView: UIView!
    
    @IBOutlet weak var actionTextLabel: UILabel!
    
    @IBOutlet var actionButtons: [UIButton]!
    
    @IBOutlet weak var actionViewHeight: NSLayoutConstraint!
    
    var notificationActions: ((_ action: Int) -> Void)? = nil // 1. accept 0. reject 3. ignore  10: review back  11: add to contacts
    var notification: NotificationModel! {
        didSet {
            setValues()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        userImage.layer.cornerRadius = userImage.frame.width/2
    }
    func setValues() {   //type:  1: new  2: earlier
        notificationLabel.text = notification.body
        timeLabel.text = notification.time
        if let url = notification.sourceUserImageId, url != "" {
            userImage.sd_setImage(with: URL(string: url), completed: nil)
        } else {
            userImage.image = UIImage(named: "avatar-2")
        }
        // Action buttons
        actionButtons[0].titleLabel?.numberOfLines = 0
        actionButtons.forEach { (btn) in
            btn.titleLabel?.textAlignment = .left
        }
        actionButtons[0].titleLabel?.textAlignment = .left
        if notification.notificationType == NotificationType.REVIEW.rawValue {
            actionViewHeight.constant = 34
            actionTextLabel.text = ""
            if let canReviewBack = notification.canReviewBack, canReviewBack == 0 {
                actionButtons[0].setTitle("You can't give feedback to people not in the contact list", for: .normal)
            } else {
                actionButtons[0].setTitle("Review Back", for: .normal)
            }
            actionButtons[1].setTitle("Reject", for: .normal)
            actionButtons[2].setTitle("Ignore", for: .normal)
            actionButtons[0].isHidden = false
            actionButtons[1].isHidden = true
            actionButtons[2].isHidden = true
            actionButtons[0].setTitleColor(UIColor.NewTheme.lightBlue, for: .normal)
        } else if notification.notificationType == NotificationType.REQUEST_SCORE.rawValue {
            actionButtons[0].setTitle("Accept", for: .normal)
            actionButtons[1].setTitle("Reject", for: .normal)
            actionButtons[2].setTitle("Ignore", for: .normal)
            actionButtons[0].setTitleColor(UIColor.darkGray, for: .normal)
            if notification.status == NotificationType.REQUESTED.rawValue {
                actionViewHeight.constant = 52
                actionTextLabel.text = NotificationType.GiveAccess
                actionButtons.forEach({$0.isHidden = false})
            } else {
                actionViewHeight.constant = 18
                actionButtons.forEach({$0.isHidden = true})
                if notification.status == NotificationType.ACCEPTED.rawValue {
                    actionTextLabel.text = NotificationType.RequestAccepted
                } else if notification.status == NotificationType.REJECTED.rawValue {
                    actionTextLabel.text = NotificationType.RequestRejected
                } else if notification.status == NotificationType.IGNORE.rawValue {
                    actionTextLabel.text = NotificationType.RequestIgnored
                }
            }
        } else {
            actionButtons[0].setTitle("Accept", for: .normal)
            actionButtons[1].setTitle("Reject", for: .normal)
            actionButtons[2].setTitle("Ignore", for: .normal)
            actionButtons[0].setTitleColor(UIColor.darkGray, for: .normal)
            actionButtons.forEach({$0.isHidden = true})
            actionViewHeight.constant = 0
        }
        
    }
    // MARK: - Actions
    @IBAction func actionButtonTapped(_ sender: Any) {
        switch (sender as! UIButton).tag {
        case 0:
            if notification.notificationType == NotificationType.REVIEW.rawValue {
                // review back action
                if let canReviewBack = notification.canReviewBack, canReviewBack == 0 {
                    notificationActions?(11)
                } else {
                    notificationActions?(10)
                }
            } else {
                notificationActions?(1)
            }
        case 1:
            notificationActions?(0)
        default:
            notificationActions?(3)
        }
        
//        if notification.notificationType == NotificationType.REVIEW.rawValue {
//            // review back action
//            notificationActions?(10)
//        } else if notification.notificationType == NotificationType.REQUEST_SCORE.rawValue {
//            if notification.status == NotificationType.ACCEPTED.rawValue {
//                notificationActions?(1)
//            } else if notification.status == NotificationType.REJECTED.rawValue {
//                notificationActions?(0)
//            } else if notification.status == NotificationType.IGNORE.rawValue {
//                notificationActions?(3)
//            }
////            1. accept 0. reject 3. ignore
//        } else {
//            // 9
//        }
    }
    
}

fileprivate enum NotificationType: String {
    typealias RawValue = String
    
    case REQUEST_SCORE = "REQUEST_SCORE"
    case REQUESTED = "REQUESTED"
    case REVIEW = "REVIEW"
    case ACCEPTED = "ACCEPTED"
    case REJECTED = "REJECTED"
    case IGNORE = "IGNORE"
    
    static let ReviewBack = "Review Back"
    static let RequestAccepted = "Request accepted"
    static let RequestRejected = "Request rejected"
    static let RequestIgnored = "Request ignored"
    static let GiveAccess = "Give access to view score?"
}
