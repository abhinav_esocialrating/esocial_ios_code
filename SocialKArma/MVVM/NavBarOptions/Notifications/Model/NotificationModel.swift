
import Foundation
import SwiftyJSON

public class NotificationModel {
	public var isShown : Int?
	public var sourceUserImageId : String?
	public var sourceName : String?
	public var sourceUserId : Int?
    public var offerId : Int?
	public var status : String?
	public var body : String?
	public var userNotificationId : Int?
	public var time : String?
	public var notificationType : String?
	public var targetUserId : Int?
    public var canReviewBack : Int?

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let NotificationModel = NotificationModel(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: NotificationModel Instance.
*/
    required public init?(dictionary: [String:JSON]) {

        isShown = dictionary["isShown"]?.int
		sourceUserImageId = dictionary["sourceUserImageId"]?.string
		sourceName = dictionary["sourceName"]?.string
		sourceUserId = dictionary["sourceUserId"]?.int
        offerId = dictionary["offerId"]?.int
		status = dictionary["status"]?.string
		body = dictionary["body"]?.string
		userNotificationId = dictionary["userNotificationId"]?.int
		time = dictionary["time"]?.string
		notificationType = dictionary["notificationType"]?.string
		targetUserId = dictionary["targetUserId"]?.int
        canReviewBack = dictionary["canReviewBack"]?.int
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.isShown, forKey: "isShown")
		dictionary.setValue(self.sourceUserImageId, forKey: "sourceUserImageId")
		dictionary.setValue(self.sourceName, forKey: "sourceName")
		dictionary.setValue(self.sourceUserId, forKey: "sourceUserId")
        dictionary.setValue(self.offerId, forKey: "offerId")
		dictionary.setValue(self.status, forKey: "status")
		dictionary.setValue(self.body, forKey: "body")
		dictionary.setValue(self.userNotificationId, forKey: "userNotificationId")
		dictionary.setValue(self.time, forKey: "time")
		dictionary.setValue(self.notificationType, forKey: "notificationType")
		dictionary.setValue(self.targetUserId, forKey: "targetUserId")
        dictionary.setValue(self.canReviewBack, forKey: "canReviewBack")

		return dictionary
	}

}
