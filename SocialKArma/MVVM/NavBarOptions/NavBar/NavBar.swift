//
//  NavBar.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 19/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class NavBar: UIView {

    var actions: ((_ action: NavButtonActions) -> Void)? = nil
    
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        actions?(.backTap)
    }
    
    enum NavButtonActions {
        case backTap
        case pointsTap
        case notificationTap
    }
}
