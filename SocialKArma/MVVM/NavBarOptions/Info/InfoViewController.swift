//
//  InfoViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var bkgView: UIView!
    
    var info: String?
    var removeInfo: completionBlock? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addTapGesture()
        infoLabel.text = info
        bkgView.layer.cornerRadius = 12
    }
    

    
    // MARK: - Navigation
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(InfoViewController.viewTapped(_:)))
        containerView.addGestureRecognizer(tap)
    }

    @objc func viewTapped(_ gesture: UIGestureRecognizer) {
        removeInfo?()
//        dismiss(animated: false, completion: nil)
    }

}
