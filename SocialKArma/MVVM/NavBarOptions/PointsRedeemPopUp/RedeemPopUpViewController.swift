//
//  RedeemPopUpViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class RedeemPopUpViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var numberTF: UITextField!
    @IBOutlet weak var redeemBtn: UIButton!
    
    var redeemInfo: ((_ number: String, _ done: Bool) -> Void)? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        redeemBtn.layer.cornerRadius = redeemBtn.frame.height/2
        addTapGesture()
    }
    
    // MARK: - Navigation
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(InfoViewController.viewTapped(_:)))
        containerView.addGestureRecognizer(tap)
    }

    @objc func viewTapped(_ gesture: UIGestureRecognizer) {
        redeemInfo?(numberTF.text!, false)
    }
    @IBAction func redeemBtnTapped(_ sender: Any) {
        if numberTF.text!.count < 10 || numberTF.text!.count > 10 {
            alert(message: "Please enter a valid number!")
        } else {
            redeemInfo?(numberTF.text!, true)
        }
    }
    
}
