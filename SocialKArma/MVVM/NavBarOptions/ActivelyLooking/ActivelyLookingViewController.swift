//
//  ActivelyLookingViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ActivelyLookingViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var friendsSwitch: UISwitch!
    @IBOutlet weak var profSwitch: UISwitch!

    var subscriber: completionBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addTapGesture()
        submitBtn.layer.cornerRadius = submitBtn.frame.height/2
        
        friendsSwitch.isOn = UserDefaults.standard.SearchStatus["friendStatus"] as! Int == 1
        profSwitch.isOn = UserDefaults.standard.SearchStatus["professionalStatus"] as! Int == 1

    }
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(InfoViewController.viewTapped(_:)))
        containerView.addGestureRecognizer(tap)
    }
    @objc func viewTapped(_ gesture: UIGestureRecognizer) {
        dismiss(animated: false, completion: nil)
        subscriber?()
    }
    
    // MARK: - Actions

    @IBAction func friendSwitchTapped(_ sender: Any) {
    }
    @IBAction func professionalSwitchTapped(_ sender: Any) {
    }
    @IBAction func submitTapped(_ sender: Any) {
        let params = ["friendStatus": friendsSwitch.isOn ? 1 : 0, "professionalStatus": profSwitch.isOn ? 1 : 0]
        UIViewController.changeSearchStatus(parameters: params, method: .put) { (_) in
            self.subscriber?()
            NotificationCenter.default.post(name: Notification.Name.SearchStatus, object: self)
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    // MARK: - Methods
    
}
