//
//  PointsModel+CoreDataClass.swift
//  
//
//  Created by Abhinav Dobhal on 25/10/20.
//
//

import Foundation
import CoreData


public class PointsModel: NSManagedObject {

}
extension PointsModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PointsModel> {
        return NSFetchRequest<PointsModel>(entityName: "PointsModel")
    }

    @NSManaged public var created_at: String?
    @NSManaged public var message: String?
    @NSManaged public var tz_points_value: Int16
    @NSManaged public var g_id: Int32

    
    func update(with dictionary: [String: Any]) {

        created_at = dictionary["created_at"] as? String
        message = dictionary["message"] as? String
        tz_points_value = dictionary["tz_points_value"] as? Int16 ?? 0
    }
}
