/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Current_week {
	public var total_current_points : Int?
	public var week : String?
	public var points_used : Int?
	public var points_earned : Int?
	public var last_balance : Int?
	public var points : Array<Points>?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let current_week_list = Current_week.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Current_week Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Current_week]
    {
        var models:[Current_week] = []
        for item in array
        {
            models.append(Current_week(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let current_week = Current_week(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Current_week Instance.
*/
	required public init?(dictionary: NSDictionary) {

		total_current_points = dictionary["total_current_points"] as? Int
		week = dictionary["week"] as? String
		points_used = dictionary["points_used"] as? Int
		points_earned = dictionary["points_earned"] as? Int
		last_balance = dictionary["last_balance"] as? Int
        if let pointss = dictionary["points"] as? NSArray {
            points = Points.modelsFromDictionaryArray(array: pointss)
        }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.total_current_points, forKey: "total_current_points")
		dictionary.setValue(self.week, forKey: "week")
		dictionary.setValue(self.points_used, forKey: "points_used")
		dictionary.setValue(self.points_earned, forKey: "points_earned")
		dictionary.setValue(self.last_balance, forKey: "last_balance")

		return dictionary
	}

}
