//
//  PointsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class PointsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableFooter: UIView!
    
    @IBOutlet weak var pointsEarnedLegend: UILabel!
    
    @IBOutlet weak var pointsUsedLegend: UILabel!
    
    // MARK: - Actions
    let viewModel = PointsViewModel()
    var filterType = 1
    var isDetailViewShown = false
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        self.viewModel.fetchData {
            self.updateUI()
        }
        tzPointsHistory()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        pointsEarnedLegend.layer.cornerRadius = 4
//        pointsUsedLegend.layer.cornerRadius = 4
    }
    func configureView() {
        tableView.register(UINib(nibName: TableViewCells.ExperienceHeaderCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.ExperienceHeaderCell.rawValue)
        tableView.estimatedRowHeight = 230
    }

    
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    // MARK: - Methods
    func updateUI() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func openRedeem() {
        let vc = RedeemPopUpViewController.instantiate(fromAppStoryboard: .MainExt)
        vc.redeemInfo = { (number, done) in
            self.remove()
            if done {
                self.redeemPoints(number: number) { success in
                    if success {
                        self.tzPointsHistory()
                    }
                }
            }
        }
        add(vc)
    }
    // MARK: - Web services
    func tzPointsHistory() {
        let params = ["id": UserDefaults.standard.userId]
        viewModel.tzPointsHistory(parameters: params) { (success, json, message) in
            if success {
                self.viewModel.fetchData {
                    self.updateUI()
                }
            }
        }
    }
    func redeemPoints(number: String) {
        showHud()
        let params = ["code":"POINTS_REDEEM_500", "number":number]
        viewModel.redeemPoints(parameters: params) { (success, _, message) in
            self.hideHud()
            if success {
                self.showAutoToast(message: message)
                self.tzPointsHistory()
            } else {
                self.alert(message: message)
            }
        }
    }
}
extension PointsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return viewModel.currentWeek != nil ? 1 : 0
        case 1:
            switch filterType {
            case 1:
                let count = viewModel.totalPoints?.count ?? 0
                return count > 0 ? count : 1
            case 2:
                let count = viewModel.credit?.count ?? 0
                return count > 0 ? count : 1
            default:
                let count = viewModel.debit?.count ?? 0
                return count > 0 ? count : 0
            }
        default:
            let count = viewModel.pointsHistory?.history?.count ?? 0
            return count > 0 ? count : 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.PointsSummaryCell.rawValue, for: indexPath) as! PointsSummaryCell
            cell.currentWeek = viewModel.currentWeek
            cell.setValues()
            cell.setShowMore(isDetailViewShown: isDetailViewShown)
            cell.showMore = {
                self.isDetailViewShown = !self.isDetailViewShown
                self.updateUI()
            }
            cell.redeem = {
                self.openRedeem()
            }
            return cell
        case 1:
            if (viewModel.totalPoints?.count ?? 0) > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.PointsDetailsCell.rawValue, for: indexPath) as! PointsDetailsCell
                switch filterType {
                case 1:
                    cell.pointsModel = viewModel.totalPoints?[indexPath.row]
                case 2:
                    cell.pointsModel = viewModel.credit?[indexPath.row]
                default:
                    cell.pointsModel = viewModel.debit?[indexPath.row]
                }
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
                cell.textLabel?.text = "    No Data found"
                cell.textLabel?.textColor = UIColor.NewTheme.darkGray
                cell.textLabel?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
                return cell
            }
        default:
            if (viewModel.pointsHistory?.history?.count ?? 0) > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.PointsDetailsCell.rawValue, for: indexPath) as! PointsDetailsCell
                cell.point = viewModel.pointsHistory?.history?[indexPath.row]
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
                cell.textLabel?.text = "    No Data found"
                cell.textLabel?.textColor = UIColor.NewTheme.darkGray
                cell.textLabel?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
                return cell
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return isDetailViewShown ? 430 : 174
        default:
            return 70
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return CGFloat.leastNonzeroMagnitude
        case 1:
            return 90
        default:
            return 40
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            return nil
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.PointsHeaderCell.rawValue) as! PointsHeaderCell
            cell.filterType = filterType
            cell.setButtonStates()
            cell.filterTapped = { type in
                self.filterType = type
                self.updateUI()
            }
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ExperienceHeaderCell.rawValue) as! ExperienceHeaderCell
            cell.label.text = "Previous History"
            cell.editImage.isHidden = true
            cell.editButton.isHidden = true
            cell.label.textColor = UIColor.NewTheme.darkGray
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude //section == 0 ? 44 : CGFloat.leastNonzeroMagnitude
    }
/*
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return section == 0 ? tableFooter : nil
    }
 */
}
