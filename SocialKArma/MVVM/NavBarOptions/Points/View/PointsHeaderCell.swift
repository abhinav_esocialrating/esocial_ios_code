//
//  PointsHeaderCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class PointsHeaderCell: UITableViewCell {
    
    @IBOutlet var filterBtn: [UIButton]!
    var filterType = 1
    
    var filterTapped: ((_ type: Int) -> Void)? = nil  // 1: all   2: credit  3: debit

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        filterBtn.forEach { (btn) in
            btn.layer.cornerRadius = 8
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setButtonStates() {
        filterBtn.forEach { (btn) in
            if btn.tag == filterType {
                btn.backgroundColor = UIColor.SearchColors.blue
                btn.setTitleColor(UIColor.white, for: .normal)
            } else {
                btn.backgroundColor = UIColor.black.withAlphaComponent(0.1)
                btn.setTitleColor(UIColor.darkGray, for: .normal)
            }
        }
    }
    // MARK: - Actions

    @IBAction func filterTapped(_ sender: Any) {
        let senderBtn = sender as? UIButton
        filterType = senderBtn?.tag ?? 0
        setButtonStates()
        filterTapped?(senderBtn?.tag ?? 0)
    }
}
