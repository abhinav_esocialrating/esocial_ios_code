//
//  PointsDetailsCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class PointsDetailsCell: UITableViewCell {

    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    var point: Points! {
        didSet {
            setValues()
        }
    }
    var pointsModel: PointsModel! {
        didSet {
            setValues()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        pointLabel.layer.cornerRadius = 8//pointLabel.frame.height / 2
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func setValues() {
        
        messageLabel.text = pointsModel.message
        timeLabel.text = pointsModel.created_at
        pointLabel.text = "\(abs(pointsModel.tz_points_value))"
        pointLabel.backgroundColor = pointsModel.tz_points_value >= 0 ? UIColor.ScoreColors.creditBkg : UIColor.ScoreColors.debitBkg
        pointLabel.textColor = pointsModel.tz_points_value >= 0 ? UIColor.ScoreColors.creditText : UIColor.ScoreColors.debitText
        
//        if let tz_points_value = pointsModel.tz_points_value {
//        } else {
//            pointLabel.text = "0"
//            pointLabel.backgroundColor = UIColor.SearchColors.blue
//        }
    }

}
