//
//  PointsSummaryCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class PointsSummaryCell: UITableViewCell {

    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!

    @IBOutlet weak var label3: UILabel!

    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var showMoreTop: NSLayoutConstraint!
    
    @IBOutlet weak var showMoreBtn: UIButton!
    
    @IBOutlet weak var pointsLabel: UILabel!
    
    @IBOutlet weak var redeemBtn: UIButton!
    
    // MARK: - Variables
    var currentWeek: Current_week?
    var showMore: completionBlock? = nil
    var redeem: completionBlock? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        containerView.layoutSubviews()
        containerView.layer.cornerRadius = 12
        containerView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        redeemBtn.layer.cornerRadius = redeemBtn.frame.height/2
    }
    func setValues() {
        label1.text = currentWeek?.week
        label2.text = "\(currentWeek?.last_balance ?? 0)"
        label3.text = "\(currentWeek?.points_earned ?? 0)"
        label4.text = "\(currentWeek?.points_used ?? 0)"
        label5.text = "\(currentWeek?.total_current_points ?? 0)"
        pointsLabel.text = "\(currentWeek?.total_current_points ?? 0)"
    }
    func setShowMore(isDetailViewShown: Bool) {
        stackView.isHidden = isDetailViewShown ? false : true
        redeemBtn.isHidden = isDetailViewShown ? false : true
        showMoreTop.constant = isDetailViewShown ? 370 : 110
        showMoreBtn.setTitle(isDetailViewShown ? "See less details" : "See more details", for: .normal)
    }
    
    // MARK: - Actions
    @IBAction func showMoreTapped(_ sender: Any) {
        showMore?()
    }
    
    @IBAction func redeemBtnTapped(_ sender: Any) {
        redeem?()
    }
}
