//
//  PointsViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

class PointsViewModel: NSObject {
    var pointsHistory: TZPointsHistory?
    var currentWeek: Current_week?
    var credit: [PointsModel]?
    var debit: [PointsModel]?
    var totalPoints: [PointsModel]?
    // MARK: - Web Services
    func tzPointsHistory(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.tzPointsHistory(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getNotification")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData["data"] as! NSDictionary
//                    self.pointsHistory = TZPointsHistory(dictionary: data)
                    UserDefaults.standard.GCoins = data.value(forKey: "current_week") as! [String:Any]
                    self.syncLocalAndRemoteData(dictionary: data as! [String : Any])
                } catch {
                    
                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func redeemPoints(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.redeemPoints(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint("redeemPoints")
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
//                let data = json["data"].dictionaryValue
                completion(true, JSON.init(), json["message"].stringValue)
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
//    func parseData() {
//        totalPoints = (pointsHistory?.current_week?.points ?? []) + (pointsHistory?.history ?? [])
//        credit = totalPoints?.filter({ ($0.tz_points_value) >= 0})
//        debit = totalPoints?.filter({ ($0.tz_points_value) < 0})
//    }
    
    // MARK: - Local Storage
    func syncLocalAndRemoteData(dictionary: [String:Any]) {
        guard let taskContext = appDelegate?.persistentContainer.newBackgroundContext() else {
            return
        }
        taskContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        taskContext.undoManager = nil
        
        taskContext.performAndWait {
            
            if let array = dictionary["history"] as? [[String:Any]] {
            
                let matchingEpisodeRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PointsModel")
                let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: matchingEpisodeRequest)
                batchDeleteRequest.resultType = .resultTypeObjectIDs
                
                do {
                    let batchDeleteResult = try taskContext.execute(batchDeleteRequest) as? NSBatchDeleteResult
                    
                    if let deletedObjectIDs = batchDeleteResult?.result as? [NSManagedObjectID] {
                        NSManagedObjectContext.mergeChanges(fromRemoteContextSave: [NSDeletedObjectsKey: deletedObjectIDs],
                                                            into: [appDelegate!.persistentContainer.viewContext])
                    }
                } catch {
                    print("Error: \(error)\nCould not batch delete existing records.")
                    return
                }
                
                for (i, dict) in array.enumerated() {
                    guard let scores = NSEntityDescription.insertNewObject(forEntityName: "PointsModel", into: taskContext) as? PointsModel else {
                       print("Error: Failed to create a new Film object!")
                       return
                    }
                    scores.update(with: dict)
                    scores.g_id = Int32(i)
                }
                
                if taskContext.hasChanges {
                    do {
                        try taskContext.save()
                    } catch {
                        print("Error: \(error)\nCould not save Core Data context.")
                    }
                    taskContext.reset() // Reset the context to clean up the cache and low the memory footprint.
                }
            }
        }
    }
    
    func fetchData(_ completion: @escaping () -> Void) {
        let persistentContainer = appDelegate?.persistentContainer
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PointsModel")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "g_id", ascending:true)]

        do {
            let results = try persistentContainer?.viewContext.fetch(fetchRequest)
            let scores = results as? [PointsModel]
            self.totalPoints = scores ?? []
            credit = totalPoints?.filter({ ($0.tz_points_value) >= 0})
            debit = totalPoints?.filter({ ($0.tz_points_value) < 0})
            currentWeek = Current_week(dictionary: UserDefaults.standard.GCoins as NSDictionary)
        } catch {
            print("error")
        }
        
        completion()
    }
}
