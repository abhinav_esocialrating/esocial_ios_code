//
//  FeedbackViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    let viewModel = FeedbackViewModel()
    // MARK :- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        viewModel.getFeedback(parameters: [:]) { (success, json, message) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    func configureView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100
        
        tableView.register(FeedbackFooterCell.self, forCellReuseIdentifier: TableViewCells.FeedbackFooterCell.rawValue)
    }
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    // MARK: - Web Services
    func submitTapped() {
        print(viewModel.userFeedback)
        let params = viewModel.createPostParams()
        if params.1 == "" {
            viewModel.postFeedback(parameters: params.0) { (success, json, message) in
                if success {
                    self.alertWithAction(message: message) {
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    self.alert(message: "Successfully submitted")
                }
            }
        } else {
            alert(message: params.1)
        }
    }
}
// MARK: - TableView methods
extension FeedbackViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.feedback.count > 0 ? (viewModel.feedback.count + 1) : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.FeedbackRatingCell.rawValue, for: indexPath) as! FeedbackRatingCell
            cell.label.text = viewModel.feedback[indexPath.row].feedback_question
            let id = viewModel.feedback[indexPath.row].feedback_question_id ?? 0
            if let rating = viewModel.userFeedback[id] as? Double {
                cell.ratingView.rating = rating
            } else {
                cell.ratingView.rating = 1
            }
            cell.ratingChanged = { rating in
                self.viewModel.userFeedback[id] = rating
            }
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.FeedbackFooterCell.rawValue, for: indexPath) as! FeedbackFooterCell
            cell.submitted = {
                self.submitTapped()
            }
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.FeedBackQuestionCell.rawValue, for: indexPath) as! FeedBackQuestionCell
            cell.feedback = viewModel.feedback[indexPath.row]
            let id = viewModel.feedback[indexPath.row].feedback_question_id ?? 0
            cell.setValues(text: viewModel.userFeedback[id] as? String)
            cell.textChanged = { [weak tableView] text in
                self.viewModel.userFeedback[id] = text
                DispatchQueue.main.async {
                    tableView?.beginUpdates()
                    tableView?.endUpdates()
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 4:
            return 120
        case 5:
            return 85
        default:
            return UITableView.automaticDimension
        }
    }
}
