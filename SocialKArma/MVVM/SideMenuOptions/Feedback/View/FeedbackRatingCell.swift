//
//  FeedbackRatingCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import FloatRatingView

class FeedbackRatingCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    
    var ratingChanged: ((_ rating: Double) -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    func configureView() {
        ratingView.delegate = self
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension FeedbackRatingCell: FloatRatingViewDelegate {
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        ratingChanged?(rating)
    }
}
