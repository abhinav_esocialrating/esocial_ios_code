//
//  FeedBackQuestionCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class FeedBackQuestionCell: UITableViewCell {
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var label: UILabel!
    
    var textChanged: ((_ text: String) -> Void)? = nil
    var feedback: FeedBackModel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    func configureView() {
        textView.delegate = self
        textView.setUpBorder(width: 0.5, color: UIColor.NewTheme.darkGray, radius: 0)
    }
    func setValues(text: String?) {
        label.text = feedback.feedback_question
    }
}
extension FeedBackQuestionCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        textChanged?(textView.text!.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
//        if textView.text! == " " {
//            textView.text = ""
//        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let maxLength = 300
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
}
