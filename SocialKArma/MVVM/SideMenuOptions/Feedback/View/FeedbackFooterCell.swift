//
//  FeedbackFooterCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 14/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class FeedbackFooterCell: UITableViewCell {
    
    var submitted: completionBlock? = nil

    private var submitButton : UIButton = {
        let btn = UIButton(type: .custom)
        btn.setTitleColor(UIColor.white, for: .normal)
        let str = Utilities.attributedText(text: "Submit", font: UIFont.fontWith(name: .Roboto, face: .Regular, size: 20), color: UIColor.white)
        btn.setAttributedTitle(str, for: .normal)
        btn.backgroundColor = UIColor.NewTheme.paleBlue
        btn.isUserInteractionEnabled = true
        return btn
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(submitButton)
        submitButton.anchor(top: nil, paddingTop: 0, bottom: nil, paddingBottom: 0, left: submitButton.superview?.leftAnchor, paddingLeft: 24, right: submitButton.superview?.rightAnchor, paddingRight: 24, width: 0, height: 50)
        submitButton.centerYAnchor.constraint(equalTo: submitButton.superview!.centerYAnchor).isActive = true
        submitButton.addTarget(self, action: #selector(submitTapped(_:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureView() {
//        selectionStyle = .none
    }
    @objc func submitTapped(_ sender: Any) {
        submitted?()
    }

}
