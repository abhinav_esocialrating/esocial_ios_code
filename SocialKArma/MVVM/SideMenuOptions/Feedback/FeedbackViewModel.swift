//
//  FeedbackViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 14/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class FeedbackViewModel: NSObject {
    var feedback = [FeedBackModel]()
    var userFeedback = [Int:Any]()
    // MARK: - Web Services
    func getFeedback(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getFeedback(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getFeedback")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                self.feedback = []
                let data = json["data"].arrayObject
                let feed = FeedBackModel.modelsFromDictionaryArray(array: (data as NSArray?) ?? [])
                self.feedback = feed
                self.createAnswerModel()
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func postFeedback(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.postFeedback(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("postFeedback")
            print(json)
            switch response.response?.statusCode {
            case 200:
                let message = json["message"].stringValue
                completion(true, JSON.init(), message)
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    // MARK: - Methods
    func createAnswerModel() {
        for (j,i) in feedback.enumerated() {
            userFeedback = [:]
            if j == 4 {
//                userFeedback[i.feedback_question_id ?? 0] = 1.0
            } else {
                userFeedback[i.feedback_question_id ?? 0] = " "
            }
        }
    }
    func createPostParams() -> ([String:Any], String) {
        var paramsDict = [String:Any]()
        var answerArray = [[String:Any]]()
        for i in userFeedback {
            if i.key == 5 {
                if let rating = i.value as? Double {
                    let dict = ["questionId": i.key, "answer": rating] as [String : Any]
                    answerArray.append(dict)
                }
            } else {
                if let answer = i.value as? String, answer != "" {
                    let dict = ["questionId": i.key, "answer": answer] as [String : Any]
                    answerArray.append(dict)
                }
            }
        }
        if answerArray.count == 0 {
            return (paramsDict, "Please answer at least one question!")
        } else {
            paramsDict["feedback"] = answerArray
            paramsDict["userId"] = UserDefaults.standard.userId
            paramsDict["deviceType"] = "ios"
            paramsDict["deviceName"] = UIDevice.current.modelName
            paramsDict["osVersion"] = UIDevice.current.systemVersion

            print(paramsDict)
            return (paramsDict, "")
        }
    }
}
