//
//  SideMenuCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
    
    @IBOutlet weak var menuImage: UIImageView!
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var badgeLabel: UILabel!
    
    @IBOutlet weak var badgeView: UIView!
    var menu: MenuModel? {
        didSet {
            setValues()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        badgeView.layer.cornerRadius = badgeView.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setValues() {
        menuImage.image = UIImage(named: menu?.image ?? "")
        labelTitle.text = menu?.title
        if menu?.notigications ?? 0 > 99 {
            badgeLabel.text = "99+"
            badgeView.isHidden = false
        } else if menu?.notigications ?? 0 == 0 {
            badgeLabel.text = ""
            badgeView.isHidden = true
        } else {
            badgeLabel.text = "\(menu?.notigications ?? 0)"
            badgeView.isHidden = false
        }
//        labelTitle.textColor = menu?.title == "Logout" ? .red : .black
    }
}
