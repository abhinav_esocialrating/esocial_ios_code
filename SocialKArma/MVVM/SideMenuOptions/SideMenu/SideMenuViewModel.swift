//
//  SideMenuViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SideMenuViewModel: NSObject {
    var menu = [
//        MenuModel(title: "Self Review", image: "Panel-Invite-all", notigications: 0),
        MenuModel(title: "Invite", image: "Panel-Invite-all", notigications: 0),
        MenuModel(title: "Resync Contacts", image: "sync", notigications: 0),
//        MenuModel(title: "Karma Points", image: "Panel-Karma-Points", notigications: 101),
//        MenuModel(title: "Messages", image: "Panel-Message", notigications: 5),
        MenuModel(title: "Notifications", image: "Panel-Notification", notigications: 0),
        MenuModel(title: "FAQs", image: "Panel-FAQ", notigications: 0),
//        MenuModel(title: "Settings", image: "Panel-Setting", notigications: 0),
//        MenuModel(title: "Terms & Conditions", image: "Panel-Terms-and-Condition", notigications: 0),
        MenuModel(title: "Privacy Policy", image: "Panel--Privacy-Policy", notigications: 0),
        MenuModel(title: "Tell us more", image: "feedback", notigications: 0),
        MenuModel(title: "Contact Us", image: "contact_us", notigications: 0),
//        MenuModel(title: "Contest", image: "trophy", notigications: 0),
        MenuModel(title: "Logout", image: "login", notigications: 0)
    ]
    
    func logout(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.logout(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
