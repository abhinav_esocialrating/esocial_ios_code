//
//  MenuModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation

struct MenuModel {
    var title: String
    var image: String
    var notigications: Int
}
