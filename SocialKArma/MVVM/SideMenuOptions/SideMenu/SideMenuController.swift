//
//  SideMenuController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import FAPanels
import SwiftyJSON

class SideMenuController: UIViewController {

    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var versionLabel: UILabel!
    // MARK: - Variables
    let viewModel = SideMenuViewModel()
    let imagePicker = ImagePicker()
    // MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(profilePicChanged), name: Notification.Name.AppNotifications.ProfilePicChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(nameChanged), name: Notification.Name.AppNotifications.NameChanged, object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureView()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureView() {
        userImg.setUpBorder(width: 3, color: darkBlue, radius: userImg.frame.width/2)
        handleUserInfo()
        if let str = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
            versionLabel.text = "Version \(str)"
        }
    }
    func handleUserInfo() {
        let userInfo = UserDefaults.standard.userInfo
        let jhdbj = userInfo.data(using: .utf8)!
        do {
            let json = try JSON(data: jhdbj)
            self.updateUI(json: json.dictionaryValue)
        } catch {
        }
    }
    func updateUI(json: [String:JSON]) {
        if let url = json["image_id"]?.string {
            self.userImg.sd_setImage(with: URL(string: url)!, completed: nil)
        } else {
            self.userImg.image = UIImage(named: "avatar-2")
        }
        if let name = json["name"]?.string {
            self.nameLabel.text = name
        } else {
            self.nameLabel.text = UserDefaults.standard.userName
        }
        phoneLabel.text = UserDefaults.standard.userPhoneNumber
    }
    // MARK: - Actions
    @IBAction func profilePicTapped(_ sender: Any) {
        if let cont = appDelegate?.window?.rootViewController as? FAPanelController {
            if let tbc = cont.center as? TabBarController {
                tbc.selectedIndex = 3//(tbc.tabBar.items?.count ?? 3) - 1
                cont.closeLeft()
            }
        }
    }
    @objc func profilePicChanged() {
        handleUserInfo()
    }
    @objc func nameChanged() {
        handleUserInfo()
    }
    @IBAction func cameraTapped(_ sender: Any) {
        imagePicker.delegate = self
        let alert = imagePicker.pickImage(sourceController: self, path: "")
        self.present(alert, animated: true, completion: nil)
    }
    
    func openChat(panel: FAPanelController) {
        if let tbc = panel.center as? TabBarController {
            tbc.selectedIndex = 2
            panel.closeLeft()
        }
//        let vc = ChannelListViewController.instantiate(fromAppStoryboard: .Chat)
//        panel.closeLeft()
//        vc.modalPresentationStyle = .fullScreen
//        panel.center?.present(vc, animated: true, completion: nil)
    }
//    func openTermsPage(type: Int) {
//        let vc = PolicyViewController.instantiate(fromAppStoryboard: .Main)
//        vc.modalPresentationStyle = .fullScreen
//        vc.type = type
//        present(vc, animated: true, completion: nil)
//    }
    func openVC<U : UIViewController>(viewControllerClass : U.Type) {
        let vc = U.instantiate(fromAppStoryboard: .Main)
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
}
// MARK: - TableView Methods
extension SideMenuController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.menu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.SideMenuCell.rawValue, for: indexPath) as! SideMenuCell
        cell.menu = viewModel.menu[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cont = appDelegate?.window?.rootViewController as? FAPanelController {
            switch indexPath.row {
            case 0:
                self.openShare()
            case 1:
                self.resyncContact(type: 2)
            case 2:
                let vc = NotificationsViewController.instantiate(fromAppStoryboard: .Main)
                cont.closeLeft()
                vc.modalPresentationStyle = .fullScreen
                cont.center?.present(vc, animated: true, completion: nil)
            case 3:
                //Opening FAQs!
                cont.closeLeft()
                openTermsPage(type: 3)
            case 4:
                //Opening T&C!
                cont.closeLeft()
                openTermsPage(type: 2)
            case 5:
                cont.closeLeft()
                openVC(viewControllerClass: FeedbackViewController.self)
            case 6:
                cont.closeLeft()
                openVC(viewControllerClass: ContactUsViewController.self)
            case 7:
                alertWithTwoAction(message: kAlertLogout, okHandler: {
                    DispatchQueue.main.async {
                        appDelegate?.setUpLoginRootView()
                    }
                    UserDefaults.standard.clearDataOnLogout()
                }, cancelHandler: {})
//            case 7:
//                cont.closeLeft()
//                openTermsPage(type: 4)
            default:
                print(viewModel.menu[indexPath.row].title)
            }
        }
    }
    
}
// MARK: - ImagePicker methods
extension SideMenuController: ImagePickerUploaderDelegate {
    func PickerSuccess(withImage: UIImage) {
        userImg.image = withImage
        imagePicker.uploadProfilePic(image: withImage) { (success, json, message) in
            if success {
                let url = json["data"].stringValue
                self.userImg.sd_setImage(with: URL(string: url)!, placeholderImage: UIImage(named: "avatar-2"), options: [], completed: nil)
                Utilities().updateProfilePicUrl(url: url)
            } else {
                self.alert(message: message)
            }
        }
    }
    func PickerFailed(withError: String) {
    }
    func profilePicRemoved(url: String!) {
        self.userImg.sd_setImage(with: URL(string: url)!, placeholderImage: UIImage(named: "avatar-2"), options: [], completed: nil)
        Utilities().updateProfilePicUrl(url: url)
    }
}
// MARK: - SendData Delegates

/*  Logout API Call
self.showHud()
viewModel.logout(parameters: [:]) { (success, json, message) in
    self.hideHud()
    if success {
        DispatchQueue.main.async {
            appDelegate?.setUpLoginRootView()
        }
    } else {
        self.alert(message: message)
    }
}
*/
/*
 if let tbc = cont.center as? TabBarController {
     tbc.selectedIndex = 3
     cont.closeLeft()
 } else {
     let vc = TabBarController.instantiate(fromAppStoryboard: .Home)
     cont.center(vc)
     vc.selectedIndex = 2
     cont.closeLeft()
 }
 */
