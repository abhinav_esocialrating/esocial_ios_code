//
//  ContactUsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 14/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // MARK: - Actions
    
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func saveTapped(_ sender: Any) {
        showHud("")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.hideHud()
            self.alertWithAction(message: "Your issues has been recorded") {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
