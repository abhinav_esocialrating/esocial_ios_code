//
//  ReportUserViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 02/03/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ReportUserViewModel: NSObject {
    var otherUserId = ""
    func reportUser(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
                
        NetworkManager.reportUser(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("reportUser")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"]
                var reportedUsers = UserDefaults.standard.reportedUsers
                if !reportedUsers.contains(self.otherUserId) {
                    reportedUsers.append(self.otherUserId)
                }
                UserDefaults.standard.reportedUsers = reportedUsers
                completion(true, data, json["message"].stringValue)
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }

}
