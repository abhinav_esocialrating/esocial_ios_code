//
//  ReportUserViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 02/03/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ReportUserViewController: UIViewController {
    
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var popUpView: UIView!
    var notify: completionBlock? = nil
    var viewModel = ReportUserViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        addTap()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureView()
    }
    func configureView() {
        textView.setUpBorder(width: 0.75, color: UIColor.NewTheme.darkGray, radius: 0)
        submitBtn.layer.cornerRadius = 8
    }
    // MARK: - Navigation
    
    @IBAction func backTapped(_ sender: Any) {
        if textView.text != "" {
            showHud("")
            let params = ["reportedUserId": viewModel.otherUserId, "query": textView.text!] as [String : Any]
            viewModel.reportUser(parameters: params) { (success, json, message) in
                if success {
                    DispatchQueue.main.async {
                        self.hideHud()
                        self.notify?()
                        self.dismiss(animated: false, completion: nil)
                    }
                } else {
                    self.alert(message: message)
                }
            }
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                self.hideHud()
//                self.notify?()
//                self.dismiss(animated: false, completion: nil)
//            }
        } else {
            alert(message: "Please describe your issue!")
        }
    }
    
    // MARK: - Methods
    func addTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
//        tap.delegate = self
        view.addGestureRecognizer(tap)
    }
    @objc func viewTapped(_ sender: UITapGestureRecognizer) {
        self.notify?()
        self.dismiss(animated: false, completion: nil)
    }
}
// MARK: - Tap gesture
extension ReportUserViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let tappedView = gestureRecognizer.view {
            if tappedView.tag == 22 {
                return true
            }
        }
        return false
    }
}
