//
//  PhotoViewerViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 22/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class PhotoViewerViewController: UIViewController {
    
    // MARK: - View Properties
    var navView: UIView!
    var scrollView: UIScrollView!
    var imageView: UIImageView!
    var backButton: UIButton!
    var titleLabel: UILabel!
    
    // MARK: - Variables
    var imageUrl: String?
    var backAction: (() -> Void)? = nil // For Notifying presenting viewcontroller (Optional)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        setValues()
    }
    
    func configureView() {
        view.backgroundColor = .black
        
        navView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        view.addSubview(navView)
        navView.anchor(top: navView.superview?.topAnchor, paddingTop: UIDevice.current.hasNotch ? 44 : 20, bottom: nil, paddingBottom: 0, left: navView.superview?.leftAnchor, paddingLeft: 0, right: navView.superview?.rightAnchor, paddingRight: 0, width: 0, height: 44)
        
        backButton = UIButton(type: .custom)
        backButton.frame = CGRect(x: 16, y: 12, width: 20, height: 20)
        navView.addSubview(backButton)
        backButton.setImage(UIImage(named: "Back-Arrow-icon-White"), for: .normal)
        backButton.tintColor = UIColor.NewTheme.background
        backButton.addTarget(self, action: #selector(backTapped(_:)), for: .touchUpInside)
        backButton.anchor(top: backButton.superview?.topAnchor, paddingTop: 12, bottom: nil, paddingBottom: 0, left: navView.superview?.leftAnchor, paddingLeft: 16, right: nil, paddingRight: 0, width: 20, height: 20)
        
        scrollView = UIScrollView(frame: view.frame)
        view.addSubview(scrollView)
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 2.0
        scrollView.zoomScale = 1.0
        scrollView.delegate = self
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false

        let imageViewHeight = UIDevice.current.hasNotch ? 88 : 64
        scrollView.anchor(top: scrollView.superview?.topAnchor, paddingTop: CGFloat(imageViewHeight), bottom: scrollView.superview?.bottomAnchor, paddingBottom: 0, left: scrollView.superview?.leftAnchor, paddingLeft: 0, right: scrollView.superview?.rightAnchor, paddingRight: 0, width: 0, height: 0)
        
        imageView = UIImageView()
        scrollView.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        imageView.anchor(top: imageView.superview?.topAnchor, paddingTop: 0, bottom: imageView.superview?.bottomAnchor, paddingBottom: 0, left: imageView.superview?.leftAnchor, paddingLeft: 0, right: imageView.superview?.rightAnchor, paddingRight: 0, width: 0, height: view.frame.height - CGFloat(imageViewHeight))
        let imageViewHeightConstraint = NSLayoutConstraint(item: imageView!, attribute: .height, relatedBy: .equal, toItem: imageView.superview?.superview, attribute: .height, multiplier: 1, constant: -CGFloat(imageViewHeight))
        let imageViewWidthConstraint = NSLayoutConstraint(item: imageView!, attribute: .width, relatedBy: .equal, toItem: imageView.superview?.superview, attribute: .width, multiplier: 1, constant: 0)
        
        view.addConstraints([imageViewHeightConstraint, imageViewWidthConstraint])
        NSLayoutConstraint.activate([imageViewHeightConstraint, imageViewWidthConstraint])
    }
    func setValues() {
        if let url = imageUrl, imageUrl != "" {
            imageView?.sd_setImage(with: URL(string: url)!, completed: nil)
        } else {
            imageView.image = UIImage(named: "avatar-2")
        }
    }
    // MARK: - Actions
    @objc func backTapped(_ sender: Any) {
        backAction?()
        self.dismiss(animated: false, completion: nil)
    }
}
// MARK: - ScrollView Delegate
extension PhotoViewerViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        print("scrollViewDidZoom")
    }
}
