//
//  ViewController.swift
//  MDatePickerViewDemo
//
//  Created by Matt on 2020/1/13.
//  Copyright © 2020 Matt. All rights reserved.
//

import UIKit
import MDatePickerView

class CalendarViewController: UIViewController {
    
    var sendData: SendData!
    
    lazy var MDate : MDatePickerView = {
        let mdate = MDatePickerView()
        mdate.delegate = self
        mdate.Color = UIColor.NewTheme.lightBlue
        mdate.cornerRadius = 14
        mdate.translatesAutoresizingMaskIntoConstraints = false
        mdate.from = 1922
        mdate.to = 2004
        return mdate
    }()
    
    let Today : UIButton = {
        let but = UIButton(type:.system)
        but.setAttributedTitle(Utilities.attributedText(text: "Done", font: .fontWith(name: .Roboto, face: .Medium, size: 22), color: .white), for: .normal)
        but.addTarget(self, action: #selector(today), for: .touchUpInside)
        but.translatesAutoresizingMaskIntoConstraints = false
        return but
    }()
    
    let Label : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 32)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    @objc func today() {
        if Label.text!.count == 0 {
            sendData.passData(dataPasser: self, data: "")
        } else {
            sendData.passData(dataPasser: self, data: Label.text!)
        }
        dismiss(animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        view.addSubview(MDate)
        NSLayoutConstraint.activate([
            MDate.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -30),
            MDate.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
            MDate.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7),
            MDate.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.4)
        ])
        
        view.addSubview(Today)
        Today.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        Today.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
        
        view.addSubview(Label)
        Label.topAnchor.constraint(equalTo: MDate.bottomAnchor, constant: 30).isActive = true
        Label.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        Label.text = ""
    }
    
    
}

extension CalendarViewController : MDatePickerViewDelegate {
    func mdatePickerView(selectDate: Date) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.string(from: selectDate)
        var arr = date.components(separatedBy: "-")
        if var year = arr.first {
            year = Int(year)! > 2004 ? "2004" : year
            arr[0] = year
            let finalDate = arr.joined(separator: "-")
            Label.text = finalDate
        } else {
            Label.text = date
        }
    }
}
