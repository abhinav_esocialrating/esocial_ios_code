//
//  EditInfoViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 03/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import DropDown
import SwiftyJSON

class EditInfoViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var textfield1: ACFloatingTextfield!
    @IBOutlet weak var textfield2: ACFloatingTextfield!
    @IBOutlet weak var textfield3: ACFloatingTextfield!
    @IBOutlet weak var textfield4: ACFloatingTextfield!
    @IBOutlet weak var textfield5: ACFloatingTextfield!
    @IBOutlet weak var textfield6: ACFloatingTextfield!
    @IBOutlet weak var textfield7: ACFloatingTextfield!
    @IBOutlet weak var textfield8: ACFloatingTextfield!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var imgView2: UIImageView!
    @IBOutlet weak var imgView3: UIImageView!
    @IBOutlet weak var imgView4: UIImageView!
    @IBOutlet weak var imgView5: UIImageView!
    @IBOutlet weak var imgView6: UIImageView!
    @IBOutlet weak var imgView7: UIImageView!
    @IBOutlet weak var imgView8: UIImageView!
    
    @IBOutlet weak var stackView4: UIStackView!
    
    @IBOutlet weak var userImgContainer: UIView!
    @IBOutlet weak var userImg: UIImageView!

    // MARK: - Variables
    let dropDown = DropDown()
    let viewModel = EditInfoViewModel()
    let imagePicker = ImagePicker()
    var imageUrl = ""
    var screenType = 0  // 1: Personal info   2: About me/Basic Details
    var userType = 1  // 1: Self   2: Other User

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tabBarController?.tabBar.isHidden = true
        configureView()
        getAboutMeInfo()
//        viewModel.loadLocalData { (success) in
//            self.setInitialValues()
//        }
    }
    
    func configureView() {
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.lightGray, opacity: 0.6, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)

        if screenType == 2 && userType == 1 {
            userImg.rounded()
            userImg.layer.borderColor = UIColor.SearchColors.blue.cgColor
            userImg.layer.borderWidth = 2
            
            let userInfo = UserDefaults.standard.userInfo
            let jhdbj = userInfo.data(using: .utf8)!
            do {
                let json = try JSON(data: jhdbj)
                if let url = json["image_id"].string {
                    self.userImg.sd_setImage(with: URL(string: url)!, completed: nil)
                    self.imageUrl = url
                } else {
                    self.userImg.image = UIImage(named: "avatar-2")
                }
            } catch {
            }
        } else {
            userImgContainer.isHidden = true
        }

        pageTitle.text = screenType == 1 ? "More insights about You" : "All About You!"
        // Textfields
        for i in [textfield1, textfield2, textfield3, textfield4, textfield5, textfield6, textfield7, textfield8] {
            i?.lineColor = .gray
            i?.placeHolderColor = .gray
            i?.selectedLineColor = .gray
            i?.selectedPlaceHolderColor = .gray
            i?.delegate = self
            i?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 16)
            i?.textColor = UIColor.NewTheme.darkGray
        }
        // Placeholders
        switch screenType {
        case 1:
            textfield1.placeholder = viewModel.personalInfoPlaceHolders[0]
            textfield2.placeholder = viewModel.personalInfoPlaceHolders[1]
            textfield3.placeholder = viewModel.personalInfoPlaceHolders[2]
            textfield4.placeholder = viewModel.personalInfoPlaceHolders[3]
            textfield5.placeholder = viewModel.personalInfoPlaceHolders[4]
            textfield6.placeholder = viewModel.personalInfoPlaceHolders[5]
            textfield7.placeholder = viewModel.personalInfoPlaceHolders[6]
            locationBtn.isHidden = true
            textfield8.isHidden = true
            imgView1.image = UIImage(named: viewModel.personalInfoImages[0])
            imgView2.image = UIImage(named: viewModel.personalInfoImages[1])
            imgView3.image = UIImage(named: viewModel.personalInfoImages[2])
            imgView4.image = UIImage(named: viewModel.personalInfoImages[3])
            imgView5.image = UIImage(named: viewModel.personalInfoImages[4])
            imgView6.image = UIImage(named: viewModel.personalInfoImages[5])
            imgView7.image = UIImage(named: viewModel.personalInfoImages[6])
            imgView8.isHidden = true
        default:
            textfield1.placeholder = viewModel.aboutMePlaceHolders[0]
            textfield2.placeholder = viewModel.aboutMePlaceHolders[1]
            textfield3.placeholder = viewModel.aboutMePlaceHolders[2]
            textfield4.placeholder = viewModel.aboutMePlaceHolders[3]
            textfield5.placeholder = viewModel.aboutMePlaceHolders[4]
            textfield8.placeholder = viewModel.aboutMePlaceHolders[5]
            stackView4.isHidden = true
            textfield6.isHidden = true
            textfield7.isHidden = true
            imgView1.image = UIImage(named: viewModel.aboutMeImages[0])
            imgView2.image = UIImage(named: viewModel.aboutMeImages[1])
            imgView3.image = UIImage(named: viewModel.aboutMeImages[2])
            imgView4.image = UIImage(named: viewModel.aboutMeImages[3])
            imgView5.image = UIImage(named: viewModel.aboutMeImages[4])
            imgView8.image = UIImage(named: viewModel.aboutMeImages[5])
            imgView1.applyTemplate(image: UIImage(named: viewModel.aboutMeImages[0]), color: UIColor.SearchColors.blue)
        }
        // InputViews
        if screenType == 2 {
//            textfield1.inputView = UIView()  // Gender
            setDatePicker(textField: textfield2)  // date picker
//            textfield4.inputView = UIView()  // Relationship status
            textfield3.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
        }

    }

    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
//        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if RechabilityHandler.shared.reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            if isValid() {
                saveAboutMeInfo()
            }
        }
    }
    @objc func textFieldChanged(_ textField: UITextField) {
        if textField.text!.count > 2 {
            LocationManager.shared.getCities(query: textField.text!) { (cities, cityNames) in
                self.viewModel.cities = cities
                if cities.count > 0 {
                    self.showDropDown(textField: textField)
                }
            }
        }
    }
    @IBAction func locationBtnTapped(_ sender: Any) {
        guard let location = LocationManager.shared.lastLocation else {
            return
        }
        LocationManager.shared.getAdressName(coords: location) { (city) in
            self.textfield3.text = city
            self.viewModel.dataSource["location_city"] = JSON(city)
        }
    }
    @IBAction func editTapped(_ sender: Any) {
        imagePicker.delegate = self
        let alert = imagePicker.pickImage(sourceController: self, path: "")
        present(alert, animated: true, completion: nil)
    }
    // MARK: - Utility methods
    func setInitialValues() {
        var value = ""
        if screenType == 2 {
            textfield1.text = viewModel.dataSource["gender"]?.stringValue
            textfield2.text = viewModel.dataSource["dob"]?.stringValue
            textfield3.text = viewModel.dataSource["location_city"]?.stringValue
            value = ConfigDataModel.shared.fetchValueFrom(id: viewModel.dataSource["relationship_status_id"]?.intValue ?? 0, array: ConfigDataModel.shared.relationship_status)
            textfield4.text = value
            textfield5.text = viewModel.dataSource["email_id"]?.stringValue
            textfield8.text = viewModel.dataSource["name"]?.stringValue
        } else {
            textfield1.text = viewModel.dataSource["height"]?.stringValue == "0" ? "-" : viewModel.dataSource["height"]?.stringValue
            textfield2.text = viewModel.dataSource["weight"]?.stringValue == "0" ? "-" : viewModel.dataSource["weight"]?.stringValue
            value = ConfigDataModel.shared.fetchValueFrom(id: viewModel.dataSource["fitness_id"]?.intValue ?? 0, array: ConfigDataModel.shared.fitness)
            textfield3.text = value
            value = ConfigDataModel.shared.fetchValueFrom(id: viewModel.dataSource["drinking_id"]?.intValue ?? 0, array: ConfigDataModel.shared.habitFrequency)
            textfield4.text = value
            value = ConfigDataModel.shared.fetchValueFrom(id: viewModel.dataSource["smoking_id"]?.intValue ?? 0, array: ConfigDataModel.shared.habitFrequency)
            textfield5.text = value
            value = ConfigDataModel.shared.fetchValueFrom(id: viewModel.dataSource["religion_id"]?.intValue ?? 0, array: ConfigDataModel.shared.religion)
            textfield6.text = value
            value = ConfigDataModel.shared.fetchValueFrom(id: viewModel.dataSource["political_view_id"]?.intValue ?? 0, array: ConfigDataModel.shared.politicalView)
            textfield7.text = value
//            textfield5.text = viewModel.dataSource["blood_group"]?.stringValue
        }
    }
    func isValid() -> Bool {
        var valid = true
        var message = ""
        if screenType == 2 {
            if textfield8.text!.count < 2 || textfield8.text!.count > 30 {
                valid = false
                message = "Name length should be between 2 and 30"
                textfield8.showErrorWithText(errorText: message)
            } else if textfield5.text!.count == 0 {
            } else if !Utilities.isValidEmail(textfield5.text!) {
                valid = false
                message = "Please enter a valid email"
                textfield5.showErrorWithText(errorText: message)
            }
        }
        return valid
    }

    func openPickerView(textField: UITextField) {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        textField.inputView = pickerView
        if textField == textfield1 {
            pickerView.selectRow(60, inComponent: 0, animated: false)
        } else {
            pickerView.selectRow(30, inComponent: 0, animated: false)
        }
    }
    // MARK: - Web Services
    func getAboutMeInfo() {
//        showHudToWindow()
        let params = ["id": UserDefaults.standard.userId]
        viewModel.getAboutMeInfo(parameters: params) { (success, json, message) in
//            self.hideHud()
            if success {
                self.setInitialValues()
            } else {
                // show message
            }
        }
    }
    func saveAboutMeInfo() {
        var params = [String:Any]()
        if screenType == 2 {
            if textfield8.text!.count > 0 {
                params["name"] = textfield8.text!
                viewModel.dataSource["name"] = JSON(textfield8.text!)
            } else {
                params.removeValue(forKey: "name")
            }
            if textfield1.text!.count > 0 {
                params["gender"] = textfield1.text!
            } else {
               params.removeValue(forKey: "gender")
            }
            if textfield2.text!.count > 0 {
                params["dob"] = textfield2.text!
            } else {
               params.removeValue(forKey: "dob")
            }
            if let id = viewModel.dataSource["location_city"]?.stringValue, id != "" {
                params["location_city"] = "\(id)"
            } else {
                params.removeValue(forKey: "location_city")
            }
            if let id = viewModel.dataSource["relationship_status_id"]?.intValue, id != 0 {
                params["relationship_status_id"] = "\(id)"
            } else {
                params.removeValue(forKey: "relationship_status_id")
            }
            if textfield5.text!.count > 0 {
                params["email_id"] = textfield5.text!
            } else {
                params.removeValue(forKey: "email_id")
            }
        } else {
            if textfield1.text!.count > 0 {
                params["height"] = textfield1.text!
            } else {
               params.removeValue(forKey: "height")
            }
            if textfield2.text!.count > 0 {
                params["weight"] = textfield2.text!
            } else {
               params.removeValue(forKey: "weight")
            }
            if let id = viewModel.dataSource["fitness_id"]?.intValue, id != 0 {
                params["fitness_id"] = "\(id)"
            } else {
                params.removeValue(forKey: "fitness_id")
            }
            if let id = viewModel.dataSource["drinking_id"]?.intValue, id != 0 {
                params["drinking_id"] = "\(id)"
            } else {
                params.removeValue(forKey: "drinking_id")
            }
            if let id = viewModel.dataSource["smoking_id"]?.intValue, id != 0 {
                params["smoking_id"] = "\(id)"
            } else {
                params.removeValue(forKey: "smoking_id")
            }
            if let id = viewModel.dataSource["religion_id"]?.intValue, id != 0 {
                params["religion_id"] = "\(id)"
            } else {
                params.removeValue(forKey: "religion_id")
            }
            if let id = viewModel.dataSource["political_view_id"]?.intValue, id != 0 {
                params["political_view_id"] = "\(id)"
            } else {
                params.removeValue(forKey: "political_view_id")
            }
        }
        print(params)
        if params.keys.count > 0 {
            self.showHud()
            viewModel.saveAboutMeInfo(parameters: params) { (success, json, message) in
                self.hideHud()
                if success {
                    self.navigationController?.popViewController(animated: true)
//                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.alert(message: message)
                }
            }
        }
    }
}
// MARK: - TextField delegates
extension EditInfoViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch screenType {
        case 1:
            if textField == textfield3 || textField == textfield4 || textField == textfield5 || textField == textfield6 || textField == textfield7 {
                view.endEditing(true)
                showDropDown(textField: textField)
                return false
            } else {
                openPickerView(textField: textField)
            }
        default:
            if textField == textfield1 || textField == textfield4 {
                showDropDown(textField: textField)
                return false
            } else if textField == textfield2 {
                openCalendar()
                return false
            }
        }
        return true
    }
}
// MARK: - Dropdown
extension EditInfoViewController {
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = dropDown.anchorView?.plainView.bounds.width
        switch screenType {
        case 1:
            switch textField {
            case textfield3:
                dropDown.dataSource = ConfigDataModel.shared.fitness.map({ (status) -> String in
                    return (status["value"]?.stringValue ?? "")
                })
            case textfield4:
                dropDown.dataSource = ConfigDataModel.shared.habitFrequency.map({ (status) -> String in
                    return (status["value"]?.stringValue ?? "")
                })
//            case textfield5:
//                dropDown.dataSource = CommonData.bloodGroup
            case textfield5:
                dropDown.dataSource = ConfigDataModel.shared.habitFrequency.map({ (status) -> String in
                    return (status["value"]?.stringValue ?? "")
                })
            case textfield6:
                dropDown.dataSource = ConfigDataModel.shared.religion.map({ (status) -> String in
                    return (status["value"]?.stringValue ?? "")
                })
            default:
                dropDown.dataSource = ConfigDataModel.shared.politicalView.map({ (status) -> String in
                    return (status["value"]?.stringValue ?? "")
                })
            }
        default:
            switch textField {
            case textfield1:
                dropDown.dataSource = CommonData.gender
            case textfield3:
                dropDown.dataSource = viewModel.cities
            default:
                dropDown.dataSource = ConfigDataModel.shared.relationship_status.map({ (status) -> String in
                    return (status["value"]?.stringValue ?? "")
                })
            }
        }
        dropDown.selectionAction = { (index: Int, item: String) in
            textField.text = item
            textField.resignFirstResponder()
            switch self.screenType {
            case 2:
                switch textField {
                case self.textfield4:
                    self.viewModel.dataSource["relationship_status_id"] = ConfigDataModel.shared.relationship_status[index]["id"]
                case self.textfield3:
                    self.viewModel.dataSource["location_city"] = JSON(item)
                default:
                    print("do nothing")
                }
            default:
                switch textField {
                case self.textfield3:
                    self.viewModel.dataSource["fitness_id"] = ConfigDataModel.shared.fitness[index]["id"]
                case self.textfield4:
                    self.viewModel.dataSource["drinking_id"] = ConfigDataModel.shared.habitFrequency[index]["id"]
                case self.textfield5:
                    self.viewModel.dataSource["smoking_id"] = ConfigDataModel.shared.habitFrequency[index]["id"]
                case self.textfield6:
                    self.viewModel.dataSource["religion_id"] = ConfigDataModel.shared.religion[index]["id"]
                case self.textfield7:
                    self.viewModel.dataSource["political_view_id"] = ConfigDataModel.shared.politicalView[index]["id"]
                default:
                    print("do nothing")
                }
            }
        }
        dropDown.show()
    }
    
    
}
// MARK: - Calendar
extension EditInfoViewController {
    func setDatePicker(textField: UITextField) {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        textField.inputView = datePicker
        // Setting current date
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        components.year = -16
        let maxDate = calendar.date(byAdding: components, to: currentDate)!
        components.year = -100
        let minDate = calendar.date(byAdding: components, to: currentDate)!
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        
        datePicker.addTarget(self, action: #selector(datePicked(_:)), for: .valueChanged)
    }
    @objc func datePicked(_ datePicker: UIDatePicker) {
        let date = datePicker.date
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = .current
        textfield2.text = formatter.string(from: date)
    }
    func openCalendar() {
        let vc = CalendarViewController()
        vc.sendData = self
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false, completion: nil)
    }
}
// MARK: - UIPickerView
extension EditInfoViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return textfield1.isEditing ? CommonData.height.count : CommonData.weight.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return textfield1.isEditing ? "\(CommonData.height[row])" : "\(CommonData.weight[row])"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if textfield1.isEditing {
            textfield1.text = "\(CommonData.height[row])"
        } else {
            textfield2.text = "\(CommonData.weight[row])"
        }
    }
}
extension EditInfoViewController: SendData {
    func passData(dataPasser: Any, data: Any) {
        if dataPasser is CalendarViewController {
            if let date = data as? String, date != "" {
                textfield2.text = date
            }
        }
    }
}
// MARK: - ImagePicker methods
extension EditInfoViewController: ImagePickerUploaderDelegate {
    func PickerSuccess(withImage: UIImage) {
//        presentCropViewController(image: withImage)
        imagePicker.uploadProfilePic(image: withImage) { (success, json, message) in
            if success {
                let url = json["data"].stringValue
                self.userImg.sd_setImage(with: URL(string: url)!, placeholderImage: UIImage(named: "avatar-2"), options: [], completed: nil)
                Utilities().updateProfilePicUrl(url: url)
                self.imageUrl = url
            } else {
                self.alert(message: message)
            }
        }
    }
    func PickerFailed(withError: String) {
    }
    func profilePicRemoved(url: String!) {
        self.userImg.sd_setImage(with: URL(string: url)!, placeholderImage: UIImage(named: "avatar-2"), options: [], completed: nil)
        Utilities().updateProfilePicUrl(url: url)
    }
}
