//
//  EditInfoViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 03/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class EditInfoViewModel: NSObject {
    let personalInfoPlaceHolders = ["How tall do you stand?","Let's share your weight!", "Fitness Level", "Do You Smoke?" ,"Do You Drink?","Your Religious Belief","Your Political Belief"]
    let aboutMePlaceHolders = ["Gender","Date of Birth","City","Relationship Status","Email ID", "Your Name"]
    let personalInfoImages = ["noun_height","noun_Weight", "noun_fitness", "noun_mug" ,"noun_Smoking","noun_Church","noun_Microphone"]
    let aboutMeImages = ["noun_gender", "noun_Birthday", "noun_Location","noun_Birthday" , "noun_Email", "edit_name"]

    var dataSource = [String:JSON]()
    var cities = [String]()
    
    func createDataSource(json: [String:JSON]) {
        // About me
        dataSource["gender"] = json["gender"]
        dataSource["dob"] = json["dob"]
        dataSource["location_city"] = json["location_city"]
        dataSource["relationship_status_id"] = json["relationship_status_id"]
        dataSource["email_id"] = json["email_id"]
        dataSource["name"] = json["name"]
        // Personal info
        dataSource["height"] = json["height"]
        dataSource["weight"] = json["weight"]
        dataSource["fitness_id"] = json["fitness_id"]
        dataSource["drinking_id"] = json["drinking_id"]
        dataSource["smoking_id"] = json["smoking_id"]
        dataSource["religion_id"] = json["religion_id"]
        dataSource["political_view_id"] = json["political_view_id"]
        dataSource["blood_group"] = json["blood_group"]
    }
    
    func loadLocalData(completion: (_ success: Bool) -> Void) {
        let p = UserDefaults.standard.AboutMe
        if p != "" {
            if let json = p.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                do {
                    let json = try JSON(data: json)
                    let list = json.dictionaryValue
//                    self.dataSource = list
                    createDataSource(json: list)
                    completion(true)
                } catch {
                    completion(false)
                }
            } else {
                completion(false)
            }
        } else {
            completion(false)
        }
    }
    func saveJsonToUserDefaults(json: JSON) {
        UserDefaults.standard.AboutMe = json.rawString() ?? ""
        Utilities.init().updateUserName(name: dataSource["name"]?.stringValue ?? "")
    }
    func saveAboutMeInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.saveAboutMeInfo(parameters: parameters, method: .put) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                self.saveJsonToUserDefaults(json: json["data"])
                let data = json["data"].dictionaryValue
                if let points = data["tzPoints"]?.int {
                    PointsHandler.shared.updateTZPoints(points: points, changedToBeShown: true)
                }
                completion(true, json, "")
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func getAboutMeInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getAboutMeInfo(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let list = json["data"].dictionaryValue
                self.createDataSource(json: list)
                self.saveJsonToUserDefaults(json: json["data"])
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }

}
