//
//  EditBioViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 03/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class EditBioViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var contentView: UIView!
    
    var viewModel = EditBioViewModel()
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tabBarController?.tabBar.isHidden = true
        textView.text = viewModel.bio.decodeEmoji
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.lightGray, opacity: 0.6, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
    }
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
//        dismiss(animated: true, completion: nil)
    }
    @IBAction func saveTapped(_ sender: Any) {
        let params = ["bio": viewModel.bio.encodeEmoji]
        self.showHud()
        viewModel.saveAboutMeInfo(parameters: params) { (success, json, message) in
            self.hideHud()
            if success {
                DispatchQueue.main.async {
                    self.view.endEditing(true)
                    self.navigationController?.popViewController(animated: true)
//                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}
extension EditBioViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        viewModel.bio = textView.text!
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let maxLength = 255
        let currentString: NSString = textView.text!.encodeEmoji as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text.encodeEmoji) as NSString
        return newString.length <= maxLength
    }
}
