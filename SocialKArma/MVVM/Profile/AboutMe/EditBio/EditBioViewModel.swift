//
//  EditBioViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import SwiftyJSON

class EditBioViewModel: NSObject {
    var bio = ""
    func saveAboutMeInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.saveAboutMeInfo(parameters: parameters, method: .put) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].dictionaryValue
                if let points = data["tzPoints"]?.intValue {
                    PointsHandler.shared.updateTZPoints(points: points, changedToBeShown: true)
                }
                completion(true, json, "")
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
