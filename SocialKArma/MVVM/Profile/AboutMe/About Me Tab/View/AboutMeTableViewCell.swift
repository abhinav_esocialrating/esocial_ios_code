//
//  AboutMeTableViewCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 28/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class AboutMeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var bkgView: UIView!

    var type = 1   //  1: About me    2: Personal info     3: Business info
    var viewModel: AboutMeViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
//        bkgView.layoutSubviews()
        if type == 2 {
            if indexPath.row == 0 {
                bkgView.roundCorners(corners: [.topLeft, .topRight], radius: 8)
            } else if indexPath.row == 7 {
                bkgView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
            } else {
                bkgView.roundCorners(corners: [.allCorners], radius: 0)
            }
        } else {
            if indexPath.row == 0 {
                bkgView.roundCorners(corners: [.topLeft, .topRight], radius: 8)
            } else if indexPath.row == 4 {
                bkgView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
            } else {
                bkgView.roundCorners(corners: [.allCorners], radius: 0)
            }
        }
    }
    func configureView() {
        bkgView.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(userType: Int) {
        switch type {
        case 1:
            switch indexPath.row {
            case 0:
                labelTitle.text = "Gender"
                valueLabel.text = viewModel.dataSource["gender"] as? String
            case 1:
                labelTitle.text = "DOB"
                valueLabel.text = userType == 1 ? (viewModel.dataSource["dob"] as? String) : "Private"
            case 2:
                labelTitle.text = "Location"
                valueLabel.text = viewModel.dataSource["location_city"] as? String
            case 3:
                labelTitle.text = "Relationship Status"
                valueLabel.text = viewModel.dataSource["relationship_status"] as? String
            default:
                labelTitle.text = "Email ID"
                valueLabel.text = userType == 1 ? (viewModel.dataSource["email_id"] as? String) : "Private"
            }
        case 2:
            switch indexPath.row {
            case 0:
                labelTitle.text = "Height (cms)"
                valueLabel.text = viewModel.dataSource["height"] as? String
            case 1:
                labelTitle.text = "Weight (kg)"
                valueLabel.text = viewModel.dataSource["weight"] as? String
            case 2:
                labelTitle.text = "Fitness"
                valueLabel.text = viewModel.dataSource["fitness"] as? String
            case 3:
                labelTitle.text = "Drinking"
                valueLabel.text = viewModel.dataSource["drinking"] as? String
            case 4:
                labelTitle.text = "Smoking"
                valueLabel.text = viewModel.dataSource["smoking"] as? String
            case 5:
                labelTitle.text = "Religion"
                valueLabel.text = viewModel.dataSource["religion"] as? String
            case 6:
                labelTitle.text = "Political View"
                valueLabel.text = viewModel.dataSource["political_view"] as? String
            default:
                labelTitle.text = "Blood Group"
                valueLabel.text = viewModel.dataSource["blood_group"] as? String
            }
        default:
            switch indexPath.row {
            case 0:
                labelTitle.text = ""
                valueLabel.text = ""
            case 2:
                labelTitle.text = ""
                valueLabel.text = ""
            default:
                labelTitle.text = ""
                valueLabel.text = ""
            }
        }
    }
    
}
