//
//  TextTableViewCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 28/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class TextTableViewCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var bkgView: UIView!
    var viewModel: AboutMeViewModel!
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
    func configureView() {
        bkgView.layer.cornerRadius = 8
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setValues() {
        attributedText(string: viewModel.biography.decodeEmoji)
//        label.text = viewModel.biography
//        let readmoreFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
//        let readmoreFontColor = darkBlue
//        if viewModel.biography.count > 140 {
//            label.addTrailing(with: "... ", moreText: "Read more", moreTextFont: readmoreFont, moreTextColor: readmoreFontColor, visibleLength: 140)
//        }
    }
    func attributedText(string: String) {
        let attributedString = NSMutableAttributedString(string: string)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontWith(name: .Roboto, face: .Regular, size: 16), range: NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
    }
}
