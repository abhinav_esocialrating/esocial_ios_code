//
//  LookingForCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 25/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class LookingForCell: UITableViewCell {
    
    @IBOutlet weak var tagView: TagListView!
    @IBOutlet weak var bkgView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        configureTagView()
    }
    func configureView() {
        bkgView.layer.cornerRadius = 8
    }
    func configureTagView() {
        tagView.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
    }

    func setTags(lookingFor: [JSON]) {
        tagView.removeAllTags()
        lookingFor.forEach { (item) in
            let dict = item.dictionaryValue
            let value = dict["value"]?.stringValue
            let id = dict["id"]?.intValue
            if id == 8 || id == 9 || id == 10 {
                let friendStr = "Friends - \(value ?? "")"
                tagView.addTag(friendStr)
            } else {
                tagView.addTag(value ?? "")
            }
        }
    }
}
