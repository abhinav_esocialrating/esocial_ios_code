//
//  AboutMeViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 28/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class AboutMeViewModel: NSObject {
    var interests = [InterestModel]()
    var aboutMeDetails = [["Gender":"Female"], ["DOB":"19/10/1992"], ["Location": "Bengaluru"], ["Relationship Status": "Single"], ["Email ID": "kavyasharma@yahoo.co.in"]]
    var biography = ""
    var personalInfo = [["Height (cms)":"183"], ["Weight (kg)":"86"], ["Fitness":"Fit"], ["Drinking": "Occasionally"], ["Smoking": "Never"], ["Religion": "Sikhism"], ["Political View": "Liberalism"]]
    var dataSource = [String:Any]()
    var lookingFor = [JSON]()
    var lookingForSelected = [JSON]()
    var dataSourceAboutMe = [[String: String]]()
    var dataSourcePersonalInfo = [[String: String]]()
    var userType = 1 // 1: Self   2: Other User
    
    // MARK: - Web Services
    func getLookingFor(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.getLookingFor(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let list = json["data"].arrayValue
                self.createLookingFor(list: list)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func getAboutMeInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getAboutMeInfo(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let list = json["data"].dictionaryValue
                self.createDataSource(list: list)
                self.saveJsonToUserDefaults(json: json["data"])
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func saveAboutMeInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.saveAboutMeInfo(parameters: parameters, method: .put) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let list = json["data"].dictionaryValue
                self.createDataSource(list: list)
                self.saveJsonToUserDefaults(json: json["data"])
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }

    // MARK: - Utility methods
    func saveJsonToUserDefaults(json: JSON) {
        UserDefaults.standard.AboutMe = json.rawString() ?? ""
    }
    
    func loadLocalData(completion: (_ success: Bool) -> Void) {
        let p = UserDefaults.standard.AboutMe
        if p != "" {
            if let json = p.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                do {
                    let json = try JSON(data: json)
                    let list = json.dictionaryValue
                    self.dataSource = list
                    completion(true)
                } catch {
                    completion(false)
                }
            } else {
                completion(false)
            }
        } else {
            completion(false)
        }
    }
    func createDataSource(list: [String:JSON]) {
        var dict = [String:String]()
        dataSourceAboutMe = []
        dataSourcePersonalInfo = []
        // About me Info Array
        dict = ["key": "Gender", "value": list["gender"]?.stringValue ?? ""]
        dataSourceAboutMe.append(dict)
        dict = ["key": "DOB", "value": userType == 1 ? (list["dob"]?.stringValue ?? "") : "Private"]
        dataSourceAboutMe.append(dict)
        var location = ""
        if userType == 2 {
            if (list["location_city"]?.stringValue ?? "") == "" {
                location = "Unavailable"
            } else {
                location = list["location_city"]?.stringValue ?? ""
            }
        }
        dict = ["key": "Location", "value": userType == 2 ? location : list["location_city"]?.stringValue ?? ""]
        dataSourceAboutMe.append(dict)
        dict = ["key": "Relationship Status", "value": list["relationship_status"]?.stringValue ?? ""]
        dataSourceAboutMe.append(dict)
        dict = ["key": "Email ID", "value": userType == 1 ?  (list["email_id"]?.stringValue ?? "") : "Private"]
        dataSourceAboutMe.append(dict)
        
        // Personal Info Array
        dict = ["key": "Height (cms)", "value": list["height"]?.stringValue ?? ""]
        dataSourcePersonalInfo.append(dict)
        dict = ["key": "Weight (kg)", "value": list["weight"]?.stringValue ?? ""]
        dataSourcePersonalInfo.append(dict)
        dict = ["key": "Fitness", "value": list["fitness"]?.stringValue ?? ""]
        dataSourcePersonalInfo.append(dict)
        dict = ["key": "Drinking", "value": list["drinking"]?.stringValue ?? ""]
        dataSourcePersonalInfo.append(dict)
        dict = ["key": "Smoking", "value": list["smoking"]?.stringValue ?? ""]
        dataSourcePersonalInfo.append(dict)
        dict = ["key": "Religion", "value": list["religion"]?.stringValue ?? ""]
        dataSourcePersonalInfo.append(dict)
        dict = ["key": "Political View", "value": list["political_view"]?.stringValue ?? ""]
        dataSourcePersonalInfo.append(dict)
//        dict = ["key": "Blood Group", "value": list["blood_group"]?.stringValue ?? ""]
//        dataSourcePersonalInfo.append(dict)
        
        // Biography
        biography = list["bio"]?.stringValue ?? ""
        
        /*  previous logic for about me
        dataSource["gender"] = list["gender"]?.stringValue ?? ""
        dataSource["dob"] = list["dob"]?.stringValue ?? ""
        dataSource["location_city"] = list["location_city"]?.stringValue ?? ""
        dataSource["relationship_status"] = list["relationship_status"]?.stringValue ?? ""
        dataSource["email_id"] = list["email_id"]?.stringValue ?? ""
        
        dataSource["height"] = list["height"]?.stringValue ?? ""
        dataSource["weight"] = list["weight"]?.stringValue ?? ""
        dataSource["fitness"] = list["fitness"]?.stringValue ?? ""
        dataSource["drinking"] = list["drinking"]?.stringValue ?? ""
        dataSource["smoking"] = list["smoking"]?.stringValue ?? ""
        dataSource["religion"] = list["religion"]?.stringValue ?? ""
        dataSource["political_view"] = list["political_view"]?.stringValue ?? ""
        dataSource["blood_group"] = list["blood_group"]?.stringValue ?? ""
        */
    }
    func createLookingFor(list: [JSON]) {
        lookingForSelected = []
        lookingFor = list
//        print(lookingFor)
        lookingFor.forEach { (item) in
            let dict = item.dictionaryValue
            if let selected = dict["selected"]?.bool, selected {
                lookingForSelected.append(item)
            }
        }
        // For managing changes in Looking for
        UserDetails.shared.workSpace = []
        UserDetails.shared.genderIDs = []
        UserDetails.shared.selectedWorkSpace = []
        UserDetails.shared.selectedGenderIDs = []
        list.forEach({ (jsonDict) in
            let dict = jsonDict.dictionaryValue
            if dict["type"]?.stringValue == "SHOW" {
                UserDetails.shared.workSpace.append(dict)
//                print(dict)
                if let selected = dict["selected"]?.bool, selected, let id = dict["id"]?.int {
                    UserDetails.shared.selectedWorkSpace.append(id)
                }
            }
            if dict["type"]?.stringValue == "HIDE" {
                UserDetails.shared.genderIDs.append(dict)
//                print(dict)
                if let selected = dict["selected"]?.bool, selected, let id = dict["id"]?.int {
                    UserDetails.shared.selectedGenderIDs.append(id)
                }
            }
        })
    }
    func checkForAllEmptyValuesIn(array: [[String: String]]) -> Bool {
        for i in array {
            let value = i["value"]
            if value != "" {
                return false
            }
        }
        return true
    }
    // MARK: - Interests
    func getAllInterests(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getAllInterests(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.interests = []
                self.interests = data.map({ (item) -> InterestModel in
                    let skill = InterestModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
