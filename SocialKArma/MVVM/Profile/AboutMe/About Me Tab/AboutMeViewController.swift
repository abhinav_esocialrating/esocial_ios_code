//
//  AboutMeViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 28/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
//import SJSegmentedScrollView

class AboutMeViewController: UIViewController {
    
    @IBOutlet weak var aboutMeTableView: UITableView!
    
    let viewModel = AboutMeViewModel()
    var userType = 1  // 1: Self   2: Other User
    var otherUserId = 0
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAboutMeInfo()
        getAllInterests()
        if userType == 1 {
            getLookingFor()
        }
    }
    func configureView() {
        aboutMeTableView.backgroundColor = .clear
        aboutMeTableView.register(UINib(nibName: TableViewCells.AboutMeTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.AboutMeTableViewCell.rawValue)
        aboutMeTableView.register(UINib(nibName: TableViewCells.TextTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.TextTableViewCell.rawValue)
        aboutMeTableView.register(UINib(nibName: TableViewCells.ExperienceHeaderCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.ExperienceHeaderCell.rawValue)
        aboutMeTableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        aboutMeTableView.estimatedRowHeight = 115
    }

    // MARK: - Methods
    func updateUI(section: Int) {
        DispatchQueue.main.async {
            self.aboutMeTableView.reloadData()
        }
    }
    func openInterests() {
        let vc = SkillsViewController.instantiate(fromAppStoryboard: .Profile)
        vc.screenType = 2
        vc.userType = userType
        vc.otherUserId = otherUserId
        navigationController?.pushViewController(vc, animated: true)
//        vc.modalPresentationStyle = .fullScreen
//        present(vc, animated: true, completion: nil)
    }
}
extension AboutMeViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return userType == 1 ? 1 : 0
        case 1:
            return viewModel.dataSourceAboutMe.count
        case 2:
            return viewModel.interests.count > 0 ? 1 : 0
        case 3:
            return viewModel.dataSourcePersonalInfo.count
        default:
            return viewModel.biography != "" ? 1 : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if userType == 2 {
                return UITableViewCell()
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.LookingForCell.rawValue, for: indexPath) as! LookingForCell
                cell.setTags(lookingFor: viewModel.lookingForSelected)
                return cell
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.AboutMeTableViewCell.rawValue, for: indexPath) as! AboutMeTableViewCell
            cell.indexPath = indexPath
            cell.viewModel = viewModel
            cell.type = 1
//            cell.setValues(userType: userType)
            cell.labelTitle.text = viewModel.dataSourceAboutMe[indexPath.row]["key"]
            cell.valueLabel.text = viewModel.dataSourceAboutMe[indexPath.row]["value"]
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TagTableViewCell.rawValue, for: indexPath) as! TagTableViewCell
            cell.interests = viewModel.interests
            cell.setTags()
//            cell.setTags(skills: nil, interests: viewModel.interests)
            cell.showSeeAllBtn = viewModel.interests.count > 5
            cell.bkgView.layer.cornerRadius = 8
            cell.handleSeeMore = {
                self.openInterests()
            }
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.AboutMeTableViewCell.rawValue, for: indexPath) as! AboutMeTableViewCell
            cell.indexPath = indexPath
            cell.viewModel = viewModel
            cell.type = 2
//            cell.setValues(userType: userType)
            cell.labelTitle.text = viewModel.dataSourcePersonalInfo[indexPath.row]["key"]
            cell.valueLabel.text = viewModel.dataSourcePersonalInfo[indexPath.row]["value"]
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TextTableViewCell.rawValue, for: indexPath) as! TextTableViewCell
            cell.viewModel = viewModel
            cell.setValues()
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return userType == 1 ? UITableView.automaticDimension : CGFloat.leastNonzeroMagnitude
        case 1:
            if let value = viewModel.dataSourceAboutMe[indexPath.row]["value"], value != "" {
                return 38
            }
            return userType == 1 ? 38 : 0
        case 2:
            return UITableView.automaticDimension
        case 3:
            if let value = viewModel.dataSourcePersonalInfo[indexPath.row]["value"], value != "" {
                return 38
            }
            return userType == 1 ? 38 : 0
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if userType == 1 {
            return 44
        }
        switch section {
        case 0:
            return CGFloat.leastNonzeroMagnitude
        case 1:
            return viewModel.checkForAllEmptyValuesIn(array: viewModel.dataSourceAboutMe) ? CGFloat.leastNonzeroMagnitude : 44
        case 2:
            return viewModel.interests.count > 0 ? 44 : CGFloat.leastNonzeroMagnitude
        case 3:
            return viewModel.checkForAllEmptyValuesIn(array: viewModel.dataSourcePersonalInfo) ? CGFloat.leastNonzeroMagnitude : 44
        default:
            return viewModel.biography != "" ? 44 : CGFloat.leastNonzeroMagnitude
        }
        /*
        if userType == 2 && section == 0 {
            return CGFloat.leastNonzeroMagnitude
        } else {
            return 44
        }
        */
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if userType == 2 && section == 0 {
            return nil
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ExperienceHeaderCell.rawValue) as! ExperienceHeaderCell
            cell.indexPath = IndexPath(row: 0, section: section)
            cell.editButton.isHidden = userType == 1 ? false : true
            cell.editImage.isHidden = userType == 1 ? false : true
            switch section {
            case 0:
                cell.label.text = "Looking For"
            case 1:
                cell.label.text = "Basic Details"
            case 2:
                cell.label.text = "Interests"
            case 3:
                cell.label.text = "Personal Info"
            default:
                cell.label.text = "Biography"
            }
            cell.editTapped = { index in
                self.handleEdit(index: index)
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 4 ? 24 : 10  // last section footer 24, for some gap from ending
    }
    func handleEdit(index: Int) {
        switch index {
        case 0:
            AppDebug.shared.consolePrint("")
            // Opening looking for screen
        case 1:
            let vc = EditInfoViewController.instantiate(fromAppStoryboard: .Profile)
            vc.screenType = 2
            navigationController?.pushViewController(vc, animated: true)
//            vc.modalPresentationStyle = .fullScreen
//            present(vc, animated: true, completion: nil)
        case 2:
            openInterests()
        case 3:
            let vc = EditInfoViewController.instantiate(fromAppStoryboard: .Profile)
            vc.screenType = 1
            navigationController?.pushViewController(vc, animated: true)
//            vc.modalPresentationStyle = .fullScreen
//            present(vc, animated: true, completion: nil)
        default:
            let vc = EditBioViewController.instantiate(fromAppStoryboard: .Profile)
            vc.viewModel.bio = viewModel.biography
            navigationController?.pushViewController(vc, animated: true)
//            vc.modalPresentationStyle = .fullScreen
//            present(vc, animated: true, completion: nil)
        }
    }
}
// MARK: - Web Services
extension AboutMeViewController {
    func getLookingFor() {
//        showHudToWindow()
        viewModel.getLookingFor(parameters: [:]) { (success, json, message) in
//            self.hideHud()
            if success {
                self.aboutMeTableView.reloadData()
            } else {
                // show message
            }
        }
    }
    func getAboutMeInfo() {
//        showHudToWindow()
        let params = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        viewModel.getAboutMeInfo(parameters: params) { (success, json, message) in
//            self.hideHud()
            if success {
                self.aboutMeTableView.reloadData()
            } else {
                // show message
            }
        }
    }
    
    // Interests
    func getAllInterests() {
        let parameters = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        viewModel.getAllInterests(parameters: parameters) { (success, json, message) in
            //            self.hideHud()
            if success {
                self.updateUI(section: 0)
            } else {
                // show message
            }
        }
    }
}
//extension AboutMeViewController: SJSegmentedViewControllerViewSource {
//    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
//        return aboutMeTableView
//    }
//}
//
