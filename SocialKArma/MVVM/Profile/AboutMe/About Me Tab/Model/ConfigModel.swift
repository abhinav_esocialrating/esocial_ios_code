//
//  ConfigModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 03/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import SwiftyJSON

class ConfigDataModel {

    static let shared = ConfigDataModel()

    private init(){}
    
    var fitness = [[String:JSON]]()
    var habitFrequency = [[String:JSON]]()
    var politicalView = [[String:JSON]]()
    var relationship = [[String:JSON]]()
    var relationship_status = [[String:JSON]]()
    var religion = [[String:JSON]]()
    
    func parseData(json: JSON) {
        var jsonArray = json["fitness"].arrayValue
        fitness = jsonArray.map { $0.dictionaryValue }
        jsonArray = json["habitFrequency"].arrayValue
        habitFrequency = jsonArray.map { $0.dictionaryValue }
        jsonArray = json["politicalView"].arrayValue
        politicalView = jsonArray.map { $0.dictionaryValue }
        jsonArray = json["relationship"].arrayValue
        relationship = jsonArray.map { $0.dictionaryValue }
        jsonArray = json["relationship_status"].arrayValue
        relationship_status = jsonArray.map { $0.dictionaryValue }
        jsonArray = json["religion"].arrayValue
        religion = jsonArray.map { $0.dictionaryValue }
    }
    
    func fetchValueFrom(id: Int, array: [[String:JSON]]) -> String {
        for i in array {
            if i["id"]?.intValue == id {
                return i["value"]?.stringValue ?? ""
            }
        }
        return ""
    }

}
