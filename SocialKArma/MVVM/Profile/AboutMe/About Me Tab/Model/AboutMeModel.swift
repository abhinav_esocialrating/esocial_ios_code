/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class AboutMeModel {
	public var dob : String?
	public var ip_address : String?
	public var registration_location : String?
	public var profile_pic : String?
	public var gender : String?
	public var updated_at : String?
	public var mobile : String?
	public var password : String?
	public var email : String?
	public var user_id : Int?
	public var full_name : String?
	public var device_id : String?
	public var country_code : Int?
	public var status : String?
	public var created_at : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [AboutMeModel]
    {
        var models:[AboutMeModel] = []
        for item in array
        {
            models.append(AboutMeModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		dob = dictionary["dob"] as? String
		ip_address = dictionary["ip_address"] as? String
		registration_location = dictionary["registration_location"] as? String
		profile_pic = dictionary["profile_pic"] as? String
		gender = dictionary["gender"] as? String
		updated_at = dictionary["updated_at"] as? String
		mobile = dictionary["mobile"] as? String
		password = dictionary["password"] as? String
		email = dictionary["email"] as? String
		user_id = dictionary["user_id"] as? Int
		full_name = dictionary["full_name"] as? String
		device_id = dictionary["device_id"] as? String
		country_code = dictionary["country_code"] as? Int
		status = dictionary["status"] as? String
		created_at = dictionary["created_at"] as? String
	}

		
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.dob, forKey: "dob")
		dictionary.setValue(self.ip_address, forKey: "ip_address")
		dictionary.setValue(self.registration_location, forKey: "registration_location")
		dictionary.setValue(self.profile_pic, forKey: "profile_pic")
		dictionary.setValue(self.gender, forKey: "gender")
		dictionary.setValue(self.updated_at, forKey: "updated_at")
		dictionary.setValue(self.mobile, forKey: "mobile")
		dictionary.setValue(self.password, forKey: "password")
		dictionary.setValue(self.email, forKey: "email")
		dictionary.setValue(self.user_id, forKey: "user_id")
		dictionary.setValue(self.full_name, forKey: "full_name")
		dictionary.setValue(self.device_id, forKey: "device_id")
		dictionary.setValue(self.country_code, forKey: "country_code")
		dictionary.setValue(self.status, forKey: "status")
		dictionary.setValue(self.created_at, forKey: "created_at")

		return dictionary
	}

}
