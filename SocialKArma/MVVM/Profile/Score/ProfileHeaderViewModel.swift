//
//  ProfileHeaderViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProfileHeaderViewModel: NSObject {
    
    var otherUserInfo = [String:Any]()
    var responseDict = [String:JSON]()
    // MARK: - Web Services
    func getUserInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.getUserInfo(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getUserInfo")
            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"]
                completion(true, data, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func requestScore(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
                
        NetworkManager.requestScore(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("requestScore")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"]
                completion(true, data, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }

}
//{
//  "status" : 200,
//  "data" : 0,
//  "message" : "Successfully requested"
//}
