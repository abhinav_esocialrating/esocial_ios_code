//
//  ScoreMultiColorSliderCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ScoreMultiColorSliderCell: UITableViewCell {
    @IBOutlet weak var slider: MultiColorSlider!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!
    
    var score: UserScore! {
        didSet {
            if let scores = score.score {
                slider.selectedMaxValue = CGFloat(scores)
                valueLabel.text = String(format: "%.1f/10", scores)
            } else {
                slider.selectedMaxValue = 1
                valueLabel.text = "1/10"
            }
            slider.refresh()
            nameLabel.text = score.parameter ?? "Parameter"
        }
    }
    var managedScore: UserScoreManagedObject! {
        didSet {
            slider.selectedMaxValue = CGFloat(managedScore.score)
            valueLabel.text = String(format: "%.1f/10", managedScore.score)

//            if let scores = managedScore.score {
//            } else {
//                slider.selectedMaxValue = 1
//                valueLabel.text = "1/10"
//            }
            slider.refresh()
            nameLabel.text = managedScore.parameter ?? "Parameter"
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        
        slider.selectedMaxValue = CGFloat(managedScore.score)
        valueLabel.text = String(format: "%.1f/10", managedScore.score)

//        if let scores = score.score {
//        } else {
//            slider.selectedMaxValue = 1
//            valueLabel.text = "1/10"
//        }
        slider.refresh()
        nameLabel.text = managedScore.parameter ?? "Parameter"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
