//
//  ScoreInfoCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 28/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ScoreInfoCell: UITableViewCell {
    
    @IBOutlet weak var bkgView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        contentView.layoutSubviews()
//        bkgView.layoutSubviews()
//        bkgView.roundCorners(corners: [.topLeft, .topRight], radius: 8)
//    }
}
