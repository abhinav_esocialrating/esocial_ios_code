//
//  ScoreTableViewCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 21/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ScoreTableViewCell: UITableViewCell {
    
    @IBOutlet weak var parameterLabel: UILabel!
    
    @IBOutlet weak var bkgView: UIView!
    
    @IBOutlet weak var avgScoreView: UIView!
        
    @IBOutlet weak var sliderLine: UIView!
    
    @IBOutlet weak var avgScoreLeading: NSLayoutConstraint!
    
    
    @IBOutlet weak var ownScoreLeading: NSLayoutConstraint!
    
    @IBOutlet weak var ownScoreView: UIImageView!
    
    var score: UserScore!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        sliderLine.layoutSubviews()
        bkgView.layoutSubviews()
        setValues()
        if indexPath.row == 7 {
            bkgView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
        } else {
            bkgView.roundCorners(corners: [.allCorners], radius: 0)
        }
    }
    func configureView() {
        avgScoreView.layer.cornerRadius = avgScoreView.frame.width/2
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues() {
        parameterLabel.text = score.parameter
        let lineFrame = sliderLine.frame.width
        if let scoree = score.userScore {
            let leadingRatio = (CGFloat(scoree) * lineFrame)/5
            print(leadingRatio)
            ownScoreLeading.constant = -leadingRatio - 4 // ticker //+ ownScoreView.frame.width
            if scoree == 5 {
                ownScoreLeading.constant += 15
            }
        } else {
            ownScoreLeading.constant = 4
        }
        if let scoree = score.score {
            let leadingRatio = (CGFloat(scoree) * lineFrame)/5
            avgScoreLeading.constant = -leadingRatio + 5 // own score
        } else {
            avgScoreLeading.constant = 0
        }
    }
    
}
