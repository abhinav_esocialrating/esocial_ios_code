//
//  ScoreViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 21/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

class ScoreViewModel: NSObject {
    var expandedCells = [0]
    var userType = 1  // 1: Self   2: Other User
    var otherUserId = 0
    var scoreData = [UserScore]()
    var managedScoreData = [UserScoreManagedObject]()
    var indexBeingModified = 0
    var countOfViewedParameters = 0
    
    // MARK: - Web Services
    func getUserReviews(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getUserReviews(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint("getUserReviews")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                self.scoreData = []
                self.countOfViewedParameters = 0
                let list = json["data"].arrayObject
                self.syncScores(array: list ?? [])
//                for i in list {
//                    let score = UserScore(dictionary: i.dictionaryValue)
//                    self.scoreData.append(score!)
//                    if (score?.status ?? false) == true {
//                        self.countOfViewedParameters += 1
//                    }
//                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func getScoreValues(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getScoreValues(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getScoreValues")
//            print(json)
            switch response.response?.statusCode {
            case 200:
//                self.scoreData = []
//                let list = json["data"].arrayValue
//                for i in list {
//                    let score = UserScore(dictionary: i.dictionaryValue)
//                    self.scoreData.append(score!)
//                }
                
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    // MARK: - Local Storage
    func syncScores(array: [Any]) {
        guard let taskContext = appDelegate?.persistentContainer.newBackgroundContext() else {
            return
        }
        taskContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        taskContext.undoManager = nil
        
        taskContext.performAndWait {
            
            if let array = array as? [[String:Any]] {
            
                let matchingEpisodeRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserScoreManagedObject")
                let episodeIds = array.map { $0["parameter_id"] as? Int }.compactMap { $0 }
                matchingEpisodeRequest.predicate = NSPredicate(format: "parameter_id in %@", argumentArray: [episodeIds])
                
                let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: matchingEpisodeRequest)
                batchDeleteRequest.resultType = .resultTypeObjectIDs
                
                do {
                    let batchDeleteResult = try taskContext.execute(batchDeleteRequest) as? NSBatchDeleteResult
                    
                    if let deletedObjectIDs = batchDeleteResult?.result as? [NSManagedObjectID] {
                        NSManagedObjectContext.mergeChanges(fromRemoteContextSave: [NSDeletedObjectsKey: deletedObjectIDs],
                                                            into: [appDelegate!.persistentContainer.viewContext])
                    }
                } catch {
                    print("Error: \(error)\nCould not batch delete existing records.")
                    return
                }
                
                for dict in array {
                    guard let scores = NSEntityDescription.insertNewObject(forEntityName: "UserScoreManagedObject", into: taskContext) as? UserScoreManagedObject else {
                       print("Error: Failed to create a new Film object!")
                       return
                    }
                    scores.update(with: dict)
                    
                }
                
                if taskContext.hasChanges {
                    do {
                        try taskContext.save()
                    } catch {
                        print("Error: \(error)\nCould not save Core Data context.")
                    }
                    taskContext.reset() // Reset the context to clean up the cache and low the memory footprint.
                }
            }
        }
    }
    
    func fetchData(_ completion: @escaping () -> Void) {
        let persistentContainer = appDelegate?.persistentContainer
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserScoreManagedObject")
           
        do {
            let results = try persistentContainer?.viewContext.fetch(fetchRequest)
            let scores = results as? [UserScoreManagedObject]
            self.managedScoreData = scores ?? []
        } catch {
            print("error")
        }
        
        completion()
    }
}
