//
//  ScoreViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 21/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
//import SJSegmentedScrollView
import DropDown
import CoreData

class ScoreViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var helpView: UIView!
    @IBOutlet weak var otherCircle: UIImageView!
    @IBOutlet weak var ownScoreLabel: UILabel!
    @IBOutlet weak var relationshipLabel: UILabel!
    
    @IBOutlet weak var dropDownBtn: UIButton!
    // MARK: - Variables
    var viewModel = ScoreViewModel()
    let profileHeaderViewModel = ProfileHeaderViewModel()
    var permission = ""
    let dropDown = DropDown()
//    lazy var fetchedResultsController: NSFetchedResultsController<UserScoreManagedObject> = {
//        let fetchRequest = NSFetchRequest<UserScoreManagedObject>(entityName:"UserScoreManagedObject")
//
//        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
//                                                    managedObjectContext: appDelegate!.persistentContainer.viewContext,
//                                                    sectionNameKeyPath: nil, cacheName: nil)
//        controller.delegate = self
//
//        do {
//            try controller.performFetch()
//        } catch {
//            let nserror = error as NSError
//            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//        }
//
//
//
//        return controller
//    }()
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        viewModel.fetchData {
            self.updateUI()
        }
        if viewModel.userType == 1 {
            getUserReviews(relationship_id: 0)
        } else {
            getUserInfo()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func configureView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: TableViewCells.ScoreTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.ScoreTableViewCell.rawValue)
        tableView.register(UINib(nibName: TableViewCells.ScoreInfoCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.ScoreInfoCell.rawValue)
        tableView.register(UINib(nibName: TableViewCells.ExperienceHeaderCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.ExperienceHeaderCell.rawValue)
        tableView.registerFromXib(name: TableViewCells.ScoreMultiColorSliderCell.rawValue)
        dropDownBtn.setUpBorder(width: 0.8, color: UIColor.darkGray, radius: 8)
    }
    // MARK: - Helper methods
    func updateUI() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func showHelpView() {
        let window = UIApplication.shared.keyWindow!
        let popUpView = UIView(frame: window.frame)
        popUpView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//        helpView.frame = CGRect(x: (screenWidth/2)-116, y: (screenHeight/2)-52, width: 232, height: 104)
        window.addSubview(popUpView)
        window.addSubview(helpView)
        helpView.layer.cornerRadius = 8
        otherCircle.layer.cornerRadius = otherCircle.frame.width/2
        helpView.anchor(top: nil, paddingTop: 0, bottom: nil, paddingBottom: 0, left: helpView.superview?.leftAnchor, paddingLeft: 50, right: helpView.superview?.rightAnchor, paddingRight: 50, width: 0, height: 0)
        let constraint = NSLayoutConstraint(item: helpView!, attribute: .centerY, relatedBy: .equal, toItem: helpView.superview, attribute: .centerY, multiplier: 1.0, constant: 0)
        window.addConstraint(constraint)
        ownScoreLabel.text = viewModel.userType == 1 ? Constants.ScoreInfoSelf.rawValue : Constants.ScoreInfoOther.rawValue
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(helpViewTapped(_:)))
        popUpView.addGestureRecognizer(tap)
    }
    // MARK: - Actions
    @IBAction func helpTapped(_ sender: Any) {
       showHelpView()
    }
    @objc func helpViewTapped(_ sender: UITapGestureRecognizer) {
        let popUpView = sender.view
        popUpView?.removeFromSuperview()
        helpView.removeFromSuperview()
    }
        
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dropDownBtnTapped(_ sender: Any) {
        showDropDown()
    }
}
// MARK: - Dropdown
extension ScoreViewController {
    // MARK: - Methods
    
    
    func showDropDown() {
        dropDown.anchorView = dropDownBtn
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 200
        var datasource = ["All"]
        datasource.append(contentsOf: ConfigDataModel.shared.relationship.map({ ($0["value"]?.stringValue ?? "All") }))
        dropDown.dataSource = datasource
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropSelection(index: index, item: item)
        }
        dropDown.show()
    }
    func handleDropSelection(index: Int, item: String) {
        relationshipLabel.text = item
        switch index {
        case 0:
            print("All")
            getUserReviews(relationship_id: 0)
        default:
            let relationship_id = ConfigDataModel.shared.relationship[index-1]["id"]?.intValue
            getUserReviews(relationship_id: relationship_id ?? 0)
            print(index-1)
        }
    }
}
// MARK: - TableView methods
extension ScoreViewController: NSFetchedResultsControllerDelegate {
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        updateUI()
    }

}
// MARK: - TableView methods
extension ScoreViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : viewModel.managedScoreData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ScoreInfoCell.rawValue, for: indexPath) as! ScoreInfoCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ScoreMultiColorSliderCell.rawValue, for: indexPath) as! ScoreMultiColorSliderCell
            cell.indexPath = indexPath
            cell.managedScore = viewModel.managedScoreData[indexPath.row]
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 60
        default:
            return 90
          /*  if viewModel.userType == 1 {
                return 80
            } else {
                if permission == "ACCEPTED" {
                    return 80
                } else if (viewModel.scoreData[indexPath.row].status ?? false) {
                    return 80
                } else {
                    return 42
                }
            } */
        }
        
    }
/*    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            print("Do nothing")
        default:
            if viewModel.userType == 1 {
                // Do nothing
            } else if permission == "ACCEPTED" {
                // Do nothing
            } else {
                if viewModel.countOfViewedParameters < 2 && (viewModel.scoreData[indexPath.row].status ?? false) == false {
                    viewModel.indexBeingModified = indexPath.row
                    getScoreValues()
                } else if (viewModel.scoreData[indexPath.row].status ?? false) == true {
                    // Do nothing
                } else {
                    alert(message: "Please request for viewing score!")
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 50 : CGFloat.leastNonzeroMagnitude
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ExperienceHeaderCell.rawValue) as! ExperienceHeaderCell
            cell.indexPath = IndexPath(row: 0, section: section)
            cell.vc = 0
            cell.label.text = ""
            cell.editImage.applyTemplate(image: UIImage(named: "Panel-FAQ"), color: UIColor.NewTheme.darkGray)
            cell.editTapped = { index in
                self.helpTapped(cell.editButton!)
            }
            return cell
        default:
            return nil
        }
        
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? CGFloat.leastNonzeroMagnitude : 18
    } */
}

// MARK: - Web Services
extension ScoreViewController {
    func getUserReviews(relationship_id: Int) {
        var params = ["id": viewModel.userType == 1 ? UserDefaults.standard.userId : "\(viewModel.otherUserId)"] as [String:Any]
        if relationship_id != 0 {
            params["relationship_id"] = relationship_id
        }
        viewModel.getUserReviews(parameters: params) { (success, json, message) in
            self.viewModel.fetchData {
                self.updateUI()
            }
        }
    }
    func getUserInfo() {
        profileHeaderViewModel.getUserInfo(parameters: ["id":viewModel.otherUserId]) { (success, json, message) in
            if success {
                if let perm = json["permission"].string {
                    self.permission = perm
                }
                self.getUserReviews(relationship_id: 0)
            }
        }
    }
    func getScoreValues() {
        let score = viewModel.scoreData[viewModel.indexBeingModified]
        let params = ["parameterId": score.parameter_id ?? 0, "userId": viewModel.otherUserId] as [String : Any]
        viewModel.getScoreValues(parameters: params) { (success, json, message) in
            self.getUserReviews(relationship_id: 0)
        }
    }
}
//// MARK: - SJSegmentedView
//extension ScoreViewController: SJSegmentedViewControllerViewSource {
//    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
//        return tableView
//    }
//}
/*
 {
  "message" : "Already viewed two parameters",
  "data" : [

  ],
  "status" : 403
}
*/
