//
//  UserScoreManagedObject+CoreDataClass.swift
//  
//
//  Created by Abhinav Dobhal on 24/10/20.
//
//

import Foundation
import CoreData


public class UserScoreManagedObject: NSManagedObject {

}
extension UserScoreManagedObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserScoreManagedObject> {
        return NSFetchRequest<UserScoreManagedObject>(entityName: "UserScoreManagedObject")
    }

    @NSManaged public var parameter_id: Int64
    @NSManaged public var parameter: String?
    @NSManaged public var status: Bool
    @NSManaged public var userScore: Int64
    @NSManaged public var score: Double
    @NSManaged public var rated_user_id: Double

    func update(with dictionary: [String: Any]) {

        parameter_id = dictionary["parameter_id"] as? Int64 ?? 0
        parameter = dictionary["parameter"] as? String
        status = dictionary["status"] as? Bool ?? false
        userScore = dictionary["userScore"] as? Int64 ?? 0
        score = dictionary["score"] as? Double ?? 1.0
        rated_user_id = dictionary["rated_user_id"] as? Double ?? 0
    }
    
}
