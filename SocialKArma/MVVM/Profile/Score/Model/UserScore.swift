
import Foundation
import SwiftyJSON

public class UserScore {
	public var parameter_id : Int?
	public var parameter : String?
	public var status : Bool?
	public var userScore : Int?
	public var score : Double?
	public var rated_user_id : Double?

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let UserScore = UserScore(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: UserScore Instance.
*/
    required public init?(dictionary: [String:JSON]) {

        parameter_id = dictionary["parameter_id"]?.int
        parameter = dictionary["parameter"]?.string
        status = dictionary["status"]?.bool
        userScore = dictionary["userScore"]?.int
        score = dictionary["score"]?.double
		rated_user_id = dictionary["rated_user_id"]?.double
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.parameter_id, forKey: "parameter_id")
		dictionary.setValue(self.parameter, forKey: "parameter")
		dictionary.setValue(self.status, forKey: "status")
		dictionary.setValue(self.userScore, forKey: "userScore")
		dictionary.setValue(self.score, forKey: "score")
		dictionary.setValue(self.rated_user_id, forKey: "rated_user_id")

		return dictionary
	}

}
