//
//  InterestsTableViewCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 17/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class InterestsTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var companyLabel: UILabel!
    
    @IBOutlet weak var desigLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var cityLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Actions
    
    @IBAction func editTapped(_ sender: Any) {
    }
    
}
