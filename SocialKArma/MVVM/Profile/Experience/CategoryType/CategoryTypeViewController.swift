//
//  CategoryTypeViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 30/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CategoryTypeViewController: UIViewController {
    
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var tapView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    
    var categoryTypes = ["Professional Work", "Freelancing Work", "Providing a Service"]
    var selectedCategory = ""
    var completion: ((_ type: String) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        addTapGesture()
        configureView()
    }
    func configureView() {
        tagListView.delegate = self
        tagListView.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
        tagListView.removeAllTags()
        categoryTypes.forEach({ tagListView.addTag($0) })
        
        saveBtn.setUpBorder(width: 1.0, color: UIColor.SearchColors.blue, radius: 8)
    }
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(mainViewTapped(_:)))
        tapView.addGestureRecognizer(tap)
    }
    
    // MARK: - Navigation

    @objc func mainViewTapped(_ gesture:UIGestureRecognizer) {
        completion?("")
    }
    
    
    @IBAction func saveTapped(_ sender: Any) {
        completion?(selectedCategory)
    }
    

}
extension CategoryTypeViewController: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print(title)
        sender.tagViews.forEach {$0.isSelected = false}
        tagView.isSelected = !tagView.isSelected
        selectedCategory = title
    }
}
