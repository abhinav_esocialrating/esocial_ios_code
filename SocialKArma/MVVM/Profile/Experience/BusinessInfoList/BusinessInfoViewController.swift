//
//  BusinessInfoViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class BusinessInfoViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    // MARK: - Variables
    let viewModel = BusinessInfoViewModel()

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getBusinessInfo()
    }
    func configureView() {
        tableView.register(UINib(nibName: TableViewCells.BusinessListCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.BusinessListCell.rawValue)
        tableView.register(UINib(nibName: TableViewCells.ExperienceHeaderCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.ExperienceHeaderCell.rawValue)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 81
        tableView.dataSource = self
        tableView.delegate = self
    }

    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    // MARK: - Methods
    func openBusinessInfoVC(addEditOrDelete: Int, index: Int) {
        let vc = EditBusinessInfoViewController.instantiate(fromAppStoryboard: .Profile)
        vc.viewModel.addEditOrDelete = addEditOrDelete
        if addEditOrDelete == 2 {
            vc.viewModel.businessService = viewModel.businessServices[index]
        }
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    func updateUI() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    // MARK: - Web Services
    func getBusinessInfo() {
        viewModel.getBusinessInfo(parameters: ["id": UserDefaults.standard.userId]) { (success, json, message) in
            //            self.hideHud()
            if success {
                self.updateUI()
            } else {
                // show message
            }
        }
    }
}

// MARK: - TableView methods
extension BusinessInfoViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.businessServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.BusinessListCell.rawValue, for: indexPath) as! BusinessListCell
        cell.indexPath = indexPath
        cell.businessService = viewModel.businessServices[indexPath.row]
        cell.edit = { index in
            self.openBusinessInfoVC(addEditOrDelete: 2, index: indexPath.row)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ExperienceHeaderCell.rawValue) as! ExperienceHeaderCell
        cell.indexPath = IndexPath(row: 0, section: section)
        cell.editButton.isHidden = false
        cell.editImage.applyTemplate(image: UIImage(named: "Add-icon-white"), color: UIColor.NewTheme.paleBlue)
        cell.label.text = "Services"
        cell.lowerView.isHidden = false
        cell.editTapped = { index in
            self.openBusinessInfoVC(addEditOrDelete: 1, index: index)
        }
        return cell
    }
}
