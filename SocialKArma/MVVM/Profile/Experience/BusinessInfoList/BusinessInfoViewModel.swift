//
//  BusinessInfoViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class BusinessInfoViewModel: NSObject {

    var businessServices = [BusinessService]()

    func getBusinessInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getBusinessInfo(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getBusinessInfo")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let serviceArray = jsonData["data"] as! NSArray
                    self.businessServices = []
                    self.businessServices = BusinessService.modelsFromDictionaryArray(array: serviceArray)
                } catch {
                    
                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
