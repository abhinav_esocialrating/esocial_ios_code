//
//  EditBusinessInfoViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class EditBusinessInfoViewController: UIViewController {

    @IBOutlet weak var servicesTF: ACFloatingTextfield!
    @IBOutlet weak var tagListView: TagListView!
    
    @IBOutlet weak var addViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var yesBtn: UIButton!
    
    
    var viewModel = EditBusinessInfoViewModel()
    let dropDown = DropDown()
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
//        if viewModel.addEditOrDelete == 2 {
        setInitialValues()
//        }
        self.addViewHeight.constant = 0
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: Notification.Name.Profile.Skills, object: self)
    }
    func configureView() {
        for (j,i) in [servicesTF].enumerated() {
            i?.lineColor = .gray
            i?.placeHolderColor = .gray
            i?.selectedPlaceHolderColor = .gray
            i?.placeholder = viewModel.placeHolders[j]
            i?.selectedLineColor = UIColor.NewTheme.paleBlue
            i?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 14)
            i?.delegate = self
        }
        servicesTF.addTarget(self, action: #selector(searchTextChanged(_:)), for: .editingChanged)
        tagListView.delegate = self
        tagListView.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
        
        yesBtn.layer.cornerRadius = 6
    }

    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
//        dismiss(animated: true, completion: nil)
    }
    @objc func searchTextChanged(_ textField: UITextField) {
        viewModel.isSelectedFromDropDown = false
        if textField.text! != "" {
            let params = ["config": "business_service", "search": textField.text!]
            viewModel.getSearchConfig(parameters: params) { (success, json, message) in
                if success {
                    DispatchQueue.main.async {
                        if self.viewModel.searchedData.count > 0 {
                            self.showDropDown(textField: textField)
                            self.addViewHeight.constant = 0
                        } else {
                            self.dropDown.hide()
                            self.addViewHeight.constant = 90
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        saveBusinessInfo(serviceId: 1)
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
//        alertWithTwoAction(message: "Are you sure to delete?", okHandler: {
//            self.deleteBusinessInfo()
//        }) {}
    }
    
    @IBAction func yesTapped(_ sender: Any) {

        if self.servicesTF.text != "" {
            let vc = CategoryTypeViewController.instantiate(fromAppStoryboard: .Profile)
            vc.completion = { type in
                self.remove()
                if type != "" {
                    self.addToMasterTable(type: type)
                }
            }
            self.add(vc)
        }
    }
    // MARK: - Utility methods
    func setInitialValues() {
        updateTagView(delete: false)
    }
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.dataSource = viewModel.dropDownData
        dropDown.selectionAction = { (index: Int, item: String) in
            self.viewModel.isSelectedFromDropDown = true
            let id = self.viewModel.searchedData[index]["id"] as? Int
//            self.viewModel.services.append(self.viewModel.searchedData[index])
            self.saveBusinessInfo(serviceId: id ?? 0)
        }
        dropDown.show()
    }
    func updateTagView(delete: Bool) {
        tagListView.removeAllTags()
        for i in viewModel.businessServices {
            if let services = i.service, services.count != 0 {
                tagListView.addTag(i.service?[0].business_service ?? "")
            }
        }
    }
    // MARK: - Web Services
    func saveBusinessInfo(serviceId: Int) {
        var params = [String:Any]()
//        params["services"] = viewModel.services.map({ (dict) -> Int in
//            return dict["id"] as! Int
//        })
        params["services"] = [serviceId]
        params["city"] = "New Delhi"
        params["start_time"] = "09:00:00"
        params["end_time"] = "18:00:00"
        if viewModel.addEditOrDelete == 2 {
            params["user_business_info_id"] = viewModel.businessService.user_business_info_id ?? 0
        }
        print(params)
        self.showHud()
        viewModel.saveBusinessInfo(parameters: params) { (success, json, message) in
            self.hideHud()
             if success {
                self.updateTagView(delete: false)
                self.servicesTF.text = ""
            }
        }
    }
    func deleteBusinessInfo(id: Int) {
        viewModel.deleteBusinessInfo(parameters: ["id": id], delete: true) { (success, json, message) in
            if success {
                self.updateTagView(delete: true)
//                DispatchQueue.main.async {
//                    self.dismiss(animated: true, completion: nil)
//                }
            }
        }
    }
    func addToMasterTable(type: String) {
        var categoryType = ""
        switch type {
        case "Professional Work":
            categoryType = "PR"
        case "Freelancing Work":
            categoryType = "FR"
        default:
            categoryType = "SP"
        }
        let params = ["categoryName": servicesTF.text!, "categoryType" : categoryType]
        viewModel.addToMasterTable(parameters: params) { (success, data, message) in
            
            if let id = data.int {
                self.saveBusinessInfo(serviceId: id)
            }
        }
    }
}
// MARK: - TextField delegate
extension EditBusinessInfoViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text!.count == 0 {
            addViewHeight.constant = 0
        }
//        dropDown.hide()
//        if !viewModel.isSelectedFromDropDown && servicesTF.text != "" {
//            let vc = CategoryTypeViewController.instantiate(fromAppStoryboard: .Profile)
//            vc.completion = { type in
//                self.remove()
//                if type != "" {
//                    self.addToMasterTable(type: type)
//                }
//            }
//            add(vc)
//        }
    }
}
// MARK: - TagListView delegates
extension EditBusinessInfoViewController: TagListViewDelegate {
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        var index = 1000
        var id: Int?
        for (j,i) in viewModel.businessServices.enumerated() {
            if title == i.service?[0].business_service {
                index = j
                id = i.user_business_info_id
                break
            }
        }
        viewModel.indexToDelete = index
        deleteBusinessInfo(id: id ?? 0)
//        tagListView.removeTag(title)
//        interestViewHeight.constant = interestView.intrinsicContentSize.height
        
    }
    
}

