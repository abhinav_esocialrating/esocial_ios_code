//
//  BusinessServicesCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 14/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class BusinessServicesCell: UITableViewCell {
    
    
    @IBOutlet weak var labelTitle: UILabel!
    
    var delete: ((_ index: IndexPath) -> Void)? = nil

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setValues(title: String?) {
        labelTitle.text = title
    }
    // MARK: - Actions
    @IBAction func deleteTapped(_ sender: Any) {
        delete?(indexPath)
    }
}
