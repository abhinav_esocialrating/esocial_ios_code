//
//  BusinessListCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class BusinessListCell: UITableViewCell {
    
    @IBOutlet weak var servicesLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var editButton: UIButton!
    var businessService: BusinessService! {
        didSet {
            setValues()
        }
    }
    
    var edit: ((_ index: IndexPath) -> Void)? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues() {
        let arr = businessService.service ?? []
        let strArr = arr.map({$0.business_service ?? ""})
        let services = strArr.joined(separator: ", ")
        servicesLabel.text = services
        locationLabel.text = businessService.city
        timeLabel.text = "\(businessService.start_time ?? "")" + "-" + "\(businessService.end_time ?? "")"
    }
    
    
    // MARK: - Action
    
    @IBAction func editTapped(_ sender: Any) {
        edit?(indexPath)
    }
    
}
