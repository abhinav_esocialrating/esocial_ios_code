//
//  EditBusinessInfoViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class EditBusinessInfoViewModel: NSObject {
    let placeHolders = ["Service"]
    var searchedData = [[String:Any]]()
    var dropDownData = [String]()
    var services = [[String:Any]]()
    var businessService: BusinessService!
    var addEditOrDelete = 1  // 1: Add   2: Edit    3: Delete
    var businessServices = [BusinessService]()
    var indexToDelete = 1000
    var isSelectedFromDropDown = false

    // MARK: - Search suggestions
    func getSearchConfig(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseSearchedData(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func deleteBusinessInfo(parameters: [String:Any], delete: Bool, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.deleteBusinessInfo(parameters: parameters, method: delete ? .delete : .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("deleteBusinessInfo")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].dictionaryValue
                if let points = data["tzPoints"]?.intValue {
                    PointsHandler.shared.updateTZPoints(points: points, changedToBeShown: true)
                }
                completion(true, JSON.init(), json["message"].stringValue)
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            case 202:
                self.businessServices.remove(at: self.indexToDelete)
                self.indexToDelete = 1000
                completion(true, JSON.init(), json["message"].stringValue)
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
        
    func saveBusinessInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.saveBusinessInfo(parameters: parameters, method: addEditOrDelete == 1 ? .post : .put) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData["data"] as! NSDictionary
                    let serviceArray = data["data"] as! NSArray
                    let updated = BusinessService.modelsFromDictionaryArray(array: serviceArray)
                    self.businessServices += updated
                    if let points = data["tzPoints"] as? Int {
                        PointsHandler.shared.updateTZPoints(points: points, changedToBeShown: true)
                    }
                } catch {
                    
                }
                completion(true, json, json["message"].stringValue)
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
        
    }
    func addToMasterTable(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ data: JSON, _ message: String) -> Void)) {

        NetworkManager.addToCategoryservicesMasterTable(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json["data"], "")
            default:
                let message = json["message"].stringValue
                completion(false, [:], message)
            }
        }
    }
    // MARK: - Utility methods
    func parseSearchedData(data: [JSON]) {
        let search = data.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["id"] = json["id"].intValue
            dict["value"] = json["value"].string
            return dict
        }
        searchedData = search
        let drop = search.map { (dict) -> String in
            return (dict["value"] as? String) ?? ""
        }
        dropDownData = drop
    }
}
