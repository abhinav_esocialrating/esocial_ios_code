//
//  ExperienceViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
//import SJSegmentedScrollView

class ExperienceViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var experienceTableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    // MARK: - Variables
    var viewModel = ExperienceViewModel()
    var userType = 1  // 1: Self   2: Other User
    var otherUserId = 0
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if userType == 1 {
//            loadLocalData()
        }
        getExperience()
        getAllSkills()
        getBusinessInfo()
    }
    func configureView() {
        experienceTableView.register(UINib(nibName: TableViewCells.InterestsTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.InterestsTableViewCell.rawValue)
        experienceTableView.register(UINib(nibName: TableViewCells.BusinessListCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.BusinessListCell.rawValue)
        experienceTableView.register(UINib(nibName: TableViewCells.ExperienceHeaderCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.ExperienceHeaderCell.rawValue)
        experienceTableView.backgroundColor = .clear
        experienceTableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        experienceTableView.estimatedRowHeight = 120
    }
    
    // MARK: - Helper methods
    func openEducationList() {
        if viewModel.experienceDataSource.count > 0 {
            let vc = EducationListViewController.instantiate(fromAppStoryboard: .Profile)
            vc.viewModel.screenType = 2
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = EditEducationViewController.instantiate(fromAppStoryboard: .Profile)
            vc.viewModel.screenType = 2
            vc.viewModel.addEditOrDelete = 1
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    func openSkillList() {
        let vc = SkillsViewController.instantiate(fromAppStoryboard: .Profile)
        vc.screenType = 1
        navigationController?.pushViewController(vc, animated: true)
    }
    func openBusinessInfoVC() {
        let vc = BusinessInfoViewController.instantiate(fromAppStoryboard: .Profile)
        navigationController?.pushViewController(vc, animated: true)
//        vc.modalPresentationStyle = .fullScreen
//        present(vc, animated: true, completion: nil)
    }
    func openEditBusinessInfoVC() {
        let vc = EditBusinessInfoViewController.instantiate(fromAppStoryboard: .Profile)
        vc.viewModel.businessServices = viewModel.businessServices
        navigationController?.pushViewController(vc, animated: true)
//        vc.modalPresentationStyle = .fullScreen
//        present(vc, animated: true, completion: nil)
    }
    func updateUI(section: Int) {
        DispatchQueue.main.async {
            if self.userType == 2 {
                if self.viewModel.experienceDataSource.count == 0 && self.viewModel.skills.count == 0 && self.viewModel.isBusinessServicesEmpty() {
                    self.noDataLabel.isHidden = false
                    self.experienceTableView.isHidden = true
                } else {
                    self.noDataLabel.isHidden = true
                    self.experienceTableView.isHidden = false
                }
            } else {
                self.noDataLabel.isHidden = true
                self.experienceTableView.isHidden = false
            }
            self.experienceTableView.reloadData()
        }
    }
    func loadLocalData() {
        viewModel.loadLocalData { (success) in
            if success {
                self.updateUI(section: 0)
            }
        }
    }
}
// MARK: - TableView methods
extension ExperienceViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return viewModel.isBusinessServicesEmpty() ? 0 : 1
        case 1:
            return viewModel.experienceDataSource.count
        default:
            return viewModel.skills.count > 0 ? 1 : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TagTableViewCell.rawValue) as! TagTableViewCell
            cell.isBusinessCell = true
            cell.setServicesTags(services: viewModel.businessServices)
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.InterestsTableViewCell.rawValue, for: indexPath) as! InterestsTableViewCell
            cell.indexPath = indexPath
            cell.viewModel = viewModel
            cell.experience = viewModel.experienceDataSource[indexPath.row]
            cell.editButton.isHidden = true
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TagTableViewCell.rawValue) as! TagTableViewCell
            cell.skills = viewModel.skills
            cell.setTags()
//            cell.setTags(skills: viewModel.skills, interests: nil)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
        case 1:
            return UITableView.automaticDimension
        default:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if userType == 1 {
            return 40
        }
        switch section {
        case 0:
            return viewModel.isBusinessServicesEmpty() ? CGFloat.leastNonzeroMagnitude : 40
        case 1:
            return viewModel.experienceDataSource.count > 0 ? 40 : CGFloat.leastNonzeroMagnitude
        default:
            return viewModel.skills.count > 0 ? 40 : CGFloat.leastNonzeroMagnitude
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ExperienceHeaderCell.rawValue) as! ExperienceHeaderCell
        cell.indexPath = IndexPath(row: 0, section: section)
        cell.vc = 0
        cell.editButton.isHidden = userType == 1 ? false : true
        cell.editImage.isHidden = userType == 1 ? false : true
        switch section {
        case 0:
           cell.label.text = "Category/Services"
           cell.totalExperienceLabel.text = ""
        case 1:
            cell.label.text = "Experience"
            cell.totalExperienceLabel.text = "\u{2022} \(UserDefaults.standard.TotalExperience)"
        default:
            cell.label.text = "Skills"
            cell.totalExperienceLabel.text = ""
        }
        cell.editTapped = { index in
            switch section {
            case 0:
                self.openEditBusinessInfoVC()
            case 1:
                self.openEducationList()
            default:
                self.openSkillList()
            }
        }
        return cell
    }

}
// MARK: - Web Services
extension ExperienceViewController {
    func getExperience() {
        //        showHudToWindow()
        let params = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        viewModel.getExperience(parameters: params) { (success, json, message) in
            //            self.hideHud()
            if success {
                self.updateUI(section: 0)
            } else {
                // show message
            }
        }
    }
    func getBusinessInfo() {
        let params: [String:Any] = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        viewModel.getBusinessInfo(parameters: params) { (success, json, message) in
            if success {
                self.updateUI(section: 1)
            } else {
                // show message
            }
        }
    }
    func getAllSkills() {
        let parameters = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        viewModel.getAllSkills(parameters: parameters) { (success, json, message) in
            if success {
                self.updateUI(section: 1)
            } else {
                // show message
            }
        }
    }
}
//extension ExperienceViewController: SJSegmentedViewControllerViewSource {
//    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
//        return experienceTableView
//    }
//}
//
