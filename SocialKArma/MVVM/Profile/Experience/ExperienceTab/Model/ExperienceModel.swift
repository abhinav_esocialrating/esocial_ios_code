/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
import SwiftyJSON
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ExperienceModel {
	public var user_organisation_id : Int?
    public var user_id : Int?
    public var designation_id : Int?
    public var industry_type_id : Int?
    public var organisation_id : Int?
    public var job_type_id : Int?
    public var start_date_month : Int?
    public var start_date_year : Int?
    public var end_date_month : Int?
    public var end_date_year : Int?
    public var designation : String?
    public var industry_type : String?
    public var job_type : String?
    public var organisation : String?
    public var city : String?
    public var currently_working : Int?

    public class func modelsFromDictionaryArray(array:NSArray) -> [ExperienceModel]
    {
        var models:[ExperienceModel] = []
        for item in array
        {
            models.append(ExperienceModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    required public init?(dictionary: NSDictionary) {

        designation_id = dictionary["designation_id"] as? Int
        start_date_month = dictionary["start_date_month"] as? Int
        job_type_id = dictionary["job_type_id"] as? Int
        organisation = dictionary["organisation"] as? String
        designation = dictionary["designation"] as? String
        currently_working = dictionary["currently_working"] as? Int
        organisation_id = dictionary["organisation_id"] as? Int
        start_date_year = dictionary["start_date_year"] as? Int
        city = dictionary["city"] as? String
        end_date_month = dictionary["end_date_month"] as? Int
        end_date_year = dictionary["end_date_year"] as? Int
        job_type = dictionary["job_type"] as? String
        industry_type_id = dictionary["industry_type_id"] as? Int
        user_id = dictionary["user_id"] as? Int
//        team = dictionary["team"] as? String
        industry_type = dictionary["industry_type"] as? String
//        sub_department = dictionary["sub_department"] as? String
        user_organisation_id = dictionary["user_organisation_id"] as? Int
//        department = dictionary["department"] as? String
    }
	required public init?(dictionary: [String:JSON]) {

        user_organisation_id = dictionary["user_organisation_id"]?.int
        user_id = dictionary["user_id"]?.int
        designation_id = dictionary["designation_id"]?.int
        industry_type_id = dictionary["industry_type_id"]?.int
        job_type_id = dictionary["job_type_id"]?.int
        organisation_id = dictionary["organisation_id"]?.int
        start_date_month = dictionary["start_date_month"]?.int
        start_date_year = dictionary["start_date_year"]?.int
        end_date_month = dictionary["end_date_month"]?.int
        end_date_year = dictionary["end_date_year"]?.int
        designation = dictionary["designation"]?.string
        industry_type = dictionary["industry_type"]?.string
        job_type = dictionary["job_type"]?.string
        organisation = dictionary["organisation"]?.string
        city = dictionary["city"]?.string
        currently_working = dictionary["currently_working"]?.int
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.user_organisation_id, forKey: "user_organisation_id")
        dictionary.setValue(self.user_id, forKey: "user_id")
        dictionary.setValue(self.designation_id, forKey: "designation_id")
        dictionary.setValue(self.industry_type_id, forKey: "industry_type_id")
        dictionary.setValue(self.organisation_id, forKey: "organisation_id")
        dictionary.setValue(self.start_date_month, forKey: "start_date_month")
        dictionary.setValue(self.start_date_year, forKey: "start_date_year")
        dictionary.setValue(self.end_date_month, forKey: "end_date_month")
        dictionary.setValue(self.end_date_year, forKey: "end_date_year")
        dictionary.setValue(self.designation, forKey: "designation")
        dictionary.setValue(self.industry_type, forKey: "industry_type")
        dictionary.setValue(self.organisation, forKey: "organisation")
        dictionary.setValue(self.city, forKey: "city")
        dictionary.setValue(self.currently_working, forKey: "currently_working")

		return dictionary
	}

}
