import Foundation
import SwiftyJSON

public class InterestModel {
    public var user_interest_id : Int?
    public var score : Int?
    public var interest_id : Int?
    public var interest : String?
    public var _id : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [InterestModel]
    {
        var models:[InterestModel] = []
        for item in array
        {
            if let dict = item as? [String:Any] {
                models.append(InterestModel(dictionary: dict)!)
            } else {
                models.append(InterestModel(dictionary: item as! [String:JSON])!)
            }
        }
        return models
    }

    required public init?(dictionary: [String:JSON]) {

        user_interest_id = dictionary["user_interest_id"]?.int
        score = dictionary["score"]?.int
        interest_id = dictionary["interest_id"]?.int
        interest = dictionary["interest"]?.string
        _id = dictionary["_id"]?.string
    }
    required public init?(dictionary: [String:Any]) {
        user_interest_id = dictionary["user_interest_id"] as? Int
        score = dictionary["score"] as? Int
        interest_id = dictionary["interest_id"] as? Int
        interest = dictionary["interest"] as? String
        _id = dictionary["_id"] as? String
    }
        
    public func dictionaryRepresentation() -> [String:Any] {

        var dictionary = [String:Any]()
        
        dictionary["interest_id"] = self.interest_id
        dictionary["score"] = self.score
        dictionary["_id"] = self._id

        return dictionary
    }

}

