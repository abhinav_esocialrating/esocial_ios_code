//
//  ExperienceViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ExperienceViewModel: NSObject {
    var businessDetails = [["Services":"Wedding Photographer +2"], ["Business Location":"Mumbai"], ["Working Hours": "18:00 - 22:00"]]

    var experienceDataSource = [ExperienceModel]()
    var skills = [SkillModel]()
    var businessServices = [BusinessService]()
    
    func getExperience(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getExperience(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getExperience")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                self.experienceDataSource = []
                let data = json["data"].dictionaryValue
                let experience = data["experience"]
                self.saveJsonToUserDefaults(json: experience ?? JSON())
                let list = experience?.arrayValue ?? []
                for i in list {
                    let edu = ExperienceModel(dictionary: i.dictionaryValue)
                    self.experienceDataSource.append(edu!)
                }
                let total = data["total"]?.dictionaryValue
                let str = "\(total?["years"]?.stringValue ?? "0") years \(total?["months"]?.stringValue ?? "0") months"
                UserDefaults.standard.TotalExperience = str
                completion(true, json, "")
            
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func getBusinessInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getBusinessInfo(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getBusinessInfo")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let serviceArray = jsonData["data"] as! NSArray
                    self.businessServices = []
                    self.businessServices = BusinessService.modelsFromDictionaryArray(array: serviceArray)
                } catch {
                    
                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func saveBusinessInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.saveBusinessInfo(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    // MARK: - Utility methods
    func saveJsonToUserDefaults(json: JSON) {
        UserDefaults.standard.ExperienceList = json.rawString() ?? "[]"
    }
    func loadLocalData(completion: (_ success: Bool) -> Void) {
        let p = UserDefaults.standard.ExperienceList
        if p != "" {
            if let json = p.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                do {
                    experienceDataSource = []
                    let json = try JSON(data: json)
                    let educationList = json.arrayValue
                    for i in educationList {
                        let edu = ExperienceModel(dictionary: i.dictionaryValue)
                        self.experienceDataSource.append(edu!)
                    }
                    completion(true)
                } catch {
                    completion(false)
                }
            } else {
                completion(false)
            }
        } else {
            completion(false)
        }
    }
    func isBusinessServicesEmpty() -> Bool {
        var count = 0
        for i in businessServices {
            if (i.service?.count ?? 0) > 0 {
                count += 1
                return false
            }
        }
        return true
    }
}
extension ExperienceViewModel {
    // MARK: - Skills
    func getAllSkills(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getAllSkills(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.skills = []
                self.skills = data.map({ (item) -> SkillModel in
                    let skill = SkillModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
