//
//  ExperienceHeaderCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ExperienceHeaderCell: UITableViewCell {
    
    
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var lowerView: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var editImage: UIImageView!
    @IBOutlet weak var totalExperienceLabel: UILabel!
    
    var vc = 0  // 1: Education  2: Experience  3: EducationList
    var editTapped: ((_ index: Int) -> Void)? = nil
    var headerTapped: ((_ index: Int) -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func editButtonTapped(_ sender: Any) {
        editTapped?(indexPath.section)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
//        headerTapped?(indexPath.section)
    }
}
