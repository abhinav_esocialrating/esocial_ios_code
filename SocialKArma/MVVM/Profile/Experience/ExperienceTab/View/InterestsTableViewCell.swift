//
//  InterestsTableViewCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 17/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class InterestsTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var companyLabel: UILabel!
    
    @IBOutlet weak var desigLabel: UILabel!
    
    @IBOutlet weak var industryLabel: UILabel!
    
    @IBOutlet weak var jobTypeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var bkgView: UIView!
    
    @IBOutlet weak var lowerView: UIView!
    var editTapped: ((_ index: Int) -> Void)? = nil
    var experience: ExperienceModel! {
        didSet {
            setValues()
        }
    }
    weak var viewModel: ExperienceViewModel?
    weak var listViewModel: EducationListViewModel?
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        if let viewModel = viewModel {
            if indexPath.row == 0 && viewModel.experienceDataSource.count == 1 {
                bkgView.roundCorners(corners: [.allCorners], radius: 8)
            } else if indexPath.row == 0 {
                bkgView.roundCorners(corners: [.topLeft, .topRight], radius: 8)
            } else if indexPath.row == viewModel.experienceDataSource.count - 1 {
                bkgView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
            } else {
                bkgView.roundCorners(corners: [.allCorners], radius: 0)
            }
        } else if let viewModel = listViewModel {
            if indexPath.row == 0 && viewModel.experienceDataSource.count == 1 {
                bkgView.roundCorners(corners: [.allCorners], radius: 8)
            } else if indexPath.row == 0 {
                bkgView.roundCorners(corners: [.topLeft, .topRight], radius: 8)
            } else if indexPath.row == viewModel.experienceDataSource.count - 1 {
                bkgView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
            } else {
                bkgView.roundCorners(corners: [], radius: 0)
            }
        } else {
            bkgView.roundCorners(corners: [], radius: 0)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Actions
    
    @IBAction func editTapped(_ sender: Any) {
        editTapped?(indexPath.row)
    }
    func setValues() {
        companyLabel.text = experience.organisation
        desigLabel.text = experience.designation
        industryLabel.text = experience.industry_type
        jobTypeLabel.text = experience.job_type
        cityLabel.text = experience.city

        let startMonth = experience?.start_date_month
        let startMonthStr = (startMonth ?? 0) < 10 ? "0\(startMonth ?? 0)" : "\(startMonth ?? 0)"
        let endMonth = experience?.end_date_month
        let endMonthStr = (endMonth ?? 0) < 10 ? "0\(endMonth ?? 0)" : "\(endMonth ?? 0)"
        let startDateFull = "\(startMonthStr)/\(experience.start_date_year ?? 2000)"
        var endDateFull = ""
        if let currently_working = experience.currently_working, currently_working == 1 {
            endDateFull = "Current"
        } else {
            endDateFull = "\(endMonthStr)/\(experience.end_date_year ?? 2004)"
        }
        timeLabel.text = "\(startDateFull)" + " - " + "\(endDateFull)"
    }
}
