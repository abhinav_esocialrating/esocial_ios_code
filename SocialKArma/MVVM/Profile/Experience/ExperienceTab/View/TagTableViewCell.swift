//
//  TagTableViewCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class TagTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bkgView: UIView!
    @IBOutlet weak var tagView: TagListView!
    @IBOutlet weak var showBtn: UIButton!
    // MARK: - Variables
    var skills: [SkillModel]?
    var interests: [InterestModel]?
    var showSeeAllBtn: Bool?
    var handleSeeMore: completionBlock? = nil
    var isBusinessCell = false
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        configureTagView()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        if !isBusinessCell {
            tagView.layoutSubviews()
            setTags()
            tagView.layoutSubviews()
            if let showSeeAllBtn = showSeeAllBtn {
                showBtn.layoutSubviews()
                showBtn.setTitle(showSeeAllBtn ? "See all interests" : "", for: .normal)
            }
        }
    }
    func configureView() {
        bkgView.layer.cornerRadius = 8
    }
    func configureTagView() {
        tagView.delegate = self
        tagView.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
    }
    // MARK: - Methods
    func setTags() {
        tagView.removeAllTags()
        if let skillssdsd = skills {
            skillssdsd.forEach { (skill) in
                tagView.addTag(skill.skill ?? "")
            }
        }
        if let skillssdsd = interests {
            if skillssdsd.count > 5 {
                for i in 0..<5 {
                    tagView.addTag(skillssdsd[i].interest ?? "")
                }
//                seeAllBtn.setTitle("See all interests", for: .normal)
            } else {
                skillssdsd.forEach { (skill) in
                    tagView.addTag(skill.interest ?? "")
                }
//                seeAllBtn.setTitle("", for: .normal)
            }
        }
    }
    func setServicesTags(services: [BusinessService]) {
        tagView.removeAllTags()
        services.forEach { (skill) in
            if (skill.service?.count ?? 0) > 0 {
                tagView.addTag(skill.service?[0].business_service ?? "")
            }
        }
    }
    // MARK: - Actions
    @IBAction func seeAllTapped(_ sender: Any) {
        handleSeeMore?()
    }
}

extension TagTableViewCell: TagListViewDelegate {
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
//        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
    }
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
//        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}
