//
//  SkillTableViewCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 03/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import FloatRatingView

class SkillTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var skillLabel: UILabel!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var ratingWidth: NSLayoutConstraint!
    var delete: ((_ index: IndexPath) -> Void)? = nil
    var viewModel: SkillsViewModel! {
        didSet {
            setValues()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ratingView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues() {
        if viewModel.screenType == 1 {
            skillLabel.text = viewModel.skills[indexPath.row].skill
            ratingView.rating = Double(viewModel.skills[indexPath.row].score ?? 1)
        } else {
            skillLabel.text = viewModel.interests[indexPath.row].interest
            ratingView.rating = Double(viewModel.interests[indexPath.row].score ?? 1)
        }
    }
    
    // MARK: - Actions
    @IBAction func deleteTapped(_ sender: Any) {
        delete?(indexPath)
    }
    
    
}

extension SkillTableViewCell: FloatRatingViewDelegate {
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        if viewModel.screenType == 1 {
            viewModel.skills[indexPath.row].score = Int(rating)
        } else {
            viewModel.interests[indexPath.row].score = Int(rating)
        }
    }
}
