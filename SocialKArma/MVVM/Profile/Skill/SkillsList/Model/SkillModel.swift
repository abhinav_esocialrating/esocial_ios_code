
import Foundation
import SwiftyJSON
 

public class SkillModel {
	public var user_skill_id : Int?
	public var score : Int?
	public var skill_id : Int?
	public var skill : String?
    public var _id : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [SkillModel]
    {
        var models:[SkillModel] = []
        for item in array
        {
            if let dict = item as? [String:Any] {
                models.append(SkillModel(dictionary: dict)!)
            } else {
                models.append(SkillModel(dictionary: item as! [String:JSON])!)
            }
        }
        return models
    }

    required public init?(dictionary: [String:JSON]) {

        user_skill_id = dictionary["user_skill_id"]?.int
        score = dictionary["score"]?.int
        skill_id = dictionary["skill_id"]?.int
        skill = dictionary["skill"]?.string
        _id = dictionary["_id"]?.string
	}
    
    required public init?(dictionary: [String:Any]) {
        user_skill_id = dictionary["user_skill_id"] as? Int
        score = dictionary["score"] as? Int
        skill_id = dictionary["skill_id"] as? Int
        skill = dictionary["skill"] as? String
        _id = dictionary["_id"] as? String
        
    }

		
    public func dictionaryRepresentation() -> [String:Any] {

		var dictionary = [String:Any]()
        
        dictionary["skill_id"] = self.skill_id
        dictionary["score"] = self.score
        dictionary["_id"] = self._id

		return dictionary
	}

}
