//
//  SkillsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 03/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class SkillsViewController: UIViewController {

    @IBOutlet weak var skillTableView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var lnfoLabel: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var searchBottomView: UIView!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    
    var screenType = 0  // 1: Skill   2: Interest
    let viewModel = SkillsViewModel()
    let dropDown = DropDown()
    var userType = 1  // 1: Self   2: Other User
    var otherUserId = 0
    var skillsTypes = [SkillTypes]()

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModel.screenType = screenType
        configureView()
        screenType == 1 ? getSkillIntoOneList() : getAllInterests()
        if screenType == 1 {
            self.updateUI(section: 0)
        }
        tabBarController?.tabBar.isHidden = true
    }
    func configureView() {
        skillTableView.register(UINib(nibName: TableViewCells.SkillTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.SkillTableViewCell.rawValue)
        skillTableView.register(UINib(nibName: TableViewCells.SkillHeaderTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.SkillHeaderTableViewCell.rawValue)
        skillTableView.backgroundColor = .clear
        searchTF.addTarget(self, action: #selector(searchTextChanged(_:)), for: .editingChanged)
        searchTF.placeholder = screenType == 1 ? "Add Skill..." : "Add Interest..."
        labelTitle.text = screenType == 1 ? "My Skills" : "My Interests"
        lnfoLabel.text = screenType == 1 ? "Choose your Skills here and showcase your skill level to find relevent opportunities" : "Choose your Interests here and showcase your interest level to find similar good people"
        
//        saveBtn.isHidden = userType == 1 ? false : true
        searchBottomView.isHidden = userType == 1 ? false : true
        searchViewHeight.constant = userType == 1 ? 40 : 0
        
        skillTableView.layer.cornerRadius = 12
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.lightGray, opacity: 0.6, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
    }

    // MARK: - Actions
    @IBAction func addTapped(_ sender: Any) {
        if screenType == 1 {
            let skills = viewModel.skills.map { (skill) -> [String:Any] in
                return skill.dictionaryRepresentation()
            }
            let params = ["skill": skills]
            print(params)
            if skills.count > 0 {
                self.showHud()
                viewModel.saveSkills(parameters: params) { (success, json, message) in
                    self.hideHud()
                    if success {
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
//                            self.dismiss(animated: true, completion: nil)
                        }
                        NotificationCenter.default.post(name: Notification.Name.Profile.SkillsWithTypes, object: self)
                    }
                }
            } else {
                self.navigationController?.popViewController(animated: true)
//                self.dismiss(animated: true, completion: nil)
            }
//            self.deleteSkills(skillId: 0)
        } else {
            let skills = viewModel.interests.map { (skill) -> [String:Any] in
                return skill.dictionaryRepresentation()
            }
            let params = ["interest": skills]
            print(params)
            if skills.count > 0 {
                self.showHud()
                viewModel.saveInterests(parameters: params) { (success, json, message) in
                    self.hideHud()
                    if success {
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
//                            self.dismiss(animated: true, completion: nil)
                        }
                        NotificationCenter.default.post(name: Notification.Name.Profile.Skills, object: self)
                    }
                }
            } else {
                self.navigationController?.popViewController(animated: true)
//                self.dismiss(animated: true, completion: nil)
            }
//            self.deleteInterests(skillId: 0)
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    @objc func searchTextChanged(_ textField: UITextField) {
        viewModel.isSelectedFromDropDown = false
        let params = screenType == 1 ? ["config": "skill", "search": textField.text!] : ["config": "interest", "search": textField.text!]
        viewModel.getSearchConfig(parameters: params) { (success, json, message) in
            if success {
                if self.viewModel.searchedData.count > 0 {
                    self.showDropDown(textField: textField)
                } else {
                    self.dropDown.hide()
                }
            }
        }
    }
    // MARK: - Helper methods
    func updateUI(section: Int) {
        DispatchQueue.main.async {
            self.searchTF.text = ""
            self.skillTableView.reloadData()
            if self.screenType == 1 {
                self.lnfoLabel.isHidden = self.viewModel.skills.count == 0 ? true : false
            } else {
                self.lnfoLabel.isHidden = self.viewModel.interests.count == 0 ? true : false
            }
        }
    }
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.dataSource = viewModel.dropDownData
        dropDown.selectionAction = { (index: Int, item: String) in
            self.viewModel.isSelectedFromDropDown = true
            if self.screenType == 1 {
                if !self.viewModel.addSkill(data: self.viewModel.searchedData[index]) {
                    self.alert(message: "Skill Already selected!")
                }
            } else {
                if !self.viewModel.addInterest(data: self.viewModel.searchedData[index]) {
                    self.alert(message: "Interest Already selected!")
                }
            }
            self.updateUI(section: 0)
        }
        dropDown.show()
    }
    func getSkillIntoOneList() {
        skillsTypes.forEach({ viewModel.skills.append(contentsOf: $0.skills ?? []) })
    }
}
// MARK: -  Textfield methods
extension SkillsViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if !viewModel.isSelectedFromDropDown && searchTF.text != "" {
            let params = ["master": screenType == 1 ? "skill" : "interest", "value": textField.text!]
            viewModel.addToMasterTable(parameters: params) { (success, dict, message) in
                if success {
                    if self.screenType == 1 {
                        self.viewModel.addSkill(data: dict)
                    } else {
                        self.viewModel.addInterest(data: dict)
                    }
                    self.skillTableView.reloadData()
                    self.searchTF.text = ""
                }
            }
        }
    }
}
// MARK: - TableView
extension SkillsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return screenType == 1 ? viewModel.skills.count : viewModel.interests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.SkillTableViewCell.rawValue, for: indexPath) as! SkillTableViewCell
        cell.indexPath = indexPath
        cell.viewModel = viewModel
        cell.delete = { index in
            if self.screenType == 1 {
                if let user_skill_id = self.viewModel.skills[indexPath.row].user_skill_id {
//                    self.viewModel.skills.remove(at: index.row)
//                    self.updateUI(section: 0)
                    if user_skill_id != 0 {
//                        self.viewModel.toDelete.append(user_skill_id)
                        self.deleteSkills(skillId: user_skill_id)
                    } else {
                        self.viewModel.skills.remove(at: index.row)
                        self.updateUI(section: 0)
                    }
                } else {
                    self.viewModel.skills.remove(at: index.row)
                    self.updateUI(section: 0)
                }
            } else {
                if let user_skill_id = self.viewModel.interests[indexPath.row].user_interest_id {
//                    self.viewModel.interests.remove(at: index.row)
//                    self.updateUI(section: 0)
                    if user_skill_id != 0 {
//                        self.viewModel.toDelete.append(user_skill_id)
                        self.deleteInterests(skillId: user_skill_id)
                    } else {
                        self.viewModel.interests.remove(at: index.row)
                        self.updateUI(section: 0)
                    }
                } else {
                    self.viewModel.interests.remove(at: index.row)
                    self.updateUI(section: 0)
                }
            }
        }
        return cell
    }

}

// MARK: - Web services
extension SkillsViewController {
    func getAllSkills() {
        //        showHudToWindow()
        let parameters = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        viewModel.getAllSkills(parameters: parameters) { (success, json, message) in
            //            self.hideHud()
            if success {
                self.updateUI(section: 1)
            } else {
                // show message
            }
        }
    }
    
    func deleteSkills(skillId: Int) {
        //        showHudToWindow()
        let params = ["skill": [skillId]]//viewModel.toDelete]
        viewModel.deleteSkills(parameters: params) { (success, json, message) in
            //            self.hideHud()
            if success {
                self.updateUI(section: 0)
            } else {
                // show message
            }
        }
    }

    // Interests
    func getAllInterests() {
        let parameters = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        viewModel.getAllInterests(parameters: parameters) { (success, json, message) in
            //            self.hideHud()
            if success {
                self.updateUI(section: 0)
            } else {
                // show message
            }
        }
    }
    func deleteInterests(skillId: Int) {
        //        showHudToWindow()
        let params = ["interest": [skillId]]//viewModel.toDelete]
        viewModel.deleteInterests(parameters: params) { (success, json, message) in
            //            self.hideHud()
            if success {
                self.updateUI(section: 0)
            } else {
                // show message
            }
        }
    }
    
}
