//
//  SkillsViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 03/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class SkillsViewModel: NSObject {
    
    var selectedSkills = [String]()
    var searchedData = [[String:Any]]()
    var dropDownData = [String]()
    var skills = [SkillModel]()
    var interests = [InterestModel]()
    var screenType = 0  // 1: Skill   2: Interest
    var isSelectedFromDropDown = false
    var toDelete = [Int]()
    // MARK: - Methods
    func addSkill(data: [String:Any]) -> Bool {
        if let id = data["id"] as? Int {
            let duplicates = skills.filter({ $0.skill_id == id })
            if duplicates.count > 0 {
                return false
            }
        }
        var dict = [String: Any]()
        dict = ["skill_id": data["id"] ?? 0, "score": 1, "skill": data["value"] ?? "qwerty", "user_skill_id": 0]
        let mySkill = SkillModel(dictionary: dict)!
        skills.append(mySkill)
        return true
    }
    func addInterest(data: [String:Any]) -> Bool {
        if let id = data["id"] as? Int {
            let duplicates = interests.filter({ $0.interest_id == id })
            if duplicates.count > 0 {
                return false
            }
        }
        var dict = [String: Any]()
        dict = ["interest_id": data["id"] ?? 0, "score": 1, "interest": data["value"] ?? "qwerty", "user_interest_id": 0]
        let mySkill = InterestModel(dictionary: dict)!
        interests.append(mySkill)
        return true
    }
    
    // MARK: - Search suggestions
    func getSearchConfig(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseSearchedData(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func addToMasterTable(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ data: [String:Any], _ message: String) -> Void)) {
        NetworkManager.addToMasterTable(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].dictionaryObject
                completion(true, data ?? [:], "")
            default:
                let message = json["message"].stringValue
                completion(false, [:], message)
            }
        }
    }
    // MARK: - Utility methods
    func parseSearchedData(data: [JSON]) {
        let search = data.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["id"] = json["id"].intValue
            dict["value"] = json["value"].string
            return dict
        }
        searchedData = search
        let drop = search.map { (dict) -> String in
            return (dict["value"] as? String) ?? ""
        }
        dropDownData = drop
    }
    // MARK: - Skills
    func getAllSkills(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getAllSkills(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.skills = []
                self.skills = data.map({ (item) -> SkillModel in
                    let skill = SkillModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func saveSkills(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.saveSkills(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let dataDict = json["data"]
                let data = dataDict["data"].arrayValue
                self.skills = []
                self.skills = data.map({ (item) -> SkillModel in
                    let skill = SkillModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                if let points = dataDict["tzPoints"].int {
                    PointsHandler.shared.updateTZPoints(points: points, changedToBeShown: true)
                }
                completion(true, json, "")
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func deleteSkills(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.deleteSkills(parameters: parameters, method: .delete) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.skills = []
                self.skills = data.map({ (item) -> SkillModel in
                    let skill = SkillModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                completion(true, json, "")
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    // MARK: - Interests
    func getAllInterests(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getAllInterests(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.interests = []
                self.interests = data.map({ (item) -> InterestModel in
                    let skill = InterestModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func saveInterests(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.saveInterests(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let dataDict = json["data"]
                let data = dataDict["data"].arrayValue
                self.interests = []
                self.interests = data.map({ (item) -> InterestModel in
                    let skill = InterestModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                if let points = dataDict["tzPoints"].int {
                    PointsHandler.shared.updateTZPoints(points: points, changedToBeShown: true)
                }
                completion(true, json, "")
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func deleteInterests(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.deleteInterests(parameters: parameters, method: .delete) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.interests = []
                self.interests = data.map({ (item) -> InterestModel in
                    let skill = InterestModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                completion(true, json, "")
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
