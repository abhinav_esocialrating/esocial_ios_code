import Foundation
import SwiftyJSON

public class EducationModel {
    public var user_organisation_id : Int?
    public var user_id : Int?
    public var degree_id : Int?
    public var degree_field_id : Int?
    public var organisation_id : Int?
    public var start_date : Int?
    public var end_date : Int?
    public var section : String?
    public var degree : String?
    public var degree_field : String?
    public var organisation : String?
    public var city : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [EducationModel]
    {
        var models:[EducationModel] = []
        for item in array
        {
            if let item = item as? NSDictionary {
                models.append(EducationModel(dictionary: item)!)
            }
        }
        return models
    }

    required public init?(dictionary: [String:JSON]) {

        user_organisation_id = dictionary["user_organisation_id"]?.int
        user_id = dictionary["user_id"]?.int
        degree_id = dictionary["degree_id"]?.int
        degree_field_id = dictionary["degree_field_id"]?.int
        organisation_id = dictionary["organisation_id"]?.int
        start_date = dictionary["start_date"]?.int
        end_date = dictionary["end_date"]?.int
        section = dictionary["section"]?.string
        degree = dictionary["degree"]?.string
        degree_field = dictionary["degree_field"]?.string
        organisation = dictionary["organisation"]?.string
        city = dictionary["city"]?.string
    }
    required public init?(dictionary: NSDictionary) {

//        degree_level = dictionary["degree_level"] as? String
        organisation = dictionary["organisation"] as? String
        degree_field_id = dictionary["degree_field_id"] as? Int
//        degree_level_id = dictionary["degree_level_id"] as? Int
        organisation_id = dictionary["organisation_id"] as? Int
        degree = dictionary["degree"] as? String
        city = dictionary["city"] as? String
        degree_field = dictionary["degree_field"] as? String
        end_date = dictionary["end_date"] as? Int
        user_id = dictionary["user_id"] as? Int
//        _id = dictionary["_id"] as? String
        section = dictionary["section"] as? String
        start_date = dictionary["start_date"] as? Int
        degree_id = dictionary["degree_id"] as? Int
        user_organisation_id = dictionary["user_organisation_id"] as? Int
    }
        
    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.user_organisation_id, forKey: "user_organisation_id")
        dictionary.setValue(self.user_id, forKey: "user_id")
        dictionary.setValue(self.degree_id, forKey: "degree_id")
        dictionary.setValue(self.degree_field_id, forKey: "degree_field_id")
        dictionary.setValue(self.organisation_id, forKey: "organisation_id")
        dictionary.setValue(self.start_date, forKey: "start_date")
        dictionary.setValue(self.end_date, forKey: "end_date")
        dictionary.setValue(self.section, forKey: "section")
        dictionary.setValue(self.degree, forKey: "degree")
        dictionary.setValue(self.degree_field, forKey: "degree_field")
        dictionary.setValue(self.organisation, forKey: "organisation")
        dictionary.setValue(self.city, forKey: "city")

        return dictionary
    }

}
