//
//  EducationListCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class EducationListCell: UITableViewCell {
    
    @IBOutlet weak var collegeLabel: UILabel!
    
    @IBOutlet weak var fieldOfStudyLabel: UILabel!
    
    @IBOutlet weak var courseLabel: UILabel!
    
    @IBOutlet weak var yearsLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var bkgView: UIView!
    @IBOutlet weak var lowerView: UIView!

    var editTapped: ((_ index: Int) -> Void)? = nil
    var education: EducationModel! {
        didSet {
            setValues()
        }
    }
    weak var viewModel: EducationViewModel?
    weak var listViewModel: EducationListViewModel?
    // MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        superview?.layoutSubviews()
        contentView.layoutSubviews()
        if let viewModel = viewModel {
            if indexPath.row == 0 && viewModel.educationDataSource.count == 1 {
                bkgView.roundCorners(corners: [.allCorners], radius: 8)
            } else if indexPath.row == 0 {
                bkgView.roundCorners(corners: [.topLeft, .topRight], radius: 8)
            } else if indexPath.row == viewModel.educationDataSource.count - 1 {
                bkgView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
            } else {
                bkgView.roundCorners(corners: [.allCorners], radius: 0)
            }
        } else if let viewModel = listViewModel {
            if indexPath.row == 0 && viewModel.educationDataSource.count == 1 {
                bkgView.roundCorners(corners: [.allCorners], radius: 8)
            } else if indexPath.row == 0 {
                bkgView.roundCorners(corners: [.topLeft, .topRight], radius: 8)
            } else if indexPath.row == viewModel.educationDataSource.count - 1 {
                bkgView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 8)
            } else {
                bkgView.roundCorners(corners: [.allCorners], radius: 0)
            }
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func editTapped(_ sender: Any) {
        editTapped?(indexPath.row)
    }
    func setValues() {
        collegeLabel.text = education.organisation
        fieldOfStudyLabel.text = education.degree_field
        courseLabel.text = education.degree
        let startYr = education.start_date ?? 2000
        let endYr = education.end_date ?? 2004
        yearsLabel.text = "\(startYr)" + " - " + "\(endYr)"
        locationLabel.text = education.city
    }
}
