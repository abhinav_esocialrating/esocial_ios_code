//
//  EducationViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class EducationViewModel: NSObject {
    
    var educationDataSource = [EducationModel]()
    
    // MARK: - Web Services
    func getEducation(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getEducation(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getEducation")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                self.educationDataSource = []
//                self.saveJsonToUserDefaults(json: json["data"])
                let educationList = json["data"].arrayValue
                for i in educationList {
                    let edu = EducationModel(dictionary: i.dictionaryValue)
                    self.educationDataSource.append(edu!)
                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func loadLocalData(completion: (_ success: Bool) -> Void) {
        let p = UserDefaults.standard.EducationList
        if p != "" {
            if let json = p.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                do {
                    educationDataSource = []
                    let json = try JSON(data: json)
                    let educationList = json.arrayValue
                    for i in educationList {
                        let edu = EducationModel(dictionary: i.dictionaryValue)
                        self.educationDataSource.append(edu!)
                    }
                    completion(true)
                } catch {
                    completion(false)
                }
            } else {
                completion(false)
            }
        } else {
            completion(false)
        }
    }
    
    
    // MARK: - Utility methods
    func saveJsonToUserDefaults(json: JSON) {
        UserDefaults.standard.EducationList = json.rawString() ?? "[]"
    }
}
