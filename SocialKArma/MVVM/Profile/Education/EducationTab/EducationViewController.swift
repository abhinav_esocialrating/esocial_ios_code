//
//  EducationViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
//import SJSegmentedScrollView

class EducationViewController: UIViewController {
    
    @IBOutlet weak var educationTableView: UITableView!
    
    @IBOutlet weak var noDataLabel: UILabel!
    
    // MARK: - Variables
    var viewModel = EducationViewModel()
    var userType = 1  // 1: Self   2: Other User
    var otherUserId = 0
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if userType == 1 {
//            loadLocalData()
//        }
        getEducation()
    }
    func configureView() {
        educationTableView.register(UINib(nibName: TableViewCells.EducationListCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.EducationListCell.rawValue)
        educationTableView.register(UINib(nibName: TableViewCells.ExperienceHeaderCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.ExperienceHeaderCell.rawValue)
        educationTableView.backgroundColor = .clear
        educationTableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    }
    func openEducationList() {
        if viewModel.educationDataSource.count > 0 {
            let vc = EducationListViewController.instantiate(fromAppStoryboard: .Profile)
            vc.viewModel.screenType = 1
            navigationController?.pushViewController(vc, animated: true)
//            vc.modalPresentationStyle = .fullScreen
//            present(vc, animated: true, completion: nil)
        } else {
            let vc = EditEducationViewController.instantiate(fromAppStoryboard: .Profile)
            vc.viewModel.screenType = 1
            vc.viewModel.addEditOrDelete = 1
            navigationController?.pushViewController(vc, animated: true)
//            vc.modalPresentationStyle = .fullScreen
//            present(vc, animated: true, completion: nil)
        }
    }
    
    // MARK: - Methods
    func loadLocalData() {
        viewModel.loadLocalData { (success) in
            if success {
                self.updateUI()
            }
        }
    }
    func updateUI() {
        DispatchQueue.main.async {
            if self.userType == 2 {
                self.educationTableView.isHidden = self.viewModel.educationDataSource.count > 0 ? false : true
                self.noDataLabel.isHidden = self.viewModel.educationDataSource.count > 0 ? true : false
            } else {
                self.noDataLabel.isHidden = true
                self.educationTableView.isHidden = false
            }
            self.educationTableView.reloadData()
        }
    }
}
// MARK: - TableView methods
extension EducationViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.educationDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.EducationListCell.rawValue, for: indexPath) as! EducationListCell
        cell.indexPath = indexPath
        cell.viewModel = viewModel
        cell.editButton.isHidden = true
        cell.education = viewModel.educationDataSource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension // indexPath.section != 0 ? 140 : UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ExperienceHeaderCell.rawValue) as! ExperienceHeaderCell
        cell.editImage.isHidden = userType == 1 ? false : true
        cell.indexPath = IndexPath(row: 0, section: section)
        cell.upperView.isHidden = true
        cell.vc = 0
        cell.label.text = "Education"
        cell.editButton.isHidden = userType == 1 ? false : true
        cell.editTapped = { index in
            self.openEducationList()
        }
        return cell
    }
}
// MARK: - Web Services
extension EducationViewController {
    func getEducation() {
        //        showHudToWindow()
        let params: [String:Any] = ["id": userType == 1 ? UserDefaults.standard.userId : otherUserId]
        viewModel.getEducation(parameters: params) { (success, json, message) in
            //            self.hideHud()
            if success {
                self.updateUI()
            } else {
//                 show message
            }
        }
    }
}
//// MARK: - SJSegmentedView
//extension EducationViewController: SJSegmentedViewControllerViewSource {
//    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
//        return educationTableView
//    }
//}
