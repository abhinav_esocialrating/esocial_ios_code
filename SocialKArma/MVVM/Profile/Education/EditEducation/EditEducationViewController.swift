//
//  EditEducationViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 02/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class EditEducationViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var startDateTF: ACFloatingTextfield!
    @IBOutlet weak var endDateTF: ACFloatingTextfield!
    @IBOutlet weak var schoolTF: ACFloatingTextfield!
    @IBOutlet weak var degreeTF: ACFloatingTextfield!
    @IBOutlet weak var fieldOfStudyTF: ACFloatingTextfield!
    
    @IBOutlet weak var jobTypeTF: ACFloatingTextfield!
    @IBOutlet weak var switchView: UIView!
    @IBOutlet weak var currentlyWorkingSwitch: UISwitch!
    @IBOutlet weak var locationTF: ACFloatingTextfield!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var graduationStackView: UIStackView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var dateErrorLabel: UILabel!
    @IBOutlet weak var dateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    
    // MARK: - Variables
    let viewModel = EditEducationViewModel()
    var dateTextField: UITextField!
    let dropDown = DropDown()
    var selectedStartDate: Date?
    var selectedEndDate: Date?
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        setInitialValues()
        tabBarController?.tabBar.isHidden = true
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.lightGray, opacity: 0.6, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
    }
    func configureView() {
        typeLabel.text = viewModel.screenType == 1 ? "Education" : "Experience"
        schoolTF.addTarget(self, action: #selector(schoolTFChanged), for: .editingChanged)
        degreeTF.addTarget(self, action: #selector(degreeTFChanged), for: .editingChanged)
        fieldOfStudyTF.addTarget(self, action: #selector(fieldOfStudyTFChanged), for: .editingChanged)
        locationTF.addTarget(self, action: #selector(locationTFChanged(_:)), for: .editingChanged)
        jobTypeTF.addTarget(self, action: #selector(JobTypeTFChanged(_:)), for: .editingChanged)
        backButton.addTarget(self, action: #selector(backTapped(_:)), for: .touchUpInside)
        titleLabel.text = viewModel.screenType == 1 ? "Edit Education" : "Edit Experience"
        for i in [startDateTF, endDateTF, schoolTF, degreeTF, fieldOfStudyTF, locationTF, jobTypeTF] {
            i?.lineColor = UIColor.gray.withAlphaComponent(0.7)
            i?.placeHolderColor = .gray
            i?.selectedLineColor = UIColor.SearchColors.blue
            i?.selectedPlaceHolderColor = .gray
            i?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
            i?.textColor = UIColor.NewTheme.darkGray
        }
        switch viewModel.screenType {
        case 1:
            schoolTF.placeholder = viewModel.educationPlaceHolders[0]
            degreeTF.placeholder = viewModel.educationPlaceHolders[1]
            fieldOfStudyTF.placeholder = viewModel.educationPlaceHolders[2]
            startDateTF.placeholder = viewModel.educationPlaceHolders[3]
            endDateTF.placeholder = viewModel.educationPlaceHolders[4]
            locationTF.placeholder = viewModel.educationPlaceHolders[5]
            switchView.isHidden = true
        default:
            schoolTF.placeholder = viewModel.experiencePlaceHolders[0]
            degreeTF.placeholder = viewModel.experiencePlaceHolders[1]
            fieldOfStudyTF.placeholder = viewModel.experiencePlaceHolders[2]
            startDateTF.placeholder = viewModel.experiencePlaceHolders[3]
            endDateTF.placeholder = viewModel.experiencePlaceHolders[4]
            locationTF.placeholder = viewModel.experiencePlaceHolders[5]
            jobTypeTF.placeholder = viewModel.experiencePlaceHolders[6]
            graduationStackView.isHidden = true
        }
        deleteButton.isHidden = viewModel.addEditOrDelete == 1 ? true : false
        jobTypeTF.isHidden = viewModel.screenType == 1
    }
    
    func setInitialValues() {
        viewModel.educationDict = [:]
        viewModel.experienceDict = [:]
        switch viewModel.screenType {
        case 1:
            if viewModel.addEditOrDelete != 1 {
                schoolTF.text = viewModel.education?.organisation
                degreeTF.text = viewModel.education?.degree
                fieldOfStudyTF.text = viewModel.education?.degree_field
                startDateTF.text = "\(viewModel.education?.start_date ?? 2000)"
                endDateTF.text = "\(viewModel.education?.end_date ?? 2004)"
                locationTF.text = viewModel.education?.city
                viewModel.educationDict = ["organisation_id": viewModel.education?.organisation_id ?? 0, "degree_id": viewModel.education?.degree_id ?? 0, "degree_field_id": viewModel.education?.degree_field_id ?? 0]
            }
        default:
            if viewModel.addEditOrDelete != 1 {
                schoolTF.text = viewModel.experience?.organisation
                degreeTF.text = viewModel.experience?.designation
                fieldOfStudyTF.text = viewModel.experience?.industry_type
                jobTypeTF.text = viewModel.experience?.job_type
                
                let startMonth = viewModel.experience?.start_date_month
                let startMonthStr = (startMonth ?? 0) < 10 ? "0\(startMonth ?? 0)" : "\(startMonth ?? 0)"
                let endMonth = viewModel.experience?.end_date_month
                let endMonthStr = (endMonth ?? 0) < 10 ? "0\(endMonth ?? 0)" : "\(endMonth ?? 0)"
                startDateTF.text = CommonData.months[startMonth ?? 1]! + ", \(viewModel.experience?.start_date_year ?? 2000)"
                if viewModel.experience?.currently_working == 1 {
                    endDateTF.text = ""
                    enableEndDateTF(enabled: false)
                    currentlyWorkingSwitch.setOn(true, animated: false)
                } else {
                    endDateTF.text = CommonData.months[endMonth ?? 1]! + ", \(viewModel.experience?.end_date_year ?? 2004)"
                    enableEndDateTF(enabled: true)
                    currentlyWorkingSwitch.setOn(false, animated: false)
                }
                
                locationTF.text = viewModel.experience?.city
                viewModel.experienceDict = ["organisation_id": viewModel.experience?.organisation_id ?? 0, "designation_id": viewModel.experience?.designation_id ?? 0, "industry_type_id": viewModel.experience?.industry_type_id ?? 0, "job_type_id": viewModel.experience?.job_type_id ?? 0]
                
                // Date
                let startFullDate = "\(viewModel.experience?.start_date_year ?? 2000)-" + startMonthStr + "-01"
                let endFullDate = "\(viewModel.experience?.end_date_year ?? 2004)-" + endMonthStr + "-01"
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let strt = formatter.date(from: startFullDate)
                let enddte = formatter.date(from: endFullDate)
                selectedStartDate = strt
                selectedEndDate = enddte
            }
        }
    }

    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if RechabilityHandler.shared.reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            saveEducation(delete: false)
        }
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        alertWithTwoAction(message: "Are you sure you want to delete?", okHandler: {
            self.viewModel.addEditOrDelete = 3
            self.deleteEducation()
        }) {}
    }
    @objc func schoolTFChanged(_ textField: UITextField) {
        viewModel.isSchoolFromDropDown = false
        locationTF.isUserInteractionEnabled = true
        if textField.text!.count > 2 {
            var params: [String:Any] = ["organisation": "school", "search": textField.text!]
            if viewModel.screenType == 1 {
                params["edu"] = 1
            }
            viewModel.getOrganisations(parameters: params) { (success, json, message) in
                if success {
                    DispatchQueue.main.async {
                        if self.viewModel.organisationData.count > 0 {
                            self.showDropDown(textField: textField)
                        } else {
                            self.dropDown.hide()
                        }
                    }
                }
            }
        }
    }
    @objc func degreeTFChanged(_ textField: UITextField) {
        viewModel.isDegreeFromDropDown = false
        let params = viewModel.screenType == 1 ? ["config": "degree", "search": textField.text!] : ["config": "designation", "search": textField.text!]
        viewModel.getSearchConfig(parameters: params) { (success, json, message) in
            if success {
                DispatchQueue.main.async {
                    if self.viewModel.searchedData.count > 0 {
                        self.showDropDown(textField: textField)
                    } else {
                        self.dropDown.hide()
                    }
                }
            }
        }
    }
    @objc func fieldOfStudyTFChanged(_ textField: UITextField) {
        viewModel.isFosFromDropDown = false
        let params = viewModel.screenType == 1 ? ["config": "degree_field", "search": textField.text!] : ["config": "industry_type", "search": textField.text!]
        viewModel.getSearchConfig(parameters: params) { (success, json, message) in
            if success {
                DispatchQueue.main.async {
                    if self.viewModel.searchedData.count > 0 {
                        self.showDropDown(textField: textField)
                    } else {
                        self.dropDown.hide()
                    }
                }
            }
        }
    }
    @objc func JobTypeTFChanged(_ textField: UITextField) {
        let params = viewModel.screenType == 1 ? ["config": "job_type", "search": textField.text!] : ["config": "job_type", "search": textField.text!]
        viewModel.getSearchConfig(parameters: params) { (success, json, message) in
            if success {
                DispatchQueue.main.async {
                    if self.viewModel.searchedData.count > 0 {
                        self.showDropDown(textField: textField)
                    } else {
                        self.dropDown.hide()
                    }
                }
            }
        }
    }
    @objc func locationTFChanged(_ textField: UITextField) {
        if textField.text!.count > 2 {
            LocationManager.shared.getCities(query: textField.text!) { (cities, cityNames) in
                self.viewModel.dropDownData = cities
                if cities.count > 0 {
                    DispatchQueue.main.async {
                        self.showDropDown(textField: textField)
                    }
                }
            }
        }
    }
    
    @IBAction func switchChanged(_ sender: Any) {
        if (sender as! UISwitch).isOn {
            enableEndDateTF(enabled: false)
        } else {
            enableEndDateTF(enabled: true)
        }
    }
    // MARK: - Utility methods
    func enableEndDateTF(enabled: Bool) {
        endDateTF.isEnabled = enabled
        endDateTF.alpha = enabled ? 1.0 : 0.3
    }
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.dataSource = viewModel.dropDownData
        
        dropDown.selectionAction = { (index: Int, item: String) in
            textField.text = item
            self.handleDropDownSelection(index: index, item: item, textField: textField)
        }
        dropDown.show()
    }
    func handleDropDownSelection(index: Int, item: String, textField: UITextField) {
        textField.text = item
        switch textField {
        case schoolTF:
            self.viewModel.isSchoolFromDropDown = true
        case degreeTF:
            self.viewModel.isDegreeFromDropDown = true
        case fieldOfStudyTF:
            self.viewModel.isFosFromDropDown = true
        default:
            print("Do nothing")
        }
        self.updateDataForParams(index: index, textField: textField)
    }
    func updateDataForParams(index: Int, textField: UITextField) {
        if viewModel.screenType == 1 {
            switch textField {
            case schoolTF:
                viewModel.educationDict["organisation_id"] = (viewModel.organisationData[index]["organisation_id"] as! Int)
                if let city = viewModel.organisationData[index]["city"] as? String, city != "" {
                    locationTF.text = city
                    locationTF.isUserInteractionEnabled = false
                }
            case degreeTF:
                viewModel.educationDict["degree_id"] = (viewModel.searchedData[index]["id"] as! Int)
            case locationTF:
                print("locationTF changed")
            default:
                viewModel.educationDict["degree_field_id"] = (viewModel.searchedData[index]["id"] as! Int)
            }
        } else {
            switch textField {
            case schoolTF:
                viewModel.experienceDict["organisation_id"] = (viewModel.organisationData[index]["organisation_id"] as! Int)
                if let city = viewModel.organisationData[index]["city"] as? String, city != "" {
                    locationTF.text = city
                    locationTF.isUserInteractionEnabled = false
                }
            case degreeTF:
                viewModel.experienceDict["designation_id"] = (viewModel.searchedData[index]["id"] as! Int)
            case jobTypeTF:
                viewModel.experienceDict["job_type_id"] = (viewModel.searchedData[index]["id"] as! Int)
            case locationTF:
                print("locationTF changed")
            default:
                viewModel.experienceDict["industry_type_id"] = (viewModel.searchedData[index]["id"] as! Int)
            }
        }
    }
    func isValid() -> Bool {
        var valid = true
        var message = ""
        if schoolTF.text!.isEmpty {
            valid = false
            message = viewModel.screenType == 1 ? "Please enter school/college" : "Please enter Company name"
            schoolTF.showErrorWithText(errorText: message)
        } else if degreeTF.text!.isEmpty {
            valid = false
            message = viewModel.screenType == 1 ? "Please enter Degree/Certificate" : "Please enter Title"
            degreeTF.showErrorWithText(errorText: message)
        } else if fieldOfStudyTF.text!.isEmpty {
            valid = false
            message = viewModel.screenType == 1 ? "Please enter field of study" : "Please enter Industry type"
            fieldOfStudyTF.showErrorWithText(errorText: message)
        } else if jobTypeTF.text!.isEmpty && viewModel.screenType == 2 {
            valid = false
            message = viewModel.screenType == 1 ? "Please enter Job Type" : "Please enter Job Type"
            jobTypeTF.showErrorWithText(errorText: message)
        } else if startDateTF.text!.isEmpty {
            valid = false
            message = viewModel.screenType == 1 ? "Please enter start year" : "Please enter start date"
            showDateError(message: message, show: true)
//            startDateTF.showErrorWithText(errorText: message)
        } else if endDateTF.text!.isEmpty && !currentlyWorkingSwitch.isOn {
            valid = false
            message = viewModel.screenType == 1 ? "Please enter end year" : "Please enter end date"
//            endDateTF.showErrorWithText(errorText: message)
            showDateError(message: message, show: true)
        } else if viewModel.screenType == 1 && !validateDate(start: startDateTF.text!, end: endDateTF.text!) {
            valid = false
            message = "Start year must be before or same as end year"
            showDateError(message: message, show: true)
        } else if viewModel.screenType == 2 && !validateDate() {
            valid = false
            message = "Start date must be before or same as end date"
            showDateError(message: message, show: true)
        } else if locationTF.text!.isEmpty {
            valid = false
            message = viewModel.screenType == 1 ? "Please enter location" : "Please enter City"
            locationTF.showErrorWithText(errorText: message)
        } else {
            schoolTF.errorTextColor = .clear
            degreeTF.errorTextColor = .clear
            fieldOfStudyTF.errorTextColor = .clear
            startDateTF.errorTextColor = .clear
            endDateTF.errorTextColor = .clear
            locationTF.errorTextColor = .clear
            showDateError(message: "", show: false)
        }
        return valid
    }
    func validateDate(start: String, end: String) -> Bool {
        if let startYear = Int(start), let endYear = Int(end) {
            if startYear > endYear {
                return false
            }
        } else {
            return false
        }
        return true
    }
    func validateDate() -> Bool {
        if let selectedStartDate = selectedStartDate, let selectedEndDate = selectedEndDate {
            if selectedStartDate > selectedEndDate {
                return false
            }
        } else if let _ = selectedStartDate, currentlyWorkingSwitch.isOn {
            return true
        } else {
            return false
        }
        return true
    }
    func showDateError(message: String, show: Bool) {
        if show {
            dateErrorLabel.text = message
            dateViewHeight.constant = 84
        } else {
            dateErrorLabel.text = ""
            dateViewHeight.constant = 64
        }
    }
    func getDateForAPI(date: Date?) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = .current
        let strt = formatter.string(from: date!)
        return strt
    }
}
// MARK: - TextField delegate
extension EditEducationViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == startDateTF || textField == endDateTF {
            dateTextField = textField
            if viewModel.screenType == 1 {
                openPickerView(textField: textField)
            } else {
                openDatePicker(textField: textField)
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if viewModel.screenType == 1 {
            if textField == startDateTF {
                if textField.text == "" {
                    textField.text = "\(viewModel.years[0])"
                }
            } else if textField == endDateTF {
                if textField.text == "" {
                    textField.text = "\(viewModel.endYear[0])"
                }
            }
        }
    }
    func openDatePicker(textField: UITextField) {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        textField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(handleDatePicker(datePicker:)), for: .valueChanged)
    }
    func openPickerView(textField: UITextField) {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        textField.inputView = pickerView
//        let years = CommonData.years
//        pickerView.selectedRow(inComponent: years.count-1)
    }
    @objc func handleDatePicker(datePicker: UIDatePicker) {
        showDateError(message: "", show: false)
        let date = datePicker.date
        formatDate(date: date)
    }
    func formatDate(date: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = .current
        if dateTextField == startDateTF {
//            startDateTF.text = formatter.string(from: date)
            selectedStartDate = date
        } else {
//            endDateTF.text = formatter.string(from: date)
            selectedEndDate = date
        }
        let monthFormatter = DateFormatter()
        monthFormatter.dateFormat = "LLLL, yyyy"
        monthFormatter.timeZone = .current
        if dateTextField == startDateTF {
            startDateTF.text = monthFormatter.string(from: date)
        } else {
            endDateTF.text = monthFormatter.string(from: date)
        }
    }
}
// MARK: - Web Services
extension EditEducationViewController {
    func deleteEducation() {
        view.endEditing(true)
        var params: [String:Any] = [:]
        let id = viewModel.screenType == 1 ? viewModel.education?.user_organisation_id : viewModel.experience?.user_organisation_id
        params = ["id": id ?? 0]
        viewModel.saveEducation(parameters: params) { (success, json, message) in
            if success {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    func saveEducation(delete: Bool) {
        if isValid() {
            view.endEditing(true)
            var params: [String:Any] = [:]
            if viewModel.screenType == 1 {
                let id = viewModel.education?.user_organisation_id
                let organisation_id = viewModel.isSchoolFromDropDown ? (viewModel.educationDict["organisation_id"] ?? 0) : 0
                let degree_id = viewModel.isDegreeFromDropDown ? (viewModel.educationDict["degree_id"] ?? 0) : 0
                let degree_field_id = viewModel.isFosFromDropDown ? (viewModel.educationDict["degree_field_id"] ?? 0) : 0
                var user_organisation_id = 0
                if viewModel.addEditOrDelete == 1 { /* default value */ }
                else {
                    user_organisation_id = id ?? 0
                }
                params = ["organisation_id":  organisation_id,
                        "degree_field_id": degree_field_id,
                        "degree_id": degree_id,
                        "start_date": "\(startDateTF.text!)-12-01",
                        "end_date": "\(endDateTF.text!)-12-01",
                        "section": "B3",
                        "user_organisation_id": user_organisation_id ]
            } else {
                let id = viewModel.experience?.user_organisation_id
                let organisation_id = viewModel.isSchoolFromDropDown ? (viewModel.experienceDict["organisation_id"] ?? 0) : 0
                let designation_id = viewModel.isDegreeFromDropDown ? (viewModel.experienceDict["designation_id"] ?? 0) : 0
                let industry_type_id = viewModel.isFosFromDropDown ? (viewModel.experienceDict["industry_type_id"] ?? 0) : 0
                let job_type_id = viewModel.isFosFromDropDown ? (viewModel.experienceDict["job_type_id"] ?? 0) : 0
                var user_organisation_id = 0
                if viewModel.addEditOrDelete == 1 {
                    // default value
                } else {
                    user_organisation_id = id ?? 0
                }
                params = ["user_organisation_id": user_organisation_id,
                      "organisation_id": organisation_id,
                      "designation_id": designation_id,
                      "industry_type_id": industry_type_id,
                      "job_type_id": job_type_id,
                      "start_date": getDateForAPI(date: selectedStartDate),
                      "end_date": getDateForAPI(date: currentlyWorkingSwitch.isOn ? Date() : selectedEndDate),
                      "currently_working": currentlyWorkingSwitch.isOn ? 1 : 0]
                    
            }
            if viewModel.isSchoolFromDropDown == false {
                params["organisation"] = schoolTF.text!
            }
            if viewModel.isDegreeFromDropDown == false { // to be edited
                if viewModel.screenType == 1 {
                    params["degree"] = degreeTF.text!
                } else {
                    params["designation"] = degreeTF.text!
                }
            }
            if viewModel.isFosFromDropDown == false { // to be edited
                if viewModel.screenType == 1 {
                    params["degree_field"] = fieldOfStudyTF.text!
                } else {
                    params["industry_type"] = fieldOfStudyTF.text!
                }
            }
            params["city"] = locationTF.text!
            AppDebug.shared.consolePrint(params)
            self.showHud()
            viewModel.saveEducation(parameters: params) { (success, json, message) in
                self.hideHud()
                if success {
                    if self.viewModel.addEditOrDelete == 2 || self.viewModel.addEditOrDelete == 1 {
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
//                            self.dismiss(animated: true, completion: nil)
                        }
                    } else if self.viewModel.addEditOrDelete == 3 {
                        self.alertWithAction(message: "Successfully Deleted!", handler: {
                            self.navigationController?.popViewController(animated: true)
//                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                } else {
                    self.alert(message: message)
                }
            }
        }
    }
}
// MARK: - UIPickerView
extension EditEducationViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return startDateTF.isEditing ? viewModel.years.count : viewModel.endYear.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return startDateTF.isEditing ? "\(viewModel.years[row])" : "\(viewModel.endYear[row])"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        showDateError(message: "", show: false)
        if startDateTF.isEditing {
            startDateTF.text = "\(viewModel.years[row])"
        } else {
            endDateTF.text = "\(viewModel.endYear[row])"
        }
    }
}
