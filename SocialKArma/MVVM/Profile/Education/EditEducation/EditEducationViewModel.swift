//
//  EditEducationViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 02/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class EditEducationViewModel: NSObject {
    let educationPlaceHolders = ["School/College","Degree/Certificate","Field of Study","Start Year","End Year","Location"]
    let experiencePlaceHolders = ["Company name","Designation","Industry type","Start Date","End Date","Location", "Job Type"]
    
    var experienceDict: [String: Int] = ["organisation_id":0, "designation_id": 0, "industry_type_id": 0]
    var educationDict: [String: Int] = ["organisation_id":0, "degree_id":0, "degree_field_id": 0]
    
    var education: EducationModel?
    var experience: ExperienceModel?
    var addEditOrDelete = 1  // 1: Add   2: Edit    3: Delete
    var indexForDeletion = 0
    var screenType = 0  // 1: Education   2: Experience
    var isSchoolFromDropDown = false
    var isDegreeFromDropDown = false
    var isFosFromDropDown = false
    var years: [Int] = CommonData.years.reversed()
    var endYear: [Int] = CommonData.endYear.reversed()
    // Search Data
    var searchedData = [[String:Any]]()
    var dropDownData = [String]()
    var organisationData = [[String:Any]]()
    var cities = [String]()
    // MARK: - Web Services
    func saveEducation(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        var method = HTTPMethod.delete
        switch addEditOrDelete {
        case 1:
            method = .post
        case 2:
            method = .put
        default:
            method = .delete
        }
        if screenType == 1 {
            NetworkManager.saveEducation(parameters: parameters, method: method) { (response) in
                let json = JSON(response.result.value ?? JSON.init())
//                print(json)
                switch response.response?.statusCode {
                case 200:
                    if self.addEditOrDelete == 1 {
                        let data = json["data"].dictionaryValue
//                        let edu = data["data"]?.arrayValue
//                        let dict = edu?[0]
//                        self.updateLocalData(params: [:], postData: dict ?? JSON())
                        if let points = data["tzPoints"]?.intValue {
                            PointsHandler.shared.updateTZPoints(points: points, changedToBeShown: true)
                        }
                    } else {
//                        self.updateLocalData(params: parameters, postData: [:])
                    }
                    NotificationCenter.default.post(name: Notification.Name.Profile.Edu, object: self)
                    completion(true, json, "")
                    ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
                case 202:
                    NotificationCenter.default.post(name: Notification.Name.Profile.Edu, object: self)
                    completion(true, json["message"], "")
                    ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
//                    self.updateLocalData(params: parameters, postData: [:])
                default:
                    let message = json["message"].stringValue
                    completion(false, JSON.init(), message)
                }
            }
        } else {
            NetworkManager.saveExperience(parameters: parameters, method: method) { (response) in
                let json = JSON(response.result.value ?? JSON.init())
//                print(json)
                switch response.response?.statusCode {
                case 200:
                    if self.addEditOrDelete == 1 {
                        let data = json["data"].dictionaryValue
//                        let edu = data["data"]?.arrayValue
//                        let dict = edu?[0]
//                        self.updateLocalData(params: [:], postData: dict ?? [:])
                        if let points = data["tzPoints"]?.intValue {
                            PointsHandler.shared.updateTZPoints(points: points, changedToBeShown: true)
//                            updateTZPoints(points: points)
                        }
                    } else {
//                        self.updateLocalData(params: parameters, postData: [:])
                    }
                    NotificationCenter.default.post(name: Notification.Name.Profile.Edu, object: self)
                    completion(true, json, "")
                    ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
                case 202:
                    completion(true, json["message"], "")
                    NotificationCenter.default.post(name: Notification.Name.Profile.Edu, object: self)
                    ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
//                    self.updateLocalData(params: parameters, postData: [:])
                default:
                    let message = json["message"].stringValue
                    completion(false, JSON.init(), message)
                }
            }
        }
    }
    
    
    
    // MARK: - Local Data
    
    func updateLocalData(params: [String: Any], postData: JSON) {
        let p = screenType == 1 ? UserDefaults.standard.EducationList : UserDefaults.standard.ExperienceList
        if p != "" {
            if let json = p.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                do {
                    let json = try JSON(data: json)
                    var educationList = json.arrayValue
                    if addEditOrDelete == 3 {
                        educationList.remove(at: indexForDeletion)
                    } else if addEditOrDelete == 1  {
                        educationList.append(postData)
                    } else {
                        educationList[indexForDeletion] = JSON(params)
                    }
                    let jsonForSaving = JSON(educationList)
                    if screenType == 1 {
                        UserDefaults.standard.EducationList = jsonForSaving.rawString() ?? "[]"
                    } else {
                        UserDefaults.standard.ExperienceList = jsonForSaving.rawString() ?? "[]"
                    }
                } catch {
                }
            }
        }
    }
    // MARK: - Utility methods
    func parseSearchedData(data: [JSON]) {
        let search = data.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["id"] = json["id"].intValue
            dict["value"] = json["value"].string
            return dict
        }
        searchedData = search
        let drop = search.map { (dict) -> String in
            return (dict["value"] as? String) ?? ""
        }
        dropDownData = drop
    }
    func parseOrganisationData(data: [JSON]) {
        var filteredData = [JSON]()
        filteredData = screenType == 1 ? data.filter({$0["type"].stringValue == "college"}) : data.filter({$0["type"].stringValue == "company"})
        let org = filteredData.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["organisation_id"] = json["organisation_id"].intValue
            dict["city"] = json["city"].string
            dict["organisation"] = json["organisation"].string
            return dict
        }
        organisationData = org
        let drop = org.map { (dict) -> String in
            return (dict["organisation"] as? String) ?? ""
        }
        dropDownData = drop
    }
}

extension EditEducationViewModel {
    func getOrganisations(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getOrganisationConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseOrganisationData(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func getSearchConfig(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseSearchedData(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
