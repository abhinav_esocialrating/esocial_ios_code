//
//  EducationListViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 02/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class EducationListViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var educationTableView: UITableView!
    
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    // MARK: - Vatiables
    let viewModel = EducationListViewModel()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        loadLocalData()
        loadDataFromAPI()
    }
    func configureView() {
        titleLabel.text = viewModel.screenType == 1 ? "Education" : "Experience"
        educationTableView.register(UINib(nibName: TableViewCells.InterestsTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.InterestsTableViewCell.rawValue)
        educationTableView.register(UINib(nibName: TableViewCells.EducationListCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.EducationListCell.rawValue)
        educationTableView.registerFromXib(name: TableViewCells.ExperienceHeaderCell.rawValue)
        backButton.addTarget(self, action: #selector(backButtonTapped(_:)), for: .touchUpInside)
    }

    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    func openEditEducation(education: EducationModel?, experience: ExperienceModel?, index: Int) {
        let vc = EditEducationViewController.instantiate(fromAppStoryboard: .Profile)
        vc.viewModel.screenType = viewModel.screenType
        vc.viewModel.addEditOrDelete = 2
        vc.viewModel.indexForDeletion = index
        vc.viewModel.isSchoolFromDropDown = true
        vc.viewModel.isDegreeFromDropDown = true
        vc.viewModel.isFosFromDropDown = true
        if viewModel.screenType == 1 {
            vc.viewModel.education = education
        } else {
            vc.viewModel.experience = experience
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        openEditScreen()
    }
    
    // MARK: - Helper methods
    func loadLocalData() {
        viewModel.loadLocalData { (success) in
            if success {
                self.educationTableView.reloadData()
            }
        }
    }
    func updateUI() {
        DispatchQueue.main.async {
            self.educationTableView.reloadData()
        }
    }
    func openEditScreen() {
        let vc = EditEducationViewController.instantiate(fromAppStoryboard: .Profile)
        vc.viewModel.screenType = viewModel.screenType
        vc.viewModel.addEditOrDelete = 1
        navigationController?.pushViewController(vc, animated: true)
    }
    // MARK: - Web Services
    func loadDataFromAPI() {
        let params = ["id": UserDefaults.standard.userId]
        if viewModel.screenType == 1 {
            viewModel.getEducation(parameters: params) { (success, json, message) in
                if success {
                    self.updateUI()
                }
            }
        } else {
            viewModel.getExperience(parameters: params) { (success, json, message) in
                if success {
                    self.updateUI()
                }
            }
        }
    }
    
}
// MARK: - TableView methods
extension EducationListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.screenType == 1 ? viewModel.educationDataSource.count : viewModel.experienceDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.screenType == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.EducationListCell.rawValue, for: indexPath) as! EducationListCell
            cell.indexPath = indexPath
            cell.listViewModel = viewModel
            cell.education = viewModel.educationDataSource[indexPath.row]
            cell.editTapped = { index in
                self.openEditEducation(education: self.viewModel.educationDataSource[indexPath.row], experience: nil, index: indexPath.row)
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.InterestsTableViewCell.rawValue, for: indexPath) as! InterestsTableViewCell
            cell.indexPath = indexPath
            cell.listViewModel = viewModel
            cell.experience = viewModel.experienceDataSource[indexPath.row]
            cell.editTapped = { index in
                self.openEditEducation(education: nil, experience: self.viewModel.experienceDataSource[indexPath.row], index: indexPath.row)
            }
//            cell.editButton.isHidden = false
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.screenType == 1 ? UITableView.automaticDimension : UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ExperienceHeaderCell.rawValue) as! ExperienceHeaderCell
        cell.indexPath = IndexPath(row: 0, section: section)
        cell.editButton.isHidden = false
        cell.editImage.applyTemplate(image: UIImage(named: "Add-icon-white"), color: UIColor.NewTheme.darkGray)
        cell.label.text = viewModel.screenType == 1 ? "Collegee/School" : "Work Experience"
        cell.lowerView.isHidden = false
        cell.editTapped = { index in
            self.openEditScreen()
        }
        cell.label.textColor = UIColor.NewTheme.darkGray
        return cell
    }
}
