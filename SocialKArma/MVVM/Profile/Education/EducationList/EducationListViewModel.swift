//
//  EducationListViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 02/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class EducationListViewModel: NSObject {
    var educationDataSource = [EducationModel]()
    var experienceDataSource = [ExperienceModel]()
    var screenType = 0  // 1: Education   2: Experience
    
    func loadLocalData(completion: (_ success: Bool) -> Void) {
        let p = screenType == 1 ? UserDefaults.standard.EducationList : UserDefaults.standard.ExperienceList
        if p != "" {
            if let json = p.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                do {
                    educationDataSource = []
                    experienceDataSource = []
                    let json = try JSON(data: json)
                    let educationList = json.arrayValue
                    for i in educationList {
                        if screenType == 1 {
                            let edu = EducationModel(dictionary: i.dictionaryValue)
                            self.educationDataSource.append(edu!)
                        } else {
                            let edu = ExperienceModel(dictionary: i.dictionaryValue)
                            self.experienceDataSource.append(edu!)
                        }
                    }
                    completion(true)
                } catch {
                    completion(false)
                }
            } else {
                completion(false)
            }
        } else {
            completion(false)
        }
    }
    
    func getEducation(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getEducation(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                self.educationDataSource = []
                self.saveJsonToUserDefaults(json: json["data"])
                let educationList = json["data"].arrayValue
                for i in educationList {
                    let edu = EducationModel(dictionary: i.dictionaryValue)
                    self.educationDataSource.append(edu!)
                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func getExperience(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getExperience(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                self.experienceDataSource = []
                let data = json["data"].dictionaryValue
                let experience = data["experience"]
                let list = experience?.arrayValue ?? []
                for i in list {
                    let edu = ExperienceModel(dictionary: i.dictionaryValue)
                    self.experienceDataSource.append(edu!)
                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    // MARK: - Utility methods
    func saveJsonToUserDefaults(json: JSON) {
        if screenType == 1 {
            UserDefaults.standard.EducationList = json.rawString() ?? "[]"
        } else {
            UserDefaults.standard.ExperienceList = json.rawString() ?? "[]"
        }
    }
}
