//
//  TabBarController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase

class TabBarController: UITabBarController {
    
    var aboutMeViewModel = AboutMeViewModel()
    var remoteConfig: RemoteConfig!
    var isSearchTapped = false  // Floating button on Home screen

    override func viewDidLoad() {
        super.viewDidLoad()

//        tabBarController?.delegate = self
        delegate = self
        getUserInfo(parameters: ["id": UserDefaults.standard.userId], completion: nil)
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            self.getInviteLink(parameters: [:], completion: nil)
//        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        decideForShowingVersionAlert()
    }
    // MARK: - Methods
    func callRecurringLocationService() {
        LocationManager.shared.setUpRecurringLocationService()
    }
}
// MARK: - Tabbar delegate
extension TabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if selectedIndex != 3 {
            ProfileV2Data.sharedInstance.notificationType = "default"
        }
    }
}
// MARK: - Web Services
extension TabBarController {
    func getUserInfo(parameters: [String:Any], completion: ((_ success: Bool, _ json: JSON, _ message: String) -> Void)?) {
                
        NetworkManager.getUserInfo(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint("getUserInfo")
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"]
                let invite_link = data["invite_link"].stringValue
                UserDefaults.standard.TZInviteLink = invite_link
                UserDefaults.standard.userInfo = data.rawString() ?? ""
                if let points = data["karma_points"].int {
                    PointsHandler.shared.updateTZPoints(points: points, changedToBeShown: false)
//                    NotificationCenter.default.post(name: Notification.Name.Points.didChange, object: nil, userInfo: ["points": points])
                }
            default:
                _ = json["message"].stringValue
            }
        }
    }
    func getInviteLink(parameters: [String:Any], completion: ((_ success: Bool, _ json: JSON, _ message: String) -> Void)?) {
                    
        NetworkManager.getInviteLink(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getUserInfo")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"]
                UserDefaults.standard.TZInviteLink = data.stringValue
            default:
                _ = json["message"].stringValue
            }
        }
    }
}
// MARK: - RemoteConfig
extension TabBarController {
    func configureRemoteConfig() {
        // [START get_remote_config_instance]
        remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.setDefaults([Constants.FirebaseRemoteConfigVersionKey.rawValue: "1.0.0" as NSObject])
        
        // Debug mode only
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        
        remoteConfig.fetch() { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate() { (error) in
                    let value = self.remoteConfig.configValue(forKey: Constants.FirebaseRemoteConfigVersionKey.rawValue)
                    let storeVersionStr = value.stringValue ?? "1.0.0" //"1.0.0"//
                    let currentVersionStr = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
                    if self.compareNumeric(current: currentVersionStr, store: storeVersionStr) ==  .orderedAscending {
                        self.openAlertForAppUpdate()
                    }
                }
            } else {
                print("Config not fetched")
                print("Error: \(error?.localizedDescription ?? "No error available.")")
            }
        }
    }
    func openAlertForAppUpdate() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "GoodSpace", message: "A new version of the app is available. Would you like to upgrade now?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No, thanks", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Sure", style: .default, handler: { (action) in
                UIApplication.shared.open(URL(string: "https://apps.apple.com/in/app/trustze/id1495314369")!, options: [:], completionHandler: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func decideForShowingVersionAlert() {
        guard let appdelegate = appDelegate else {
            return
        }
        if !appdelegate.isVersionAlertShown {
            appdelegate.isVersionAlertShown = true
            configureRemoteConfig()
        }
    }
}
/*
{
"message" : "Fetched successfully",
"status" : 200,
"data" : {
 "reviews" : 8,
 "name" : "Abhinav Voda",
 "image_id" : null,
 "karma_points" : -19,
 "score" : 3.875
}
}
 */
