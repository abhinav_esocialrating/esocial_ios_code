//
//  ProfileV2EducationListViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ProfileV2EducationListViewController: UIViewController {
    
    @IBOutlet weak var educationTableView: UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var editBtn: UIButton!

    // MARK: - Vatiables
    let viewModel = EducationListViewModel()
    var userType = 1   // 1: self   2: other
    var otherUserId = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        addEduNotification()
        loadDataFromAPI()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureView() {
        educationTableView.register(UINib(nibName: TableViewCells.InterestsTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.InterestsTableViewCell.rawValue)
        educationTableView.register(UINib(nibName: TableViewCells.EducationListCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.EducationListCell.rawValue)
        educationTableView.registerFromXib(name: TableViewCells.ExperienceHeaderCell.rawValue)
        educationTableView.contentInset = UIEdgeInsets.zero
        
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        educationTableView.layer.cornerRadius = 12
        typeLabel.text = viewModel.screenType == 1 ? "My Education" : "My Experience"

        editBtn.isHidden = userType == 2
    }

    // MARK: - Actions
    @IBAction func editTapped(_ sender: Any) {
        openEditScreen()
    }
    // MARK: - Methods
    func updateUI(success: Bool) {
        self.educationTableView.reloadData()
        self.educationTableView.isHidden = !success
        self.noDataLabel.isHidden = success
        self.noDataLabel.text = self.viewModel.screenType == 1 ? "Education details not entered by the user" : "Work details not entered by the user"
    }

    func loadDataFromAPI() {
        let parameters = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        if viewModel.screenType == 1 {
            viewModel.getEducation(parameters: parameters) { (success, json, message) in
                
                self.updateUI(success: self.viewModel.educationDataSource.count > 0)
            }
        } else {
            viewModel.getExperience(parameters: parameters) { (success, json, message) in
                
                self.updateUI(success: self.viewModel.experienceDataSource.count > 0)
            }
        }
    }
    func openEditEducation(education: EducationModel?, experience: ExperienceModel?, index: Int) {
        let vc = EditEducationViewController.instantiate(fromAppStoryboard: .Profile)
        vc.viewModel.screenType = viewModel.screenType
        vc.viewModel.addEditOrDelete = 2
        vc.viewModel.indexForDeletion = index
        vc.viewModel.isSchoolFromDropDown = true
        vc.viewModel.isDegreeFromDropDown = true
        vc.viewModel.isFosFromDropDown = true
        if viewModel.screenType == 1 {
            vc.viewModel.education = education
        } else {
            vc.viewModel.experience = experience
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    func openEditScreen() {
        let vc = EditEducationViewController.instantiate(fromAppStoryboard: .Profile)
        vc.viewModel.screenType = viewModel.screenType
        vc.viewModel.addEditOrDelete = 1
        navigationController?.pushViewController(vc, animated: true)
    }

}
// MARK: - TableView methods
extension ProfileV2EducationListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.screenType == 1 ? viewModel.educationDataSource.count : viewModel.experienceDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.screenType == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.EducationListCell.rawValue, for: indexPath) as! EducationListCell
            cell.indexPath = indexPath
            cell.listViewModel = viewModel
            cell.education = viewModel.educationDataSource[indexPath.row]
            cell.editTapped = { index in
                self.openEditEducation(education: self.viewModel.educationDataSource[indexPath.row], experience: nil, index: indexPath.row)
            }
            cell.editButton.isHidden = userType == 1 ? false : true
            cell.lowerView.isHidden = (viewModel.educationDataSource.count-1 == indexPath.row)

            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.InterestsTableViewCell.rawValue, for: indexPath) as! InterestsTableViewCell
            cell.indexPath = indexPath
            cell.listViewModel = viewModel
            cell.experience = viewModel.experienceDataSource[indexPath.row]
            cell.editTapped = { index in
                self.openEditEducation(education: nil, experience: self.viewModel.experienceDataSource[indexPath.row], index: indexPath.row)
            }
            cell.lowerView.isHidden = (viewModel.experienceDataSource.count-1 == indexPath.row)
            cell.editButton.isHidden = userType == 1 ? false : true
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
}
// MARK: - Notification
extension ProfileV2EducationListViewController {
    func addEduNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(eduChanged(_:)), name: Notification.Name.Profile.Edu, object: nil)
    }
    
    @objc func eduChanged(_ notification: Notification) {
        loadDataFromAPI()
    }
}
