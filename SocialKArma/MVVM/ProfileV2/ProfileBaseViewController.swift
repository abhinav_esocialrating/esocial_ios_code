//
//  ProfileBaseViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 02/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import FlexiblePageControl
import SwiftyJSON
import FAPanels
import DropDown

class ProfileBaseViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var flexiblePageControl: FlexiblePageControl!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var pointsBtn: UIButton!
    @IBOutlet weak var notificationBadge: UILabel!
    @IBOutlet weak var chatNotificationBadge: UILabel!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var reportBtn: UIButton!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var msgBtn: UIButton!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchStatusBtn: UIButton!

    var otherUserId = 0
    var userType = 1  // 1: Self   2: Other User
    let pagerVC = ProfileV2PagerViewController()
    var dropDownData = ["Report"]
    var header: ProfileV2HeaderView!
    var imageUrl: String?
    let imagePicker = ImagePicker()
    let dropDown = DropDown()
    var profileCategory: ProfileCategoryTypes = .Biography

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        configurePager()
        if userType == 1 {
            setUpNavBar()
//            addNameChangeNotification()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        if (tabBarController?.selectedIndex) != nil {
            ProfileV2Data.sharedInstance.userType = userType
        }
        
        let params = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        ProfileV2Data.sharedInstance.getAboutMeInfo(parameters: params) { (success, json, message) in
            NotificationCenter.default.post(name: Notification.Name.Profile.AboutMe, object: nil)
        }
//        configureHeader()
        
        if userType == 1 {
//            setNotificationBadges()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !UserDefaults.standard.isProfileCoachMarkShown {
            UserDefaults.standard.isProfileCoachMarkShown = true
            let vc = InfoCardsViewController.instantiate(fromAppStoryboard: .Main)
            vc.modalPresentationStyle = .fullScreen
            vc.viewModel.screenType = 3
            present(vc, animated: false, completion: nil)
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureView() {
//        let image = userType == 1 ? UIImage(named: "menu") : UIImage(named: "Back-Arrow-icon-Grey")
        let image = UIImage(named: "Back-Arrow-icon-Grey")
        menuBtn.setImage(image, for: .normal)
        notificationBadge.layer.cornerRadius = notificationBadge.frame.width/2
        notificationBadge.clipsToBounds = true
        chatNotificationBadge.layer.cornerRadius = chatNotificationBadge.frame.width/2
        chatNotificationBadge.clipsToBounds = true
    }
    func configureHeader() {
        headerView.subviews.forEach({ $0.removeFromSuperview() })
        
        header = ProfileV2HeaderView.instantiateFromNib()
        headerView.addSubview(header)
        header.anchor(top: headerView.topAnchor, paddingTop: 0, bottom: headerView.bottomAnchor, paddingBottom: 0, left: headerView.leftAnchor, paddingLeft: 0, right: headerView.rightAnchor, paddingRight: 0, width: 0, height: 0)
        
        header.layer.cornerRadius = 12
        headerView.layer.cornerRadius = 12
        headerView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        header.userImg.rounded()
        
        header.userType = userType
//        if userType == 1 {
//            let userInfo = UserDefaults.standard.userInfo
//            let data = userInfo.data(using: .utf8)
//            let json = JSON(data ?? Data())
//            let dict = json.dictionaryObject
//            header.userInfo = dict ?? [String: Any]()
//        } else {
            let params = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
            header.getUserInfo(parameters: params)
//        }
        header.percentStackView.isHidden = userType == 2
        header.scoreViewTop.constant = userType == 1 ? 70 : 30
        header.editBtn.setImage(UIImage(named: userType == 1 ? "Edit-blue" : "score_message"), for: .normal)
        header.cameraButton.isHidden = userType == 2
        
        
        if userType == 1 {
            notificationBadge.isHidden = PointsHandler.shared.isNotification ? false : true
            chatNotificationBadge.isHidden = PointsHandler.shared.isChatNotificationBadge ? false : true
            reportBtn.isHidden = true
            headerViewHeight.constant = 206
        } else {
            notificationBadge.isHidden = true
            pointsBtn.isHidden = true
            notificationBtn.isHidden = true
            reportBtn.isHidden = false
            msgBtn.isHidden = true
            infoBtn.isHidden = true
            headerViewHeight.constant = 170
        }
        header.editTap = { dict, type in
            if type == 1 {
                self.openBasicDetails()
            } else {
                self.openChat(dict: dict)
            }
        }
        header.addAboutMeNotification()
        if userType == 1 {
            header.addPicEditNotification()
            
            header.editProfilePic = {
                self.imagePicker.delegate = self
                let alert = self.imagePicker.pickImage(sourceController: self, path: "")
                self.present(alert, animated: true, completion: nil)
            }
        }
        header.profilePicTap = { url in
            self.imageUrl = url
            self.showProfilePic()
        }
        header.userImg.setUpBorder(width: 1.5, color: UIColor.SearchColors.blue, radius: header.userImg.frame.width/2)
    }
    func configurePager() {
        pagerVC.numberOfPagesChanged = { count in
            self.flexiblePageControl.numberOfPages = count
            self.flexiblePageControl.layoutIfNeeded()
            self.flexiblePageControl.updateViewSize()
        }
        pagerVC.userType = userType
        pagerVC.otherUserId = otherUserId
        pagerVC.profileCategory = profileCategory
        addChild(pagerVC)
        pagerVC.pageMoved = { index in
            self.flexiblePageControl.setCurrentPage(at: index)
        }
        pagerVC.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        pagerVC.view.frame = containerView.bounds
        
        containerView.addSubview(pagerVC.view)
        pagerVC.didMove(toParent: self)
        
        flexiblePageControl.numberOfPages = 1 //userType == 1 ? 6 : 5
        view.addSubview(flexiblePageControl)
        
        flexiblePageControl.pageIndicatorTintColor = UIColor.SearchColors.tagBkg
        flexiblePageControl.currentPageIndicatorTintColor = UIColor.SearchColors.blue

        // size
        let config = FlexiblePageControl.Config(
            displayCount: 6,
            dotSize: 12,
            dotSpace: 7,
            smallDotSizeRatio: 0.5,
            mediumDotSizeRatio: 0.7
        )
        flexiblePageControl.setConfig(config)
        
        
    }
    // MARK: - Actions
    @IBAction func sideMenuTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)

//        if userType == 2 {
//            navigationController?.popViewController(animated: true)
//        } else {
//            if let panel = appDelegate?.window?.rootViewController as? FAPanelController {
//                panel.openLeft(animated: true)
//            }
//        }
    }
    @IBAction func notificationTapped(_ sender: Any) {
        openNotificationFromNavBar()
    }
    @IBAction func pointsBtnTapped(_ sender: Any) {
        openPointsScreenFromNavBar()
    }
    @IBAction func reportTapped(_ sender: Any) {
        showDropDown()
    }
    
    @IBAction func messageTapped(_ sender: Any) {
        openChatFromNavBar()
    }
    @IBAction func infoTapped(_ sender: Any) {
        openInfoFromNavBar(info: Constants.ProfileInfo.rawValue)
    }
    // MARK: - Methods
    func showDropDown() {
        dropDown.anchorView = reportBtn
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 200
        dropDown.dataSource = dropDownData
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropSelection(index: index, item: item)
        }
        dropDown.show()
    }
    func handleDropSelection(index: Int, item: String) {
        switch index {
        case 0:
            openReport()
        default:
            print("")
        }
    }
    func openReport() {
        let vc = ReportUserViewController.instantiate(fromAppStoryboard: .MainExt)
        vc.viewModel.otherUserId = "\(otherUserId)"
        vc.modalPresentationStyle = .overCurrentContext
        vc.notify = {
            self.tabBarController?.tabBar.isHidden = false
        }
        present(vc, animated: false, completion: nil)
        tabBarController?.tabBar.isHidden = true
    }
    func openChat(dict: [String:Any]) {
        let vc = ChatViewController.instantiate(fromAppStoryboard: .Chat)
        SocketIOManager.shared.toUserId = "\(self.otherUserId)"
        let chatUser = ChatUser(dictionary: dict as NSDictionary)
        vc.viewModel.chatUser = chatUser
        navigationController?.pushViewController(vc, animated: true)
    }
    func openBasicDetails() {
        let vc = EditInfoViewController.instantiate(fromAppStoryboard: .Profile)
        vc.screenType = 2
        navigationController?.pushViewController(vc, animated: true)
    }
    func showProfilePic() {
        let vc = PhotoViewerViewController()
        vc.modalPresentationStyle = .overCurrentContext
        vc.imageUrl = imageUrl
        vc.backAction = {
            self.tabBarController?.tabBar.isHidden = false
        }
        present(vc, animated: false, completion: nil)
        tabBarController?.tabBar.isHidden = true
    }
}
// MARK: - NavBar
extension ProfileBaseViewController {
    func setUpNavBar() {
        NotificationCenter.default.addObserver(self, selector: #selector(pushNotificationReceived(_:)), name: Notification.Name.PushReceived, object: nil)
    }
    @objc func pushNotificationReceived(_ notification: Notification) {
        if let type = notification.userInfo?["type"] as? String {
            if type == PushNotificationType.FEEDBACK_UE.rawValue || type == PushNotificationType.FEEDBACK_UE_2.rawValue || type == PushNotificationType.CONTACT_UE.rawValue || type == PushNotificationType.INVITE_UE.rawValue || type == PushNotificationType.PROFILE_UE.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_1.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_2.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_3.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_4.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_5.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_6.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_7.rawValue || type == PushNotificationType.UE_INFO_1.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_1.rawValue || type == PushNotificationType.UE_REINVITE_1.rawValue || type == PushNotificationType.UE_REINVITE_2.rawValue || type == PushNotificationType.CHAT_UNREAD_MESSAGES.rawValue {  }
            else if type == PushNotificationType.CHAT.rawValue {
//                PointsHandler.shared.isChatNotificationBadge = true
//                chatNotificationBadge.isHidden = false
                setNotificationBadges()
            }
            else {
//                PointsHandler.shared.isNotification = true
//                notificationBadge.isHidden = false
                setNotificationBadges()
            }
        }
    }
    func setNotificationBadges() {
        PointsHandler.shared.getNotificationCount { notificationCountData in
            
            if let notifications = notificationCountData["notification"] as? Int, notifications > 0 {
                PointsHandler.shared.isNotification = true
                self.notificationBadge.isHidden = false
                self.notificationBadge.text = "\(notifications)"
            } else {
                self.notificationBadge.isHidden = PointsHandler.shared.isNotification ? false : true
            }
            if let chat = notificationCountData["chat"] as? Int, chat > 0 {
                PointsHandler.shared.isChatNotificationBadge = true
                self.chatNotificationBadge.isHidden = false
                self.chatNotificationBadge.text = "\(chat)"
            } else {
                self.chatNotificationBadge.isHidden = PointsHandler.shared.isChatNotificationBadge ? false : true
            }
        }
    }
}
// MARK: - Name Changed
extension ProfileBaseViewController {
    func addNameChangeNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(nameChanged), name: Notification.Name.AppNotifications.NameChanged, object: nil)

    }
    @objc func nameChanged(_ notification: Notification) {
        let data = notification.userInfo
        if let name = data?["name"] as? String {
            header.nameLabel.text = name
        }
    }
}
// MARK: - ImagePicker methods
extension ProfileBaseViewController: ImagePickerUploaderDelegate {
    func PickerSuccess(withImage: UIImage) {
//        presentCropViewController(image: withImage)
        imagePicker.uploadProfilePic(image: withImage) { (success, json, message) in
            if success {
                let url = json["data"].stringValue
                self.header.userImg.sd_setImage(with: URL(string: url)!, placeholderImage: UIImage(named: "avatar-2"), options: [], completed: nil)
                Utilities().updateProfilePicUrl(url: url)
                self.imageUrl = url
            } else {
                self.alert(message: message)
            }
        }
    }
    func PickerFailed(withError: String) {
    }
    func profilePicRemoved(url: String!) {
        self.header.userImg.sd_setImage(with: URL(string: url)!, placeholderImage: UIImage(named: "avatar-2"), options: [], completed: nil)
        Utilities().updateProfilePicUrl(url: url)
    }
}
