//
//  ProfileV2SkillsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProfileV2SkillsViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tagView: TagListView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tagViewHeight: NSLayoutConstraint!
    @IBOutlet weak var noDataLabel: UILabel!
    // MARK: - Variables
    var screenType = 0  // 1: skill  2: interest  3: looking for  4: Category/Service
    var skills: [SkillModel]?
    var interests: [InterestModel]?
    var businessServices = [BusinessService]()
    var userType = 1 // 1: Self   2: Other User
    var otherUserId = 0
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        addSkillsNotification()
        if screenType == 3 {
            typeLabel.text = "I'm Looking For..."
            setTags()
        } else if screenType == 1 {
            typeLabel.text = "My Skills"
            getAllSkills()
        } else if screenType == 2 {
            typeLabel.text = "My Interests"
            getAllInterests()
        } else {
            typeLabel.text = "My Category/Services"
            getBusinessInfo()
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureView() {
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        tagView.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
        editBtn.isHidden = userType == 1 ? false : true
    }
    // MARK: - Methods
    func setTags() {
        tagView.removeAllTags()
        ProfileV2Data.sharedInstance.lookingForSelected.forEach { (item) in
            let dict = item.dictionaryValue
            let value = dict["value"]?.stringValue
            let id = dict["id"]?.intValue
            if id == 8 || id == 9 || id == 10 {
                let friendStr = "Friends - \(value ?? "")"
                tagView.addTag(friendStr)
            } else {
                tagView.addTag(value ?? "")
            }
        }
        tagViewHeight.constant = tagView.intrinsicContentSize.height
        tagView.layoutIfNeeded()
    }
    func setSkillTags() {
        tagView.removeAllTags()
        if let skillssdsd = skills {
            skillssdsd.forEach { (skill) in
                tagView.addTag(skill.skill ?? "")
            }
            tagViewHeight.constant = tagView.intrinsicContentSize.height
        }
    }
    func setInterestTags() {
        tagView.removeAllTags()
        if let skillssdsd = interests {
            skillssdsd.forEach { (skill) in
                tagView.addTag(skill.interest ?? "")
            }
        }
        tagViewHeight.constant = tagView.intrinsicContentSize.height
    }
    func setCategoryTags() {
        tagView.removeAllTags()
        for i in businessServices {
            if let services = i.service, services.count != 0 {
                tagView.addTag(i.service?[0].business_service ?? "")
            }
        }
        tagViewHeight.constant = tagView.intrinsicContentSize.height
        // Managing no data
        scrollView.isHidden = businessServices.count == 0
        noDataLabel.isHidden = businessServices.count != 0
    }
    // MARK: - Actions
    @IBAction func editTapped(_ sender: Any) {
        if screenType == 3 {
            
            let vc = OnboardingLookingForViewController.instantiate(fromAppStoryboard: .Onboarding)
            vc.screenType = 2
            let lookingForSelected = ProfileV2Data.sharedInstance.lookingForSelected.map({ (json) -> Int in
                let dict = json.dictionaryValue
                let id = dict["id"]?.intValue
                return id ?? 0
            })
            vc.selectedIds = lookingForSelected
            navigationController?.pushViewController(vc, animated: true)
            
 /*           let vc = UserDetailsStep3ViewController.instantiate(fromAppStoryboard: .Main)
            vc.viewModel.screenType = 2
            var lookingForSelected = ProfileV2Data.sharedInstance.lookingForSelected.map({ (json) -> Int in
                let dict = json.dictionaryValue
                let id = dict["id"]?.intValue
                return id ?? 0
            })
            if lookingForSelected.contains(8) || lookingForSelected.contains(9) || lookingForSelected.contains(10) {
                lookingForSelected.append(1)
            }
            UserDetails.shared.selectedWorkSpace = lookingForSelected
            navigationController?.pushViewController(vc, animated: true)  */
        } else if screenType == 1 {
            let vc = SkillsViewController.instantiate(fromAppStoryboard: .Profile)
            vc.screenType = 1
            vc.userType = userType
            vc.otherUserId = otherUserId
            navigationController?.pushViewController(vc, animated: true)
        } else if screenType == 2 {
            let vc = SkillsViewController.instantiate(fromAppStoryboard: .Profile)
            vc.screenType = 2
            vc.userType = userType
            vc.otherUserId = otherUserId
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = EditBusinessInfoViewController.instantiate(fromAppStoryboard: .Profile)
            vc.viewModel.businessServices = businessServices
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    // MARK: - Web Services
    func getAllSkills() {
        let parameters = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        NetworkManager.getAllSkills(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.skills = []
                self.skills = data.map({ (item) -> SkillModel in
                    let skill = SkillModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                self.setSkillTags()
            default:
                _ = json["message"].stringValue
            }
        }
    }
    func getAllInterests() {
        let parameters = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        NetworkManager.getAllInterests(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.interests = []
                self.interests = data.map({ (item) -> InterestModel in
                    let skill = InterestModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                self.setInterestTags()
            default:
                _ = json["message"].stringValue
            }
        }
    }
    func getBusinessInfo() {
        let params: [String:Any] = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        NetworkManager.getBusinessInfo(parameters: params, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getBusinessInfo")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let serviceArray = jsonData["data"] as! NSArray
                    self.businessServices = []
                    self.businessServices = BusinessService.modelsFromDictionaryArray(array: serviceArray)
                    self.setCategoryTags()
                } catch {
                    
                }
            default:
                _ = json["message"].stringValue
            }
        }
    }
}

// MARK: - Notification
extension ProfileV2SkillsViewController {
    func addSkillsNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(skillsChanged(_:)), name: Notification.Name.Profile.Skills, object: nil)
    }
    
    @objc func skillsChanged(_ notification: Notification) {
//        if screenType == 1 {
//            getAllSkills()
//        } else if screenType == 2 {
//            getAllInterests()
//        } else
        if screenType == 4 {
            getBusinessInfo()
        }
    }
}
