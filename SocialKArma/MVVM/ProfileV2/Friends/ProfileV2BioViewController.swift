//
//  ProfileV2BioViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ProfileV2BioViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var bioLabel: UITextView!
    
    @IBOutlet weak var editBtn: UIButton!
    
    
    var userType = 1   // 1: self   2: other

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        addAboutMeNotification()
        setValues()
    }
    func configureView() {
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        editBtn.isHidden = userType == 1 ? false : true
    }

    // MARK: - Methods
    func setValues() {
        let str = userType == 1 ? ProfileV2Data.sharedInstance.biography : ProfileV2Data.sharedInstance.OtherBiography
        attributedText(string: str)
    }
    func attributedText(string: String) {
        let attributedString = NSMutableAttributedString(string: string.decodeEmoji)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontWith(name: .Roboto, face: .Regular, size: 16), range: NSMakeRange(0, attributedString.length))
        bioLabel.attributedText = attributedString
    }
    @IBAction func editTapped(_ sender: Any) {
        let vc = EditBioViewController.instantiate(fromAppStoryboard: .Profile)
        vc.viewModel.bio = ProfileV2Data.sharedInstance.biography
        navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK: - Notification
extension ProfileV2BioViewController {
    func addAboutMeNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(aboutMeChanged(_:)), name: Notification.Name.Profile.AboutMe, object: nil)
    }
    
    @objc func aboutMeChanged(_ notification: Notification) {
        setValues()
    }
}
