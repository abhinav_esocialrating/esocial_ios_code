//
//  ProfileV2BasicDetailViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ProfileV2BasicDetailViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var genderTF: UITextField!
    
    @IBOutlet weak var dobTF: UITextField!
    
    @IBOutlet weak var locationTF: UITextField!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var editBtn: UIButton!

    var userType = 1   // 1: self   2: other
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        addAboutMeNotification()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setValues()
    }
    func configureView() {
        scrollView.layer.cornerRadius = 12
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        editBtn.isHidden = userType == 1 ? false : true
    }

    // MARK: - Methods
    @IBAction func editTapped(_ sender: Any) {
        let vc = EditInfoViewController.instantiate(fromAppStoryboard: .Profile)
        vc.screenType = 2
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setValues() {
        let aboutMe = userType == 1 ? ProfileV2Data.sharedInstance.dataSourceAboutMe : ProfileV2Data.sharedInstance.dataSourceOtherAboutMe
        if let value = aboutMe["Gender"], value != "" {
            genderTF.text = aboutMe["Gender"]
        } else {
            genderTF.text = "NA"
        }
        if let value = aboutMe["DOB"], value != "" {
            dobTF.text = aboutMe["DOB"]
        } else {
            dobTF.text = "NA"
        }
        if let value = aboutMe["Location"], value != "" {
            locationTF.text = aboutMe["Location"]
        } else {
            locationTF.text = "NA"
        }
        if let value = aboutMe["Email ID"], value != "" {
            emailTF.text = aboutMe["Email ID"]
        } else {
            emailTF.text = "NA"
        }
    }

}
// MARK: - Notification
extension ProfileV2BasicDetailViewController {
    func addAboutMeNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(aboutMeChanged(_:)), name: Notification.Name.Profile.AboutMe, object: nil)
    }
    
    @objc func aboutMeChanged(_ notification: Notification) {
        setValues()
    }
}
