//
//  ProfileV2PersonalDetailViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ProfileV2PersonalDetailViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!

    @IBOutlet weak var heightTF: UITextField!
    
    @IBOutlet weak var weightTF: UITextField!
    
    @IBOutlet weak var fitnessTF: UITextField!
    
    @IBOutlet weak var drinkingTF: UITextField!
    
    @IBOutlet weak var smokingTF: UITextField!
    
    @IBOutlet weak var religionTF: UITextField!
    
    @IBOutlet weak var politicalTF: UITextField!
    @IBOutlet weak var editBtn: UIButton!

    var userType = 1   // 1: self   2: other

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        addAboutMeNotification()
        setValues()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setValues()
    }
    func configureView() {
        scrollView.layer.cornerRadius = 12
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        editBtn.isHidden = userType == 1 ? false : true
    }
    // MARK: - Methods
    func setValues() {
        let personalInfo = userType == 1 ? ProfileV2Data.sharedInstance.dataSourcePersonalInfo : ProfileV2Data.sharedInstance.dataSourceOtherPersonalInfo
        
        if let value = personalInfo["Height (cms)"], value != "" {
            heightTF.text = value
        } else {
            heightTF.text = "NA"
        }
        if let value = personalInfo["Weight (kg)"], value != "" {
            weightTF.text = value
        } else {
            weightTF.text = "NA"
        }
        if let value = personalInfo["Fitness"], value != "" {
            fitnessTF.text = value
        } else {
            fitnessTF.text = "NA"
        }
        if let value = personalInfo["Drinking"], value != "" {
            drinkingTF.text = value
        } else {
            drinkingTF.text = "NA"
        }
        if let value = personalInfo["Smoking"], value != "" {
            smokingTF.text = value
        } else {
            smokingTF.text = "NA"
        }
        if let value = personalInfo["Religion"], value != "" {
            religionTF.text = value
        } else {
            religionTF.text = "NA"
        }
        if let value = personalInfo["Political View"], value != "" {
            politicalTF.text = value
        } else {
            politicalTF.text = "NA"
        }
    }
    
    @IBAction func editTapped(_ sender: Any) {
        let vc = EditInfoViewController.instantiate(fromAppStoryboard: .Profile)
        vc.screenType = 1
        navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK: - Notification
extension ProfileV2PersonalDetailViewController {
    func addAboutMeNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(aboutMeChanged(_:)), name: Notification.Name.Profile.AboutMe, object: nil)
    }
    
    @objc func aboutMeChanged(_ notification: Notification) {
        setValues()
    }
}
