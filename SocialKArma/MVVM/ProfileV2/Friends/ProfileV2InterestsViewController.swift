//
//  ProfileV2InterestsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 29/07/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProfileV2InterestsViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    // MARK: - Variables
    var screenType = 0  // 1: skill  2: interest
    var skills: [SkillModel]?
    var interests: [InterestModel]?
    var userType = 1 // 1: Self   2: Other User
    var otherUserId = 0
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        addSkillsNotification()
        if screenType == 1 {
            typeLabel.text = "My Skills"
            getAllSkills()
        } else {
            typeLabel.text = "My Interests"
            getAllInterests()
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureView() {
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        editBtn.isHidden = userType == 1 ? false : true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: TableViewCells.SkillV2TableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.SkillV2TableViewCell.rawValue)
    }
    // MARK: - Actions
    @IBAction func editTapped(_ sender: Any) {
        if screenType == 1 {
            let vc = SkillsViewController.instantiate(fromAppStoryboard: .Profile)
            vc.screenType = 1
            vc.userType = userType
            vc.otherUserId = otherUserId
            navigationController?.pushViewController(vc, animated: true)
        } else if screenType == 2 {
            let vc = SkillsViewController.instantiate(fromAppStoryboard: .Profile)
            vc.screenType = 2
            vc.userType = userType
            vc.otherUserId = otherUserId
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    // MARK: - Methods
    func updateUI(success: Bool) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.isHidden = success ? false : true
            self.noDataLabel.isHidden = success ? true : false
        }
    }
    // MARK: - Web Services
    func getAllSkills() {
        let parameters = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        NetworkManager.getAllSkills(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.skills = []
                self.skills = data.map({ (item) -> SkillModel in
                    let skill = SkillModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                self.updateUI(success: self.skills!.count > 0)
            default:
                _ = json["message"].stringValue
                self.updateUI(success: false)
            }
        }
    }
    func getAllInterests() {
        let parameters = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        NetworkManager.getAllInterests(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.interests = []
                self.interests = data.map({ (item) -> InterestModel in
                    let skill = InterestModel(dictionary: item.dictionaryValue)
                    return skill!
                })
                self.updateUI(success: self.interests!.count > 0)
            default:
                _ = json["message"].stringValue
                self.updateUI(success: false)
            }
        }
    }
}
// MARK: - TableView
extension ProfileV2InterestsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return screenType == 1 ? (skills?.count ?? 0) : (interests?.count ?? 0)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.SkillV2TableViewCell.rawValue, for: indexPath) as! SkillV2TableViewCell
        if screenType == 1 {
            cell.skill = skills?[indexPath.row]
        } else {
            cell.interest = interests?[indexPath.row]
        }
        cell.screenType = screenType
        cell.setValues()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
// MARK: - Notification
extension ProfileV2InterestsViewController {
    func addSkillsNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(skillsChanged(_:)), name: Notification.Name.Profile.Skills, object: nil)
    }
    
    @objc func skillsChanged(_ notification: Notification) {
        if screenType == 1 {
            getAllSkills()
        } else {
            getAllInterests()
        }
    }
}
