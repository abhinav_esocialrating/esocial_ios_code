/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class TagRelationship {
	public var source_user_id : Int?
	public var target_user_id : Int?
	public var relationship_id : Int?
	public var target_user_name : String?
	public var target_user_image_id : String?
	public var relationship : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let TagRelationship_list = TagRelationship.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of TagRelationship Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [TagRelationship]
    {
        var models:[TagRelationship] = []
        for item in array
        {
            models.append(TagRelationship(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let TagRelationship = TagRelationship(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: TagRelationship Instance.
*/
	required public init?(dictionary: NSDictionary) {

		source_user_id = dictionary["source_user_id"] as? Int
		target_user_id = dictionary["target_user_id"] as? Int
		relationship_id = dictionary["relationship_id"] as? Int
		target_user_name = dictionary["target_user_name"] as? String
		target_user_image_id = dictionary["target_user_image_id"] as? String
		relationship = dictionary["relationship"] as? String
	}
    public init?(source_user_id: Int?, target_user_id: Int?, relationship_id: Int?, target_user_name: String?, target_user_image_id: String?, relationship: String?) {

        self.source_user_id = source_user_id
        self.target_user_id = target_user_id
        self.relationship_id = relationship_id
        self.target_user_name = target_user_name
        self.target_user_image_id = target_user_image_id
        self.relationship = relationship
    }
		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.source_user_id, forKey: "source_user_id")
		dictionary.setValue(self.target_user_id, forKey: "target_user_id")
		dictionary.setValue(self.relationship_id, forKey: "relationship_id")
		dictionary.setValue(self.target_user_name, forKey: "target_user_name")
		dictionary.setValue(self.target_user_image_id, forKey: "target_user_image_id")
		dictionary.setValue(self.relationship, forKey: "relationship")

		return dictionary
	}

}
