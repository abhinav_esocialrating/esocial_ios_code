//
//  TagRelationsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 20/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class TagRelationsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    @IBOutlet weak var navViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var saveBtnTop: UIView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var searchTF: UITextField!
    // MARK: - Variables
    let viewModel = TagRelationsViewModel()
    let dropDown = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        getNetworkData()
//        addNotificationObservers()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getNetworkData()
    }
    func configureView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
//        tableViewTop.constant = viewModel.showOrEdit == 1 ? 50 : 8
        navViewHeight.constant = viewModel.showOrEdit == 1 ? 0 : 44
        scrollViewBottom.constant = 0 //viewModel.showOrEdit == 1 ? 0 : 44
//        saveBtn.isHidden = viewModel.showOrEdit == 1 ? true : false
        editView.isHidden = viewModel.showOrEdit == 1 ? false : true
//        saveBtnTop.isHidden = viewModel.showOrEdit == 1 ? true : false
        editBtn.isHidden = viewModel.userType == 1 ? false : true
        searchTF.isHidden = viewModel.showOrEdit == 1 ? true : false

        searchTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    // MARK: - Actions
    @IBAction func editTapped(_ sender: Any) {
        openEditScreen()
    }
    
    @IBAction func saveTapped(_ sender: Any) {
    }
    
    @IBAction func backTapped(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.Profile.relationshipTags, object: nil)
        navigationController?.popViewController(animated: true)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text!.count > 0 {
            contactSearch(query: textField.text!)
        } else {
            viewModel.contactsSearchedFromRemote = []
            self.dropDown.hide()
        }
    }
    
    
    // MARK: - Methods
    func updateUI() {
        tableView.reloadData()
        noDataLabel.isHidden = (viewModel.tags?.count ?? 0) > 0
        tableView.isHidden = (viewModel.tags?.count ?? 0) == 0
    }
    func openEditScreen() {
        let vc = TagRelationsViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.viewModel.showOrEdit = 2
        navigationController?.pushViewController(vc, animated: true)
    }
    // MARK: - Web Service
    func getNetworkData() {
        let parameters = ["id": viewModel.userType == 1 ? UserDefaults.standard.userId : "\(viewModel.otherUserId)"]
        viewModel.network(parameters: parameters) { (success, _, message) in
            self.updateUI()
        }
    }
    func addToDataSource(index: Int) {
        let contact = viewModel.contactsSearchedFromRemote[index]
        let tags = TagRelationship(source_user_id: Int(UserDefaults.standard.userId), target_user_id: contact.contact_user_id, relationship_id: 1, target_user_name: contact.contact_name, target_user_image_id: contact.image_id, relationship: "Friend")!
        viewModel.tags?.append(tags)
        self.saveTags(delete: false, id: 0)
    }
    func contactSearch(query: String) {
        let params = ["query": query]
        viewModel.contactSearch(parameters: params) { (success, json, message) in
            if success {
                self.showDropDown(textField: self.searchTF)
            }
        }
    }
    func saveTags(delete: Bool, id: Int) {
        if let tags = viewModel.tags {
            var params = [String:Any]()
            if delete {
                params = ["users": [id]]
            } else {
                let dictArray = tags.map({ $0.dictionaryRepresentation() })
                params = ["users": dictArray]
            }
            AppDebug.shared.consolePrint(params)
            viewModel.saveNetwork(parameters: params, method: delete ? .delete : .post) { (success, _, message) in
                if success {
                    self.getNetworkData()
                }
            }
        }
    }
    func deleteTags(id: Int) {
        self.saveTags(delete: true, id: id)
    }
    // MARK: - DropDown
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.dataSource = viewModel.dropDownData
        
        dropDown.selectionAction = { (index: Int, item: String) in
            self.addToDataSource(index: index)
        }
        dropDown.show()
    }

}
// MARK: - TableView methods
extension TagRelationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tags?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TagRelationsCell.rawValue, for: indexPath) as! TagRelationsCell
        cell.indexPath = indexPath
        cell.showOrEdit = viewModel.showOrEdit
        cell.tags = viewModel.tags?[indexPath.row]
        cell.relationshipTap = { id, index in
            self.viewModel.tags?[index].relationship_id = id
            self.saveTags(delete: false, id: 0)
        }
        cell.crossTap = { index in
//            self.viewModel.tags?.remove(at: index)
            let id = self.viewModel.tags?[index].target_user_id
            self.deleteTags(id: id ?? 0)
            self.updateUI()
        }
        return cell
    }
    
    
}
// MARK: - Notification
extension TagRelationsViewController {
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(tagsChanged(_:)), name: Notification.Name.Profile.relationshipTags, object: nil)
    }
    
    @objc func tagsChanged(_ notification: Notification) {
        getNetworkData()
    }
}
