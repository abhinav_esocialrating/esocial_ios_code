//
//  TagRelationsCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 20/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class TagRelationsCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var relationshipBtn: UIButton!
    @IBOutlet weak var relationLabel: UILabel!
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var relationLabelTrailing: NSLayoutConstraint!
    @IBOutlet weak var downArrow: UIButton!
    
    let dropDown = DropDown()
    var showOrEdit = 1  { // 1: Show   2: Edit
        didSet {
            crossBtn.isHidden = showOrEdit == 1
            downArrow.isHidden = showOrEdit == 1
            relationshipBtn.isUserInteractionEnabled = showOrEdit != 1
            relationLabelTrailing.constant = showOrEdit == 1 ? 0 : 40
        }
    }
    var relationshipTap: ((_ relationship_id: Int, _ index: Int) -> Void)? = nil
    var crossTap: ((_ index: Int) -> Void)? = nil
    var tags: TagRelationship? {
        didSet {
            setValues()
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setValues() {
        nameLabel.text = tags?.target_user_name
        relationLabel.text = tags?.relationship
    }
    
    @IBAction func relationshipTapped(_ sender: Any) {
        showDropDown()
    }
    
    @IBAction func crossTapped(_ sender: Any) {
        crossTap?(indexPath.row)
    }
    
    func showDropDown() {
        dropDown.anchorView = relationshipBtn
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.dataSource = ConfigDataModel.shared.relationship.map({ ($0["value"]?.stringValue ?? "") })
        
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropDownSelection(index, item)
        }
        dropDown.show()
    }
    func handleDropDownSelection(_ index: Int, _ item: String) {
        relationLabel.text = item
        let id = ConfigDataModel.shared.relationship[index]["id"]?.intValue ?? 0
        relationshipTap?(id, indexPath.row)
    }
    
}
