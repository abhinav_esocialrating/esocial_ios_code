//
//  TagRelationsViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 20/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class TagRelationsViewModel: NSObject {
    
    var showOrEdit = 1  // 1: Show   2: Edit
    var userType = 1   // 1: self   2: other
    var otherUserId = 0
    var contactsSearchedFromRemote = [ContactSearch]()
    var tags: [TagRelationship]?
    var dropDownData = [String]()

    // MARK: - Web Services
    func network(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.network(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint("network")
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayObject
                self.tags = TagRelationship.modelsFromDictionaryArray(array: (data ?? []) as NSArray)
                completion(true, JSON.init(), json["message"].stringValue)
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func saveNetwork(parameters: [String:Any], method: HTTPMethod, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.network(parameters: parameters, method: method) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint("network")
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200, 202:
//                let data = json["data"].arrayObject
//                self.tags = TagRelationship.modelsFromDictionaryArray(array: (data ?? []) as NSArray)
                completion(true, JSON.init(), json["message"].stringValue)
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func contactSearch(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.contactSearch(parameters: parameters, method: .get) { (response) in
            
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    self.contactsSearchedFromRemote = []
                    let data = jsonData.value(forKey: "data") as? NSArray
                    if (data?.count ?? 0) > 0 {
                        self.contactsSearchedFromRemote = ContactSearch.modelsFromDictionaryArray(array: data!)
                        self.dropDownData = self.contactsSearchedFromRemote.map({ $0.contact_name ?? "" })
                    }
                } catch {
                
                }
                completion(true, JSON(), "")
            default:
                let json = JSON(response.result.value ?? JSON.init())
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
