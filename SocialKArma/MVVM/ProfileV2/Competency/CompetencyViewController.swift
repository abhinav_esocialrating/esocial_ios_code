//
//  CompetencyViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 21/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class CompetencyViewController: UIViewController {

    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var navViewHeight: NSLayoutConstraint!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var tagListView: TagListView!

    let dropDown = DropDown()
    let viewModel = CompetencyViewModel()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        getCompetency()
        addNotificationObservers()
    }
    func configureView() {
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        navViewHeight.constant = viewModel.showOrEdit == 1 ? 0 : 44
        editView.isHidden = viewModel.showOrEdit == 1 ? false : true
        editBtn.isHidden = viewModel.userType == 1 ? false : true
        searchTF.isHidden = viewModel.showOrEdit == 1 ? true : false
        if viewModel.showOrEdit == 2 {
            tagListView.enableRemoveButton = true
            tagListView.removeButtonIconSize = 8
            tagListView.removeIconLineColor = UIColor.SearchColors.blue
        }

        searchTF.addTarget(self, action: #selector(searchTextChanged(_:)), for: .editingChanged)
        
        tagListView.delegate = self
        tagListView.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)

    }

    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.Profile.competency, object: nil)
        navigationController?.popViewController(animated: true)
    }
    @objc func searchTextChanged(_ textField: UITextField) {
        if textField.text! != "" {
            let params = ["config": "competency", "search": textField.text!]
            viewModel.getSearchConfig(parameters: params) { (success, json, message) in
                if success {
                    DispatchQueue.main.async {
                        if self.viewModel.searchedData.count > 0 {
                            self.showDropDown(textField: textField)
                        } else {
                            self.dropDown.hide()
                        }
                    }
                }
            }
        }
    }
    @IBAction func editTapped(_ sender: Any) {
        openEditScreen()
    }

    // MARK: - Utility methods

    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.dataSource = viewModel.dropDownData
        dropDown.selectionAction = { (index: Int, item: String) in
            let id = self.viewModel.searchedData[index]["id"] as? Int
//            self.viewModel.services.append(self.viewModel.searchedData[index])
            self.saveCompetency(serviceId: id ?? 0)
        }
        dropDown.show()
    }
    func updateUI() {
        updateTagView(delete: false)
        tagListView.isHidden = viewModel.competencies.count == 0
        noDataLabel.isHidden = viewModel.competencies.count > 0
    }
    func updateTagView(delete: Bool) {
        tagListView.removeAllTags()
        for i in viewModel.competencies {
            tagListView.addTag(i.competency ?? "")
        }
    }
    func openEditScreen() {
        let vc = CompetencyViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.viewModel.showOrEdit = 2
        navigationController?.pushViewController(vc, animated: true)
    }
    // MARK: - Web Services
    func getCompetency() {
        let params: [String:Any] = ["id": viewModel.userType == 1 ? UserDefaults.standard.userId : "\(viewModel.otherUserId)"]
        viewModel.getCompetency(parameters: params) { (success, _, message) in
            self.updateUI()
        }
    }
    func saveCompetency(serviceId: Int) {
        var params = [String:Any]()

//        if viewModel.addEditOrDelete == 2 {
        params["score"] = 5
        params["competency_id"] = serviceId
//        }
        let comp = Competency(dictionary: params as NSDictionary)!
        self.viewModel.competencies.append(comp)
        
        let dictArray = viewModel.competencies.map({$0.dictionaryRepresentation()})
        
        AppDebug.shared.consolePrint(params)
        self.showHud()
        viewModel.saveCompetency(parameters: ["competency": dictArray]) { (success, json, message) in
            self.hideHud()
             if success {
                self.updateUI()
                self.searchTF.text = ""
            }
        }
    }
    func deleteCompetency(id: Int) {
        viewModel.deleteCompetency(parameters: ["competency": [id]], delete: true) { (success, _, message) in
            if success {
                self.getCompetency()
            } else {
                self.alert(message: message)
            }
        }
    }
}
// MARK: - TagListView delegates
extension CompetencyViewController: TagListViewDelegate {
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        var index = 1000
        var id: Int?
        for (j,i) in viewModel.competencies.enumerated() {
            if title == i.competency {
                index = j
                id = i.user_competency_id
                break
            }
        }
        viewModel.indexToDelete = index
        deleteCompetency(id: id ?? 0)
//        tagListView.removeTag(title)
//        interestViewHeight.constant = interestView.intrinsicContentSize.height
        
    }
    
}
// MARK: - Notification
extension CompetencyViewController {
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(valuesChanged(_:)), name: Notification.Name.Profile.competency, object: nil)
    }
    
    @objc func valuesChanged(_ notification: Notification) {
        getCompetency()
    }
}
