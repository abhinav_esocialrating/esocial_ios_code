//
//  CompetencyViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 21/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class CompetencyViewModel: NSObject {
    var showOrEdit = 1  // 1: Show   2: Edit
    var userType = 1   // 1: self   2: other
    var otherUserId = 0
    var searchedData = [[String:Any]]()
    var dropDownData = [String]()
    var competencies = [Competency]()
    var indexToDelete = 1000

    // MARK: - Search suggestions
    func getSearchConfig(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseSearchedData(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func getCompetency(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.competency(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayObject
                self.competencies = Competency.modelsFromDictionaryArray(array: (data ?? []) as NSArray)
//                do {
//                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
//                    let serviceArray = jsonData["data"] as! NSArray
//
//                } catch {
//
//                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func deleteCompetency(parameters: [String:Any], delete: Bool, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.competency(parameters: parameters, method: .delete) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, JSON.init(), json["message"].stringValue)
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            case 202:
                let data = json["data"].arrayObject
                self.competencies = Competency.modelsFromDictionaryArray(array: (data ?? []) as NSArray)
//                self.competencies.remove(at: self.indexToDelete)
                self.indexToDelete = 1000
                completion(true, JSON.init(), json["message"].stringValue)
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
        
    func saveCompetency(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.competency(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayObject
                self.competencies = Competency.modelsFromDictionaryArray(array: (data ?? []) as NSArray)
                completion(true, json, json["message"].stringValue)
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
        
    }
    
    // MARK: - Utility methods
    func parseSearchedData(data: [JSON]) {
        let search = data.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["id"] = json["id"].intValue
            dict["value"] = json["value"].string
            return dict
        }
        searchedData = search
        let drop = search.map { (dict) -> String in
            return (dict["value"] as? String) ?? ""
        }
        dropDownData = drop
    }
}
