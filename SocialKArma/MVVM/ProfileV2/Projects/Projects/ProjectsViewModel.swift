//
//  ProjectsViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 28/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProjectsViewModel: NSObject {
    
    var projects = [Project]()
    var userType = 1   // 1: self   2: other
    var otherUserId = 0

    // MARK: - Web Services
    func projects(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.projects(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayObject
                self.projects = Project.modelsFromDictionaryArray(array: (data ?? []) as NSArray)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
