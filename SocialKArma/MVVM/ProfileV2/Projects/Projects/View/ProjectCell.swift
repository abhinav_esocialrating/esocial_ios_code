//
//  ProjectCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 28/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ProjectCell: UITableViewCell {
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var currentlyWorkingLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var lowerView: UIView!
    @IBOutlet weak var endDateView: UIView!
    @IBOutlet weak var currentlyWorkingView: UIView!
    
    var project: Project? {
        didSet {
            setValues()
        }
    }
    var editTapped: ((_ index: Int) -> Void)? = nil


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setValues() {
        nameLabel.text = project?.project_name
        descLabel.text = project?.project_description
        currentlyWorkingLabel.text = (project?.currently_working ?? 0) == 1 ? "Yes" : "No"
        
        let currently_working = project?.currently_working
        endDateView.isHidden = currently_working == 1
        currentlyWorkingView.isHidden = currently_working != 1
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = .current
        let strt = formatter.date(from: project?.start_date ?? "2000-1-1")
        let enddte = formatter.date(from: project?.end_date ?? "2000-1-1")
        
        let monthFormatter = DateFormatter()
        monthFormatter.dateFormat = "LLLL, yyyy"
        monthFormatter.timeZone = .current
        startDateLabel.text = monthFormatter.string(from: strt!)
        endDateLabel.text = monthFormatter.string(from: enddte!)
    }
    @IBAction func editTapped(_ sender: Any) {
        editTapped?(indexPath.row)
    }
}
