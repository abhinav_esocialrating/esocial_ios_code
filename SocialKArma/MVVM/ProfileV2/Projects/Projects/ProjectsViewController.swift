//
//  ProjectsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 28/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ProjectsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var editBtn: UIButton!

    let viewModel = ProjectsViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        loadDataFromAPI()
        addNotificationObservers()
    }
    func configureView() {
        
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        tableView.layer.cornerRadius = 12
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        
        editBtn.isHidden = viewModel.userType == 2
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions
    @IBAction func editTapped(_ sender: Any) {
        openEditScreen(project: nil)
    }

    // MARK: - Methods
    func updateUI(success: Bool) {
        self.tableView.reloadData()
        self.tableView.isHidden = !success
        self.noDataLabel.isHidden = success
        self.noDataLabel.text = "Projects details not entered by the user"
    }
    func openEditScreen(project: Project?) {
        let vc = EditProjectsViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.viewModel.addEditOrDelete = 1
        if project != nil {
            vc.viewModel.project = project
            vc.viewModel.addEditOrDelete = 2
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    // MARK: - Web Services
    func loadDataFromAPI() {
        let parameters = ["id": viewModel.userType == 1 ? UserDefaults.standard.userId : "\(viewModel.otherUserId)"]
        viewModel.projects(parameters: parameters) { (success, _, message) in
            self.updateUI(success: self.viewModel.projects.count > 0)
        }
        
    }
}
// MARK: - TableView methods
extension ProjectsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.projects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ProjectCell.rawValue, for: indexPath) as! ProjectCell
        cell.indexPath = indexPath
        cell.project = viewModel.projects[indexPath.row]
        cell.editTapped = { index in
            self.openEditScreen(project: self.viewModel.projects[index])
        }
        cell.editButton.isHidden = viewModel.userType == 1 ? false : true
        cell.lowerView.isHidden = (viewModel.projects.count-1 == indexPath.row)

        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
}
// MARK: - Notification
extension ProjectsViewController {
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(valuesChanged(_:)), name: Notification.Name.Profile.projects, object: nil)
    }
    
    @objc func valuesChanged(_ notification: Notification) {
        loadDataFromAPI()
    }
}
