/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Project {
	public var user_project_id : Int?
	public var user_id : Int?
	public var project_name : String?
	public var start_date : String?
	public var end_date : String?
	public var currently_working : Int?
	public var project_description : String?
	public var project_link : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let Project_list = Project.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Project Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Project]
    {
        var models:[Project] = []
        for item in array
        {
            models.append(Project(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let Project = Project(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Project Instance.
*/
	required public init?(dictionary: NSDictionary) {

		user_project_id = dictionary["user_project_id"] as? Int
		user_id = dictionary["user_id"] as? Int
		project_name = dictionary["project_name"] as? String
		start_date = dictionary["start_date"] as? String
		end_date = dictionary["end_date"] as? String
		currently_working = dictionary["currently_working"] as? Int
		project_description = dictionary["project_description"] as? String
		project_link = dictionary["project_link"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.user_project_id, forKey: "user_project_id")
		dictionary.setValue(self.user_id, forKey: "user_id")
		dictionary.setValue(self.project_name, forKey: "project_name")
		dictionary.setValue(self.start_date, forKey: "start_date")
		dictionary.setValue(self.end_date, forKey: "end_date")
		dictionary.setValue(self.currently_working, forKey: "currently_working")
		dictionary.setValue(self.project_description, forKey: "project_description")
		dictionary.setValue(self.project_link, forKey: "project_link")

		return dictionary
	}

}
