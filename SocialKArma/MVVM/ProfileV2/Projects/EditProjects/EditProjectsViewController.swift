//
//  EditProjectsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 28/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class EditProjectsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var startDateTF: ACFloatingTextfield!
    @IBOutlet weak var endDateTF: ACFloatingTextfield!
    @IBOutlet weak var projNameTF: ACFloatingTextfield!
    @IBOutlet weak var projDescTF: ACFloatingTextfield!
    @IBOutlet weak var switchView: UIView!
    @IBOutlet weak var currentlyWorkingSwitch: UISwitch!
    @IBOutlet weak var dateErrorLabel: UILabel!
    @IBOutlet weak var dateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var contentView: UIView!

    // MARK: - Variables
    let viewModel = EditProjectsViewModel()
    var selectedStartDate: Date?
    var selectedEndDate: Date?
    var dateTextField: UITextField!

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        addInitialValues()
    }
    
    func configureView() {
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.lightGray, opacity: 0.6, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)

        for i in [projNameTF, projDescTF, startDateTF, endDateTF] {
            i?.lineColor = UIColor.gray.withAlphaComponent(0.7)
            i?.placeHolderColor = .gray
            i?.selectedLineColor = UIColor.SearchColors.blue
            i?.selectedPlaceHolderColor = .gray
            i?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
            i?.textColor = UIColor.NewTheme.darkGray
            i?.delegate = self
        }
       
        projNameTF.placeholder = viewModel.placeHolders[0]
        projDescTF.placeholder = viewModel.placeHolders[1]
        startDateTF.placeholder = viewModel.placeHolders[2]
        endDateTF.placeholder = viewModel.placeHolders[3]
    
        deleteButton.isHidden = viewModel.addEditOrDelete == 1 ? true : false
    }
    func addInitialValues() {
        if viewModel.addEditOrDelete != 1 {
            projNameTF.text = viewModel.project?.project_name
            projDescTF.text = viewModel.project?.project_description
            currentlyWorkingSwitch.isOn = (viewModel.project?.currently_working ?? 0) == 1 ? true : false
            
            if viewModel.project?.currently_working == 1 {
                endDateTF.text = ""
                enableEndDateTF(enabled: false)
                currentlyWorkingSwitch.setOn(true, animated: false)
            } else {
                endDateTF.text = viewModel.project?.end_date
                enableEndDateTF(enabled: true)
                currentlyWorkingSwitch.setOn(false, animated: false)
            }
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            formatter.timeZone = .current
            let strt = formatter.date(from: viewModel.project?.start_date ?? "2000-1-1")
            let enddte = formatter.date(from: viewModel.project?.end_date ?? "2000-1-1")
            selectedStartDate = strt
            selectedEndDate = enddte
            
            let monthFormatter = DateFormatter()
            monthFormatter.dateFormat = "LLLL, yyyy"
            monthFormatter.timeZone = .current
            startDateTF.text = monthFormatter.string(from: strt!)
            endDateTF.text = monthFormatter.string(from: enddte!)
            
            viewModel.user_project_id = viewModel.project?.user_project_id ?? 0
        }
    }
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if RechabilityHandler.shared.reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            projects(delete: false)
        }
    }
    @IBAction func deleteTapped(_ sender: Any) {
        alertWithTwoAction(message: "Are you sure you want to delete?", okHandler: {
            self.viewModel.addEditOrDelete = 3
            self.deleteProject()
        }) {}
    }
    @IBAction func switchChanged(_ sender: Any) {
        if (sender as! UISwitch).isOn {
            enableEndDateTF(enabled: false)
        } else {
            enableEndDateTF(enabled: true)
        }
    }
    // MARK: - Utility methods
    func enableEndDateTF(enabled: Bool) {
        endDateTF.isEnabled = enabled
        endDateTF.alpha = enabled ? 1.0 : 0.3
    }
    func isValid() -> Bool {
        var valid = true
        var message = ""
        if projNameTF.text!.isEmpty {
            valid = false
            message = "Please enter Company name"
            projNameTF.showErrorWithText(errorText: message)
        } else if projDescTF.text!.isEmpty {
            valid = false
            message = "Please enter Title"
            projDescTF.showErrorWithText(errorText: message)
        } else if startDateTF.text!.isEmpty {
            valid = false
            message = "Please enter start date"
            showDateError(message: message, show: true)
        } else if endDateTF.text!.isEmpty && !currentlyWorkingSwitch.isOn {
            valid = false
            message = "Please enter end date"
            showDateError(message: message, show: true)
        } else if !validateDate() {
            valid = false
            message = "Start date must be before or same as end date"
            showDateError(message: message, show: true)
        } else {
            projNameTF.errorTextColor = .clear
            projDescTF.errorTextColor = .clear
            startDateTF.errorTextColor = .clear
            endDateTF.errorTextColor = .clear
            showDateError(message: "", show: false)
        }
        return valid
    }
    func showDateError(message: String, show: Bool) {
        if show {
            dateErrorLabel.text = message
            dateViewHeight.constant = 64
        } else {
            dateErrorLabel.text = ""
            dateViewHeight.constant = 50
        }
    }
    func validateDate() -> Bool {
        if let selectedStartDate = selectedStartDate, let selectedEndDate = selectedEndDate {
            if selectedStartDate > selectedEndDate {
                return false
            }
        } else if let _ = selectedStartDate, currentlyWorkingSwitch.isOn {
            return true
        } else {
            return false
        }
        return true
    }
    func openDatePicker(textField: UITextField) {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        textField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(handleDatePicker(datePicker:)), for: .valueChanged)
    }
    @objc func handleDatePicker(datePicker: UIDatePicker) {
        showDateError(message: "", show: false)
        let date = datePicker.date
        formatDate(date: date)
    }
    func formatDate(date: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = .current
        if dateTextField == startDateTF {
//            startDateTF.text = formatter.string(from: date)
            selectedStartDate = date
        } else {
//            endDateTF.text = formatter.string(from: date)
            selectedEndDate = date
        }
        let monthFormatter = DateFormatter()
        monthFormatter.dateFormat = "LLLL, yyyy"
        monthFormatter.timeZone = .current
        if dateTextField == startDateTF {
            startDateTF.text = monthFormatter.string(from: date)
        } else {
            endDateTF.text = monthFormatter.string(from: date)
        }
    }
    func getDateForAPI(date: Date?) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = .current
        let strt = formatter.string(from: date ?? Date())
        return strt
    }
    // MARK: - Web Services
    func projects(delete: Bool) {
        if isValid() {
            view.endEditing(true)
            var params: [String:Any] = [:]
            params["project_name"] = projNameTF.text!
            params["project_description"] = projDescTF.text!
            params["start_date"] = getDateForAPI(date: selectedStartDate)
            params["end_date"] = getDateForAPI(date: selectedEndDate)
            params["currently_working"] = currentlyWorkingSwitch.isOn
            params["project_link"] = ""
            if viewModel.addEditOrDelete != 1 {
                params["user_project_id"] = viewModel.user_project_id
            }
            AppDebug.shared.consolePrint(params)
            self.showHud()
            viewModel.projects(parameters: params) { (success, json, message) in
                self.hideHud()
                if success {
                    if self.viewModel.addEditOrDelete == 2 || self.viewModel.addEditOrDelete == 1 {
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
                        }
                    } else if self.viewModel.addEditOrDelete == 3 {
                        self.alertWithAction(message: "Successfully Deleted!", handler: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                } else {
                    self.alert(message: message)
                }
            }
        }
    }
    func deleteProject() {
        view.endEditing(true)
        viewModel.addEditOrDelete = 3
        var params: [String:Any] = [:]
        params = ["user_project_id": viewModel.user_project_id]
        viewModel.projects(parameters: params) { (success, json, message) in
            if success {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.alert(message: message)
            }
        }
    }
}
// MARK: - TextField delegate
extension EditProjectsViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == startDateTF || textField == endDateTF {
            dateTextField = textField
            
            openDatePicker(textField: textField)
        }
        return true
    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if viewModel.screenType == 1 {
//            if textField == startDateTF {
//                if textField.text == "" {
//                    textField.text = "\(viewModel.years[0])"
//                }
//            } else if textField == endDateTF {
//                if textField.text == "" {
//                    textField.text = "\(viewModel.endYear[0])"
//                }
//            }
//        }
//    }
}
