//
//  EditProjectsViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 28/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditProjectsViewModel: NSObject {

    let placeHolders = ["Project Name", "Project Description", "Start Date", "End Date"]
    var addEditOrDelete = 1  // 1: Add   2: Edit    3: Delete
    var user_project_id = 0
    var project: Project?

    // MARK: - Web Services
    func projects(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        var method = HTTPMethod.delete
        switch addEditOrDelete {
        case 1:
            method = .post
        case 2:
            method = .put
        default:
            method = .delete
        }
        NetworkManager.projects(parameters: parameters, method: method) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            switch response.response?.statusCode {
            case 200:
                NotificationCenter.default.post(name: Notification.Name.Profile.projects, object: self)
                completion(true, json, "")
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            case 202:
                NotificationCenter.default.post(name: Notification.Name.Profile.projects, object: self)
                completion(true, json, "")
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
