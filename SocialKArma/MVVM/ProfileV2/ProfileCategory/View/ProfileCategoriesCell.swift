//
//  ProfileCategoriesCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 24/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileCategoriesCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var category: ProfileCategories! {
        didSet {
            setValues()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: imgView.frame.size.width, height: imgView.frame.size.height)
        imgView.layer.insertSublayer(gradient, at: 0)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        imgView.layoutSubviews()
        imgView.layer.cornerRadius = 12
    }
    
    func setValues() {
        nameLabel.text = category.page_name
        if let url = URL(string: category.pageUrl) {
            let transformer = SDImageResizingTransformer(size: CGSize(width: 300, height: 300), scaleMode: .fill)
            imgView.sd_setImage(with: url, placeholderImage: UIImage(), options: [], context: [.imageTransformer: transformer])
        } else {
            imgView.image = UIImage()
        }
    }
}
