//
//  ProfileCategoriesViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 24/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ProfileCategoriesViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var friendsList = [String: [String]]()
    var serviceEmployList = [String: [String]]()

    var otherUserId = 0
    var userType = 1  // 1: Self   2: Other User
    var dataSource = [ProfileCategories]()
    var imgArray = [
        "https://trustzedev.s3.ap-south-1.amazonaws.com/profile_categories/B.jpg",
        "https://trustzedev.s3.ap-south-1.amazonaws.com/profile_categories/Compensation+1.jpg",
        "https://trustzedev.s3.ap-south-1.amazonaws.com/profile_categories/DetailedFeedback1.jpg",
        "https://trustzedev.s3.ap-south-1.amazonaws.com/profile_categories/Interests1.jpg",
        "https://trustzedev.s3.ap-south-1.amazonaws.com/profile_categories/Personal1.jpg",
        "https://trustzedev.s3.ap-south-1.amazonaws.com/profile_categories/Professional1.jpg",
        "https://trustzedev.s3.ap-south-1.amazonaws.com/profile_categories/Socialaccounts1.jpg",
        "https://trustzedev.s3.ap-south-1.amazonaws.com/profile_categories/looking+for+1.jpg",
        "https://trustzedev.s3.ap-south-1.amazonaws.com/profile_categories/skills%26comp1.jpg",
        "https://trustzedev.s3.ap-south-1.amazonaws.com/profile_categories/tagging1.jpg",
        "https://trustzedev.s3.ap-south-1.amazonaws.com/profile_categories/Education+details1.jpg"
    ]
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getLookingFor()
    }
    func configureView() {
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    func setUpDataSource() {
        dataSource = []
        var ids = [Int]()
        if userType == 1 {
            ids = ProfileV2Data.sharedInstance.lookingForSelected.map { (json) -> Int in
                let dict = json.dictionaryValue
                let id = dict["id"]?.intValue
                return id ?? 0
            }
        } else {
            ids = ProfileV2Data.sharedInstance.otherLookingForSelected.map { (json) -> Int in
                let dict = json.dictionaryValue
                let id = dict["id"]?.intValue
                return id ?? 0
            }
        }
        if userType == 1 {
            dataSource.append(ProfileCategories(page_name: "I'm Looking For", pageUrl: imgArray[7], page_type: ProfileCategoryTypes.LookingFor))
        }
        if ids.contains(10) || ids.contains(8) || ids.contains(9) || ids.contains(11) {
            
            dataSource.append(ProfileCategories(page_name: "Personal Details", pageUrl: imgArray[4], page_type: ProfileCategoryTypes.PersonalDetails))
            dataSource.append(ProfileCategories(page_name: "Interests", pageUrl: imgArray[3], page_type: ProfileCategoryTypes.Interests))
        }
        if ids.contains(2) || ids.contains(3) || ids.contains(4) || ids.contains(5) || ids.contains(11) {
            
            dataSource.append(ProfileCategories(page_name: "Professional Details", pageUrl: imgArray[5], page_type: ProfileCategoryTypes.ProfessionalDetails))
            dataSource.append(ProfileCategories(page_name: "Education Details", pageUrl: imgArray[10], page_type: ProfileCategoryTypes.EducationDetails))
            dataSource.append(ProfileCategories(page_name: "Skill and Competencies", pageUrl: imgArray[8], page_type: ProfileCategoryTypes.SkillAndCompetencies))
            dataSource.append(ProfileCategories(page_name: "Compensation Details", pageUrl: imgArray[1], page_type: ProfileCategoryTypes.CompensationDetails))
        }
        
        dataSource.append(ProfileCategories(page_name: "Biography", pageUrl: imgArray[0], page_type: ProfileCategoryTypes.Biography))
        dataSource.append(ProfileCategories(page_name: "Detailed Feedback", pageUrl: imgArray[2], page_type: ProfileCategoryTypes.DetailedFeedback))
        dataSource.append(ProfileCategories(page_name: "My Social Media", pageUrl: imgArray[6], page_type: ProfileCategoryTypes.MySocialMedia))
        dataSource.append(ProfileCategories(page_name: "Tagging", pageUrl: imgArray[9], page_type: ProfileCategoryTypes.Tagging))
        
        collectionView.reloadData()
    }
    // MARK: - Navigation
    
    @IBAction func backTapped(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    // MARK: - Web Services
    func getLookingFor() {
        let params = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        ProfileV2Data.sharedInstance.getLookingFor(parameters: params) { (success, json, message) in
            self.setUpDataSource()
        }
    }
    

}
extension ProfileCategoriesViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCells.ProfileCategoriesCell.rawValue, for: indexPath) as! ProfileCategoriesCell
        cell.category = dataSource[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (screenWidth-48)/2
        return CGSize(width: width, height: width*0.8)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = ProfileBaseViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.userType = userType
        vc.otherUserId = otherUserId
        vc.profileCategory = dataSource[indexPath.row].page_type
        ProfileV2Data.sharedInstance.userType = userType
        ProfileV2Data.sharedInstance.otherUserId = otherUserId
        navigationController?.pushViewController(vc, animated: true)
    }
}
