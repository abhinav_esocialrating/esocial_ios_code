//
//  ProfileCategories.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 25/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

struct ProfileCategories {
    var page_name: String
    var pageUrl: String
    var page_type: ProfileCategoryTypes
}
enum ProfileCategoryTypes {
    case PersonalDetails, ProfessionalDetails, EducationDetails, SkillAndCompetencies, CompensationDetails, Biography, DetailedFeedback, MySocialMedia, Tagging, LookingFor, Interests
}
