//
//  EditCertificateViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 14/07/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class EditCertificateViewModel: NSObject {
    var addEditOrDelete = 1  // 1: Add   2: Edit    3: Delete
    var isOrgFromDropDown = false
    var dropDownData = [String]()
    var certificateDict: [String: Int] = ["organisation_id":0, "degree_id":0, "degree_field_id": 0]
    var organisationData = [[String:Any]]()

}
