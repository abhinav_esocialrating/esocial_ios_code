//
//  EditCertificateViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 14/07/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class EditCertificateViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var nameTF: ACFloatingTextfield!
    @IBOutlet weak var organisationNameTF: ACFloatingTextfield!
    @IBOutlet weak var issueDateTF: ACFloatingTextfield!
    @IBOutlet weak var endDateTF: ACFloatingTextfield!
    @IBOutlet weak var credentialIdTF: ACFloatingTextfield!
    @IBOutlet weak var credentialUrlTF: ACFloatingTextfield!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var noExpirySwitch: UISwitch!
    @IBOutlet weak var dateErrorLabel: UILabel!
    @IBOutlet weak var dateViewHeight: NSLayoutConstraint!

    // MARK: - Variables
    let dropDown = DropDown()
    let viewModel = EditCertificateViewModel()
    var selectedStartDate: Date?
    var selectedEndDate: Date?
    var dateTextField: UITextField!

    // MARK: - ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarController?.tabBar.isHidden = true
        setInitialValues()
        configureView()
        
    }
    

    
    // MARK: - Setup view
    func configureView() {
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.lightGray, opacity: 0.6, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)

//        typeLabel.text = viewModel.screenType == 1 ? "Education" : "Experience"
        nameTF.addTarget(self, action: #selector(textfieldTextChanged(_:)), for: .editingChanged)
        organisationNameTF.addTarget(self, action: #selector(textfieldTextChanged(_:)), for: .editingChanged)
        issueDateTF.addTarget(self, action: #selector(textfieldTextChanged(_:)), for: .editingChanged)
        endDateTF.addTarget(self, action: #selector(textfieldTextChanged(_:)), for: .editingChanged)

        for i in [nameTF, organisationNameTF, issueDateTF, endDateTF, credentialIdTF, credentialUrlTF] {
            i?.lineColor = UIColor.gray.withAlphaComponent(0.7)
            i?.placeHolderColor = .gray
            i?.selectedLineColor = UIColor.SearchColors.blue
            i?.selectedPlaceHolderColor = .gray
            i?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
            i?.textColor = UIColor.NewTheme.darkGray
        }
        
        deleteButton.isHidden = viewModel.addEditOrDelete == 1 ? true : false
    }

    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @objc func textfieldTextChanged(_ textField: UITextField) {
        if textField == organisationNameTF {
            
        }
    }
    @IBAction func saveTapped(_ sender: Any) {
        if RechabilityHandler.shared.reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
//            saveEducation(delete: false)
        }
    }
    @IBAction func deleteTapped(_ sender: Any) {
        alertWithTwoAction(message: "Are you sure you want to delete?", okHandler: {
            self.viewModel.addEditOrDelete = 3
//            self.deleteEducation()
        }) {}
    }
    @IBAction func switchChanged(_ sender: Any) {
        if (sender as! UISwitch).isOn {
            enableEndDateTF(enabled: false)
        } else {
            enableEndDateTF(enabled: true)
        }
    }
}
// MARK: - Utility methods
extension EditCertificateViewController {
    func setInitialValues() {
        nameTF.text = ""
        organisationNameTF.text = ""
        issueDateTF.text = ""
        endDateTF.text = ""
        credentialIdTF.text = ""
        credentialUrlTF.text = ""
    }
    func enableEndDateTF(enabled: Bool) {
        endDateTF.isEnabled = enabled
        endDateTF.alpha = enabled ? 1.0 : 0.3
    }
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.dataSource = viewModel.dropDownData
        
        dropDown.selectionAction = { (index: Int, item: String) in
            textField.text = item
            self.handleDropDownSelection(index: index, item: item, textField: textField)
        }
        dropDown.show()
    }
    func handleDropDownSelection(index: Int, item: String, textField: UITextField) {
        textField.text = item
        switch textField {
        case organisationNameTF:
            self.viewModel.isOrgFromDropDown = true
        default:
            print("Do nothing")
        }
        self.updateDataForParams(index: index, textField: textField)
    }
    func updateDataForParams(index: Int, textField: UITextField) {
        switch textField {
        case organisationNameTF:
            viewModel.certificateDict["organisation_id"] = (viewModel.organisationData[index]["organisation_id"] as! Int)
        default:
            print("Do nothing!")
        }
    }
    func isValid() -> Bool {
        var valid = true
        var message = ""
        if nameTF.text!.isEmpty {
            valid = false
            message = "Please enter Certificate/License number"
            nameTF.showErrorWithText(errorText: message)
        } else if organisationNameTF.text!.isEmpty {
            valid = false
            message = "Please enter organisation"
            organisationNameTF.showErrorWithText(errorText: message)
        } else if issueDateTF.text!.isEmpty {
            valid = false
            message = "Please enter start date"
            showDateError(message: message, show: true)
//            startDateTF.showErrorWithText(errorText: message)
        } else if endDateTF.text!.isEmpty && !noExpirySwitch.isOn {
            valid = false
            message = "Please enter end date"
//            endDateTF.showErrorWithText(errorText: message)
            showDateError(message: message, show: true)
        } else if !validateDate() {
            valid = false
            message = "Start date must be before or same as end date"
            showDateError(message: message, show: true)
        } else {
            nameTF.errorTextColor = .clear
            organisationNameTF.errorTextColor = .clear
            issueDateTF.errorTextColor = .clear
            endDateTF.errorTextColor = .clear
            showDateError(message: "", show: false)
        }
        return valid
    }
    func showDateError(message: String, show: Bool) {
        if show {
            dateErrorLabel.text = message
            dateViewHeight.constant = 84
        } else {
            dateErrorLabel.text = ""
            dateViewHeight.constant = 64
        }
    }
    func validateDate() -> Bool {
        if let selectedStartDate = selectedStartDate, let selectedEndDate = selectedEndDate {
            if selectedStartDate > selectedEndDate {
                return false
            }
        } else if let _ = selectedStartDate, noExpirySwitch.isOn {
            return true
        } else {
            return false
        }
        return true
    }
}
// MARK: - TextField delegate
extension EditCertificateViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == issueDateTF || textField == endDateTF {
            dateTextField = textField
            openDatePicker(textField: textField)
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    func openDatePicker(textField: UITextField) {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        textField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(handleDatePicker(datePicker:)), for: .valueChanged)
    }

    @objc func handleDatePicker(datePicker: UIDatePicker) {
        showDateError(message: "", show: false)
        let date = datePicker.date
        formatDate(date: date)
    }
    func formatDate(date: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = .current
        if dateTextField == issueDateTF {
//            startDateTF.text = formatter.string(from: date)
            selectedStartDate = date
        } else {
//            endDateTF.text = formatter.string(from: date)
            selectedEndDate = date
        }
        let monthFormatter = DateFormatter()
        monthFormatter.dateFormat = "LLLL, yyyy"
        monthFormatter.timeZone = .current
        if dateTextField == issueDateTF {
            issueDateTF.text = monthFormatter.string(from: date)
        } else {
            endDateTF.text = monthFormatter.string(from: date)
        }
    }
}
/*
 {
             "certificate_name":"Node Js Advanced",
             "organisation_id":"11",
             "issue_date": "2013-12-01",
             "expiration_datr": "2022-12-01",
             "location":"Dehradun ",
             "credential_url":"https://www.linkedin.com/in/siddharth-bisht-4739958b/",
             "credential_id":"sdjfjh1",
             "does_expire":1
             
 }
 */
