/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Certification {
	public var training_name : String?
	public var organisation_id : Int?
	public var start_year : String?
	public var end_year : String?
	public var location : String?
	public var currently_training : Int?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let Certification_list = Certification.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Certification Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Certification]
    {
        var models:[Certification] = []
        for item in array
        {
            models.append(Certification(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let Certification = Certification(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Certification Instance.
*/
	required public init?(dictionary: NSDictionary) {

		training_name = dictionary["training_name"] as? String
		organisation_id = dictionary["organisation_id"] as? Int
		start_year = dictionary["start_year"] as? String
		end_year = dictionary["end_year"] as? String
		location = dictionary["location"] as? String
		currently_training = dictionary["currently_training"] as? Int
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.training_name, forKey: "training_name")
		dictionary.setValue(self.organisation_id, forKey: "organisation_id")
		dictionary.setValue(self.start_year, forKey: "start_year")
		dictionary.setValue(self.end_year, forKey: "end_year")
		dictionary.setValue(self.location, forKey: "location")
		dictionary.setValue(self.currently_training, forKey: "currently_training")

		return dictionary
	}

}
