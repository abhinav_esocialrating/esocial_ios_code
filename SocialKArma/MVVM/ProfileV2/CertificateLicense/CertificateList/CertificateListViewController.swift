//
//  CertificateListViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 14/07/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CertificateListViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = CertificateListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        
    }
    func configureView() {
        
        tableView.registerFromXib(name: TableViewCells.ExperienceHeaderCell.rawValue)
        
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        tableView.layer.cornerRadius = 12
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - TableView methods
extension CertificateListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.certifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.EducationListCell.rawValue, for: indexPath) as! EducationListCell
//        cell.indexPath = indexPath
//        cell.listViewModel = viewModel
//        cell.education = viewModel.educationDataSource[indexPath.row]
//        cell.editTapped = { index in
//            self.openEditEducation(education: self.viewModel.educationDataSource[indexPath.row], experience: nil, index: indexPath.row)
//        }
//        cell.editButton.isHidden = userType == 1 ? false : true
//        cell.lowerView.isHidden = (viewModel.educationDataSource.count-1 == indexPath.row)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ExperienceHeaderCell.rawValue) as! ExperienceHeaderCell
//        cell.indexPath = IndexPath(row: 0, section: section)
//        cell.editButton.isHidden = userType == 2
//        cell.editImage.isHidden = userType == 2
//        cell.editImage.applyTemplate(image: UIImage(named: "Add-icon-white"), color: UIColor.NewTheme.darkGray)
//        cell.label.text = viewModel.screenType == 1 ? "My Education" : "My Experience"
//        cell.lowerView.isHidden = true
//        cell.editTapped = { index in
//            self.openEditScreen()
//        }
//        cell.label.textColor = UIColor.black
        return cell
    }
}
