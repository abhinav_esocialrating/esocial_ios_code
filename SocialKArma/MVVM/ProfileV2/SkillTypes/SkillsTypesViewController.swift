//
//  SkillsTypesViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 20/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class SkillsTypesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var editBtn: UIButton!

    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        addNotificationObservers()
        getAllSkills()
        configureView()
    }
    func configureView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        editBtn.isHidden = viewModel.userType == 2
    }
    // MARK: - Variables
    let viewModel = SkillTypesViewModel()


    // MARK: - Actions
    @IBAction func editTapped(_ sender: Any) {
        openEditScreen()
    }

    // MARK: - Web Services
    func getAllSkills() {
        let parameters = ["id": viewModel.userType == 1 ? UserDefaults.standard.userId : "\(viewModel.otherUserId)"]
        viewModel.getAllSkills(parameters: parameters) { (success, json, message) in
            if success {
                self.updateUI()
            } else {
                self.alert(message: message)
            }
        }
    }
    // MARK: - Methods
    func updateUI() {
        self.tableView.reloadData()
        var count = 0
        viewModel.skills.forEach({ count += $0.skills?.count ?? 0 })
        noDataLabel.isHidden = count > 0
        tableView.isHidden = count == 0
    }
    func openEditScreen() {
        let vc = SkillsViewController.instantiate(fromAppStoryboard: .Profile)
        vc.screenType = 1
        vc.userType = viewModel.userType
        vc.otherUserId = viewModel.otherUserId
        vc.skillsTypes = viewModel.skills
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension SkillsTypesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.skills.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.SkillTypesCell.rawValue, for: indexPath) as! SkillTypesCell
        cell.skills = viewModel.skills[indexPath.section].skills
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.skills[section].skill_category?.capitalized
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
}
// MARK: - Notification
extension SkillsTypesViewController {
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(valuesChanged(_:)), name: Notification.Name.Profile.SkillsWithTypes, object: nil)
    }
    
    @objc func valuesChanged(_ notification: Notification) {
        getAllSkills()
    }
}
