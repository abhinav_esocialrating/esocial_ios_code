//
//  SkillTypesCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 22/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class SkillTypesCell: UITableViewCell {
    
    @IBOutlet weak var tagListView: TagListView!
    var skills: [SkillModel]? {
        didSet {
            setValues()
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        tagListView.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
    }

    func setValues() {
        guard let skills = skills else { return }
        tagListView.removeAllTags()
        for i in skills {
            tagListView.addTag(i.skill ?? "")
        }
    }

}
