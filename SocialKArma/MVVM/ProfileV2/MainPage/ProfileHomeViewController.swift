//
//  ProfileHomeViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 22/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import UICircularProgressRing
import SwiftyJSON
import FAPanels
import DropDown
import FirebaseAnalytics

class ProfileHomeViewController: UIViewController {
    
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var nameLabek: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var reviewersLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var medalStack: UIStackView!
    @IBOutlet weak var progressRing: UICircularProgressRing!
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var pointsBtn: UIButton!
    @IBOutlet weak var notificationBadge: UILabel!
    @IBOutlet weak var chatNotificationBadge: UILabel!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var reportBtn: UIButton!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var msgBtn: UIButton!
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var searchStatusBtn: UIButton!

    // MARK: - Variables
    var otherUserId = 0
    var userType = 1  // 1: Self   2: Other User
    var userInfo = [String:Any]()
    let dropDown = DropDown()
    var dropDownData = ["Report"]

    // MARK: - Life Cycle
    /**
    Life Cycle method called when view is loaded.
    */
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        if userType == 1 {
            addPicEditNotification()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(searchStatus(notification:)), name: Notification.Name.SearchStatus, object: nil)

    }
    /**
    Life Cycle method called when view is loaded and about to appear on the screen.
    */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let params = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        getUserInfo(parameters: params)
        
        userImg.clipsToBounds = true
        
        if userType == 1 {
            setNotificationBadges()
            searchStatusBtn.tintColor = (UserDefaults.standard.SearchStatus["friendStatus"] as! Int == 1 || UserDefaults.standard.SearchStatus["professionalStatus"] as! Int == 1) ? .green : .red
        }
        getAboutMeInfo(parameters: params)
        
    }
    /**
    Life cycle method when view has appeared fully on screen
    */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Screen tracking
        Analytics.logEvent("screen_tracking", parameters: [
            "name": "TabClass" as NSObject,
            "full_text": "ProfileHomeViewController" as NSObject
        ])
    }
    /**
    Setting up ui of the app and adding targets to different controls that needs to be done after view has loaded
    */
    func configureView() {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.withAlphaComponent(0.99).cgColor]
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: userImg.frame.size.width, height: userImg.frame.size.height)
        userImg.layer.insertSublayer(gradient, at: 0)
        
        let image = userType == 1 ? UIImage(named: "menu") : UIImage(named: "Back-Arrow-icon-Grey")
        menuBtn.setImage(image, for: .normal)
        
        editProfileBtn.isHidden = userType == 2

        if userType == 1 {
            notificationBadge.isHidden = PointsHandler.shared.isNotification ? false : true
            chatNotificationBadge.isHidden = PointsHandler.shared.isChatNotificationBadge ? false : true
            reportBtn.isHidden = true
        } else {
            notificationBadge.isHidden = true
            pointsBtn.isHidden = true
            notificationBtn.isHidden = true
            reportBtn.isHidden = false
            msgBtn.isHidden = true
            infoBtn.isHidden = true
        }
        
        notificationBadge.layer.cornerRadius = notificationBadge.frame.width/2
        notificationBadge.clipsToBounds = true
        chatNotificationBadge.layer.cornerRadius = chatNotificationBadge.frame.width/2
        chatNotificationBadge.clipsToBounds = true
        
    }
    // MARK: - Methods
    /**
     Open Profile Category screen where user can select which section of profile user wants to open e.g., Education, Profession etc.
     */
    func openProfileCategories() {
        let vc = ProfileCategoriesViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.userType = userType
        vc.otherUserId = otherUserId
        ProfileV2Data.sharedInstance.userType = userType
        ProfileV2Data.sharedInstance.otherUserId = otherUserId
        navigationController?.pushViewController(vc, animated: true)
    }
    /**
    Set values of user details on UI after fetching from API or local storage
    */
    func setValues() {
        // name score reviews medals image_url
        if let name = userInfo["name"] as? String {
            nameLabek.text = name
        }
        if let score = userInfo["score"] as? Double {
            scoreLabel.text = "Goodness Score: " + String(format: "%.1f", score)
        }
        if let score = userInfo["reviews"] as? Int {
            reviewersLabel.text = "Feedback: \(score)"//String(format: "Goodness score: %.1f", score)
        }
        if let url = userInfo["image_id"] as? String, url != "" {
            userImg.sd_setImage(with: URL(string: url)!, completed: nil)
        } else {
            userImg.image = UIImage(named: CommonData.avatars.randomElement() ?? "")
        }
        if let medals = userInfo["medals"] as? [Any] {
            medalStack.arrangedSubviews.forEach({$0.removeFromSuperview()})
            medals.forEach { (medal) in
                if let medal = medal as? [String: Any], let image_url = medal["image_url"] as? String, image_url != "" {
                    let img = UIImageView()
                    medalStack.addArrangedSubview(img)
                    img.anchor(top: nil, paddingTop: 0, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, width: 20, height: 20)
                    img.sd_setImage(with: URL(string: image_url), completed: nil)
                }
            }
        }
        if let profile_percentage = userInfo["profile_percentage"] as? CGFloat {
            progressRing.endAngle = (profile_percentage*3.6) - 90
            percentLabel.text = "\(Int(profile_percentage))% Profile completed"
        }
    }
    /**
    Method for opening a dropdown of some values
    */
    func showDropDown() {
        dropDown.anchorView = reportBtn
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 200
        dropDown.dataSource = dropDownData
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropSelection(index: index, item: item)
        }
        dropDown.show()
    }
    /**
    Method for handling dropdown selection
     
     - Parameter index: index of seleted item
     - Parameter item: String value of the selected item
    */
    func handleDropSelection(index: Int, item: String) {
        switch index {
        case 0:
            openReport()
        default:
            print("")
        }
    }
    /**
    Opening screen where user can report other users if other used is misbehaving or sharing obscence content.
    */
    func openReport() {
        let vc = ReportUserViewController.instantiate(fromAppStoryboard: .MainExt)
        vc.viewModel.otherUserId = "\(otherUserId)"
        vc.modalPresentationStyle = .overCurrentContext
        vc.notify = {
            self.tabBarController?.tabBar.isHidden = false
        }
        present(vc, animated: false, completion: nil)
        tabBarController?.tabBar.isHidden = true
    }
    /**
    Opening user's basic details screen, where he can edit basic details like name, age
    */
    func openBasicDetails() {
        let vc = EditInfoViewController.instantiate(fromAppStoryboard: .Profile)
        vc.screenType = 2
        navigationController?.pushViewController(vc, animated: true)
    }
    /**
    Setting DOB and location of user on UI
    */
    func setDOBandLocation(list: [String: JSON]) {
        var distanceAndAge = ""
        if userType == 1 {
            if let age = list["dob"]?.string, age != "" {
                let formatter = DateFormatter()
                formatter.timeZone = TimeZone.init(abbreviation: "UTC")
                formatter.locale = Locale.current
                formatter.dateFormat = "yyyy-MM-dd"
                let fromDate = formatter.date(from: age) ?? Date()
                let ageYears = Utilities().days(from: fromDate, to: Date())
                distanceAndAge = "\(ageYears)"
            }
            if let loc = list["location_city"]?.string {
                if distanceAndAge == "" {
                    distanceAndAge = loc
                } else {
                    distanceAndAge.append("  \u{2022}  \(loc)")
                }
            }
            locationLabel.text = distanceAndAge
        }
        else {
            if let age = list["dob"]?.string, age != "" {
                let formatter = DateFormatter()
                formatter.timeZone = TimeZone.init(abbreviation: "UTC")
                formatter.locale = Locale.current
                formatter.dateFormat = "yyyy-MM-dd"
                let fromDate = formatter.date(from: age) ?? Date()
                let ageYears = Utilities().days(from: fromDate, to: Date())
                distanceAndAge = "\(ageYears)"
            }
            if let loc = list["location_city"]?.string {
                if distanceAndAge == "" {
                    distanceAndAge = loc
                } else {
                    distanceAndAge.append("  \u{2022}  \(loc)")
                }
            }
            locationLabel.text = distanceAndAge
        }
//        list["DOB"] = list["dob"]?.stringValue ?? ""
//        list["Location"] = list["location_city"]?.stringValue ?? ""
    }
    // MARK: - Actions
    @IBAction func seeFullProfileAction(_ sender: Any) {
        openProfileCategories()
    }
    @IBAction func sideMenuAction(_ sender: Any) {
        if userType == 2 {
            navigationController?.popViewController(animated: true)
        } else {
            if let panel = appDelegate?.window?.rootViewController as? FAPanelController {
                panel.openLeft(animated: true)
            }
        }
    }
    @IBAction func notificationTapped(_ sender: Any) {
        openNotificationFromNavBar()
    }
    @IBAction func pointsBtnTapped(_ sender: Any) {
        openPointsScreenFromNavBar()
    }
    @IBAction func reportTapped(_ sender: Any) {
        showDropDown()
    }
    
    @IBAction func messageTapped(_ sender: Any) {
        openChatFromNavBar()
    }
    @IBAction func infoTapped(_ sender: Any) {
//        openInfoFromNavBar(info: Constants.ProfileInfo.rawValue)
        openVideoPlayer()
    }
    @IBAction func editBasicDetailsTapped(_ sender: Any) {
        self.openBasicDetails()
    }
    @IBAction func activelyLookingTapped(_ sender: Any) {
        openActivelyLooking()
    }
    /**
     Selector for observing changes in the user's active status for searching friends or professional jobs/works
     */
    @objc func searchStatus(notification: Notification) {
        searchStatusBtn.tintColor = (UserDefaults.standard.SearchStatus["friendStatus"] as! Int == 1 || UserDefaults.standard.SearchStatus["professionalStatus"] as! Int == 1) ? .green : .red
    }

    // MARK: - Web Services
    /**
    API calling function for getting user's basic details
          
    - Parameter parameters: Parameter dictionary containing message and other info.
    */
    func getUserInfo(parameters: [String:Any]) {

        NetworkManager.getUserInfo(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"]
                let dict = data.dictionaryObject
                self.userInfo = dict ?? [:]
                self.setValues()
            default:
                _ = json["message"].stringValue
            }
        }
    }
    /**
    API calling function for getting user's Personal detail info.
          
    - Parameter parameters: Parameter dictionary containing message and other info.
    */
    func getAboutMeInfo(parameters: [String:Any]) {
        
        NetworkManager.getAboutMeInfo(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                let list = json["data"].dictionaryValue
                self.setDOBandLocation(list: list)
                
            default:
                _ = json["message"].stringValue
            }
        }
    }
}
// MARK: - NavBar
extension ProfileHomeViewController {
    // Notification Handling- To be moved to a separate class soon
    func setUpNavBar() {
        NotificationCenter.default.addObserver(self, selector: #selector(pushNotificationReceived(_:)), name: Notification.Name.PushReceived, object: nil)
    }
    @objc func pushNotificationReceived(_ notification: Notification) {
        if let type = notification.userInfo?["type"] as? String {
            if type == PushNotificationType.FEEDBACK_UE.rawValue || type == PushNotificationType.FEEDBACK_UE_2.rawValue || type == PushNotificationType.CONTACT_UE.rawValue || type == PushNotificationType.INVITE_UE.rawValue || type == PushNotificationType.PROFILE_UE.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_1.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_2.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_3.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_4.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_5.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_6.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_7.rawValue || type == PushNotificationType.UE_INFO_1.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_1.rawValue || type == PushNotificationType.UE_REINVITE_1.rawValue || type == PushNotificationType.UE_REINVITE_2.rawValue || type == PushNotificationType.CHAT_UNREAD_MESSAGES.rawValue {  }
            else if type == PushNotificationType.CHAT.rawValue {
//                PointsHandler.shared.isChatNotificationBadge = true
//                chatNotificationBadge.isHidden = false
                setNotificationBadges()
            }
            else {
//                PointsHandler.shared.isNotification = true
//                notificationBadge.isHidden = false
                setNotificationBadges()
            }
        }
    }
    /**
    Setting badges on Notificaiton and Chat button to show number of unread notifications and numbers.
    */
    func setNotificationBadges() {
        PointsHandler.shared.getNotificationCount { notificationCountData in
            
            if let notifications = notificationCountData["notification"] as? Int, notifications > 0 {
                PointsHandler.shared.isNotification = true
                self.notificationBadge.isHidden = false
                self.notificationBadge.text = "\(notifications)"
            } else {
                self.notificationBadge.isHidden = PointsHandler.shared.isNotification ? false : true
            }
            if let chat = notificationCountData["chat"] as? Int, chat > 0 {
                PointsHandler.shared.isChatNotificationBadge = true
                self.chatNotificationBadge.isHidden = false
                self.chatNotificationBadge.text = "\(chat)"
            } else {
                self.chatNotificationBadge.isHidden = PointsHandler.shared.isChatNotificationBadge ? false : true
            }
        }
    }
}
// MARK: - Name Changed
extension ProfileHomeViewController {
    /**
     Addding observer for updating user's profile picture
     */
    func addPicEditNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(profilePicChanged), name: Notification.Name.AppNotifications.ProfilePicChanged, object: nil)
    }
    /**
    selector for observing user's profile picture update observer
    */
    @objc func profilePicChanged() {
        if userType == 1 {
            handleUserInfo()
        }
    }
    /**
    Fetching and updating user's basic details from UserDefaults
    */
    func handleUserInfo() {
        let dsv = UserDefaults.standard.userInfo
        let data = dsv.data(using: .utf8)
        let json = JSON(data ?? Data())
        let dict = json.dictionaryObject
        userInfo = dict ?? [String: Any]()
        setValues()
        if let url = userInfo["image_id"] as? String, url != "" {
            userImg.sd_setImage(with: URL(string: url)!, completed: nil)
        } else {
            userImg.image = UIImage(named: CommonData.avatars.randomElement() ?? "")
        }
    }
}
