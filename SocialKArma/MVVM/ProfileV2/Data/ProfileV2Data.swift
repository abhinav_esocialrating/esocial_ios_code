//
//  ProfileV2Data.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
import SwiftyJSON
import FirebaseAnalytics

class ProfileV2Data: NSObject {
    // MARK: - Singleton methods
    struct Static {
        fileprivate static var instance: ProfileV2Data?
    }
    class var sharedInstance: ProfileV2Data {
        if Static.instance == nil {
            Static.instance = ProfileV2Data()
        }
        return Static.instance!
    }
    func dispose() {
        ProfileV2Data.Static.instance = nil
        print("Disposed Singleton instance")
    }
    // MARK: - Variables
    // Self
    var dataSourceAboutMe = [String: String]()
    var dataSourcePersonalInfo = [String: String]()
    var userType = 1 // 1: Self   2: Other User
    var biography = ""
    var lookingFor = [JSON]()
    var lookingForSelected = [JSON]()
    
    // Other
    var dataSourceOtherAboutMe = [String: String]()
    var dataSourceOtherPersonalInfo = [String: String]()
    var OtherBiography = ""
    var otherUserId = 0
    var otherLookingFor = [JSON]()
    var otherLookingForSelected = [JSON]()
    var notificationType = "default"
    
    func reset() {
        OtherBiography = ""
        dataSourceOtherPersonalInfo = [:]
        dataSourceOtherAboutMe = [:]
        otherLookingFor = []
        otherLookingForSelected = []
    }
    
    // MARK: -  Web services
    func getAboutMeInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getAboutMeInfo(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let list = json["data"].dictionaryValue
                self.userType == 1 ? self.createDataSource(list: list) : self.createOtherDataSource(list: list)
//                self.saveJsonToUserDefaults(json: json["data"])
                completion(true, json["data"], "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func getLookingFor(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
                
        NetworkManager.getLookingFor(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let list = json["data"].arrayValue
                self.userType == 1 ? self.createLookingFor(list: list) : self.createOtherLookingFor(list: list)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    // MARK: -  Methods
    func createDataSource(list: [String:JSON]) {
        dataSourceAboutMe = [:]
        dataSourcePersonalInfo = [:]
        // About me Info
        dataSourceAboutMe["Gender"] = list["gender"]?.stringValue ?? "NA"
        dataSourceAboutMe["DOB"] = list["dob"]?.stringValue ?? ""
        dataSourceAboutMe["Location"] = list["location_city"]?.stringValue ?? ""
        dataSourceAboutMe["Relationship Status"] = list["relationship_status"]?.stringValue ?? ""
        dataSourceAboutMe["Email ID"] = list["email_id"]?.stringValue ?? ""
        dataSourceAboutMe["name"] = list["name"]?.stringValue ?? ""
        
        // Personal Info
        dataSourcePersonalInfo["Height (cms)"] = list["height"]?.stringValue ?? "NA"
        dataSourcePersonalInfo["Weight (kg)"] = list["weight"]?.stringValue ?? "NA"
        dataSourcePersonalInfo["Fitness"] = list["fitness"]?.stringValue ?? "NA"
        dataSourcePersonalInfo["Drinking"] = list["drinking"]?.stringValue ?? "NA"
        dataSourcePersonalInfo["Smoking"] = list["smoking"]?.stringValue ?? "NA"
        dataSourcePersonalInfo["Religion"] = list["religion"]?.stringValue ?? "NA"
        dataSourcePersonalInfo["Political View"] = list["political_view"]?.stringValue ?? "NA"

        // Biography
        biography = list["bio"]?.stringValue ?? "NA"
        if biography == "" {
            biography = "NA"
        }
    }
    func createOtherDataSource(list: [String:JSON]) {
        dataSourceOtherAboutMe = [:]
        dataSourceOtherPersonalInfo = [:]
        // About me Info Array
        dataSourceOtherAboutMe["Gender"] = list["gender"]?.stringValue ?? "NA"
        dataSourceOtherAboutMe["DOB"] = list["dob"]?.stringValue ?? "NA"
        var location = ""
        if userType == 2 {
            if (list["location_city"]?.stringValue ?? "") == "" {
                location = "Unavailable"
            } else {
                location = list["location_city"]?.stringValue ?? "NA"
            }
        }
        dataSourceOtherAboutMe["Location"] = location
        dataSourceOtherAboutMe["Relationship Status"] = list["relationship_status"]?.stringValue ?? "NA"
        dataSourceOtherAboutMe["Email ID"] = list["email_id"]?.stringValue ?? "NA"

        // Personal Info Array
        dataSourceOtherPersonalInfo["Height (cms)"] = list["height"]?.stringValue ?? "NA"
        dataSourceOtherPersonalInfo["Weight (kg)"] = list["weight"]?.stringValue ?? "NA"
        dataSourceOtherPersonalInfo["Fitness"] = list["fitness"]?.stringValue ?? "NA"
        dataSourceOtherPersonalInfo["Drinking"] = list["drinking"]?.stringValue ?? "NA"
        dataSourceOtherPersonalInfo["Smoking"] = list["smoking"]?.stringValue ?? "NA"
        dataSourceOtherPersonalInfo["Religion"] = list["religion"]?.stringValue ?? "NA"
        dataSourceOtherPersonalInfo["Political View"] = list["political_view"]?.stringValue ?? "NA"

        // Biography
        OtherBiography = list["bio"]?.stringValue ?? "NA"
        if OtherBiography == "" {
            OtherBiography = "NA"
        }
    }
    func saveJsonToUserDefaults(json: JSON) {
        UserDefaults.standard.AboutMe = json.rawString() ?? ""
    }
    func createLookingFor(list: [JSON]) {
        lookingForSelected = []
        lookingFor = list
//        print(lookingFor)
        lookingFor.forEach { (item) in
            let dict = item.dictionaryValue
            if let selected = dict["selected"]?.bool, selected {
                lookingForSelected.append(item)
            }
        }
        // For managing changes in Looking for
        UserDetails.shared.workSpace = []
        UserDetails.shared.genderIDs = []
        UserDetails.shared.selectedWorkSpace = []
        UserDetails.shared.selectedGenderIDs = []
        list.forEach({ (jsonDict) in
            let dict = jsonDict.dictionaryValue
            if dict["type"]?.stringValue == "SHOW" {
                UserDetails.shared.workSpace.append(dict)
//                print(dict)
                if let selected = dict["selected"]?.bool, selected, let id = dict["id"]?.int {
                    UserDetails.shared.selectedWorkSpace.append(id)
                }
            }
            if dict["type"]?.stringValue == "HIDE" {
                UserDetails.shared.genderIDs.append(dict)
//                print(dict)
                if let selected = dict["selected"]?.bool, selected, let id = dict["id"]?.int {
                    UserDetails.shared.selectedGenderIDs.append(id)
                }
            }
        })
    }
    func createOtherLookingFor(list: [JSON]) {
        otherLookingForSelected = []
        otherLookingFor = list
//        print(lookingFor)
        otherLookingFor.forEach { (item) in
            let dict = item.dictionaryValue
            if let selected = dict["selected"]?.bool, selected {
                otherLookingForSelected.append(item)
            }
        }
    }
}
// MARK: - Firebase analytics
extension ProfileV2Data {
    func logAnalyticsEventProfileUpdate() {
        Analytics.logEvent("profile_update", parameters: [
        "name": "notification_type" as NSObject,
        "full_text": notificationType as NSObject
        ])
    }
}
