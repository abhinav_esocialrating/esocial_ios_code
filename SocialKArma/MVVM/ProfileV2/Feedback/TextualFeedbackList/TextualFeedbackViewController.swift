//
//  TextualFeedbackViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/07/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class TextualFeedbackViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var scoreCardView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var conteeViewLeading: NSLayoutConstraint!
    @IBOutlet weak var contentViewTrailing: NSLayoutConstraint!
    @IBOutlet weak var contentViewBottom: NSLayoutConstraint!
    @IBOutlet weak var noDataLabel: UILabel!
    // MARK: - Variables
    let viewModel = TextualFeedbackViewModel()
    var screenType = 1 // 1: scoreCard   2: profile
    var userType = 1  // 1: self   2: other
    var otherUserId = 0
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel.fetchData {
            self.updateUI(success: self.viewModel.managedFeedback.count > 0)
        }
        getUserFeedback()
        configureView()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    // MARK: - Set up View
    func configureView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180
        
        scoreCardView.isHidden = screenType == 2
        profileView.isHidden = screenType == 1
        
        if screenType == 2 {
            contentView.layer.cornerRadius = 12
            tableView.layer.cornerRadius = 12
            contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
            addFeedbackNotification()
        } else {
            conteeViewLeading.constant = 0
            contentViewBottom.constant = 0
            contentViewTrailing.constant = 0
        }
    }
    func updateUI(success: Bool) {
        if self.screenType == 2 {
            self.viewModel.managedFeedback = self.viewModel.managedFeedback.filter({ $0.is_approved == 1 })
        }
        
        self.tableView.reloadData()
        self.tableView.isHidden = !success
        self.noDataLabel.isHidden = success
    }
    // MMARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Web Services
    func getUserFeedback() {
        viewModel.getUserFeedback(parameters: ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"], method: .get) { (success, message) in
            self.viewModel.fetchData {
                
                self.updateUI(success: self.viewModel.managedFeedback.count > 0)
            }
        }
    }
    func putUserFeedback(index: Int, delete: Bool, pin: Bool) {
        let feedback_id = viewModel.managedFeedback[index].feedback_id
        let is_approved = viewModel.managedFeedback[index].is_approved
        var params: [String:Any] = ["feedback_id": feedback_id]
        if pin {
            params["isPinned"] = 1
            params["status"] = 1
        } else if delete {
            params["status"] = 2
        } else {
            params["status"] = is_approved == 0 ? 1 : 0
        }
        viewModel.getUserFeedback(parameters: params, method: .put) { (success, message) in
            NotificationCenter.default.post(name: Notification.Name.Profile.feedback_text, object: self)
            self.getUserFeedback()
        }
    }
}
extension TextualFeedbackViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.managedFeedback.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TextualFeedbackCell.rawValue, for: indexPath) as! TextualFeedbackCell
        cell.indexPath = indexPath
//        cell.feedback = viewModel.feedback[indexPath.row]
        cell.feedbackModel = viewModel.managedFeedback[indexPath.row]
        cell.publish = { index in
            self.putUserFeedback(index: index, delete: false, pin: false)
        }
        cell.delete = { index in
            self.putUserFeedback(index: index, delete: true, pin: false)
        }
        cell.pin = { index in
            let is_pinned = self.viewModel.managedFeedback[index].is_pinned
            if is_pinned == 0 {
                self.putUserFeedback(index: index, delete: true, pin: true)
            }
        }
        cell.btnStackView.isHidden = screenType == 2
        cell.pinBtn.isHidden = screenType == 2
        return cell
    }
}
// MARK: - Notification
extension TextualFeedbackViewController {
    func addFeedbackNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(feedbackChanged), name: Notification.Name.Profile.feedback_text, object: nil)
    }
    
    @objc func feedbackChanged(_ notification: Notification) {
        getUserFeedback()
    }
}
