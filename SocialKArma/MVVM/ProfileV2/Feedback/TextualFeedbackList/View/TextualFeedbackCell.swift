//
//  TextualFeedbackCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/07/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class TextualFeedbackCell: UITableViewCell {
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var feedbackLabel: UILabel!

    @IBOutlet weak var btnStackView: UIStackView!
    
    @IBOutlet weak var publishBtn: UIButton!
    
    @IBOutlet weak var deleteBtn: UIButton!
    
    @IBOutlet weak var pinBtn: UIButton!
    
    var feedback: TextualFeedback? {
        didSet {
            setValues()
        }
    }
    var feedbackModel: TextualFeedbackModel? {
        didSet {
            setLocalValues()
        }
    }
    var publish: ((_ index: Int) -> Void)? = nil
    
    var delete: ((_ index: Int) -> Void)? = nil
    var pin: ((_ index: Int) -> Void)? = nil

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        publishBtn.setUpBorder(width: 0.8, color: UIColor.SearchColors.blue, radius: 8)
        deleteBtn.setUpBorder(width: 0.8, color: UIColor.SearchColors.blue, radius: 8)
    }
    func configureView() {
        userImg.rounded()
    }
    func setValues() {
        if let name = feedback?.name {
            nameLabel.text = name
        }
        if let date = feedback?.date {
            timeLabel.text = date
        }
        if let feed = feedback?.feedback {
            feedbackLabel.text = feed.decodeEmoji
        }
        if let image = feedback?.image_id, let url = URL(string: image) {
            userImg.sd_setImage(with: url, completed: nil)
        }
        if let is_pinned = feedback?.is_pinned {
            pinBtn.tintColor = is_pinned == 1 ? UIColor.red : UIColor.black
        } else {
            pinBtn.tintColor = UIColor.black
        }
        if let is_approved = feedback?.is_approved {
            publishBtn.setTitle(is_approved == 1 ? "Unpublish" : "Publish", for: .normal)
        }
    }
    func setLocalValues() {
        if let name = feedbackModel?.name {
            nameLabel.text = name
        } else {
            nameLabel.text = "null"
        }
        if let date = feedbackModel?.date {
            timeLabel.text = date
        }
        if let feed = feedbackModel?.feedback {
            feedbackLabel.text = feed.decodeEmoji
        } else {
            feedbackLabel.text = ""
        }
        if let image = feedbackModel?.image_id, let url = URL(string: image) {
            userImg.sd_setImage(with: url, completed: nil)
        } else {
            userImg.image = UIImage(named: "avatar-2")
        }
        if let is_pinned = feedbackModel?.is_pinned {
            pinBtn.tintColor = is_pinned == 1 ? UIColor.red : UIColor.black
        } else {
            pinBtn.tintColor = UIColor.black
        }
        if let is_approved = feedbackModel?.is_approved {
            publishBtn.setTitle(is_approved == 1 ? "Unpublish" : "Publish", for: .normal)
        } else {
            publishBtn.setTitle("Publish", for: .normal)
        }
    }
    // MARK: - Actions
    @IBAction func publishTapped(_ sender: Any) {
        publish?(indexPath.row)
    }
    @IBAction func deleteTapped(_ sender: Any) {
        delete?(indexPath.row)
    }
    
    @IBAction func pinTapped(_ sender: Any) {
        pin?(indexPath.row)
    }
}
