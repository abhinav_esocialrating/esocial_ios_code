/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class TextualFeedback {
	public var is_approved : Int?
	public var feedback : String?
	public var date : String?
	public var feedback_id : Int?
	public var is_pinned : Int?
	public var name : String?
	public var image_id : String?
	public var user_id : Int?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let TextualFeedback_list = TextualFeedback.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of TextualFeedback Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [TextualFeedback]
    {
        var models:[TextualFeedback] = []
        for item in array
        {
            models.append(TextualFeedback(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let TextualFeedback = TextualFeedback(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: TextualFeedback Instance.
*/
	required public init?(dictionary: NSDictionary) {

		is_approved = dictionary["is_approved"] as? Int
		feedback = dictionary["feedback"] as? String
		date = dictionary["date"] as? String
		feedback_id = dictionary["feedback_id"] as? Int
		is_pinned = dictionary["is_pinned"] as? Int
		name = dictionary["name"] as? String
		image_id = dictionary["image_id"] as? String
		user_id = dictionary["user_id"] as? Int
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.is_approved, forKey: "is_approved")
		dictionary.setValue(self.feedback, forKey: "feedback")
		dictionary.setValue(self.date, forKey: "date")
		dictionary.setValue(self.feedback_id, forKey: "feedback_id")
		dictionary.setValue(self.is_pinned, forKey: "is_pinned")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.image_id, forKey: "image_id")
		dictionary.setValue(self.user_id, forKey: "user_id")

		return dictionary
	}

}
