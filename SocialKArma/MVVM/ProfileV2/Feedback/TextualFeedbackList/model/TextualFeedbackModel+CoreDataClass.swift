//
//  TextualFeedbackModel+CoreDataClass.swift
//  
//
//  Created by Abhinav Dobhal on 25/10/20.
//
//

import Foundation
import CoreData


public class TextualFeedbackModel: NSManagedObject {

}

extension TextualFeedbackModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TextualFeedbackModel> {
        return NSFetchRequest<TextualFeedbackModel>(entityName: "TextualFeedbackModel")
    }

    @NSManaged public var is_approved: Int16
    @NSManaged public var feedback: String?
    @NSManaged public var date: String?
    @NSManaged public var feedback_id: Int64
    @NSManaged public var is_pinned: Int16
    @NSManaged public var name: String?
    @NSManaged public var user_id: Int64
    @NSManaged public var image_id: String?
    @NSManaged public var g_id: Int32

    func update(with dictionary: [String: Any]) {

        is_approved = dictionary["is_approved"] as? Int16 ?? 0
        feedback = dictionary["feedback"] as? String
        date = dictionary["date"] as? String
        user_id = dictionary["user_id"] as? Int64 ?? 0
        is_pinned = dictionary["is_pinned"] as? Int16 ?? 0
        feedback_id = dictionary["feedback_id"] as? Int64 ?? 0
        name = dictionary["name"] as? String
        image_id = dictionary["image_id"] as? String

    }

}
