//
//  TextualFeedbackViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/07/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreData

class TextualFeedbackViewModel: NSObject {
    
    var feedback = [TextualFeedback]()
    var managedFeedback = [TextualFeedbackModel]()
    // MARK: - Web Services
    func getUserFeedback(parameters: [String:Any], method: HTTPMethod , completion: @escaping ((_ success: Bool, _ message: String) -> Void)) {
        
        NetworkManager.getUserFeedback(parameters: parameters, method: method) { (response) in
            AppDebug.shared.debug {
                let json = JSON(response.result.value ?? JSON.init())
                print("getUserFeedback")
                print(json)
            }
            do {
                let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                switch response.response?.statusCode {
                case 200:
                    let data = jsonData["data"] as! NSArray
//                    self.feedback = TextualFeedback.modelsFromDictionaryArray(array: data)
                    self.syncLocalAndRemoteData(array: data as! [Any])
                    completion(true, "")
                default:
                    let message = jsonData["message"] as? String
                    completion(false, message ?? "Something went wrong!")
                }
            } catch {
                completion(false, "Something went wrong!")
            }
        }
    }
    // MARK: - Local Storage
    func syncLocalAndRemoteData(array: [Any]) {
        guard let taskContext = appDelegate?.persistentContainer.newBackgroundContext() else {
            return
        }
        taskContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        taskContext.undoManager = nil
        
        taskContext.performAndWait {
            
            if let array = array as? [[String:Any]] {
            
                let matchingEpisodeRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TextualFeedbackModel")
                let episodeIds = array.map { $0["feedback_id"] as? Int }.compactMap { $0 }
                matchingEpisodeRequest.predicate = NSPredicate(format: "feedback_id in %@", argumentArray: [episodeIds])
                
                let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: matchingEpisodeRequest)
                batchDeleteRequest.resultType = .resultTypeObjectIDs
                
                do {
                    let batchDeleteResult = try taskContext.execute(batchDeleteRequest) as? NSBatchDeleteResult
                    
                    if let deletedObjectIDs = batchDeleteResult?.result as? [NSManagedObjectID] {
                        NSManagedObjectContext.mergeChanges(fromRemoteContextSave: [NSDeletedObjectsKey: deletedObjectIDs],
                                                            into: [appDelegate!.persistentContainer.viewContext])
                    }
                } catch {
                    print("Error: \(error)\nCould not batch delete existing records.")
                    return
                }
                
                for (i, dict) in array.enumerated() {
                    guard let scores = NSEntityDescription.insertNewObject(forEntityName: "TextualFeedbackModel", into: taskContext) as? TextualFeedbackModel else {
                       print("Error: Failed to create a new Film object!")
                       return
                    }
                    scores.update(with: dict)
                    scores.g_id = Int32(i)

                }
                
                if taskContext.hasChanges {
                    do {
                        try taskContext.save()
                    } catch {
                        print("Error: \(error)\nCould not save Core Data context.")
                    }
                    taskContext.reset() // Reset the context to clean up the cache and low the memory footprint.
                }
            }
        }
    }
    func fetchData(_ completion: @escaping () -> Void) {
        let persistentContainer = appDelegate?.persistentContainer
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TextualFeedbackModel")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "g_id", ascending:true)]

        do {
            let results = try persistentContainer?.viewContext.fetch(fetchRequest)
            let scores = results as? [TextualFeedbackModel]
            self.managedFeedback = scores ?? []
        } catch {
            print("error")
        }
        
        completion()
    }
}
