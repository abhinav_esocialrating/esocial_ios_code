//
//  CompensationViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class CompensationViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var availableSwitch: UISwitch!
    @IBOutlet weak var availableView: UIView!
    @IBOutlet weak var currentCompTF: ACFloatingTextfield!
    @IBOutlet weak var expectedCompTF: ACFloatingTextfield!
    @IBOutlet weak var currencyTF: ACFloatingTextfield!
    @IBOutlet weak var maxBudgetTF: ACFloatingTextfield!
    @IBOutlet weak var minRateTF: ACFloatingTextfield!
    @IBOutlet weak var minHoursTF: ACFloatingTextfield!
    @IBOutlet weak var contentView: UIView!

    // MARK: - Variables
    let viewModel = CompensationViewModel()
    let dropDown = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        setInitialValues()
    }
    
    func configureView() {
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.lightGray, opacity: 0.6, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        typeLabel.text = viewModel.screenType == 1 ? "Compensation" : "Hourly Engagement"
        if viewModel.screenType == 2 {
            currencyTF.addTarget(self, action: #selector(currencyTFChanged(_:)), for: .editingChanged)
        }
        titleLabel.text = viewModel.screenType == 1 ? "Compensation" : "Hourly Engagement"
        var tfArray = [ACFloatingTextfield]()
        if viewModel.screenType == 1 {
            tfArray = [currentCompTF, expectedCompTF]
        } else {
            tfArray = [currencyTF, maxBudgetTF, minRateTF, minHoursTF]
        }
        for i in tfArray {
            i.lineColor = UIColor.gray.withAlphaComponent(0.7)
            i.placeHolderColor = .gray
            i.selectedLineColor = UIColor.SearchColors.blue
            i.selectedPlaceHolderColor = .gray
            i.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
            i.textColor = UIColor.NewTheme.darkGray
        }
        switch viewModel.screenType {
        case 1:
            currentCompTF.placeholder = viewModel.placeHolders[0]
            expectedCompTF.placeholder = viewModel.placeHolders[1]
        default:
            currencyTF.placeholder = viewModel.placeHolders[2]
            maxBudgetTF.placeholder = viewModel.placeHolders[3]
            minRateTF.placeholder = viewModel.placeHolders[4]
            minHoursTF.placeholder = viewModel.placeHolders[5]
        }
        currentCompTF.isHidden = viewModel.screenType == 2
        expectedCompTF.isHidden = viewModel.screenType == 2
        currencyTF.isHidden = viewModel.screenType == 1
        maxBudgetTF.isHidden = viewModel.screenType == 1
        minRateTF.isHidden = viewModel.screenType == 1
        minHoursTF.isHidden = viewModel.screenType == 1
        availableView.isHidden = viewModel.screenType == 1
    }
    func setInitialValues() {
        if viewModel.screenType == 1 {
            if let value = viewModel.compensationData["current_compensation"] as? Double {
                currentCompTF.text = String(format: "%.2f", value)
            }
            if let value = viewModel.compensationData["expected_compensation"] as? Double {
                expectedCompTF.text = String(format: "%.2f", value)
            }
        } else {
            if let value = viewModel.compensationData["currency"] as? String, let currency_id = viewModel.compensationData["currency_id"] as? Int {
                currencyTF.text = value
                viewModel.selectedCurrencyId = currency_id
            }
            if let value = viewModel.compensationData["min_price"] as? Int {
                maxBudgetTF.text = "\(value)"
            }
            if let value = viewModel.compensationData["min_hourly_rate"] as? Int {
                minRateTF.text = "\(value)"
            }
            if let value = viewModel.compensationData["min_hours"] as? Int {
                minHoursTF.text = "\(value)"
            }
            if let value = viewModel.compensationData["is_consulting"] as? Int {
                availableSwitch.isOn = value == 1 ? true : false
            }
        }
    }
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func currencyTFChanged(_ textField: UITextField) {
        let params = viewModel.screenType == 1 ? ["config": "currency", "search": textField.text!] : ["config": "currency", "search": textField.text!]
        viewModel.getSearchConfig(parameters: params) { (success, json, message) in
            if success {
                DispatchQueue.main.async {
                    if self.viewModel.searchedData.count > 0 {
                        self.showDropDown(textField: textField)
                    } else {
                        self.dropDown.hide()
                    }
                }
            }
        }
    }
    @IBAction func saveTapped(_ sender: Any) {
        if RechabilityHandler.shared.reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            saveCompensation(delete: false)
        }
    }
    // MARK: - Methods
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.dataSource = viewModel.dropDownData
        
        dropDown.selectionAction = { (index: Int, item: String) in
            textField.text = item
            self.handleDropDownSelection(index: index, item: item, textField: textField)
        }
        dropDown.show()
    }
    func handleDropDownSelection(index: Int, item: String, textField: UITextField) {
        textField.text = item
        self.updateDataForParams(index: index, textField: textField)
    }
    func updateDataForParams(index: Int, textField: UITextField) {
        if textField == currencyTF {
            viewModel.selectedCurrencyId = (viewModel.searchedData[index]["id"] as! Int)
        }
    }
    func isValid() -> Bool {
        var valid = true
        var message = ""
        if currentCompTF.text!.isEmpty && viewModel.screenType == 1 {
            valid = false
            message = "Please enter current compensation"
            currentCompTF.showErrorWithText(errorText: message)
        } else if expectedCompTF.text!.isEmpty && viewModel.screenType == 1 {
            valid = false
            message = "Please enter expected compensation"
            expectedCompTF.showErrorWithText(errorText: message)
        } else if currencyTF.text!.isEmpty && viewModel.screenType == 2 {
            valid = false
            message = "Please enter currency"
            currencyTF.showErrorWithText(errorText: message)
        } else if maxBudgetTF.text!.isEmpty && viewModel.screenType == 2 {
            valid = false
            message = "Please enter minimum fixed price budget"
            maxBudgetTF.showErrorWithText(errorText: message)
        } else if minRateTF.text!.isEmpty && viewModel.screenType == 2 {
            valid = false
            message = "Please enter minimum hourly rate"
            minRateTF.showErrorWithText(errorText: message)
//            startDateTF.showErrorWithText(errorText: message)
        } else if minHoursTF.text!.isEmpty && viewModel.screenType == 2 {
            valid = false
            message = "Minimum hours for a contract"
            minHoursTF.showErrorWithText(errorText: message)
        } else {
            currentCompTF.errorTextColor = .clear
            expectedCompTF.errorTextColor = .clear
            currencyTF.errorTextColor = .clear
            maxBudgetTF.errorTextColor = .clear
            minRateTF.errorTextColor = .clear
            minHoursTF.errorTextColor = .clear
        }
        return valid
    }
    // MARK: - Web Services
    func saveCompensation(delete: Bool) {
        if isValid() {
            view.endEditing(true)
            var params: [String:Any] = [:]
            if viewModel.screenType == 1 {
                params = ["currentCompensation":  currentCompTF.text!,
                        "expectedCompensation": expectedCompTF.text!]
            } else {
                params = ["currency_id": viewModel.selectedCurrencyId,
                      "min_price": maxBudgetTF.text!,
                      "min_hourly_price": minRateTF.text!,
                      "min_hours": minHoursTF.text!,
                      "is_consulting": availableSwitch.isOn ? 1 : 0]
                    
            }
            AppDebug.shared.consolePrint(params)
            self.showHud()
            viewModel.saveCompensation(parameters: params) { (success, json, message) in
                self.hideHud()
                if success {
//                    if self.viewModel.addEditOrDelete == 2 || self.viewModel.addEditOrDelete == 1 {
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
//                            self.dismiss(animated: true, completion: nil)
                        }
//                    } else if self.viewModel.addEditOrDelete == 3 {
//                        self.alertWithAction(message: "Successfully Deleted!", handler: {
//                            self.navigationController?.popViewController(animated: true)
////                            self.dismiss(animated: true, completion: nil)
//                        })
//                    }
                } else {
                    self.alert(message: message)
                }
            }
        }
    }
}
