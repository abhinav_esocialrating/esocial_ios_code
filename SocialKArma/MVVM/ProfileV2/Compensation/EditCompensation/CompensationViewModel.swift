//
//  CompensationViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class CompensationViewModel: NSObject {
    var screenType = 0  // 1: Compensation   2: Hourly Engagement
    let placeHolders = ["Current Compensation(Anually in INR)", "Expected Compensation(Anually in INR)", "Select Currency", "Minimum fixed price budget", "Minimum hourly rate?", "Minimum hours for a contract"]
    var searchedData = [[String:Any]]()
    var dropDownData = [String]()
    var selectedCurrencyId = 0
    var compensationData = [String:Any]()
    // MARK: - Web Services
    func getSearchConfig(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseSearchedData(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func saveCompensation(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
//        var method = HTTPMethod.delete
//        switch addEditOrDelete {
//        case 1:
//            method = .post
//        case 2:
//            method = .put
//        default:
//            method = .delete
//        }
        if screenType == 1 {
            NetworkManager.saveCompensation(parameters: parameters, method: .post) { (response) in
                let json = JSON(response.result.value ?? JSON.init())
                switch response.response?.statusCode {
                case 200:
                    NotificationCenter.default.post(name: Notification.Name.Profile.compensation, object: self)
                    completion(true, json, "")
                    ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
                case 202:
                    NotificationCenter.default.post(name: Notification.Name.Profile.compensation, object: self)
                    completion(true, json["message"], "")
                    ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
                default:
                    let message = json["message"].stringValue
                    completion(false, JSON.init(), message)
                }
            }
        } else {
            NetworkManager.saveEngagement(parameters: parameters, method: .post) { (response) in
                let json = JSON(response.result.value ?? JSON.init())
                switch response.response?.statusCode {
                case 200:
                    NotificationCenter.default.post(name: Notification.Name.Profile.hourlyEngagement, object: self)
                    completion(true, json, "")
                    ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
                case 202:
                    completion(true, json["message"], "")
                    NotificationCenter.default.post(name: Notification.Name.Profile.hourlyEngagement, object: self)
                    ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
                default:
                    let message = json["message"].stringValue
                    completion(false, JSON.init(), message)
                }
            }
        }
    }
    // MARK: - Utility methods
    func parseSearchedData(data: [JSON]) {
        let search = data.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["id"] = json["id"].intValue
            dict["value"] = json["value"].string
            return dict
        }
        searchedData = search
        let drop = search.map { (dict) -> String in
            return (dict["value"] as? String) ?? ""
        }
        dropDownData = drop
    }
}
