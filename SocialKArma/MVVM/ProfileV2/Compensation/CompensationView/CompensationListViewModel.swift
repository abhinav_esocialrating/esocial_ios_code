//
//  CompensationListViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class CompensationListViewModel: NSObject {
    var screenType = 1  // 1: Compensation   2: Hourly Engagement
    var compensationData = [String:Any]()
    var userType = 1   // 1: self   2: other
    var otherUserId = 0

    func getCompensation(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.saveCompensation(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].dictionaryObject
                self.compensationData = data ?? [:]
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func getEngagement(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            NetworkManager.saveEngagement(parameters: parameters, method: .get) { (response) in
                let json = JSON(response.result.value ?? JSON.init())
    //            print(json)
                switch response.response?.statusCode {
                case 200:
                    let data = json["data"].dictionaryObject
                    self.compensationData = data ?? [:]
                    completion(true, json, "")
                default:
                    let message = json["message"].stringValue
                    completion(false, JSON.init(), message)
                }
            }
        }
}
