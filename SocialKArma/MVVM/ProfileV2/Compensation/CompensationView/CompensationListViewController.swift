//
//  CompensationListViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CompensationListViewController: UIViewController {
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var currentCompView: UIView!
    @IBOutlet weak var expectedCompView: UIView!
    @IBOutlet weak var currencyView: UIView!
    @IBOutlet weak var minBudgetView: UIView!
    @IBOutlet weak var minRateView: UIView!
    @IBOutlet weak var minHoursView: UIView!
    @IBOutlet weak var avaliableView: UIView!
    
    @IBOutlet weak var currentCompLabel: UILabel!
    @IBOutlet weak var expectedCompLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var minBudgetLabel: UILabel!
    @IBOutlet weak var minRateLabel: UILabel!
    @IBOutlet weak var minHoursLabel: UILabel!
    @IBOutlet weak var availableLabel: UILabel!
    
    let viewModel = CompensationListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        getDataFromAPI()
        if viewModel.userType == 1 {
            addNotificationObservers()
        }
    }
    
    func configureView() {
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.lightGray, opacity: 0.6, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        typeLabel.text = viewModel.screenType == 1 ? "Compensation" : "Hourly Engagement"
        
        if viewModel.screenType == 1 {
            currencyView.isHidden = true
            minBudgetView.isHidden = true
            minRateView.isHidden = true
            minHoursView.isHidden = true
            avaliableView.isHidden = true
        } else {
            currentCompView.isHidden = true
            expectedCompView.isHidden = true
        }
        
        editBtn.isHidden = viewModel.userType == 2
    }
    // MARK: - Navigation

    @IBAction func editTapped(_ sender: Any) {
        openEditScreen()
    }

    // MARK: - Methods
    func openEditScreen() {
        let vc = CompensationViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.viewModel.screenType = viewModel.screenType
        vc.viewModel.compensationData = viewModel.compensationData
        navigationController?.pushViewController(vc, animated: true)
    }
    func updateUI() {
        if viewModel.screenType == 1 {
            if let value = viewModel.compensationData["current_compensation"] as? Double {
                currentCompLabel.text = String(format: "%.2f", value)
            }
            if let value = viewModel.compensationData["expected_compensation"] as? Double {
                expectedCompLabel.text = String(format: "%.2f", value)
            }
        } else {
            if let value = viewModel.compensationData["currency"] as? String {
                currencyLabel.text = value
            }
            if let value = viewModel.compensationData["min_price"] as? Int {
                minBudgetLabel.text = "\(value)"
            }
            if let value = viewModel.compensationData["min_hourly_rate"] as? Int {
                minRateLabel.text = "\(value)"
            }
            if let value = viewModel.compensationData["min_hours"] as? Int {
                minHoursLabel.text = "\(value)"
            }
            if let value = viewModel.compensationData["is_consulting"] as? Int {
                availableLabel.text = value == 1 ? "Yes" : "No"
            }
        }
    }
    // MARK: - Web Services
    func getDataFromAPI() {
        let params: [String:Any] = ["id": viewModel.userType == 1 ? UserDefaults.standard.userId : "\(viewModel.otherUserId)"]
        if viewModel.screenType == 1 {
            viewModel.getCompensation(parameters: params) { (success, _, message) in
                if success {
                    self.updateUI()
                } else {
                    self.alert(message: message)
                }
            }
        } else {
            viewModel.getEngagement(parameters: params) { (success, _, message) in
                if success {
                    self.updateUI()
                } else {
                    self.alert(message: message)
                }
            }
        }
    }
}
// MARK: - Notification
extension CompensationListViewController {
    func addNotificationObservers() {
        if viewModel.screenType == 1 {
            NotificationCenter.default.addObserver(self, selector: #selector(valuesChanged(_:)), name: Notification.Name.Profile.compensation, object: nil)
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(valuesChanged(_:)), name: Notification.Name.Profile.hourlyEngagement, object: nil)
        }
    }
    
    @objc func valuesChanged(_ notification: Notification) {
        getDataFromAPI()
    }
}

//{
//  "expected_compensation" : null,
//  "current_compensation" : null,
//  "user_id" : 27
//}

//{
//  "currency_id" : 1,
//  "min_price" : 1000,
//  "min_hours" : 10,
//  "user_hourly_engagement_id" : 1,
//  "currency_symbol" : "ALL",
//  "user_id" : 27,
//  "currency" : "Albania Lek",
//  "is_consulting" : 1,
//  "min_hourly_rate" : 150
//}
