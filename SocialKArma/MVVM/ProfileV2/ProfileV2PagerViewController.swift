//
//  ProfileV2PagerViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import ViewPager_Swift

class ProfileV2PagerViewController: UIViewController {
    var vcs: [UIViewController] = []
    var viewPager: ViewPager!
    var pageMoved: ((_ index: Int) -> Void)? = nil
    var userType = 1   // 1: self   2: other
    var otherUserId = 0
    var numberOfPagesChanged: ((_ count: Int) -> Void)? = nil
    var profileCategory: ProfileCategoryTypes = .Biography
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLookingForNotification()
        configrePagerOnCategory()
//        getLookingFor()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configrePagerOnCategory() {
        var ids = [Int]()
        if userType == 1 {
            ids = ProfileV2Data.sharedInstance.lookingForSelected.map { (json) -> Int in
                let dict = json.dictionaryValue
                let id = dict["id"]?.intValue
                return id ?? 0
            }
        } else {
            ids = ProfileV2Data.sharedInstance.otherLookingForSelected.map { (json) -> Int in
                let dict = json.dictionaryValue
                let id = dict["id"]?.intValue
                return id ?? 0
            }
        }
        
        vcs = []
        
        switch profileCategory {
        case .LookingFor:
            let lookingFor = ProfileV2SkillsViewController.instantiate(fromAppStoryboard: .ProfileV2)
            lookingFor.screenType = 3
            vcs.append(lookingFor)
        case .PersonalDetails:
            let personalDetails = ProfileV2PersonalDetailViewController.instantiate(fromAppStoryboard: .ProfileV2)
            personalDetails.userType = userType
            vcs.append(personalDetails)
        case .Interests:
            let interest = ProfileV2InterestsViewController.instantiate(fromAppStoryboard: .ProfileV2)  // Interests
            interest.userType = userType
            interest.otherUserId = otherUserId
            interest.screenType = 2
            vcs.append(interest)
        case .ProfessionalDetails:
            let exp = ProfileV2EducationListViewController.instantiate(fromAppStoryboard: .ProfileV2)
            exp.viewModel.screenType = 2
            exp.userType = userType
            exp.otherUserId = otherUserId
            vcs.append(exp)
            
            let category = ProfileV2SkillsViewController.instantiate(fromAppStoryboard: .ProfileV2)    // Category/Service
            category.userType = userType
            category.otherUserId = otherUserId
            category.screenType = 4
            vcs.append(category)
            
            let projects = ProjectsViewController.instantiate(fromAppStoryboard: .ProfileV2)    // Compensation
            projects.viewModel.userType = userType
            projects.viewModel.otherUserId = otherUserId
            vcs.append(projects)
        case .EducationDetails:
            let edu = ProfileV2EducationListViewController.instantiate(fromAppStoryboard: .ProfileV2)
            edu.viewModel.screenType = 1
            edu.userType = userType
            edu.otherUserId = otherUserId
            vcs.append(edu)
        case .SkillAndCompetencies:
            let skill = SkillsTypesViewController.instantiate(fromAppStoryboard: .ProfileV2)  // Skill
            skill.viewModel.userType = userType
            skill.viewModel.otherUserId = otherUserId
            vcs.append(skill)
            
            let competency = CompetencyViewController.instantiate(fromAppStoryboard: .ProfileV2)
            competency.viewModel.userType = userType
            competency.viewModel.otherUserId = otherUserId
            vcs.append(competency)
        case .CompensationDetails:
            if ids.contains(2) {
                let compensation = CompensationListViewController.instantiate(fromAppStoryboard: .ProfileV2)    // Compensation
                compensation.viewModel.screenType = 1
                compensation.viewModel.userType = userType
                compensation.viewModel.otherUserId = otherUserId
                vcs.append(compensation)
            }
            
            let engagement = CompensationListViewController.instantiate(fromAppStoryboard: .ProfileV2)    // Hourly engagement
            engagement.viewModel.screenType = 2
            engagement.viewModel.userType = userType
            engagement.viewModel.otherUserId = otherUserId
            vcs.append(engagement)
            
        case .Biography:
            let bio = ProfileV2BioViewController.instantiate(fromAppStoryboard: .ProfileV2)
            bio.userType = userType
            vcs.append(bio)
        case .DetailedFeedback:
            let feedback = TextualFeedbackViewController.instantiate(fromAppStoryboard: .ScoreCard)
            feedback.screenType = 2
            feedback.userType = userType
            feedback.otherUserId = otherUserId
        case .MySocialMedia:
            let social = SocialMediaURLViewController.instantiate(fromAppStoryboard: .ProfileV2)
            social.viewModel.userType = userType
            social.viewModel.otherUserId = otherUserId
            vcs.append(social)
        default:
            let tags = TagRelationsViewController.instantiate(fromAppStoryboard: .ProfileV2)
            tags.viewModel.userType = userType
            tags.viewModel.otherUserId = otherUserId
            vcs.append(tags)
        }
        
        numberOfPagesChanged?(vcs.count)
        
        if vcs.count > 0 {
            let myOptions = ViewPagerOptions()
            myOptions.isTabIndicatorAvailable = false
            myOptions.tabViewHeight = 0

            viewPager = ViewPager(viewController: self)
            viewPager.setOptions(options: myOptions)
            viewPager.setDataSource(dataSource: self)
            viewPager.setDelegate(delegate: self)
            viewPager.build()
        }
    }
    func configurePager() {
                
        vcs = []
                
        let feedback = TextualFeedbackViewController.instantiate(fromAppStoryboard: .ScoreCard)
       feedback.screenType = 2
       feedback.userType = userType
        feedback.otherUserId = otherUserId
        
        vcs.append(feedback)

        
        let bio = ProfileV2BioViewController.instantiate(fromAppStoryboard: .ProfileV2)
        bio.userType = userType

        vcs.append(bio)
        
        var ids = [Int]()
        if userType == 1 {
            ids = ProfileV2Data.sharedInstance.lookingForSelected.map { (json) -> Int in
                let dict = json.dictionaryValue
                let id = dict["id"]?.intValue
                return id ?? 0
            }
        } else {
            ids = ProfileV2Data.sharedInstance.otherLookingForSelected.map { (json) -> Int in
                let dict = json.dictionaryValue
                let id = dict["id"]?.intValue
                return id ?? 0
            }
        }
        
        
        if ids.contains(10) || ids.contains(8) || ids.contains(9) || ids.contains(11) {
            
            let personalDetails = ProfileV2PersonalDetailViewController.instantiate(fromAppStoryboard: .ProfileV2)
            personalDetails.userType = userType
            vcs.append(personalDetails)

            let interest = ProfileV2InterestsViewController.instantiate(fromAppStoryboard: .ProfileV2)  // Interests
            interest.userType = userType
            interest.otherUserId = otherUserId
            interest.screenType = 2
            vcs.append(interest)
        }
        
        if ids.contains(2) || ids.contains(3) || ids.contains(4) || ids.contains(5) || ids.contains(11) {
            
            let edu = ProfileV2EducationListViewController.instantiate(fromAppStoryboard: .ProfileV2)
            edu.viewModel.screenType = 1
            edu.userType = userType
            edu.otherUserId = otherUserId
            vcs.append(edu)
            
            let exp = ProfileV2EducationListViewController.instantiate(fromAppStoryboard: .ProfileV2)
            exp.viewModel.screenType = 2
            exp.userType = userType
            exp.otherUserId = otherUserId
            vcs.append(exp)
            
            let category = ProfileV2SkillsViewController.instantiate(fromAppStoryboard: .ProfileV2)    // Category/Service
            category.userType = userType
            category.otherUserId = otherUserId
            category.screenType = 4
            vcs.append(category)
            
            let skill = SkillsTypesViewController.instantiate(fromAppStoryboard: .ProfileV2)  // Skill
            skill.viewModel.userType = userType
            skill.viewModel.otherUserId = otherUserId
            vcs.append(skill)
        }
        
        if userType == 1 {
            let lookingFor = ProfileV2SkillsViewController.instantiate(fromAppStoryboard: .ProfileV2)
            lookingFor.screenType = 3
            vcs.insert(lookingFor, at: 0)
        }
        
        let social = SocialMediaURLViewController.instantiate(fromAppStoryboard: .ProfileV2)
        social.viewModel.userType = userType
        social.viewModel.otherUserId = otherUserId
        vcs.append(social)
        
        let tags = TagRelationsViewController.instantiate(fromAppStoryboard: .ProfileV2)
        tags.viewModel.userType = userType
        tags.viewModel.otherUserId = otherUserId
        vcs.append(tags)
        
        let competency = CompetencyViewController.instantiate(fromAppStoryboard: .ProfileV2)
        competency.viewModel.userType = userType
        competency.viewModel.otherUserId = otherUserId
        vcs.append(competency)
        
        numberOfPagesChanged?(vcs.count)
       
       let myOptions = ViewPagerOptions()
       myOptions.isTabIndicatorAvailable = false
       myOptions.tabViewHeight = 0

       viewPager = ViewPager(viewController: self)
       viewPager.setOptions(options: myOptions)
       viewPager.setDataSource(dataSource: self)
       viewPager.setDelegate(delegate: self)
       viewPager.build()
    }

    
    // MARK: - Actions
    

}
// MARK: - ViewPager Methods
extension ProfileV2PagerViewController: ViewPagerDataSource, ViewPagerDelegate {
    func numberOfPages() -> Int {
        return vcs.count
    }
    
    func viewControllerAtPosition(position: Int) -> UIViewController {
        return vcs[position]
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        let tab = ViewPagerTab(title: "fdsdfds", image: nil)
        let addfls = Array(repeating: tab, count: vcs.count)
        return addfls
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
    
    func willMoveToControllerAtIndex(index: Int) {
        
    }
    
    func didMoveToControllerAtIndex(index: Int) {
        viewPager.displayViewController(atIndex: index)
        pageMoved?(index)
    }
    
    
}
// MARK: - Notification
extension ProfileV2PagerViewController {
    func addLookingForNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(lookingForChanged), name: Notification.Name.Profile.LookingFor, object: nil)
    }
    @objc func lookingForChanged(_ notification: Notification) {
        viewPager.removeAllTabs()
        getLookingFor()
    }
    func getLookingFor() {
        let params = ["id": userType == 1 ? UserDefaults.standard.userId : "\(otherUserId)"]
        ProfileV2Data.sharedInstance.getLookingFor(parameters: params) { (success, json, message) in
            self.configrePagerOnCategory()
        }
    }
}
