//
//  SocialMediaURLViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
import SwiftyJSON

class SocialMediaURLViewModel: NSObject {
    
    var showOrEdit = 1  // 1: Show   2: Edit
    var userType = 1   // 1: self   2: other
    var otherUserId = 0
    
    var urls: [String: Any]?

    func getSocialMediaURLs(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.socialAccount(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].dictionaryObject
                self.urls = data
                completion(true, json, "")
            default:
                self.urls = [:]
                let message = json["message"].stringValue
                completion(false, json, message)
            }
        }
    }
    func saveSocialMediaURLs(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.socialAccount(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].dictionaryObject
                self.urls = data
                completion(true, json, "")
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                self.urls = [:]
                let message = json["message"].stringValue
                completion(false, json, message)
            }
        }
    }
}
