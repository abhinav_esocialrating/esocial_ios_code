//
//  SocialMediaURLViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/08/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class SocialMediaURLViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var urlViewTop: NSLayoutConstraint!
    @IBOutlet weak var navViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet var tvs: [UITextView]!
    @IBOutlet var textViewBottomLine: [UIView]!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var saveBtnTop: UIView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet var linkActionButtons: [UIButton]!
    // MARK: - Variables
    let viewModel = SocialMediaURLViewModel()

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        addSocialMediaNotification()
        getSocialAccounts()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureView() {
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        urlViewTop.constant = viewModel.showOrEdit == 1 ? 50 : 0
        navViewHeight.constant = viewModel.showOrEdit == 1 ? 0 : 44
        scrollViewBottom.constant = viewModel.showOrEdit == 1 ? 0 : 44
        saveBtn.isHidden = viewModel.showOrEdit == 1 ? true : false
        editView.isHidden = viewModel.showOrEdit == 1 ? false : true
        saveBtnTop.isHidden = viewModel.showOrEdit == 1 ? true : false
        editBtn.isHidden = viewModel.userType == 1 ? false : true

        if viewModel.showOrEdit == 1 {
            tvs.forEach({ $0.isUserInteractionEnabled = false })
            textViewBottomLine.forEach({ $0.isHidden = true })
        } else {
            linkActionButtons.forEach({ $0.isHidden = true })
        }
    }

    // MARK: - Navigation
    @IBAction func editTapped(_ sender: Any) {
        openEditScreen()
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        
        viewModel.urls?[SocialMediaTypes.linkedInPost.rawValue] = tvs[0].text!
        viewModel.urls?[SocialMediaTypes.facebookPost.rawValue] = tvs[1].text!
        viewModel.urls?[SocialMediaTypes.instagramPost.rawValue] = tvs[2].text!
        viewModel.urls?[SocialMediaTypes.twitterPost.rawValue] = tvs[3].text!
        
        postSocialAccounts()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func linkActionBtnTapped(_ sender: Any) {
        if let sender = sender as? UIButton {
            switch sender {
            case linkActionButtons[0]:
                self.openURL(url: viewModel.urls?[SocialMediaTypes.linkedIn.rawValue] as? String)
            case linkActionButtons[1]:
                self.openURL(url: viewModel.urls?[SocialMediaTypes.facebook.rawValue] as? String)
            case linkActionButtons[2]:
                self.openURL(url: viewModel.urls?[SocialMediaTypes.instagram.rawValue] as? String)
            default:
                self.openURL(url: viewModel.urls?[SocialMediaTypes.twitter.rawValue] as? String)
            }
        }
    }
    // MARK: - Methods
    func openURL(url: String?) {
        guard let url = url, let urlToOpen = URL(string: url) else { return }
        UIApplication.shared.open(urlToOpen, options: [:], completionHandler: nil)
    }
    func openEditScreen() {
        let vc = SocialMediaURLViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.viewModel.showOrEdit = 2
        navigationController?.pushViewController(vc, animated: true)
    }
    func updateUI() {
        
        if let url = viewModel.urls?[SocialMediaTypes.linkedIn.rawValue] as? String {
            tvs[0].text = url
        }
        if let url = viewModel.urls?[SocialMediaTypes.facebook.rawValue] as? String {
            tvs[1].text = url
        }
        if let url = viewModel.urls?[SocialMediaTypes.instagram.rawValue] as? String {
            tvs[2].text = url
        }
        if let url = viewModel.urls?[SocialMediaTypes.twitter.rawValue] as? String {
            tvs[3].text = url
        }
    }
    func getSocialAccounts() {
        let parameters = ["id": viewModel.userType == 1 ? UserDefaults.standard.userId : "\(viewModel.otherUserId)"]
        viewModel.getSocialMediaURLs(parameters: parameters) { (success, _, message) in
            
            self.updateUI()
        }
    }
    func postSocialAccounts() {
        let parameters = viewModel.urls ?? [:]
        viewModel.saveSocialMediaURLs(parameters: parameters) { (success, _, message) in
            
            if success {
                NotificationCenter.default.post(name: Notification.Name.Profile.socialURLs, object: nil)
                self.navigationController?.popViewController(animated: true)
            } else {
                self.alert(message: message)
            }
        }
    }
}
// MARK: - Notification
extension SocialMediaURLViewController {
    func addSocialMediaNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(urlsChanged(_:)), name: Notification.Name.Profile.socialURLs, object: nil)
    }
    
    @objc func urlsChanged(_ notification: Notification) {
        getSocialAccounts()
    }
}
