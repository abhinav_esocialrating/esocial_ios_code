//
//  CompetencyTagCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CompetencyTagCell: UITableViewCell {
    
    @IBOutlet weak var bkgView: UIView!
    @IBOutlet weak var tagView: TagListView!

    var competencies: [Competency]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        configureTagView()
    }
    // MARK: - Methods
    func configureTagView() {
        tagView.delegate = self
        tagView.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
    }
    func setTags() {
        tagView.removeAllTags()
        if let competencies = competencies {
            competencies.forEach { (item) in
                tagView.addTag(item.competency ?? "")
            }
        }
    }
}
extension CompetencyTagCell: TagListViewDelegate {
}
