//
//  ProfileDetailBasicInfoCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ProfileDetailBasicInfoCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var messageBtn: UIButton!
    @IBOutlet weak var offersBtn: UIButton!
    
    var profileDetails: ProfileDetails? {
        didSet {
            setValues()
        }
    }
    var msgTap: completionBlock?
    var userImgTap: completionBlock?
    var offersTap: completionBlock?
    
    // MARK: - Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        offersBtn.setUpBorder(width: 1, color: UIColor.SearchColors.blue, radius: 6)
    }

    func setValues() {
        var str = ""
        if let name = profileDetails?.name {
            str = name
        } else {
            str = "Mr. Xyz"
        }
        nameLabel.text = str
        
        if let url = profileDetails?.image_id, url != "" {
            let trimmed = url.trimmingCharacters(in: .whitespacesAndNewlines)
            userImg.sd_setImage(with: URL(string: trimmed)!, completed: nil)
        } else {
            userImg.image = UIImage(named: "avatar-2")
        }
        
        var distanceAndAge = " "
        if let age = profileDetails?.age {
            distanceAndAge = "\(age)"
        }
        if let loc = profileDetails?.location_city {
            if distanceAndAge == " " {
                distanceAndAge = loc
            } else {
                distanceAndAge.append("  \u{2022}  \(loc)")
            }
        }
        locationLabel.text = distanceAndAge
        
        
        if let bio = profileDetails?.bio, bio != "" {
            bioLabel.text = bio.decodeEmoji
        } else {
            bioLabel.text = "About Info not added"
        }
        
        
        // image border according to score
        if let rateValue = profileDetails?.score {
            if rateValue < 2.5 {
                userImg.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("ee1d24"), radius: userImg.frame.height/2)

            } else if rateValue >= 2.5 && rateValue < 3.75 {
                userImg.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("f36821"), radius: userImg.frame.height/2)

            } else {
                userImg.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("72bf43"), radius: userImg.frame.height/2)
            }
        } else {
            userImg.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("ee1d24"), radius: userImg.frame.height/2)
        }
    }

    @IBAction func messageTapped(_ sender: Any) {
        msgTap?()
    }
    
    @IBAction func userImgTapped(_ sender: Any) {
        userImgTap?()
    }
    
    @IBAction func offersTapped(_ sender: Any) {
        offersTap?()
    }
    
}
