//
//  ProfileDetailsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class ProfileDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reportBtn: UIButton!

    var otherUserId = 0
    var dataSource = [ProfileDetailCategories]()
    let viewModel = ProfileDetailsViewModel()
    let dropDown = DropDown()
    var dropDownData = ["Report"]

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        viewModel.profileDetails(parameters: ["id": otherUserId]) { (success, _, message) in
            
            if success {
                self.getLookingFor()
            } else {
                self.alert(message: message)
            }
        }
    }
    func configureView() {
        
        configureTableView()
    }
    func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.registerFromXib(name: TableViewCells.InterestsTableViewCell.rawValue)
        tableView.registerFromXib(name: TableViewCells.EducationListCell.rawValue)

    }
    // MARK: - Actions
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func reportTapped(_ sender: Any) {
        showDropDown()
    }
    func openChat() {
        let user_id = viewModel.profileDetails?.user_id ?? 0
        let name = viewModel.profileDetails?.name ?? "NA"
        let image_id = viewModel.profileDetails?.image_id ?? ""
        
        let vc = ChatViewController.instantiate(fromAppStoryboard: .Chat)
        SocketIOManager.shared.toUserId = "\(user_id)"
        let chatUser = ChatUser(dictionary: ["name":name, "image_id":image_id] as NSDictionary)
        vc.viewModel.chatUser = chatUser
        
        navigationController?.pushViewController(vc, animated: true)
    }
    func showProfilePic() {
        let vc = PhotoViewerViewController()
        vc.modalPresentationStyle = .overCurrentContext
        vc.imageUrl = viewModel.profileDetails?.image_id
        vc.backAction = {
            self.tabBarController?.tabBar.isHidden = false
        }
        present(vc, animated: false, completion: nil)
        tabBarController?.tabBar.isHidden = true
    }
    // MARK: - Methods
    func setUpDataSource() {
        dataSource = []
        
        dataSource.append(ProfileDetailCategories(page_name: "Basic Info", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.BasicInfo))
        dataSource.append(ProfileDetailCategories(page_name: "Experience", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.ExperienceDetails))
        dataSource.append(ProfileDetailCategories(page_name: "Education", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.EducationDetails))
        dataSource.append(ProfileDetailCategories(page_name: "Interests", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.Interests))
        dataSource.append(ProfileDetailCategories(page_name: "Skill", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.Skills))
        dataSource.append(ProfileDetailCategories(page_name: "Category/Services", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.CategoryServices))
        dataSource.append(ProfileDetailCategories(page_name: "Projects", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.Projects))
        dataSource.append(ProfileDetailCategories(page_name: "Competencies", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.Competencies))
        dataSource.append(ProfileDetailCategories(page_name: "Detailed Feedback", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.DetailedFeedback))
        dataSource.append(ProfileDetailCategories(page_name: "Tagging", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.Tagging))

/*
         var ids = [Int]()
         ids = ProfileV2Data.sharedInstance.otherLookingForSelected.map { (json) -> Int in
             let dict = json.dictionaryValue
             let id = dict["id"]?.intValue
             return id ?? 0
         }
        if ids.contains(10) || ids.contains(8) || ids.contains(9) || ids.contains(11) {
            
            dataSource.append(ProfileDetailCategories(page_name: "Personal Details", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.PersonalDetails))
        }
        if ids.contains(2) || ids.contains(3) || ids.contains(4) || ids.contains(5) || ids.contains(11) {
            
            dataSource.append(ProfileDetailCategories(page_name: "Compensation", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.Compensation))
            dataSource.append(ProfileDetailCategories(page_name: "HourlyEngagement", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.HourlyEngagement))
        }
        dataSource.append(ProfileDetailCategories(page_name: "My Social Media", pageUrl: "", image: "", page_type: ProfileDetailCategoryTypes.MySocialMedia))
*/
        
        tableView.reloadData()
    }
    private func openReport() {
        let vc = ReportUserViewController.instantiate(fromAppStoryboard: .MainExt)
        vc.viewModel.otherUserId = "\(otherUserId)"
        vc.modalPresentationStyle = .overCurrentContext
        vc.notify = {
            self.tabBarController?.tabBar.isHidden = false
        }
        present(vc, animated: false, completion: nil)
        tabBarController?.tabBar.isHidden = true
    }
    private func openOffers() {
        let vc = OffersListViewController.instantiate(fromAppStoryboard: .Offers)
        vc.viewModel.offerType = 6
        vc.viewModel.otherUserId = otherUserId
        navigationController?.pushViewController(vc, animated: true)
    }
    // MARK: - Menu drop down
    func showDropDown() {
        dropDown.anchorView = reportBtn
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 200
        dropDown.dataSource = dropDownData
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropSelection(index: index, item: item)
        }
        dropDown.show()
    }
    func handleDropSelection(index: Int, item: String) {
        switch index {
        case 0:
            openReport()
        default:
            print("")
        }
    }
    // MARK: - Web Services
    func getLookingFor() {
        let params = ["id": "\(otherUserId)"]
        ProfileV2Data.sharedInstance.getLookingFor(parameters: params) { (success, json, message) in
            if success {
                self.setUpDataSource()
            } else {
                self.alert(message: message)
            }
        }
    }
}
extension ProfileDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch dataSource[section].page_type {
        case .BasicInfo:
            return 1
        case .ExperienceDetails:
            return viewModel.profileDetails?.experience?.count ?? 0
        case .EducationDetails:
            return viewModel.profileDetails?.education?.count ?? 0
        case .Interests:
            return (viewModel.profileDetails?.interest?.count ?? 0) == 0 ? 0 : 1
        case .Skills:
            return (viewModel.profileDetails?.skill?.count ?? 0) == 0 ? 0 : 1
        case .CategoryServices:
            return (viewModel.profileDetails?.business_info?.count ?? 0) == 0 ? 0 : 1
        case .Projects:
            return viewModel.profileDetails?.project?.count ?? 0
        case .Competencies:
            return (viewModel.profileDetails?.competency?.count ?? 0) == 0 ? 0 : 1
        case .DetailedFeedback:
            return viewModel.profileDetails?.pinned_feedback?.count ?? 0
        case .Tagging:
            return viewModel.profileDetails?.pinned_feedback?.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch dataSource[indexPath.section].page_type {
        case .BasicInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ProfileDetailBasicInfoCell.rawValue) as! ProfileDetailBasicInfoCell
            cell.profileDetails = viewModel.profileDetails
            cell.msgTap = {
                self.openChat()
            }
            cell.userImgTap = {
                self.showProfilePic()
            }
            cell.offersTap = {
                self.openOffers()
            }
            return cell
        case .ExperienceDetails:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.InterestsTableViewCell.rawValue, for: indexPath) as! InterestsTableViewCell
            cell.indexPath = indexPath
//            cell.viewModel = experienceViewModel
            cell.experience = viewModel.profileDetails?.experience?[indexPath.row]
            cell.editButton.isHidden = true
            return cell
        case .EducationDetails:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.EducationListCell.rawValue, for: indexPath) as! EducationListCell
            cell.indexPath = indexPath
//            cell.listViewModel = educationViewModel
            cell.education = viewModel.profileDetails?.education?[indexPath.row]
            cell.editButton.isHidden = true
//            cell.lowerView.isHidden = (viewModel.educationDataSource.count-1 == indexPath.row)
            return cell
        case .Skills:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TagTableViewCell.rawValue) as! TagTableViewCell
            cell.skills = viewModel.profileDetails?.skill
            cell.setTags()
            return cell
        case .Interests:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TagTableViewCell.rawValue) as! TagTableViewCell
            cell.interests = viewModel.profileDetails?.interest
            cell.setTags()
            return cell
        case .CategoryServices:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TagTableViewCell.rawValue) as! TagTableViewCell
            cell.isBusinessCell = true
            cell.setServicesTags(services: viewModel.profileDetails?.business_info ?? [])
            return cell
        case .Projects:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ProjectCell.rawValue, for: indexPath) as! ProjectCell
            cell.indexPath = indexPath
            cell.project = viewModel.profileDetails?.project?[indexPath.row]
            cell.editButton.isHidden = true
//            cell.lowerView.isHidden = (viewModel.projects.count-1 == indexPath.row)
            return cell
        case .Competencies:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.CompetencyTagCell.rawValue) as! CompetencyTagCell
            cell.competencies = viewModel.profileDetails?.competency
            cell.setTags()
            return cell
        case .DetailedFeedback:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TextualFeedbackCell.rawValue, for: indexPath) as! TextualFeedbackCell
            cell.indexPath = indexPath
            cell.feedback = viewModel.profileDetails?.pinned_feedback?[indexPath.row]
            cell.btnStackView.isHidden = true
            cell.pinBtn.isHidden = true
            return cell
        case .Tagging:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TagRelationsCell.rawValue, for: indexPath) as! TagRelationsCell
            cell.indexPath = indexPath
            cell.showOrEdit = 1
            cell.tags = viewModel.profileDetails?.tags?[indexPath.row]
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch dataSource[section].page_type {
        case .BasicInfo:
            return CGFloat.leastNonzeroMagnitude
        case .ExperienceDetails:
            return (viewModel.profileDetails?.experience?.count ?? 0) == 0 ? CGFloat.leastNonzeroMagnitude : 44
        case .EducationDetails:
            return (viewModel.profileDetails?.education?.count ?? 0) == 0 ? CGFloat.leastNonzeroMagnitude : 44
        case .Interests:
            return (viewModel.profileDetails?.interest?.count ?? 0) == 0 ? CGFloat.leastNonzeroMagnitude : 44
        case .Skills:
            return (viewModel.profileDetails?.skill?.count ?? 0) == 0 ? CGFloat.leastNonzeroMagnitude : 44
        case .CategoryServices:
            return (viewModel.profileDetails?.business_info?.count ?? 0) == 0 ? CGFloat.leastNonzeroMagnitude : 44
        case .Projects:
            return (viewModel.profileDetails?.project?.count ?? 0) == 0 ? CGFloat.leastNonzeroMagnitude : 44
        case .Competencies:
            return (viewModel.profileDetails?.competency?.count ?? 0) == 0 ? CGFloat.leastNonzeroMagnitude : 44
        case .DetailedFeedback:
            return (viewModel.profileDetails?.pinned_feedback?.count ?? 0) == 0 ? CGFloat.leastNonzeroMagnitude : 44
        case .Tagging:
            return (viewModel.profileDetails?.tags?.count ?? 0) == 0 ? CGFloat.leastNonzeroMagnitude : 44
        default:
            return CGFloat.leastNonzeroMagnitude
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ProfileDetailHeaderCell.rawValue) as! ProfileDetailHeaderCell
        cell.label.text = dataSource[section].page_name
        return cell
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
}
