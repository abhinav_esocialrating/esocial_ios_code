//
//  ProfileDetailsViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProfileDetailsViewModel: NSObject {
    var profileDetails: ProfileDetails?
    func profileDetails(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getProfileDetails(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData["data"] as? NSDictionary
                    self.profileDetails = ProfileDetails(dictionary: data ?? [:])
                    completion(true, json, "")
                } catch {
                    completion(false, json, "Something went wrong.")
                }
                
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
