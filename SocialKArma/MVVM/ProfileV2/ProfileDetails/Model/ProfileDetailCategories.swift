//
//  ProfileDetailCategories.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation

struct ProfileDetailCategories {
    var page_name: String
    var pageUrl: String
    var image: String
    var page_type: ProfileDetailCategoryTypes
}
enum ProfileDetailCategoryTypes {
    
    case BasicInfo, Biography, Skills, Interests, ExperienceDetails, EducationDetails, PersonalDetails, CategoryServices, Projects, Competencies, DetailedFeedback, MySocialMedia, Tagging, Compensation, HourlyEngagement
}
