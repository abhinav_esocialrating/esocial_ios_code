/*
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ProfileDetails {
    public var experience : Array<ExperienceModel>?
    public var email_id : String?
    public var religion_id : Int?
//    public var training : Training?
    public var drinking : String?
//    public var certification : Array<Certification>?
    public var total_work_experience_years : Int?
    public var medals : Array<Medals>?
    public var religion : String?
    public var score : Double?
    public var lastSeen : String?
    public var location_city : String?
    public var project : Array<Project>?
    public var gender : String?
    public var age : Int?
    public var drinking_id : Int?
    public var smoking_id : Int?
    public var business_info : Array<BusinessService>?
    public var image_id : String?
    public var karma_points : Int?
    public var isOnline : String?
    public var education : Array<EducationModel>?
    public var fitness : String?
    public var skill : Array<SkillModel>?
    public var relationship_status_id : Int?
    public var height : Int?
    public var competency : Array<Competency>?
    public var weight : Int?
    public var total_work_experience_months : Int?
    public var relationship_status : String?
    public var political_view_id : Int?
    public var bio : String?
    public var name : String?
    public var pinned_feedback : Array<TextualFeedback>?
    public var services : Array<Service>?
    public var smoking : String?
    public var dob : String?
    public var fitness_id : Int?
    public var blood_group : String?
    public var __v : Int?
    public var interest : Array<InterestModel>?
    public var tags : Array<TagRelationship>?
    public var user_id : Int?
    public var political_view : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let ProfileDetails_list = ProfileDetails.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of ProfileDetails Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ProfileDetails]
    {
        var models:[ProfileDetails] = []
        for item in array
        {
            models.append(ProfileDetails(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let ProfileDetails = ProfileDetails(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: ProfileDetails Instance.
*/
    required public init?(dictionary: NSDictionary) {

        if (dictionary["experience"] != nil) { experience = ExperienceModel.modelsFromDictionaryArray(array: dictionary["experience"] as! NSArray) }
        email_id = dictionary["email_id"] as? String
        religion_id = dictionary["religion_id"] as? Int
//        if (dictionary["training"] != nil) { training = Training(dictionary: dictionary["training"] as! NSDictionary) }
        drinking = dictionary["drinking"] as? String
//        if (dictionary["certification"] != nil) { certification = Certification.modelsFromDictionaryArray(dictionary["certification"] as! NSArray) }
        total_work_experience_years = dictionary["total_work_experience_years"] as? Int
        if (dictionary["medals"] != nil) { medals = Medals.modelsFromDictionaryArray(array: dictionary["medals"] as! NSArray) }
        if (dictionary["tags"] != nil) { tags = TagRelationship.modelsFromDictionaryArray(array: dictionary["tags"] as! NSArray) }
        religion = dictionary["religion"] as? String
        score = dictionary["score"] as? Double
        lastSeen = dictionary["lastSeen"] as? String
        location_city = dictionary["location_city"] as? String
        if (dictionary["project"] != nil) { project = Project.modelsFromDictionaryArray(array: dictionary["project"] as! NSArray) }
        gender = dictionary["gender"] as? String
        age = dictionary["age"] as? Int
        drinking_id = dictionary["drinking_id"] as? Int
        smoking_id = dictionary["smoking_id"] as? Int
        if (dictionary["business_info"] != nil) { business_info = BusinessService.modelsFromDictionaryArray(array: dictionary["business_info"] as! NSArray) }
        image_id = dictionary["image_id"] as? String
        karma_points = dictionary["karma_points"] as? Int
        isOnline = dictionary["isOnline"] as? String
        if (dictionary["education"] != nil) { education = EducationModel.modelsFromDictionaryArray(array: dictionary["education"] as! NSArray) }
        fitness = dictionary["fitness"] as? String
        if (dictionary["skill"] != nil) { skill = SkillModel.modelsFromDictionaryArray(array: dictionary["skill"] as! NSArray) }
        relationship_status_id = dictionary["relationship_status_id"] as? Int
        height = dictionary["height"] as? Int
        if (dictionary["competency"] != nil) { competency = Competency.modelsFromDictionaryArray(array: dictionary["competency"] as! NSArray) }
        weight = dictionary["weight"] as? Int
        total_work_experience_months = dictionary["total_work_experience_months"] as? Int
        relationship_status = dictionary["relationship_status"] as? String
        political_view_id = dictionary["political_view_id"] as? Int
        bio = dictionary["bio"] as? String
        name = dictionary["name"] as? String
        if (dictionary["pinned_feedback"] != nil) { pinned_feedback = TextualFeedback.modelsFromDictionaryArray(array: dictionary["pinned_feedback"] as! NSArray) }
        if (dictionary["services"] != nil) { services = Service.modelsFromDictionaryArray(array: dictionary["services"] as! NSArray) }
        smoking = dictionary["smoking"] as? String
        dob = dictionary["dob"] as? String
        fitness_id = dictionary["fitness_id"] as? Int
        blood_group = dictionary["blood_group"] as? String
        __v = dictionary["__v"] as? Int
        if (dictionary["interest"] != nil) { interest = InterestModel.modelsFromDictionaryArray(array: dictionary["interest"] as! NSArray) }
        user_id = dictionary["user_id"] as? Int
        political_view = dictionary["political_view"] as? String
    }

        
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.email_id, forKey: "email_id")
        dictionary.setValue(self.religion_id, forKey: "religion_id")
//        dictionary.setValue(self.training?.dictionaryRepresentation(), forKey: "training")
        dictionary.setValue(self.drinking, forKey: "drinking")
        dictionary.setValue(self.total_work_experience_years, forKey: "total_work_experience_years")
        dictionary.setValue(self.religion, forKey: "religion")
        dictionary.setValue(self.score, forKey: "score")
        dictionary.setValue(self.lastSeen, forKey: "lastSeen")
        dictionary.setValue(self.location_city, forKey: "location_city")
        dictionary.setValue(self.gender, forKey: "gender")
        dictionary.setValue(self.age, forKey: "age")
        dictionary.setValue(self.drinking_id, forKey: "drinking_id")
        dictionary.setValue(self.smoking_id, forKey: "smoking_id")
        dictionary.setValue(self.image_id, forKey: "image_id")
        dictionary.setValue(self.karma_points, forKey: "karma_points")
        dictionary.setValue(self.isOnline, forKey: "isOnline")
        dictionary.setValue(self.fitness, forKey: "fitness")
        dictionary.setValue(self.relationship_status_id, forKey: "relationship_status_id")
        dictionary.setValue(self.height, forKey: "height")
        dictionary.setValue(self.weight, forKey: "weight")
        dictionary.setValue(self.total_work_experience_months, forKey: "total_work_experience_months")
        dictionary.setValue(self.relationship_status, forKey: "relationship_status")
        dictionary.setValue(self.political_view_id, forKey: "political_view_id")
        dictionary.setValue(self.bio, forKey: "bio")
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.smoking, forKey: "smoking")
        dictionary.setValue(self.dob, forKey: "dob")
        dictionary.setValue(self.fitness_id, forKey: "fitness_id")
        dictionary.setValue(self.blood_group, forKey: "blood_group")
        dictionary.setValue(self.__v, forKey: "__v")
        dictionary.setValue(self.user_id, forKey: "user_id")
        dictionary.setValue(self.political_view, forKey: "political_view")

        return dictionary
    }

}
