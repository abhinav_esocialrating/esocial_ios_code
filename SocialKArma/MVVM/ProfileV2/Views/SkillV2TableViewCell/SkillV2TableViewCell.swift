//
//  SkillV2TableViewCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 29/07/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import FloatRatingView

class SkillV2TableViewCell: UITableViewCell {
    @IBOutlet weak var skillLabel: UILabel!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    
    var screenType = 0  // 1: Skill   2: Interest
    var skill: SkillModel?
    var interest: InterestModel?
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setValues() {
        if screenType == 1 {
            skillLabel.text = skill?.skill
            ratingView.rating = Double(skill?.score ?? 1)
        } else {
            skillLabel.text = interest?.interest
            ratingView.rating = Double(interest?.score ?? 1)
        }
    }
    
}
