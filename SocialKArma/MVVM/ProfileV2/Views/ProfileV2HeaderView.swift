//
//  ProfileV2HeaderView.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 04/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProfileV2HeaderView: UIView {
    
    @IBOutlet weak var medalStack: UIStackView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var reviewersLabel: UILabel!
    
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var percentLabel: UILabel!
    
    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet weak var percentStackView: UIStackView!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var cameraButton: UIButton!
    
    @IBOutlet weak var scoreViewTop: NSLayoutConstraint!
    
    var userType = 1  // 1: self  2: other
    var userInfo = [String:Any]() {
        didSet {
            setValues()
        }
    }
    var editTap: ((_ dict: [String:Any], _ type: Int) -> Void)? = nil // 1: edit name  2: open messages
    var profilePicTap: ((_ url: String) -> Void)? = nil
    var editProfilePic: completionBlock? = nil
    var imageUrl = ""
    // MARK: - Methods
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func setValues() {
//        if let
        // name score reviews medals image_url
        if let name = userInfo["name"] as? String {
            nameLabel.text = name
        }
        if let score = userInfo["score"] as? Double {
            scoreLabel.text = String(format: "%.1f", score)
        }
        if let score = userInfo["reviews"] as? Int {
            reviewersLabel.text = "Feedback: \(score)"//String(format: "Goodness score: %.1f", score)
        }
        if let url = userInfo["image_id"] as? String, url != "" {
            userImg.sd_setImage(with: URL(string: url)!, completed: nil)
            imageUrl = url
        } else {
            userImg.image = UIImage(named: CommonData.avatars.randomElement() ?? "")
        }
        if let medals = userInfo["medals"] as? [Any] {
            medalStack.arrangedSubviews.forEach({$0.removeFromSuperview()})
            medals.forEach { (medal) in
                if let medal = medal as? [String: Any], let image_url = medal["image_url"] as? String, image_url != "" {
                    let img = UIImageView()
                    medalStack.addArrangedSubview(img)
                    img.anchor(top: nil, paddingTop: 0, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, width: 20, height: 20)
                    img.sd_setImage(with: URL(string: image_url), completed: nil)
                }
            }
        }
        if let profile_percentage = userInfo["profile_percentage"] as? Float {
            progressView.progress = profile_percentage/100
            percentLabel.text = "\(Int(profile_percentage))% Profile completed"
        }
        
    }
    // MARK: - Web Services
    func getUserInfo(parameters: [String:Any]) {

        NetworkManager.getUserInfo(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getUserInfo other")
            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"]
                let dict = data.dictionaryObject
                self.userInfo = dict ?? [:]
            default:
                _ = json["message"].stringValue
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func editTapped(_ sender: Any) {
        if userType == 1 {
            editTap?([:], 1)
        } else {
            var dict = [String:Any]()
            if let name = userInfo["name"] as? String {
                dict["name"] = name
            }
            if let url = userInfo["image_id"] as? String, url != "" {
                dict["image_id"] = url
            }
            editTap?(dict, 2)
        }
    }
    
    @IBAction func profilePicTapped(_ sender: Any) {
        profilePicTap?(imageUrl)
    }
    
    @IBAction func cameraTapped(_ sender: Any) {
        editProfilePic?()
    }
}
// MARK: - Name Changed
extension ProfileV2HeaderView {
    func addAboutMeNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(aboutMeChanged), name: Notification.Name.Profile.AboutMe, object: nil)

    }
    @objc func aboutMeChanged(_ notification: Notification) {
        // Age and Distance
        var distanceAndAge = ""
        if userType == 1 {
            if let age = ProfileV2Data.sharedInstance.dataSourceAboutMe["DOB"], age != "" {
                let formatter = DateFormatter()
                formatter.timeZone = TimeZone.init(abbreviation: "UTC")
                formatter.locale = Locale.current
                formatter.dateFormat = "yyyy-MM-dd"
                let fromDate = formatter.date(from: age) ?? Date()
                let ageYears = Utilities().days(from: fromDate, to: Date())
                distanceAndAge = "\(ageYears)"
            }
            if let loc = ProfileV2Data.sharedInstance.dataSourceAboutMe["Location"] {
                if distanceAndAge == "" {
                    distanceAndAge = loc
                } else {
                    distanceAndAge.append("  \u{2022}  \(loc)")
                }
            }
            locationLabel.text = distanceAndAge
        }
        else {
            if let age = ProfileV2Data.sharedInstance.dataSourceOtherAboutMe["DOB"], age != "" {
                let formatter = DateFormatter()
                formatter.timeZone = TimeZone.init(abbreviation: "UTC")
                formatter.locale = Locale.current
                formatter.dateFormat = "yyyy-MM-dd"
                let fromDate = formatter.date(from: age) ?? Date()
                let ageYears = Utilities().days(from: fromDate, to: Date())
                distanceAndAge = "\(ageYears)"
            }
            if let loc = ProfileV2Data.sharedInstance.dataSourceOtherAboutMe["Location"] {
                if distanceAndAge == "" {
                    distanceAndAge = loc
                } else {
                    distanceAndAge.append("  \u{2022}  \(loc)")
                }
            }
            locationLabel.text = distanceAndAge
        }
    }
    func addPicEditNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(profilePicChanged), name: Notification.Name.AppNotifications.ProfilePicChanged, object: nil)
    }
    @objc func profilePicChanged() {
        if userType == 1 {
            handleUserInfo()
        }
    }
    func handleUserInfo() {
        let dsv = UserDefaults.standard.userInfo
        let data = dsv.data(using: .utf8)
        let json = JSON(data ?? Data())
        let dict = json.dictionaryObject
        userInfo = dict ?? [String: Any]()
//        if let url = userInfo["image_id"] as? String, url != "" {
//            userImg.sd_setImage(with: URL(string: url)!, completed: nil)
//        } else {
//            userImg.image = UIImage(named: CommonData.avatars.randomElement() ?? "")
//        }
    }
}
