//
//  OnboardingNavigationController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/11/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class OnboardingNavigationController: UINavigationController, UIGestureRecognizerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        interactivePopGestureRecognizer?.delegate = self
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }

}
