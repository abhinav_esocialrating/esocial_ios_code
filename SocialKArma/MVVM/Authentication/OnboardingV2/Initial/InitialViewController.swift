//
//  InitialViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/11/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    
    
    @IBOutlet weak var signUpBtn: UIButton!
    
    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }
    

    func configureView() {
        signUpBtn.layer.cornerRadius = 12
        loginBtn.setUpBorder(width: 1, color: UIColor.Colors.blue, radius: 12)
        
    }
    // MARK: - Methods
    func openSignUpController(requestType: Int) {
        let vc = SignUpViewController.instantiate(fromAppStoryboard: .Main)
        vc.requestType = requestType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    // MARK: - Actions
    @IBAction func registerTapped(_ sender: Any) {
        let vc = RegisterViewController.instantiate(fromAppStoryboard: .Onboarding)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        openSignUpController(requestType: 2)
    }
}
