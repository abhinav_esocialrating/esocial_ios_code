//
//  ProfilePicViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/11/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ProfilePicViewController: UIViewController {

    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var profilePic: UIButton!
    
    let imagePicker = ImagePicker()
    var imageUrl = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
    

    func configureView() {
        nextBtn.layer.cornerRadius = nextBtn.frame.height/2
        profilePic.rounded()
    }
    
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func picTapped(_ sender: Any) {
        imagePicker.delegate = self
        let alert = imagePicker.pickImage(sourceController: self, path: "")
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        OnboardingProcess.shared.nextV2(vc: self)
    }
    
    @IBAction func skipTapped(_ sender: Any) {
        
        OnboardingProcess.shared.skip(vc: self)
    }
    
}
// MARK: - ImagePicker methods
extension ProfilePicViewController: ImagePickerUploaderDelegate {
    func PickerSuccess(withImage: UIImage) {
//        presentCropViewController(image: withImage)
        imagePicker.uploadProfilePic(image: withImage) { (success, json, message) in
            if success {
                let url = json["data"].stringValue
                self.profilePic.sd_setImage(with: URL(string: url)!, for: .normal, completed: nil)
                Utilities().updateProfilePicUrl(url: url)
                self.imageUrl = url
            } else {
                self.alert(message: message)
            }
        }
    }
    func PickerFailed(withError: String) {
    }
    func profilePicRemoved(url: String!) {
//        self.profilePic.sd_setImage(with: URL(string: url)!, for: .normal, placeholderImage: UIImage(named: "AddProfilePic"), options: [], context: nil)
        Utilities().updateProfilePicUrl(url: url)
        profilePic.setImage(UIImage(named: "AddProfilePic"), for: .normal)
    }
}
