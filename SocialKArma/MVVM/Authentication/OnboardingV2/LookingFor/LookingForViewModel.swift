//
//  LookingForViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/11/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class LookingForViewModel: NSObject {
    
    var selectedIds: [Int] = []
    
    func getWorkProfile(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.getWorkProfile(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
    //            print(json)
            switch response.response?.statusCode {
            case 200:
                self.saveConfigData(json: json)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func saveConfigData(json: JSON) {
        let data = json["data"].arrayValue
        UserDetails.shared.workSpace = []
        UserDetails.shared.purpose = []
        UserDetails.shared.genderIDs = []
        data.forEach({ (jsonDict) in
            let dict = jsonDict.dictionaryValue
            if dict["type"]?.stringValue == "SHOW" {
                UserDetails.shared.workSpace.append(dict)
            }
            if dict["type"]?.stringValue == "HIDE" {
                UserDetails.shared.genderIDs.append(dict)
            }
        })

    }
}
