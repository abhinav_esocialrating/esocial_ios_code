//
//  OnboardingLookingForCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/11/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class OnboardingLookingForCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    var viewModel: LookingForViewModel! = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        label.layer.masksToBounds = true
    }

    func setValues() {
        let dict = UserDetails.shared.workSpace[indexPath.row]
        label.text = dict["value"]?.stringValue
        let id = dict["id"]?.intValue ?? 0
        
        if viewModel.selectedIds.contains(id) {
            label.backgroundColor = .black
            label.textColor = .white
            label.setUpBorder(width: 1, color: .black, radius: label.frame.height/2)
        } else {
            label.backgroundColor = .white
            label.textColor = .black
            label.setUpBorder(width: 1, color: .black, radius: label.frame.height/2)
        }
    }

}
