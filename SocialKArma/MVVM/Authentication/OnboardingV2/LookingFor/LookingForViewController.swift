//
//  LookingForViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/11/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import CoreLocation

class LookingForViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var nextBtn: UIButton!
    
    let viewModel = LookingForViewModel()
    var locationManager: LocationManager!

    override func viewDidLoad() {
        super.viewDidLoad()

        getConfigData()
        configureView()
        
    }
    

    func configureView() {
        nextBtn.layer.cornerRadius = 12
        
        tableView.dataSource = self
        tableView.delegate = self
    }

    // MARK: - Actions
    @IBAction func nextTapped(_ sender: Any) {
        if viewModel.selectedIds.count == 0 {
            alert(message: "Please select atleast one option!")
        } else {
            OnboardingProcess.shared.basicDetailRequestDict["purpose"] = viewModel.selectedIds
            OnboardingProcess.shared.nextV2(vc: self)
        }
    }
    @IBAction func backTappde(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    // MARK: - Methods
    func initializeLocation() {
        locationManager = LocationManager.shared
        locationManager.delegate = self
    }
    func openPermissionsScreen() {
        let vc = AppPermissionsViewController.instantiate(fromAppStoryboard: .Main)
        vc.modalPresentationStyle = .overCurrentContext
        vc.passData = self
        present(vc, animated: false, completion: nil)
    }
    func checkForContactPermission() {
        if !UserDefaults.standard.isContactPermission {
            UserDefaults.standard.isContactPermission = true
            PhoneContacts.requestAccess { (granted) in
                if granted {
                    DispatchQueue.main.async {
                        self.initializeLocation()
                    }
                    DispatchQueue.global(qos: .utility).async {
                        self.loadContacts()
                    }
                } else {
                    DispatchQueue.main.async {
    //                    self.submitButton.isUserInteractionEnabled = false
                        self.alertWithAction(message: Constants.kAlertContactPermissionDenied.rawValue) {
                            self.initializeLocation()
                        }
                    }
                }
            }
        }
    }
    func loadContacts() {
        UserContacts.sharedInstance.loadContacts()
    }
}
extension LookingForViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserDetails.shared.workSpace.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.OnboardingLookingForCell.rawValue, for: indexPath) as! OnboardingLookingForCell
        cell.indexPath = indexPath
        cell.viewModel = viewModel
        cell.setValues()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = UserDetails.shared.workSpace[indexPath.row]
        let id = dict["id"]?.intValue ?? 0

        if viewModel.selectedIds.contains(id) {
            viewModel.selectedIds = viewModel.selectedIds.filter({ $0 != id })
        } else {
            viewModel.selectedIds.append(id)
        }
        tableView.reloadData()
    }
}
// MARK: - Web Services
extension LookingForViewController {
    func getConfigData() {
        viewModel.getWorkProfile(parameters: [:]) { (success, json, message) in
            if success {
                self.openPermissionsScreen()
                self.tableView.reloadData()
            }
        }
    }
}
// MARK: - Location Service
extension LookingForViewController: LocationManagerDelegate {
    func tracingLocation(currentLocation: CLLocation) {
//        locationManager.stopUpdatingLocation()
//        checkForContactPermission()
    }
    
    func tracingLocationDidFailWithError(error: NSError) {
        print("tracingLocationDidFailWithError")
//        checkForContactPermission()
    }
}
// MARK: - SendData delegate
extension LookingForViewController: SendData {
    func passData(dataPasser: Any, data: Any) {
        if let granted = data as? Bool {
            if granted {
                checkForContactPermission()
            }
        }
    }
    
    
}
