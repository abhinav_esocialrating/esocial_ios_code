//
//  RegisterViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/11/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var nameTF: ACFloatingTextfield!
    @IBOutlet weak var dobTF: ACFloatingTextfield!
    @IBOutlet weak var genderTF: ACFloatingTextfield!
    @IBOutlet weak var emailTF: ACFloatingTextfield!
    @IBOutlet weak var codeTF: ACFloatingTextfield!
    @IBOutlet weak var numberTF: ACFloatingTextfield!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var termsActiveLabel: ActiveLabel!

    // Pop up
    @IBOutlet var signupPopup: UIView!
    @IBOutlet weak var popUpTapView: UIView!
    @IBOutlet weak var popUpCheckBTn: UIButton!
    @IBOutlet weak var popUpTermsActiveLabel: ActiveLabel!
    @IBOutlet weak var popUpContinueBtn: UIButton!
    @IBOutlet weak var popUpCancelBtn: UIButton!
    @IBOutlet weak var popUpContainerView: UIView!

    let dropDown = DropDown()
    var selectedCountryCode = "+91"
    let viewModel = RegisterViewModel()

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTermsActiveLabel(label: termsActiveLabel)
    }
    

    func configureView() {
        nextBtn.layer.cornerRadius = 12
        
        for i in [nameTF, dobTF, genderTF, emailTF, codeTF, numberTF] {
            i?.lineColor = .gray
            i?.placeHolderColor = .gray
            i?.selectedLineColor = .gray
            i?.selectedPlaceHolderColor = .gray
            i?.delegate = self
            i?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 16)
            i?.textColor = UIColor.NewTheme.darkGray
        }
    }

    // MARK: - Helper methods
    func openCalendar() {
        let vc = CalendarViewController()
        vc.sendData = self
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false, completion: nil)
    }
    func openCountryPicker() {
        let vc = CountryPickerViewController()
        vc.selectionAction = { name, code in
            self.codeTF.text = code
            self.selectedCountryCode = code
        }
        navigationController?.pushViewController(vc, animated: true)
    }

    func moveToVerifyOTP() {
        UserDetails.shared.isComingFromSignIn = 1
        view.endEditing(true)
        let vc = VerifyOTPViewController.instantiate(fromAppStoryboard: .Main)
        vc.phoneNumber = numberTF.text!
        vc.countryCode = selectedCountryCode
        vc.requestType = 1
        navigationController?.pushViewController(vc, animated: true)
    }
    func isValid() -> Bool {
        var valid = true
        if nameTF.text!.count < 2 || nameTF.text!.count > 30 {
            nameTF.showErrorWithText(errorText: "Name length should be between 2 and 30")
            return false
        } else if dobTF.text! == "" {
            dobTF.showErrorWithText(errorText: "Age should be atleast 14")
//            alert(message: "Age should be atleast 14")
            return false
        } else if genderTF.text! == "" {
            genderTF.showErrorWithText(errorText: "Please fill the Gender field!")
//            alert(message: "Please fill the Gender field!")
            return false
        } else if !Utilities.isValidEmail(emailTF.text!) {
            emailTF.showErrorWithText(errorText: "Please fill the Gender field!")
            alert(message: "Please enter a valid email!")
            return false
        } else if numberTF.text!.isEmpty || numberTF.text!.count < 5 {
            numberTF.showErrorWithText(errorText: "Please enter a valid phone number")
            valid = false
        } else if !checkBtn.isSelected {
            valid = false
            alert(message: "Please accept Terms and Conditions first!")
        } else {
            nameTF.errorTextColor = .clear
            dobTF.errorTextColor = .clear
            emailTF.errorTextColor = .clear
            genderTF.errorTextColor = .clear
            numberTF.errorTextColor = .clear
        }
        return valid
    }
    func configureTermsActiveLabel(label: ActiveLabel) {
        let customType1 = ActiveType.custom(pattern: "Terms Of Services")
        let customType2 = ActiveType.custom(pattern: "Privacy Policy")
        label.enabledTypes = [customType1, customType2]
        label.text = "I have read and agreed to Terms Of Services and Privacy Policy"
        label.customColor[customType1] = UIColor.Colors.blue
        label.customColor[customType2] = UIColor.Colors.blue
        label.handleCustomTap(for: customType1) { (dkjf) in
            self.openTermsPage(type: 1)
        }
        label.handleCustomTap(for: customType2) { (dkjf) in
            self.openTermsPage(type: 2)
        }
    }
    // Pop up
    func openSignupPopUp() {
        view.addSubview(signupPopup)
        signupPopup.anchor(top: view.topAnchor, paddingTop: 0, bottom: view.bottomAnchor, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 0, right: view.rightAnchor, paddingRight: 0, width: 0, height: 0)
        let tap = UITapGestureRecognizer(target: self, action: #selector(signupPopUpTapped(_:)))
        popUpTapView.addGestureRecognizer(tap)
        configureTermsActiveLabel(label: popUpTermsActiveLabel)
        popUpCancelBtn.setUpBorder(width: 0.8, color: .darkGray, radius: 8)
        popUpContinueBtn.layer.cornerRadius = 8
    }
    func removeSignUpPopUp() {
        signupPopup.removeFromSuperview()
    }
    @objc func signupPopUpTapped(_ gesture: UITapGestureRecognizer) {
        popUpTapView.removeGestureRecognizer(gesture)
        removeSignUpPopUp()
    }
    
    // MARK: - Actions
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
//        moveToVerifyOTP()
        
        if RechabilityHandler.shared.reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            if isValid() {
                if numberTF.text! == Constants.DemoPhoneNumber {
                    UserDefaults.standard.isDemoPhoneNumber = true
                } else {
                    UserDefaults.standard.isDemoPhoneNumber = false
                }
                UserDefaults.standard.userName = nameTF.text!
                callSignUpAPI(requestType: 1)
            }
        }
    }
    @IBAction func checkBtnTapped(_ sender: Any) {
        checkBtn.isSelected = !checkBtn.isSelected
        checkBtn.tintColor = checkBtn.isSelected ? UIColor.Colors.blue : UIColor.NewTheme.lightGray
    }

    @IBAction func popUpCheckBtnTapped(_ sender: Any) {
        popUpCheckBTn.isSelected = !popUpCheckBTn.isSelected
        popUpCheckBTn.tintColor = popUpCheckBTn.isSelected ? UIColor.Colors.blue : UIColor.NewTheme.lightGray
    }
    @IBAction func popUpContinueTapped(_ sender: Any) {
        if popUpCheckBTn.isSelected {
            callSignUpAPI(requestType: 1)
        } else {
            alert(message: "Please accept Terms and Conditions first!")
        }
    }
    
    @IBAction func popUpCancelTapped(_ sender: Any) {
        removeSignUpPopUp()
//        self.requestType = 1
    }
    // MARK: - Web Services
    func callSignUpAPI(requestType: Int) {
        self.showHud()
        let parameters = ["number":numberTF.text!,
                          "countryCode": selectedCountryCode,
                          "fcmId": UserDefaults.standard.FirebaseInstanceId,
                          "inviteId": UserDefaults.standard.TZInviteCode,
                          "name": nameTF.text!,
                          "dob": dobTF.text!,
                          "gender": genderTF.text!,
                          "email": emailTF.text!,
                          "latitude":"23.23",
                          "longitude":"32.234"
        ]
        viewModel.register(requestType: requestType, parameters: parameters) { (success, json, message) in
            self.hideHud()
            if success {
                self.removeSignUpPopUp()
                self.moveToVerifyOTP()
            } else {
                if message == kAlertUserAlreadyExists {
                    self.alertWithTwoAction(message: kAlertAskForMovingToSignIn, okHandler: {
                        self.navigationController?.popViewController(animated: true)
                    }) {}
                } else {
                    self.alert(message: message)
                }

                
            }
        }
    }
}
// MARK: - TextField delegates
extension RegisterViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case dobTF:
            openCalendar()
            return false
        case genderTF:
            showDropDown(textField: textField)
            return false
        case codeTF:
            openCountryPicker()
            return false
        default:
            return true
        }
    }
}
// MARK: - Dropdown
extension RegisterViewController {
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 200
        switch textField {
        case genderTF:
            dropDown.dataSource = ["Male", "Female"]
        default:
            AppDebug.shared.consolePrint("")
        }
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropDownSelection(index: index, item: item, textField: textField)
        }
        dropDown.show()
    }
    func handleDropDownSelection(index: Int, item: String, textField: UITextField) {
        switch textField {
        case genderTF:
            textField.text = item
        default:
            AppDebug.shared.consolePrint("")
        }
    }
    
}
// MARK: - SendData methods
extension RegisterViewController: SendData {
    func passData(dataPasser: Any, data: Any) {
        if dataPasser is CalendarViewController {
            if let date = data as? String, date != "" {
                dobTF.text = date
            }
        }
    }
}
