//
//  RegisterViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/11/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class RegisterViewModel: NSObject {
    func register(requestType: Int, parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.register(requestType: requestType, parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
