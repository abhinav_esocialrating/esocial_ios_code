//
//  UserDetails.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/10/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class UserDetails {
    
    static let shared = UserDetails()

    private init(){}
    
    var isComingFromSignIn = 0  // 1: Sign up   2: Sign in
    var name = ""
    var age = 9
    var gender = ""
    var email = ""
    var profilePic: UIImage?
    var whoAreYou = ""
    var workSpace = [[String:JSON]]()
    var genderIDs = [[String:JSON]]()
    var purpose = [[String:JSON]]()
    var selectedWorkSpace = [Int]()
    var selectedPurpose = [Int]()
    var selectedGenderIDs = [Int]()
    
    func reset() {
        workSpace = []
        genderIDs = []
        purpose = []
        selectedWorkSpace = []
        selectedPurpose = []
        selectedGenderIDs = []
        profilePic = nil
        name = ""
        age = 9
        gender = ""
        whoAreYou = ""
    }
    
}
