//
//  OnboardingCompensationViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/11/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
//import Alamofire

class OnboardingCompensationViewModel: NSObject {
    
    var monthlyData = [[String:JSON]]()
    var hourlyData = [[String:JSON]]()
    var selectedMonthId = 0
    var selectedHourId = 0

    func getCompensationConfig(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getCompensationConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].dictionaryValue
//                ConfigDataModel.shared.parseData(json: list)
                var jsonArray = data["monthlyMaster"]?.arrayValue ?? []
                self.monthlyData = jsonArray.map { $0.dictionaryValue }
                jsonArray = data["hourlyMaster"]?.arrayValue ?? []
                self.hourlyData = jsonArray.map { $0.dictionaryValue }
                completion(true, JSON(), "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON(), message)
            }
        }
    }
    
    func createBasicProfile(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.createBasicProfile(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
