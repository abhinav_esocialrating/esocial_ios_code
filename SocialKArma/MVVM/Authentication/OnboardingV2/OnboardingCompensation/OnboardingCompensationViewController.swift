//
//  OnboardingCompensationViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/11/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class OnboardingCompensationViewController: UIViewController {
    
    @IBOutlet weak var monthTF: ACFloatingTextfield!
    @IBOutlet weak var houtTF: ACFloatingTextfield!
    @IBOutlet weak var nextBtn: UIButton!
    
    let dropDown = DropDown()
    let viewModel = OnboardingCompensationViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        getCompensationConfig()
    }
    

    func configureView() {
        nextBtn.layer.cornerRadius = 12
        
        for i in [monthTF, houtTF] {
            i?.lineColor = .gray
            i?.placeHolderColor = .gray
            i?.selectedLineColor = .gray
            i?.selectedPlaceHolderColor = .gray
            i?.delegate = self
            i?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 16)
            i?.textColor = UIColor.NewTheme.darkGray
        }
    }
    
    // MARK: - Actions

    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        if monthTF.text == "" && houtTF.text == "" {
            alert(message: "Please enter atleast one detail")
        } else {
            if viewModel.selectedMonthId != 0 {
                OnboardingProcess.shared.basicDetailRequestDict["monthly_compensation_id"] = viewModel.selectedMonthId
            }
            if viewModel.selectedHourId != 0 {
                OnboardingProcess.shared.basicDetailRequestDict["hourly_compensation_id"] = viewModel.selectedHourId
            }
            
            AppDebug.shared.consolePrint(OnboardingProcess.shared.basicDetailRequestDict)
            createBasicProfile(params: OnboardingProcess.shared.basicDetailRequestDict)
        }
    }
    // MARK: - Web Services
    func getCompensationConfig() {
        viewModel.getCompensationConfig(parameters: [:]) { (_, _, _) in
        }
    }
    func createBasicProfile(params: [String : Any]) {
        self.showHud()
        viewModel.createBasicProfile(parameters: params) { (success, json, message) in
            self.hideHud()
            if success {
                UserDefaults.standard.isFormComplete = true
                OnboardingProcess.shared.nextV2(vc: self)
            } else {
                self.alert(message: message)
            }
        }
    }
}
// MARK: - TextField delegates
extension OnboardingCompensationViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        showDropDown(textField: textField)
        return false
    }
}
// MARK: - Dropdown
extension OnboardingCompensationViewController {
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = dropDown.anchorView?.plainView.bounds.width
        switch textField {
        case monthTF:
            dropDown.dataSource = viewModel.monthlyData.map({ (status) -> String in
                return (status["compensation"]?.stringValue ?? "")
            })
        default:
            dropDown.dataSource = viewModel.hourlyData.map({ (status) -> String in
                return (status["compensation"]?.stringValue ?? "")
            })
        }
        
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropDownSelection(index: index, item: item, textField: textField)
        }
        dropDown.show()
    }
    func handleDropDownSelection(index: Int, item: String, textField: UITextField) {
        textField.text = item
        switch textField {
        case monthTF:
            viewModel.selectedMonthId = viewModel.monthlyData[index]["compensation_id"]?.intValue ?? 0
        default:
            viewModel.selectedHourId = viewModel.hourlyData[index]["compensation_id"]?.intValue ?? 0
        }
    }
    
}
