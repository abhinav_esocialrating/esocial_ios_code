//
//  SignUpViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SignUpViewModel: NSObject {
    func register(requestType: Int, parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.register(requestType: requestType, parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
