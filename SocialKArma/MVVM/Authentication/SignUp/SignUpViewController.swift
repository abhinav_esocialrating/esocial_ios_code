//
//  SignUpViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class SignUpViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var nameTF: ACFloatingTextfield!
    @IBOutlet weak var phoneTF: ACFloatingTextfield!
    @IBOutlet weak var codeTF: ACFloatingTextfield!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var signUpLabel: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var termsActiveLabel: ActiveLabel!
    // Pop Up
    @IBOutlet var signupPopup: UIView!
    @IBOutlet weak var popUpCheckBTn: UIButton!
    @IBOutlet weak var popUpTermsActiveLabel: ActiveLabel!
    @IBOutlet weak var popUpContinueBtn: UIButton!
    @IBOutlet weak var popUpCancelBtn: UIButton!
    @IBOutlet weak var popUpContainerView: UIView!
    @IBOutlet weak var popUpTapView: UIView!
    // MARK: - Variables
    let viewModel = SignUpViewModel()
    var requestType = 1  // 1: Sign up   2: Sign in
    let reachability = try! Reachability()
    let dropDown = DropDown()
    var selectedCountryCode = "+91"
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        do{
          try reachability.startNotifier()
        }catch{
          print("could not start reachability notifier")
        }
        #if DEBUG
        phoneTF.text = "8630289572"
        #endif
    }
    func configureView() {
        navigationController?.interactivePopGestureRecognizer?.delegate = self
//        signUpLabel.text = requestType == 1 ? "SIGN UP" : "SIGN IN"
//        signUpButton.setTitle(requestType == 1 ? "SIGN UP" : "SIGN IN", for: .normal)
        
        phoneTF.errorTextColor = .red
        phoneTF.placeHolderColor = darkBlue
        phoneTF.selectedPlaceHolderColor = darkBlue
        phoneTF.lineColor = darkBlue
        phoneTF.selectedLineColor = darkBlue
        phoneTF.disableFloatingLabel = true
        
        codeTF.errorTextColor = .red
        codeTF.placeHolderColor = darkBlue
        codeTF.selectedPlaceHolderColor = darkBlue
        codeTF.lineColor = darkBlue
        codeTF.selectedLineColor = darkBlue
        codeTF.disableFloatingLabel = true
        
        signUpButton.layer.cornerRadius = 8
        signUpButton.setAttributedTitle(Utilities.attributedText(text: "Request OTP", font: .fontWith(name: .Roboto, face: .Medium, size: 19), color: .white), for: .normal)
        
        configureTermsActiveLabel(label: termsActiveLabel)
        if requestType == 2 {
            checkBtn.isHidden = true
            termsActiveLabel.isHidden = true
        }
    }
    func configureTermsActiveLabel(label: ActiveLabel) {
        let customType1 = ActiveType.custom(pattern: "Terms Of Services")
        let customType2 = ActiveType.custom(pattern: "Privacy Policy")
        label.enabledTypes = [customType1, customType2]
        label.text = "I have read and agreed to Terms Of Services and Privacy Policy"
        label.customColor[customType1] = UIColor.Colors.blue
        label.customColor[customType2] = UIColor.Colors.blue
        label.handleCustomTap(for: customType1) { (dkjf) in
            self.openTermsPage(type: 1)
        }
        label.handleCustomTap(for: customType2) { (dkjf) in
            self.openTermsPage(type: 2)
        }
    }
    // MARK: - Actions
    @IBAction func proceedButtonTapped(_ sender: Any) {
        if RechabilityHandler.shared.reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            if isValid() {
                if phoneTF.text! == Constants.DemoPhoneNumber {
                    UserDefaults.standard.isDemoPhoneNumber = true
                } else {
                    UserDefaults.standard.isDemoPhoneNumber = false
                }
                callSignUpAPI(requestType: requestType)
            }
        }
    }

    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func checkBtnTapped(_ sender: Any) {
        checkBtn.isSelected = !checkBtn.isSelected
        checkBtn.tintColor = checkBtn.isSelected ? UIColor.Colors.blue : UIColor.NewTheme.lightGray
    }
    @IBAction func popUpCheckBtnTapped(_ sender: Any) {
        popUpCheckBTn.isSelected = !popUpCheckBTn.isSelected
        popUpCheckBTn.tintColor = popUpCheckBTn.isSelected ? UIColor.Colors.blue : UIColor.NewTheme.lightGray
    }
    @IBAction func popUpContinueTapped(_ sender: Any) {
        if popUpCheckBTn.isSelected {
            callSignUpAPI(requestType: requestType)
        } else {
            alert(message: "Please accept Terms and Conditions first!")
        }
    }
    
    @IBAction func popUpCancelTapped(_ sender: Any) {
        removeSignUpPopUp()
        self.requestType = 2
    }
    // MARK: - Methods
    func callSignUpAPI(requestType: Int) {
        self.showHud()
        let parameters: [String:Any] = ["number":phoneTF.text!, "countryCode": selectedCountryCode, "fcmId": UserDefaults.standard.FirebaseInstanceId, "inviteId": UserDefaults.standard.TZInviteCode]
        viewModel.register(requestType: requestType, parameters: parameters) { (success, json, message) in
            self.hideHud()
            if success {
                self.removeSignUpPopUp()
                self.moveToVerifyOTP()
            } else {
                if message == kAlertUserAlreadyExists {
                    self.requestType = 2
                    self.alertWithTwoAction(message: kAlertAskForSignIn, okHandler: {
                        self.callSignUpAPI(requestType: self.requestType)
                    }) {
                        self.requestType = 1
                    }
                } else if message == kAlertUserNotExist {
                    self.requestType = 1
                    self.view.endEditing(true)
                    self.openSignupPopUp()
                } else {
                    self.alert(message: message)
                }
            }
        }
    }
    
    func moveToVerifyOTP() {
        UserDetails.shared.isComingFromSignIn = requestType
        view.endEditing(true)
        let vc = VerifyOTPViewController.instantiate(fromAppStoryboard: .Main)
        vc.phoneNumber = phoneTF.text!
        vc.countryCode = selectedCountryCode
        vc.requestType = requestType
        navigationController?.pushViewController(vc, animated: true)
    }
    func isValid() -> Bool {
        var valid = true
        if phoneTF.text!.isEmpty || phoneTF.text!.count < 5 {
            phoneTF.showErrorWithText(errorText: "Please enter a valid phone number")
            valid = false
        } else if requestType == 1 && !checkBtn.isSelected {
            valid = false
            alert(message: "Please accept Terms and Conditions first!")
        } else {
            phoneTF.errorTextColor = .clear
        }
        return valid
    }
    func showDropDown() {
        dropDown.anchorView = codeTF
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 250
        dropDown.dataSource = CommonData.countries
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropSelection(index: index)
        }
        dropDown.show()
    }
    func handleDropSelection(index: Int) {
        codeTF.text = CommonData.countryCodes[index]
        selectedCountryCode = CommonData.countryCodes[index]
    }
    func openCountryPicker() {
        let vc = CountryPickerViewController()
        vc.selectionAction = { name, code in
            self.codeTF.text = code
            self.selectedCountryCode = code
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    func openSignupPopUp() {
        view.addSubview(signupPopup)
        signupPopup.anchor(top: view.topAnchor, paddingTop: 0, bottom: view.bottomAnchor, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 0, right: view.rightAnchor, paddingRight: 0, width: 0, height: 0)
        let tap = UITapGestureRecognizer(target: self, action: #selector(signupPopUpTapped(_:)))
        popUpTapView.addGestureRecognizer(tap)
        configureTermsActiveLabel(label: popUpTermsActiveLabel)
        popUpCancelBtn.setUpBorder(width: 0.8, color: .darkGray, radius: 8)
        popUpContinueBtn.layer.cornerRadius = 8
    }
    func removeSignUpPopUp() {
        signupPopup.removeFromSuperview()
    }
    @objc func signupPopUpTapped(_ gesture: UITapGestureRecognizer) {
        popUpTapView.removeGestureRecognizer(gesture)
        removeSignUpPopUp()
    }
}
// MARK: - Textfield delegate methods
extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case codeTF:
//            showDropDown()
            openCountryPicker()
            return false
        default:
            return true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTF {
            let maxLength = 13
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
extension SignUpViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
