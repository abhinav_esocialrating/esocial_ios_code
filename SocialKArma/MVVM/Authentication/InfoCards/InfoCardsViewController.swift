//
//  InfoCardsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 20/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class InfoCardsViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionBottomConst: NSLayoutConstraint!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var proceedButton: UIButton!

    let viewModel = InfoCardsViewModel()
    var completion: completionBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if viewModel.screenType == 5 {
            viewModel.getInviteOnlyCards(parameters: [:]) { (_, _, _) in
                self.collectionView.reloadData()
                self.pageControl.numberOfPages = self.viewModel.cards?.count ?? 0
            }
        } else {
            viewModel.configureRemoteConfig { (success, message) in
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.pageControl.numberOfPages = self.viewModel.imageLinks.count
                }
            }
        }
        configureView()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }
    }
    func configureView() {
        collectionBottomConst.constant = screenHeight > 736 ? 120 : 100
        pageControl.numberOfPages = 2
        pageControl.currentPage = 0
        proceedButton.layer.cornerRadius = 8
        proceedButton.setTitle(viewModel.screenType == 5 ? "Proceed" : "Done", for: .normal)
    }
    // MARK: - Actions
    @IBAction func proceedTapped(_ sender: Any) {
        if viewModel.screenType == 5 {
            // Invite flow removed from here
        } else {
            completion?()
            dismiss(animated: false, completion: nil)
        }
    }
}
// MARK: - CollectionView methods
extension InfoCardsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.screenType == 5 ? (viewModel.cards?.count ?? 0) : viewModel.imageLinks.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if viewModel.screenType == 5 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCells.OnBoardingCollectionViewCell.rawValue, for: indexPath) as! OnBoardingCollectionViewCell
            cell.card = viewModel.cards?[indexPath.row]
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCells.OnBoardingCollectionViewCell.rawValue, for: indexPath) as! OnBoardingCollectionViewCell
            cell.isCoachMark = true
            cell.setImages(url: viewModel.imageLinks[indexPath.row])
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenWidth, height: collectionView.frame.height)
    }
}
// MARK: - UIScrollViewDelegate
extension InfoCardsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
}
