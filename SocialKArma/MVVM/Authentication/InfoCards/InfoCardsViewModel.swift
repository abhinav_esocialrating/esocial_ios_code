//
//  InfoCardsViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 20/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import FirebaseRemoteConfig

class InfoCardsViewModel: NSObject {
    
    var cards: [InfoCardsModel]?
    var remoteConfig: RemoteConfig!
    var screenType = 5   // 0: feedback  1: scorecard  2: find  3: Profile  4: Offers
    var imageLinks = [String]()

    func getInviteOnlyCards(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getInviteOnlyCards(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            switch response.response?.statusCode {
            case 200:
                let array = json["data"].arrayObject
                self.cards = InfoCardsModel.modelsFromDictionaryArray(array: (array ?? []) as NSArray)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func configureRemoteConfig(completion: @escaping ((_ success: Bool, _ message: String) -> Void)) {
        remoteConfig = RemoteConfig.remoteConfig()
        
        var keys = [String]()
        switch screenType {
        case 0:
            keys = [Constants.FirebaseFeedback3Key.rawValue, Constants.FirebaseFeedback1Key.rawValue, Constants.FirebaseFeedback2Key.rawValue]
        case 1:
            keys = [Constants.FirebaseScore1Key.rawValue, Constants.FirebaseScore2Key.rawValue, Constants.FirebaseScore3Key.rawValue]
        case 2:
            keys = [Constants.FirebaseFind1Key.rawValue, Constants.FirebaseFind2Key.rawValue]
        default:
            keys = [Constants.FirebaseFeedback1Key.rawValue, Constants.FirebaseFeedback2Key.rawValue]
        }
        
        // Debug mode only
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        
        remoteConfig.fetch() { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate() { (error) in
                    keys.forEach { (key) in
                        let value = self.remoteConfig.configValue(forKey: key)
                        self.imageLinks.append(value.stringValue ?? "")
                    }
                    completion(true, "")
                }
            } else {
                print("Config not fetched")
                print("Error: \(error?.localizedDescription ?? "No error available.")")
                completion(false, "")
            }
        }
    }
}
