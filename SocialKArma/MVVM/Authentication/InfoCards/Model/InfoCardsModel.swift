//
//  InfoCardsModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 20/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class InfoCardsModel {
    public var background_image : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let InfoCardsModel_list = InfoCardsModel.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of InfoCardsModel Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [InfoCardsModel]
    {
        var models:[InfoCardsModel] = []
        for item in array
        {
            models.append(InfoCardsModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let InfoCardsModel = InfoCardsModel(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: InfoCardsModel Instance.
*/
    required public init?(dictionary: NSDictionary) {

        background_image = dictionary["background_image"] as? String
    }

        
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.background_image, forKey: "background_image")

        return dictionary
    }

}
