//
//  WekcomeViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 22/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class WekcomeViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        nameLabel.text = UserDefaults.standard.userName
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
            appDelegate?.setUpSideMenuAndRootView()
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
}
