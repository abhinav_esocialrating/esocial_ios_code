//
//  AppPermissionsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 22/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class AppPermissionsViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var locationDescLabel: UILabel!
    @IBOutlet weak var contactDescLabel: UILabel!
    var passData: SendData!
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        contactDescLabel.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: screenWidth == 320 ? 15 : 17)
        locationDescLabel.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: screenWidth == 320 ? 15 : 17)

        button.layer.cornerRadius = 8
    }
    // MARK: - Navigation
    @IBAction func allowTapped(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        passData.passData(dataPasser: self, data: true)
    }
}
