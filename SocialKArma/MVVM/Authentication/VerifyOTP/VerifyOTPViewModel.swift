//
//  VerifyOTPViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Crashlytics
import FirebaseAnalytics

class VerifyOTPViewModel: NSObject {
    
    func verifyOTP(requestType: Int, parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.verifyOTP(requestType: requestType, parameters: parameters, method: .post) { (response) in
            
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            switch response.response?.statusCode {
            case 201:
                let data = json["data"].dictionaryValue
                let access_token = data["token"]?.stringValue ?? ""
                UserDefaults.standard.Authorization = access_token
                let userId = data["user_id"]?.stringValue ?? ""
                UserDefaults.standard.userId = userId
                let inviteLink = data["inviteLink"]?.stringValue ?? ""
                UserDefaults.standard.TZInviteLink = inviteLink
                Crashlytics.sharedInstance().setUserIdentifier("\(access_token) \(userId)")
                let currentVersionStr = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
                UserDefaults.standard.minimumAppVersion = currentVersionStr
                completion(true, json["data"], "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func postInviteCode(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.postInviteCode(parameters: parameters, method: .post) { (response) in
            
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            switch response.response?.statusCode {
            case 201:
                let data = json["data"].dictionaryValue
//                let token_type = data["token_type"]?.stringValue ?? ""
                let access_token = data["token"]?.stringValue ?? ""
//                UserDefaults.standard.Authorization = token_type + " " + access_token
                UserDefaults.standard.Authorization = access_token
                let userId = data["user_id"]?.stringValue ?? ""
                UserDefaults.standard.userId = userId
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func resendOTP(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.resendOTP(parameters: parameters, method: .post) { (response) in
                    
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json, json["message"].stringValue)
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func sendFirebaseAnalytics(requestType: Int) {
        Analytics.logEvent(requestType == 1 ? AnalyticsEventSignUp : AnalyticsEventLogin, parameters: [
            AnalyticsParameterMethod: method
        ])
    }
}
