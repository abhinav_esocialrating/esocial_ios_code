//
//  VerifyOTPViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class VerifyOTPViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var fourthTextField: UITextField!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    // MARK:- Variables
    var viewModel = VerifyOTPViewModel()
    var phoneNumber = ""
    var countryCode = ""
    var requestType = 1  // 1: Sign up   2: Sign in
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }
    func configureView() {
        if UserDefaults.standard.isDemoPhoneNumber {
            phoneNumberLabel.text = Constants.OTPAlreadyFilled
        } else {
//            phoneNumberLabel.text = "OTP sent on \(phoneNumber)"
        }
        setTextFieldEditingChanged()
        for i in [firstTextField,secondTextField,thirdTextField,fourthTextField] {
            i?.layer.borderWidth = 1
            i?.layer.borderColor = UIColor.white.cgColor
            i?.layer.cornerRadius = 8
            (i as! ACFloatingTextfield).selectedLineColor = .clear
        }
        if UserDefaults.standard.isDemoPhoneNumber {
            firstTextField.text = "1"
            secondTextField.text = "0"
            thirdTextField.text = "1"
            fourthTextField.text = "0"
        }
        verifyButton.layer.cornerRadius = 8
        if !UserDefaults.standard.isDemoPhoneNumber {
            firstTextField.becomeFirstResponder()
        }
    }
    // MARK: - Actions
    @IBAction func resendTapped(_ sender: Any) {
        self.showHud()
        let parameters = ["number":phoneNumber,"countryCode": countryCode]
        viewModel.resendOTP(parameters: parameters) { (success, json, message) in
            self.hideHud()
            self.alert(message: message)
        }
    }
    
    @IBAction func verifyOTPTapped(_ sender: Any) {
//        self.handleOtpSuccess()
        
        let validCheck = isValid()
        if validCheck.0 {
            self.showHud()
            let parameters = ["number":phoneNumber, "otp":validCheck.1, "inviteId": UserDefaults.standard.TZInviteCode]
            viewModel.verifyOTP(requestType: requestType, parameters: parameters) { (success, json, message) in
                if success {
                    self.viewModel.sendFirebaseAnalytics(requestType: self.requestType)
                    self.hideHud()
                    self.handleOtpSuccess()
//                    self.moveToHomeScreen(json: json)
                    UserDefaults.standard.isFormComplete = self.requestType == 2 ? true : false
                } else {
                    self.hideHud()
                    self.alert(message: message)
                }
            }
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    // MARK: - Methods
    func isValid() -> (Bool,String) {
        var valid = true
        if firstTextField.text!.isEmpty || secondTextField.text!.isEmpty || thirdTextField.text!.isEmpty || fourthTextField.text!.isEmpty {
            valid = false
        }
        let otp1 = firstTextField.text! + secondTextField.text! + thirdTextField.text! + fourthTextField.text!
        return (valid,otp1)
    }
    
    func handleOtpSuccess() {
        UserDefaults.standard.userPhoneNumber = phoneNumber
        if self.requestType == 1 {
            createOnboardingDetailDatasource()
            OnboardingProcess.shared.nextV2(vc: self)
        } else {
            appDelegate?.setUpSideMenuAndRootView()
        }

    }
    func createOnboardingDetailDatasource() {
        var types = [OnboardingCategoriesV2]()
        
        let lookingFor = OnboardingCategoriesV2(page_name: "Add your Interests", page_type: .LookingFor)
        let skills = OnboardingCategoriesV2(page_name: "How tall do you stand", page_type: .Skills)
        let compensation = OnboardingCategoriesV2(page_name: "How much do you weigh", page_type: .Compensation)
        let profilePic = OnboardingCategoriesV2(page_name: "What type of Drinker are you?", page_type: .ProfilePic)
        types.append(contentsOf: [lookingFor, skills, compensation, profilePic])
        
        types.append(OnboardingCategoriesV2(page_name: "None", page_type: .None))
        
        OnboardingProcess.shared.types2 = types
        OnboardingProcess.shared.currentPage2 = types[0].page_type
    }
    // MARK: - Web Services
    func postInviteCode() {
        let params = ["hostUser": UserDefaults.standard.TZInviteCode]
        viewModel.postInviteCode(parameters: params) { (success, json, message) in
            UserDefaults.standard.TZInviteCode = ""
            DispatchQueue.main.async {
                self.hideHud()
                // Move to Home Screen
            }
        }
    }
}

// MARK: - UITextFieldDelegate
extension VerifyOTPViewController: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        switch textField {
        case firstTextField:
            if !textField.text!.isEmpty {
//                textField.resignFirstResponder()
                _ = secondTextField.becomeFirstResponder()
            }
        case secondTextField:
            if !textField.text!.isEmpty {
//                textField.resignFirstResponder()
                _ = thirdTextField.becomeFirstResponder()
            }
        case thirdTextField:
            if !textField.text!.isEmpty {
//                textField.resignFirstResponder()
                _ = fourthTextField.becomeFirstResponder()
            }
        default:
            if !textField.text!.isEmpty {
                textField.resignFirstResponder()
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return textField.text!.count == 1
        } else {
            return textField.text!.count == 0
        }
    }

    func setTextFieldEditingChanged() {
        firstTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        secondTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        thirdTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        fourthTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)

    }
}
// MARK: - Custom textfield
class OTPTextField: ACFloatingTextfield {
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:1, y:1, width:bounds.size.width, height:bounds.size.height);
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:1, y:1, width:bounds.size.width, height:bounds.size.height);
    }
}
