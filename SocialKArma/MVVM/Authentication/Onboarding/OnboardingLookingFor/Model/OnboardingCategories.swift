//
//  OnboardingCategories.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 06/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation

struct OnboardingCategories {
    var page_name: String
    var page_type: OnboardingCategoryTypes
}
enum OnboardingCategoryTypes {
    case Interests, Drinking, Smoking, Height, Weight, Skills, Categories, None
}

// V2

struct OnboardingCategoriesV2 {
    var page_name: String
    var page_type: OnboardingCategoryTypesV2
}
enum OnboardingCategoryTypesV2 {
    case LookingFor, Skills, Compensation, ProfilePic, None
}
