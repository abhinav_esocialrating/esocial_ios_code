//
//  OnboardingProcess.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 07/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation

class OnboardingProcess {
    // MARK: - Singleton instance
    struct Static {
        fileprivate static var instance: OnboardingProcess?
    }
    class var shared: OnboardingProcess {
        if Static.instance == nil {
            Static.instance = OnboardingProcess()
        }
        return Static.instance!
    }
    func dispose() {
        OnboardingProcess.Static.instance = nil
        print("Disposed Singleton instance")
    }
    
    // MARK: - Variables
    var currentPage: OnboardingCategoryTypes = .None
    var currentPage2: OnboardingCategoryTypesV2 = .None
    var types = [OnboardingCategories]()
    var types2 = [OnboardingCategoriesV2]()
    var currentIndex = -1
    var basicDetailRequestDict = [String:Any]()
    
    func next(vc: UIViewController) {
        currentIndex += 1
        
        if types[currentIndex].page_type == .Interests || types[currentIndex].page_type == .Skills {
            
            let vc1 = OnboardingProfessionalViewController.instantiate(fromAppStoryboard: .Onboarding)
            vc1.onboardingType = types[currentIndex]
            vc.navigationController?.pushViewController(vc1, animated: true)
        } else if types[currentIndex].page_type == .Height || types[currentIndex].page_type == .Weight {
            
            let vc1 = OnboardingPerosnalViewController.instantiate(fromAppStoryboard: .Onboarding)
            vc1.onboardingType = types[currentIndex]
            vc.navigationController?.pushViewController(vc1, animated: true)
        } else if types[currentIndex].page_type == .Drinking || types[currentIndex].page_type == .Smoking {
            
            let vc1 = OnboardingFriendsViewController.instantiate(fromAppStoryboard: .Onboarding)
            vc1.onboardingType = types[currentIndex]
            vc.navigationController?.pushViewController(vc1, animated: true)
        } else {
            vc.openWelcomeScreen()
        }
        
    }
    func nextV2(vc: UIViewController) {
        currentIndex += 1
        
        if types2[currentIndex].page_type == .Skills {
            
            let vc1 = OnboardingProfessionalViewController.instantiate(fromAppStoryboard: .Onboarding)
//            vc1.onboardingType = types[currentIndex]
            vc.navigationController?.pushViewController(vc1, animated: true)
        } else if types2[currentIndex].page_type == .LookingFor {
            
            let vc1 = LookingForViewController.instantiate(fromAppStoryboard: .Onboarding)
            vc.navigationController?.pushViewController(vc1, animated: true)
        } else if types2[currentIndex].page_type == .Compensation {
            
            let vc1 = OnboardingCompensationViewController.instantiate(fromAppStoryboard: .Onboarding)
            vc.navigationController?.pushViewController(vc1, animated: true)
        } else if types2[currentIndex].page_type == .ProfilePic {
            let vc1 = ProfilePicViewController.instantiate(fromAppStoryboard: .Onboarding)
            vc.navigationController?.pushViewController(vc1, animated: true)
        } else {
            vc.openWelcomeScreen()
            currentIndex = -1
        }
    }
    func skip(vc: UIViewController) {
        currentIndex = -1
        vc.openWelcomeScreen()
    }
    func createOnboardingDetailDatasource() {
        var types = [OnboardingCategoriesV2]()
        
        let lookingFor = OnboardingCategoriesV2(page_name: "Add your Interests", page_type: .LookingFor)
        let skills = OnboardingCategoriesV2(page_name: "How tall do you stand", page_type: .Skills)
        let compensation = OnboardingCategoriesV2(page_name: "How much do you weigh", page_type: .Compensation)
        let profilePic = OnboardingCategoriesV2(page_name: "What type of Drinker are you?", page_type: .ProfilePic)
        types.append(contentsOf: [lookingFor, skills, compensation, profilePic])
        
        types.append(OnboardingCategoriesV2(page_name: "None", page_type: .None))
        
        OnboardingProcess.shared.types2 = types
        OnboardingProcess.shared.currentPage2 = types[0].page_type
    }
}
