//
//  OnboardingLookingForViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 06/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class OnboardingLookingForViewController: UIViewController {

    @IBOutlet weak var friendsCV: UICollectionView!
    @IBOutlet weak var professionalCV: UICollectionView!
    @IBOutlet weak var otherCV: UICollectionView!
    
    var screenType = 1   // 1: Sign up flow    2: Edit flow
    
    var selectedFriend = 0 {
        didSet {
            friendsCV.reloadData()
        }
    }
    var selectedProfessional = [Int]() {
        didSet {
            professionalCV.reloadData()
        }
    }
    var selectedOther = 0 {
        didSet {
            otherCV.reloadData()
        }
    }

    var selectedIds = [Int]()
    var friendIds = [[String: Any]]()
    var professionalIds = [[String: Any]]()
    let viewModel = OnboardingLookingForViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createDatasources()
        configureView()
    }
    func configureView() {
        let array = [friendsCV, professionalCV, otherCV]
        array.forEach { (cv) in
            cv?.dataSource = self
            cv?.delegate = self
            cv?.register(UINib(nibName: CollectionViewCells.FilterTagCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CollectionViewCells.FilterTagCell.rawValue)
        }
        
        friendsCV.dataSource = self
        friendsCV.delegate = self
        
        professionalCV.dataSource = self
        professionalCV.delegate = self
        
        otherCV.dataSource = self
        otherCV.delegate = self
    }
    func createDatasources() {
        // Creating datasources
        friendIds = [["id": 8, "value": "Male"], ["id": 9, "value": "Female"], ["id": 10, "value": "Both"]]
        professionalIds = [["id": 2, "value": "Employment"], ["id": 3, "value": "Professional Work"], ["id": 4, "value": "Freelancing Work"], ["id": 5, "value": "Providing A Service"]]
        
        if screenType == 2 {
            if selectedIds.contains(11) {
                selectedOther = 11
            }
            [8,9,10].forEach { (i) in
                if selectedIds.contains(i) {
                    selectedFriend = i
                }
            }
            [2,3,4,5].forEach { (i) in
                if selectedIds.contains(i) {
                    selectedProfessional.append(i)
                }
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func saveTapped(_ sender: Any) {
        
        if selectedOther == 0 && selectedFriend == 0 && selectedProfessional.count == 0 {
            alert(message: "Please select at least one option!")
        } else {
            if screenType == 1 {
                createOnboardingDetailDatasource()
                handleSubmit()
            } else {
                var ids = [Int]()
                if selectedOther == 11 {
                    ids.append(11)
                }
                if selectedFriend != 0 {
                    ids.append(selectedFriend)
                }
                if selectedProfessional.count > 0 {
                    ids.append(contentsOf: selectedProfessional)
                }
                let params = ["purpose": ids]
                saveLookingFor(params: params)
            }
        }
        
    }
    
    // MARK: - Methods
    func openPersonalDetailsScreen() {
        let vc = OnboardingPerosnalViewController.instantiate(fromAppStoryboard: .Onboarding)
        navigationController?.pushViewController(vc, animated: true)
    }
    func handleSubmit() {
        let userDetails = UserDetails.shared
        var workProfileIds = [Int]()
        if selectedFriend != 0 {
            workProfileIds.append(selectedFriend)
        }
        if selectedProfessional.count != 0 {
            workProfileIds.append(contentsOf: selectedProfessional)
        }
        if selectedOther != 0 {
            workProfileIds.removeAll()
            workProfileIds.append(selectedOther)
        }
        
        if userDetails.selectedWorkSpace.contains(1) && workProfileIds.count == 0 {
            alert(message: Constants.kAlertSelectFriendType.rawValue)
        } else if workProfileIds.count == 0 {
            alert(message: Constants.kAlertSelectLookingFor.rawValue)
        } else {
//            let params = ["name": "name", "age": 17, "gender": "Male", "purpose": workProfileIds, "email": "fcgfc@bjh.mgm"] as [String : Any]
            let params = ["name": userDetails.name, "age": userDetails.age, "gender": userDetails.gender, "purpose": workProfileIds, "email": userDetails.email] as [String : Any] //, "fcm_reg_id": "Abc Xyz", "purpose": purposeIds
            print("params = \(params)")
            createBasicProfile(params: params)
        }
    }
    func createOnboardingDetailDatasource() {
        var types = [OnboardingCategories]()
        if selectedFriend != 0 {
            let interests = OnboardingCategories(page_name: "Add your Interests", page_type: .Interests)
            let height = OnboardingCategories(page_name: "How tall do you stand", page_type: .Height)
            let weight = OnboardingCategories(page_name: "How much do you weigh", page_type: .Weight)
            let drinking = OnboardingCategories(page_name: "What type of Drinker are you?", page_type: .Drinking)
            let smoking = OnboardingCategories(page_name: "What type of Smoker are you?", page_type: .Smoking)
            types.append(contentsOf: [interests, height, weight, drinking, smoking])
        }
        if selectedProfessional.count != 0 {
            let skills = OnboardingCategories(page_name: "Add your Skills", page_type: .Skills)
//            let categories = OnboardingCategories(page_name: "Categories", page_type: .Categories)
            types.append(contentsOf: [skills])
        }
        types.append(OnboardingCategories(page_name: "None", page_type: .None))
        OnboardingProcess.shared.types = types
        OnboardingProcess.shared.currentPage = types[0].page_type
    }
    func dismissToProfile() {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Web services
    func createBasicProfile(params: [String : Any]) {
        self.showHud()
        viewModel.createBasicProfile(parameters: params) { (success, json, message) in
            self.hideHud()
            if success {
                UserDefaults.standard.isFormComplete = true
                OnboardingProcess.shared.next(vc: self)
//                self.openPersonalDetailsScreen()
            } else {
                self.alert(message: message)
            }
        }
    }
    func saveLookingFor(params: [String : Any]) {
        self.showHud()
        viewModel.saveLookingFor(parameters: params) { (success, json, message) in
            self.hideHud()
            if success {
                self.dismissToProfile()
            } else {
                self.alert(message: message)
            }
        }
    }
}
// MARK: - CollectionView methods
extension OnboardingLookingForViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        print(ConfigDataModel.shared.habitFrequency)
        switch collectionView {
        case friendsCV:
            return friendIds.count
        case professionalCV:
            return professionalIds.count
        default:
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCells.FilterTagCell.rawValue, for: indexPath) as! FilterTagCell
        cell.label.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
        switch collectionView {
        case friendsCV:
            cell.label.text = friendIds[indexPath.row]["value"] as? String
            cell.isCellSelected = friendIds[indexPath.row]["id"] as? Int == selectedFriend
        case professionalCV:
            cell.label.text = professionalIds[indexPath.row]["value"] as? String
            cell.isCellSelected = selectedProfessional.contains(professionalIds[indexPath.row]["id"] as? Int ?? 0)
        default:
            cell.label.text = "Other"
            cell.isCellSelected = selectedOther == 11
        }
        cell.layoutSubviews()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (screenWidth-50)/2
        return CGSize(width: width, height: 44)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case friendsCV:
            selectedOther = 0
            selectedFriend = friendIds[indexPath.row]["id"] as? Int ?? 0
        case professionalCV:
            selectedOther = 0
            let id = professionalIds[indexPath.row]["id"] as? Int ?? 0
            if selectedProfessional.contains(id) {
                selectedProfessional = selectedProfessional.filter({ $0 != id })
            } else {
                selectedProfessional.append(professionalIds[indexPath.row]["id"] as? Int ?? 0)
            }
        default:
            selectedFriend = 0
            selectedProfessional = []
            selectedOther = 11
        }
    }
}
