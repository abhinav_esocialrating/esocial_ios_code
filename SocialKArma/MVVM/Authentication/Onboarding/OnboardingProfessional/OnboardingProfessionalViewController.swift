//
//  OnboardingProfessionalViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 06/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class OnboardingProfessionalViewController: UIViewController {
    
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var headingLabel: UILabel!

    @IBOutlet weak var searchTF: ACFloatingTextfield!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var tagListViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchWrapView: UIView!
    
    var screenType = 1   // 1: Skill    2: Category
    let viewModel = OnboardingProfessionalViewModel()
//    var onboardingType: OnboardingCategoryTypes = .Skills
    var onboardingType: OnboardingCategories!
    let dropDown = DropDown()

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        
        viewModel.getPopularConfigData(parameters: ["limit": 10, "offset": 1], configType: screenType == 1 ? "skill" : "category") { (success,_,message) in
            
            self.updateTagView(tag: "", selectedDict: [:], tagArray: self.viewModel.selectedValues, textField: UITextField())
        }
    }
    func configureView() {
        searchTF.lineColor = UIColor.clear
        searchTF.selectedLineColor = UIColor.clear
        searchTF.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        searchTF.textColor = UIColor.NewTheme.darkGray
        searchTF.disableFloatingLabel = true
        searchTF.delegate = self
        searchTF.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
        
        searchWrapView.setUpBorder(width: 1, color: .lightGray, radius: searchWrapView.frame.height/2)

//        headingLabel.text = onboardingType.page_name
//        skipBtn.setUpBorder(width: 1, color: .darkGray, radius: 8)
        nextBtn.setUpBorder(width: 1, color: .darkGray, radius: nextBtn.frame.height/2)
        
        tagListView.delegate = self
        tagListView.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
    }

    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func skipTapped(_ sender: Any) {
        openWelcomeScreen()
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        var skillObj = [[String: Any]]()
        viewModel.selectedValues.forEach { (dict) in
            if let issel = dict["isSelected"] as? Bool, issel {
                skillObj.append(["skill_level_id": 1, "skill_id": dict["id"] as! Int])
            }
        }
        AppDebug.shared.consolePrint(skillObj)
        OnboardingProcess.shared.basicDetailRequestDict["skill"] = skillObj
        OnboardingProcess.shared.nextV2(vc: self)
    }
    // MARK: - Methods
    @objc func textFieldChanged(_ textField: UITextField) {
        viewModel.isSelectedFromDropDown = false
        let params = ["config": "skill", "search": textField.text!]
        viewModel.getSearchConfig(parameters: params) { (success, json, message) in
            if success {
                DispatchQueue.main.async {
                    if self.viewModel.searchedData.count > 0 {
                        if self.viewModel.isFirstTime {
                            self.viewModel.isFirstTime = false
                            self.updateTagView(tag: "", selectedDict: [:], tagArray: self.viewModel.selectedValues, textField: UITextField())
                        } else {
                            self.showDropDown(textField: textField)
                        }
                    } else {
                        self.dropDown.hide()
                    }
                }
            }
        }
    }
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        
        dropDown.dataSource = viewModel.dropDownData
        
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropDownSelection(index: index, item: item, textField: textField)
        }
        dropDown.show()
    }
    func handleDropDownSelection(index: Int, item: String, textField: UITextField) {

        textField.text = ""
        self.viewModel.isSelectedFromDropDown = true

        if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
            let allIds = viewModel.selectedValues.map { (dict) -> Int in
                let isSelected = dict["isSelected"] as! Bool
                if isSelected {
                    return dict["id"] as! Int
                } else {
                    return 0
                }
            }
            let selectedIds = allIds.filter({ $0 != 0 })
            if !selectedIds.contains(skillId) {
                var dict = self.viewModel.searchedData[index]
                dict["isSelected"] = true
                self.viewModel.selectedValues.append(dict)
                self.updateTagView(tag: item, selectedDict: dict, tagArray: [], textField: textField)
            }
        }
    }
    func updateTagView(tag: String, selectedDict: [String: Any], tagArray: [[String: Any]], textField: UITextField) {
        
        tagListView.removeAllTags()
        viewModel.selectedValues.forEach { (dict) in
            tagListView.addTag(dict["value"] as! String)
        }
        tagListViewHeight.constant = tagListView.intrinsicContentSize.height
        checkForSelection()
    }
    
    func checkForSelection() {
        var i = 0
        _ = tagListView.tagViews.map { (tagView) -> Bool in
            
            tagView.isSelected = viewModel.selectedValues[i]["isSelected"] as! Bool
//            if viewModel.selectedValues[i]["isSelected"] as! Bool == true {
//                tagView.isSelected = true
//            } else {
//                tagView.isSelected = false
//            }
            i += 1
            return true
        }
    }
}
// MARK: - TextFieldDelegate
extension OnboardingProfessionalViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.text = "a"
        textFieldChanged(textField)
        textField.text = ""
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if !viewModel.isSelectedFromDropDown && searchTF.text != "" {
            let params = ["master": screenType == 1 ? "skill" : "interest", "value": textField.text!]
            viewModel.addToMasterTable(parameters: params) { (success, dict, message) in
                if success {
                    self.searchTF.text = ""

                    if let skillId = dict["id"] as? Int {
                        let allIds = self.viewModel.selectedValues.map { (dict) -> Int in
                            let isSelected = dict["isSelected"] as! Bool
                            if isSelected {
                                return dict["id"] as! Int
                            } else {
                                return 0
                            }
                        }
                        let selectedIds = allIds.filter({ $0 != 0 })
                        if !selectedIds.contains(skillId) {
                            var dictNew = dict
                            dictNew["isSelected"] = true
                            self.viewModel.selectedValues.append(dictNew)
                            self.updateTagView(tag: dictNew["value"] as! String, selectedDict: dict, tagArray: [], textField: textField)
                        }
                    }
                    
                }
            }
        }
    }
}
// MARK: - TagListView delegates
extension OnboardingProfessionalViewController: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        // Finding Pressed tag
        var index = 1000
        for (i,j) in viewModel.selectedValues.enumerated() {
            if j["value"] as! String == title {
                index = i
            }
        }
        if index != 1000 {
            viewModel.selectedValues[index]["isSelected"] = !(viewModel.selectedValues[index]["isSelected"] as! Bool)
        }
        checkForSelection()
    }
}
