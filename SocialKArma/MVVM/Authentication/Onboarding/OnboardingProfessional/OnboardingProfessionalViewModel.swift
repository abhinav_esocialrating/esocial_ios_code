//
//  OnboardingProfessionalViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 06/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import SwiftyJSON

class OnboardingProfessionalViewModel: NSObject {
    
    var searchedData = [[String:Any]]()
    var dropDownData = [String]()
    var selectedValues = [[String:Any]]()
    var isFirstTime = false
    var selectedIds = [Int]()
    var isSelectedFromDropDown = false

    // MARK: - Search suggestions
    func getSearchConfig(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseSearchedData(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func getPopularConfigData(parameters: [String:Any], configType: String, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getPopularConfigData(parameters: parameters, configType: configType, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseFirstTime(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func addToMasterTable(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ data: [String:Any], _ message: String) -> Void)) {
       NetworkManager.addToMasterTable(parameters: parameters, method: .post) { (response) in
           let json = JSON(response.result.value ?? JSON.init())
           print(json)
           switch response.response?.statusCode {
           case 200:
               let data = json["data"].dictionaryObject
               completion(true, data ?? [:], "")
           default:
               let message = json["message"].stringValue
               completion(false, [:], message)
           }
       }
    }
    func parseFirstTime(data: [JSON]) {
        
        let search = data.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["id"] = json["id"].intValue
            dict["value"] = json["value"].string
            return dict
        }
        
        selectedValues = search
        selectedValues = selectedValues.map { (dict) -> [String:Any] in
            var newDict = dict
            newDict["isSelected"] = false
            return newDict
        }
    }
    func parseSearchedData(data: [JSON]) {
        let search = data.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["id"] = json["id"].intValue
            dict["value"] = json["value"].string
            return dict
        }
        searchedData = search
        let drop = search.map { (dict) -> String in
            return (dict["value"] as? String) ?? ""
        }
        dropDownData = drop
        
//        if isFirstTime {
//            selectedValues = Array(searchedData.prefix(10))
//            selectedValues = selectedValues.map { (dict) -> [String:Any] in
//                var newDict = dict
//                newDict["isSelected"] = false
//                return newDict
//            }
//        }
    }
}
