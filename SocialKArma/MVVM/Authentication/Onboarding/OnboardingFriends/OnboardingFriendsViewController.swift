//
//  OnboardingFriendsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 06/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class OnboardingFriendsViewController: UIViewController {
    
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headingLabel: UILabel!

    var screenType = 1   // 1: Gender   2: Drinking   3: Smoking
    var selectedValue = 0
    var onboardingType: OnboardingCategories!
    let viewModel = OnboardingFriendsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
    func configureView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: CollectionViewCells.FilterTagCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CollectionViewCells.FilterTagCell.rawValue)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        skipBtn.setUpBorder(width: 1, color: .darkGray, radius: 8)
        nextBtn.setUpBorder(width: 1, color: .darkGray, radius: 8)
        headingLabel.text = onboardingType.page_name

    }

    // MARK: - Actions
    
    @IBAction func skipTapped(_ sender: Any) {
        openWelcomeScreen()
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        if selectedValue != 0 {
            saveData()
        } else {
            alert(message: "Please select a value before proceeding")
        }
    }
    
    // MARK: - Methods
    private func saveData() {
        let key = onboardingType.page_type == .Height ? "drinking_id" : "smoking_id"
        let params = [key: selectedValue]
        self.showHud()
        viewModel.saveAboutMeInfo(parameters: params) { (success, json, message) in
            self.hideHud()
            if success {
                OnboardingProcess.shared.next(vc: self)
            } else {
                self.alert(message: message)
            }
        }
    }
//    private func openProfessionalVC() {
//        let vc = OnboardingProfessionalViewController.instantiate(fromAppStoryboard: .Onboarding)
//        vc.screenType = 1
//        navigationController?.pushViewController(vc, animated: true)
//    }
//    private func openFriendsLevel2() {
//        let vc = OnboardingFriendsViewController.instantiate(fromAppStoryboard: .Onboarding)
//        vc.screenType = 2
//        navigationController?.pushViewController(vc, animated: true)
//    }
}
// MARK: - CollectionView methods
extension OnboardingFriendsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return ConfigDataModel.shared.habitFrequency.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCells.FilterTagCell.rawValue, for: indexPath) as! FilterTagCell
        switch onboardingType.page_type {
        case .Drinking:
            cell.label.text = ConfigDataModel.shared.habitFrequency[indexPath.row]["value"]?.string ?? ""
            cell.isCellSelected = OnboardingModel.shared.drinking == ConfigDataModel.shared.habitFrequency[indexPath.row]["id"]?.intValue ?? 0
        default:
            cell.label.text = ConfigDataModel.shared.habitFrequency[indexPath.row]["value"]?.string ?? ""
            cell.isCellSelected = OnboardingModel.shared.smoking == ConfigDataModel.shared.habitFrequency[indexPath.row]["id"]?.intValue ?? 0
        }
        cell.layoutSubviews()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (screenWidth-50)/2
        return CGSize(width: width, height: 44)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch onboardingType.page_type {
        case .Drinking:
            OnboardingModel.shared.drinking = ConfigDataModel.shared.habitFrequency[indexPath.row]["id"]?.intValue ?? 0
            selectedValue = ConfigDataModel.shared.habitFrequency[indexPath.row]["id"]?.intValue ?? 0
        default:
            OnboardingModel.shared.smoking = ConfigDataModel.shared.habitFrequency[indexPath.row]["id"]?.intValue ?? 0
            selectedValue = ConfigDataModel.shared.habitFrequency[indexPath.row]["id"]?.intValue ?? 0
        }
        collectionView.reloadData()
    }
}
