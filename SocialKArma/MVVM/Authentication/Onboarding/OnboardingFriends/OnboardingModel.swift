//
//  OnboardingModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 06/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class OnboardingModel: NSObject {
    struct Static {
        fileprivate static var instance: OnboardingModel?
    }
    class var shared: OnboardingModel {
        if Static.instance == nil {
            Static.instance = OnboardingModel()
        }
        return Static.instance!
    }
    func dispose() {
        OnboardingModel.Static.instance = nil
        print("Disposed Singleton instance")
    }
    
    // MARK: - Variables
    var paramDictionary = [String:Any]()

    var gender = "" {
        didSet {
            paramDictionary["gender"] = gender
        }
    }
    var smoking = 0 {
        didSet {
            paramDictionary["smoking"] = smoking
        }
    }
    var drinking = 0 {
        didSet {
            paramDictionary["drinking"] = drinking
        }
    }
    // MARK: - Methods
    func resetValues() {
        
        gender = ""
        smoking = 0
        drinking = 0
        
        // Dictionary
        paramDictionary = [:]
    }
}
