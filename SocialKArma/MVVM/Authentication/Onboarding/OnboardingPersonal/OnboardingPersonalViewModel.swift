//
//  OnboardingPersonalViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 07/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class OnboardingPersonalViewModel: NSObject {
    func saveAboutMeInfo(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.saveAboutMeInfo(parameters: parameters, method: .put) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json, "")
                ProfileV2Data.sharedInstance.logAnalyticsEventProfileUpdate()
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
