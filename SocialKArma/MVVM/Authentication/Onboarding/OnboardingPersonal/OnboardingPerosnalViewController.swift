//
//  OnboardingPerosnalViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 07/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import RKMultiUnitRuler

class OnboardingPerosnalViewController: UIViewController {
    
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var ruler: RKMultiUnitRuler?

    var screenType = 1   // 1: Height  2: Weight
    var selectedValue = 0
    let viewModel = OnboardingPersonalViewModel()
    var onboardingType: OnboardingCategories!

    var segments = Array<RKSegmentUnit>()
    var rangeStart = Measurement(value: 30.0, unit:  UnitMass.kilograms)
    var rangeLength = Measurement(value: Double(90), unit: UnitMass.kilograms)
    
    var rangeStartType1 = Measurement(value: 30.0, unit:  UnitLength.centimeters)
    var rangeLengthType1 = Measurement(value: Double(90), unit: UnitLength.centimeters)

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
    private func configureView() {
        selectedValue = onboardingType.page_type == .Height ? 160 : 60
        headingLabel.text = onboardingType.page_name
        
        skipBtn.setUpBorder(width: 1, color: .darkGray, radius: 8)
        nextBtn.setUpBorder(width: 1, color: .darkGray, radius: 8)

        // Ruler
        rangeStart = Measurement(value: 30.0, unit: UnitMass.kilograms)
        rangeLength = Measurement(value: Double(200), unit: UnitMass.kilograms)
        
        rangeStartType1 = Measurement(value: 130.0, unit: UnitLength.centimeters)
        rangeLengthType1 = Measurement(value: Double(140), unit: UnitLength.centimeters)
        
        ruler?.direction = .horizontal
        ruler?.tintColor = UIColor(red: 0.15, green: 0.18, blue: 0.48, alpha: 1.0)
        segments = self.createSegments()
        ruler?.delegate = self
        ruler?.dataSource = self
        
    }

    // MARK: - Actions
    @IBAction func skipTapped(_ sender: Any) {
        openWelcomeScreen()
    }
    @IBAction func nextTapped(_ sender: Any) {
        if selectedValue != 0 {
            saveData()
        } else {
            alert(message: "Please select a value before proceeding")
        }
    }
    
    // MARK: - Methods
    private func saveData() {
        let key = onboardingType.page_type == .Height ? "height" : "weight"
        let params = [key: selectedValue]
        self.showHud()
        viewModel.saveAboutMeInfo(parameters: params) { (success, json, message) in
            self.hideHud()
            if success {
                OnboardingProcess.shared.nextV2(vc: self)
            } else {
                self.alert(message: message)
            }
        }
    }
//    private func openFriendsVC() {
//        let vc = OnboardingFriendsViewController.instantiate(fromAppStoryboard: .Onboarding)
//        vc.screenType = 1
//        navigationController?.pushViewController(vc, animated: true)
//    }
//    private func openPersonalDetailsVC() {
//        let vc = OnboardingPerosnalViewController.instantiate(fromAppStoryboard: .Onboarding)
//        vc.screenType = 2
//        navigationController?.pushViewController(vc, animated: true)
//    }
    private func createSegments() -> Array<RKSegmentUnit> {
        let formatter = MeasurementFormatter()
        formatter.unitStyle = .medium
        formatter.unitOptions = .providedUnit
        if onboardingType.page_type == .Height {
            let kgSegment = RKSegmentUnit(name: "Centimeter", unit: UnitMass.kilograms, formatter: formatter)

            kgSegment.name = "Centimeter"
            kgSegment.unit = UnitLength.centimeters
            let kgMarkerTypeMax = RKRangeMarkerType(color: UIColor.darkGray, size: CGSize(width: 1.0, height: 50.0), scale: 5.0)
            kgMarkerTypeMax.labelVisible = true
            kgSegment.markerTypes = [
                RKRangeMarkerType(color: UIColor.darkGray, size: CGSize(width: 1.0, height: 35.0), scale: 0.5),
                RKRangeMarkerType(color: UIColor.darkGray, size: CGSize(width: 1.0, height: 50.0), scale: 1.0)]
            kgSegment.markerTypes.last?.labelVisible = true
            return [kgSegment]
        } else {
            let kgSegment = RKSegmentUnit(name: "Kilograms", unit: UnitMass.kilograms, formatter: formatter)

            kgSegment.name = "Kilogram"
            kgSegment.unit = UnitMass.kilograms
            let kgMarkerTypeMax = RKRangeMarkerType(color: UIColor.darkGray, size: CGSize(width: 1.0, height: 50.0), scale: 5.0)
            kgMarkerTypeMax.labelVisible = true
            kgSegment.markerTypes = [
                RKRangeMarkerType(color: UIColor.darkGray, size: CGSize(width: 1.0, height: 35.0), scale: 0.5),
                RKRangeMarkerType(color: UIColor.darkGray, size: CGSize(width: 1.0, height: 50.0), scale: 1.0)]
            kgSegment.markerTypes.last?.labelVisible = true
            return [kgSegment]
        }
        
    }
}
extension OnboardingPerosnalViewController: RKMultiUnitRulerDataSource, RKMultiUnitRulerDelegate {
    func unitForSegmentAtIndex(index: Int) -> RKSegmentUnit {
        return segments[index]
    }
    
    func rangeForUnit(_ unit: Dimension) -> RKRange<Float> {
        if let unit = unit as? UnitMass {
            let locationConverted = rangeStart.converted(to: unit)
            let lengthConverted = rangeLength.converted(to: unit )
            return RKRange<Float>(location: ceilf(Float(locationConverted.value)),
                                  length: ceilf(Float(lengthConverted.value)))
        } else if let unit = unit as? UnitLength {
            let locationConverted = rangeStartType1.converted(to: unit)
            let lengthConverted = rangeLengthType1.converted(to: unit)
            return RKRange<Float>(location: ceilf(Float(locationConverted.value)),
                                  length: ceilf(Float(lengthConverted.value)))
        }
        let locationConverted = rangeStart.converted(to: unit as! UnitMass)
        let lengthConverted = rangeLength.converted(to: unit as! UnitMass)
        return RKRange<Float>(location: ceilf(Float(locationConverted.value)),
                              length: ceilf(Float(lengthConverted.value)))
    }
    
    var numberOfSegments: Int {
        get {
            return 1
        }
        set(newValue) {
            
        }
    }
    
    func styleForUnit(_ unit: Dimension) -> RKSegmentUnitControlStyle {
        let style: RKSegmentUnitControlStyle = RKSegmentUnitControlStyle()
        style.scrollViewBackgroundColor = UIColor.white
        let range = self.rangeForUnit(unit)
        if unit == UnitMass.pounds {

            style.textFieldBackgroundColor = UIColor.clear
            // color override location:location+40% red , location+60%:location.100% green
        } else {
            style.textFieldBackgroundColor = UIColor.red
        }
//        if (colorOverridesEnabled) {
            style.colorOverrides = [
                RKRange<Float>(location: range.location, length: 0.1 * (range.length)): UIColor.red,
                RKRange<Float>(location: range.location + 0.4 * (range.length), length: 0.2 * (range.length)): UIColor.green]
//        }
        style.textFieldBackgroundColor = UIColor.clear
        style.textFieldTextColor = UIColor.darkGray
        
        return style
    }
    
    func valueChanged(measurement: NSMeasurement) {
        print("value changed to \(measurement.doubleValue)")
        selectedValue = Int(measurement.doubleValue)
    }
    

}
