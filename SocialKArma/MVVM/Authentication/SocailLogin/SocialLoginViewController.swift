//
//  SocialLoginViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 28/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit

class SocialLoginViewController: UIViewController {
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
    }
    

    // MARK: - Navigation

    @IBAction func googleTapped(_ sender: Any) {
        typeLabel.text = "google"
        GIDSignIn.sharedInstance()?.signIn()
    }
    @IBAction func fbTapped(_ sender: Any) {
        typeLabel.text = "fb"
        
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["email", "public_profile"], from: self) { (result, error) in
            if error != nil {
                print(error as Any)
            } else if result?.isCancelled ?? false {
                print("Cancelled")
            } else {
                print("Success")
//                result?.token.
            }
        }
    }
}
extension SocialLoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
          if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
            print("The user has not signed in before or they have since signed out.")
          } else {
            print("\(error.localizedDescription)")
          }
          return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        
        DispatchQueue.main.async {
            self.nameLabel.text = user.profile.name
            self.emailLabel.text = user.profile.email
        }
    }
    
    
}
