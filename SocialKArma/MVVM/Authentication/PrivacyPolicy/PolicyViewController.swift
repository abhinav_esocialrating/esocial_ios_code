//
//  PolicyViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 15/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import WebKit

class PolicyViewController: UIViewController {

    var type = 1   //1: Terms  2: Policy   3: FAQs  4: Contest
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        webView.navigationDelegate = self
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        showHud()
        var urlStr = ""
        switch type {
        case 1:
            urlStr = AppUrl.DemoTermsAndConditions.rawValue
        case 2:
            urlStr = AppUrl.DemoPrivacyPolicy.rawValue
        case 3:
            urlStr = AppUrl.DemoFAQs.rawValue
        default:
            urlStr = AppUrl.Contest.rawValue
        }
        
        let request = URLRequest(url: URL(string: urlStr)!)
        webView.load(request)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
extension PolicyViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        hideHud()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
