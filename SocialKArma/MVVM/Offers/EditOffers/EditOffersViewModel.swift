//
//  EditOffersViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class EditOffersViewModel: NSObject {
    
    var searchedData = [[String:Any]]()
    var dropDownData = [String]()
    var selectedSkillsDict = [[String:Any]]()
    var cityNames = [String]()
    var selectedJobType = 0
    var selectedOfferType = 1

    // MARK: - Web Services
    func saveOffer(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
//        var method = HTTPMethod.delete
//        switch addEditOrDelete {
//        case 1:
//            method = .post
//        case 2:
//            method = .put
//        default:
//            method = .delete
//        }
        NetworkManager.saveOffer(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200, 202:
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
        
    }
    // MARK: - Search suggestions
    func getSearchConfig(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseSearchedData(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    // MARK: - Utility methods
    func parseSearchedData(data: [JSON]) {
        let search = data.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["id"] = json["id"].intValue
            dict["value"] = json["value"].string
            return dict
        }
        searchedData = search
        let drop = search.map { (dict) -> String in
            return (dict["value"] as? String) ?? ""
        }
        dropDownData = drop
    }
}
