//
//  EditOffersViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class EditOffersViewController: UIViewController {
    
    @IBOutlet weak var textfield1: ACFloatingTextfield!
    @IBOutlet weak var textfield2: ACFloatingTextfield!
    @IBOutlet weak var textfield3: ACFloatingTextfield!
    @IBOutlet weak var textfield4: ACFloatingTextfield!
    @IBOutlet weak var textfield5: ACFloatingTextfield!

    @IBOutlet weak var skillTagView: TagListView!
    @IBOutlet weak var skillViewHeight: NSLayoutConstraint!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var scoreTF: UITextField!
    
    @IBOutlet weak var linkTF: ACFloatingTextfield!
    @IBOutlet weak var jobroleContainer: UIView!
    @IBOutlet weak var skillContainer: UIView!
    @IBOutlet weak var remoteSwitch: UISwitch!
    @IBOutlet weak var remoteJobView: UIView!
    // MARK: - Properties
    let viewModel = EditOffersViewModel()
    let dropDown = DropDown()

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
    
    func configureView() {
        var array1 = [textfield1, textfield2, textfield3, textfield4, textfield5, linkTF]
        for (_,textField) in array1.enumerated() {
            textField?.lineColor = UIColor.NewTheme.darkGray
            textField?.selectedLineColor = UIColor.NewTheme.paleBlue
            textField?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
            textField?.textColor = UIColor.NewTheme.darkGray
            textField?.disableFloatingLabel = true
            textField?.delegate = self
        }
        
        array1 = [textfield2, textfield4, textfield5]
        array1.forEach({ $0?.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged) })
        
        scoreTF.setUpBorder(width: 1, color: .darkGray, radius: 4)
        
        descriptionTextView.setUpBorder(width: 1, color: .darkGray, radius: 12)
        
        skillTagView.delegate = self
    }

    // MARK: - Actions
    
    @IBAction func saveTapped(_ sender: Any) {
        if isValid() {
            saveOffer()
        }
    }
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Methods
    func isValid() -> Bool {
        var valid = true
        var message = ""
        if textfield1.text!.isEmpty {
            valid = false
            message = "Please enter Offer Type"
            textfield1.showErrorWithText(errorText: message)
        } else if textfield2.text!.isEmpty && viewModel.selectedOfferType == 1 {
            valid = false
            message = "Please enter Job Role"
            textfield2.showErrorWithText(errorText: message)
        } else if textfield3.text!.isEmpty {
            valid = false
            message = "Please enter Title"
            textfield3.showErrorWithText(errorText: message)
        } else if descriptionTextView.text!.isEmpty {
            valid = false
            message = "Please enter Description"
            alert(message: message)
        } else if viewModel.selectedSkillsDict.count == 0 && viewModel.selectedOfferType == 1 {
            valid = false
            message = "Please enter Skills"
            textfield5.showErrorWithText(errorText: message)
//            startDateTF.showErrorWithText(errorText: message)
        } else if textfield5.text!.isEmpty {
            valid = false
            message = "Please enter Location"
            textfield5.showErrorWithText(errorText: message)
        } else if scoreTF.text!.isEmpty {
            valid = false
            message = "Please enter Minimum Score"
            alert(message: message)
        } else {
            textfield1.errorTextColor = .clear
            textfield2.errorTextColor = .clear
            textfield3.errorTextColor = .clear
            textfield4.errorTextColor = .clear
            textfield5.errorTextColor = .clear
        }
        return valid
    }
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        switch textField {
        case textfield1:
            dropDown.dataSource = ["Job Offer", "Sales Offer", "Other Offer"]
        case textfield2, textfield4, textfield5:
            dropDown.dataSource = viewModel.dropDownData
        default:
            dropDown.dataSource = []
        }
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropDownSelection(index: index, item: item, textField: textField)
        }
        dropDown.show()
    }
    func handleDropDownSelection(index: Int, item: String, textField: UITextField) {
        switch textField {
        case textfield1: // Job Offer
            textField.text = item
            viewModel.selectedOfferType = index + 1
            hideShowMendatoryViews(hide: index != 0)
        case textfield2:
            textField.text = item
        case textfield4:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.skills.contains(skillId) {
                    self.viewModel.selectedSkillsDict.append( self.viewModel.searchedData[index])
                    self.updateSkillsTagView(tag: item, textField: textField)
                    FilterModel.shared.skills.append(skillId)
                }
            }
        case textfield5:
            textField.text = item
        default:
            textField.text = ""
        }
    }
    func updateSkillsTagView(tag: String, textField: UITextField) {
        switch textField {
        case textfield4:
            skillTagView.addTag(tag)
            skillViewHeight.constant = skillTagView.intrinsicContentSize.height
        default:
            print("")
        }
    }
    func hideShowMendatoryViews(hide: Bool) {
        skillContainer.isHidden = hide
        jobroleContainer.isHidden = hide
        remoteJobView.isHidden = hide
    }
    // MARK: - Web Services
    func saveOffer() {
        let params = [
            "offerTypeId": viewModel.selectedOfferType,
            "title": textfield3.text!,
            "description": descriptionTextView.text!,
            "minScore": scoreTF.text!,
            "locationCity": textfield5.text!,
            "skill": viewModel.selectedSkillsDict.map({ $0["id"] as! Int }),
            "jobType": [viewModel.selectedJobType],
            "url": linkTF.text!,
            "minAge": 15,
            "maxAge": 30,
            "is_remote": remoteSwitch.isOn ? 1 : 0
            ] as [String : Any]
        viewModel.saveOffer(parameters: params) { (success, _, message) in
            if success {
                self.alertWithAction(message: "Offer created!") {
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                self.alert(message: message)
            }
        }
    }

}
// MARK: - TextFieldDelegate
extension EditOffersViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textfield1 {
            showDropDown(textField: textField)
            return false
        }
        return true
    }
    @objc func textFieldChanged(_ textField: UITextField) {
        var params = ["config": "job_type", "search": textField.text!]
        switch textField {
        case textfield2:
                params = ["config": "job_type", "search": textField.text!]
        case textfield4:
            params = ["config": "skill", "search": textField.text!]
        case textfield5:
            if textField.text!.count > 2 {
                LocationManager.shared.getCities(query: textField.text!) { (cities, cityNames) in
                    self.viewModel.dropDownData = cities
                    self.viewModel.cityNames = cityNames
                    DispatchQueue.main.async {
                        self.showDropDown(textField: textField)
                    }
                }
            }
        default:
            print(textField.text!)
        }
        if textField == textfield2 || textField == textfield4 {
            viewModel.getSearchConfig(parameters: params) { (success, json, message) in
                if success {
                    DispatchQueue.main.async {
                        if self.viewModel.searchedData.count > 0 {
                            self.showDropDown(textField: textField)
                        } else {
                            self.dropDown.hide()
                        }
                    }
                }
            }
        }
    }
}
// MARK: - TagListView delegates
extension EditOffersViewController: TagListViewDelegate {
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        switch sender {
        case skillTagView:
            skillTagView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedSkillsDict, tagView: sender)
            skillViewHeight.constant = skillTagView.intrinsicContentSize.height
        default:
            debugPrint("")
        }
    }
    func checkForStringInTagViewData(tag: String, tagArray: inout [[String:Any]], tagView: TagListView) {
        for (j,i) in tagArray.enumerated() {
            if let value = i["value"] as? String {
                if value == tag {
                    tagArray.remove(at: j)
                    break
                }
            }
        }
    }
}

/*
{
    "offerTypeId":1,
    "title":"Job requirement",
    "description":"need a full stack developer along side dev ops knowledge",
    "minScore":3.5,
    "locationCity":"dehradun ",
    "skill":[10,11,12,13,14,15],
    "jobType":[1,2],
    "minAge":15,
    "maxAge":30
}
*/
