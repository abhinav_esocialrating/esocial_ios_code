//
//  OfferCommentViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 14/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class OfferCommentViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var textViewBottom: NSLayoutConstraint!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var sendIcon: UIButton!
    @IBOutlet weak var messageTV: KMPlaceholderTextView!

    var keyboardHeight: CGFloat = 0.0
    let viewModel = OfferCommentViewModel()

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        setUpKeyboardNotification()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        messageTV.layer.cornerRadius = 12
        sendIcon.layer.cornerRadius = sendIcon.frame.height/2
        
        messageTV.delegate = self
    }
    
    func updateUI(hideKeyboard: Bool) {
        tableView.reloadData()
        if hideKeyboard {
            self.view.endEditing(true)
            textViewHeight.constant = 35
        }
    }
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendTapped(_ sender: Any) {
        if messageTV.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            postOfferComments(comment: messageTV.text!)
        }
    }
    // MARK: - Methods
    private func handleOfferActions(index: Int, action: String) {
        switch action {
        case "comment":
            let userId = viewModel.comment?.user_id
            openOtherUserProfile(userId: userId ?? 0)
        case "reply":
            let userId = viewModel.replies?[index].user_id
            openOtherUserProfile(userId: userId ?? 0)
        default:
            AppDebug.shared.consolePrint("")
        }
    }
    // MARK: - Web Services
    func postOfferComments(comment: String) {
        let params = ["offerId": 0, "comment": comment, "userCommentId": viewModel.comment?.user_offer_comment_id ?? 0] as [String : Any]
        viewModel.offerComment(parameters: params, method: comment == "" ? .get : .post) { (success, _, message) in
            if success {
                self.messageTV.text = ""
                self.updateUI(hideKeyboard: true)
//                self.getOfferComments()
            } else {
                self.alert(message: message)
            }
        }
    }
    func getOfferComments() {
        let params = ["offerId": viewModel.offer?.offerId ?? 0]
         viewModel.offerComment(parameters: params, method: .get) { (success, _, _) in
            self.updateUI(hideKeyboard: false)
        }
    }
}
// MARK: - Keyboard Management
extension OfferCommentViewController {
    private func setUpKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShown(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHidden(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChanged(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    @objc func keyboardShown(_ notification: Notification) {
        var offset = CGFloat.init()
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardViewEndFrame = view.convert(keyboardSize, from: view.window)
            let keyboardHeights = keyboardViewEndFrame.height
            print(keyboardHeights)  // 260
            offset += keyboardHeights  // add keyboard height
        } else {
            offset += 260
        }
//        if self.isModalInPresentation {
            UIView.animate(withDuration: 1.0) {
                self.view.layoutIfNeeded()
                self.textViewBottom.constant = self.keyboardHeight
                self.view.layoutIfNeeded()
            }
//        }
    }
    @objc func keyboardHidden(_ notification: Notification) {
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
            self.textViewBottom.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    @objc func keyboardFrameChanged(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let window = UIApplication.shared.keyWindow
            let bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
            keyboardHeight = keyboardSize.height - bottomPadding - 12 // -16/12: for textview padding in its container view
        }
    }
}
// MARK: - TextView methods
extension OfferCommentViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text != "" {
            let height = textView.contentSize.height
            if height > 86 {
                textViewHeight.constant = 86
            } else {
                textViewHeight.constant = textView.contentSize.height
            }
        } else {
            textViewHeight.constant = 35
        }
    }
}
extension OfferCommentViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : viewModel.replies?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.OfferReplyCell.rawValue, for: indexPath) as! OfferReplyCell
            cell.type = 1
            cell.comment = viewModel.comment
            cell.actions = { (index, action) in
                self.handleOfferActions(index: index, action: action)
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.OfferReplyCell.rawValue, for: indexPath) as! OfferReplyCell
            cell.type = 2
            cell.comment = viewModel.replies?[indexPath.row]
            cell.actions = { (index, action) in
                self.handleOfferActions(index: index, action: action)
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 2 : CGFloat.leastNonzeroMagnitude
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            let footer = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 2))
            footer.backgroundColor = .lightGray
            return footer
        } else {
            return nil
        }
    }
}
