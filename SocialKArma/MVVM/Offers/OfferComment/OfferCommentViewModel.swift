//
//  OfferCommentViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 14/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class OfferCommentViewModel: NSObject {
    
    var offer: Offer?
    var comment: OfferComment?
    var replies: [OfferComment]?
    var commentId = 0

    func offerComment(parameters: [String:Any], method: HTTPMethod, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.offerComment(parameters: parameters, method: method) { (response) in
            do {
                let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                AppDebug.shared.consolePrint(jsonData)
                switch response.response?.statusCode {
                case 200:
                    if let data = jsonData["data"] as? NSDictionary {
                        let comments = OfferComment(dictionary: data)
//                        self.replies?.append(comments!)
                        self.replies?.insert(comments!, at: 0)
                    }
                    completion(true, JSON.init(), "")
                default:
                    let message = jsonData["message"] as? String
                    completion(false, JSON.init(), message ?? "Some Error Occured!")
                }
            } catch {
                completion(false, JSON.init(), "Some Error Occured!")
            }
            
        }
    }
}
