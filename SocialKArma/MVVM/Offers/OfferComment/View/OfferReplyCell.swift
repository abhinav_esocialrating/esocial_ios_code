//
//  OfferReplyCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class OfferReplyCell: UITableViewCell {
    
    @IBOutlet weak var imgLeadingConst: NSLayoutConstraint!
    @IBOutlet weak var userImg: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var comment: OfferComment? {
        didSet {
            setValues()
        }
    }
    var type = 1 {  // 1: Comment  2: Reply
        didSet {
            setupConstraints()
        }
    }
    var actions: ((_ index: Int, _ action: String) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    func configureView() {
        userImg.rounded()
        containerView.layer.cornerRadius = 8
    }
    func setupConstraints() {
        imgLeadingConst.constant = type == 1 ? 16 : 40
    }
    func setValues() {
        nameLabel.text = comment?.name ?? "null"
        commentLabel.text = comment?.comment
        if let urlStr = comment?.image_id, let url = URL(string: urlStr) {
            userImg.sd_setImage(with: url, for: .normal, completed: nil)
        } else {
            userImg.setImage(UIImage(named: "avatar-2"), for: .normal)
        }
    }
    // MARK: - Actions
    @IBAction func userImgTapped(_ sender: Any) {
        actions?(indexPath.row,  type == 1 ? "comment" : "reply")
    }

}
