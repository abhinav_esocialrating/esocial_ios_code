//
//  OfferDetailsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import Alamofire

class OfferDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var textViewBottom: NSLayoutConstraint!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var sendIcon: UIButton!
    @IBOutlet weak var messageTV: KMPlaceholderTextView!

    var keyboardHeight: CGFloat = 0.0
    let viewModel = OfferDetailViewModel()
    var isEditingComment = false
    var userCommentId = 0
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        setUpKeyboardNotification()
        
//        getOfferComments()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getOffers()
    }
    func configureView() {
        // TableView
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerFromXib(name: TableViewCells.OffersCell.rawValue)
        
        messageTV.layer.cornerRadius = 12
        sendIcon.layer.cornerRadius = sendIcon.frame.height/2
        
        messageTV.delegate = self
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    

    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendTapped(_ sender: Any) {
        if messageTV.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            postOfferComments(comment: messageTV.text!, delete: false)
        }
    }
    // MARK: - Web Services
    func getOffers() {
        let params = ["offerId": viewModel.offerId]
        viewModel.getOfferDetails(parameters: params) { (success, _, _) in
            self.getOfferComments()
            self.updateUI(hideKeyboard: false)
        }
    }
    func getOfferComments() {
       let params = ["offerId": viewModel.offerId]
        viewModel.offerComment(parameters: params, method: .get) { (success, _, _) in
           self.updateUI(hideKeyboard: false)
       }
    }
    func postOfferComments(comment: String, delete: Bool) {
        var params = ["offerId": viewModel.offerId, "comment": comment] as [String : Any]
        if isEditingComment || delete {
            params["userCommentId"] = userCommentId
            params["offerId"] = nil
        }
        var method: HTTPMethod = .get
        if isEditingComment {
            method = .put
        } else if comment == "" {
            method = .get
        } else {
            method = .post
        }
        if delete {
            method = .delete
        }
        viewModel.offerComment(parameters: params, method: method) { (success, _, message) in
            if success {
                self.isEditingComment = false
                self.messageTV.text = ""
                self.updateUI(hideKeyboard: true)
                self.getOfferComments()
            } else {
                self.alert(message: message)
            }
        }
    }
    // MARK: - Methods
    private func handleOfferActions(index: Int, action: String) {
        switch action {
        case "like":
            let method: HTTPMethod = (viewModel.offer?.hasLiked ?? false) ? .delete : .post
            OfferAPI.likeOffer(parameters: ["offerId": viewModel.offer?.offerId ?? 0], method: method) { (success, _, message) in
                
                if success {
                    self.viewModel.offer?.hasLiked = !(self.viewModel.offer?.hasLiked ?? false)
                    self.viewModel.offer?.likes = (self.viewModel.offer?.hasLiked ?? false) ? (self.viewModel.offer?.likes ?? 0)+1 : (self.viewModel.offer?.likes ?? 1)-1
                    self.updateUI(hideKeyboard: false)
                } else {
                    self.alert(message: message)
                }
            }
        case "likesCount":
            let vc = LikesViewController.instantiate(fromAppStoryboard: .Offers)
            vc.viewModel.offerId = viewModel.offer?.offerId ?? 0
            navigationController?.pushViewController(vc, animated: true)
        case "link":
            if let url = viewModel.offer?.url {
                UIApplication.shared.openAny(url: url)
            }
        case "reply":
            openCommentReplies(index: index)
        case "edit":
            editDeleteComment(index: index, delete: false)
        case "delete":
            editDeleteComment(index: index, delete: true)
//            print("")
        case "image":
            let userId = viewModel.offer?.userId
            openOtherUserProfile(userId: userId ?? 0)
        case "imageComment":
            let userId = viewModel.comments?[index].user_id
            openOtherUserProfile(userId: userId ?? 0)
        default:
            openChat(index: index)
        }
    }
    private func editDeleteComment(index: Int, delete: Bool) {
        userCommentId = viewModel.comments?[index].user_offer_comment_id ?? 0
        if delete {
            postOfferComments(comment: messageTV.text!, delete: true)
        } else {
            isEditingComment = true
            messageTV.becomeFirstResponder()
            messageTV.text = viewModel.comments?[index].comment
        }
    }
    private func openChat(index: Int) {
        let user_id = viewModel.offer?.userId ?? 0
        let name = viewModel.offer?.userInfo?.name ?? "NA"
        let image_id = viewModel.offer?.userInfo?.image_id ?? ""
        
        let vc = ChatViewController.instantiate(fromAppStoryboard: .Chat)
        SocketIOManager.shared.toUserId = "\(user_id)"
        let chatUser = ChatUser(dictionary: ["name":name, "image_id":image_id] as NSDictionary)
        vc.viewModel.chatUser = chatUser
        
        navigationController?.pushViewController(vc, animated: true)
    }
    private func openCommentReplies(index: Int) {
        let vc = OfferCommentViewController.instantiate(fromAppStoryboard: .Offers)
        vc.viewModel.offer = viewModel.offer
        vc.viewModel.commentId = viewModel.comments?[index].user_offer_comment_id ?? 0
        vc.viewModel.comment = viewModel.comments?[index]
        vc.viewModel.replies = viewModel.comments?[index].replies?.reversed()
        navigationController?.pushViewController(vc, animated: true)
    }
    private func updateUI(hideKeyboard: Bool) {
        tableView.reloadData()
        if hideKeyboard {
            self.view.endEditing(true)
            textViewHeight.constant = 35
        }
    }
    private func setUpKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShown(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHidden(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChanged(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // MARK: - Keyboard Management
    @objc func keyboardShown(_ notification: Notification) {
        var offset = CGFloat.init()
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardViewEndFrame = view.convert(keyboardSize, from: view.window)
            let keyboardHeights = keyboardViewEndFrame.height
            print(keyboardHeights)  // 260
            offset += keyboardHeights  // add keyboard height
        } else {
            offset += 260
        }
//        if self.isModalInPresentation {
            UIView.animate(withDuration: 1.0) {
                self.view.layoutIfNeeded()
                self.textViewBottom.constant = self.keyboardHeight
                self.view.layoutIfNeeded()
            }
//        }
    }
    @objc func keyboardHidden(_ notification: Notification) {
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
            self.textViewBottom.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    @objc func keyboardFrameChanged(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let window = UIApplication.shared.keyWindow
            let bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
            keyboardHeight = keyboardSize.height - bottomPadding - 12 // -16/12: for textview padding in its container view
        }
    }
    
}

// MARK: - TextView methods
extension OfferDetailsViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text != "" {
            let height = textView.contentSize.height
            if height > 86 {
                textViewHeight.constant = 86
            } else {
                textViewHeight.constant = textView.contentSize.height
            }
        } else {
            textViewHeight.constant = 35
        }
    }
}
extension OfferDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : viewModel.comments?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.OffersCell.rawValue, for: indexPath) as! OffersCell
            cell.indexPath = indexPath
            cell.listType = 1
            cell.screenType = 2
            cell.hideShowViews()
            cell.offer = viewModel.offer
            cell.actions = { (index, action) in
                self.handleOfferActions(index: index, action: action)
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.OfferCommentsCell.rawValue, for: indexPath) as! OfferCommentsCell
            cell.indexPath = indexPath
            cell.comment = viewModel.comments?[indexPath.row]
            cell.actions = { (index, action) in
                self.handleOfferActions(index: index, action: action)
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
}
