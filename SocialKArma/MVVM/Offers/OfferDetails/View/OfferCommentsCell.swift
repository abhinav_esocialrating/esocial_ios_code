//
//  OfferCommentsCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 19/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class OfferCommentsCell: UITableViewCell {
    
    @IBOutlet weak var userImg: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var replyCountBtn: UIButton!
    @IBOutlet weak var imgLeadingConst: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var menuBtn: UIButton!
    
    var actions: ((_ index: Int, _ action: String) -> Void)?
    let dropDown = DropDown()
    var comment: OfferComment? {
        didSet {
            setValues()
        }
    }
    var type = 1  // 1: Comment  2: reply header  3: reply

    // MARK: - Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    func configureView() {
        userImg.rounded()
        containerView.layer.cornerRadius = 8
    }

    func setValues() {
        nameLabel.text = comment?.name ?? "null"
        commentLabel.text = comment?.comment
        if let urlStr = comment?.image_id, let url = URL(string: urlStr) {
            userImg.sd_setImage(with: url, for: .normal, completed: nil)
        } else {
            userImg.setImage(UIImage(named: "avatar-2"), for: .normal)
        }
        if let count = comment?.replies?.count  {
            replyCountBtn.setTitle("\(count) \(count > 1 ? "replies" : "reply")", for: .normal)
        } else {
            replyCountBtn.setTitle("0 replies", for: .normal)
        }
        menuBtn.isHidden = comment?.user_id != Int(UserDefaults.standard.userId)
    }
    func showDropDown() {
        dropDown.anchorView = menuBtn
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 150
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        dropDown.selectionAction = { (index: Int, item: String) in
            self.actions?(self.indexPath.row, index == 0 ? "edit" : "delete")
        }
        dropDown.dataSource = ["Edit", "Delete"]
        dropDown.show()
    }
    // MARK: - Actions
    @IBAction func replyTapped(_ sender: Any) {
        actions?(indexPath.row, "reply")
    }
    
    @IBAction func commentOptionsTapped(_ sender: Any) {
        showDropDown()
    }
    
    @IBAction func userImgTapped(_ sender: Any) {
        actions?(indexPath.row, "imageComment")
    }
}
