/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class OfferComment {
	public var comment : String?
	public var image_id : String?
	public var name : String?
	public var user_id : Int?
	public var user_offer_comment_id : Int?
    public var replies : Array<OfferComment>?
    
/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let OfferComment_list = OfferComment.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of OfferComment Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [OfferComment]
    {
        var models:[OfferComment] = []
        for item in array
        {
            models.append(OfferComment(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let OfferComment = OfferComment(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: OfferComment Instance.
*/
	required public init?(dictionary: NSDictionary) {

		comment = dictionary["comment"] as? String
		image_id = dictionary["image_id"] as? String
		name = dictionary["name"] as? String
		user_id = dictionary["user_id"] as? Int
		user_offer_comment_id = dictionary["user_offer_comment_id"] as? Int
        if let reply = dictionary["replies"] as? NSArray {
            replies = OfferComment.modelsFromDictionaryArray(array: reply)
        }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.comment, forKey: "comment")
		dictionary.setValue(self.image_id, forKey: "image_id")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.user_id, forKey: "user_id")
		dictionary.setValue(self.user_offer_comment_id, forKey: "user_offer_comment_id")
        dictionary.setValue(self.replies, forKey: "replies")

		return dictionary
	}

}
