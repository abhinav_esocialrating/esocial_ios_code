//
//  OfferDetailViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 19/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class OfferDetailViewModel: NSObject {

    var offer: Offer?
    var offerId = 0
    var comments: [OfferComment]?
    
    // MARK: - Web Services
    func getOfferDetails(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.offerDetails(parameters: parameters) { (response) in
            do {
                let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                AppDebug.shared.consolePrint(jsonData)
                switch response.response?.statusCode {
                case 200:
                    self.offer = Offer(dictionary: jsonData["data"] as! NSDictionary)
                    completion(true, JSON.init(), "")
                default:
                    let message = jsonData["message"] as? String
                    completion(false, JSON.init(), message ?? "Some Error Occured!")
                }
            } catch {
                completion(false, JSON.init(), "Some Error Occured!")
            }
            
        }
    }
    func offerComment(parameters: [String:Any], method: HTTPMethod, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.offerComment(parameters: parameters, method: method) { (response) in
            do {
                let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                AppDebug.shared.consolePrint(jsonData)
                switch response.response?.statusCode {
                case 200:
                    if let data = jsonData["data"] as? NSArray {
                        self.comments = OfferComment.modelsFromDictionaryArray(array: data).reversed()
                    }
                    completion(true, JSON.init(), "")
                case 202:
                    completion(true, JSON.init(), "")
                default:
                    let message = jsonData["message"] as? String
                    completion(false, JSON.init(), message ?? "Some Error Occured!")
                }
            } catch {
                completion(false, JSON.init(), "Some Error Occured!")
            }
            
        }
    }
}
