//
//  OfferFilterViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 25/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class OfferFilterViewController: UIViewController {

    @IBOutlet weak var aroundMeSwitch: UISwitch!
    @IBOutlet weak var skillContainer: UIView!
    @IBOutlet weak var locationTF: ACFloatingTextfield!
    @IBOutlet weak var scoreSlider: RangeSeekSlider!
    @IBOutlet weak var skillTF: ACFloatingTextfield!
    @IBOutlet weak var skillTagView: TagListView!
    @IBOutlet weak var skillViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sliderValueLabel: UILabel!
    
    // MARK: - Properties
    let viewModel = OfferFilterViewModel()
    let dropDown = DropDown()
    var sendData: SendData!
    var scoreSliderValue = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
    

    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        let dict = [
            "limit":10,
            "offset":1,
            "type":1,
            "score": scoreSliderValue,
            "location": locationTF.text!,
            "skill_id": viewModel.selectedSkillsDict.map({ $0["id"] as! Int }),
            "is_remote": aroundMeSwitch.isOn ? 1 : 0
        ] as [String : Any]
        sendData.passData(dataPasser: self, data: dict)
        navigationController?.popViewController(animated: true)
    }
    // MARK: - Methods
    func configureView() {
        var array1 = [skillTF, locationTF]
        for (_,textField) in array1.enumerated() {
            textField?.lineColor = UIColor.NewTheme.darkGray
            textField?.selectedLineColor = UIColor.NewTheme.paleBlue
            textField?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
            textField?.textColor = UIColor.NewTheme.darkGray
            textField?.disableFloatingLabel = true
            textField?.delegate = self
        }
        
        array1 = [skillTF, locationTF]
        array1.forEach({ $0?.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged) })
        
        skillTagView.delegate = self
        
        scoreSlider.delegate = self
    }
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        switch textField {
        case skillTF:
            dropDown.dataSource = viewModel.dropDownData
        case locationTF:
            dropDown.dataSource = viewModel.cityNames
        default:
            dropDown.dataSource = []
        }
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropDownSelection(index: index, item: item, textField: textField)
        }
        dropDown.show()
    }
    func handleDropDownSelection(index: Int, item: String, textField: UITextField) {
        switch textField {
        case skillTF:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.skills.contains(skillId) {
                    self.viewModel.selectedSkillsDict.append( self.viewModel.searchedData[index])
                    self.updateSkillsTagView(tag: item, textField: textField)
                    FilterModel.shared.skills.append(skillId)
                }
            }
        case locationTF:
            textField.text = item
        default:
            textField.text = ""
        }
    }
    func updateSkillsTagView(tag: String, textField: UITextField) {
        switch textField {
        case skillTF:
            skillTagView.addTag(tag)
            skillViewHeight.constant = skillTagView.intrinsicContentSize.height
        default:
            print("")
        }
    }
}
// MARK: - TextFieldDelegate
extension OfferFilterViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    @objc func textFieldChanged(_ textField: UITextField) {
        var params = ["config": "skill", "search": textField.text!]
        switch textField {
        case skillTF:
            params = ["config": "skill", "search": textField.text!]
        case locationTF:
            if textField.text!.count > 2 {
                LocationManager.shared.getCities(query: textField.text!) { (cities, cityNames) in
                    self.viewModel.dropDownData = cities
                    self.viewModel.cityNames = cityNames
                    DispatchQueue.main.async {
                        self.showDropDown(textField: textField)
                    }
                }
            }
        default:
            print(textField.text!)
        }
        if textField == skillTF {
            viewModel.getSearchConfig(parameters: params) { (success, json, message) in
                if success {
                    DispatchQueue.main.async {
                        if self.viewModel.searchedData.count > 0 {
                            self.showDropDown(textField: textField)
                        } else {
                            self.dropDown.hide()
                        }
                    }
                }
            }
        }
    }
    // MARK: - Web Services
    
}
// MARK: - TagListView delegates
extension OfferFilterViewController: TagListViewDelegate {
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        switch sender {
        case skillTagView:
            skillTagView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedSkillsDict, tagView: sender)
            skillViewHeight.constant = skillTagView.intrinsicContentSize.height
        default:
            debugPrint("")
        }
    }
    func checkForStringInTagViewData(tag: String, tagArray: inout [[String:Any]], tagView: TagListView) {
        for (j,i) in tagArray.enumerated() {
            if let value = i["value"] as? String {
                if value == tag {
                    tagArray.remove(at: j)
                    break
                }
            }
        }
    }
}
extension OfferFilterViewController: RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
        let roundedValue = roundf(Float(maxValue))
        let intValue = Int(roundedValue)
        let max = (abs(intValue/10) + 1)
         
//        let roundedValue1 = roundf(Float(minValue))
//        let intValue1 = Int(roundedValue1)
//        let min = (abs(intValue1/10) + 1)
        
        sliderValueLabel.text = "\(max == 11 ? 10 : max)"

    }
    func didEndTouches(in slider: RangeSeekSlider) {
        var roundedValue = roundf(Float(slider.selectedMaxValue))
        var intValue = Int(roundedValue)
        let max = (abs(intValue/10) + 1)
         
//        roundedValue = roundf(Float(slider.selectedMinValue))
//        intValue = Int(roundedValue)
//        let min = (abs(intValue/10) + 1)
        
        scoreSliderValue = max
        
//        print("\(min)   \(max)")
//        if slider == slider8 {
//            FilterModel.shared.minScore = Int(min)
//            FilterModel.shared.maxScore = Int(max == 11 ? 10 : max)
//            passData.passData(dataPasser: self, data: FilterModel.shared.paramDictionary)
//        } else {
//            let innerDict = ["parameter_id": slider.tag, "min": Int(min), "max": Int(max)]
//            FilterModel.shared.reviewParams[slider.tag] = innerDict
//            passData.passData(dataPasser: self, data: FilterModel.shared.reviewParams)
//        }
    }
}
