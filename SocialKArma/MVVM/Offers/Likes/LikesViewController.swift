//
//  LikesViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class LikesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    let viewModel = LikesViewModel()
    
    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        
        getLikesData()
    }
    func configureView() {
        // TableView
        tableView.dataSource = self
        tableView.delegate = self
    }
    func updateUI() {
        tableView.reloadData()
        tableView.isHidden = viewModel.likes?.count ?? 0 == 0
        noDataLabel.isHidden = viewModel.likes?.count ?? 0 > 0
    }
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    // MARK: - Web Services
    private func getLikesData() {
        OfferAPI.likeOffer(parameters: ["offerId": viewModel.offerId], method: .get) { (success, json, message) in
            
            if success {
                let data = json["data"].arrayObject
                self.viewModel.likes = LikesModel.modelsFromDictionaryArray(array: (data ?? []) as NSArray)
                self.updateUI()
            } else {
                self.alert(message: message)
            }
        }
    }
}
// MARK: - TableView methods
extension LikesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.likes?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.LikesCell.rawValue, for: indexPath) as! LikesCell
        cell.likesModel = viewModel.likes?[indexPath.row]
        return cell
    }
    
    
}
