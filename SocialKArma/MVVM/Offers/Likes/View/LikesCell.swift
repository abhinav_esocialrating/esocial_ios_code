//
//  LikesCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/10/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class LikesCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var userImg: UIImageView!
    
    var likesModel: LikesModel? {
        didSet {
            setValues()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        userImg.rounded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setValues() {
        nameLabel.text = likesModel?.name
        if let image_id = likesModel?.image_id, image_id != "" {
            userImg.sd_setImage(with: URL(string: image_id), completed: nil)
        } else {
            userImg.image = UIImage(named: "avatar-2")
        }
    }

}
