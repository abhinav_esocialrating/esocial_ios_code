//
//  OffersListViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import Alamofire
import DropDown

class OffersListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var navBarHeight: NSLayoutConstraint!
    @IBOutlet weak var menuBtn: UIButton!
    
    let viewModel = OfferListViewModel()
    var isFilterApplied = false
    let dropDown = DropDown()

    // MARK: - Life Cycle
    /**
    Life Cycle method called when view is loaded.
    */
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
    /**
    Life Cycle method called when view is loaded and about to appear on the screen.
    */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.offset = 1
        viewModel.offers = []
        if isFilterApplied == false {
            getOffers(id: 0)
        }
    }
    /**
    Setting up ui of the app and adding targets to different controls that needs to be done after view has loaded
    */
    func configureView() {
        // TableView
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        
        tableView.registerFromXib(name: TableViewCells.OffersCell.rawValue)
        
        navBarHeight.constant = viewModel.offerType == 6 ? 44 : 0
        menuBtn.rounded()
    }
    /**
    Updating whole UI after API call or Some other action
    */
    func updateUI() {
        tableView.reloadData()
        tableView.isHidden = viewModel.offers.count == 0
        noDataLabel.isHidden = viewModel.offers.count > 0
    }
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuTapped(_ sender: Any) {
        showDropDown()
    }
    // MARK: - Web Services
    /**
    API calling function for getting offer list for currently selected category
          
    - Parameter id: Not used for now.
    */
    func getOffers(id: Int) {
        let params = ["offset": viewModel.offset, "limit": viewModel.limit, "typeId": viewModel.offerType == 0 ? "" : "\(viewModel.offerType)", "otherUserId": viewModel.otherUserId] as [String : Any]
        viewModel.getOffers(parameters: params, isMyOffer: viewModel.offerType == 5, isOtherUserOffer: viewModel.offerType == 6, method: .get) { (success, _, _) in
            self.updateUI()
        }
    }
    /**
    API calling function for deleting a particular offers from the list.
          
    - Parameter id: Offer id of the offer that is to be deleted
    - Parameter index: index of the offer from the list that is to be deleted
    */
    func deleteOffers(id: Int, index: Int) {
        let params = ["offerId": id]
        viewModel.deleteOffers(parameters: params) { (success, _, message) in
            if success {
                self.showAutoToast(message: message)
            }
            self.viewModel.offers.remove(at: index)
            self.updateUI()
        }
    }
    /**
    API calling function for fetching filters according to the selected filters,
          
    - Parameter filers: Parameter dictionary containing message and other info.
    */
    func filterOffer(filers: [String:Any]) {
        let params = [
            "limit":10,
            "offset":viewModel.offset,
            "type":viewModel.offerType,
            "score": filers["score"] as? Int ?? "",
            "location": filers["location"] as? String ?? "",
            "skill_id": filers["skill_id"] as? [Int] ?? [],
            "is_remote": filers["is_remote"] as? Int ?? 0
            ] as [String : Any]
        AppDebug.shared.consolePrint(params)
        viewModel.filterOffer(parameters: params) { (success, _, message) in
            if success {
                self.updateUI()
            } else {
                self.alert(message: message)
            }
        }
    }
    // MARK: - Methods
    /**
    Handling action performed on an item from the list, by clicking buttons and links
          
    - Parameter index: Index of the item on which action is performed.
    - Parameter action: Type of action performed, denoted by string.
    */
    private func handleOfferActions(index: Int, action: String) {
        switch action {
        case "like":
            let method: HTTPMethod = (viewModel.offers[index].hasLiked ?? false) ? .delete : .post
            OfferAPI.likeOffer(parameters: ["offerId": viewModel.offers[index].offerId ?? 0], method: method) { (success, _, message) in
                
                if success {
                    self.viewModel.offers[index].hasLiked = !(self.viewModel.offers[index].hasLiked ?? false)
                    self.viewModel.offers[index].likes = (self.viewModel.offers[index].hasLiked ?? false) ? (self.viewModel.offers[index].likes ?? 0)+1 : (self.viewModel.offers[index].likes ?? 1)-1
                    self.updateUI()
                } else {
                    self.alert(message: message)
                }
            }
        case "comment":
            moveToDetailScreen(index: index)
        case "delete":
            deleteOffers(id: viewModel.offers[index].offerId ?? 0, index: index)
        case "likesCount":
            let vc = LikesViewController.instantiate(fromAppStoryboard: .Offers)
            vc.viewModel.offerId = viewModel.offers[index].offerId ?? 0
            navigationController?.pushViewController(vc, animated: true)
        case "link":
            if let url = viewModel.offers[index].url {
                UIApplication.shared.openAny(url: url)
            }
        case "image":
            let userId = viewModel.offers[index].userId
            openOtherUserProfile(userId: userId ?? 0)
        default:
            openChat(index: index)
        }
    }
    /**
    Moving to Offer details  screen.
          
    - Parameter index: Index of the item which is selected from the list.
    */
    func moveToDetailScreen(index: Int) {
        let vc = OfferDetailsViewController.instantiate(fromAppStoryboard: .Offers)
//        vc.viewModel.offer = viewModel.offers[index]
        vc.viewModel.offerId = viewModel.offers[index].offerId ?? 0
        navigationController?.pushViewController(vc, animated: true)
    }
    /**
    Moving to Chat window of the person who has put the offer
          
    - Parameter index: Index of the item which is selected from the list.
    */
    func openChat(index: Int) {
        let user_id = viewModel.offers[index].userId ?? 0
        let name = viewModel.offers[index].userInfo?.name ?? "NA"
        let image_id = viewModel.offers[index].userInfo?.image_id ?? ""
        
        let vc = ChatViewController.instantiate(fromAppStoryboard: .Chat)
        SocketIOManager.shared.toUserId = "\(user_id)"
        let chatUser = ChatUser(dictionary: ["name":name, "image_id":image_id] as NSDictionary)
        vc.viewModel.chatUser = chatUser
        
        navigationController?.pushViewController(vc, animated: true)
    }
    /**
    Method for opening a dropdown of some values
    */
    func showDropDown() {
        dropDown.anchorView = menuBtn
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 200
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        dropDown.dataSource = ["Create new offer", "Filter Offers"]
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropDownSelection(index: index)
        }
        dropDown.show()
    }
    /**
    Method for handling dropdown selection
     
     - Parameter index: index of seleted item
     - Parameter item: String value of the selected item
    */
    func handleDropDownSelection(index: Int) {
        if index == 0 {
            openCreateOffer()
        } else {
            openOfferFilter()
        }
    }
    /**
     Opening screen for creating new offers
     */
    func openCreateOffer() {
        let vc = EditOffersViewController.instantiate(fromAppStoryboard: .Offers)
        navigationController?.pushViewController(vc, animated: true)
    }
    /**
    Opening screen for applying filters to offers
    */
    func openOfferFilter() {
        let vc = OfferFilterViewController.instantiate(fromAppStoryboard: .Offers)
        vc.sendData = self
        navigationController?.pushViewController(vc, animated: true)
    }
}
extension OffersListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.offers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.OffersCell.rawValue, for: indexPath) as! OffersCell
        cell.indexPath = indexPath
        cell.listType = viewModel.offerType == 5 ? 2 : 1
        cell.screenType = 1
        cell.hideShowViews()
        cell.offer = viewModel.offers[indexPath.row]
        cell.actions = { (index, action) in
            self.handleOfferActions(index: index, action: action)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (viewModel.offers.count - 1) && (viewModel.offers.count % viewModel.limit) == 0 {
            viewModel.offset += 1
            getOffers(id: 0)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        moveToDetailScreen(index: indexPath.row)
    }
    
}
// MARK: - SendData
extension OffersListViewController: SendData {
    /**
    SendData Delegate method for getting notified whenever any other type wants to send data to current class.
     
     - Parameter dataPasser: Instance of the type that wants to send data, or Simply user defined String value or type to identify different types
     - Parameter data: Actual data thaat needs to be transferred.
    */
    func passData(dataPasser: Any, data: Any) {
        if let data = data as? [String:Any] {
            isFilterApplied = true
            viewModel.offset = 1
            filterOffer(filers: data)
        }
    }
}
