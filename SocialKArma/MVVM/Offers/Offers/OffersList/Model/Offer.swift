/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Offer {
	public var offerId : Int?
	public var userId : Int?
	public var offerType_Id : Int?
	public var offer_type : String?
	public var description : String?
	public var minScore : Int?
	public var minAge : Int?
	public var maxAge : Int?
	public var location_city : String?
	public var is_organic : Int?
	public var title : String?
	public var likes : Int?
	public var comments : Int?
	public var date : String?
    public var created_at : String?
	public var skills : Array<SkillModel>?
	public var jobType : Array<String>?
	public var hasLiked : Bool?
	public var userInfo : UserInfo?
    public var status : String?
    public var url : String?
    public var viewCount : Int?
    public var is_remote : Int?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let Offer_list = Offer.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Offer Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Offer]
    {
        var models:[Offer] = []
        for item in array
        {
            models.append(Offer(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let Offer = Offer(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Offer Instance.
*/
	required public init?(dictionary: NSDictionary) {

		offerId = dictionary["offerId"] as? Int
		userId = dictionary["userId"] as? Int
		offerType_Id = dictionary["offerType_Id"] as? Int
		offer_type = dictionary["offer_type"] as? String
		description = dictionary["description"] as? String
		minScore = dictionary["minScore"] as? Int
		minAge = dictionary["minAge"] as? Int
		maxAge = dictionary["maxAge"] as? Int
		location_city = dictionary["location_city"] as? String
		is_organic = dictionary["is_organic"] as? Int
		title = dictionary["title"] as? String
		likes = dictionary["likes"] as? Int
		comments = dictionary["comments"] as? Int
		date = dictionary["date"] as? String
        created_at = dictionary["created_at"] as? String
        if (dictionary["skills"] != nil) {
            skills = SkillModel.modelsFromDictionaryArray(array: dictionary["skills"] as! NSArray)
        }
//		if (dictionary["jobType"] != nil) { jobType = JobType.modelsFromDictionaryArray(dictionary["jobType"] as! NSArray) }
		hasLiked = dictionary["hasLiked"] as? Bool
		if (dictionary["userInfo"] != nil) { userInfo = UserInfo(dictionary: dictionary["userInfo"] as! NSDictionary) }
        status = dictionary["status"] as? String
        url = dictionary["url"] as? String
        viewCount = dictionary["viewCount"] as? Int
        is_remote = dictionary["is_remote"] as? Int

	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.offerId, forKey: "offerId")
		dictionary.setValue(self.userId, forKey: "userId")
		dictionary.setValue(self.offerType_Id, forKey: "offerType_Id")
		dictionary.setValue(self.offer_type, forKey: "offer_type")
		dictionary.setValue(self.description, forKey: "description")
		dictionary.setValue(self.minScore, forKey: "minScore")
		dictionary.setValue(self.minAge, forKey: "minAge")
		dictionary.setValue(self.maxAge, forKey: "maxAge")
		dictionary.setValue(self.location_city, forKey: "location_city")
		dictionary.setValue(self.is_organic, forKey: "is_organic")
		dictionary.setValue(self.title, forKey: "title")
		dictionary.setValue(self.likes, forKey: "likes")
		dictionary.setValue(self.comments, forKey: "comments")
		dictionary.setValue(self.date, forKey: "date")
		dictionary.setValue(self.hasLiked, forKey: "hasLiked")
		dictionary.setValue(self.userInfo?.dictionaryRepresentation(), forKey: "userInfo")
        dictionary.setValue(self.status, forKey: "status")
        dictionary.setValue(self.url, forKey: "url")
        dictionary.setValue(self.viewCount, forKey: "viewCount")

		return dictionary
	}

}
