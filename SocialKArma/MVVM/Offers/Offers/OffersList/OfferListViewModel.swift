//
//  OfferListViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 19/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class OfferListViewModel: NSObject {
    
    var offers = [Offer]()
    
    var offerType = 1  // 5: My Offers  6: Other User Offers
    var offset = 1
    var limit = 10
    var otherUserId = 0
    
    // MARK: - Web Services
    func getOffers(parameters: [String:Any], isMyOffer: Bool, isOtherUserOffer: Bool, method: HTTPMethod, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getOffers(parameters: parameters, isMyOffer: isMyOffer, isOtherUserOffer: isOtherUserOffer, method: method) { (response) in
            do {
                let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                AppDebug.shared.consolePrint(jsonData)
                switch response.response?.statusCode {
                case 200:
                    if let data = jsonData["data"] as? NSArray {
                        let offers = Offer.modelsFromDictionaryArray(array: data)
                        if self.offerType == 5 {
                            self.offers.insert(contentsOf: offers.reversed(), at: 0)
                        } else {
                            self.offers.append(contentsOf: offers)
                        }
                    }
                    completion(true, JSON.init(), "")
                case 202:
                    completion(true, JSON.init(), "")
                default:
                    let message = jsonData["message"] as? String
                    completion(false, JSON.init(), message ?? "Some Error Occured!")
                }
            } catch {
                completion(false, JSON.init(), "Some Error Occured!")
            }
            
        }
    }
    func deleteOffers(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.deleteOffers(parameters: parameters) { (response) in
            do {
                let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                AppDebug.shared.consolePrint(jsonData)
                switch response.response?.statusCode {
                case 200:
//                    if let data = jsonData["data"] as? NSArray {
//                        self.offers = Offer.modelsFromDictionaryArray(array: data)
//                    }
                    completion(true, JSON.init(), jsonData["message"] as? String ?? "Delete request submitted")
                case 202:
                    completion(true, JSON.init(), jsonData["message"] as? String ?? "Delete request submitted")
                default:
                    let message = jsonData["message"] as? String
                    completion(false, JSON.init(), message ?? "Some Error Occured!")
                }
            } catch {
                completion(false, JSON.init(), "Some Error Occured!")
            }
            
        }
    }
    func filterOffer(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {

        NetworkManager.filterOffer(parameters: parameters, method: .post) { (response) in
            do {
                let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                AppDebug.shared.consolePrint(jsonData)
                switch response.response?.statusCode {
                case 200:
                    if let data = jsonData["data"] as? NSArray {
                        let offers = Offer.modelsFromDictionaryArray(array: data)
                        if self.offerType == 5 {
                            self.offers.insert(contentsOf: offers.reversed(), at: 0)
                        } else {
                            self.offers.append(contentsOf: offers)
                        }
                    }
                    completion(true, JSON.init(), "")
                case 202:
                    completion(true, JSON.init(), "")
                default:
                    let message = jsonData["message"] as? String
                    completion(false, JSON.init(), message ?? "Some Error Occured!")
                }
            } catch {
                completion(false, JSON.init(), "Some Error Occured!")
            }
            
        }
    }
}
