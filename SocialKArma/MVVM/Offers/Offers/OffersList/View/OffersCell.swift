//
//  OffersCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class OffersCell: UITableViewCell {
    
    @IBOutlet weak var bkgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var jobTypeLabel: UILabel!
    @IBOutlet weak var myOfferLocationLabel: UILabel!
    @IBOutlet weak var LocationLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var userImg: UIButton!
    @IBOutlet weak var skillTagView: TagListView!
    @IBOutlet weak var noSkiilsLabel: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var actionsStack: UIStackView!
    @IBOutlet weak var responsesStack: UIStackView!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var userInfoHeight: NSLayoutConstraint!
    @IBOutlet weak var likesCountBtn: UIButton!
    @IBOutlet weak var linkBtn: UIButton!
    // MARK: - Properties
    var actions: ((_ index: Int, _ action: String) -> Void)?
    var screenType = 1  // 1: List   2: Details
    var listType = 1    // 1: Offers   2: My Offers
    var offer: Offer? {
        didSet {
            setValues()
        }
    }
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if screenType == 1 {
            contentView.layoutSubviews()
            bkgView.layoutSubviews()
            bkgView.layer.cornerRadius = 12
            bkgView.dropShadow(color: UIColor.lightGray, opacity: 0.6, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        }
    }
    // MARK: - Methods
    func configureView() {
        userImg.rounded()
    }
    func hideShowViews() {
        if listType == 1 {
            responsesStack.subviews.forEach({ $0.isHidden = true })
            actionsStack.subviews.forEach({ $0.isHidden = false })
            userInfoHeight.constant = 60
        } else {
            responsesStack.subviews.forEach({ $0.isHidden = false })
            actionsStack.subviews.forEach({ $0.isHidden = true })
            userInfoHeight.constant = 0
        }
        
        if (listType == 1 && screenType == 2) || (listType == 2 && screenType == 1) {
            commentBtn.isHidden = true
            
            
        } else {
            
            commentBtn.isHidden = false
            
        }
    }
    func setValues() {
        nameLabel.text = offer?.userInfo?.name
        timeLabel.text = offer?.date
        titleLabel.text = offer?.title
        descLabel.text = offer?.description
        myOfferLocationLabel.text = listType == 2 ? offer?.location_city : ""
        
        if listType == 1 {
            if let remote = offer?.is_remote, remote == 1 {
                LocationLabel.text = "Remote Job"
            } else {
                LocationLabel.text = offer?.location_city
            }
            jobTypeLabel.text = offer?.offer_type
        } else {
            if offer?.status == "UNDER_REVIEW" {
                LocationLabel.text = "Approval Pending"
                jobTypeLabel.text = "Submitted On \(offer?.created_at?.components(separatedBy: " ").first ?? "")"
            } else if offer?.status == "ACTIVE" {
                LocationLabel.text = "Approved"
                jobTypeLabel.text = "Live On \(offer?.created_at?.components(separatedBy: " ").first ?? "")"
            } else {
                LocationLabel.text = "Rejected"
                jobTypeLabel.text = "Rejected On \(offer?.created_at?.components(separatedBy: " ").first ?? "")"
            }
        }
        
        if listType == 1 {
            if let urlStr = offer?.userInfo?.image_id, let url = URL(string: urlStr) {
                userImg.sd_setImage(with: url, for: .normal, completed: nil)
            } else {
                userImg.setImage(UIImage(named: "avatar-2"), for: .normal)
            }
        } else {
            userImg.setImage(nil, for: .normal)
        }
        if let score = offer?.minScore {
            scoreLabel.text = "Minimum Goodness Score: \(score)"
        } else {
            scoreLabel.text = "Minimum Goodness Score: 0.0"
        }
        skillTagView.removeAllTags()
        if let skills = offer?.skills, skills.count > 0 {
            noSkiilsLabel.isHidden = true
            skillTagView.isHidden = false
            skills.forEach({ skillTagView.addTag($0.skill ?? "") })
        } else {
            noSkiilsLabel.isHidden = false
            skillTagView.isHidden = true
        }
        if let hasLiked = offer?.hasLiked, hasLiked {
            likeBtn.setImage(UIImage(named: "like_filled"), for: .normal)
        } else {
            likeBtn.setImage(UIImage(named: "like"), for: .normal)
        }
        likesCountBtn.setTitle("\(offer?.likes ?? 0) likes", for: .normal)
        if screenType == 1 {
            commentBtn.setTitle("\(offer?.comments ?? 0)", for: .normal)
        } else {
            commentBtn.setTitle(nil, for: .normal)
        }
        
        if let approved = offer?.status, approved == "ACTIVE", let responses = offer?.viewCount {
            responseLabel.isHidden = screenType == 2 ? true : false
            responseLabel.text = listType == 1 ? "" : "Responses: \(responses)"
            deleteBtn.setTitle("Close Offer", for: .normal)
        } else {
            responseLabel.isHidden = true
            responseLabel.text = ""
            deleteBtn.setTitle("Delete", for: .normal)
        }
        if let url = offer?.url {
            let textRange = NSMakeRange(0, url.count)
            let attributedText = NSMutableAttributedString(string: url)
            attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
            // Add other attributes if needed
            linkBtn.setAttributedTitle(attributedText, for: .normal)
        } else {
            linkBtn.setTitle("", for: .normal)
        }
    }
    
    // MARK: - Actions
    @IBAction func llikeTapped(_ sender: Any) {
        actions?(indexPath.row, "like")
    }
    @IBAction func commentTapped(_ sender: Any) {
        actions?(indexPath.row, "comment")
    }
    @IBAction func connectTapped(_ sender: Any) {
        actions?(indexPath.row, "connect")
    }
    @IBAction func deleteTapped(_ sender: Any) {
        actions?(indexPath.row, "delete")
    }
    @IBAction func linkTapped(_ sender: Any) {
        actions?(indexPath.row, "link")
    }
    @IBAction func likesTapped(_ sender: Any) {
        actions?(indexPath.row, "likesCount")
    }
    @IBAction func userImgTapped(_ sender: Any) {
        actions?(indexPath.row, "image")
    }
}
