//
//  OffersViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import FAPanels
import FirebaseAnalytics
import ViewPager_Swift

class OffersViewController: UIViewController {

    @IBOutlet weak var notificationBadge: UILabel!
    @IBOutlet weak var chatNotificationBadge: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var createBtn: UIButton!
    @IBOutlet weak var searchStatusBtn: UIButton!
    
    var viewPager: ViewPager!
    let pagerVC = OffersPagerViewController()

    // MARK: - Life Cycle
    /**
    Life Cycle method called when view is loaded.
    */
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        setUpNavBar()
        configurePager()
        NotificationCenter.default.addObserver(self, selector: #selector(searchStatus(notification:)), name: Notification.Name.SearchStatus, object: nil)

    }
    /**
    Life Cycle method called when view is loaded and about to appear on the screen.
    */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setNotificationBadges()
        // Search status
        
        searchStatusBtn.tintColor = (UserDefaults.standard.SearchStatus["friendStatus"] as! Int == 1 || UserDefaults.standard.SearchStatus["professionalStatus"] as! Int == 1) ? .green : .red
    }
    /**
    Life cycle method when view has appeared fully on screen
    */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Screen tracking
        Analytics.logEvent("screen_tracking", parameters: [
            "name": "TabClass" as NSObject,
            "full_text": "OffersViewController" as NSObject
        ])
    }
    /**
     Destructor method when the class if removed from memory
     */
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    /**
    Setting up ui of the app and adding targets to different controls that needs to be done after view has loaded
    */
    func configureView() {
        createBtn.rounded()
        
        // Notification badges
        chatNotificationBadge.layer.cornerRadius = chatNotificationBadge.frame.width/2
        chatNotificationBadge.clipsToBounds = true
        notificationBadge.layer.cornerRadius = notificationBadge.frame.width/2
        notificationBadge.clipsToBounds = true
    }
    /**
     Configuring the Sliding pager for the different categories of offers
     */
    func configurePager() {
        addChild(pagerVC)
        pagerVC.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        pagerVC.view.frame = containerView.bounds
        
        containerView.addSubview(pagerVC.view)
        pagerVC.didMove(toParent: self)
        
    }
    // MARK: - Methods

    // MARK: - Actions
    @IBAction func infoTapped(_ sender: Any) {
        openVideoPlayer()
    }
    @IBAction func sideMenuTapped(_ sender: Any) {
        if let panel = appDelegate?.window?.rootViewController as? FAPanelController {
            panel.openLeft(animated: true)
        }
    }
    @IBAction func notificationTapped(_ sender: Any) {
        openNotificationFromNavBar()
    }
    @IBAction func pointsBtnTapped(_ sender: Any) {
        openPointsScreenFromNavBar()
    }
    @IBAction func messageTapped(_ sender: Any) {
        openChatFromNavBar()
    }
    @IBAction func createOfferTapped(_ sender: Any) {
        let vc = EditOffersViewController.instantiate(fromAppStoryboard: .Offers)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func activelyLookingTapped(_ sender: Any) {
        openActivelyLooking()
    }
    /**
     Selector for observing changes in the user's active status for searching friends or professional jobs/works
     */
    @objc func searchStatus(notification: Notification) {
        searchStatusBtn.tintColor = (UserDefaults.standard.SearchStatus["friendStatus"] as! Int == 1 || UserDefaults.standard.SearchStatus["professionalStatus"] as! Int == 1) ? .green : .red
    }
}
// MARK: - NavBar
extension OffersViewController {
    // Notification Handling- To be moved to a separate class soon
    func setUpNavBar() {
        NotificationCenter.default.addObserver(self, selector: #selector(pushNotificationReceived(_:)), name: Notification.Name.PushReceived, object: nil)
    }
    @objc func pushNotificationReceived(_ notification: Notification) {
        if let type = notification.userInfo?["type"] as? String {
            if type == PushNotificationType.FEEDBACK_UE.rawValue || type == PushNotificationType.FEEDBACK_UE_2.rawValue || type == PushNotificationType.CONTACT_UE.rawValue || type == PushNotificationType.INVITE_UE.rawValue || type == PushNotificationType.PROFILE_UE.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_1.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_2.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_3.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_4.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_5.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_6.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_7.rawValue || type == PushNotificationType.UE_INFO_1.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_1.rawValue || type == PushNotificationType.UE_REINVITE_1.rawValue || type == PushNotificationType.UE_REINVITE_2.rawValue || type == PushNotificationType.CHAT_UNREAD_MESSAGES.rawValue {  }
            else if type == PushNotificationType.CHAT.rawValue {
                setNotificationBadges()
            }
//            else if type == PushNotificationType.OFFER_ACCEPT.rawValue, type == PushNotificationType.OFFER_COMMENT_ORGANIC.rawValue, type == PushNotificationType.OFFER_COMMENT_NOTIFICATION.rawValue, type == PushNotificationType.OFFER_LIKE_NOTIFICATION.rawValue {
//                moveToDetailScreen(id: 0)
//            }
            else {
                setNotificationBadges()
            }
        }
    }
    /**
    Setting badges on Notificaiton and Chat button to show number of unread notifications and numbers.
    */
    func setNotificationBadges() {
        PointsHandler.shared.getNotificationCount { notificationCountData in
            
            if let notifications = notificationCountData["notification"] as? Int, notifications > 0 {
                PointsHandler.shared.isNotification = true
                self.notificationBadge.isHidden = false
                self.notificationBadge.text = "\(notifications)"
            } else {
                self.notificationBadge.isHidden = PointsHandler.shared.isNotification ? false : true
            }
            if let chat = notificationCountData["chat"] as? Int, chat > 0 {
                PointsHandler.shared.isChatNotificationBadge = true
                self.chatNotificationBadge.isHidden = false
                self.chatNotificationBadge.text = "\(chat)"
            } else {
                self.chatNotificationBadge.isHidden = PointsHandler.shared.isChatNotificationBadge ? false : true
            }
        }
    }
}
