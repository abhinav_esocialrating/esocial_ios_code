//
//  OffersPagerViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 16/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import ViewPager_Swift
import DropDown

class OffersPagerViewController: UIViewController {
    
    var viewPager: ViewPager!
    var vcs: [UIViewController] = []


    override func viewDidLoad() {
        super.viewDidLoad()

        configurePager()
    }
    
    func configurePager() {
        
        let vc1 = OffersListViewController.instantiate(fromAppStoryboard: .Offers)
        vc1.viewModel.offerType = 0
        vcs.append(vc1)
        let vc5 = OffersListViewController.instantiate(fromAppStoryboard: .Offers)
        vc5.viewModel.offerType = 5
        vcs.append(vc5)
        let vc2 = OffersListViewController.instantiate(fromAppStoryboard: .Offers)
        vc2.viewModel.offerType = 1
        vcs.append(vc2)
        let vc3 = OffersListViewController.instantiate(fromAppStoryboard: .Offers)
        vc3.viewModel.offerType = 2
        vcs.append(vc3)
        let vc4 = OffersListViewController.instantiate(fromAppStoryboard: .Offers)
        vc4.viewModel.offerType = 3
        vcs.append(vc4)
        
        let options = ViewPagerOptions()
        options.tabViewBackgroundDefaultColor = .white
        options.tabIndicatorViewBackgroundColor = UIColor.SearchColors.blue
        options.tabViewTextDefaultColor = .darkGray
        options.distribution = .segmented
        options.isTabHighlightAvailable = false
        
        viewPager = ViewPager(viewController: self)
        viewPager.setOptions(options: options)
        viewPager.setDataSource(dataSource: self)
        viewPager.setDelegate(delegate: self)
        viewPager.build()
    }
    

}
// MARK: - ViewPager Methods
extension OffersPagerViewController: ViewPagerDataSource, ViewPagerDelegate {
    func numberOfPages() -> Int {
        return vcs.count
    }
    func viewControllerAtPosition(position: Int) -> UIViewController {
        return vcs[position]
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        let titles = ["All", "My Offers", "Job", "Sales", "Others"]
        return titles.map({ ViewPagerTab(title: $0, image: nil) })
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
    
    func willMoveToControllerAtIndex(index: Int) {
        
    }
    
    func didMoveToControllerAtIndex(index: Int) {
        viewPager.displayViewController(atIndex: index)
//        pageMoved?(index)
    }
    
    
}
