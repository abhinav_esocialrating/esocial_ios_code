//
//  OfferAPI.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 22/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class OfferAPI: NSObject {
    class func likeOffer(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {

        NetworkManager.likeOffer(parameters: parameters, method: method) { (response) in
            do {
                let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                AppDebug.shared.consolePrint(jsonData)
                switch response.response?.statusCode {
                case 200:
                    completion(true, JSON(jsonData), "")
                default:
                    let message = jsonData["message"] as? String
                    completion(false, JSON.init(), message ?? "Some Error Occured!")
                }
            } catch {
                completion(false, JSON.init(), "Some Error Occured!")
            }
        }
    }
}
