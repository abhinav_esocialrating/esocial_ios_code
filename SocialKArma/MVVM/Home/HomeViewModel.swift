//
//  HomeViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import ContactsUI
import SwiftyJSON
import Alamofire

class HomeViewModel: NSObject {
    var phoneContacts = [PhoneContact]()
    var searchedContact: PhoneContact!
    var extendedContacts = [Int]()
    var searchedIndexPath: IndexPath?
    var indexPathBeforeSearch: IndexPath?
    var filter: ContactsFilter = .none
    var indexForSelected = 0
    
    /** sliderValues
            a dictionary of [Int:Int]()
            Key Int:  id of the parameter
            Value Int:   Rated by integer.
    */
    var sliderValues = [Int:Int]()
    
    /** changedRatingsForParameterIDs
            An array of Int
        Whichever ratings are chanegd, their keys will be saved here and will be sent in rating api.
    */
    var changedRatingsForParameterIDs = [Int]()
    
    /** sliderExactValues
            a dictionary of [Int:Int]()
            Key Int:  id of the parameter
            Value Int:   value that slider was left on.
    */
    var sliderExactValues = [Int:Int]()
    var blockedParameters = [Int]()
    var ratings = [Double]()
    var sliderTexts = [String]()
    var isShowMore = false
    var selectedCategory = 100
    var tzCards = [TZCard]()
    var shouldCallCardsApiOnAppear = true
    
    /**  requestModel
            A dictionary of [String:Any]
            When a card is swiped, the request model will be sent for rating, and then it will be initialized.
    */
    var requestModel = [String:Any]()
    var ipnForCurrentCard = [String:String]()
    /**  pushCards
            An array of TZCard
                Whenever the review api has a card in the response, it will be added to this array
     */
    var pushCards = [TZCard]()
    var isSearchOn = false
    var isPushCardAdded = false
    var actionPerformedOnCard = Card_Action.IGNORE.rawValue
    weak var controller: HomeViewController!
    var feedback = ""
    var goodnessScore = 0
    var relationshipCloseness = 0
    var relationshipYears = 0
    var relationship = ""
    var relationship_id = 0
    var currentIndexForFeedback = 0
    var notificationType = "default"
    var notificationTypeToBeNil = false
    // MARK: - Methods
    /**
     Starting contact fetching and uploading process
     */
    func loadContacts(completion: completionBlock) {
        UserContacts.sharedInstance.loadContacts()
    }
    /**
    Creating request parameters for feedback API
     
     Newer version is used now
     ```` createRequestModelForNew(index: Int) ````
     
     - Parameter index: Index of the clicked Card in the list of All cards (datasource).
    */
    func createRequestModelFor(index: Int) {
        requestModel = [:]
        requestModel["rated_user_id"] = tzCards[index].target_user_id ?? 1
        var ratingKey = [[String:Int]]()
        ratingKey = changedRatingsForParameterIDs.map { (ratingId) -> [String:Int] in
            var dict = [String:Int]()
            dict["parameter_id"] = ratingId
            dict["rating_value"] = sliderValues[ratingId]
            return dict
        }
        requestModel["rating"] = ratingKey
//        requestModel["relationship_id"] = selectedCategory  // To be used in later version. Hence should not be removed.
        print(requestModel)
    }
    /**
    Creating request parameters for feedback API
     
     - Parameter index: Index of the clicked Card in the list of All cards (datasource).
    */
    func createRequestModelForNew(index: Int) {
        requestModel = [:]
        requestModel["rated_user_id"] = tzCards[index].target_user_id ?? 1
        var ratingKey = [[String:Int]]()
        if goodnessScore != 0 && changedRatingsForParameterIDs.count == 0 {
            guard let allParameters = ShareParameters.shared.allParameters else {
                return
            }
            ratingKey = allParameters.map({ (parameter) -> [String:Int] in
                var dict = [String:Int]()
                dict["parameter_id"] = parameter.parameter_id ?? 0
                dict["rating_value"] = goodnessScore
                return dict
            })
            
            var dict = [String:Int]()
            dict["parameter_id"] = 9
            dict["rating_value"] = goodnessScore
            ratingKey.append(dict)
            
        } else {
            ratingKey = changedRatingsForParameterIDs.map { (ratingId) -> [String:Int] in
                var dict = [String:Int]()
                dict["parameter_id"] = ratingId
                dict["rating_value"] = sliderValues[ratingId]
                return dict
            }
        }
        if ratingKey.count > 0 {
            requestModel["rating"] = ratingKey
        }
        if relationshipCloseness != 0 {
            requestModel["relationship_strength"] = relationshipCloseness
        }
        if relationshipYears != 0 {
            requestModel["relationship_years"] = relationshipYears
        }
        if feedback != "" {
            requestModel["feedback"] = feedback
        }
        if relationship_id != 0 {
            requestModel["relationship_id"] = relationship_id
        }
//        requestModel["relationship_id"] = selectedCategory  // To be used in later version. Hence should not be removed.
        print(requestModel)
    }
    /**
     Creating IPN dictionary for the current card
     */
    func createIPNAndModifyCardData(card: TZCard) {
        var dict = [String:String]()
        if let card_id = card.ignore_action_card_id {
            for (_,j) in tzCards.enumerated() {
                if j.card_id == card_id {
                    dict[Card_Action.IGNORE.rawValue] = card_id
                    break
                }
            }
        }
        if let card_id = card.positive_action_card_id {
            for (_,j) in tzCards.enumerated() {
                if j.card_id == card_id {
                    dict[Card_Action.POSITIVE.rawValue] = card_id
                    break
                }
            }
        }
        if let card_id = card.negative_action_card_id {
            for (_,j) in tzCards.enumerated() {
                if j.card_id == card_id {
                    dict[Card_Action.NEGATIVE.rawValue] = card_id
                    break
                }
            }
        }
        ipnForCurrentCard = dict
    }
    /**
     Reset viewModel data by clear all temporary variable that contain current feedback values
     
     Newer version is used now
     ```` resetViewModelData() ````
     */
    func resetViewModelData() {
        requestModel = [:]
        changedRatingsForParameterIDs = []
        sliderExactValues = [:]
        selectedCategory = 100
        blockedParameters = []
//        ipnForCurrentCard = [:]
    }
    /**
    Reset viewModel data by clear all temporary variable that contain current feedback values
    */
    func resetViewModelDataNew() {
        requestModel = [:]
        changedRatingsForParameterIDs = []
        sliderExactValues = [:]
        sliderValues = [:]
        selectedCategory = 100
        blockedParameters = []
        goodnessScore = 0
        feedback = ""
        relationshipCloseness = 0
        relationshipYears = 0
        relationship = ""
        relationship_id = 0
    }
    func handleDetailedFeedbackNotification() {
        PointsHandler.shared.isFeedbackDetailNotification = false
        var count = 0
        for (i,j) in tzCards.enumerated() {
            if j.subType == CardSubtype.FEEDBACK.rawValue {
                count = i
                break
            }
        }
        if count != 0 {
            tzCards = Array(tzCards.dropFirst(count))
        }
    }
    func deleteCards() {
        let userId = tzCards[0].target_user_id ?? 0
        var indexes = [Int]()
        for (i,j) in tzCards.enumerated() {
            if j.target_user_id == userId {
                indexes.append(i)
            }
        }
        tzCards.remove(at: indexes)
    }
    // MARK: - Web services
    func getTZCards(parameters: [String:Any], query: Int, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getTZCards(parameters: query != 0 ? parameters : [:], query: "on", method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData["data"] as! NSArray
                    if query != 0 {
                        let newCards = TZCard.modelsFromDictionaryArray(array: data)
                        if newCards.count > 0 {
                            self.tzCards.insert(contentsOf: newCards, at: 0)
                        }
                    } else {
                        self.tzCards = []
                        self.tzCards = TZCard.modelsFromDictionaryArray(array: data)
                        self.checkWaitingCards()
                        self.setParametersToSingleton()
                        if PointsHandler.shared.isFeedbackDetailNotification && self.tzCards.count > 0 {
                            self.handleDetailedFeedbackNotification()
                        }
                    }
                } catch {
                    
                }
                completion(true, json, "")
                if query == 0 {
                    self.saveCardsToUserDefaults(json: json["data"])
                }
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func rateUser(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.rateUser(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("rateUser")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData["data"] as! NSDictionary
                    if let points = data["points"] as? Int {
                        PointsHandler.shared.updateTZPoints(points: points, changedToBeShown: true)
                        NotificationCenter.default.post(name: Notification.Name.Points.didChange, object: nil, userInfo: ["points": points])
                    }
//                    self.pushCards = []
                    if let card = data["card"] as? NSDictionary {
                        let tzCard = TZCard(dictionary: card)
                        self.pushCards.append(tzCard!)
                    }
                } catch {
                    
                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func getSelfAssessment(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getSelfAssessment(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getSelfAssessment")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    if let card = jsonData["data"] as? NSDictionary {
                        let tzCard = TZCard(dictionary: card)
                        self.tzCards.insert(tzCard!, at: 0)
                    }
                } catch {
                }
                completion(true, json, "")
            default:
                completion(false, JSON.init(), "")
            }
        }
    }
    func reviewSwipe(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
    
        NetworkManager.reviewSwipe(parameters: parameters, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("reviewSwipe")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json, "")
            default:
                completion(false, JSON.init(), "")
            }
        }
    }
    func deleteContact(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        let id = parameters["id"] as! Int
        let url = AppUrl.baseUrl.rawValue + "contact" + "/\(id)"
        NetworkManager.deleteContactCards(url: url, parameters: [:], method: .delete) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint("deleteContactCards")
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                self.deleteCards()
                completion(true, json, "")
            default:
                completion(false, JSON.init(), "")
            }
        }
    }
}
// MARK: - Card new logic
extension HomeViewModel {
    func deleteCardById(cardId: String) {
        var index = 1000
        for (j,i) in tzCards.enumerated() {
            if i.card_id == cardId {
                index = j
                break
            }
        }
        if index != 1000 {
            tzCards.remove(at: index)
        }
    }
    func getCardByID(cardId: String) -> (TZCard?, Int) {
        for (j,i) in tzCards.enumerated() {
            if i.card_id == cardId {
                return (i,j)
            }
        }
        return (nil, 0)
    }
    func removeCardRecursively(cardId: String) {
        let card = getCardByID(cardId: cardId).0
        if let id = card?.ignore_action_card_id {
            removeCardRecursively(cardId: id)
        }
        if let id = card?.positive_action_card_id {
            removeCardRecursively(cardId: id)
        }
        if let id = card?.negative_action_card_id {
            removeCardRecursively(cardId: id)
        }
        deleteCardById(cardId: cardId)
    }
    func renewCardDataAfterAction(index: Int) {
        // Check if there is a push card: if exixts, add it to index+1
        checkForPushCards(index: index)
        // create IPN dictionary for current card
        createIPNAndModifyCardData(card: tzCards[index])
        // check for removing all the IPN cards
        checkIPNForAction(index: index)
        
        // Check if coach marks to show and reload data accordingly
        actionPerformedOnCard = Card_Action.IGNORE.rawValue  // set default action to ignore
        if tzCards.count > 0 {
            if !UserDefaults.standard.isCoachMarkDone {
                if let type = tzCards[0].type, let rc_source = tzCards[0].rc_source {
                    if type == CardType.review.rawValue && rc_source == Rc_source.SELF.rawValue {
                        controller.coachMarkToShow = .HighLow
                    } else if type == CardType.review.rawValue {
                        controller.coachMarkToShow = .InviteAndSearch
                    }
                }
                if let cardCode = tzCards[0].card_code {
                    if cardCode == Card_Code.REWARD_SA.rawValue || cardCode == Card_Code.REWARD_REVIEW.rawValue {
                        controller.coachMarkToShow = .Points
                    } else if cardCode == Card_Code.FIRST_CARD.rawValue {
                        controller.coachMarkToShow = .SwipeUp
                    }
                }
            }
            isShowMore = false
            controller.kolodaView.resetCurrentCardIndex()
        } else {
//            controller.getRatingCards(query: 0)
        }
    }
    func checkIPNForAction(index: Int) {
        switch actionPerformedOnCard {
        case Card_Action.IGNORE.rawValue:
            removePositiveActionCard(value: true, index: 0)
            removeNegativeActionCard(value: true, index: 0)
            
            if tzCards[index].ignore_action_card_id != nil {
                if ipnForCurrentCard[Card_Action.IGNORE.rawValue] != nil {
                    let pos = getCardByID(cardId: ipnForCurrentCard[Card_Action.IGNORE.rawValue]!)
                    tzCards.remove(at: index)
                    tzCards.insert(pos.0!, at: isPushCardAdded ? index+1 : index)
                    isPushCardAdded = false
                    removeIgnoreActionCard(value: false, index: pos.1)
                } else {
                    tzCards.remove(at: index)
                }
            } else {
                tzCards.remove(at: index)
            }
            
        case Card_Action.NEGATIVE.rawValue:
            removePositiveActionCard(value: true, index: 0)
            removeIgnoreActionCard(value: true, index: 0)
            
            if tzCards[index].negative_action_card_id != nil {
                if ipnForCurrentCard[Card_Action.NEGATIVE.rawValue] != nil {
                    let pos = getCardByID(cardId: ipnForCurrentCard[Card_Action.NEGATIVE.rawValue]!)
                    tzCards.remove(at: index)
                    tzCards.insert(pos.0!, at: isPushCardAdded ? index+1 : index)
                    isPushCardAdded = false
                    removeNegativeActionCard(value: false, index: pos.1)
                } else {
                    tzCards.remove(at: index)
                }
            } else {
                tzCards.remove(at: index)
            }
        default:
            removeIgnoreActionCard(value: true, index: 0)
            removeNegativeActionCard(value: true, index: 0)
            
            if tzCards[index].positive_action_card_id != nil {
                if ipnForCurrentCard[Card_Action.POSITIVE.rawValue] != nil {
                    let pos = getCardByID(cardId: ipnForCurrentCard[Card_Action.POSITIVE.rawValue]!)
                    tzCards.remove(at: index)
                    tzCards.insert(pos.0!, at: isPushCardAdded ? index+1 : index)
                    isPushCardAdded = false
                    removePositiveActionCard(value: false, index: pos.1)
                } else {
                    tzCards.remove(at: index)
                }
            } else {
                tzCards.remove(at: index)
            }
        }
    }
    func removePositiveActionCard(value: Bool, index: Int) {
        if let index1 = ipnForCurrentCard[Card_Action.POSITIVE.rawValue] {
            if value {
                removeCardRecursively(cardId: index1)
            } else {
                tzCards.remove(at: index)
            }
        }
    }
    func removeNegativeActionCard(value: Bool, index: Int) {
        if let index1 = ipnForCurrentCard[Card_Action.NEGATIVE.rawValue] {
            if value {
                removeCardRecursively(cardId: index1)
            } else {
                tzCards.remove(at: index)
            }
        }
    }
    func removeIgnoreActionCard(value: Bool, index: Int) {
        if let index1 = ipnForCurrentCard[Card_Action.IGNORE.rawValue] {
            if value {
                removeCardRecursively(cardId: index1)
            } else {
                tzCards.remove(at: index)
            }
        }
    }
    func checkForPushCards(index: Int) {
        for (j,i) in pushCards.enumerated() {
            if let is_waiting = i.is_waiting {
                if is_waiting == 0 {
                    tzCards.insert(i, at: index + 1)
                    pushCards.remove(at: j)
                    isPushCardAdded = true
                } else if let waiting_card_type = i.waiting_card_type, let waiting_action_type = i.waiting_action_type, let mainCardType = tzCards[index].type {
                    if waiting_action_type == actionPerformedOnCard && waiting_card_type == mainCardType {
                        tzCards.insert(i, at: index + 1)
                        pushCards.remove(at: j)
                        isPushCardAdded = true
                    }
                }
            }
        }
    }
    // check for waiting cards in main card list
    func checkWaitingCards() {
        var indexesToRemove = [Int]()
        for (j,i) in tzCards.enumerated() {
            if let is_waiting = i.is_waiting {
                if is_waiting == 1 {
                    pushCards.append(i)
                    indexesToRemove.append(j)
                }
            }
        }
        tzCards.remove(at: indexesToRemove)
    }
    func setParametersToSingleton() {
        for i in 0..<tzCards.count {
            ShareParameters.shared.allParameters = tzCards[i].parameters
            if ShareParameters.shared.allParameters != nil {
                break
            }
        }
    }
}
// MARK: - Local Storage
extension HomeViewModel {
    func saveCardsToUserDefaults(json: JSON) {
        UserDefaults.standard.TZCards = json.rawString() ?? "[]"
    }
    func loadLocalData(completion: (_ success: Bool) -> Void) {
        let p = UserDefaults.standard.TZCards
        if p != "" {
            if let json = p.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                do {
                    tzCards = []
                    let json = try JSON(data: json)
                    let cards = json.arrayValue
                    for i in cards {
                        let card = TZCard(dictionary: i.dictionaryObject! as NSDictionary)
                        self.tzCards.append(card!)
                    }
                    completion(true)
                } catch {
                    completion(false)
                }
            } else {
                completion(false)
            }
        } else {
            completion(false)
        }
    }
}
