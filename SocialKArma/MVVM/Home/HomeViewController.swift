//
//  HomeViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import ContactsUI
import FAPanels
import Koloda
import SwiftyJSON
import Crashlytics
import FittedSheets
import FirebaseAnalytics

class HomeViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var cardsCollectionView: UICollectionView!
    @IBOutlet weak var kolodaView: KolodaView!
    @IBOutlet weak var contactInfoView: UIView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var karmaPointsButton: UIButton!
    @IBOutlet weak var kolodaViewHeight: NSLayoutConstraint!
    @IBOutlet weak var notificationBadge: UILabel!
    @IBOutlet weak var chatNotificationBadge: UILabel!
    @IBOutlet weak var searchBtn: UIButton!
    // Search View
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchImage: UIImageView!
    // Negative state view
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var noDataActionButton: UIButton!
    @IBOutlet weak var searchStatusBtn: UIButton!

    // MARK: - Variables
    let viewModel = HomeViewModel()
    var isSearchOn = false
    var isSubmitTapped = false
    var coachMarkToShow = CoachMarksTypes.None
    var cardId = ""
    
    var apiCallCount = 0
    fileprivate var screenState = HomeScreenStates.allGood
    var isCoachMarkShown = false
    // MARK: - Life Cycle
    /**
    Life Cycle method called when view is loaded.
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModel.controller = self
        configureView()
        // Adding observers
        addNotifications()
        if !UserDefaults.standard.isCoachMarkDone {
            UserDefaults.standard.isCoachMarkDone = UserDetails.shared.isComingFromSignIn == 1 ? false : true
        }
        coachMarkToShow = UserDefaults.standard.isCoachMarkDone ? CoachMarksTypes.None : CoachMarksTypes.SwipeUp
        setUpNavBar()
    }
    /**
    Life Cycle method called when view is loaded and about to appear on the screen.
    */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

//        PointsHandler.shared.flashButton(button: karmaPointsButton)
//        karmaPointsButton.setTitle("\(PointsHandler.shared.points)", for: .normal)

        // Calling feedback card API
        if viewModel.shouldCallCardsApiOnAppear {
            viewModel.isShowMore = false
            getRatingCards(query: 0)
        } else {
            viewModel.shouldCallCardsApiOnAppear = true
            if viewModel.tzCards.count == 0 {
                getRatingCards(query: 0)
            } else {
                kolodaView.resetCurrentCardIndex()
            }
        }
        
        // Setting notification and chat badges
        setNotificationBadges()
        
        if viewModel.notificationTypeToBeNil {
            viewModel.notificationTypeToBeNil = false
            viewModel.notificationType = "default"
        }
        
        // Search status
        searchStatusBtn.tintColor = (UserDefaults.standard.SearchStatus["friendStatus"] as! Int == 1 || UserDefaults.standard.SearchStatus["professionalStatus"] as! Int == 1) ? .green : .red

    }
    /**
     Life cycle method when view has appeared fully on screen
     */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        PointsHandler.shared.flipCoin(button: karmaPointsButton)
        
        // checking for coachmarks
        if !UserDefaults.standard.isFeedbackCoachMarkShown {
            UserDefaults.standard.isFeedbackCoachMarkShown = true
            isCoachMarkShown = true
            let vc = InfoCardsViewController.instantiate(fromAppStoryboard: .Main)
            vc.modalPresentationStyle = .fullScreen
            vc.completion = {
                self.isCoachMarkShown = false
            }
            vc.viewModel.screenType = 0
            present(vc, animated: false, completion: nil)
        }
        // checking if coachmarks are shown, if yes, show contact permission alert
        if !UserContacts.sharedInstance.isContactSyncDone && isCoachMarkShown == false {
            UserContacts.sharedInstance.isContactSyncDone = true
            DispatchQueue.global().asyncAfter(deadline: .now() + 2.0) {
                PhoneContacts.requestAccess { (granted) in
                    if granted {
                        self.viewModel.loadContacts { }
                    }
                }
            }
        }
        // Screen tracking
        Analytics.logEvent("screen_tracking", parameters: [
            "name": "TabClass" as NSObject,
            "full_text": "HomeViewController" as NSObject
        ])
    }
    /**
    Life cycle method when view is about to disappear from screen
    */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.shouldCallCardsApiOnAppear = false
    }
    /**
    Life cycle method when view are laid out on screen after calculating their position and size
    */
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        searchView.setUpBorder(width: 0.5, color: UIColor.NewTheme.darkGray, radius: 8)
    }
    /**
    Extension method for observing change in GCoins for the user
     
     To be removed in the future
    */
    override func pointsChanged(notification: Notification) {
        super.pointsChanged(notification: notification)
        let userInfo = notification.userInfo
        if let points = userInfo?["points"] as? Int {
            karmaPointsButton.setTitle("\(points)", for: .normal)
        }
        PointsHandler.shared.flashButton(button: self.karmaPointsButton)
    }
    /**
    Adding observers so that any actions that are not in the Navigation hierarchy can be handled.
    */
    func addNotifications() {
//        NotificationCenter.default.addObserver(self, selector: #selector(pointsChanged(notification:)), name: Notification.Name.Points.didChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reviewBack(notification:)), name: Notification.Name.AppNotifications.ReviewBack, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selfReview(notification:)), name: Notification.Name.AppNotifications.SelfReview, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(searchStatus(notification:)), name: Notification.Name.SearchStatus, object: nil)

    }
    /**
     Destructor method when the class if removed from memory
     */
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    /**
    Setting up ui of the app and adding targets to different controls that needs to be done after view has loaded
    */
    func configureView() {
        kolodaView.dataSource = self
        kolodaView.delegate = self
        notificationButton.applyTemplate(image: UIImage(named: "Notification"), color: UIColor.NewTheme.darkGray)
        notificationBadge.layer.cornerRadius = notificationBadge.frame.width/2
        notificationBadge.clipsToBounds = true
        chatNotificationBadge.layer.cornerRadius = chatNotificationBadge.frame.width/2
        chatNotificationBadge.clipsToBounds = true
        noDataActionButton.layer.cornerRadius = noDataActionButton.frame.height / 2
        
        searchBtn.rounded()
        searchBtn.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: searchBtn.frame.width/2, scale: true)

    }
    /**
     Updating points in the navbar
     
     To be removed in the future
     */
    func updatePoints() {
        let userInfo = UserDefaults.standard.userInfo
        let json = JSON(userInfo).dictionary
        if let points = json?["points"]?.int {
            karmaPointsButton.setTitle("\(points)", for: .normal)
        }
    }

    // MARK: - Actions
    @IBAction func sideMenuTapped(_ sender: Any) {
        if let panel = appDelegate?.window?.rootViewController as? FAPanelController {
            panel.openLeft(animated: true)
        }
    }
    @IBAction func searchTapped(_ sender: Any) {
        openSearch()
    }
    @IBAction func notificationTapped(_ sender: Any) {
        viewModel.shouldCallCardsApiOnAppear = false
        openNotificationFromNavBar()
    }
    @IBAction func pointsBtnTapped(_ sender: Any) {
        viewModel.shouldCallCardsApiOnAppear = false
        openPointsScreenFromNavBar()
    }
    @IBAction func searchIconTapped(_ sender: Any) {
        if isSearchOn {
            isSearchOn = false
            updateUIForSearch()
        } else {
            openSearch()
        }
    }
    /**
     Method for observing review back action from any other part of the app. e.g., from notitication, contact screen
     */
    @objc func reviewBack(notification: Notification) {
//        viewModel.searchedIndexPath = IndexPath(row: 0, section: 0)
//        viewModel.searchedContact = viewModel.phoneContacts[0]
//        isSearchOn = true
//        updateUIForSearch()
        self.tabBarController?.selectedIndex = 0
        let userInfo = notification.userInfo
        if (userInfo?["notif"] as? Bool) != nil {
            if let contact_user_id = userInfo?["contact_user_id"] as? Int {
                viewModel.isSearchOn = true
                viewModel.resetViewModelDataNew()
                getRatingCards(query: contact_user_id)
                viewModel.notificationType = userInfo?["notification_type"] as? String ?? ""
                viewModel.notificationTypeToBeNil = true
            }
        } else {
            if let cards = userInfo?["cards"] as? [TZCard] {
                viewModel.tzCards.insert(contentsOf: cards, at: 0)
                self.kolodaView.resetCurrentCardIndex()
            }
        }
    }
    /**
    Method for observing self review action from any other part of the app.
     
     To be removed in the future
    */
    @objc func selfReview(notification: Notification) {
        viewModel.getSelfAssessment(parameters: [:]) { (success, json, message) in
            if success {
                DispatchQueue.main.async {
                    self.kolodaView.resetCurrentCardIndex()
                }
            }
        }
    }
    /**
    Selector for observing changes in the user's active status for searching friends or professional jobs/works
    */
    @objc func searchStatus(notification: Notification) {
        searchStatusBtn.tintColor = (UserDefaults.standard.SearchStatus["friendStatus"] as! Int == 1 || UserDefaults.standard.SearchStatus["professionalStatus"] as! Int == 1) ? .green : .red
    }
    @IBAction func noDataBtnTapped(_ sender: Any) {
        if screenState == .noContactPermission {
            self.alert(message: Constants.ContactSync.rawValue)
            UserContacts.sharedInstance.loadContacts()
        } else if screenState == .noInternet {
            getRatingCards(query: 0)
        } else if screenState == .cardsCountZero {
            getRatingCards(query: 0)
        }
    }
    @IBAction func messageTapped(_ sender: Any) {
        openChatFromNavBar()
    }
    @IBAction func infoTapped(_ sender: Any) {
//        openInfoFromNavBar(info: Constants.FeedbackInfo.rawValue)
        openVideoPlayer()
//        openSocialSignIn()
    }
    @IBAction func activelyLookingTapped(_ sender: Any) {
        openActivelyLooking()
    }
    // MARK: - Helper Methods
    /**
     Checks cards for valid input for submitting either for submission or for moving to next card for same user.

     - Parameter cardType: Instance of Card, so that its type can be identified
     */
    func isValidForSubmit(cardType: UICollectionViewCell) -> Bool {
        if cardType is BaseReviewCell {
            if viewModel.goodnessScore == 0 {
                alert(message: "Please slide to a value on overall goodness before submitting")
                return false
            }
        } else if cardType is ParameterBaseCell {
            if viewModel.goodnessScore == 0 && viewModel.changedRatingsForParameterIDs.count == 0 {
                alert(message: "Please slide on at least one GoodSpace value before submitting")
                return false
            }
        } else if cardType is FeedbackCardCell {
            if viewModel.goodnessScore == 0 && viewModel.changedRatingsForParameterIDs.count == 0 && viewModel.feedback == "" {
                alert(message: "Please write something in the text box before submitting")
                return false
            }
        } else {
            if viewModel.goodnessScore == 0 && viewModel.changedRatingsForParameterIDs.count == 0 && viewModel.feedback == "" && (viewModel.relationship_id == 0 && viewModel.relationshipYears == 0 && viewModel.relationshipCloseness == 0) {
                alert(message: "Please provide input on at least one option before submitting")
                return false
            }
        }
        return true
    }
    /**
    Opens Contact search screen on button action or after api response
    */
    func openSearch() {
        let vc = ContactListViewController.instantiate(fromAppStoryboard: .Home)
        navigationController?.pushViewController(vc, animated: true)
//        (tabBarController as? TabBarController)?.isSearchTapped = true
//        tabBarController?.selectedIndex = 2
    }
    /**
    Updating karma points on the nav bar
     
     To be removed in the future
    */
    func karmaPoints() {
        let userInfo = UserDefaults.standard.userInfo
        let jhdbj = userInfo.data(using: .utf8)!
        do {
            let json = try JSON(data: jhdbj)
            if let karma_points = json["karma_points"].int {
                self.karmaPointsButton.setTitle("\(karma_points)", for: .normal)
            }
        } catch {
        }
    }
    /**
    Open Profiles for other User on tapping of profile picture or card click

    - Parameter index: Index of the clicked Card in the list of All cards (datasource).
    */
    func openOtherUsersProfile(index: Int) {
        if (viewModel.tzCards[index].userInfo?.status ?? 0) == 3 {
            if let id = viewModel.tzCards[index].target_user_id, let sourceId = viewModel.tzCards[index].source_user_id, id != sourceId {
                
                let vc = ProfileDetailsViewController.instantiate(fromAppStoryboard: .ProfileV2)
                vc.otherUserId = id
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    /**
    Open Coach Marks from cards

    - Parameter points: Array of CGPoint. Origin of all the points where coach marks need to be shown
    - Parameter type: Type of coachmark
    */
    func openCoachMarks(points: [CGPoint], type: Int) {
        if coachMarkToShow == .HighLow {
            let vc = CoachMarks2ViewController.instantiate(fromAppStoryboard: .Home)
            vc.points = points
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: false, completion: nil)
        } else if coachMarkToShow == .InviteAndSearch {
            let vc = CoachMarksViewController.instantiate(fromAppStoryboard: .Home)
            vc.points = points
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: false, completion: nil)
        } else if coachMarkToShow == .SwipeUp {
            let vc = CoachMarks3ViewController.instantiate(fromAppStoryboard: .Home)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: false, completion: nil)
        } else if coachMarkToShow == .Points {
            let vc = CoachMarks4ViewController.instantiate(fromAppStoryboard: .Home)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: false, completion: nil)
        }
        coachMarkToShow = .None
    }
    /**
    Opens Feedback templates to be selected from Detailed Feedback card.
     
     - Parameter card: Instance of the FeedbackCardCell.
     - Parameter index: Index of the clicked Card in the list of All cards (datasource).
    */
    func openFeedbackTemplate(card: FeedbackCardCell, index: Int) {
        view.endEditing(true)
        let vc = TextFeedbackSuggestionViewController.instantiate(fromAppStoryboard: .Home)
        vc.suggestions = viewModel.tzCards[index].suggestions
        vc.feedbackSelected = { feedback in
            card.textView.text = feedback
            self.viewModel.feedback = feedback
        }
        let sheetController = SheetViewController(controller: vc, sizes: [.fullScreen, .halfScreen])
        self.present(sheetController, animated: false, completion: nil)
    }
    /**
    Handling action on 3-Dot Menu button on all cards.
     
     - Parameter index: Index of the clicked Card in the list of All cards (datasource).
    */
    func handleMenuTap(index: Int) {
        alertWithTwoActionCustomTitles(message: "Do you want to delete this  number from our database?", okTitle: "Sure", cancelTitle: "No, Thanks", okHandler: {
            
            let params = ["id": self.viewModel.tzCards[index].target_user_id ?? 0]
            self.viewModel.deleteContact(parameters: params) { (success, json, message) in
                
                self.kolodaView.resetCurrentCardIndex()
            }
        }) {}
    }
    // MARK: - Negative State
    /**
    Checks for negative states e.g., No internet connection, No Cards on the list etc.
     
     - Parameter isCountZero: True if length of list of All cards (datasource) is zero.
    */
    func checkForContactPermission(isCountZero: Bool) {
        if isCountZero {
            PhoneContacts.requestAccess { (granted) in
                if granted {
                    self.screenState = .cardsCountZero
                } else {
                    self.screenState = .noContactPermission
                }
            }
        } else {
            screenState = .allGood
        }
        self.handleKolodaViewState()
    }
    /**
    Checks for negative states at any point in time e.g., No internet connection, No Cards on the list etc. and updates the view with a messge and action button accordingly.
     
     - Parameter isCountZero: True if length of list of All cards (datasource) is zero.
    */
    func handleKolodaViewState() {
        if screenState == .noInternet {
            noDataView.isHidden = false
            noDataLabel.text = "No Internet Connection!"
            noDataActionButton.setTitle("Try again", for: .normal)
            kolodaView.isHidden = true
        } else if screenState == .noContactPermission {
            noDataView.isHidden = false
            noDataLabel.text = "No contact permission!"
            noDataActionButton.setTitle("Resync Contact", for: .normal)
            kolodaView.isHidden = true
        } else if screenState == .cardsCountZero {
            noDataView.isHidden = false
            noDataLabel.text = "Oops! This should not have happened. Please try again to start giving feedback"
            noDataActionButton.setTitle("Try again", for: .normal)
            kolodaView.isHidden = true
        } else {
            noDataView.isHidden = true
            kolodaView.isHidden = false
        }
    }
    /**
    Enum for Different states of Home Screen.
    */
    fileprivate enum HomeScreenStates {
        
        // Device not connected to Internet.
        case noInternet
        
        // No cards to show
        case cardsCountZero
        
        // No contact permission given by user
        case noContactPermission
        
        // No negative states, cards can be rendered.
        case allGood
    }
}
// MARK: - SendData delegate
extension HomeViewController: SendData {
    /**
    SendData Delegate method for getting notified whenever any other type wants to send data to current class.
     
     - Parameter dataPasser: Instance of the type that wants to send data, or Simply user defined String value or type to identify different types
     - Parameter data: Actual data thaat needs to be transferred.
    */
    func passData(dataPasser: Any, data: Any) {
        if dataPasser is ContactListViewController {
            if let contact_id = data as? Int, contact_id != 0 {
                viewModel.isSearchOn = true
//                viewModel.resetViewModelData()
                viewModel.resetViewModelDataNew()
                getRatingCards(query: contact_id)
            }
        }
    }
    /**
    Updating UI after searching some user
     
     To be removed in the future.
    */
    func updateUIForSearch() {
        if isSearchOn {
            viewModel.indexPathBeforeSearch = IndexPath(row: kolodaView.currentCardIndex, section: 0) //prevIndexPath
            kolodaView.reloadData()
        } else {
            kolodaView.reloadData()
        }
    }
}
// MARK: - Web Services
extension HomeViewController {
    /**
    API calling function for getting list of cards
     
     Query Parameter here represents the UserId of the user for whom cards are to be fetched.
     
    - Parameter query: Flag to know if cards for a particular person need to be shown.
    */
    func getRatingCards(query: Int) {
        if RechabilityHandler.shared.reachability.connection == .unavailable {
            viewModel.loadLocalData { (success) in
                if success {
                    self.kolodaView.resetCurrentCardIndex()
                } else {
                    alert(message: Errors.NetworkNotAvailable.rawValue)
                    screenState = .noInternet
                    handleKolodaViewState()
                }
            }
        } else {
            if apiCallCount > 2 && UserDefaults.standard.isDemoPhoneNumber  {
                alert(message: Constants.DemoAccountAPICall)
            } else {
                var params = [String:Any]()
                if query != 0 {
                    params = ["id": query]
//                    params = ["target_user_id": query]
                }
                viewModel.getTZCards(parameters: params, query: query) { (success, json, message) in
                    self.checkForContactPermission(isCountZero: self.viewModel.tzCards.count == 0)
                    self.kolodaView.resetCurrentCardIndex()
                }
            }
            apiCallCount += 1
        }
    }
    /**
    API calling function for submitting feedback
          
     Not used for now. will be removed in future.
     Use ``` rateUserNew(index: Int) ```  instead.
     
    - Parameter index: Index of the clicked Card in the list of All cards (datasource).
    */
    func rateUser(index: Int) {
        if let _ = viewModel.tzCards[index].target_user_id {
            viewModel.createRequestModelFor(index: index)
            viewModel.rateUser(parameters: viewModel.requestModel) { (success, json, message) in
                if success {
                    self.showAutoToast(message: Constants.ReviewedSuccessfully.rawValue)
                    self.kolodaView.swipe(.up)
                } else {
                    DispatchQueue.main.async {
                        self.alert(message: message)
                    }
                }
            }
        } else {
            alert(message: Errors.TargetUserIdNull.rawValue)
        }
    }
    /**
    API calling function for submitting the every card swipe information to backend.
          
    - Parameter index: Index of the clicked Card in the list of All cards (datasource).
    */
    func reviewSwipe(index: Int) {
        let cardType = viewModel.tzCards[index].type ?? ""
        var cardId = 0
        switch cardType {
        case CardType.review.rawValue:
            cardId = viewModel.tzCards[index].user_card_lor_id ?? 0
        default:
            cardId = viewModel.tzCards[index].user_card_loa_id ?? 0
        }
        let params = ["cardId": cardId, "action": viewModel.actionPerformedOnCard, "cardType": cardType] as [String : Any]
        viewModel.reviewSwipe(parameters: params) { (_, _, _) in
        }
    }
    /**
    API calling function for submitting the every card swipe information to backend.
          
    - Parameter index: Index of the clicked Card in the list of All cards (datasource).
    */
    func rateUserNew(index: Int) {
        if let _ = viewModel.tzCards[index].target_user_id {
            viewModel.currentIndexForFeedback = index
            
            viewModel.createRequestModelForNew(index: index)
            
            let userNumber = viewModel.tzCards[index].userInfo?.mobile_number
            let userCode = viewModel.tzCards[index].userInfo?.country_code
            
            ShareParameters.shared.numberForSharing = "\(userCode ?? "+91")\(userNumber ?? "9999999999")"
            ShareParameters.shared.nameForSharing = viewModel.tzCards[index].userInfo?.name
            ShareParameters.shared.ratedParameters = viewModel.sliderValues
            ShareParameters.shared.feedback = viewModel.feedback
            ShareParameters.shared.overallScore = viewModel.goodnessScore
                        
            viewModel.rateUser(parameters: viewModel.requestModel) { (success, json, message) in
                if success {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.openShareReview()
                    }
                    self.kolodaView.swipe(.up)
                } else {
                    DispatchQueue.main.async {
                        self.alert(message: message)
                    }
                }
            }
        } else {
            alert(message: Errors.TargetUserIdNull.rawValue)
        }
    }
}
// MARK: - KolodaView Delegate methods
extension HomeViewController: KolodaViewDelegate {
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        if isSearchOn {
            isSearchOn = false
            kolodaView.reloadData()
        } else {
            getRatingCards(query: 0)
        }
    }
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [.up]
    }
    func kolodaShouldApplyAppearAnimation(_ koloda: KolodaView) -> Bool {
        return false
    }
    func kolodaSwipeThresholdRatioMargin(_ koloda: KolodaView) -> CGFloat? {
        return 0.2
    }
    func kolodaShouldTransparentizeNextCard(_ koloda: KolodaView) -> Bool {
        return false
    }
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        /// Remove all the linked cards and resetCurrentCardIndex
//        if RechabilityHandler.shared.reachability.connection == .unavailable {
//            koloda.revertAction()
//            alert(message: Errors.NetworkNotAvailable.rawValue)
//        } else {
            // for notifying db which action is performed.
            if viewModel.actionPerformedOnCard != Card_Action.NEGATIVE.rawValue {
                if viewModel.tzCards.count > 0 {
                    if RechabilityHandler.shared.reachability.connection == .unavailable { } else {
                        reviewSwipe(index: index)
                    }
                }
                // reset
                viewModel.resetViewModelDataNew()
            }
            if viewModel.tzCards.count > 0 {
                viewModel.renewCardDataAfterAction(index: index)
            }
//        }
    }
    func koloda(_ koloda: KolodaView, shouldSwipeCardAt index: Int, in direction: SwipeResultDirection) -> Bool {
        return direction == .up ? true : false
    }
    
    
}
extension HomeViewController: KolodaViewDataSource {
    
    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
        return isSearchOn ? 1 : viewModel.tzCards.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        
        if let type = viewModel.tzCards[index].type {
            switch type {
            case CardType.review.rawValue:
                return showBaseReviewCard(index: index)//cell
            case CardType.aux.rawValue:
                if let template = viewModel.tzCards[index].template {
                    if template == Card_Template.INFO_1.rawValue {
                        let cell = Bundle.main.loadNibNamed(CollectionViewCells.InfoCardCell.rawValue, owner: self, options: nil)?[0] as! InfoCardCell
                        cell.tzCard = viewModel.tzCards[index]
                        cell.didLayout = { points in
                            if self.coachMarkToShow == .Points {
                               self.openCoachMarks(points: points, type: 4)
                            }
                        }
                        return cell
                    } else if template == Card_Template.INFO_4.rawValue {
                        let cell = Bundle.main.loadNibNamed(CollectionViewCells.AuxCardCell.rawValue, owner: self, options: nil)?[0] as! AuxCardCell
                        cell.tzCard = viewModel.tzCards[index]
                        return cell
                    } else if template == Card_Template.INFO_5.rawValue {
                        let cell = Bundle.main.loadNibNamed(CollectionViewCells.LinkCardCell5.rawValue, owner: self, options: nil)?[0] as! LinkCardCell5
                        cell.tzCard = viewModel.tzCards[index]
                        return cell
                    } else if template == Card_Template.BACKGROUND_IMAGE_ONLY.rawValue {
                        let cell = Bundle.main.loadNibNamed(CollectionViewCells.BackgroundImageCell.rawValue, owner: self, options: nil)?[0] as! BackgroundImageCell
                        let url = viewModel.tzCards[index].background_image ?? ""
                        if url != "" {
                            let trimmed = url.trimmingCharacters(in: .whitespacesAndNewlines)
                            cell.bkgImageView.sd_setImage(with: URL(string: trimmed)!, completed: nil)
                        }
                        cell.didLayout = { points in
                            if self.coachMarkToShow == .SwipeUp {
                                self.openCoachMarks(points: points, type: 1)
                            }
                        }
                        return cell
                    } else {
                        let cell = Bundle.main.loadNibNamed(CollectionViewCells.PointsEarnedCell.rawValue, owner: self, options: nil)?[0] as! PointsEarnedCell
                       cell.tzCard = viewModel.tzCards[index]
                       
                       return cell
                    }
                } else {
                    return UIView()
                }
            default:
                let cell = Bundle.main.loadNibNamed(CollectionViewCells.WebCardCell.rawValue, owner: self, options: nil)?[0] as! WebCardCell
                return cell
            }
        } else {
            return UIView()
        }
    }
}

// MARK: - BaseReviewCell
extension HomeViewController {
    /**
    Showing different types of Feedback cards
          
    - Parameter index: Index of the clicked Card in the list of All cards (datasource).
    */
    func showBaseReviewCard(index: Int) -> UIView {
        switch viewModel.tzCards[index].subType {
        case CardSubtype.OVERALL.rawValue:
            let cell = Bundle.main.loadNibNamed( CollectionViewCells.BaseReviewCell.rawValue, owner: self, options: nil)?[0] as! BaseReviewCell
            cell.viewModel = viewModel
            cell.overAll = viewModel.tzCards[index].overall_count
            cell.userInfo = viewModel.tzCards[index].userInfo
            cell.contactUserId = viewModel.tzCards[index].target_user_id ?? 0
            cell.configureView()
            cell.callBack = {
                self.performNegativeAction()
            }
            cell.submit = {
                self.performPositiveAction(index: index, cardType: cell)
            }
            cell.editNameTap = {
                self.openEditName()
            }
            cell.profilePicTap = {
                self.openOtherUsersProfile(index: index)
            }
            cell.menuTap = {
                self.handleMenuTap(index: index)
            }
            return cell
        case CardSubtype.FEEDBACK.rawValue:
            let cell = Bundle.main.loadNibNamed("FeedbackCardCell", owner: self, options: nil)?[0] as! FeedbackCardCell
            cell.viewModel = viewModel
            cell.userInfo = viewModel.tzCards[index].userInfo
            cell.contactUserId = viewModel.tzCards[index].target_user_id ?? 0
            cell.configureView()
            cell.callBack = {
                self.performNegativeAction()
            }
            cell.submit = {
                self.performPositiveAction(index: index, cardType: cell)
            }
            cell.editNameTap = {
                self.openEditName()
            }
            cell.templateTap = {
                self.openFeedbackTemplate(card: cell, index: index)
            }
            cell.profilePicTap = {
                self.openOtherUsersProfile(index: index)
            }
            cell.menuTap = {
                self.handleMenuTap(index: index)
            }
            return cell
        case CardSubtype.RELATIONSHIP.rawValue:
            let cell = Bundle.main.loadNibNamed("RelationshipCardCell", owner: self, options: nil)?[0] as! RelationshipCardCell
            cell.viewModel = viewModel
            cell.tzCard = viewModel.tzCards[index]
            cell.userInfo = viewModel.tzCards[index].userInfo
            cell.contactUserId = viewModel.tzCards[index].target_user_id ?? 0
            cell.configureView()
            cell.callBack = {
                self.viewModel.resetViewModelDataNew()
                self.performNegativeAction()
            }
            cell.submit = {
                self.performPositiveAction(index: index, cardType: cell)
            }
            cell.editNameTap = {
                self.openEditName()
            }
            cell.profilePicTap = {
                self.openOtherUsersProfile(index: index)
            }
            cell.menuTap = {
                self.handleMenuTap(index: index)
            }
            return cell
        default:
            let cell = Bundle.main.loadNibNamed("ParameterBaseCell", owner: self, options: nil)?[0] as! ParameterBaseCell
            cell.viewModel = viewModel
            cell.tzCard = viewModel.tzCards[index]
            cell.collectionview.reloadData()
            cell.userInfo = viewModel.tzCards[index].userInfo
            cell.contactUserId = viewModel.tzCards[index].target_user_id ?? 0
            cell.configureView()
            cell.callBack = {
                self.performNegativeAction()
            }
            cell.submit = {
                self.performPositiveAction(index: index, cardType: cell)
            }
            cell.errorCallBack = { message in
                self.showAutoToast(message: message)
            }
            ShareParameters.shared.allParameters = viewModel.tzCards[index].parameters
            cell.editNameTap = {
                self.openEditName()
            }
            cell.profilePicTap = {
                self.openOtherUsersProfile(index: index)
            }
            cell.menuTap = {
                self.handleMenuTap(index: index)
            }
            return cell
        }
    }
    /**
    Opens Share review Pop up on after rateUser api success response, so that user can share the feedback to whatsapp.
    */
    func openShareReview() {
        let vc = SharePopUpViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalPresentationStyle = .overFullScreen
        vc.contactUserId = viewModel.tzCards[viewModel.currentIndexForFeedback].target_user_id ?? 0
        self.tabBarController?.present(vc, animated: false, completion: nil)
    }
    /**
    Perform Negative Action on any card and swipe the card programatically to remove it.
    */
    func performNegativeAction() {
        self.viewModel.actionPerformedOnCard = Card_Action.NEGATIVE.rawValue
        self.kolodaView.swipe(.up)
    }
    /**
    Perform Positive Action on any card and swipe the card programatically to remove it. and perform any following actions

    - Parameter index: Index of the clicked Card in the list of All cards (datasource).
    - Parameter cardType: Instance of Card, so that its type can be identified
    */
    func performPositiveAction(index: Int, cardType: UICollectionViewCell) {
        if RechabilityHandler.shared.reachability.connection == .unavailable {
            self.alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            if isValidForSubmit(cardType: cardType) {
                self.viewModel.actionPerformedOnCard = Card_Action.POSITIVE.rawValue
                self.rateUserNew(index: index)
                Analytics.logEvent("feedback_given", parameters: [
                "name": "notification_type" as NSObject,
                "full_text": viewModel.notificationType as NSObject
                ])
                /*
                 Bundle params = new Bundle();
                 params.putString("notification_type", notification_type);
                 App.mFirebaseAnalytics.logEvent("feedback_given", params);
                 */
            }
        }
    }
    /**
    Opens Edit name Pop up on clicking Edit button near Name on the card.
    */
    func openEditName() {
        let vc = EditNameViewController.instantiate(fromAppStoryboard: .Home)
        vc.name = viewModel.tzCards[0].userInfo?.name ?? ""
        vc.contactuserid = viewModel.tzCards[0].target_user_id ?? 0
        vc.nameEditComplete = { (name, contactuserid) in
            self.updateNameInDataSource(name: name, contactuserid: contactuserid)
        }
        vc.modalPresentationStyle = .overFullScreen
        self.tabBarController?.present(vc, animated: false, completion: nil)
    }
    /**
    Opens Edit name Pop up on clicking Edit button near Name on the card.
    */
    func updateNameInDataSource(name: String, contactuserid: Int) {
        viewModel.tzCards.forEach { (card) in
            if card.target_user_id == contactuserid {
                card.userInfo?.name = name
            }
        }
        kolodaView.resetCurrentCardIndex()
    }
}
// MARK: - NavBar
extension HomeViewController {
    // Notification Handling- To be moved to a separate class soon
    func setUpNavBar() {
        NotificationCenter.default.addObserver(self, selector: #selector(pushNotificationReceived(_:)), name: Notification.Name.PushReceived, object: nil)
    }
    @objc func pushNotificationReceived(_ notification: Notification) {
        if let type = notification.userInfo?["type"] as? String {
            if type == PushNotificationType.FEEDBACK_UE.rawValue || type == PushNotificationType.FEEDBACK_UE_2.rawValue || type == PushNotificationType.CONTACT_UE.rawValue || type == PushNotificationType.INVITE_UE.rawValue || type == PushNotificationType.PROFILE_UE.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_1.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_2.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_4.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_5.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_7.rawValue || type == PushNotificationType.UE_INFO_1.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_1.rawValue || type == PushNotificationType.UE_REINVITE_1.rawValue || type == PushNotificationType.UE_REINVITE_2.rawValue || type == PushNotificationType.CHAT_UNREAD_MESSAGES.rawValue {  }
            else if type == PushNotificationType.UE_FEEDBACK_REMINDER_3.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_6.rawValue {
                PointsHandler.shared.isFeedbackDetailNotification = true
                getRatingCards(query: 0)
            } else if type == PushNotificationType.CHAT.rawValue {
//                PointsHandler.shared.isChatNotificationBadge = true
//                chatNotificationBadge.isHidden = false
                setNotificationBadges()
            }
            else {
//                PointsHandler.shared.isNotification = true
//                notificationBadge.isHidden = false
                setNotificationBadges()
            }
        }
    }
    /**
    Setting badges on Notificaiton and Chat button to show number of unread notifications and numbers.
    */
    func setNotificationBadges() {
        PointsHandler.shared.getNotificationCount { notificationCountData in
            
            if let notifications = notificationCountData["notification"] as? Int, notifications > 0 {
                PointsHandler.shared.isNotification = true
                self.notificationBadge.isHidden = false
                self.notificationBadge.text = "\(notifications)"
            } else {
                self.notificationBadge.isHidden = PointsHandler.shared.isNotification ? false : true
            }
            if let chat = notificationCountData["chat"] as? Int, chat > 0 {
                PointsHandler.shared.isChatNotificationBadge = true
                self.chatNotificationBadge.isHidden = false
                self.chatNotificationBadge.text = "\(chat)"
            } else {
                self.chatNotificationBadge.isHidden = PointsHandler.shared.isChatNotificationBadge ? false : true
            }
        }
    }
}
