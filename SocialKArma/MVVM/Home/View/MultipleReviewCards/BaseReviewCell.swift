//
//  BaseReviewCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/05/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class BaseReviewCell: UICollectionViewCell {
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var shareBtn: UIButton!
    
    @IBOutlet weak var upBtn: UIButton!
    
    @IBOutlet weak var downBtn: UIButton!
    
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    
    
    
    
    var header: CardHeaderView!
    var callBack: completionBlock?
    var userInfo: UserInfo?
    var submit: completionBlock?
    weak var viewModel: HomeViewModel!
    var contactUserId = 0
    var editNameTap: completionBlock?
    var profilePicTap: completionBlock?
    var menuTap: completionBlock?
    var overAll: Overall?

    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCollectionView()
    }
    func configureView() {
        containerView.layer.cornerRadius = 12
        containerView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 0.5, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        // Header
        header = CardHeaderView.instantiateFromNib()
        headerView.insertSubview(header, at: 0)
        header.anchor(top: headerView.topAnchor, paddingTop: 0, bottom: headerView.bottomAnchor, paddingBottom: 0, left: headerView.leftAnchor, paddingLeft: 0, right: headerView.rightAnchor, paddingRight: 0, width: 0, height: 0)
        header.detailView.backgroundColor = UIColor.white
        header.detailView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        header.detailView.layer.cornerRadius = 12
        header.nameLabel.textColor = .darkGray
        header.reviewerLabel.textColor = .darkGray
        header.scoreLabel.textColor = .darkGray
        header.scoreStaticLabel.textColor = .darkGray
        header.locationLabel.isHidden = true
        header.distanceLabel.isHidden = true
        header.separatorView.backgroundColor = .darkGray
        header.stackView.spacing = screenWidth == 320 ? 6 : 8
        
        header.userImage.layer.cornerRadius = header.userImage.frame.height/2
//        header.inviteBtn.setImage(UIImage(named: "whatsapp_icon"), for: .normal)
        header.inviteBtn.layer.cornerRadius = 15
        header.shareBtn.isHidden = true
        
        submitBtn.layer.cornerRadius = 8
        shareBtn.layer.cornerRadius = 8
        
        if let status = userInfo?.status {
            header.buttonStack.isHidden = status == 3
            headerViewHeight.constant = status == 3 ? 180 : 210
        }
        
        header.userInfo = userInfo
        header.setMedals()
        header.contactUserId = contactUserId
        
        header.editNameTap = {
            self.editNameTap?()
        }
        header.picTap = {
            self.profilePicTap?()
        }
    }
    
    // MARK: - Actions
    @IBAction func upTapped(_ sender: Any) {
    }
    
    @IBAction func menuTapped(_ sender: Any) {
        menuTap?()
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        submit?()
    }
    
    
}
extension BaseReviewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private func configureCollectionView() {
        collectionView.registerFromXib(name: "RelationshipCardCell")
        collectionView.registerFromXib(name: "GoodnessCardCell")
        collectionView.registerFromXib(name: "FeedbackCardCell")
        collectionView.registerFromXib(name: "ParameterCell")
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            let cell: GoodnessCardCell = collectionView.dequeueReusableCell(for: indexPath)
            cell.viewModel = viewModel
            cell.overAll = overAll
            cell.callBack = {
                self.callBack?()
            }
            return cell
        case 1:
            let cell: ParameterCell = collectionView.dequeueReusableCell(for: indexPath)
            return cell
        case 2:
            let cell: FeedbackCardCell = collectionView.dequeueReusableCell(for: indexPath)
            return cell
        default:
            let cell: RelationshipCardCell = collectionView.dequeueReusableCell(for: indexPath)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return collectionView.frame.size
        
//        let width = screenWidth - 24
//        switch indexPath.row {
//        case 0:
//            return CGSize(width: width, height: 140) //GoodnessCardCell
//        case 0:
//            return CGSize(width: width, height: 170) //FeedbackCardCell
//        default:
//            return CGSize(width: width, height: collectionView.frame.height)
//        }
    }
    
}
