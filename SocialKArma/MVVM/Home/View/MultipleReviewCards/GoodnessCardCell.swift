//
//  GoodnessCardCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/05/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class GoodnessCardCell: UICollectionViewCell, ReusableView {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var slider: MultiColorSlider!
    
    @IBOutlet weak var valueLabel: UILabel!
    
    
    var callBack: completionBlock?
    weak var viewModel: HomeViewModel!
    var overAll: Overall?
//    var errorCallBack: ((_ message: String) ->Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = 12
        containerView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        slider.delegate = self
    }
    
    @IBAction func seeMoreTapped(_ sender: Any) {
        callBack?()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        if overAll?.count != 0 {
            slider.isBlocked = true
            slider.alpha = 0.5
        } else {
            slider.isBlocked = false
            slider.alpha = 1.0
        }
    }
}
// MARK: - MultiColorSlider Delegate
extension GoodnessCardCell: MultiColorSliderDelegate {
    func rangeSeekSlider(_ slider: MultiColorSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        let intValue = Int(maxValue)
        valueLabel.text = "\(intValue)/10"
    }
    func didEndTouches(in slider: MultiColorSlider) {
        let sliderValue = slider.selectedMaxValue
        let roundedValue = roundf(Float(sliderValue))
        let intValue = Int(roundedValue)
        viewModel.goodnessScore = intValue
    }
    func didStartTouches(in slider: MultiColorSlider) {
        if overAll?.count != 0 {
            window?.rootViewController?.showAutoToast(message: overAll?.message ?? "You can give overall feedback to _ after 7 more days")
        } else {
        }
    }
}
