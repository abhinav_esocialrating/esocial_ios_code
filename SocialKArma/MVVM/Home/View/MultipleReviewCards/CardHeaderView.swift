//
//  CardHeaderView.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 19/05/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class CardHeaderView: UIView {
    
    // MARK: - Outlets
    @IBOutlet weak var buttonStack: UIStackView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var scoreStaticLabel: UILabel!
    @IBOutlet weak var reviewerLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var inviteBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var medalStack: UIStackView!
    @IBOutlet weak var editNameBtn: UIButton!
    
    @IBOutlet weak var numberLabel: UILabel!
    // MARK: - Variables
    var userInfo: UserInfo? {
        didSet {
            setReviewValues()
        }
    }
    var picTap: completionBlock?
    var editNameTap: completionBlock?
    var contactUserId = 0
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        stackView.spacing = 8// screenWidth == 320 ? 8 : 13
    }
    func setReviewValues() {
        nameLabel.text = userInfo?.name
        numberLabel.text = (userInfo?.country_code ?? "") +  (userInfo?.mobile_number ?? " ")
        if let reviews = userInfo?.reviews {
            reviewerLabel.text = "Feedback: \(reviews)"
        }
        if let score = userInfo?.score {
            scoreLabel.text = String(format: "%.1f", score)
        } else {
            scoreLabel.text = "0.0"
        }
        if let url = userInfo?.image_id, url != "" {
            userImage.sd_setImage(with: URL(string: url)!, completed: nil)
        } else {
            userImage.image = UIImage(named: CommonData.avatars.randomElement() ?? "")
        }
    }

    @IBAction func picTapped(_ sender: Any) {
        picTap?()
    }
    
    @IBAction func whatsappBtnTapped(_ sender: Any) {
        if var number = userInfo?.mobile_number, number != "", let country_code = userInfo?.country_code {
            number = "\(country_code)\(number)"
            Utilities.inviteUsingWhatsapp(number: number, type: 2, name: userInfo?.name ?? "abc")
            NetworkManager.inviteCount(parameters: ["contactuserid": contactUserId], method: .post) { (_) in
            }
        } else {
            Utilities.inviteUsingWhatsapp(number: userInfo?.mobile_number ?? "", type: 1, name: "")
        }
    }
    
    @IBAction func editNameTapped(_ sender: Any) {
        editNameTap?()
    }
    
    func setMedals() {
        guard let medals = userInfo?.medals else {
            return
        }
        for (_,j) in medals.enumerated() {
            if let image_url = j.image_url, image_url != "" {
                
                let img = UIImageView()
                medalStack.addArrangedSubview(img)
                img.anchor(top: nil, paddingTop: 0, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, width: 20, height: 20)
                img.sd_setImage(with: URL(string: image_url), completed: nil)
            }
        }
    }
}
