//
//  ParameterCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/05/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ParameterCell: UICollectionViewCell, ReusableView {
    
    @IBOutlet weak var slider: MultiColorSlider!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!
    
    @IBOutlet weak var contentViwe: UIView!
    
    var ratingChanged: ((_ ratingValue: Int, _ ratingID: Int, _ indexPath: IndexPath, _ sliderExactValue: Int)->Void)? = nil
    
    var errorCallBack: ((_ message: String) ->Void)? = nil
    var viewModel: HomeViewModel!
    var parameter: RatingParameters!
//    {
//        didSet {
//            slider.selectedMinValue = 1
//            if let id = parameter.parameter_id, let exact = viewModel.sliderValues[id] {
//                slider.selectedMaxValue = CGFloat(exact)
//                valueLabel.text = "\(exact)/10"
//            } else {
//                slider.selectedMaxValue = 1
//                valueLabel.text = "1/10"
//            }
//            nameLabel.text = parameter.parameter ?? "Parameter"
//            if parameter.isBlocked != 0 {
//                slider.isBlocked = true
//                slider.alpha = 0.5
//            } else {
//                slider.isBlocked = false
//                slider.alpha = 1.0
//            }
//        }
//    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        slider.selectedMaxValue = 1
        slider.selectedMinValue = 1
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        slider.selectedMaxValue = 1
        slider.selectedMinValue = 1
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        setUpSlider()
        contentViwe.layer.cornerRadius = 12
        contentViwe.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
//        if let id = parameter.parameter_id, let exact = viewModel.sliderValues[id] {
//            slider.selectedMaxValue = CGFloat(exact)
//            valueLabel.text = "\(exact)/10"
//        } else {
//            slider.selectedMaxValue = 1
//            valueLabel.text = "1/10"
//        }
        slider.selectedMinValue = 1
        if parameter.isBlocked != 0 {
            slider.alpha = 0.5
        } else {
            slider.alpha = 1.0
        }
    }
    func setUpSlider() {
        slider.delegate = self
        slider.tintAdjustmentMode = .normal
    }
    func setValues() {
        slider.selectedMinValue = 1
        if let id = parameter.parameter_id, let exact = viewModel.sliderValues[id] {
            slider.selectedMaxValue = CGFloat(exact)
            valueLabel.text = "\(exact)/10"
        } else {
            slider.selectedMaxValue = 1
            valueLabel.text = "1/10"
        }
        slider.refresh()
        nameLabel.text = parameter.parameter ?? "Parameter"
        if parameter.isBlocked != 0 {
            slider.isBlocked = true
            slider.alpha = 0.5
        } else {
            slider.isBlocked = false
            slider.alpha = 1.0
        }
    }

}
// MARK: - MultiColorSlider Delegate
extension ParameterCell: MultiColorSliderDelegate {
    func didEndTouches(in slider: MultiColorSlider) {
        let sliderValue = slider.selectedMaxValue
        let roundedValue = roundf(Float(sliderValue))
        let intValue = Int(roundedValue)
        print(intValue)
//        let ratedValue = processSliderRating(value: intValue)
        ratingChanged?(intValue, parameter.parameter_id ?? 0, indexPath, intValue)
    }
    func didStartTouches(in slider: MultiColorSlider) {
        if parameter.isBlocked != 0 {
            errorCallBack?(parameter.message ?? "Error!")
        } else {
        }
    }
    func rangeSeekSlider(_ slider: MultiColorSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        valueLabel.text = "\(Int(maxValue))/10"
        let sliderValue = slider.selectedMaxValue
        let roundedValue = roundf(Float(sliderValue))
        let intValue = Int(roundedValue)
        ratingChanged?(intValue, parameter.parameter_id ?? 0, indexPath, intValue)
    }
}
