//
//  ParameterBaseCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 09/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import WKVerticalScrollBar

class ParameterBaseCell: UICollectionViewCell, ReusableView {
    
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var customScrollBar: WKVerticalScrollBar!
    // MARK: - Variables
    var tzCard: TZCard?
    var header: CardHeaderView!
    var userInfo: UserInfo?
    var submit: completionBlock?
    var callBack: completionBlock?
    weak var viewModel: HomeViewModel!
    var contactUserId = 0
    var errorCallBack: ((_ message: String) ->Void)? = nil
    var editNameTap: completionBlock?
    var profilePicTap: completionBlock?
    var menuTap: completionBlock?
    var isScrollFirstTime = true

    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCollectionView()
    }
    func configureView() { 
        containerView.layer.cornerRadius = 12
        containerView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 0.5, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        customScrollBar.scrollView = collectionview
        customScrollBar.handleSelectedWidth = 2
        customScrollBar.handleHitWidth = 2
        // Header
        header = CardHeaderView.instantiateFromNib()
        headerView.insertSubview(header, at: 0)
        header.anchor(top: headerView.topAnchor, paddingTop: 0, bottom: headerView.bottomAnchor, paddingBottom: 0, left: headerView.leftAnchor, paddingLeft: 0, right: headerView.rightAnchor, paddingRight: 0, width: 0, height: 0)
        header.detailView.backgroundColor = UIColor.white
        header.detailView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        header.detailView.layer.cornerRadius = 12
        header.nameLabel.textColor = .darkGray
        header.reviewerLabel.textColor = .darkGray
        header.scoreLabel.textColor = .darkGray
        header.scoreStaticLabel.textColor = .darkGray
        header.locationLabel.isHidden = true
        header.distanceLabel.isHidden = true
        header.separatorView.backgroundColor = .darkGray
        header.stackView.spacing = screenWidth == 320 ? 6 : 8
        
        header.userImage.layer.cornerRadius = header.userImage.frame.height/2
//        header.inviteBtn.setImage(UIImage(named: "whatsapp_icon"), for: .normal)
        header.inviteBtn.layer.cornerRadius = 15
        header.shareBtn.isHidden = true
        
        submitBtn.layer.cornerRadius = 8

        if let status = userInfo?.status{
            header.buttonStack.isHidden = status == 3
            headerViewHeight.constant = status == 3 ? 180 : 210
        }
        
        header.userInfo = userInfo
        header.setMedals()
        header.contactUserId = contactUserId
        
        header.editNameTap = {
            self.editNameTap?()
        }
        header.picTap = {
            self.profilePicTap?()
        }
    }
    func configureCollectionView() {
        collectionview.registerFromXib(name: "ParameterCell")
        collectionview.dataSource = self
        collectionview.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.75) {
            if self.collectionview != nil {//&& !self.isScrollFirstTime {
                self.collectionview.flashScrollIndicators()
            }
//            else {
//                self.isScrollFirstTime = false
//            }
        }
    }
    // MARK: - Actions
    @IBAction func seeMoreTapped(_ sender: Any) {
        callBack?()
    }
    @IBAction func submitTapped(_ sender: Any) {
        submit?()
    }
    @IBAction func menuTapped(_ sender: Any) {
        menuTap?()
    }
}
extension ParameterBaseCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tzCard?.parameters?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ParameterCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.indexPath = indexPath
        cell.viewModel = viewModel
        cell.parameter = tzCard?.parameters?[indexPath.row]
        cell.setValues()
        cell.ratingChanged = { (ratingValue, ratingId, index, exact) in
            if !self.viewModel.changedRatingsForParameterIDs.contains(ratingId) {
                self.viewModel.changedRatingsForParameterIDs.append(ratingId)
            }
            self.viewModel.sliderValues[ratingId] = ratingValue
            self.viewModel.sliderExactValues[ratingId] = exact
        }
        cell.errorCallBack = { message in
            self.errorCallBack?(message)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = screenWidth - 56
        return CGSize(width: width, height: 90)
    }
}
