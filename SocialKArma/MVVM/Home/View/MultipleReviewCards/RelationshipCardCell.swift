//
//  RelationshipCardCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/05/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown
import WKVerticalScrollBar

class RelationshipCardCell: UICollectionViewCell, ReusableView {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var relationshipTF: UITextField!
    @IBOutlet weak var closenessSlider: RangeSeekSlider!
    @IBOutlet weak var yearsSlider: RangeSeekSlider!
    @IBOutlet weak var closenessLabel: UILabel!
    @IBOutlet weak var yearsLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var customScrollBar: WKVerticalScrollBar!
    // MARK: - Variables
    var header: CardHeaderView!
    var userInfo: UserInfo?
    var submit: completionBlock?
    var callBack: completionBlock?
    var tzCard: TZCard?
    weak var viewModel: HomeViewModel!
    var contactUserId = 0
    var editNameTap: completionBlock?
    var profilePicTap: completionBlock?
    var menuTap: completionBlock?
    let dropDown = DropDown()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        view1.layer.cornerRadius = 12
        view1.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        view2.layer.cornerRadius = 12
        view2.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        view3.layer.cornerRadius = 12
        view3.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        closenessSlider.delegate = self
        yearsSlider.delegate = self
    }
    func configureView() {
        containerView.layer.cornerRadius = 12
        containerView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 0.5, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        customScrollBar.scrollView = scrollView
        customScrollBar.handleSelectedWidth = 2
        customScrollBar.handleHitWidth = 1
        // Header
        header = CardHeaderView.instantiateFromNib()
        headerView.insertSubview(header, at: 0)
        header.anchor(top: headerView.topAnchor, paddingTop: 0, bottom: headerView.bottomAnchor, paddingBottom: 0, left: headerView.leftAnchor, paddingLeft: 0, right: headerView.rightAnchor, paddingRight: 0, width: 0, height: 0)
        header.detailView.backgroundColor = UIColor.white
        header.detailView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        header.detailView.layer.cornerRadius = 12
        header.nameLabel.textColor = .darkGray
        header.reviewerLabel.textColor = .darkGray
        header.scoreLabel.textColor = .darkGray
        header.scoreStaticLabel.textColor = .darkGray
        header.locationLabel.isHidden = true
        header.distanceLabel.isHidden = true
        header.separatorView.backgroundColor = .darkGray
        header.stackView.spacing = screenWidth == 320 ? 6 : 8
        
        header.userImage.layer.cornerRadius = header.userImage.frame.height/2
//        header.inviteBtn.setImage(UIImage(named: "whatsapp_icon"), for: .normal)
        header.inviteBtn.layer.cornerRadius = 15
        header.shareBtn.isHidden = true
        
        submitBtn.layer.cornerRadius = 8
        
        if let status = userInfo?.status{
            header.buttonStack.isHidden = status == 3
            headerViewHeight.constant = status == 3 ? 180 : 210
        }

        header.userInfo = userInfo
        header.setMedals()
        header.contactUserId = contactUserId
        
        header.editNameTap = {
            self.editNameTap?()
        }
        header.picTap = {
            self.profilePicTap?()
        }
    }
    // MARK: - Actions
    @IBAction func seeMoreTapped(_ sender: Any) {
        callBack?()
    }
    @IBAction func submitTapped(_ sender: Any) {
        submit?()
    }
    @IBAction func menuTapped(_ sender: Any) {
        menuTap?()
    }
}
extension RelationshipCardCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        showDropDown()
        return false
    }
    func showDropDown() {
        dropDown.anchorView = relationshipTF
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = dropDown.anchorView?.plainView.frame.width
        dropDown.dataSource = tzCard?.relationships?.map({ (re) -> String in
            return (re.value ?? "")
        }) ?? []
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropSelection(index: index, item: item)
        }
        dropDown.show()
    }
    func handleDropSelection(index: Int, item: String) {
        relationshipTF.text = item
        viewModel.relationship_id = tzCard?.relationships?[index].id ?? 0
    }
}
// MARK: - MultiColorSlider Delegate
extension RelationshipCardCell: RangeSeekSliderDelegate {
    func didEndTouches(in slider: RangeSeekSlider) {
        let sliderValue = slider.selectedMaxValue
        let intValue = Int(sliderValue)
        if slider == closenessSlider {
            viewModel.relationshipCloseness = intValue
            closenessLabel.text = "\(intValue)/10"
        } else {
            viewModel.relationshipYears = intValue
            yearsLabel.text = "\(intValue)/30"
        }
    }
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        let sliderValue = slider.selectedMaxValue
        let intValue = Int(sliderValue)
        if slider == closenessSlider {
            viewModel.relationshipCloseness = intValue
            closenessLabel.text = "\(intValue)/10"
        } else {
            viewModel.relationshipYears = intValue
            yearsLabel.text = "\(intValue)/30"
        }
    }
}
