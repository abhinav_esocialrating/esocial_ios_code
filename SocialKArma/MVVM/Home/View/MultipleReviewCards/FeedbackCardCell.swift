//
//  FeedbackCardCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/05/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class FeedbackCardCell: UICollectionViewCell, ReusableView {
    
    @IBOutlet weak var feedBackContainerView: UIView!
    @IBOutlet weak var textViewContainer: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var templateBtn: UIButton!
    // MARK: - Variables
    var header: CardHeaderView!
    var userInfo: UserInfo?
    var submit: completionBlock?
    var callBack: completionBlock?
    var contactUserId = 0
    var templateTap: completionBlock?
    var editNameTap: completionBlock?
    var profilePicTap: completionBlock?
    var menuTap: completionBlock?
    weak var viewModel: HomeViewModel!
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        feedBackContainerView.layer.cornerRadius = 12
        feedBackContainerView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        textViewContainer.setUpBorder(width: 0.5, color: .lightGray, radius: 12)
        
        textView.delegate = self
    }
    func configureView() {
        containerView.layer.cornerRadius = 12
        containerView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 0.5, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        // Header
        header = CardHeaderView.instantiateFromNib()
        headerView.insertSubview(header, at: 0)
        header.anchor(top: headerView.topAnchor, paddingTop: 0, bottom: headerView.bottomAnchor, paddingBottom: 0, left: headerView.leftAnchor, paddingLeft: 0, right: headerView.rightAnchor, paddingRight: 0, width: 0, height: 0)
        header.detailView.backgroundColor = UIColor.white
        header.detailView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        header.detailView.layer.cornerRadius = 12
        header.nameLabel.textColor = .darkGray
        header.reviewerLabel.textColor = .darkGray
        header.scoreLabel.textColor = .darkGray
        header.scoreStaticLabel.textColor = .darkGray
        header.locationLabel.isHidden = true
        header.distanceLabel.isHidden = true
        header.separatorView.backgroundColor = .darkGray
        header.stackView.spacing = screenWidth == 320 ? 6 : 8
        
        header.userImage.layer.cornerRadius = header.userImage.frame.height/2
//        header.inviteBtn.setImage(UIImage(named: "whatsapp_icon"), for: .normal)
        header.inviteBtn.layer.cornerRadius = 15
        header.shareBtn.isHidden = true
        
        submitBtn.layer.cornerRadius = 8
        
        if let status = userInfo?.status{
            header.buttonStack.isHidden = status == 3
            headerViewHeight.constant = status == 3 ? 180 : 210
        }

        header.userInfo = userInfo
        header.setMedals()
        header.contactUserId = contactUserId
        
        header.editNameTap = {
            self.editNameTap?()
        }
        header.picTap = {
            self.profilePicTap?()
        }
    }
    // MARK: - Actions
    @IBAction func seeMoreTapped(_ sender: Any) {
        callBack?()
    }
    @IBAction func submitTapped(_ sender: Any) {
        submit?()
    }
    @IBAction func templateTapped(_ sender: Any) {
        templateTap?()
    }
    @IBAction func menuTapped(_ sender: Any) {
        menuTap?()
    }
}
extension FeedbackCardCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        viewModel.feedback = textView.text!
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let maxLength = 255
        let currentString: NSString = textView.text!.encodeEmoji as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text.encodeEmoji) as NSString
        return newString.length <= maxLength
    }
}
