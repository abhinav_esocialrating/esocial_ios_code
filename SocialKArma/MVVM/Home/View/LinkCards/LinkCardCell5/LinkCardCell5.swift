//
//  LinkCardCell5.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 11/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class LinkCardCell5: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var tzCard: TZCard!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        imgView.layoutSubviews()
        setValues()
    }
    func setValues() {
        label1.text = tzCard.primary_text
        label2.text = tzCard.secondary_text
        label3.text = tzCard.tertiary_text
        if let primary_text_color = tzCard.primary_text_color {
            label1.textColor = UIColor.colorFromHexString(primary_text_color)
        }
        if let secondary_text_color = tzCard.secondary_text_color {
            label2.textColor = UIColor.colorFromHexString(secondary_text_color)
        }
        if let tertiary_text_color = tzCard.tertiary_text_color {
            label3.textColor = UIColor.colorFromHexString(tertiary_text_color)
        }
        if let url = tzCard.background_image, url != "" {
            imgView.sd_setImage(with: URL(string: url), completed: nil)
        } else if var bkgColor = tzCard.background {
            if bkgColor.first == "#" {
                bkgColor.removeFirst()
            }
            imgView.backgroundColor = UIColor.colorFromHexString(bkgColor)
        } else {
            imgView.backgroundColor = UIColor.colorFromHexString("673AB7")
        }
        imgView.setUpBorder(width: 0.5, color: UIColor.NewTheme.darkGray, radius: 8)
    }

}
