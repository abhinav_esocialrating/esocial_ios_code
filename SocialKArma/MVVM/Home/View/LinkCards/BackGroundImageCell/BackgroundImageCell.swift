//
//  BackgroundImageCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 11/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class BackgroundImageCell: UICollectionViewCell {

    @IBOutlet weak var bkgImageView: UIImageView!
    
    var didLayout: ((_ origins: [CGPoint]) -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        bkgImageView.layoutSubviews()
        bkgImageView.setUpBorder(width: 0.5, color: UIColor.NewTheme.darkGray, radius: 8)
        didLayout?([])
    }

}
