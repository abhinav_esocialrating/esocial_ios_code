//
//  PointsEarnedCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class PointsEarnedCell: UICollectionViewCell {
    
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var bkgView: UIView!
    
    var tzCard: TZCard!
    var didLayout: ((_ origins: [CGPoint]) -> Void)? = nil
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        bkgView.layoutSubviews()
        bkgView.setUpBorder(width: 0.5, color: UIColor.NewTheme.darkGray, radius: 8)
        setValues()
        didLayout?([])
    }
    func setValues() {
        label1.text = tzCard.primary_text
        label2.text = tzCard.secondary_text
        label3.text = tzCard.tertiary_text
        if let start_color = tzCard.start_color, let middle_color = tzCard.middle_color, let end_color = tzCard.end_color {
            bkgView.createVerticalGradient(colors: [UIColor.colorFromHexString(end_color).cgColor, UIColor.colorFromHexString(middle_color).cgColor, UIColor.colorFromHexString(start_color).cgColor])
        }
        if let primary_text_color = tzCard.primary_text_color {
            label1.textColor = UIColor.colorFromHexString(primary_text_color)
        }
        if let secondary_text_color = tzCard.secondary_text_color {
            label2.textColor = UIColor.colorFromHexString(secondary_text_color)
        }
        if let tertiary_text_color = tzCard.tertiary_text_color {
            label3.textColor = UIColor.colorFromHexString(tertiary_text_color)
        }
    }
}
