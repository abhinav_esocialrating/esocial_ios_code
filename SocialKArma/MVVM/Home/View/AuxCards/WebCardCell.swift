//
//  WebCardCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 24/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import WebKit

class WebCardCell: UICollectionViewCell {

    @IBOutlet weak var webView: WKWebView!
    var url: String!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func loadWebView() {
        
        
        if url != "" {
//            let str = """
//<html><head><meta name="viewport" content="width=device-width, minimum-scale=0.1"><title>feature-screen.png (1858×3456)</title></head><body style="margin: 0px; background: #0e0e0e;"><img style="-webkit-user-select: none;margin: auto;cursor: zoom-in;" src="https://esocialbucket.s3.ap-south-1.amazonaws.com/cards/first_card.svg" width="\(screenWidth)" height="\(screenHeight - 48)"></body></html>
//"""
//            webView.loadHTMLString(str, baseURL: nil)
            
            let urlRequest = URLRequest(url: URL(string: url)!)
            webView.load(urlRequest)
            webView.scrollView.isScrollEnabled = false
        }
    }
}
