//
//  AuxCardCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 26/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class AuxCardCell: UICollectionViewCell {

    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bkgView: UIView!
    
    var tzCard: TZCard!
    
    var didLayout: ((_ origins: [CGPoint]) -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        imgView.layoutSubviews()
        setValues()
        didLayout?([])
    }
    func setValues() {
        if let url = tzCard.background_image, url != "" {
            imgView.sd_setImage(with: URL(string: url), completed: nil)
        } else if var bkgColor = tzCard.background {
            if bkgColor.first == "#" {
                bkgColor.removeFirst()
            }
            imgView.backgroundColor = UIColor.colorFromHexString(bkgColor)
        } else {
            imgView.backgroundColor = UIColor.colorFromHexString("673AB7")
        }
        imgView.setUpBorder(width: 0.5, color: UIColor.NewTheme.darkGray, radius: 8)
        if let primary_text_color = tzCard.primary_text_color {
            label2.textColor = UIColor.colorFromHexString(primary_text_color)
        }
        label2.text = tzCard.primary_text
    }

}
