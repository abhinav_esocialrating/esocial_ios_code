//
//  TextFeedbackSuggestionViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 24/07/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class TextFeedbackSuggestionViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var suggestions : Array<Suggestions>?
    var feedbackSelected: ((_ feedback: String) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
    func configureView() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.dataSource = self
        tableView.delegate = self
    }

    // MARK: - Navigation

    @IBAction func backTapped(_ sender: Any) {
        
    }
    

}
// MARK: - TableView methods
extension TextFeedbackSuggestionViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggestions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TextFeedbackSuggestionCell.rawValue, for: indexPath)
        cell.textLabel?.text = suggestions?[indexPath.row].text_feedback
        cell.textLabel?.textColor = .darkGray
        cell.textLabel?.numberOfLines = 0
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        feedbackSelected?(suggestions?[indexPath.row].text_feedback ?? "")
        dismiss(animated: false, completion: nil)
    }
    
}
