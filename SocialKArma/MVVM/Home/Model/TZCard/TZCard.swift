/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
import SDWebImage
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class TZCard {
	public var _id : String?
	public var user_card_lor_id : Int?
    public var user_card_loa_id : Int?
	public var card_id : String?
	public var source_user_id : Int?
	public var target_user_id : Int?
	public var weight : Int?
	public var status : String?
	public var rc_source : String?
	public var positive_action_card_id : String?
	public var ignore_action_card_id : String?
	public var negative_action_card_id : String?
	public var parameters : Array<RatingParameters>?
	public var userInfo : UserInfo?
    public var type : String?
    public var primary_text : String?
    public var secondary_text : String?
    public var title : String?
    public var background : String?
    public var display_value : String?
    public var start_color : String?
    public var middle_color : String?
    public var end_color : String?
    public var primary_text_color : String?
    public var secondary_text_color : String?
    public var tertiary_text_color : String?
    public var tertiary_text : String?
    public var waiting_card_type : String?
    public var waiting_action_type : String?
    public var template : String?
    public var is_waiting : Int?
    public var action : Action?
    public var background_image : String?
    public var card_code : String?
    public var image_url : String?
    public var overall_count : Overall?
    public var subType : String?
    public var relationships : Array<Relationships>?
    public var suggestions : Array<Suggestions>?
    
/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let TZCard_list = TZCard.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of TZCard Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [TZCard]
    {
        var models:[TZCard] = []
        for item in array
        {
            if (item as? NSDictionary) != nil {
                models.append(TZCard(dictionary: item as! NSDictionary)!)
            }
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let TZCard = TZCard(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: TZCard Instance.
*/
	required public init?(dictionary: NSDictionary) {

		_id = dictionary["_id"] as? String
		user_card_lor_id = dictionary["user_card_lor_id"] as? Int
        user_card_loa_id = dictionary["user_card_loa_id"] as? Int
		card_id = dictionary["card_id"] as? String
		source_user_id = dictionary["source_user_id"] as? Int
		target_user_id = dictionary["target_user_id"] as? Int
		weight = dictionary["weight"] as? Int
		status = dictionary["status"] as? String
		rc_source = dictionary["rc_source"] as? String
		positive_action_card_id = dictionary["positive_action_card_id"] as? String
		ignore_action_card_id = dictionary["ignore_action_card_id"] as? String
		negative_action_card_id = dictionary["negative_action_card_id"] as? String
        if let parameter = dictionary["parameters"] as? NSArray {
            parameters = RatingParameters.modelsFromDictionaryArray(array: parameter)
        }
        if let userInfos = dictionary["userInfo"] as? NSDictionary {
            userInfo = UserInfo.init(dictionary: userInfos)
        }
        type = dictionary["type"] as? String
        primary_text = dictionary["primary_text"] as? String
        secondary_text = dictionary["secondary_text"] as? String
        title = dictionary["title"] as? String
        background = dictionary["background"] as? String
        
        display_value = dictionary["display_value"] as? String
        start_color = dictionary["start_color"] as? String
        middle_color = dictionary["middle_color"] as? String
        end_color = dictionary["end_color"] as? String
        primary_text_color = dictionary["primary_text_color"] as? String
        secondary_text_color = dictionary["secondary_text_color"] as? String
        tertiary_text_color = dictionary["tertiary_text_color"] as? String
        tertiary_text = dictionary["tertiary_text"] as? String
        
        waiting_card_type = dictionary["waiting_card_type"] as? String
        template = dictionary["template"] as? String
        waiting_action_type = dictionary["waiting_action_type"] as? String
        is_waiting = dictionary["is_waiting"] as? Int
        if let actions = dictionary["action"] as? NSDictionary {
            action = Action.init(dictionary: actions)
        }
        background_image = dictionary["background_image"] as? String
//        if let url = dictionary["background_image"] as? String {
//            background_image = dictionary["background_image"] as? String
//            SDWebImageDownloader.init().downloadImage(with: URL(string: url), completed: nil)
//        } else {
//            background_image = dictionary["background_image"] as? String
//        }
        card_code = dictionary["card_code"] as? String
        image_url = dictionary["image_url"] as? String
//        if let url = dictionary["image_url"] as? String {
//            SDWebImageDownloader.init().downloadImage(with: URL(string: url), completed: nil)
//        }
        if (dictionary["overall_count"] != nil) { overall_count = Overall(dictionary: dictionary["overall_count"] as! NSDictionary) }
        subType = dictionary["subType"] as? String
        if (dictionary["relationships"] != nil) { relationships = Relationships.modelsFromDictionaryArray(array: dictionary["relationships"] as! NSArray) }
        if (dictionary["suggestions"] != nil) { suggestions = Suggestions.modelsFromDictionaryArray(array: dictionary["suggestions"] as! NSArray) }

	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self._id, forKey: "_id")
		dictionary.setValue(self.user_card_lor_id, forKey: "user_card_lor_id")
        dictionary.setValue(self.user_card_loa_id, forKey: "user_card_loa_id")
		dictionary.setValue(self.card_id, forKey: "card_id")
		dictionary.setValue(self.source_user_id, forKey: "source_user_id")
		dictionary.setValue(self.target_user_id, forKey: "target_user_id")
		dictionary.setValue(self.weight, forKey: "weight")
		dictionary.setValue(self.status, forKey: "status")
		dictionary.setValue(self.rc_source, forKey: "rc_source")
		dictionary.setValue(self.positive_action_card_id, forKey: "positive_action_card_id")
		dictionary.setValue(self.ignore_action_card_id, forKey: "ignore_action_card_id")
		dictionary.setValue(self.negative_action_card_id, forKey: "negative_action_card_id")
    dictionary.setValue(self.userInfo?.dictionaryRepresentation(), forKey: "userInfo")
        dictionary.setValue(self.primary_text, forKey: "primary_text")
        dictionary.setValue(self.secondary_text, forKey: "secondary_text")
        dictionary.setValue(self.title, forKey: "title")
        dictionary.setValue(self.background, forKey: "background")
        
        dictionary.setValue(self.display_value, forKey: "display_value")
        dictionary.setValue(self.start_color, forKey: "start_color")
        dictionary.setValue(self.middle_color, forKey: "middle_color")
        dictionary.setValue(self.end_color, forKey: "end_color")
        dictionary.setValue(self.primary_text_color, forKey: "primary_text_color")
        dictionary.setValue(self.secondary_text_color, forKey: "secondary_text_color")
        dictionary.setValue(self.tertiary_text_color, forKey: "tertiary_text_color")
        dictionary.setValue(self.background, forKey: "tertiary_text")
        dictionary.setValue(self.action?.dictionaryRepresentation(), forKey: "action")
        dictionary.setValue(self.background_image, forKey: "background_image")
        dictionary.setValue(self.card_code, forKey: "card_code")
        dictionary.setValue(self.image_url, forKey: "image_url")

		return dictionary
	}

}
