/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class UserInfo {
	public var name : String?
	public var image_id : String?
	public var score : Double?
	public var karma_points : Int?
    public var status : Int?
    public var reviews : Int?
    public var relationship : Int?
    public var lastReviewedOn : String?
    public var mobile_number : String?
    public var country_code : String?
    public var medals : Array<Medals>?

    
/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let userInfo_list = UserInfo.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of UserInfo Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [UserInfo]
    {
        var models:[UserInfo] = []
        for item in array
        {
            models.append(UserInfo(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let userInfo = UserInfo(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: UserInfo Instance.
*/
	required public init?(dictionary: NSDictionary) {

		name = dictionary["name"] as? String
		image_id = dictionary["image_id"] as? String
		score = dictionary["score"] as? Double
		karma_points = dictionary["karma_points"] as? Int
        status = dictionary["status"] as? Int
        reviews = dictionary["reviews"] as? Int
        relationship = dictionary["relationship"] as? Int
        lastReviewedOn = dictionary["lastReviewedOn"] as? String
        mobile_number = dictionary["mobile_number"] as? String
        country_code = dictionary["country_code"] as? String
        if let medalsArray = dictionary["medals"] as? NSArray {
            medals = Medals.modelsFromDictionaryArray(array: medalsArray)
        }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.image_id, forKey: "image_id")
		dictionary.setValue(self.score, forKey: "score")
		dictionary.setValue(self.karma_points, forKey: "karma_points")
        dictionary.setValue(self.status, forKey: "status")
        dictionary.setValue(self.reviews, forKey: "reviews")
        dictionary.setValue(self.relationship, forKey: "relationship")
        dictionary.setValue(self.lastReviewedOn, forKey: "lastReviewedOn")
        dictionary.setValue(self.mobile_number, forKey: "mobile_number")
        dictionary.setValue(self.country_code, forKey: "country_code")
        
		return dictionary
	}

}
