/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
import CoreData
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Medals {
	public var medal_type_id : Int?
	public var medal_name : String?
	public var medal_type : String?
	public var medal_id : Int?
	public var image_url : String?
    public var user_medal_id : Int?
    public var medal_info : String?
/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let medals_list = Medals.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Medals Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Medals]
    {
        var models:[Medals] = []
        for item in array
        {
            models.append(Medals(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let medals = Medals(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Medals Instance.
*/

	required public init?(dictionary: NSDictionary) {

        medal_type_id = dictionary["medal_type_id"] as? Int
		medal_name = dictionary["medal_name"] as? String
		medal_type = dictionary["medal_type"] as? String
        medal_id = dictionary["medal_id"] as? Int
		image_url = dictionary["image_url"] as? String
        user_medal_id = dictionary["user_medal_id"] as? Int
        medal_info = dictionary["medal_info"] as? String
	}

    
//    func update(with dictionary: [String: Any]) {
//
//        medal_type_id = dictionary["medal_type_id"] as? Int ?? 0
//        medal_name = dictionary["medal_name"] as? String
//        medal_type = dictionary["medal_type"] as? String
//        medal_id = dictionary["medal_id"] as? Int ?? 0
//        image_url = dictionary["image_url"] as? String
//        user_medal_id = dictionary["user_medal_id"] as? Int ?? 0
//        medal_info = dictionary["medal_info"] as? String
//    }
		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.medal_type_id, forKey: "medal_type_id")
		dictionary.setValue(self.medal_name, forKey: "medal_name")
		dictionary.setValue(self.medal_type, forKey: "medal_type")
		dictionary.setValue(self.medal_id, forKey: "medal_id")
		dictionary.setValue(self.image_url, forKey: "image_url")
        dictionary.setValue(self.user_medal_id, forKey: "user_medal_id")
        dictionary.setValue(self.medal_info, forKey: "medal_info")

		return dictionary
	}

}
