//
//  ShareParameters.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 17/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation

class ShareParameters: NSObject {
    // MARK: - Singleton instance
    struct Static {
        fileprivate static var instance: ShareParameters?
    }
    class var shared: ShareParameters {
        if Static.instance == nil {
            Static.instance = ShareParameters()
        }
        return Static.instance!
    }
    func dispose() {
        ShareParameters.Static.instance = nil
        print("Disposed Singleton instance")
    }
    private override init() {}
    // MARK: - Variables
    var nameForSharing: String?
    var numberForSharing: String?
    var allParameters: [RatingParameters]?
    var ratedParameters: [Int:Int]?
    var feedback: String?
    var overallScore: Int?
    
    func createParameterList() {
        var ratedStr = ""
        if ratedParameters?.count == 0 {
            if let overallScore = overallScore {
                ratedStr.append("\n ✴ *Overall Goodness: \(overallScore)/10 *")
            }
        }
        if let ratingParams = allParameters, let ratedParams = ratedParameters {
            for i in ratedParams {
                let ratedValue = i.value
                let ratingKey = i.key
                let parameter = ratingParams.first(where: { $0.parameter_id == ratingKey })
                let paramName = parameter?.parameter ?? ""
                ratedStr.append("\(CommonData.emojiCodes[ratingKey] ?? "") *\(paramName): \(ratedValue)/10 * \n\n")
            }
        }
        if let feedback = feedback, feedback != "" {
            ratedStr.append("\(CommonData.emojiCodes[103] ?? "") *Detailed Feedback:* \(feedback)")
        }
        ratedStr.append("\n\n")
        ratedStr.append("And I would be really happy to receive your feedback.✅ \n\n")
        print(ratedStr)
        shareUsingWhatsapp(values: ratedStr)
    }
    func shareUsingWhatsapp(values: String) {
        
        guard let name = nameForSharing, let number = numberForSharing else {
            return
        }
        if UserDefaults.standard.TZInviteLink != "" {
            var inviteUrl = ""
            inviteUrl = "Dear \(name) 🙋‍♀, \n\nI think you are one of the good people in my life. I would like to share my feedback about you. \n\nYou can see it below 😊 :\n\n" + values + UserDefaults.standard.TZInviteLink
//            var components = URLComponents()
//            components.scheme = "whatsapp"
//            components.host = "send"
//            components.path = ""
//            let query = URLQueryItem(name: "text", value: inviteUrl.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed))
//            let phone = URLQueryItem(name: "phone", value: number)
//            components.queryItems = number == "" ? [query] : [query, phone]
            
            let sjdb = inviteUrl.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            var fone = number
            if fone.count > 0 && fone.first == "+" {
                fone.removeFirst()
            }
            let urlStr = "whatsapp://send?text=" + sjdb + "&phone=" + fone
            if let url = URL(string: urlStr) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                UIApplication.shared.open(url, options: [:]) { (success) in
                    AppDebug.shared.consolePrint("Whatsapp share")
                    AppDebug.shared.consolePrint(success)
                }
            }
        }
    }
}
