//
//  SharePopUpViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 30/05/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
 
public class Relationship {
	public var relationship_id : Int?
	public var relationship_name : String?
	public var selected : Bool?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let relationship_list = Relationship.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Relationship Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Relationship]
    {
        var models:[Relationship] = []
        for item in array
        {
            models.append(Relationship(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let relationship = Relationship(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Relationship Instance.
*/
	required public init?(dictionary: NSDictionary) {

		relationship_id = dictionary["relationship_id"] as? Int
		relationship_name = dictionary["relationship_name"] as? String
		selected = dictionary["selected"] as? Bool
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.relationship_id, forKey: "relationship_id")
		dictionary.setValue(self.relationship_name, forKey: "relationship_name")
		dictionary.setValue(self.selected, forKey: "selected")

		return dictionary
	}

}
