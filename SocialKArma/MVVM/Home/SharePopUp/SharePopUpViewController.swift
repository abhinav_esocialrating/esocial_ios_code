//
//  SharePopUpViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 30/05/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class SharePopUpViewController: UIViewController {
    
    @IBOutlet weak var tickImage: UIImageView!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var tapView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var number = ""
    var contactUserId = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addTapGesture()
        configureView()
    }
    func configureView() {
        tickImage.applyTemplate(image: nil, color: .white)
        submitBtn.layer.cornerRadius = 8
        cancelBtn.setUpBorder(width: 0.8, color: UIColor.SearchColors.blue, radius: 8)
    }
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(mainViewTapped(_:)))
        tapView.addGestureRecognizer(tap)
    }
    // MARK: - Actions
    @objc func mainViewTapped(_ gesture:UIGestureRecognizer) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func submitTapped(_ sender: Any) {
        NetworkManager.inviteCount(parameters: ["contactuserid": contactUserId, "share": 1], method: .post) { (_) in
        }
        dismiss(animated: false, completion: nil)
        ShareParameters.shared.createParameterList()
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    

}
