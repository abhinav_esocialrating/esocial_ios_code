//
//  EditNameViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 26/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class EditNameViewController: UIViewController {
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var tapView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    
    var name = ""
    var contactuserid = 0
    var nameEditComplete: ((_ name: String, _ contactuserid: Int) -> Void)? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addTapGesture()
        nameTF.text = name
        saveBtn.layer.cornerRadius = saveBtn.frame.height/2
    }
    
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(mainViewTapped(_:)))
        tapView.addGestureRecognizer(tap)
    }
    
    // MARK: - Actions

    @IBAction func saveTappe(_ sender: Any) {
        //name contactuserid
        if nameTF.text! == "" {
            showAutoToast(message: "Please enter name!")
            return
        }
        let params = ["name": nameTF.text!, "contactuserid": contactuserid] as [String : Any]
        NetworkManager.contactEditName(parameters: params, method: .put) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("contactEditName")
            print(json)
            switch response.response?.statusCode {
            case 200:
                self.nameEditComplete?(self.nameTF.text!, self.contactuserid)
                self.dismiss(animated: true, completion: nil)
                print("success")
            default:
                print("failure")
                self.alert(message: "Something Went Wrong!")
            }
        }
    }
    @objc func mainViewTapped(_ gesture:UIGestureRecognizer) {
        dismiss(animated: false, completion: nil)
    }
   

}
