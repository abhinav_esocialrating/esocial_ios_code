//
//  SearchContainerViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 26/05/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import FAPanels
import FirebaseAnalytics

class SearchContainerViewController: UIViewController {
    
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var contactViewWidth: NSLayoutConstraint!
    @IBOutlet weak var searchViewWidth: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var indicatorViewLeading: NSLayoutConstraint!
    @IBOutlet weak var notificationBadge: UILabel!
    @IBOutlet weak var chatNotificationBadge: UILabel!
    @IBOutlet weak var karmaPointsButton: UIButton!
    @IBOutlet weak var searchStatusBtn: UIButton!

    // MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        scrollView.delegate = self
//        contactViewWidth.constant = screenWidth
        searchViewWidth.constant = screenWidth
        view.layoutIfNeeded()
        addControllers()
        configureView()
        setUpNavBar()
        NotificationCenter.default.addObserver(self, selector: #selector(searchStatus(notification:)), name: Notification.Name.SearchStatus, object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        notificationBadge.isHidden = PointsHandler.shared.isNotification ? false : true
//        chatNotificationBadge.isHidden = PointsHandler.shared.isChatNotificationBadge ? false : true
        tabBarController?.tabBar.isHidden = false
//        updatePoints()
        
//        if (tabBarController as? TabBarController)?.isSearchTapped ?? false {
//            (tabBarController as? TabBarController)?.isSearchTapped = true
//
//            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
//            indicatorViewLeading.constant = 0
//        }
        
        setNotificationBadges()
        
        searchStatusBtn.tintColor = (UserDefaults.standard.SearchStatus["friendStatus"] as! Int == 1 || UserDefaults.standard.SearchStatus["professionalStatus"] as! Int == 1) ? .green : .red

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        if !UserDefaults.standard.searchCoachMarks {
//            UserDefaults.standard.searchCoachMarks = true
//            openSearchCoachMarks()
//        } else {
//        }
        if !UserDefaults.standard.isFindCoachMarkShown {
            UserDefaults.standard.isFindCoachMarkShown = true
            let vc = InfoCardsViewController.instantiate(fromAppStoryboard: .Main)
            vc.modalPresentationStyle = .fullScreen
            vc.viewModel.screenType = 2
            present(vc, animated: false, completion: nil)
        }
        else {
            LocationManager.shared.callLocationUpdate()
        }
        PointsHandler.shared.flipCoin(button: karmaPointsButton)
        
        // Screen tracking
        Analytics.logEvent("screen_tracking", parameters: [
            "name": "TabClass" as NSObject,
            "full_text": "SearchContainerViewController" as NSObject
        ])
    }
    func configureView() {
        notificationBadge.layer.cornerRadius = notificationBadge.frame.width/2
        notificationBadge.clipsToBounds = true
        chatNotificationBadge.layer.cornerRadius = chatNotificationBadge.frame.width/2
        chatNotificationBadge.clipsToBounds = true
    }
    func addControllers() {
//        let contactVC = ContactListViewController.instantiate(fromAppStoryboard: .Home)
//        contactView.addSubview(contactVC.view)
//        contactVC.view.frame.size.height = screenHeight - 84
//        addChild(contactVC)
//        contactVC.didMove(toParent: self)
        
        let searchVC = SearchResultsViewController.instantiate(fromAppStoryboard: .Search)
        searchView.addSubview(searchVC.view)
        searchVC.view.frame.size.height = screenHeight - 44
        addChild(searchVC)
        searchVC.didMove(toParent: self)
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        indicatorViewLeading.constant = 0//screenWidth/2
    }

    
    // MARK: - Actions

    @IBAction func contactsTapped(_ sender: Any) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @IBAction func trustzeTapped(_ sender: Any) {
        scrollView.setContentOffset(CGPoint(x: screenWidth, y: 0), animated: true)
    }
    @IBAction func menuTapped(_ sender: Any) {
        if let panel = appDelegate?.window?.rootViewController as? FAPanelController {
            panel.openLeft(animated: true)
        }
    }
    @IBAction func notificationTapped(_ sender: Any) {           openNotificationFromNavBar()
    }
    @IBAction func pointsBtnTapped(_ sender: Any) {
        openPointsScreenFromNavBar()
    }
    
    @IBAction func messageTapped(_ sender: Any) {
        openChatFromNavBar()
    }
    @IBAction func infoTapped(_ sender: Any) {
//        openInfoFromNavBar(info: Constants.SearchInfo.rawValue)
        openVideoPlayer()
    }
    @IBAction func activelyLookingTapped(_ sender: Any) {
        openActivelyLooking()
    }
    @objc func searchStatus(notification: Notification) {
        searchStatusBtn.tintColor = (UserDefaults.standard.SearchStatus["friendStatus"] as! Int == 1 || UserDefaults.standard.SearchStatus["professionalStatus"] as! Int == 1) ? .green : .red
    }

    // MARK: - Methods
    func openSearchCoachMarks() {
        let vc = ProfileCoachMarkViewController.instantiate(fromAppStoryboard: .Profile)
        vc.modalPresentationStyle = .overFullScreen
        vc.type = 1
        vc.completion = {
            LocationManager.shared.callLocationUpdate()
        }
        present(vc, animated: false, completion: nil)
    }
}
extension SearchContainerViewController: UIScrollViewDelegate {
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        scrollEnded()
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollEnded()
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            scrollEnded()
        }
    }
    func scrollEnded() {
        if scrollView.contentOffset.x == 0 {
            indicatorViewLeading.constant = 0
        } else {
            indicatorViewLeading.constant = screenWidth/2
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}
// MARK: - NavBar
extension SearchContainerViewController {
    func setUpNavBar() {
        NotificationCenter.default.addObserver(self, selector: #selector(pushNotificationReceived(_:)), name: Notification.Name.PushReceived, object: nil)
    }
    @objc func pushNotificationReceived(_ notification: Notification) {
        if let type = notification.userInfo?["type"] as? String {
            if type == PushNotificationType.FEEDBACK_UE.rawValue || type == PushNotificationType.FEEDBACK_UE_2.rawValue || type == PushNotificationType.CONTACT_UE.rawValue || type == PushNotificationType.INVITE_UE.rawValue || type == PushNotificationType.PROFILE_UE.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_1.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_2.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_3.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_4.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_5.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_6.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_7.rawValue || type == PushNotificationType.UE_INFO_1.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_1.rawValue || type == PushNotificationType.UE_REINVITE_1.rawValue || type == PushNotificationType.UE_REINVITE_2.rawValue || type == PushNotificationType.CHAT_UNREAD_MESSAGES.rawValue {  }
                
                
            else if type == PushNotificationType.CHAT.rawValue {
//                PointsHandler.shared.isChatNotificationBadge = true
//                chatNotificationBadge.isHidden = false
                setNotificationBadges()
            }
            else {
//                PointsHandler.shared.isNotification = true
//                notificationBadge.isHidden = false
                setNotificationBadges()
            }
        }
    }
    func setNotificationBadges() {
        PointsHandler.shared.getNotificationCount { notificationCountData in
            
            if let notifications = notificationCountData["notification"] as? Int, notifications > 0 {
                PointsHandler.shared.isNotification = true
                self.notificationBadge.isHidden = false
                self.notificationBadge.text = "\(notifications)"
            } else {
                self.notificationBadge.isHidden = PointsHandler.shared.isNotification ? false : true
            }
            if let chat = notificationCountData["chat"] as? Int, chat > 0 {
                PointsHandler.shared.isChatNotificationBadge = true
                self.chatNotificationBadge.isHidden = false
                self.chatNotificationBadge.text = "\(chat)"
            } else {
                self.chatNotificationBadge.isHidden = PointsHandler.shared.isChatNotificationBadge ? false : true
            }
        }
    }
}
