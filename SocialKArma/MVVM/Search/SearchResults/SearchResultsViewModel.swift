//
//  SearchResultsViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 06/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class SearchResultsViewModel: NSObject {
    let tags = ["Friends", "Service Providers", "Professionals", "Freelancers", "Employees", "Others"]
    var selectedCell: Int!
    let text = ["Friends", "Freelancers", "Service Providers", "Employess", "Professionals", "Others"]
    let images = ["search-friend-icon", "Search-freelancer-icon", "search-professional-icon", "search-service-provider-icon", "Search-freelancer-icon", "search-social-icon"]
    var searchResults = [SearchResults?]()
    var page = 1
        
    func getSearchFiltered(parameters: [String:Any], filter: String, count: Bool, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchFiltered(parameters: parameters, filter: filter, count: false, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                let results = data.map { (res) -> SearchResults? in
                    let model = SearchResults(dictionary: res.dictionaryValue)
                    return model
                }
//                if results.count > 0 {
                if self.page == 1 {
                    self.searchResults = self.filterSearchForReportedUsers(data: results)
                } else if results.count == 0 {
                    self.searchResults = []
                } else {
                    self.searchResults.append(contentsOf: self.filterSearchForReportedUsers(data: results))
                }
//                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func filterSearchForReportedUsers(data: [SearchResults?]) -> [SearchResults?] {
        let reportedUsers = UserDefaults.standard.reportedUsers
        let filtered = data.filter({!reportedUsers.contains("\($0?.user_id ?? 0)")})
        return filtered
    }
}
