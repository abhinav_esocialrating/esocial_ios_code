//
//  SearchResultsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 06/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import FAPanels
import CoreLocation
import Koloda

class SearchResultsViewController: UIViewController {
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var karmaPointsButton: UIButton!
    @IBOutlet weak var notificationBadge: UILabel!
    @IBOutlet weak var kolodaView: KolodaView!
    @IBOutlet var permissionView: UIView!
    
    @IBOutlet weak var searchBtn: UIButton!
    // MARK: - Variables
    let viewModel = SearchResultsViewModel()
    var currentFilter = 100  // 100 implies name filter
//    var friendsFilter = 1    // 1: Similar interests  2: Dating
    var initialSearch = 0
    var initialSearchString = ""
    let reachability = try! Reachability()
    var isFilterApplied = false
    var filterParams = [String:Any]()
    var isFirstTimeSearch = true
    // MARK: - Life Cycle methods
    /**
    Life Cycle method called when view is loaded.
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
//        setUpNavBar()
        do{
          try reachability.startNotifier()
        }catch{
          print("could not start reachability notifier")
        }
        handleLocationPermission()
    }
    /**
    Life Cycle method called when view is loaded and about to appear on the screen.
    */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        notificationBadge.isHidden = PointsHandler.shared.isNotification ? false : true
//        updatePoints()
    }
    /**
    Life cycle method when view has appeared fully on screen
    */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !UserDefaults.standard.searchCoachMarks {
            UserDefaults.standard.searchCoachMarks = true
//            openSearchCoachMarks()
        } else {
            LocationManager.shared.callLocationUpdate()
        }
        PointsHandler.shared.flipCoin(button: karmaPointsButton)
    }
    /**
     Setting up ui of the app and adding targets to different controls that needs to be done after view has loaded
     */
    func configureView() {
        kolodaView.dataSource = self
        kolodaView.delegate = self
        
        searchTF.addTarget(self, action: #selector(searchEditingChanged(_:)), for: .editingChanged)
        notificationBadge.layer.cornerRadius = notificationBadge.frame.width/2
        notificationBadge.clipsToBounds = true
        searchView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        filterButton.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        searchView.layer.cornerRadius = 12
        filterButton.layer.cornerRadius = 12
        searchBtn.setUpBorder(width: 0.8, color: UIColor.SearchColors.blue, radius: 8)
    }
    /**
    Fetching and updating user's basic details from UserDefaults
    */
    func updatePoints() {
        let userInfo = UserDefaults.standard.userInfo
        let data = userInfo.data(using: .utf8)
        let json = JSON(data ?? Data())
        let dict = json.dictionaryObject
        if let points = dict?["karma_points"] as? Int {
            karmaPointsButton.setTitle("\(points)", for: .normal)
        }
    }
    // MARK: - Actions
    @IBAction func filterTapped(_ sender: Any) {
        openFilter()
    }
    @IBAction func menuTapped(_ sender: Any) {
        if let panel = appDelegate?.window?.rootViewController as? FAPanelController {
            panel.openLeft(animated: true)
        }
    }
    /**
     Selector for getting updates whenever text in the search box is changed
     */
    @objc func searchEditingChanged(_ textField: UITextField) {
//        if textField.text!.count != 0 {
//            getSearchData(index: 5, q: textField.text!, isTextSearch: true)
//        }
    }
    @IBAction func searchTapped(_ sender: Any) {
        if RechabilityHandler.shared.reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailableSearch.rawValue)
        } else {
            if searchTF.text! == "" {
                alert(message: "Please enter text")
            }else {
                searchTF.resignFirstResponder()
                viewModel.searchResults = []
                viewModel.page = 1
                if searchTF.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil {
                    getSearchData(index: 5, q: searchTF.text!, isTextSearch: true, number: searchTF.text!)
                } else {
                    getSearchData(index: 5, q: searchTF.text!, isTextSearch: true, number: "")
                }
            }
        }
    }
    /**
     Allow button tapped from negative state view for allowing location permission.
     */
    @IBAction func allowTapped(_ sender: Any) {
        let url = URL(string: UIApplication.openSettingsURLString)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func notificationTapped(_ sender: Any) {           openNotificationFromNavBar()
    }
    
    @IBAction func pointsBtnTapped(_ sender: Any) {
        openPointsScreenFromNavBar()
    }
}
// MARK: - Web services
extension SearchResultsViewController {
    /**
    API calling function for getting data that user has searched for
     
     Query Parameter here represents the UserId of the user for whom cards are to be fetched.
     
    - Parameter index: index of filter type array.
    - Parameter q: Query entered in the search box.
    - Parameter isTextSearch: Flag to check if the search is called when text changes or button clicks, true if triggered by text change
    - Parameter number: sent as param when user wants to search using phone number.
    */
    func getSearchData(index: Int, q: String, isTextSearch: Bool, number: String) {
        isFilterApplied = true
        var param = [String:Any]()
        param["type"] = filterTypes[index]
        param["latitude"] = LocationManager.shared.lastLocation?.coordinate.latitude
        param["longitude"] = LocationManager.shared.lastLocation?.coordinate.longitude
        param["pagenumber"] = viewModel.page
        param["size"] = 15
        
        if number != "" {
            param["number"] = number
        } else if isTextSearch {
            param["name"] = q
        }
        AppDebug.shared.consolePrint(param)
        viewModel.getSearchFiltered(parameters: param, filter: "friends", count: true) { (success, json, message) in
            if success {
                self.updateUIWithKoloda(success: self.viewModel.searchResults.count > 0)
//                self.updateUI(success: self.viewModel.searchResults.count > 0)
                if !isTextSearch {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        
                        if self.isFirstTimeSearch {
                            self.isFirstTimeSearch = false
                        } else {
                            self.view.endEditing(true)
                            self.openFilter()
                        }
                    }
                }
            } else {
                self.updateUIWithKoloda(success: false)
//                self.updateUI(success: false)
            }
        }
    }
    /**
    API calling function for getting data that user has searched for after applying filters
    */
    func getSearchDataFiltered() {
        var params = filterParams
        params["latitude"] = LocationManager.shared.lastLocation?.coordinate.latitude
        params["longitude"] = LocationManager.shared.lastLocation?.coordinate.longitude
        params["pagenumber"] = viewModel.page
        params["size"] = 15
        print(params)
        viewModel.getSearchFiltered(parameters: params, filter: "friends", count: false) { (success, json, message) in
            if success {
                self.updateUIWithKoloda(success: self.viewModel.searchResults.count > 0)
            } else {
                self.updateUIWithKoloda(success: false)
            }
        }
    }
}
// MARK: - Helper Methods
extension SearchResultsViewController {
    /**
     Updating whole UI after API call or Some other action
     
     Not used anymore, To be removed in the future
     
     - Parameter success: Parameter to know whether there is data present in the datasource or not. If count is zero, a label containing related error info is shown
     */
    func updateUI(success: Bool) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.isHidden = success ? false : true
            self.infoLabel.isHidden = success ? true : false
        }
    }
    /**
    Updating whole UI after API call or Some other action
    
    - Parameter success: Parameter to know whether there is data present in the datasource or not. If count is zero, a label containing related info is shown
    */
    func updateUIWithKoloda(success: Bool) {
        DispatchQueue.main.async {
            if self.viewModel.page > 1 {
                self.kolodaView.reloadData()
            } else {
                self.kolodaView.resetCurrentCardIndex()
            }
            self.kolodaView.isHidden = success ? false : true
            self.infoLabel.isHidden = success ? true : false
        }
    }
    /**
     Opens Filter screen according to the currently selected filter type
     */
    func openFilter() {
        FilterModel.shared.resetValues()
        switch currentFilter {
        case 100:
            print("currentFilter = 100, No filter selected. ")
//            alert(message: "Please select a Filter!")
            let vc = OthersFilterVC.instantiate(fromAppStoryboard: .Search)
            vc.passData = self
            navigationController?.pushViewController(vc, animated: true)
        case 0:
            let vc = DatingFilterVC.instantiate(fromAppStoryboard: .Search)
            vc.passData = self
            navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = SimilarInterestsFilterVC.instantiate(fromAppStoryboard: .Search)
            vc.passData = self
            vc.viewModel.filterType = "freelancers"
            vc.viewModel.categoryType = CategoryFilterType.freelancers.rawValue
            navigationController?.pushViewController(vc, animated: true)
        case 2:
            let vc = SimilarInterestsFilterVC.instantiate(fromAppStoryboard: .Search)
            vc.passData = self
            vc.viewModel.filterType = "serviceProviders"
            vc.viewModel.categoryType = CategoryFilterType.serviceProviders.rawValue
            navigationController?.pushViewController(vc, animated: true)
        case 4:
            let vc = SimilarInterestsFilterVC.instantiate(fromAppStoryboard: .Search)
            vc.passData = self
            vc.viewModel.filterType = "professionals"
            vc.viewModel.categoryType = CategoryFilterType.professionals.rawValue
            navigationController?.pushViewController(vc, animated: true)
        case 3:
            let vc = EmployeeFilterVC.instantiate(fromAppStoryboard: .Search)
            vc.passData = self
            navigationController?.pushViewController(vc, animated: true)
        default:
            let vc = OthersFilterVC.instantiate(fromAppStoryboard: .Search)
            vc.passData = self
            navigationController?.pushViewController(vc, animated: true)
        }
        view.endEditing(true)
    }
    
    /**
    Opens profile of the selected user
     
     Not used anymore, To be removed in the future
    
    - Parameter id: UserId of the user whose profile needs to be opened.
    */
    func openProfileV2(id: Int) {
        let vc = ProfileHomeViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.otherUserId = id
        vc.userType = 2
        ProfileV2Data.sharedInstance.userType = 2
        ProfileV2Data.sharedInstance.otherUserId = id
        navigationController?.pushViewController(vc, animated: true)
    }
    /**
    Opens profile of the selected user
    
    - Parameter id: UserId of the user whose profile needs to be opened.
    */
    func openProfileDetails(id: Int) {
        let vc = ProfileDetailsViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.otherUserId = id
        navigationController?.pushViewController(vc, animated: true)
    }
    /**
    Opens Filter screen by providing a Generic filter type screen
    
    - Parameter viewControllerClass: A generic type U for filter type class
    */
    func openFilter<U : UIViewController>(viewControllerClass : U.Type) {
        let vc = U.instantiate(fromAppStoryboard: .Search)
        navigationController?.pushViewController(vc, animated: true)
    }
    /**
    Opens Coach marks ny providing an overlay view
    */
    func openSearchCoachMarks() {
        let vc = ProfileCoachMarkViewController.instantiate(fromAppStoryboard: .Profile)
        vc.modalPresentationStyle = .overFullScreen
        vc.type = 1
        vc.completion = {
            LocationManager.shared.callLocationUpdate()
        }
        present(vc, animated: false, completion: nil)
    }
    /**
    Opens Chat window for the currently selected user.
    */
    func openChat() {
        let index = kolodaView.currentCardIndex
        let user_id = viewModel.searchResults[index]?.user_id ?? 0
        let name = viewModel.searchResults[index]?.name ?? "NA"
        let image_id = viewModel.searchResults[index]?.image_id ?? ""
        
        let vc = ChatViewController.instantiate(fromAppStoryboard: .Chat)
        SocketIOManager.shared.toUserId = "\(user_id)"
        let chatUser = ChatUser(dictionary: ["name":name, "image_id":image_id] as NSDictionary)
        vc.viewModel.chatUser = chatUser
        
        navigationController?.pushViewController(vc, animated: true)
    }
    /**
    Handling various actions performed by the user by buttons present on the search card UI, e.g., opening detailed profile, opening chat window etc
    */
    func handleSearchCardActions(data: Any) {
        // profile message
        if let data = data as? String {
            switch data {
            case "profile":
                if RechabilityHandler.shared.reachability.connection == .unavailable {
                    alert(message: Errors.NetworkNotAvailable.rawValue)
                } else {
//                    openProfileV2(id: viewModel.searchResults[kolodaView.currentCardIndex]?.user_id ?? 0)
                    openProfileDetails(id: viewModel.searchResults[kolodaView.currentCardIndex]?.user_id ?? 0)
                }
            default:
                openChat()
            }
        }
    }
    /**
    Handling of location permission callbacks and showing the negative state if permission is not given by the user.
    */
    func handleLocationPermission() {
        changedLocationPermission = { isAllowed in
            if isAllowed {
                self.hidePermissionView()
                self.getSearchData(index: 5, q: "", isTextSearch: false, number: "")
            } else {
                self.addPermissionView()
            }
        }
        firstTimeSearch = {
            self.getSearchData(index: 5, q: "", isTextSearch: false, number: "")
        }
    }
    /**
    Showing a view for asking user to allow location permission by taking him to iPhone Setting app, where he can change the permissions.
    */
    func addPermissionView() {
        view.addSubview(permissionView)
        permissionView.anchor(top: view.topAnchor, paddingTop: 0, bottom: view.bottomAnchor, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 0, right: view.rightAnchor, paddingRight: 0, width: 0, height: 0)
    }
    /**
    Hiding the permission request view.
    */
    func hidePermissionView() {
        permissionView.removeFromSuperview()
    }
    /**
    Revert a swipe action for the Card by pressing a button.
    */
    func swipeBack() {
        if kolodaView.currentCardIndex != 0 {
            kolodaView.revertAction(direction: .up)
            kolodaView.revertAction()
        } else {
            AppDebug.shared.consolePrint("Please perform an action first!")
        }
    }
}
// MARK: - CollectionView methods
extension SearchResultsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.text.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCells.SearchSuggestionsCell.rawValue, for: indexPath) as! SearchSuggestionsCell
       cell.setValues(text: viewModel.text[indexPath.row], imageName: viewModel.images[indexPath.row], isCellSelected: viewModel.selectedCell == indexPath.row)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            isFilterApplied = false
            viewModel.page = 1
            viewModel.selectedCell = indexPath.row
            collectionView.reloadData()
            currentFilter = indexPath.row
            getSearchData(index: indexPath.row, q: "", isTextSearch: false, number: "")
            searchTF.text = ""
        }
    }
}
// MARK: - KolodaView datasource
extension SearchResultsViewController: KolodaViewDataSource, KolodaViewDelegate {
    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
        return viewModel.searchResults.count
    }
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let cell = SearchCard.instantiateFromNib()
        cell.configureView()
        cell.currentFilter = currentFilter
        cell.searchResult = viewModel.searchResults[index]
        cell.delegate = self
        cell.swipeBack = {
            self.swipeBack()
        }
        return cell
    }
    // MARK: - KolodaView delegate
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        // call pagination
//        if index >= viewModel.searchResults.count - 5 {
        if RechabilityHandler.shared.reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailableSearch.rawValue)
        } else {
//            if (viewModel.searchResults.count % 15) == 0 {
            viewModel.page += 1
//            }
            if isFilterApplied {
                getSearchDataFiltered()
            } else {
                getSearchData(index: currentFilter, q: "", isTextSearch: false, number: "")
            }
        }
    }
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [.up]
    }
    func kolodaShouldApplyAppearAnimation(_ koloda: KolodaView) -> Bool {
        return false
    }
    func kolodaSwipeThresholdRatioMargin(_ koloda: KolodaView) -> CGFloat? {
        return 0.3
    }
    func kolodaShouldTransparentizeNextCard(_ koloda: KolodaView) -> Bool {
        return false
    }
    func koloda(_ koloda: KolodaView, shouldSwipeCardAt index: Int, in direction: SwipeResultDirection) -> Bool {
        return direction == .up ? true : false
    }
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        
    }
}
// MARK: - SendData Delegates
extension SearchResultsViewController: SendData {
    /**
    SendData Delegate method for getting notified whenever any other type wants to send data to current class.
     
     - Parameter dataPasser: Instance of the type that wants to send data, or Simply user defined String value or type to identify different types
     - Parameter data: Actual data thaat needs to be transferred.
    */
    func passData(dataPasser: Any, data: Any) {
        if dataPasser is SearchCard {  // SearchSwipeCell
            handleSearchCardActions(data: data)
        } else {
            isFilterApplied = true
            filterParams = data as! [String:Any]
            viewModel.page = 1
            getSearchDataFiltered()
        }
    }
}
// MARK: - NavBar
extension SearchResultsViewController {
    // Notification Handling- To be moved to a separate class soon
    func setUpNavBar() {
        NotificationCenter.default.addObserver(self, selector: #selector(pushNotificationReceived(_:)), name: Notification.Name.PushReceived, object: nil)
    }
    @objc func pushNotificationReceived(_ notification: Notification) {
        let userInfo = notification.userInfo
        if let type = userInfo?["type"] as? String, type == PushNotificationType.FEEDBACK_UE.rawValue, type == PushNotificationType.FEEDBACK_UE_2.rawValue, type == PushNotificationType.CONTACT_UE.rawValue, type == PushNotificationType.INVITE_UE.rawValue, type == PushNotificationType.PROFILE_UE.rawValue {  }
        else {
            PointsHandler.shared.isNotification = true
            notificationBadge.isHidden = false
        }
    }
}

