//
//  SearchCategoriesTagCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class SearchCategoriesTagCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    func setValues(currentFilter: Int, title: String) {
        label.text = title
        layer.borderWidth = 0.5
        layer.borderColor = darkBlue.cgColor
        if indexPath.row == currentFilter {
            label.textColor = .white
            backgroundColor = darkBlue
        } else {
            label.textColor = darkBlue
            backgroundColor = .white
        }
    }
}
