//
//  SearchSuggestionsCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class SearchSuggestionsCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    
    func setValues(text: String, imageName: String, isCellSelected: Bool) {
        label.text = text
        image.image = UIImage(named: imageName)
        if isCellSelected {
            label.textColor = UIColor.NewTheme.lightBlue
            image.tintColor = UIColor.NewTheme.lightBlue
            setUpBorder(width: 0.5, color: UIColor.NewTheme.lightBlue, radius: 5)
        } else {
            label.textColor = UIColor.NewTheme.lightGray
            image.tintColor = UIColor.NewTheme.lightGray
            setUpBorder(width: 0.5, color: UIColor.NewTheme.lightGray, radius: 5)
        }
        
    }
}
