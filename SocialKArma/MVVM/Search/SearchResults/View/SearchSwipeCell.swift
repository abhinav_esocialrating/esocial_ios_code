//
//  SearchSwipeCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/05/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import WKVerticalScrollBar

class SearchSwipeCell: UIView {

    @IBOutlet weak var headerContainerView: UIView!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet var scrollButtons: [UIButton]!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var noSkillsLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var skillLabel: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var feedbackLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var customScrollBar: WKVerticalScrollBar!
    // MARK: - Variables
    var searchResult: SearchResults? {
        didSet {
            setValues()
        }
    }
    var delegate: SendData!
    var currentFilter = 1
    var headerView: CardHeaderView!
    var swipeBack: (() -> Void)?
    
    // MARK: - Actions
    @IBAction func viewProfileTapped(_ sender: Any) {
        delegate.passData(dataPasser: self, data: "message")
    }
    @IBAction func upBtnTapped(_ sender: UIButton) {
        swipeBack?()
        /*
        let content = scrollView.contentSize.height
        if sender == scrollButtons[0] {
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        } else {
            scrollView.setContentOffset(CGPoint(x: 0, y: content/2), animated: true)
        }
        */
    }
    // MARK: - Methods
    func configureView() {
        layer.cornerRadius = 12
        dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
        userImg.rounded()
        customScrollBar.scrollView = scrollView
        customScrollBar.isHidden = screenWidth == 320
        customScrollBar.handleSelectedWidth = 2
        customScrollBar.handleHitWidth = 2

        // Header
        headerView = CardHeaderView.instantiateFromNib()
        headerContainerView.addSubview(headerView)
        headerView.anchor(top: headerContainerView.topAnchor, paddingTop: 0, bottom: headerContainerView.bottomAnchor, paddingBottom: 0, left: headerContainerView.leftAnchor, paddingLeft: 0, right: headerContainerView.rightAnchor, paddingRight: 0, width: 0, height: 0)
        headerView.buttonStack.isHidden = true
        headerView.detailView.backgroundColor = UIColor.SearchColors.blue
        headerView.detailView.layer.cornerRadius = 12
        headerView.nameLabel.textColor = .white
        headerView.reviewerLabel.textColor = .white
        headerView.scoreLabel.textColor = .white
        headerView.scoreStaticLabel.textColor = .white
        headerView.locationLabel.textColor = .white
        headerView.distanceLabel.textColor = .white
        headerView.separatorView.backgroundColor = .white
        headerView.editNameBtn.isHidden = true
        headerView.numberLabel.isHidden = true
        
        tagListView.removeAllTags()
        tagListView.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 18)
        
        headerView.picTap = {
            self.delegate.passData(dataPasser: self, data: "profile")
        }
    }
    
    func setValues() {
        var str = ""
        if let name = searchResult?.name {
            str = name
        } else {
            str = "Mr. Xyz"
        }
        headerView.nameLabel.text = str
        if let url = searchResult?.image_id, url != "" {
            let trimmed = url.trimmingCharacters(in: .whitespacesAndNewlines)
            headerView.userImage.sd_setImage(with: URL(string: trimmed)!, completed: nil)
        } else {
            headerView.userImage.image = UIImage(named: "avatar-2")
        }
        if let location = searchResult?.location_city, location != "" {
            headerView.locationLabel.text = location
        } else {
            headerView.locationLabel.text = ""
        }
        // taglistview
        if let interests = searchResult?.interest, interests.count > 0 {
            interests.forEach { (interest) in
                tagListView.addTag(interest.interest ?? "No Data Found")
            }
            noSkillsLabel.text = ""
        } else if let skills = searchResult?.skill, skills.count > 0 {
            skills.forEach { (skill) in
                tagListView.addTag(skill.skill ?? "No Data Found")
            }
            noSkillsLabel.text = ""
        } else {
            noSkillsLabel.text = "No Data Found"
        }
        // image border according to score
        if let rateValue = searchResult?.score {
            if rateValue < 2.5 {
                headerView.userImage.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("ee1d24"), radius: headerView.userImage.frame.height/2)

            } else if rateValue >= 2.5 && rateValue < 3.75 {
                headerView.userImage.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("f36821"), radius: headerView.userImage.frame.height/2)

            } else {
                headerView.userImage.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("72bf43"), radius: headerView.userImage.frame.height/2)
            }
        } else {
            headerView.userImage.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("ee1d24"), radius: headerView.userImage.frame.height/2)
        }
        if let score = searchResult?.score {
            headerView.scoreLabel.text = String(format: "%.1f", score)
        }
        if let reviewers = searchResult?.reviews {
            headerView.reviewerLabel.text = String(format: "Feedback: %d", reviewers)
        }
        if let dist = searchResult?.dist {
            headerView.distanceLabel.text = String(format: "%d km", dist)
        }
        if let bio = searchResult?.bio, bio != "" {
            bioLabel.text = bio.decodeEmoji
        } else {
            bioLabel.text = "No Data Found"
        }
        if currentFilter == 0 {
            skillLabel.text = "Interests"
        } else {
            skillLabel.text = "Skills"
        }
        
        if let medals = searchResult?.medals {
            for (_,j) in medals.enumerated() {
                if let image_url = j.image_url, image_url != "" {
                    
                    let img = UIImageView()
                    headerView.medalStack.addArrangedSubview(img)
                    img.anchor(top: nil, paddingTop: 0, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, width: 20, height: 20)
                    img.sd_setImage(with: URL(string: image_url), completed: nil)
                }
            }
        }
        
        if let pinned_feedback = searchResult?.pinned_feedback, pinned_feedback.count > 0 {
            if let name = pinned_feedback[0].name {
                nameLabel.text = name
            }
            if let date = pinned_feedback[0].date {
                timeLabel.text = date
            }
            if let feed = pinned_feedback[0].feedback {
                feedbackLabel.text = feed
            }
            if let image = pinned_feedback[0].image_id, let url = URL(string: image) {
                userImg.sd_setImage(with: url, completed: nil)
            }
        } else {
            userImg.removeFromSuperview()
            stackView.arrangedSubviews.forEach({$0.removeFromSuperview()})
        }
    }
    
}
/*  colors of user image border
<2.5             ee1d24
>=2.5  <3.75     f36821
>=3.75           72bf43
*/
/* Interests/skills in a label
var interestStr = ""
if let interests = searchResult?.interest, interests.count > 0 {
    for (j,i) in interests.enumerated() {
        if j >= 3 {
            let more = interests.count - 3
            interestStr += "  +\(more) more"
            break
        }
        if j == 0 {
            interestStr += i.interest ?? ""
        } else {
            interestStr += ", " + (i.interest ?? "")
        }
    }
} else if let skills = searchResult?.skill, skills.count > 0 {
    for (j,i) in skills.enumerated() {
        if j >= 3 {
            let more = skills.count - 3
            interestStr += "  +\(more) more"
            break
        }
        if j == 0 {
            interestStr += i.skill ?? ""
        } else {
            interestStr += ", " + (i.skill ?? "")
        }
    }
}
interestLabel.text = interestStr
*/
