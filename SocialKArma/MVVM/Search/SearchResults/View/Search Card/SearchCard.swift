//
//  SearchCard.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 03/09/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class SearchCard: UIView {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var medalStack: UIStackView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var reviewersLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var skillInterestView: TagListView!
    @IBOutlet weak var noSkillFoundLabel: UILabel!
    @IBOutlet weak var educLabel: UILabel!
    @IBOutlet weak var expLabel: UILabel!
    // MARK: - Variables
    var searchResult: SearchResults? {
        didSet {
            setValues()
        }
    }
    var swipeBack: (() -> Void)?  //swipeBack?()
    var currentFilter = 1
    var delegate: SendData!

    func configureView() {
        layer.cornerRadius = 12
        dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        
//        let gradient: CAGradientLayer = CAGradientLayer()
//        gradient.colors = [UIColor.clear.cgColor, UIColor.black.withAlphaComponent(0.99).cgColor]
//        gradient.frame = CGRect(x: 0.0, y: 0.0, width: userImg.frame.size.width, height: userImg.frame.size.height)
//        userImg.layer.insertSublayer(gradient, at: 0)
        
        userImg.rounded()

        scoreLabel.setUpBorder(width: 0.5, color: .darkGray, radius: 8)
        
        searchBtn.rounded()
        searchBtn.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: searchBtn.frame.width/2, scale: true)

//        imageContainer.rounded() //roundCorners(corners: [.topLeft, .topRight], radius: 12)
        
        skillInterestView.removeAllTags()
        skillInterestView.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 16)
    }
    func setValues() {
        var str = ""
        if let name = searchResult?.name {
            str = name
        } else {
            str = "Mr. Xyz"
        }
        nameLabel.text = str
        
        if let url = searchResult?.image_id, url != "" {
            let trimmed = url.trimmingCharacters(in: .whitespacesAndNewlines)
            userImg.sd_setImage(with: URL(string: trimmed)!, completed: nil)
        } else {
            userImg.image = UIImage(named: "avatar-2")
        }
        
        var distanceAndAge = " "
        if let age = searchResult?.age {
            distanceAndAge = "\(age)"
        }
        if let loc = searchResult?.location_city {
            if distanceAndAge == " " {
                distanceAndAge = loc
            } else {
                distanceAndAge.append("  \u{2022}  \(loc)")
            }
        }
        locationLabel.text = distanceAndAge
        
        if let score = searchResult?.score {
            scoreLabel.text = String(format: "%.1f", score)
        } else {
            scoreLabel.text = "0.0"
        }
        
        if let dist = searchResult?.dist {
            distanceLabel.text = String(format: "%d km", dist)
        } else {
            distanceLabel.text = "0 km"
        }
        if let bio = searchResult?.bio, bio != "" {
            bioLabel.text = bio.decodeEmoji
        } else {
            bioLabel.text = "About Info not added"
        }
        
        educLabel.text = searchResult?.educ
        expLabel.text = searchResult?.exp

        if let interests = searchResult?.interest, interests.count > 0 {
            interests.forEach { (interest) in
                skillInterestView.addTag(interest.interest ?? "No Data Found")
            }
            skillInterestView.isHidden = false
            noSkillFoundLabel.isHidden = true
        } else if let skills = searchResult?.skill, skills.count > 0 {
            skills.forEach { (skill) in
                skillInterestView.addTag(skill.skill ?? "No Data Found")
            }
            skillInterestView.isHidden = false
            noSkillFoundLabel.isHidden = true
        } else {
            skillInterestView.isHidden = true
            noSkillFoundLabel.isHidden = false
            noSkillFoundLabel.text = "No Skill/Interest added"
        }
        
        // image border according to score
        if let rateValue = searchResult?.score {
            if rateValue < 2.5 {
                userImg.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("ee1d24"), radius: userImg.frame.height/2)

            } else if rateValue >= 2.5 && rateValue < 3.75 {
                userImg.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("f36821"), radius: userImg.frame.height/2)

            } else {
                userImg.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("72bf43"), radius: userImg.frame.height/2)
            }
        } else {
            userImg.setUpBorder(width: 1.5, color: UIColor.colorFromHexString("ee1d24"), radius: userImg.frame.height/2)
        }
        
        bioLabel.numberOfLines = screenWidth > 375 ? 3 : 1
    }
    
    // MARK: - Actions
    @IBAction func searchTapped(_ sender: Any) {
        swipeBack?()
    }
    @IBAction func viewProfileTapped(_ sender: Any) {
        self.delegate.passData(dataPasser: self, data: "profile")
    }
    
    @IBAction func messageTapped(_ sender: Any) {
        delegate.passData(dataPasser: self, data: "message")
    }
    
}
