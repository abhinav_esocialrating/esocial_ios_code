

import Foundation
import SwiftyJSON

public class SearchResults {
	public var user_id : Int?
	public var name : String?
	public var image_id : String?
	public var location_city : String?
	public var age : Int?
	public var status : String?
	public var score : Float?
    public var id : String?
    public var interest: [InterestModel]?
    public var skill: [SkillModel]?
    public var bio : String?
    public var educ : String?
    public var exp : String?
    public var dist : Int?
    public var reviews : Int?
    public var medals : Array<Medals>?
    public var pinned_feedback : Array<TextualFeedback>?

    required public init?(dictionary: [String:JSON]) {

        user_id = dictionary["user_id"]?.int
        name = dictionary["name"]?.string
		image_id = dictionary["image_id"]?.string
		location_city = dictionary["location_city"]?.string
        age = dictionary["age"]?.int
		status = dictionary["status"]?.string
        score = dictionary["score"]?.float
        id = dictionary["_id"]?.string
        if let interests = dictionary["interest"]?.arrayObject {
            interest = InterestModel.modelsFromDictionaryArray(array: interests as NSArray)
        }
        if let skills = dictionary["skill"]?.arrayObject {
            skill = SkillModel.modelsFromDictionaryArray(array: skills as NSArray)
        }
        bio = dictionary["bio"]?.string
        educ = dictionary["educ"]?.string
        exp = dictionary["exp"]?.string
        dist = dictionary["dist"]?.int
        reviews = dictionary["reviews"]?.int
        if let medalsArray = dictionary["medals"]?.arrayObject as NSArray? {
            medals = Medals.modelsFromDictionaryArray(array: medalsArray)
        }
        if let pinned_feedback_array = dictionary["pinned_feedback"]?.arrayObject as NSArray? {
            pinned_feedback = TextualFeedback.modelsFromDictionaryArray(array: pinned_feedback_array)
        }
	}

}
