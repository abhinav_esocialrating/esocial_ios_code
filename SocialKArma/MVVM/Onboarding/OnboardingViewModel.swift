//
//  OnboardingViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class OnboardingViewModel: NSObject {
    var onboardingDataSource = ["w1" ,"w2", "w3", "w4"]
    var onboardingText = ["Empower yourself\nShare human Experience","Find reliable people for your every need","Know what people think of you","Get found"]
}
