//
//  OnboardingViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var collectionBottomConst: NSLayoutConstraint!
    // MARK: - Variables
    let viewModel = OnboardingViewModel()
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }
    }
    func configureView() {
        pageControl.numberOfPages = viewModel.onboardingDataSource.count
        pageControl.currentPage = 0
        signInButton.layer.borderColor = UIColor.white.cgColor
        signInButton.layer.borderWidth = 1
        signInButton.layer.cornerRadius = 8
//        collectionBottomConst.constant = screenHeight > 736 ? 50 : 20
    }

    // MARK: - Actions
    @IBAction func signUpTapped(_ sender: Any) {
        openSignUpController(requestType: 2)
    }
    
    @IBAction func signInTapped(_ sender: Any) {
        openSignUpController(requestType: 1)
    }
    func openSignUpController(requestType: Int) {
        let vc = SignUpViewController.instantiate(fromAppStoryboard: .Main)
        vc.requestType = requestType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
// MARK: - CollectionView methods
extension OnboardingViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCells.OnBoardingCollectionViewCell.rawValue, for: indexPath) as! OnBoardingCollectionViewCell
//        cell.imgView.contentMode = screenHeight > 736 ? UIView.ContentMode.scaleAspectFit : UIView.ContentMode.scaleAspectFill
        cell.setValues(image: viewModel.onboardingDataSource[indexPath.item], text: viewModel.onboardingText[indexPath.item])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenWidth, height: collectionView.frame.height)
    }
}
// MARK: - UIScrollViewDelegate
extension OnboardingViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        if Int(pageNumber) == viewModel.onboardingDataSource.count - 1 {
            
        }
    }
}
