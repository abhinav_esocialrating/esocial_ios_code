//
//  OnBoardingCollectionViewCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class OnBoardingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var imgContView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var isCoachMark = false
    
    var card: InfoCardsModel? {
        didSet {
            setCardsValues()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        if scrollView != nil {
            scrollView.zoomScale = 1
            scrollView.minimumZoomScale = 1
            scrollView.maximumZoomScale = 2
            scrollView.delegate = self
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        imgContView.layoutSubviews()
        if card == nil && isCoachMark == false {
            imgContView.layer.cornerRadius = 12
            imgContView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        } else {
            imgContView.layer.cornerRadius = 0
            imgContView.dropShadow(color: UIColor.clear, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        }
    }
    func setValues(image: String, text: String) {
        imgView.image = UIImage(named: image)
    }
    
    func setCardsValues() {
        if let background_image = card?.background_image, let url = URL(string: background_image) {
            imgView.sd_setImage(with: url, completed: nil)
        } else {
            imgView.image = UIImage(named: "splash screen_transparent")
        }
    }
    
    func setImages(url: String) {
        if let url = URL(string: url) {
            imgView.sd_setImage(with: url, completed: nil)
        } else {
            imgView.image = UIImage(named: "splash screen_transparent")
        }
    }
}
// MARK: - ScrollView Delegate
extension OnBoardingCollectionViewCell: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgView
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let imageViewSize = imgView.frame.size
        let scrollViewSize = scrollView.bounds.size

        let verticalPadding = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2 : 0
        let horizontalPadding = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2 : 0

        if verticalPadding >= 0 {
            // Center the image on screen
            scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
        } else {
            // Limit the image panning to the screen bounds
            scrollView.contentSize = imageViewSize
        }
    }
}
