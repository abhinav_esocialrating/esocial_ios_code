//
//  CoachMarks4ViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CoachMarks4ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addTapGesture()
    }
    // MARK: - Methods
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(mainViewTapped(_:)))
        view.addGestureRecognizer(tap)
    }
    @objc func mainViewTapped(_ gesture:UIGestureRecognizer) {
        dismiss(animated: false, completion: nil)
    }
}
