//
//  CoarchMark.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 08/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation

class CoarchMark: NSObject {
    var origin: CGPoint!
    var diameter: CGFloat!
    
    required init(origin: CGPoint, diameter: CGFloat) {
        self.origin = origin
        self.diameter = diameter
    }
}
