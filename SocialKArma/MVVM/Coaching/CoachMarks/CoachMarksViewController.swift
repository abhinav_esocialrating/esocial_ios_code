//
//  CoachMarksViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 08/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CoachMarksViewController: UIViewController {
    
    @IBOutlet weak var whatsapp: UIImageView!
    
    @IBOutlet weak var search: UIImageView!
    
    @IBOutlet weak var whatsappArrow: UIImageView!
    
    @IBOutlet weak var relationshipArrpw: UIImageView!
    
    // Constraints
    @IBOutlet weak var whatsappTop: NSLayoutConstraint!
    
    @IBOutlet weak var whatsappLeading: NSLayoutConstraint!
    @IBOutlet weak var searchTop: NSLayoutConstraint!
    
    @IBOutlet weak var searchTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var relationshipTop: NSLayoutConstraint!
    
    // MARK: - Variables
    var coachMarks = [CoarchMark]()
    var points = [CGPoint]()
    // MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addTapGesture()
        addSwipeGesture()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        createCoachMarks()
        UserDefaults.standard.isCoachMarkDone = true
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        rotate()
    }
    
    // MARK: - Methods
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(mainViewTapped(_:)))
        view.addGestureRecognizer(tap)
    }
    func addSwipeGesture() {
       let swipe = UISwipeGestureRecognizer(target: self, action: #selector(mainViewTapped(_:)))
       swipe.direction = .up
       view.addGestureRecognizer(swipe)
   }
    @objc func mainViewTapped(_ gesture:UIGestureRecognizer) {
        dismiss(animated: false, completion: nil)
//        navigationController?.popViewController(animated: false)
    }
    func createCoachMarks() {
        var notchHeight = UIDevice.current.hasNotch ? 30 : 5
        
        whatsappLeading.constant = points[0].x //- (whatsapp.frame.width/2)
        whatsappTop.constant = points[0].y - (whatsapp.frame.height/2) - CGFloat(notchHeight)
        
//        searchTrailing.constant = points[1].x //- (search.frame.width/2)
//        searchTop.constant = points[1].y - (search.frame.height/2) - CGFloat(notchHeight)
        
        notchHeight = UIDevice.current.hasNotch ? 0 : 30
        relationshipTop.constant = points[2].y + CGFloat(notchHeight) //+ (relationshipArrpw.frame.height/2)
    }
    func rotate() {
        whatsappArrow.rotate(angle: 45)
        relationshipArrpw.rotate(angle: 90)
    }
}
