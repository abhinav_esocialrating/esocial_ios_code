//
//  CoachMarks2ViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 08/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CoachMarks2ViewController: UIViewController {


    @IBOutlet weak var submitTop: NSLayoutConstraint!
    @IBOutlet weak var parameterTop: NSLayoutConstraint!
    
    var points = [CGPoint]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapGesture()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        createCoachMarks()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    // MARK: - Methods
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(mainViewTapped(_:)))
        view.addGestureRecognizer(tap)
    }
    @objc func mainViewTapped(_ gesture:UIGestureRecognizer) {
        dismiss(animated: false, completion: nil)
    }
     
    func createCoachMarks() {
//        parameterTop.constant = points[0].y
//        let notchHeight = UIDevice.current.hasNotch ? 105 : 0
        submitTop.constant = points[1].y - 105//CGFloat(notchHeight)
        
    }
}
