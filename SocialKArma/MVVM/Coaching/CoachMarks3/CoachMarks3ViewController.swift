//
//  CoachMarks3ViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 13/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CoachMarks3ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addTapGesture()
        addSwipeGesture()
    }
    // MARK: - Methods
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(mainViewTapped(_:)))
        view.addGestureRecognizer(tap)
    }
    @objc func mainViewTapped(_ gesture:UIGestureRecognizer) {
        dismiss(animated: false, completion: nil)
    }
    func addSwipeGesture() {
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(mainViewTapped(_:)))
        swipe.direction = .up
        view.addGestureRecognizer(swipe)
    }
}
