//
//  ProfileCoachMarkViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/03/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ProfileCoachMarkViewController: UIViewController {
    
    @IBOutlet var coachViews: [UIView]!   // 0: Others Profile   1: Search   2: self profile
    @IBOutlet var othersProfileArrow1: [UIImageView]!
    @IBOutlet var selfProfileArrow1: [UIImageView]!
    @IBOutlet var searchArraw: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    var type: Int = 0      // 0: Others Profile   1: Search
    var otherUserName: String?
    var completion: completionBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addTapGesture()
        for (j,i) in coachViews.enumerated() {
            i.isHidden = type != j
        }
        if let name = otherUserName, type == 0 {
            label1.text = "Connect with \(name)"
            label2.text = "Check of \(name)'s score of any two parameters of ypur choice without requesting/letting him know"
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if type == 0 {
            othersProfileArrow1[0].rotate(angle: -45)
            othersProfileArrow1[1].rotate(angle: -90)
        } else if type == 1 {
            searchArraw.rotate(angle: -90)
        } else {
            selfProfileArrow1[0].rotate(angle: -45)
            selfProfileArrow1[1].rotate(angle: -90)
        }
    }
    
    // MARK: - Navigation

    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(mainViewTapped(_:)))
        view.addGestureRecognizer(tap)
    }
    @objc func mainViewTapped(_ gesture:UIGestureRecognizer) {
        dismiss(animated: false, completion: nil)
        completion?()
    }

}
