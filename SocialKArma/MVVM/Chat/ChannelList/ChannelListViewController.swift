//
//  ChannelListViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 23/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import FAPanels
import SwiftyJSON
import SocketIO

class ChannelListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let viewModel = ChannelListViewModel()
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var karmaPointsButton: UIButton!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var notificationBadge: UILabel!
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        setUpNavBar()
        setUpPush()
        PointsHandler.shared.isChatNotificationBadge = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        chatGetRecentChats()
//        notificationBadge.isHidden = PointsHandler.shared.isNotification ? false : true
//        PointsHandler.shared.flashButton(button: karmaPointsButton)
//        karmaPointsButton.setTitle("\(PointsHandler.shared.points)", for: .normal)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        PointsHandler.shared.flipCoin(button: karmaPointsButton)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureView() {
        tableView.dataSource = self
        tableView.delegate = self
//        karmaPointsButton.setUpBorder(width: 0.75, color: UIColor.NewTheme.darkGray, radius: karmaPointsButton.frame.height/2)
        
//        notificationBadge.layer.cornerRadius = notificationBadge.frame.width/2
//        notificationBadge.clipsToBounds = true
    }
    func updatePoints() {
        let userInfo = UserDefaults.standard.userInfo
        let data = userInfo.data(using: .utf8)
        let json = JSON(data ?? Data())
        let dict = json.dictionaryObject
        if let points = dict?["karma_points"] as? Int {
            karmaPointsButton.setTitle("\(points)", for: .normal)
        }
    }
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
//        if let panel = appDelegate?.window?.rootViewController as? FAPanelController {
//            panel.openLeft(animated: true)
//        }
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func notificationTapped(_ sender: Any) {           openNotificationFromNavBar()
    }
    
    @IBAction func pointsBtnTapped(_ sender: Any) {
        openPointsScreenFromNavBar()
    }
    // MARK: - Methods
    func updateUI() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.isHidden = self.viewModel.recentChatList.count > 0 ? false : true
            self.noDataLabel.isHidden = self.viewModel.recentChatList.count > 0 ? true : false
        }
    }
    func openChatVC(index: Int) {
        let vc = ChatViewController.instantiate(fromAppStoryboard: .Chat)
        SocketIOManager.shared.toUserId = "\(viewModel.recentChatList[index].user?.user_id ?? 0)"
        vc.viewModel.chatUser = viewModel.recentChatList[index].user
        navigationController?.pushViewController(vc, animated: true)
    }
    func getChatFilteredForReportedUsers(data: [RecentChat]) -> [RecentChat] {
        let reported = UserDefaults.standard.reportedUsers
        let filtered = data.filter({!reported.contains("\($0.user?.user_id ?? 0)")})
        return filtered
    }
    func openProfile(index: Int) {
        let vc = ProfileBaseViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.userType = 2
        vc.otherUserId = viewModel.recentChatList[index].user?.user_id ?? 0
        ProfileV2Data.sharedInstance.userType = 2
        ProfileV2Data.sharedInstance.otherUserId = viewModel.recentChatList[index].user?.user_id ?? 0
        navigationController?.pushViewController(vc, animated: true)
    }
    // MARK: - Web Services
    func chatGetRecentChats() {
        let params = ["userId": UserDefaults.standard.userId, "page": 0] as [String : Any]
        SocketIOManager.shared.chatGetRecentChats(params: params) { (chats, error) in
            if error == "" {
                self.viewModel.recentChatList = []
                self.viewModel.recentChatList = self.getChatFilteredForReportedUsers(data: chats)
                self.updateUI()
            } else {
                self.updateUI()
            }
        }
    }
}
// MARK: - TableView methods
extension ChannelListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.recentChatList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ChannelListCell.rawValue, for: indexPath) as! ChannelListCell
        cell.indexPath = indexPath
        cell.recentChat = viewModel.recentChatList[indexPath.row]
        cell.profilePicTap = { index in
            self.openProfile(index: index)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2]
            tabItem.badgeColor = nil
            tabItem.badgeValue = nil
        }
        openChatVC(index: indexPath.row)
    }

}
// MARK: - NavBar
extension ChannelListViewController {
    func setUpNavBar() {
        NotificationCenter.default.addObserver(self, selector: #selector(pushNotificationReceived(_:)), name: Notification.Name.PushReceived, object: nil)
    }
    @objc func pushNotificationReceived(_ notification: Notification) {
        if let type = notification.userInfo?["type"] as? String {
            if type == PushNotificationType.FEEDBACK_UE.rawValue || type == PushNotificationType.FEEDBACK_UE_2.rawValue || type == PushNotificationType.CONTACT_UE.rawValue || type == PushNotificationType.INVITE_UE.rawValue || type == PushNotificationType.PROFILE_UE.rawValue {  }
            else {
//                PointsHandler.shared.isNotification = true
//                notificationBadge.isHidden = false
            }
        }
    }
}
// MARK: - Push Notifications
extension ChannelListViewController {
    func setUpPush() {
        NotificationCenter.default.addObserver(self, selector: #selector(pushReceivedChat(_:)), name: Notification.Name.PushReceivedChat, object: nil)
    }
    @objc func pushReceivedChat(_ notification: Notification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        if self.navigationController?.viewControllers.count == 1 {
            if let notificationHandleType = userInfo["notificationHandleType"] as? String, notificationHandleType == NotificationHandleType.willPresent.rawValue {
                chatGetRecentChats()
            } else if let type = userInfo["type"] as? String, type == PushNotificationType.CHAT_UNREAD_MESSAGES.rawValue {
                chatGetRecentChats()
            } else {
                openChatFromPush(userInfo: userInfo)
            }
        } else {
            self.navigationController?.popToRootViewController(animated: false)
            openChatFromPush(userInfo: userInfo)
        }
    }
    func openChatFromPush(userInfo: [AnyHashable:Any]) {
        var userDict = [String:Any]()
        if let name = userInfo["fromUserName"] as? String, let image_id = userInfo["image"] as? String, let fromUserId = userInfo["fromUserId"] as? String {
            userDict["name"] = name
            userDict["image_id"] = image_id
            SocketIOManager.shared.toUserId = fromUserId
            let chatUser = ChatUser(dictionary: userDict as NSDictionary)
            let vc = ChatViewController.instantiate(fromAppStoryboard: .Chat)
            vc.viewModel.chatUser = chatUser
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
