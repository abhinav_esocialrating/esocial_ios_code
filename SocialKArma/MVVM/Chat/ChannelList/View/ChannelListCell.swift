//
//  ChannelListCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 23/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ChannelListCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var unreadLabel: UILabel!
    
    var recentChat: RecentChat? {
        didSet {
            setChatValues()
        }
    }
    var profilePicTap: ((_ index: Int) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        userImage.rounded()
    }
    func configureView() {
        unreadLabel.rounded()
    }
    func setChatValues() {
        labelTitle.text = recentChat?.user?.name
        messageLabel.text = recentChat?.message?.message
        if let url = recentChat?.user?.image_id, url != "" {
            userImage.sd_setImage(with: URL(string: url)!, completed: nil)
        } else {
            userImage.image = UIImage(named: "avatar-2")
        }
        if let isRead = recentChat?.message?.isRead, !isRead, let fromUserId = recentChat?.message?.fromUserId, fromUserId != Int(UserDefaults.standard.userId) {
            unreadLabel.isHidden = false
        } else {
            unreadLabel.isHidden = true
        }
    }
    
    @IBAction func picTapped(_ sender: Any) {
        
        profilePicTap?(indexPath.row)
    }
    
}
