//
//  ChatViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 23/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ChatViewModel: NSObject {
    var channelId = String()
    var messages = [Messages]()
    var chatHistory: ChatHistory?
    var userName = ""
    var userImageUrl = ""
    var user_id: Int?
    var chatUser: ChatUser?
    var page = 0
    var isFirstTime = true
    var dropDownData = ["Block", "Report"]
    var isAcceptViewShowing = false
    var currentAction: ChatRequestStatus = .None
    var isRejected: Bool = false
    var isBlocked: Bool = false
}
