/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Messages {
	public var _id : String?
	public var fromUserId : Int?
	public var toUserId : Int?
	public var message : String?
	public var type : String?
	public var isRead : Bool?
	public var crtd : String?
	public var mdfd : String?
    public var rejected : Bool?
    public var error : Bool?
/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let messages_list = Messages.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Messages Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Messages]
    {
        var models:[Messages] = []
        for item in array
        {
            models.append(Messages(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let messages = Messages(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Messages Instance.
*/
	required public init?(dictionary: NSDictionary) {

		_id = dictionary["_id"] as? String
		fromUserId = dictionary["fromUserId"] as? Int
		toUserId = dictionary["toUserId"] as? Int
		message = dictionary["message"] as? String
		type = dictionary["type"] as? String
		isRead = dictionary["isRead"] as? Bool
		crtd = dictionary["crtd"] as? String
		mdfd = dictionary["mdfd"] as? String
        rejected = dictionary["rejected"] as? Bool
        error = dictionary["error"] as? Bool
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self._id, forKey: "_id")
		dictionary.setValue(self.fromUserId, forKey: "fromUserId")
		dictionary.setValue(self.toUserId, forKey: "toUserId")
		dictionary.setValue(self.message, forKey: "message")
		dictionary.setValue(self.type, forKey: "type")
		dictionary.setValue(self.isRead, forKey: "isRead")
		dictionary.setValue(self.crtd, forKey: "crtd")
		dictionary.setValue(self.mdfd, forKey: "mdfd")
        dictionary.setValue(self.rejected, forKey: "rejected")
        
		return dictionary
	}

}
