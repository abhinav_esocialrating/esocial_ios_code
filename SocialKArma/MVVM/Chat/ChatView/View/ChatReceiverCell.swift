//
//  ChatReceiverCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 23/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ChatReceiverCell: UITableViewCell {
    
    @IBOutlet weak var bkgView: UIView!

    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    func configureView() {
        bkgView.layer.cornerRadius = 8
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
