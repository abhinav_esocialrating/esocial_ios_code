//
//  ChatSenderCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 23/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ChatSenderCell: UITableViewCell {
    
    @IBOutlet weak var bkgView: UIView!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        contentView.layoutSubviews()
//        bkgView.layoutSubviews()
//        bkgView.setGradient(gradientColors: [UIColor.ChatColors.senderBlue3.cgColor, UIColor.ChatColors.senderBlue1.cgColor, UIColor.ChatColors.senderBlue2.cgColor], gradientLocations: [0.0, 0.3, 0.6, 1.0], startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), cornerRadius: 12)
//    }
    func configureView() {
        bkgView.layer.cornerRadius = 12
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
