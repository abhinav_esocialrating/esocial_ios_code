//
//  ChatViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 23/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class ChatViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTV: KMPlaceholderTextView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var textViewBottom: NSLayoutConstraint!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet var acceptView: UIView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet var acceptButtons: [UIButton]!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var sendIcon: UIButton!
    
    // MARK: - Variables
    let viewModel = ChatViewModel()
    var chatDelegate: SocketIOManagerDelegate!
    let dropDown = DropDown()
    var keyboardHeight: CGFloat = 0.0
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        configureView()
        setUpKeyboardNotification()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        SocketIOManager.shared.connectSocket()
        SocketIOManager.shared.establishConnection()
        SocketIOManager.shared.startChatSocket()
        SocketIOManager.shared.delegate = self
        chatGetHistory()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SocketIOManager.shared.stopChatSocket()
        SocketIOManager.shared.closeConnection()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        userImage.layer.cornerRadius = userImage.frame.width/2
        messageTV.layer.cornerRadius = 12
        timeView.layer.cornerRadius = timeView.frame.height/2
        sendIcon.layer.cornerRadius = sendIcon.frame.height/2
    }
    // MARK: - Set up essentials
    /**
    Setting up ui of the app and adding targets to different controls that needs to be done after view has loaded
    */
    func configureView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
                
        if let chatUser = viewModel.chatUser {
            if let url = chatUser.image_id, url != "" {
                userImage.sd_setImage(with: URL(string: url), completed: nil)
            }
            userNameLabel.text = chatUser.name
        } else {
            userImage.image = UIImage(named: "avatar-2")
            userNameLabel.text = "ChatUser"
        }
        messageTV.delegate = self
    }
    /**
     Adding observers for keyboard, so that view can be managed when keyboard show and hide
     */
    private func setUpKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShown(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHidden(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChanged(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        if navigationController != nil {
            navigationController?.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
        
    }
    @IBAction func sendTapped(_ sender: Any) {
        if messageTV.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            sendMessage()
        } else {
            alert(message: "Please enter message")
        }
    }
    @IBAction func menuTapped(_ sender: Any) {
        showDropDown()
    }
    @IBAction func acceptBtnTapped(_ sender: Any) {
        let messageid = viewModel.messages.last?._id ?? "1"
        switch sender as! UIButton {
        case acceptButtons[0]:
            print("0")
            viewModel.currentAction = .Reject
            actionsOnChatUsers(status: .Reject, messageid: messageid)
        case acceptButtons[1]:
            print("1")
            actionsOnChatUsers(status: .Accept, messageid: messageid)
            viewModel.currentAction = .Accept
        default:
            print("2")
            actionsOnChatUsers(status: .Ingnored, messageid: messageid)
            viewModel.currentAction = .Ingnored
        }
    }
    // MARK: - Keyboard Management
    @objc func keyboardShown(_ notification: Notification) {
        var offset = CGFloat.init()
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardViewEndFrame = view.convert(keyboardSize, from: view.window)
            let keyboardHeights = keyboardViewEndFrame.height
            print(keyboardHeights)  // 260
            offset += keyboardHeights  // add keyboard height
        } else {
            offset += 260
        }
//        if self.isModalInPresentation {
            UIView.animate(withDuration: 1.0) {
                self.view.layoutIfNeeded()
                self.textViewBottom.constant = self.keyboardHeight
                self.view.layoutIfNeeded()
            }
//        }
    }
    @objc func keyboardHidden(_ notification: Notification) {
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
            self.textViewBottom.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    @objc func keyboardFrameChanged(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let window = UIApplication.shared.keyWindow
            let bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
            keyboardHeight = keyboardSize.height - bottomPadding - 12 // -16/12: for textview padding in its container view
        }
    }
    // MARK: - Helper Methods
    /**
    Updating UI after fetching data or performing some action.
     
     - Parameter scrollToBottom: boolean to know if the table needs to be scrolled to bottom on UI change or action performed
     - Parameter newMessageCount: Count of new message upto where table needs to be scrolled.
    */
    func updateUI(scrollToBottom: Bool, newMessageCount: Int) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.viewModel.dropDownData[0] = (self.viewModel.chatHistory?.isRecipientBlocked ?? false) ? "Unblock" : "Block"
            if scrollToBottom {
                if self.viewModel.messages.count > 0 {
                    self.tableView.scrollToRow(at: IndexPath(row: self.viewModel.messages.count-1, section: 0), at: .bottom, animated: false)
                }
                if (self.viewModel.chatHistory?.isRecipientBlocked! ?? false) { // Hide both
                    self.messageView.isHidden = true
                    self.removeAcceptView()
                } else if (self.viewModel.chatHistory?.hasChatted! ?? false) {  // hide aceept and show msg
                    self.removeAcceptView()
                    self.messageView.isHidden = false
                } else if (self.viewModel.messages.count > 0) {
                    let lastMessage = self.viewModel.messages.last
                    if (lastMessage?.type == ChatMessageType.request.rawValue) && lastMessage?.fromUserId != Int(UserDefaults.standard.userId) && !(lastMessage?.rejected ?? false) {
                        self.showAcceptView()
                        self.messageView.isHidden = true
                        self.view.endEditing(true)
                    } else {
                        self.removeAcceptView()
                        self.messageView.isHidden = false
                    }
                } else {
                    self.removeAcceptView()
                    self.messageView.isHidden = false
                }
            } else {
                self.tableView.scrollToRow(at: IndexPath(row: newMessageCount + 1, section: 0), at: .top, animated: false)
            }
        }
    }
    /**
    Method for opening a dropdown of some values
    */
    func showDropDown() {
        dropDown.anchorView = menuBtn
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 200
        dropDown.dataSource = viewModel.dropDownData
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropSelection(index: index, item: item)
        }
        dropDown.show()
    }
    /**
    Method for handling dropdown selection
     
     - Parameter index: index of seleted item
     - Parameter item: String value of the selected item
    */
    func handleDropSelection(index: Int, item: String) {
        switch index {
        case 0:
            actionsOnChatUsers(status: (viewModel.chatHistory?.isRecipientBlocked! ?? false) ? .Unblocked : .Blocked, messageid: "")
            viewModel.currentAction = (viewModel.chatHistory?.isRecipientBlocked! ?? false) ? .Unblocked : .Blocked
        default:
            openReport()
        }
    }
    /**
    Opening screen where user can report other users if other used is misbehaving or sharing obscence content.
    */
    func openReport() {
        let vc = ReportUserViewController.instantiate(fromAppStoryboard: .MainExt)
        vc.viewModel.otherUserId = SocketIOManager.shared.toUserId
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: false, completion: nil)
        tabBarController?.tabBar.isHidden = true
    }
    /**
    Adding a view where user can accept or reject a chat request of other user
    */
    func showAcceptView() {
        let margins = view.layoutMarginsGuide
        viewModel.isAcceptViewShowing = true
        view.addSubview(acceptView)
        acceptView.anchor(top: nil, paddingTop: 0, bottom: margins.bottomAnchor, paddingBottom: 0, left: acceptView.superview?.leftAnchor, paddingLeft: 0, right: acceptView.superview?.rightAnchor, paddingRight: 0, width: 0, height: 80)
    }
    /**
    Removing a view where user can accept or reject a chat request of other user
    */
    func removeAcceptView() {
        if viewModel.isAcceptViewShowing {
            viewModel.isAcceptViewShowing = false
            acceptView.removeFromSuperview()
        }
    }
    /**
    Updating status of other user after user has performed some action.
    */
    func updateStatus() {
        if viewModel.currentAction == .Blocked {
            viewModel.chatHistory?.isRecipientBlocked = true
        } else if viewModel.currentAction == .Unblocked {
            viewModel.chatHistory?.isRecipientBlocked = false
        } else if viewModel.currentAction == .Accept { //} || viewModel.currentAction == .Reject {
            viewModel.chatHistory?.hasChatted = true
        } else if viewModel.currentAction == .Reject {
            viewModel.chatHistory?.hasChatted = false
            let count = viewModel.messages.count
            viewModel.messages[count-1].rejected = true
        }
        viewModel.currentAction = .None
        self.updateUI(scrollToBottom: true, newMessageCount: 0)
    }
    /**
    Updating floating time label that shows time which date messages are shown at the top of table
    */
    func updateTimeLabel(hide: Bool, date: String) {
        timeView.isHidden = hide
        timeLabel.text = date
    }
    /**
    Updating status of messages to Read if user has entered the chat window.
    */
    func findMessageForRead(list: [Messages]) {
        for i in list {
            if i.fromUserId == Int(SocketIOManager.shared.toUserId) && (i.isRead ?? false) == false {
                
                let params = ["toUserId": i.toUserId ?? 0, "fromUserId": i.fromUserId ?? 0, "crtd": i.crtd ?? ""] as [String : Any]
                AppDebug.shared.consolePrint(params)
                SocketIOManager.shared.updateMessageStatus(params: params) { (_, _) in
                }
                break
            }
        }
    }
    // MARK: - Web service
    /**
     Calling message send function of the Socket manager singleton.
     */
    func sendMessage() {
        SocketIOManager.shared.emitChatMessage(message: messageTV.text!, messageType: (viewModel.chatHistory?.hasChatted ?? false) ? ChatMessageType.chat : ChatMessageType.request)
    }
    /**
    API calling function for getting chat history between 2 users in an offset of 10 or 20.
    */
    func chatGetHistory() {
        let params = ["fromUserId": UserDefaults.standard.userId, "toUserId": SocketIOManager.shared.toUserId, "page": viewModel.page] as [String:Any]
        SocketIOManager.shared.chatGetHistory(params: params) { (chatHistory, data, error) in
            if data.count == 0 {
                self.viewModel.isFirstTime = true
            }
            if error == "" {
                if self.viewModel.page == 0 {
                    self.viewModel.chatHistory = chatHistory
                    self.findMessageForRead(list: data)
                    self.viewModel.messages = data.reversed()
                    self.updateUI(scrollToBottom: true, newMessageCount: 0)
                } else {
                    self.viewModel.chatHistory = chatHistory
                    let reversed = data.reversed()
                    self.viewModel.messages.insert(contentsOf: reversed, at: 0)
                    self.updateUI(scrollToBottom: false, newMessageCount: data.count)
                }
            }
        }
    }
    /**
    API calling function for updating chat request of another user
     
     - Parameter status: Updated status of the request
     - Parameter messageid: Message id of the last message sent by other user
    */
    func actionsOnChatUsers(status: ChatRequestStatus, messageid: String) {
        SocketIOManager.shared.chatSetReqAction(status: status, messageid: messageid) { (error) in
            if error == "" {
                self.updateStatus()
            }
        }
    }
}
// MARK: - TableView methods
extension ChatViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.messages[indexPath.row].fromUserId == Int(UserDefaults.standard.userId) {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ChatSenderCell.rawValue, for: indexPath) as! ChatSenderCell
            cell.messageLabel.text = viewModel.messages[indexPath.row].message
            cell.timeLabel.text = Utilities.chatDate(date: viewModel.messages[indexPath.row].mdfd ?? "")
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ChatReceiverCell.rawValue, for: indexPath) as! ChatReceiverCell
            cell.messageLabel.text = viewModel.messages[indexPath.row].message
            cell.timeLabel.text = Utilities.chatDate(date: viewModel.messages[indexPath.row].mdfd ?? "")
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        print(indexPath.row)
        if !viewModel.isFirstTime {
            if indexPath.row == 0 && (viewModel.messages.count / 20) >= 1 {
                viewModel.page += 1
                chatGetHistory()
            }
        } else {
            viewModel.isFirstTime = !viewModel.isFirstTime
        }
    }
}
// MARK: - ScrollView methods
extension ChatViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        perform(#selector(scrollViewDidEndScrollingAnimation(_:)), with: scrollView, afterDelay: 0.3)
        let firstVisibleIndexPath = tableView.indexPathsForVisibleRows?.first
        guard let index = firstVisibleIndexPath else {
            return
        }
        
        let mdfd = viewModel.messages[index.row].mdfd
        let date = Utilities.dateFromString(dateString: mdfd ?? "")
        let calendar = Calendar.current

        let currentStartOfDay = calendar.startOfDay(for: Date())
        let mdfdStartOfDay = calendar.startOfDay(for: date.0)
        let components = calendar.dateComponents([.day], from: currentStartOfDay, to: mdfdStartOfDay)
        if components.day == 0 {
            updateTimeLabel(hide: false, date: "Today")// today
        } else if components.day == 1 {
            updateTimeLabel(hide: false, date: "Yesterday")// yesterday
        } else {
            updateTimeLabel(hide: false, date: date.1)// other
        }
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
//        updateTimeLabel(hide: true, date: "Today")// other
    }
}
// MARK: - TextView methods
extension ChatViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text != "" {
            let height = textView.contentSize.height
            if height > 86 {
                textViewHeight.constant = 86
            } else {
                textViewHeight.constant = textView.contentSize.height
            }
        } else {
            textViewHeight.constant = 35
        }
    }
}
extension ChatViewController: SocketIOManagerDelegate {
    /**
    Callback method for updating the view whenever a message is received and sent or deleted.
     
     - Parameter chatMessage: Object of the received from Socket manager Singleton
    */
    func messageSent(chatMessage: Messages) {
        if let _ = chatMessage.error, let message = chatMessage.message {
            self.viewModel.messages.removeLast()
            alertWithAction(message: message) {
                self.updateUI(scrollToBottom: true, newMessageCount: 0)
            }
        } else if chatMessage.fromUserId == Int(SocketIOManager.shared.toUserId)! {  // sent by other
            self.viewModel.messages.append(chatMessage)
//            self.messageTV.text = ""
            textViewHeight.constant = 35
            self.updateUI(scrollToBottom: true, newMessageCount: 0)
            let params = ["toUserId": chatMessage.toUserId ?? 0, "fromUserId": chatMessage.fromUserId ?? 0, "crtd": chatMessage.crtd ?? ""] as [String : Any]
            AppDebug.shared.consolePrint(params)
            SocketIOManager.shared.updateMessageStatus(params: params) { (_, _) in
            }
        } else if chatMessage.fromUserId == Int(UserDefaults.standard.userId)! {  // sent by self
            self.viewModel.messages.append(chatMessage)
            self.messageTV.text = ""
            textViewHeight.constant = 35
            self.updateUI(scrollToBottom: true, newMessageCount: 0)
        }
    }
}
