//
//  EmployeeFilterVC.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown
import IQKeyboardManagerSwift

class EmployeeFilterVC: UIViewController {
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var textfield1: ACFloatingTextfield!
    @IBOutlet weak var textfield2: ACFloatingTextfield!
    @IBOutlet weak var textfield3: ACFloatingTextfield!
    @IBOutlet weak var textfield4: ACFloatingTextfield!
    @IBOutlet weak var textfield5: ACFloatingTextfield!
    @IBOutlet weak var textfield6: ACFloatingTextfield!
    @IBOutlet weak var textfield7: ACFloatingTextfield!
    @IBOutlet weak var textfield8: ACFloatingTextfield!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var eduFieldView: TagListView!
    @IBOutlet weak var eduFieldViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var eduLevelView: TagListView!
    @IBOutlet weak var eduLevelViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var interestView: TagListView!
    @IBOutlet weak var interestViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var industryView: TagListView!
    @IBOutlet weak var industryViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var designationView: TagListView!
    @IBOutlet weak var designationViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var organisationView: TagListView!
    @IBOutlet weak var organisationViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var locationSlider: TTRangeSliderCircle!
    @IBOutlet weak var ageSlider: TTRangeSliderCircle!
    
    @IBOutlet weak var ageRangeSlider: RangeSeekSlider!
    
    @IBOutlet weak var locationRangeSlider: RangeSeekSlider!

    @IBOutlet weak var sliderViewHeight: NSLayoutConstraint!

    // MARK: - Variables
    let viewModel = EmployeeFilterViewModel()
    let dropDown = DropDown()
    var passData: SendData!
    let reachability = try! Reachability()
    var commonFilterView: CommonRangeSliderView!
    // Slider
    var distance = 0;
    var radius = 0;
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        do{
          try reachability.startNotifier()
        }catch{
          print("could not start reachability notifier")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dropDown.hide()
        self.view.endEditing(true)
    }
    func configureView() {
        setUpTextFields()
        setUpSlider()
        setUpTagViews()
        submitButton.layer.cornerRadius = 8
        
        countView.layer.cornerRadius = countView.frame.height/2

        commonFilterView = CommonRangeSliderView.instantiateFromNib()
        ratingView.addSubview(commonFilterView)
        commonFilterView.anchor(top: commonFilterView.superview?.topAnchor, paddingTop: 0, bottom: commonFilterView.superview?.bottomAnchor, paddingBottom: 0, left: commonFilterView.superview?.leftAnchor, paddingLeft: 0, right: commonFilterView.superview?.rightAnchor, paddingRight: 0, width: 0, height: 0)
        commonFilterView.passData = self
        commonFilterView.setUpSlider()
        
        // Placeholders
        var attributedPlaceholder = Utilities.attributedText(text: Constants.SkillsPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield2.attributedPlaceholder = attributedPlaceholder

        attributedPlaceholder = Utilities.attributedText(text: Constants.ExperiencePlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield3.attributedPlaceholder = attributedPlaceholder
        
        attributedPlaceholder = Utilities.attributedText(text: Constants.IndustryPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield5.attributedPlaceholder = attributedPlaceholder

        attributedPlaceholder = Utilities.attributedText(text: Constants.EducationPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield7.attributedPlaceholder = attributedPlaceholder
        
        attributedPlaceholder = Utilities.attributedText(text: Constants.EducationFieldPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield8.attributedPlaceholder = attributedPlaceholder
    }
    func setUpTextFields() {
        var array = [textfield3]
        array.forEach { (textField) in
            textField?.inputView = UIView()
        }
        array = [textfield1, textfield2, textfield4, textfield5, textfield6, textfield7, textfield8]
        array.forEach { (textField) in
            textField?.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
        }
        array = [textfield1, textfield2, textfield3, textfield4, textfield5, textfield6, textfield7, textfield8]
        for (_, textField) in array.enumerated() {
            textField?.lineColor = UIColor.NewTheme.darkGray
            textField?.selectedLineColor = UIColor.NewTheme.paleBlue
            textField?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
            textField?.textColor = UIColor.NewTheme.darkGray
            textField?.disableFloatingLabel = true
            textField?.delegate = self
        }
    }
    func setUpSlider() {
        let array = [ageRangeSlider, locationRangeSlider]
        for slider in array {
            slider?.delegate = self
            slider?.minLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 11)
            slider?.maxLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 11)
        }
    }
    func setUpTagViews() {
        let arr = [interestView, designationView, industryView, organisationView, eduFieldView, eduLevelView]
        arr.forEach { (tag) in
            tag?.delegate = self
            tag?.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
        }
    }

    
    // MARK: - Navigation

    @IBAction func backTapped(_ sender: Any) {
//        navigationController?.dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    @IBAction func applyTapped(_ sender: Any) {
        if reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            FilterModel.shared.paramDictionary["type"] = viewModel.filterType
            passData.passData(dataPasser: self, data: FilterModel.shared.paramDictionary)
            navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func resetTapped(_ sender: Any) {
        resetFiltersAndViews()
    }
    @objc func textFieldChanged(_ textField: UITextField) {
        var params = ["config": "", "search": textField.text!]
        switch textField {
        case textfield1:
            params = ["config": "designation", "search": textField.text!]
        case textfield2:
            params = ["config": "skill", "search": textField.text!]
        case textfield4:
            if textField.text!.count > 2 {
                LocationManager.shared.getCities(query: textField.text!) { (cities, cityNames) in
                    self.viewModel.dropDownData = cities
                    DispatchQueue.main.async {
                        self.showDropDown(textField: textField)
                    }
                }
            }
        case textfield6:
            params = ["search": textField.text!]
        case textfield5:
            params = ["config": "industry_type", "search": textField.text!]
        case textfield8:
            params = ["config": "degree_field", "search": textField.text!]
        default:
            params = ["config": "degree_level", "search": textField.text!]
        }
        if textField == textfield4 {
        } else if textField == textfield6 {
            viewModel.getOrganisations(parameters: params) { (success, json, message) in
                if success {
                    DispatchQueue.main.async {
                        if self.viewModel.organisationData.count > 0 {
                            self.showDropDown(textField: textField)
                        } else {
                            self.dropDown.hide()
                        }
                    }
                }
            }
        } else {
            viewModel.getSearchConfig(parameters: params) { (success, json, message) in
                if success {
                    DispatchQueue.main.async {
                        if self.viewModel.searchedData.count > 0 {
                            self.showDropDown(textField: textField)
                        } else {
                            self.dropDown.hide()
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Methods
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        switch textField {
        case textfield1, textfield2, textfield4, textfield5, textfield6, textfield7, textfield8:
            dropDown.dataSource = viewModel.dropDownData
        case textfield3:
            dropDown.dataSource = CommonData.experience
        default:
            print("")
        }
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropDownSelection(index: index, item: item, textField: textField)
        }
        dropDown.show()
    }
    
    func handleDropDownSelection(index: Int, item: String, textField: UITextField) {
        switch textField {
        case textfield1:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.jobTitle.contains(skillId) {
                    self.viewModel.selectedDesignationDict.append( self.viewModel.searchedData[index])
                    self.updateTagViews(tag: item, textField: textField)
                    FilterModel.shared.jobTitle.append(skillId)
                }
            }
        case textfield2:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.skills.contains(skillId) {
                    self.viewModel.selectedInterestsDict.append( self.viewModel.searchedData[index])
                    self.updateTagViews(tag: item, textField: textField)
                    FilterModel.shared.skills.append(skillId)
                }
            }
        case textfield3:
            textField.text = item
            FilterModel.shared.experience = Utilities().getExperience(index: index)
        case textfield4: // City
            textField.text = item
            textField.resignFirstResponder()
            FilterModel.shared.removeLocationValues()
            locationRangeSlider.selectedMaxValue = 1
            locationRangeSlider.layoutSubviews()
            distanceLabel.text = "5 m"
            FilterModel.shared.city = item
        case textfield6:
            textField.text = ""
            if let skillId = self.viewModel.organisationData[index]["organisation_id"] as? Int {
                if !FilterModel.shared.companyOrg.contains(skillId) {
                    self.viewModel.selectedOrganisationDict.append( self.viewModel.organisationData[index])
                    self.updateTagViews(tag: item, textField: textField)
                    FilterModel.shared.companyOrg.append(skillId)
                }
            }
        case textfield5:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.industry.contains(skillId) {
                    self.viewModel.selectedIndustryDict.append( self.viewModel.searchedData[index])
                    self.updateTagViews(tag: item, textField: textField)
                    FilterModel.shared.industry.append(skillId)
                }
            }
        case textfield7:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.educationLevel.contains(skillId) {
                    self.viewModel.selectedEduLevelDict.append( self.viewModel.searchedData[index])
                    self.updateTagViews(tag: item, textField: textField)
                    FilterModel.shared.educationLevel.append(skillId)
                }
            }
        default:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.educationField.contains(skillId) {
                    self.viewModel.selectedEduFieldDict.append( self.viewModel.searchedData[index])
                    self.updateTagViews(tag: item, textField: textField)
                    FilterModel.shared.educationField.append(skillId)
                }
            }
        }
        self.getSearchFiltered()
    }
    func updateTagViews(tag: String, textField: UITextField) {
        switch textField {
        case textfield1:
            designationView.addTag(tag)
            designationViewHeight.constant = designationView.intrinsicContentSize.height
        case textfield2:
            interestView.addTag(tag)
            interestViewHeight.constant = interestView.intrinsicContentSize.height
        case textfield6:
            organisationView.addTag(tag)
            organisationViewHeight.constant = organisationView.intrinsicContentSize.height
        case textfield5:
            industryView.addTag(tag)
            industryViewHeight.constant = industryView.intrinsicContentSize.height
        case textfield7:
            eduLevelView.addTag(tag)
            eduLevelViewHeight.constant = eduLevelView.intrinsicContentSize.height
        default:
            eduFieldView.addTag(tag)
            eduFieldViewHeight.constant = eduFieldView.intrinsicContentSize.height
        }
        
    }
    func updateSliderViewHeight() {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.sliderViewHeight.constant = self.viewModel.isSliderExpanded ? 1000 : 210
            self.view.layoutIfNeeded()
        }
    }
    func updateCountView(count: Int) {
        DispatchQueue.main.async {
            self.countView.isHidden = false
            self.countLabel.text = "\(count) Results found"
        }
    }
    func resetFiltersAndViews() {
        view.endEditing(true)
        // Reset filter model
        FilterModel.shared.resetValues()
        // Reset textfields
        let textfields = [textfield1, textfield2, textfield3, textfield4, textfield5, textfield6, textfield7, textfield8]
        textfields.forEach { (textfield) in
            textfield?.text = ""
        }
        // Reset Sliders
        ageRangeSlider.selectedMinValue = 16
        ageRangeSlider.selectedMaxValue = 100
        locationRangeSlider.selectedMaxValue = 1
        distanceLabel.text = "5 m"
        
        ageRangeSlider.layoutSubviews()
        locationRangeSlider.layoutSubviews()
                
        // TagListViews
        eduLevelView.removeAllTags()
        interestView.removeAllTags()
        industryView.removeAllTags()
        eduFieldView.removeAllTags()
        organisationView.removeAllTags()
        designationView.removeAllTags()
        
        eduLevelViewHeight.constant = 0
        interestViewHeight.constant = 0
        industryViewHeight.constant = 0
        eduFieldViewHeight.constant = 0
        organisationViewHeight.constant = 0
        designationViewHeight.constant = 0
        
        // Count Label
        countView.isHidden = true
        
        // Score Sliders
        commonFilterView.resetValues()
    }
    // MARK: - Web Services
    func getSearchFiltered() {
        if reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            FilterModel.shared.paramDictionary["type"] = viewModel.filterType
            FilterModel.shared.paramDictionary["latitude"] = LocationManager.shared.lastLocation?.coordinate.latitude
            FilterModel.shared.paramDictionary["longitude"] = LocationManager.shared.lastLocation?.coordinate.longitude
            print(FilterModel.shared.paramDictionary)
            self.viewModel.getSearchFiltered(parameters: FilterModel.shared.paramDictionary, filter: "friends", count: true) { (success, json, message) in
                if success {
                    let count = json.intValue
                    self.updateCountView(count: count)
                }
            }
        }
    }
}
// MARK: - TextFieldDelegate
extension EmployeeFilterVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textfield3 {
            showDropDown(textField: textField)
            return false
        } else if textField == textfield1 || textField == textfield2 || textField == textfield4 || textField == textfield5 || textField == textfield6 || textField == textfield8 || textField == textfield7 {
            textField.text = "a"
            textFieldChanged(textField)
            textField.text = ""
        }
        return true
    }
}

// MARK: - RangeSlider methods
extension EmployeeFilterVC: RangeSeekSliderDelegate {
    func didEndTouches(in slider: RangeSeekSlider) {
        if slider == ageRangeSlider {
            let min = slider.selectedMinValue
            let max = slider.selectedMaxValue
            let intMin = Int(min)
            let intMax = Int(max)
            FilterModel.shared.minAge = intMin
            FilterModel.shared.maxAge = intMax
            getSearchFiltered()
        } else {
            if Int(slider.selectedMaxValue) == 1 {
                slider.selectedMaxValue = 1;
            } else if (slider.selectedMaxValue >= 21 && slider.selectedMaxValue <= 27) {
                slider.selectedMaxValue = 27;
            } else if (slider.selectedMaxValue >= 28 && slider.selectedMaxValue <= 35) {
                slider.selectedMaxValue = 35;
            } else if (slider.selectedMaxValue >= 36 && slider.selectedMaxValue <= 43) {
                slider.selectedMaxValue = 43;
            } else if (slider.selectedMaxValue >= 44 && slider.selectedMaxValue <= 49) {
                slider.selectedMaxValue = 49;
            } else if (slider.selectedMaxValue > 98) {
                slider.selectedMaxValue = 100;
            }
            textfield4.text = ""
            getSearchLocationFiltered()
        }
    }
    func processSliderRating(value: Int) -> Int {
        return (abs(value/20) + 1)
    }
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider == locationRangeSlider {
            let selectedMaxInt = Int(slider.selectedMaxValue)
            if slider.selectedMaxValue < 1 {
                radius = 5
                distanceLabel.text = "5 m"
            } else if (selectedMaxInt == 1) {
                radius = 5
                distanceLabel.text = "5 m"
            } else if (selectedMaxInt >= 2 && selectedMaxInt <= 20) {
                radius = selectedMaxInt * 5
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 21 && selectedMaxInt <= 27) {
                radius = 200;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 28 && selectedMaxInt <= 35) {
                radius = 400;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 36 && selectedMaxInt <= 43) {
                radius = 600;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 44 && selectedMaxInt <= 49) {
                radius = 800;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt == 50) {
                radius = 1000;
                distanceLabel.text = "1 km"
            } else if (selectedMaxInt > 98) {
                radius = (selectedMaxInt/2) * 1000;
                distanceLabel.text = "\(selectedMaxInt / 2) km"
            } else {
                let distance = getDistance(progress: selectedMaxInt)
                radius = distance * 100;
                distanceLabel.text = "\(distance) km"
            }
        }
    }
    func getDistance(progress: Int) -> Int {
        if (progress % 10 != 0) {
            distance = progress % 50;
        }
        return distance;
    }
    func getSearchLocationFiltered() {
        var locationfiler = [String:Any]()
        if let lastLocation = LocationManager.shared.lastLocation {
            locationfiler["latitude"] = lastLocation.coordinate.latitude
            locationfiler["longitude"] = lastLocation.coordinate.longitude
        }
        locationfiler["type"] = viewModel.filterType
        locationfiler["radius"] = radius
        FilterModel.shared.locationFilter = locationfiler
        getSearchFiltered()
    }
}



// MARK: - TagListView delegates
extension EmployeeFilterVC: TagListViewDelegate {
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        switch sender {
        case designationView:
            sender.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedDesignationDict, tagView: sender)
            designationViewHeight.constant = sender.intrinsicContentSize.height
        case interestView:
            sender.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedInterestsDict, tagView: sender)
            interestViewHeight.constant = sender.intrinsicContentSize.height
        case organisationView:
            sender.removeTag(title)
            checkForStringInOrgTagViewData(tag: title, tagArray: &viewModel.selectedOrganisationDict, tagView: sender)
            organisationViewHeight.constant = sender.intrinsicContentSize.height
        case industryView:
            sender.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedIndustryDict, tagView: sender)
            industryViewHeight.constant = sender.intrinsicContentSize.height
        case eduLevelView:
            sender.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedEduLevelDict, tagView: sender)
            eduLevelViewHeight.constant = sender.intrinsicContentSize.height
        default:
            sender.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedEduFieldDict, tagView: sender)
            eduFieldViewHeight.constant = sender.intrinsicContentSize.height
        }
    }
    func checkForStringInTagViewData(tag: String, tagArray: inout [[String:Any]], tagView: TagListView) {
        for (j,i) in tagArray.enumerated() {
            if let id = i["id"] as? Int, let value = i["value"] as? String {
                if value == tag {
                    switch tagView {
                    case designationView:
                        FilterModel.shared.jobTitle = FilterModel.shared.jobTitle.filter({$0 != id})
                    case interestView:
                        FilterModel.shared.skills = FilterModel.shared.skills.filter({$0 != id})
                    case organisationView:
                        FilterModel.shared.companyOrg = FilterModel.shared.companyOrg.filter({$0 != id})
                    case industryView:
                        FilterModel.shared.industry = FilterModel.shared.industry.filter({$0 != id})
                    case eduLevelView:
                        FilterModel.shared.educationLevel = FilterModel.shared.educationLevel.filter({$0 != id})
                    default:
                        FilterModel.shared.educationField = FilterModel.shared.educationField.filter({$0 != id})
                    }
                    tagArray.remove(at: j)
                    self.getSearchFiltered()
                    break
                }
            }
        }
    }
    func checkForStringInOrgTagViewData(tag: String, tagArray: inout [[String:Any]], tagView: TagListView) {
        for (j,i) in tagArray.enumerated() {
            if let id = i["organisation_id"] as? Int, let value = i["organisation"] as? String {
                if value == tag {
                    switch tagView {
                    case organisationView:
                        FilterModel.shared.companyOrg = FilterModel.shared.companyOrg.filter({$0 != id})
                    default:
                        print("Other than organisation")
                    }
                    tagArray.remove(at: j)
                    self.getSearchFiltered()
                    break
                }
            }
        }
    }
}
// MARK: - SendData Delegates
extension EmployeeFilterVC: SendData {
    func passData(dataPasser: Any, data: Any) {
        if data is Bool {
            viewModel.isSliderExpanded = !viewModel.isSliderExpanded
            updateSliderViewHeight()
        } else {
            getSearchFiltered()
        }
    }
}
