//
//  EmployeeFilterViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 11/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class EmployeeFilterViewModel: NSObject {
    let placeholders = ["Designation", "Skills", "Experience", "City", "Company", "Industry", "Education Level", "Education Field"]
    var filterType = "employees"
    
    var searchedData = [[String:Any]]()
    var organisationData = [[String:Any]]()
    var dropDownData = [String]()
    
    var selectedServices: [String]!   // Not Used for now

    // Data for TagViews
    var selectedDesignation = [String]()
    var selectedInterests = [String]()
    var selectedOrganisation = [String]()
    var selectedIndustry = [String]()
    var selectedEduField = [String]()
    var selectedEduLevel = [String]()

    var selectedDesignationDict = [[String:Any]]()
    var selectedInterestsDict = [[String:Any]]()
    var selectedOrganisationDict = [[String:Any]]()
    var selectedIndustryDict = [[String:Any]]()
    var selectedEduFieldDict = [[String:Any]]()
    var selectedEduLevelDict = [[String:Any]]()
    
    var isSliderExpanded = false

    // MARK: - Search suggestions
    func getSearchConfig(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseSearchedData(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func getOrganisations(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getOrganisationConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseOrganisationData(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    // MARK: - Utility methods
    func parseSearchedData(data: [JSON]) {
        let search = data.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["id"] = json["id"].intValue
            dict["value"] = json["value"].string
            return dict
        }
        searchedData = search
        let drop = search.map { (dict) -> String in
            return (dict["value"] as? String) ?? ""
        }
        dropDownData = drop
    }
    func parseOrganisationData(data: [JSON]) {
        var filteredData = [JSON]()
        filteredData = data.filter({$0["type"].stringValue == "company"})
        let org = filteredData.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["organisation_id"] = json["organisation_id"].intValue
            dict["city"] = json["city"].string
            dict["organisation"] = json["organisation"].string
            return dict
        }
        organisationData = org
        let drop = org.map { (dict) -> String in
            return (dict["organisation"] as? String) ?? ""
        }
        dropDownData = drop
    }
    // MARK: - Search Filtered
    func getSearchFiltered(parameters: [String:Any], filter: String, count: Bool, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchFiltered(parameters: parameters, filter: filter, count: count, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"]
                print(data)
//                let results = data.map { (res) -> SearchResults? in
//                    let model = SearchResults(dictionary: res.dictionaryValue)
//                    return model
//                }
//                self.searchResults = results
                completion(true, data, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }

}
