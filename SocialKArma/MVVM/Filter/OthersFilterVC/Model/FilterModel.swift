//
//  FilterModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 27/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import SwiftyJSON

class FilterModel: NSObject {
    struct Static {
        fileprivate static var instance: FilterModel?
    }
    class var shared: FilterModel {
        if Static.instance == nil {
            Static.instance = FilterModel()
        }
        return Static.instance!
    }
    func dispose() {
        FilterModel.Static.instance = nil
        print("Disposed Singleton instance")
    }
    var paramDictionary = [String:Any]()
    // MARK: - Variables
    var gender = "" {
        didSet {
            paramDictionary["gender"] = gender
        }
    }
    // Score
    var minScore = 0 {
        didSet {
            paramDictionary["lowerscore"] = minScore
        }
    }
    var maxScore = 0 {
        didSet {
            paramDictionary["upperscore"] = maxScore
        }
    }
    
    var reviewParams = [Int:[String: Int]]() {
        didSet {
            var arr = [[String:Int]]()
            for i in reviewParams.values {
                arr.append(i)
            }
            paramDictionary["scoreParam"] = arr
        }
    }
    // Age
    var minAge = 0 {
        didSet {
            paramDictionary["lowerage"] = minAge
        }
    }
    var maxAge = 0 {
        didSet {
            paramDictionary["upperage"] = maxAge
        }
    }
    
    // TextFields
    var skills = [Int]() {
        didSet {
            paramDictionary["skills"] = skills
//            let dict = [["parameter_id": 1, "min": 2, "max": 4],["parameter_id": 2, "min": 3, "max": 4]]
//            paramDictionary["scoreParam"] = dict
        }
    }
    var professions = [Int]() {
        didSet {
            paramDictionary["professions"] = professions
        }
    }
    var interest = [Int]() {
        didSet {
            paramDictionary["interest"] = interest
        }
    }
    var industry = [Int]() {
        didSet {
            paramDictionary["industry"] = industry
        }
    }
    var jobTitle = [Int]() {
        didSet {
            paramDictionary["jobtype"] = jobTitle
        }
    }
    var experience = [0,1] {
        didSet {
            if experience.count > 0 {
                paramDictionary["upperworkex"] = experience[1]
                paramDictionary["lowerworkex"] = experience[0]
            }
        }
    }
    var professionalLevel = 0 {
        didSet {
            paramDictionary["professionalLevel"] = professionalLevel
        }
    }
    var educationLevel = [Int]() {
        didSet {
            paramDictionary["educationlevel"] = educationLevel
        }
    }
    var relationship = 0 {
        didSet {
            paramDictionary["relationship"] = relationship
        }
    }
    var fitness = 0 {
        didSet {
            paramDictionary["fitness"] = fitness
        }
    }
    var smoking = 0 {
        didSet {
            paramDictionary["smoking"] = smoking
        }
    }
    var drinking = 0 {
        didSet {
            paramDictionary["drinking"] = drinking
        }
    }
    var religion = 0 {
        didSet {
            paramDictionary["religion"] = religion
        }
    }
    // 27 jan
    var educationField = [Int]() {
        didSet {
            paramDictionary["educationfield"] = educationField
        }
    }
    var companyOrg = [Int]() {
        didSet {
            paramDictionary["organisation"] = companyOrg
        }
    }
    var serviceCategory = [Int]() {
        didSet {
            paramDictionary["service"] = serviceCategory
        }
    }
    var locationFilter = [String:Any]() {
        didSet {
            paramDictionary["city"] = nil
            
            paramDictionary["type"] = locationFilter["type"]
            paramDictionary["radius"] = locationFilter["radius"]
            paramDictionary["latitude"] = locationFilter["latitude"]
            paramDictionary["longitude"] = locationFilter["longitude"]
        }
    }
    var city = "" {
        didSet {
            paramDictionary["city"] = city
        }
    }
    // MARK: - Methods
    func resetValues() {
        
        gender = ""
        
        minScore = 0
        maxScore = 0
        
        // Parameters
        reviewParams = [:]

        // Age
        minAge = 0
        maxAge = 0
        
        // TextFields
        skills = []
        interest = []
        industry = []
        jobTitle = []
        experience = []
        professionalLevel = 0
        educationLevel = []
        educationField = []
        relationship = 0
        fitness = 0
        smoking = 0
        drinking = 0
        religion = 0
        
        // Dictionary
        paramDictionary = [:]
    }
    func dictionaryValues() -> [String:Any] {
        let dict = [String:Any]()
        return dict
    }
    func removeLocationValues() {
//        paramDictionary["type"] = nil
        paramDictionary["radius"] = nil
        paramDictionary["latitude"] = nil
        paramDictionary["longitude"] = nil
    }
}
