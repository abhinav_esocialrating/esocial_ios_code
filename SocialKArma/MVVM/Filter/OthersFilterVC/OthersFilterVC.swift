//
//  OthersFilterVC.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown
import IQKeyboardManagerSwift

class OthersFilterVC: UIViewController {
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var textfield1: ACFloatingTextfield!
    @IBOutlet weak var textfield2: ACFloatingTextfield!
    @IBOutlet weak var textfield3: ACFloatingTextfield!
    @IBOutlet weak var textfield4: ACFloatingTextfield!
    @IBOutlet weak var textfield5: ACFloatingTextfield!
    @IBOutlet weak var textfield6: ACFloatingTextfield!
    
    @IBOutlet weak var textfield7: ACFloatingTextfield!
    @IBOutlet weak var textfield8: ACFloatingTextfield!
    @IBOutlet weak var textfield9: ACFloatingTextfield!
    @IBOutlet weak var textfield10: ACFloatingTextfield!
    @IBOutlet weak var textfield11: ACFloatingTextfield!
    @IBOutlet weak var textfield12: ACFloatingTextfield!
    @IBOutlet weak var textfield13: ACFloatingTextfield!
    @IBOutlet weak var textfield14: ACFloatingTextfield!
    @IBOutlet weak var submitButton: UIButton!
    
    // TagViews
    @IBOutlet weak var skillsView: TagListView!
    @IBOutlet weak var skillsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var interestView: TagListView!
    @IBOutlet weak var interestViewHeight: NSLayoutConstraint!

    @IBOutlet weak var industryView: TagListView!
    @IBOutlet weak var industryViewHeight: NSLayoutConstraint!

    @IBOutlet weak var jobTypeView: TagListView!
    @IBOutlet weak var jobTypeViewHeight: NSLayoutConstraint!

    @IBOutlet weak var eduFieldView: TagListView!
    @IBOutlet weak var eduFieldViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var categoryView: TagListView!
    @IBOutlet weak var categoryViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var companyView: TagListView!
    @IBOutlet weak var companyViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var eduLevelView: TagListView!
    @IBOutlet weak var eduLevelViewHeight: NSLayoutConstraint!

    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var countView: UIView!

    // Sliders
    @IBOutlet weak var ageSlider: TTRangeSliderCircle!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var locationSlider: TTRangeSliderCircle!
    @IBOutlet weak var ageRangeSlider: RangeSeekSlider!
    
    @IBOutlet weak var locationRangeSlider: RangeSeekSlider!

    @IBOutlet weak var sliderViewHeight: NSLayoutConstraint!
    // MARK: - CollectionViews
    
    @IBOutlet weak var genderCV: UICollectionView!
    @IBOutlet weak var smokingCV: UICollectionView!
    @IBOutlet weak var drinkingCV: UICollectionView!

    // MARK: - Variables
    let viewModel = OthersFilterViewModel()
    let dropDown = DropDown()
    var passData: SendData!
    let reachability = try! Reachability()
    var commonFilterView: CommonRangeSliderView!
    // Slider
    var distance = 0;
    var radius = 0;
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        do{
          try reachability.startNotifier()
        }catch{
          print("could not start reachability notifier")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dropDown.hide()
        self.view.endEditing(true)
    }
    func configureView() {
        setUpTextFields()
        setUpSlider()
        setUpTagViews()
        setUpCollectionViews()
        
        countView.layer.cornerRadius = countView.frame.height/2

        commonFilterView = CommonRangeSliderView.instantiateFromNib()
        ratingView.addSubview(commonFilterView)
        commonFilterView.anchor(top: commonFilterView.superview?.topAnchor, paddingTop: 0, bottom: commonFilterView.superview?.bottomAnchor, paddingBottom: 0, left: commonFilterView.superview?.leftAnchor, paddingLeft: 0, right: commonFilterView.superview?.rightAnchor, paddingRight: 0, width: 0, height: 0)
        commonFilterView.passData = self
        commonFilterView.setUpSlider()
        
        var attributedPlaceholder = Utilities.attributedText(text: Constants.CategoryPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield3.attributedPlaceholder = attributedPlaceholder
        
        attributedPlaceholder = Utilities.attributedText(text: Constants.SkillsPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield4.attributedPlaceholder = attributedPlaceholder
        
        attributedPlaceholder = Utilities.attributedText(text: Constants.ExperiencePlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield5.attributedPlaceholder = attributedPlaceholder
        
        attributedPlaceholder = Utilities.attributedText(text: Constants.IndustryPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield6.attributedPlaceholder = attributedPlaceholder
        
        attributedPlaceholder = Utilities.attributedText(text: Constants.InterestsPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield9.attributedPlaceholder = attributedPlaceholder
        
        attributedPlaceholder = Utilities.attributedText(text: Constants.EducationPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield11.attributedPlaceholder = attributedPlaceholder
        
        attributedPlaceholder = Utilities.attributedText(text: Constants.fitnessPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield10.attributedPlaceholder = attributedPlaceholder

        attributedPlaceholder = Utilities.attributedText(text: Constants.EducationPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield11.attributedPlaceholder = attributedPlaceholder

    }
    func setUpTextFields() {
        var array1 = [textfield5, textfield8, textfield10]
        
        array1.forEach { (textField) in
            textField?.inputView = UIView()
        }
        
        array1 = [textfield1, textfield2, textfield3, textfield4, textfield6, textfield7, textfield9, textfield11, textfield12]
        array1.forEach { (textField) in
            textField?.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
        }
        
        array1 = [textfield1, textfield2, textfield8, textfield3, textfield4, textfield5, textfield6, textfield7, textfield9, textfield10, textfield11, textfield12]
        for (_,textField) in array1.enumerated() {
            textField?.lineColor = UIColor.NewTheme.darkGray
            textField?.selectedLineColor = UIColor.NewTheme.paleBlue
            textField?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
            textField?.textColor = UIColor.NewTheme.darkGray
            textField?.disableFloatingLabel = true
            textField?.delegate = self
        }
        
    }
    func setUpSlider() {
//        let array = [ageSlider, locationSlider]
//        for slider in array {
//            slider?.delegate = self
//            slider?.minLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 11)
//            slider?.maxLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 11)
//        }
        
        let array = [ageRangeSlider, locationRangeSlider]
        for slider in array {
            slider?.delegate = self
            slider?.minLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 11)
            slider?.maxLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 11)
        }

    }
    func setUpTagViews() {
        let arr = [interestView, eduFieldView, industryView, skillsView, jobTypeView, categoryView, eduLevelView, companyView]
        arr.forEach { (tag) in
            tag?.delegate = self
            tag?.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
        }
    }
    func setUpCollectionViews() {
        let array = [genderCV, smokingCV, drinkingCV]
        array.forEach { (cv) in
            cv?.dataSource = self
            cv?.delegate = self
            cv?.register(UINib(nibName: CollectionViewCells.FilterTagCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CollectionViewCells.FilterTagCell.rawValue)
        }
    }

    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
//        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func textFieldChanged(_ textField: UITextField) {
        var params = ["config": "skills", "search": textField.text!]
        switch textField {
        case textfield1:
            if textField.text!.count > 2 {
                LocationManager.shared.getCities(query: textField.text!) { (cities, cityNames) in
                    self.viewModel.dropDownData = cities
                    self.viewModel.cityNames = cityNames
                    DispatchQueue.main.async {
                        self.showDropDown(textField: textField)
                    }
                }
            }
        case textfield3:
            params = ["config": "business_service", "search": textField.text!, "type": CategoryFilterType.all.rawValue]
        case textfield2:
            params = ["config": "designation", "search": textField.text!]
        case textfield4:
            params = ["config": "skill", "search": textField.text!]
        case textfield9:
            params = ["config": "interest", "search": textField.text!]
        case textfield6:
            params = ["config": "industry_type", "search": textField.text!]
        case textfield12:
            params = ["config": "degree_field", "search": textField.text!]
        case textfield11:
            params = ["config": "degree_level", "search": textField.text!]
        case textfield7: // organisation
            params = ["search": textField.text!]
        default:
            print(textField.text!)
        }
        if textField == textfield1 {
        } else if textField == textfield7 {
            viewModel.getOrganisations(parameters: params) { (success, json, message) in
                if success {
                    DispatchQueue.main.async {
                        if self.viewModel.organisationData.count > 0 {
                            self.showDropDown(textField: textField)
                        } else {
                            self.dropDown.hide()
                        }
                    }
                }
            }
        } else {
            viewModel.getSearchConfig(parameters: params) { (success, json, message) in
                if success {
                    DispatchQueue.main.async {
                        if self.viewModel.searchedData.count > 0 {
                            self.showDropDown(textField: textField)
                        }else {
                            self.dropDown.hide()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func applyTapped(_ sender: Any) {
        FilterModel.shared.paramDictionary["type"] = viewModel.filterType
        passData.passData(dataPasser: self, data: FilterModel.shared.paramDictionary)
        navigationController?.popViewController(animated: true)
    }
    @IBAction func resetTapped(_ sender: Any) {
        resetFiltersAndViews()
    }
    // MARK: - Helper methods
    func updateUI(section: Int) {
        DispatchQueue.main.async {
//            self.skillTableView.reloadData()
        }
    }
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        switch textField {
        case textfield5:
            dropDown.dataSource = CommonData.experience
        case textfield8:
            dropDown.dataSource = ConfigDataModel.shared.relationship_status.map({ $0["value"]?.stringValue ?? "" })
        case textfield10:
            dropDown.dataSource = ConfigDataModel.shared.fitness.map({ $0["value"]?.stringValue ?? "" })
        default:
            dropDown.dataSource = viewModel.dropDownData
        }
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropDownSelection(index: index, item: item, textField: textField)
        }
        dropDown.show()
    }
    func handleDropDownSelection(index: Int, item: String, textField: UITextField) {
        switch textField {
        case textfield1: // City
            textField.text = item
            FilterModel.shared.removeLocationValues()
            locationRangeSlider.selectedMaxValue = 1
            locationRangeSlider.layoutSubviews()
            distanceLabel.text = "5 m"
            FilterModel.shared.city = viewModel.cityNames[index]
        case textfield2:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.jobTitle.contains(skillId) {
                    self.viewModel.selectedJobTitleDict.append( self.viewModel.searchedData[index])
                    self.updateSkillsTagView(tag: item, textField: textField)
                    FilterModel.shared.jobTitle.append(skillId)
                }
            }
        case textfield3:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.serviceCategory.contains(skillId) {
                    self.viewModel.selectedCategoryDict.append( self.viewModel.searchedData[index])
                    self.updateSkillsTagView(tag: item, textField: textField)
                    FilterModel.shared.serviceCategory.append(skillId)
                }
            }
        case textfield4:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.skills.contains(skillId) {
                    self.viewModel.selectedSkillsDict.append( self.viewModel.searchedData[index])
                    self.updateSkillsTagView(tag: item, textField: textField)
                    FilterModel.shared.skills.append(skillId)
                }
            }
        case textfield5:
            textField.text = item
            textField.resignFirstResponder()
            FilterModel.shared.experience = Utilities().getExperience(index: index)
        case textfield6:
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.industry.contains(skillId) {
                    self.viewModel.selectedIndustryDict.append( self.viewModel.searchedData[index])
                    self.updateSkillsTagView(tag: item, textField: textField)
                    FilterModel.shared.industry.append(skillId)
                }
            }
        case textfield7:
            if let skillId = self.viewModel.organisationData[index]["organisation_id"] as? Int {
                if !FilterModel.shared.companyOrg.contains(skillId) {
                    self.viewModel.selectedOrganisationDict.append( self.viewModel.organisationData[index])
                    self.updateSkillsTagView(tag: item, textField: textField)
                    FilterModel.shared.companyOrg.append(skillId)
                }
            }
        case textfield8:
            textField.text = item
            textField.resignFirstResponder()
            FilterModel.shared.relationship = ConfigDataModel.shared.relationship_status[index]["id"]?.intValue ?? 0
        case textfield9:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.interest.contains(skillId) {
                    self.viewModel.selectedInterestsDict.append( self.viewModel.searchedData[index])
                    self.updateSkillsTagView(tag: item, textField: textField)
                    FilterModel.shared.interest.append(skillId)
                }
            }
        case textfield10:
            textField.text = item
            textField.resignFirstResponder()
            FilterModel.shared.fitness = ConfigDataModel.shared.fitness[index]["id"]?.intValue ?? 0
        case textfield11:
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.educationLevel.contains(skillId) {
                    self.viewModel.selectedEduLevelDict.append( self.viewModel.searchedData[index])
                    self.updateSkillsTagView(tag: item, textField: textField)
                    FilterModel.shared.educationLevel.append(skillId)
                }
            }
        default:
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.educationField.contains(skillId) {
                    self.viewModel.selectedEduFieldDict.append( self.viewModel.searchedData[index])
                    self.updateSkillsTagView(tag: item, textField: textField)
                    FilterModel.shared.educationField.append(skillId)
                }
            }
        }
        self.getSearchFiltered()
    }
    func updateSkillsTagView(tag: String, textField: UITextField) {
        switch textField {
        case textfield3:
            categoryView.addTag(tag)
            categoryViewHeight.constant = categoryView.intrinsicContentSize.height
        case textfield4:
            skillsView.addTag(tag)
            skillsViewHeight.constant = skillsView.intrinsicContentSize.height
        case textfield6:
            industryView.addTag(tag)
            industryViewHeight.constant = industryView.intrinsicContentSize.height
        case textfield2:
            jobTypeView.addTag(tag)
            jobTypeViewHeight.constant = jobTypeView.intrinsicContentSize.height
        case textfield12:
            eduFieldView.addTag(tag)
            eduFieldViewHeight.constant = eduFieldView.intrinsicContentSize.height
        case textfield7:
            companyView.addTag(tag)
            companyViewHeight.constant = companyView.intrinsicContentSize.height
        case textfield9:
            interestView.addTag(tag)
            interestViewHeight.constant = interestView.intrinsicContentSize.height
        default:
            eduLevelView.addTag(tag)
            eduLevelViewHeight.constant = eduLevelView.intrinsicContentSize.height
        }
        
    }
    func updateSliderViewHeight() {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.sliderViewHeight.constant = self.viewModel.isSliderExpanded ? 1000 : 210
            self.view.layoutIfNeeded()
        }
    }
    func updateCountView(count: Int) {
        DispatchQueue.main.async {
            self.countView.isHidden = false
            self.countLabel.text = "\(count) Results found"
        }
    }
    func resetFiltersAndViews() {
        view.endEditing(true)
        // Reset filter model
        FilterModel.shared.resetValues()
        // Reset textfields
        let textfields = [textfield1, textfield2, textfield8, textfield3, textfield4, textfield5, textfield6, textfield7, textfield9, textfield10, textfield11, textfield12]
        textfields.forEach { (textfield) in
            textfield?.text = ""
        }
        // Reset Sliders
        ageRangeSlider.selectedMinValue = 16
        ageRangeSlider.selectedMaxValue = 100
        locationRangeSlider.selectedMaxValue = 1
        distanceLabel.text = "5 m"
        
        ageRangeSlider.layoutSubviews()
        locationRangeSlider.layoutSubviews()
                
        // TagListViews
        skillsView.removeAllTags()
        eduLevelView.removeAllTags()
        interestView.removeAllTags()
        industryView.removeAllTags()
        eduFieldView.removeAllTags()
        companyView.removeAllTags()
        jobTypeView.removeAllTags()
        categoryView.removeAllTags()
        
        skillsViewHeight.constant = 0
        eduLevelViewHeight.constant = 0
        interestViewHeight.constant = 0
        industryViewHeight.constant = 0
        eduFieldViewHeight.constant = 0
        companyViewHeight.constant = 0
        jobTypeViewHeight.constant = 0
        categoryViewHeight.constant = 0
        
        // CollectionViews
        genderCV.reloadData()
        smokingCV.reloadData()
        drinkingCV.reloadData()
        
        // Count Label
        countView.isHidden = true
        
        // Score Sliders
        commonFilterView.resetValues()
    }
    // MARK: - Web Services
    func getSearchFiltered() {
        if reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            FilterModel.shared.paramDictionary["type"] = viewModel.filterType
            FilterModel.shared.paramDictionary["latitude"] = LocationManager.shared.lastLocation?.coordinate.latitude
            FilterModel.shared.paramDictionary["longitude"] = LocationManager.shared.lastLocation?.coordinate.longitude
            print(FilterModel.shared.paramDictionary)
            self.viewModel.getSearchFiltered(parameters: FilterModel.shared.paramDictionary, filter: "friends", count: true) { (success, json, message) in
                if success {
                    let count = json.intValue
                    self.updateCountView(count: count)
                }
            }
        }
    }
}
// MARK: - Slider methods
extension OthersFilterVC: TTRangeSliderCircleDelegate {
    func didEndTouches(inRangeSlider sender: TTRangeSliderCircle!) {
        if sender == ageSlider {
            let min = sender.selectedMaximum
            let max = sender.selectedMinimum
            let intMin = Int(min)
            let intMax = Int(max)
            FilterModel.shared.minAge = intMin
            FilterModel.shared.maxAge = intMax
            getSearchFiltered()
        } else {
            if Int(sender.selectedMaximum) == 1 {
                sender.selectedMaximum = 1;
            } else if (sender.selectedMaximum >= 21 && sender.selectedMaximum <= 27) {
                sender.selectedMaximum = 27;
            } else if (sender.selectedMaximum >= 28 && sender.selectedMaximum <= 35) {
                sender.selectedMaximum = 35;
            } else if (sender.selectedMaximum >= 36 && sender.selectedMaximum <= 43) {
                sender.selectedMaximum = 43;
            } else if (sender.selectedMaximum >= 44 && sender.selectedMaximum <= 49) {
                sender.selectedMaximum = 49;
            } else if (sender.selectedMaximum > 98) {
                sender.selectedMaximum = 100;
            }
            textfield1.text = ""
            getSearchLocationFiltered()
        }
    }
    func rangeSlider(_ sender: TTRangeSliderCircle!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float) {
        if sender == locationSlider {
            let selectedMaxInt = Int(sender.selectedMaximum)
            if sender.selectedMaximum < 1 {
                radius = 5
                distanceLabel.text = "5 m"
            } else if (selectedMaxInt == 1) {
                radius = 5
                distanceLabel.text = "5 m"
            } else if (selectedMaxInt >= 2 && selectedMaxInt <= 20) {
                radius = selectedMaxInt * 5
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 21 && selectedMaxInt <= 27) {
                radius = 200;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 28 && selectedMaxInt <= 35) {
                radius = 400;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 36 && selectedMaxInt <= 43) {
                radius = 600;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 44 && selectedMaxInt <= 49) {
                radius = 800;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt == 50) {
                radius = 1000;
                distanceLabel.text = "1 km"
            } else if (selectedMaxInt > 98) {
                distanceLabel.text = "\(selectedMaxInt / 2) km"
            } else {
                let distance = getDistance(progress: selectedMaxInt)
                radius = selectedMaxInt * 100;
                distanceLabel.text = "\(distance) km"
            }
        }
    }
}
// MARK: - RangeSlider methods
extension OthersFilterVC: RangeSeekSliderDelegate {
    func didEndTouches(in slider: RangeSeekSlider) {
        if slider == ageRangeSlider {
            let min = slider.selectedMinValue
            let max = slider.selectedMaxValue
            let intMin = Int(min)
            let intMax = Int(max)
            FilterModel.shared.minAge = intMin
            FilterModel.shared.maxAge = intMax
            getSearchFiltered()
        } else {
            if Int(slider.selectedMaxValue) == 1 {
                slider.selectedMaxValue = 1;
            } else if (slider.selectedMaxValue >= 21 && slider.selectedMaxValue <= 27) {
                slider.selectedMaxValue = 27;
            } else if (slider.selectedMaxValue >= 28 && slider.selectedMaxValue <= 35) {
                slider.selectedMaxValue = 35;
            } else if (slider.selectedMaxValue >= 36 && slider.selectedMaxValue <= 43) {
                slider.selectedMaxValue = 43;
            } else if (slider.selectedMaxValue >= 44 && slider.selectedMaxValue <= 49) {
                slider.selectedMaxValue = 49;
            } else if (slider.selectedMaxValue > 98) {
                slider.selectedMaxValue = 100;
            }
            getSearchLocationFiltered()
        }
    }
    func processSliderRating(value: Int) -> Int {
        return (abs(value/20) + 1)
    }
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider == locationRangeSlider {
            let selectedMaxInt = Int(slider.selectedMaxValue)
            if slider.selectedMaxValue < 1 {
                radius = 5
                distanceLabel.text = "5 m"
            } else if (selectedMaxInt == 1) {
                radius = 5
                distanceLabel.text = "5 m"
            } else if (selectedMaxInt >= 2 && selectedMaxInt <= 20) {
                radius = selectedMaxInt * 5
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 21 && selectedMaxInt <= 27) {
                radius = 200;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 28 && selectedMaxInt <= 35) {
                radius = 400;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 36 && selectedMaxInt <= 43) {
                radius = 600;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 44 && selectedMaxInt <= 49) {
                radius = 800;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt == 50) {
                radius = 1000;
                distanceLabel.text = "1 km"
            } else if (selectedMaxInt > 98) {
                radius = (selectedMaxInt/2) * 1000;
                distanceLabel.text = "\(selectedMaxInt / 2) km"
            } else {
                let distance = getDistance(progress: selectedMaxInt)
                radius = distance * 100;
                distanceLabel.text = "\(distance) km"
            }
        }
    }
    func getDistance(progress: Int) -> Int {
        if (progress % 10 != 0) {
            distance = progress % 50;
        }
        return distance;
    }
    func getSearchLocationFiltered() {
        var locationfiler = [String:Any]()
        if let lastLocation = LocationManager.shared.lastLocation {
            locationfiler["latitude"] = lastLocation.coordinate.latitude
            locationfiler["longitude"] = lastLocation.coordinate.longitude
        }
        locationfiler["type"] = viewModel.filterType
        locationfiler["radius"] = radius
        FilterModel.shared.locationFilter = locationfiler
        getSearchFiltered()
    }
}



// MARK: - TextFieldDelegate
extension OthersFilterVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textfield5 || textField == textfield8 || textField == textfield10 {
            showDropDown(textField: textField)
            return false
        }
        textField.text = "a"
        textFieldChanged(textField)
        textField.text = ""
        return true
    }
}
// MARK: - TagListView delegates
extension OthersFilterVC: TagListViewDelegate {
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        switch sender {
        case skillsView:
            skillsView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedSkillsDict, tagView: sender)
            skillsViewHeight.constant = skillsView.intrinsicContentSize.height
        case industryView:
            industryView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedIndustryDict, tagView: sender)
            industryViewHeight.constant = industryView.intrinsicContentSize.height
        case jobTypeView:
            jobTypeView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedJobTitleDict, tagView: sender)
            jobTypeViewHeight.constant = jobTypeView.intrinsicContentSize.height
        case companyView:
            companyView.removeTag(title)
            checkForStringInOrgTagViewData(tag: title, tagArray: &viewModel.selectedOrganisationDict, tagView: sender)
            companyViewHeight.constant = companyView.intrinsicContentSize.height
        case eduFieldView:
            eduFieldView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedEduFieldDict, tagView: sender)
            eduFieldViewHeight.constant = eduFieldView.intrinsicContentSize.height
        case categoryView:
            categoryView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedCategoryDict, tagView: sender)
            categoryViewHeight.constant = categoryView.intrinsicContentSize.height
        case eduLevelView:
            eduLevelView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedEduLevelDict, tagView: sender)
            eduLevelViewHeight.constant = eduLevelView.intrinsicContentSize.height
        default:
            interestView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedInterestsDict, tagView: sender)
            interestViewHeight.constant = interestView.intrinsicContentSize.height
        }
    }
    func checkForStringInTagViewData(tag: String, tagArray: inout [[String:Any]], tagView: TagListView) {
        for (j,i) in tagArray.enumerated() {
            if let id = i["id"] as? Int, let value = i["value"] as? String {
                if value == tag {
                    switch tagView {
                    case skillsView:
                        FilterModel.shared.skills = FilterModel.shared.skills.filter({$0 != id})
                    case industryView:
                        FilterModel.shared.industry = FilterModel.shared.industry.filter({$0 != id})
                    case jobTypeView:
                        FilterModel.shared.jobTitle = FilterModel.shared.jobTitle.filter({$0 != id})
                    case interestView:
                        FilterModel.shared.interest = FilterModel.shared.interest.filter({$0 != id})
                    case categoryView:
                        FilterModel.shared.serviceCategory = FilterModel.shared.serviceCategory.filter({$0 != id})
                    case eduLevelView:
                        FilterModel.shared.educationLevel = FilterModel.shared.educationLevel.filter({$0 != id})
                    default:
                        FilterModel.shared.educationField = FilterModel.shared.educationField.filter({$0 != id})
                    }
                    tagArray.remove(at: j)
                    self.getSearchFiltered()
                    break
                }
            }
        }
    }
    func checkForStringInOrgTagViewData(tag: String, tagArray: inout [[String:Any]], tagView: TagListView) {
        for (j,i) in tagArray.enumerated() {
            if let id = i["organisation_id"] as? Int, let value = i["organisation"] as? String {
                if value == tag {
                    switch tagView {
                    case companyView:
                        FilterModel.shared.companyOrg = FilterModel.shared.companyOrg.filter({$0 != id})
                    default:
                        print("Other than organisation")
                    }
                    tagArray.remove(at: j)
                    self.getSearchFiltered()
                    break
                }
            }
        }
    }
}
// MARK: - SendData Delegates
extension OthersFilterVC: SendData {
    func passData(dataPasser: Any, data: Any) {
        if data is Bool {
            viewModel.isSliderExpanded = !viewModel.isSliderExpanded
            updateSliderViewHeight()
        } else {
            getSearchFiltered()
        }
    }
}
// MARK: - CollectionView methods
extension OthersFilterVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        print(ConfigDataModel.shared.habitFrequency)
        switch collectionView {
        case genderCV:
            return CommonData.genderAll.count
        default:
            return ConfigDataModel.shared.habitFrequency.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCells.FilterTagCell.rawValue, for: indexPath) as! FilterTagCell
        switch collectionView {
        case genderCV:
            cell.label.text = CommonData.genderAll[indexPath.row]
            cell.isCellSelected = CommonData.genderAll[indexPath.row] == FilterModel.shared.gender
        case smokingCV:
            cell.label.text = ConfigDataModel.shared.habitFrequency[indexPath.row]["value"]?.string ?? ""
            cell.isCellSelected = FilterModel.shared.smoking == ConfigDataModel.shared.habitFrequency[indexPath.row]["id"]?.intValue ?? 0
        default:
            cell.label.text = ConfigDataModel.shared.habitFrequency[indexPath.row]["value"]?.string ?? ""
            cell.isCellSelected = FilterModel.shared.drinking == ConfigDataModel.shared.habitFrequency[indexPath.row]["id"]?.intValue ?? 0
        }
        cell.layoutSubviews()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (screenWidth-92)/2
        return CGSize(width: width, height: 44)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case genderCV:
            FilterModel.shared.gender = CommonData.genderAll[indexPath.row]
        case smokingCV:
            FilterModel.shared.smoking = ConfigDataModel.shared.habitFrequency[indexPath.row]["id"]?.intValue ?? 0
        default:
            FilterModel.shared.drinking = ConfigDataModel.shared.habitFrequency[indexPath.row]["id"]?.intValue ?? 0
        }
        collectionView.reloadData()
        getSearchFiltered()
    }
}
