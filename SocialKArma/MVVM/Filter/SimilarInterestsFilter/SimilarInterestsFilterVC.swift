//
//  SimilarInterestsFilterVC.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class SimilarInterestsFilterVC: UIViewController {
    
    @IBOutlet weak var ratingView: UIView!
    
    @IBOutlet weak var textfield1: ACFloatingTextfield!
    @IBOutlet weak var textfield2: ACFloatingTextfield!
    @IBOutlet weak var textfield3: ACFloatingTextfield!
    @IBOutlet weak var textfield4: ACFloatingTextfield!
    @IBOutlet weak var textfield5: ACFloatingTextfield!
    @IBOutlet weak var textfield6: ACFloatingTextfield!
    @IBOutlet weak var textfield7: ACFloatingTextfield!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var categoryView: TagListView!
    @IBOutlet weak var categoryViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var interestView: TagListView!
    @IBOutlet weak var interestViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var industryView: TagListView!
    @IBOutlet weak var industryViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var eduLevelView: TagListView!
    @IBOutlet weak var eduLevelViewHeight: NSLayoutConstraint!
        
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var countView: UIView!

    @IBOutlet weak var locationSlider: TTRangeSliderCircle!
    @IBOutlet weak var locationRangeSlider: RangeSeekSlider!

    @IBOutlet weak var ageSlider: TTRangeSliderCircle!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var sliderViewHeight: NSLayoutConstraint!

    @IBOutlet weak var infoLabel: UILabel!
    // MARK: - Variables
    let viewModel = SimilarInterestsViewModel()
    let dropDown = DropDown()
    var passData: SendData!
    let reachability = try! Reachability()
    var commonFilterView: CommonRangeSliderView!
    // Slider
    var distance = 0;
    var radius = 0;
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        do{
          try reachability.startNotifier()
        }catch{
          print("could not start reachability notifier")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dropDown.hide()
        self.view.endEditing(true)
    }
    func configureView() {
        setUpTextFields()
        setUpSlider()
        setUpTagViews()
        
        countView.layer.cornerRadius = countView.frame.height/2

        commonFilterView = CommonRangeSliderView.instantiateFromNib()
        ratingView.addSubview(commonFilterView)
        commonFilterView.anchor(top: commonFilterView.superview?.topAnchor, paddingTop: 0, bottom: commonFilterView.superview?.bottomAnchor, paddingBottom: 0, left: commonFilterView.superview?.leftAnchor, paddingLeft: 0, right: commonFilterView.superview?.rightAnchor, paddingRight: 0, width: 0, height: 0)
        commonFilterView.passData = self
        commonFilterView.setUpSlider()
        
        // Placeholders
        var attributedPlaceholder = Utilities.attributedText(text: Constants.CategoryPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield1.attributedPlaceholder = attributedPlaceholder
        
        attributedPlaceholder = Utilities.attributedText(text: Constants.SkillsPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield2.attributedPlaceholder = attributedPlaceholder
        
        attributedPlaceholder = Utilities.attributedText(text: Constants.ExperiencePlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield3.attributedPlaceholder = attributedPlaceholder

        attributedPlaceholder = Utilities.attributedText(text: Constants.IndustryPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield4.attributedPlaceholder = attributedPlaceholder
        
        attributedPlaceholder = Utilities.attributedText(text: Constants.EducationPlaceholder.rawValue, font: UIFont.fontWith(name: .Roboto, face: .Regular, size: CommonData.filterPlaceholderSize), color: .lightGray)
        textfield6.attributedPlaceholder = attributedPlaceholder
    }
    func setUpTextFields() {
        let array = [textfield3]
        
        array.forEach { (textField) in
            textField?.inputView = UIView()
        }
        
        let array2 = [textfield1, textfield2, textfield4, textfield5, textfield6]
        array2.forEach { (textField) in
            textField?.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
        }
        
        let array3 = [textfield1, textfield2, textfield3, textfield4, textfield5, textfield6]
        for (i, textField) in array3.enumerated() {
            textField?.delegate = self
            textField?.lineColor = UIColor.NewTheme.darkGray
            textField?.selectedLineColor = UIColor.NewTheme.paleBlue
            textField?.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
            textField?.textColor = UIColor.NewTheme.darkGray
            textField?.disableFloatingLabel = true
        }
        
    }
    func setUpSlider() {
//        let array = [locationSlider]
//        for slider in array {
//            slider?.delegate = self
//            slider?.minLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 11)
//            slider?.maxLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 11)
//        }
        let array = [locationRangeSlider]
        for slider in array {
            slider?.delegate = self
            slider?.minLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 11)
            slider?.maxLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 11)
        }

    }
    func setUpTagViews() {
        let arr = [interestView, categoryView, industryView, eduLevelView]
        arr.forEach { (tag) in
            tag?.delegate = self
            tag?.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
        }
    }

    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func applyTapped(_ sender: Any) {
        if reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            FilterModel.shared.paramDictionary["type"] = viewModel.filterType
            passData.passData(dataPasser: self, data: FilterModel.shared.paramDictionary)
            navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func resetTapped(_ sender: Any) {
        resetFiltersAndViews()
    }
    @objc func textFieldChanged(_ textField: UITextField) {
        var params = [String:String]()
        switch textField {
        case textfield1:
            params = ["config": "business_service", "type": viewModel.categoryType, "search": textField.text!]
        case textfield2:
            params = ["config": "skill", "search": textField.text!]
        case textfield5:
            if textField.text!.count > 2 {
                LocationManager.shared.getCities(query: textField.text!) { (cities, cityNames) in
                    self.viewModel.dropDownData = cities
                    DispatchQueue.main.async {
                        self.showDropDown(textField: textField)
                    }
                }
            }
        case textfield4:
            params = ["config": "industry_type", "search": textField.text!]
        
        default:
            params = ["config": "degree_level", "search": textField.text!]
        }
        if textField != textfield5 { //&& textField.text!.count >= 2 {
            viewModel.getSearchConfig(parameters: params) { (success, json, message) in
                if success {
                    DispatchQueue.main.async {
                        if self.viewModel.searchedData.count > 0 {
                            self.showDropDown(textField: textField)
                        }else {
                            self.dropDown.hide()
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Methods
    func showDropDown(textField: UITextField) {
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = (dropDown.anchorView?.plainView.bounds.width)!
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        switch textField {
        case textfield1, textfield2, textfield4, textfield5, textfield6:
            dropDown.dataSource = viewModel.dropDownData
        case textfield3:
            dropDown.dataSource = CommonData.experience
        default:  // textfield6 : Education level
            dropDown.dataSource = []
        }
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropDownSelection(index: index, item: item, textField: textField)
        }
        dropDown.show()
    }
    
    func handleDropDownSelection(index: Int, item: String, textField: UITextField) {
        switch textField {
        case textfield1:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.serviceCategory.contains(skillId) {
                    self.viewModel.selectedCategoryDict.append( self.viewModel.searchedData[index])
                    self.updateTagViews(tag: item, textField: textField)
                    FilterModel.shared.serviceCategory.append(skillId)
                }
            }
        case textfield2:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.skills.contains(skillId) {
                    self.viewModel.selectedInterestsDict.append( self.viewModel.searchedData[index])
                    self.updateTagViews(tag: item, textField: textField)
                    FilterModel.shared.skills.append(skillId)
                }
            }
        case textfield3:
            textField.text = item
            FilterModel.shared.experience = Utilities().getExperience(index: index)
        case textfield5:  // City
            textField.text = item
            FilterModel.shared.removeLocationValues()
            locationRangeSlider.selectedMaxValue = 1
            locationRangeSlider.layoutSubviews()
            distanceLabel.text = "5 m"
            FilterModel.shared.city = item
        case textfield4:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.industry.contains(skillId) {
                    self.viewModel.selectedIndustryDict.append( self.viewModel.searchedData[index])
                    self.updateTagViews(tag: item, textField: textField)
                    FilterModel.shared.industry.append(skillId)
                }
            }
        default:
            textField.text = ""
            if let skillId = self.viewModel.searchedData[index]["id"] as? Int {
                if !FilterModel.shared.educationLevel.contains(skillId) {
                    self.viewModel.selectedEduLevelDict.append( self.viewModel.searchedData[index])
                    self.updateTagViews(tag: item, textField: textField)
                    FilterModel.shared.educationLevel.append(skillId)
                }
            }
        }
        self.getSearchFiltered()
    }
    func updateTagViews(tag: String, textField: UITextField) {
        switch textField {
        case textfield2:
            interestView.addTag(tag)
            interestViewHeight.constant = interestView.intrinsicContentSize.height
        case textfield4:
            industryView.addTag(tag)
            industryViewHeight.constant = industryView.intrinsicContentSize.height
        case textfield1:
            categoryView.addTag(tag)
            categoryViewHeight.constant = categoryView.intrinsicContentSize.height
        default:
            eduLevelView.addTag(tag)
            eduLevelViewHeight.constant = eduLevelView.intrinsicContentSize.height
        }
    }
    func updateSliderViewHeight() {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.sliderViewHeight.constant = self.viewModel.isSliderExpanded ? 1000 : 210
            self.view.layoutIfNeeded()
        }
    }
    func updateCountView(count: Int) {
        DispatchQueue.main.async {
            self.countView.isHidden = false
            self.countLabel.text = "\(count) Results found"
        }
    }
    func resetFiltersAndViews() {
        view.endEditing(true)
        // Reset filter model
        FilterModel.shared.resetValues()
        // Reset textfields
        let textfields = [textfield1, textfield2, textfield3, textfield4, textfield5]
        textfields.forEach { (textfield) in
            textfield?.text = ""
        }
        // Reset Sliders
        locationRangeSlider.selectedMaxValue = 1
        distanceLabel.text = "5 m"
        
        locationRangeSlider.layoutSubviews()
                
        // TagListViews
        eduLevelView.removeAllTags()
        interestView.removeAllTags()
        categoryView.removeAllTags()
        industryView.removeAllTags()
        eduLevelViewHeight.constant = 0
        interestViewHeight.constant = 0
        categoryViewHeight.constant = 0
        industryViewHeight.constant = 0
        
        // Count Label
        countView.isHidden = true
        
        // Score Sliders
        commonFilterView.resetValues()
    }
    // MARK: - Web Services
    func getSearchFiltered() {
        if reachability.connection == .unavailable {
            alert(message: Errors.NetworkNotAvailable.rawValue)
        } else {
            FilterModel.shared.paramDictionary["type"] = viewModel.filterType
            FilterModel.shared.paramDictionary["latitude"] = LocationManager.shared.lastLocation?.coordinate.latitude
            FilterModel.shared.paramDictionary["longitude"] = LocationManager.shared.lastLocation?.coordinate.longitude
            print(FilterModel.shared.paramDictionary)
            self.viewModel.getSearchFiltered(parameters: FilterModel.shared.paramDictionary, filter: "friends", count: true) { (success, json, message) in
                if success {
                    let count = json.intValue
                    self.updateCountView(count: count)
                }
            }
        }
    }

}
// MARK: - TextFieldDelegate
extension SimilarInterestsFilterVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textfield3 {
            showDropDown(textField: textField)
            return false
        }
//        else if textField == textfield4 || textField == textfield5 || textField == textfield1 || textField == textfield2 || textField == textfield6 {
//        }
        textField.text = "a"
        textFieldChanged(textField)
        textField.text = ""
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textfield4 || textField == textfield5 || textField == textfield6 {
//            dropDown.hide()
        }
    }
}


// MARK: - RangeSlider methods
extension SimilarInterestsFilterVC: RangeSeekSliderDelegate {
    func didEndTouches(in slider: RangeSeekSlider) {
        if Int(slider.selectedMaxValue) == 1 {
            slider.selectedMaxValue = 1;
        } else if (slider.selectedMaxValue >= 21 && slider.selectedMaxValue <= 27) {
            slider.selectedMaxValue = 27;
        } else if (slider.selectedMaxValue >= 28 && slider.selectedMaxValue <= 35) {
            slider.selectedMaxValue = 35;
        } else if (slider.selectedMaxValue >= 36 && slider.selectedMaxValue <= 43) {
            slider.selectedMaxValue = 43;
        } else if (slider.selectedMaxValue >= 44 && slider.selectedMaxValue <= 49) {
            slider.selectedMaxValue = 49;
        } else if (slider.selectedMaxValue > 98) {
            slider.selectedMaxValue = 100;
        }
        textfield5.text = ""
        getSearchLocationFiltered()
    }
    func processSliderRating(value: Int) -> Int {
        return (abs(value/20) + 1)
    }
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider == locationRangeSlider {
            let selectedMaxInt = Int(slider.selectedMaxValue)
            if slider.selectedMaxValue < 1 {
                radius = 5
                distanceLabel.text = "5 m"
            } else if (selectedMaxInt == 1) {
                radius = 5
                distanceLabel.text = "5 m"
            } else if (selectedMaxInt >= 2 && selectedMaxInt <= 20) {
                radius = selectedMaxInt * 5
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 21 && selectedMaxInt <= 27) {
                radius = 200;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 28 && selectedMaxInt <= 35) {
                radius = 400;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 36 && selectedMaxInt <= 43) {
                radius = 600;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt >= 44 && selectedMaxInt <= 49) {
                radius = 800;
                distanceLabel.text = "\(radius) m"
            } else if (selectedMaxInt == 50) {
                radius = 1000;
                distanceLabel.text = "1 km"
            } else if (selectedMaxInt > 98) {
                radius = (selectedMaxInt/2) * 1000;
                distanceLabel.text = "\(selectedMaxInt / 2) km"
            } else {
                let distance = getDistance(progress: selectedMaxInt)
                radius = distance * 100;
                distanceLabel.text = "\(distance) km"
            }
        }
    }
    func getDistance(progress: Int) -> Int {
        if (progress % 10 != 0) {
            distance = progress % 50;
        }
        return distance;
    }
    func getSearchLocationFiltered() {
        var locationfiler = [String:Any]()
        if let lastLocation = LocationManager.shared.lastLocation {
            locationfiler["latitude"] = lastLocation.coordinate.latitude
            locationfiler["longitude"] = lastLocation.coordinate.longitude
        }
        locationfiler["type"] = viewModel.filterType
        locationfiler["radius"] = radius
        FilterModel.shared.locationFilter = locationfiler
        getSearchFiltered()
    }
}

// MARK: - TagListView delegates
extension SimilarInterestsFilterVC: TagListViewDelegate {
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        switch sender {
        case categoryView:
            sender.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedCategoryDict, tagView: sender)
            categoryViewHeight.constant = sender.intrinsicContentSize.height
        case industryView:
            industryView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedIndustryDict, tagView: sender)
            industryViewHeight.constant = industryView.intrinsicContentSize.height
        case eduLevelView:
            eduLevelView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedEduLevelDict, tagView: sender)
            eduLevelViewHeight.constant = eduLevelView.intrinsicContentSize.height
        default:
            interestView.removeTag(title)
            checkForStringInTagViewData(tag: title, tagArray: &viewModel.selectedInterestsDict, tagView: sender)
            interestViewHeight.constant = interestView.intrinsicContentSize.height
        }
    }
    func checkForStringInTagViewData(tag: String, tagArray: inout [[String:Any]], tagView: TagListView) {
        for (j,i) in tagArray.enumerated() {
            if let id = i["id"] as? Int, let value = i["value"] as? String {
                if value == tag {
                    switch tagView {
                    case categoryView:
                        FilterModel.shared.serviceCategory = FilterModel.shared.serviceCategory.filter({$0 != id})
                    case industryView:
                        FilterModel.shared.industry = FilterModel.shared.industry.filter({$0 != id})
                    case eduLevelView:
                        FilterModel.shared.educationLevel = FilterModel.shared.educationLevel.filter({$0 != id})
                    default:
                        FilterModel.shared.skills = FilterModel.shared.skills.filter({$0 != id})
                    }
                    tagArray.remove(at: j)
                    self.getSearchFiltered()
                    break
                }
            }
        }
    }
}
// MARK: - SendData Delegates
extension SimilarInterestsFilterVC: SendData {
    func passData(dataPasser: Any, data: Any) {
        if data is Bool {
            viewModel.isSliderExpanded = !viewModel.isSliderExpanded
            updateSliderViewHeight()
        } else {
            getSearchFiltered()
        }
    }
}
