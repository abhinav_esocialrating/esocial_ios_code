//
//  SimilarInterestsViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 11/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class SimilarInterestsViewModel: NSObject {
    let placeholders = ["Category", "Skills", "Experience" ,"City", "Industry", "Education Level"]
    
    var filterType = "freelancers"
    var categoryType = ""
    
    var searchedData = [[String:Any]]()
    var dropDownData = [String]()
    
    var selectedEduLevel = [String]()
    var selectedInterests = [String]()
    var selectedIndustry = [String]()
    var selectedCategory = [String]()
    
    var selectedEduLevelDict = [[String:Any]]()
    var selectedInterestsDict = [[String:Any]]()
    var selectedIndustryDict = [[String:Any]]()
    var selectedCategoryDict = [[String:Any]]()
    
    var isSliderExpanded = false

    
    // MARK: - Search suggestions
    func getSearchConfig(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchConfig(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"].arrayValue
                self.parseSearchedData(data: data)
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    // MARK: - Utility methods
    func parseSearchedData(data: [JSON]) {
        let search = data.map { (json) -> [String:Any] in
            var dict = [String:Any]()
            dict["id"] = json["id"].intValue
            dict["value"] = json["value"].string
            return dict
        }
        searchedData = search
        let drop = search.map { (dict) -> String in
            return (dict["value"] as? String) ?? ""
        }
        dropDownData = drop
    }
    
    // MARK: - Search Filtered
    func getSearchFiltered(parameters: [String:Any], filter: String, count: Bool, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getSearchFiltered(parameters: parameters, filter: filter, count: count, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let data = json["data"]
                print(data)
//                let results = data.map { (res) -> SearchResults? in
//                    let model = SearchResults(dictionary: res.dictionaryValue)
//                    return model
//                }
//                self.searchResults = results
                completion(true, data, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
