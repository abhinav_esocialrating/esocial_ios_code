//
//  FriendsFilterOptionsVC.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 11/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class FriendsFilterOptionsVC: UIViewController {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var radioButton1: UIButton!
    @IBOutlet weak var radioButton2: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var cardView: UIView!
    
    var sendData: SendData!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }
    func configureView() {
        toggleRadioButtons(selected: 1)
        cancelButton.applyTemplate(image: UIImage(named: "Cancel-icon"), color: appGray)
        submitButton.layer.cornerRadius = 8
        cardView.layer.cornerRadius = 8
    }

    // MARK: - Actions
    @IBAction func submitTapped(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        sendData.passData(dataPasser: self, data: radioButton1.isSelected ? 6 : 7)
    }
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    @IBAction func radio1Tapped(_ sender: Any) {
        toggleRadioButtons(selected: 1)
    }
    
    @IBAction func radio2Tapped(_ sender: Any) {
        toggleRadioButtons(selected: 2)
    }
    func toggleRadioButtons(selected: Int) {
        radioButton1.isSelected = selected == 1 ? true : false
        radioButton2.isSelected = selected == 2 ? true : false
    }
}
