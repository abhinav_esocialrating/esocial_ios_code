//
//  FilterTagCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 07/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class FilterTagCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    var isCellSelected = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()
        setUpBorder(width: 1, color: isCellSelected ? UIColor.NewTheme.paleBlue :  UIColor.NewTheme.darkGray, radius: 4)
        label.textColor = isCellSelected ? UIColor.white : UIColor.NewTheme.darkGray
        backgroundColor = isCellSelected ? UIColor.NewTheme.paleBlue : UIColor.white
    }
}
