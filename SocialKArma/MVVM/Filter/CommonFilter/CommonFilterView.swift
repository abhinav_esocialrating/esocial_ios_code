//
//  CommonFilterView.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CommonFilterView: UIView {
    
    // MARK: - Outlets
    @IBOutlet weak var slider8: TTRangeSliderCircle!
    @IBOutlet weak var slider0: TTRangeSliderCircle!
    @IBOutlet weak var slider1: TTRangeSliderCircle!
    @IBOutlet weak var slider2: TTRangeSliderCircle!
    @IBOutlet weak var slider3: TTRangeSliderCircle!
    @IBOutlet weak var slider4: TTRangeSliderCircle!
    @IBOutlet weak var slider5: TTRangeSliderCircle!
    @IBOutlet weak var slider6: TTRangeSliderCircle!
    @IBOutlet weak var slider7: TTRangeSliderCircle!
    
    @IBOutlet weak var collapseButton: UIButton!
    // MARK: - Variables
    var passData: SendData!
    
    func setUpSlider() {
        let array = [slider0, slider1, slider2, slider3, slider4, slider5, slider6, slider7, slider8]
        for i in array {
            i?.minValue = 1
            i?.maxValue = 100
            i?.selectedMinimum = 1
            i?.selectedMaximum = 100
            i?.enableStep = true
            i?.step = 1
            i?.tintColor = darkBlue
            i?.minLabelColour = i == slider8 ? UIColor.NewTheme.background : UIColor.NewTheme.paleBlue
            i?.maxLabelColour = i == slider8 ? UIColor.NewTheme.background : UIColor.NewTheme.paleBlue
            i?.minDistance = 1
            i?.isRectangular = false
            i?.handleDiameter = i == slider8 ? 22 : 12
            i?.delegate = self
            i?.hideLabels = i == slider8 ? false : true
            i?.minLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 12)
            i?.maxLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 12)
        }
    }

    @IBAction func collapseTapped(_ sender: Any) {
        collapseButton.isSelected = !collapseButton.isSelected
        passData.passData(dataPasser: self, data: true)
    }
}
extension CommonFilterView: TTRangeSliderCircleDelegate {
    func didEndTouches(inRangeSlider sender: TTRangeSliderCircle!) {
        
        var roundedValue = roundf(sender.selectedMaximum)
        var intValue = Int(roundedValue)
        let max = (abs(intValue/20) + 1)
         
        roundedValue = roundf(sender.selectedMinimum)
        intValue = Int(roundedValue)
        let min = (abs(intValue/20) + 1)
        
        print("\(min)   \(max)")
        if sender == slider8 {
            FilterModel.shared.minScore = Int(min)
            FilterModel.shared.maxScore = Int(max)
            passData.passData(dataPasser: self, data: FilterModel.shared.paramDictionary)
        } else {
            let innerDict = ["parameter_id": sender.tag, "min": Int(min), "max": Int(max)]
            FilterModel.shared.reviewParams[sender.tag] = innerDict
            passData.passData(dataPasser: self, data: FilterModel.shared.reviewParams)
        }
    }
}
