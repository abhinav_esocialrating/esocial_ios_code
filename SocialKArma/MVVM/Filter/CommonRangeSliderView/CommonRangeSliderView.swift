//
//  CommonRangeSliderView.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 07/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class CommonRangeSliderView: UIView {

    // MARK: - Outlets
    @IBOutlet weak var slider8: RangeSeekSlider!
    @IBOutlet weak var slider0: RangeSeekSlider!
    @IBOutlet weak var slider1: RangeSeekSlider!
    @IBOutlet weak var slider2: RangeSeekSlider!
    @IBOutlet weak var slider3: RangeSeekSlider!
    @IBOutlet weak var slider4: RangeSeekSlider!
    @IBOutlet weak var slider5: RangeSeekSlider!
    @IBOutlet weak var slider6: RangeSeekSlider!
    @IBOutlet weak var slider7: RangeSeekSlider!
    
    @IBOutlet weak var collapseButton: UIButton!
    
    @IBOutlet weak var collapseImage: UIButton!
    // Min-max labels
    @IBOutlet weak var minLbl: UILabel!
    @IBOutlet weak var maxLbl: UILabel!
    @IBOutlet weak var minLbl0: UILabel!
    @IBOutlet weak var maxLbl0: UILabel!
    @IBOutlet weak var minLbl1: UILabel!
    @IBOutlet weak var maxLbl1: UILabel!
    @IBOutlet weak var minLbl2: UILabel!
    @IBOutlet weak var maxLbl2: UILabel!
    @IBOutlet weak var minLbl3: UILabel!
    @IBOutlet weak var maxLbl3: UILabel!
    @IBOutlet weak var minLbl4: UILabel!
    @IBOutlet weak var maxLbl4: UILabel!
    @IBOutlet weak var minLbl5: UILabel!
    @IBOutlet weak var maxLbl5: UILabel!
    @IBOutlet weak var minLbl6: UILabel!
    @IBOutlet weak var maxLbl6: UILabel!

    // MARK: - Variables
    var passData: SendData!
    
    func setUpSlider() {
        let array = [slider0, slider1, slider2, slider3, slider4, slider5, slider6, slider7, slider8]
        for i in array {
            i?.minValue = 1
            i?.maxValue = 100
            i?.selectedMinValue = 1
            i?.selectedMaxValue = 100
            i?.enableStep = true
            i?.step = 1
            i?.tintColor = darkBlue
            i?.minLabelColor = UIColor.NewTheme.paleBlue
            i?.maxLabelColor = UIColor.NewTheme.paleBlue
            i?.minDistance = 1
            i?.handleDiameter = i == slider8 ? 22 : 20
//            i?.hideLabels = i == slider8 ? false : true
            i?.minLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 12)
            i?.maxLabelFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 12)
            i?.delegate = self
        }
    }
    func resetValues() {
        let array = [slider0, slider1, slider2, slider3, slider4, slider5, slider6, slider7, slider8]
        for i in array {
            i?.selectedMinValue = 1
            i?.selectedMaxValue = 100
            i?.layoutSubviews()
        }
        minLbl.text = "1"
        maxLbl.text = "10"
        minLbl0.text = "1"
        maxLbl0.text = "10"
        minLbl1.text = "1"
        maxLbl1.text = "10"
        minLbl2.text = "1"
        maxLbl2.text = "10"
        minLbl3.text = "1"
        maxLbl3.text = "10"
        minLbl4.text = "1"
        maxLbl4.text = "10"
        minLbl5.text = "1"
        maxLbl5.text = "10"
        minLbl6.text = "1"
        maxLbl6.text = "10"
    }
    @IBAction func collapseTapped(_ sender: Any) {
        collapseButton.isSelected = !collapseButton.isSelected
        collapseImage.isSelected = !collapseImage.isSelected
        passData.passData(dataPasser: self, data: true)
    }
}
extension CommonRangeSliderView: RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
        let roundedValue = roundf(Float(maxValue))
        let intValue = Int(roundedValue)
        let max = (abs(intValue/10) + 1)
         
        let roundedValue1 = roundf(Float(minValue))
        let intValue1 = Int(roundedValue1)
        let min = (abs(intValue1/10) + 1)
        
        if slider == slider8 {
            minLbl.text = "\(min)"
            maxLbl.text = "\(max == 11 ? 10 : max)"
        } else if slider == slider0 {
            minLbl0.text = "\(min)"
            maxLbl0.text = "\(max == 11 ? 10 : max)"
        } else if slider == slider1 {
            minLbl1.text = "\(min)"
            maxLbl1.text = "\(max == 11 ? 10 : max)"
        } else if slider == slider2 {
            minLbl2.text = "\(min)"
            maxLbl2.text = "\(max == 11 ? 10 : max)"
        } else if slider == slider3 {
            minLbl3.text = "\(min)"
            maxLbl3.text = "\(max == 11 ? 10 : max)"
        } else if slider == slider4 {
            minLbl4.text = "\(min)"
            maxLbl4.text = "\(max == 11 ? 10 : max)"
        } else if slider == slider5 {
            minLbl5.text = "\(min)"
            maxLbl5.text = "\(max == 11 ? 10 : max)"
        } else if slider == slider6 {
            minLbl6.text = "\(min)"
            maxLbl6.text = "\(max == 11 ? 10 : max)"
        }

    }
    func didEndTouches(in slider: RangeSeekSlider) {
        var roundedValue = roundf(Float(slider.selectedMaxValue))
        var intValue = Int(roundedValue)
        let max = (abs(intValue/10) + 1)
         
        roundedValue = roundf(Float(slider.selectedMinValue))
        intValue = Int(roundedValue)
        let min = (abs(intValue/10) + 1)
        
        print("\(min)   \(max)")
        if slider == slider8 {
            FilterModel.shared.minScore = Int(min)
            FilterModel.shared.maxScore = Int(max == 11 ? 10 : max)
            passData.passData(dataPasser: self, data: FilterModel.shared.paramDictionary)
        } else {
            let innerDict = ["parameter_id": slider.tag, "min": Int(min), "max": Int(max)]
            FilterModel.shared.reviewParams[slider.tag] = innerDict
            passData.passData(dataPasser: self, data: FilterModel.shared.reviewParams)
        }
    }
}
