//
//  NetworkContainerViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class NetworkContainerViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!

    let pagerVC = NetworkPagerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configurePager()
    }
    
    func configurePager() {
    
        addChild(pagerVC)
        pagerVC.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        pagerVC.view.frame = containerView.bounds
        
        containerView.addSubview(pagerVC.view)
        pagerVC.didMove(toParent: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
