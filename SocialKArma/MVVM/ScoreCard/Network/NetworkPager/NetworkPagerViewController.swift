//
//  NetworkPagerViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import ViewPager_Swift

class NetworkPagerViewController: UIViewController {

    var vcs: [UIViewController] = []
    var viewPager: ViewPager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configurePager()
    }
    func configurePager() {
        
        let vc1 = NetworkViewController.instantiate(fromAppStoryboard: .ScoreCard)
        vc1.screenType = 1
        
        let vc2 = NetworkViewController.instantiate(fromAppStoryboard: .ScoreCard)
        vc2.screenType = 2
        
        let vc3 = NetworkViewController.instantiate(fromAppStoryboard: .ScoreCard)
        vc3.screenType = 3
        
        vcs = [vc1, vc2, vc3]
        
        let myOptions = ViewPagerOptions()
        myOptions.tabType = ViewPagerTabType.basic
        myOptions.distribution = ViewPagerOptions.Distribution.segmented
        myOptions.isTabHighlightAvailable = true
        myOptions.isTabIndicatorAvailable = true
        myOptions.tabViewBackgroundDefaultColor = UIColor.white
        myOptions.tabViewBackgroundHighlightColor = UIColor.white
        myOptions.tabIndicatorViewBackgroundColor = UIColor.SearchColors.blue
        myOptions.tabViewTextDefaultColor = .darkGray
        myOptions.tabViewTextHighlightColor = .darkGray
        myOptions.tabViewHeight = 40

        viewPager = ViewPager(viewController: self)
        viewPager.setOptions(options: myOptions)
        viewPager.setDataSource(dataSource: self)
        viewPager.setDelegate(delegate: self)
        viewPager.build()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - ViewPager methods
extension NetworkPagerViewController: ViewPagerDataSource, ViewPagerDelegate {
    func numberOfPages() -> Int {
        return vcs.count
            
    }
    
    func viewControllerAtPosition(position: Int) -> UIViewController {
        return vcs[position]
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        let tab1 = ViewPagerTab(title: "Feedback", image: nil)
        let tab2 = ViewPagerTab(title: "Cheers", image: nil)
        let tab3 = ViewPagerTab(title: "Invites", image: nil)

        let tabs = [tab1, tab2, tab3]
        return tabs
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
    
    func willMoveToControllerAtIndex(index: Int) {
        
    }
    
    func didMoveToControllerAtIndex(index: Int) {
        viewPager.displayViewController(atIndex: index)
    }
    
    
}
