//
//  NetworkViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class NetworkViewModel: NSObject {
     
    var networkList: [NetworkList]?
    
    func getReviewDoneList(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.getReviewDoneList(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getScoreCard")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData["data"] as! NSArray
                    self.networkList = NetworkList.modelsFromDictionaryArray(array: data)
                    completion(true, JSON.init(), "")
                } catch {
                    
                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func getReviewersList(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.getReviewersList(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getScoreCard")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData["data"] as! NSArray
                    self.networkList = NetworkList.modelsFromDictionaryArray(array: data)
                    completion(true, JSON.init(), "")
                } catch {
                    
                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func getUserInvitationList(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
            
        NetworkManager.getUserInvitationList(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getScoreCard")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData["data"] as! NSArray
                    self.networkList = NetworkList.modelsFromDictionaryArray(array: data)
                    completion(true, JSON.init(), "")
                } catch {
                    
                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }

}
