/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class NetworkList {
	public var relationship_strength : String?
	public var user_id : Int?
	public var name : String?
	public var relationship_id : String?
	public var relationship_length : String?
	public var image_id : String?
    public var status : Int?
    public var mobile_number : String?
    public var country_code : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let NetworkList_list = NetworkList.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of NetworkList Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [NetworkList]
    {
        var models:[NetworkList] = []
        for item in array
        {
            models.append(NetworkList(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let NetworkList = NetworkList(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: NetworkList Instance.
*/
	required public init?(dictionary: NSDictionary) {

		relationship_strength = dictionary["relationship_strength"] as? String
		user_id = dictionary["user_id"] as? Int
		name = dictionary["name"] as? String
		relationship_id = dictionary["relationship_id"] as? String
		relationship_length = dictionary["relationship_length"] as? String
		image_id = dictionary["image_id"] as? String
        status = dictionary["status"] as? Int
        mobile_number = dictionary["mobile_number"] as? String
        country_code = dictionary["country_code"] as? String

	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.relationship_strength, forKey: "relationship_strength")
		dictionary.setValue(self.user_id, forKey: "user_id")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.relationship_id, forKey: "relationship_id")
		dictionary.setValue(self.relationship_length, forKey: "relationship_length")
		dictionary.setValue(self.image_id, forKey: "image_id")
        dictionary.setValue(self.status, forKey: "status")
        dictionary.setValue(self.mobile_number, forKey: "mobile_number")
        dictionary.setValue(self.country_code, forKey: "country_code")

		return dictionary
	}

}
