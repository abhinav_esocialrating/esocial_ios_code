//
//  NetworkViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class NetworkViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var screenType = 1   // 1: ReviewDone 2: Reviewers  3: UserInvitation
    let viewModel = NetworkViewModel()
    var titleText = ""

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureTableView()
        getRemoteData()
        titleLabel.text = titleText
    }
    func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerFromXib(name: TableViewCells.NetworkCell.rawValue)
    }
    func getRemoteData() {
        switch screenType {
        case 1:
            viewModel.getReviewDoneList(parameters: [:]) { (success, json, message) in
                self.updateUI()
            }
        case 2:
            viewModel.getReviewersList(parameters: [:]) { (success, json, message) in
                self.updateUI()
            }
        default:
            viewModel.getUserInvitationList(parameters: [:]) { (success, json, message) in
                self.updateUI()
            }
        }
    }
    
    // MARK: - Actions

    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    // MARK: - Methods
    func updateUI() {
        tableView.reloadData()
    }
    func openChat(index: Int) {
        let network = viewModel.networkList?[index]
        let vc = ChatViewController.instantiate(fromAppStoryboard: .Chat)
        SocketIOManager.shared.toUserId = "\(network?.user_id ?? 0)"
        let dict = ["name": network?.name, "image_id": network?.image_id]
        let chatUser = ChatUser(dictionary: dict as NSDictionary)
        vc.viewModel.chatUser = chatUser
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
//        navigationController?.pushViewController(vc, animated: true)
    }
    func openWhatsapp(index: Int) {
        let network = viewModel.networkList?[index]
        if let name = network?.name, let country_code = network?.country_code, var number = network?.mobile_number {
            number = "\(country_code)\(number)"
            Utilities.inviteUsingWhatsapp(number: number, type: 2, name: name)
        } else {
            openShare()
        }
    }
}
extension NetworkViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.networkList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.NetworkCell.rawValue, for: indexPath) as! NetworkCell
        cell.indexPath = indexPath
        cell.network = viewModel.networkList?[indexPath.row]
        cell.setValues(screenType: screenType)
        cell.messageTap = { index in
            self.openChat(index: index)
        }
        cell.inviteTap = { index in
            self.openWhatsapp(index: index)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
}

