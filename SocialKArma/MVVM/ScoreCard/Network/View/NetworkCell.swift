//
//  NetworkCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class NetworkCell: UITableViewCell, ReusableView {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageBtn: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var inviteBtn: UIButton!
    
    var network: NetworkList?
    var messageTap: ((_ index: Int) -> Void)? = nil
    var inviteTap: ((_ index: Int) -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgView.rounded()
        imgView.contentMode = .scaleAspectFill
        messageBtn.applyTemplate(image: nil, color: UIColor.SearchColors.blue)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(screenType: Int) {
        if let name = network?.name {
            nameLabel.text = name
        } else {
            nameLabel.text = ""
        }
        if let urlStr = network?.image_id, let url = URL(string: urlStr) {
            imgView.sd_setImage(with: url, completed: nil)
        } else {
            imgView.image = UIImage(named: "avatar-2")
        }
        if screenType == 3 || screenType == 1 {
            messageBtn.isHidden = network?.status != 3
            inviteBtn.isHidden = network?.status == 3
            inviteBtn.setTitle(screenType == 3 ? "Resend Invite" : "Invite", for: .normal)
        } else {
            messageBtn.isHidden = false
            inviteBtn.isHidden = true
        }
    }
    // MARK: - Actions
    @IBAction func messageTapped(_ sender: Any) {
        messageTap?(indexPath.row)
    }
    
    @IBAction func inviteTapped(_ sender: Any) {
        inviteTap?(indexPath.row)
    }
    
}
