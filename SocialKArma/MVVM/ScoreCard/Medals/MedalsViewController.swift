//
//  MedalsViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 11/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import CoreData

class MedalsViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var medals: [[String: Any]]?
//    lazy var fetchedResultsController: NSFetchedResultsController<Medals> = {
//        let fetchRequest = NSFetchRequest<Medals>(entityName:"Medals")
//
//        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
//                                                    managedObjectContext: appDelegate!.persistentContainer.viewContext,
//                                                    sectionNameKeyPath: nil, cacheName: nil)
//        controller.delegate = self
//
//        do {
//            try controller.performFetch()
//        } catch {
//            let nserror = error as NSError
//            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//        }
//
//
//
//        return controller
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureCollectionView()
        
        fetchData()
    }
    func configureCollectionView() {
        collectionView.register(UINib(nibName: "MedalHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "MedalHeaderView")
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    
    // MARK: - Action

    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Methods
    func fetchData() {
        let persistentContainer = appDelegate?.persistentContainer
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MedalsModel")
           
        do {
            let results = try persistentContainer?.viewContext.fetch(fetchRequest)
            let allMedals = results as? [MedalsModel]
            guard let medals = allMedals else {
                return
            }
            
            let chunked = medals.chunked(into: 3)
            var sectionedMedals = [[String: Any]]()
            for i in 0..<5 {
                let dict = ["title": "Level \(i+1)", "value": chunked[i]] as [String : Any]
                sectionedMedals.append(dict)
            }
            self.medals = sectionedMedals
            collectionView.reloadData()
        } catch {
            print("error")
        }
    }
}
extension MedalsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return medals?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let array = medals?[section]["value"] as? [MedalsModel]
        return array?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCells.MedalsCell.rawValue, for: indexPath) as! MedalsCell
        let array = medals?[indexPath.section]["value"] as? [MedalsModel]
        if (array?[indexPath.row].user_medal_id) != nil {
            if let urlStr = array?[indexPath.row].image_url, let url = URL(string: urlStr) {
                cell.imgView.sd_setImage(with: url, completed: nil)
            } else {
                cell.imgView.image = UIImage(named: "avatar-2")
            }
        } else {
            cell.imgView.image = UIImage(named: "medal_lock")
        }
        cell.label.text = array?[indexPath.row].medal_info
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (screenWidth-20)/3
        return CGSize(width: width, height: 130)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionFooter) {
            return UIView() as! UICollectionReusableView
        } else if (kind == UICollectionView.elementKindSectionHeader) {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "MedalHeaderView", for: indexPath) as! MedalHeaderView
            let title = medals?[indexPath.section]["title"] as? String
            headerView.label.text = title
            return headerView
        }
        fatalError()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: screenWidth, height: 44)
    }
}
// MARK: - FetchedResultsController
extension MedalsViewController: NSFetchedResultsControllerDelegate {
}
