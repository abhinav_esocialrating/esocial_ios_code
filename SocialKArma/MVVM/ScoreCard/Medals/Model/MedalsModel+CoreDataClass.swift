//
//  MedalsModel+CoreDataClass.swift
//  
//
//  Created by Abhinav Dobhal on 24/10/20.
//
//

import Foundation
import CoreData


public class MedalsModel: NSManagedObject {

}
extension MedalsModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MedalsModel> {
        return NSFetchRequest<MedalsModel>(entityName: "MedalsModel")
    }

    @NSManaged public var medal_type_id: Int64
    @NSManaged public var medal_name: String?
    @NSManaged public var medal_type: String?
    @NSManaged public var medal_id: Int64
    @NSManaged public var image_url: String?
    @NSManaged public var user_medal_id: Int64
    @NSManaged public var medal_info: String?
    
    func update(with dictionary: [String: Any]) {

        medal_type_id = dictionary["medal_type_id"] as? Int64 ?? 0
        medal_name = dictionary["medal_name"] as? String
        medal_type = dictionary["medal_type"] as? String
        medal_id = dictionary["medal_id"] as? Int64 ?? 0
        image_url = dictionary["image_url"] as? String
        user_medal_id = dictionary["user_medal_id"] as? Int64 ?? 0
        medal_info = dictionary["medal_info"] as? String
    }
}
