//
//  ScoreCardViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 08/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

class ScoreCardViewModel: NSObject {
    // MARK: - Variables
    var scoreCard: ScoreCard?
    // MARK: - Web Services
    func getScoreCard(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getScoreCard(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print("getScoreCard")
//            print(json)
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData["data"] as! NSDictionary
                    self.syncScoreCard(dictionary: data as! [String : Any])
                    let scoreCard = ScoreCard(dictionary: data)
                    self.scoreCard = scoreCard
                    completion(true, JSON.init(), "")
                    var dictToSave = data as! [String : Any]
                    dictToSave["medals"] = nil
                    UserDefaults.standard.ScoreCard = dictToSave
                } catch {
                    
                }
                completion(true, json, "")
            default:
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    
    func syncScoreCard(dictionary: [String:Any]) {
        guard let taskContext = appDelegate?.persistentContainer.newBackgroundContext() else {
            return
        }
        taskContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        taskContext.undoManager = nil
        
        taskContext.performAndWait {
            
            if let medals = dictionary["medals"] as? [[String:Any]] {
                let matchingEpisodeRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MedalsModel")
                let episodeIds = medals.map { $0["medal_type_id"] as? Int }.compactMap { $0 }
                matchingEpisodeRequest.predicate = NSPredicate(format: "medal_type_id in %@", argumentArray: [episodeIds])
                
                let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: matchingEpisodeRequest)
                batchDeleteRequest.resultType = .resultTypeObjectIDs
                
                do {
                    let batchDeleteResult = try taskContext.execute(batchDeleteRequest) as? NSBatchDeleteResult
                    
                    if let deletedObjectIDs = batchDeleteResult?.result as? [NSManagedObjectID] {
                        NSManagedObjectContext.mergeChanges(fromRemoteContextSave: [NSDeletedObjectsKey: deletedObjectIDs],
                                                            into: [appDelegate!.persistentContainer.viewContext])
                    }
                } catch {
                    print("Error: \(error)\nCould not batch delete existing records.")
                    return
                }
                
                for dict in medals {
                               
                   guard let medals = NSEntityDescription.insertNewObject(forEntityName: "MedalsModel", into: taskContext) as? MedalsModel else {
                       print("Error: Failed to create a new Film object!")
                       return
                   }
                   
                    medals.update(with: dict)

               }
            }
            
            if taskContext.hasChanges {
                do {
                    try taskContext.save()
                } catch {
                    print("Error: \(error)\nCould not save Core Data context.")
                }
                taskContext.reset() // Reset the context to clean up the cache and low the memory footprint.
            }
            
        }
    }
}
