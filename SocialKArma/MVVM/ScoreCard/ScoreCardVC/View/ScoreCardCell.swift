//
//  ScoreCardCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 08/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ScoreCardCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var valueLbl: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layer.cornerRadius = 12
        contentView.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 4, scale: true)
        layer.cornerRadius = 12
        containerView.layer.cornerRadius = 12
    }
}
