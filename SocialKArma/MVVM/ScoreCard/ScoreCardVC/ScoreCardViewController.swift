//
//  ScoreCardViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 08/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON
import FAPanels
import FirebaseAnalytics
import CoreData

class ScoreCardViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet var view1: [UIView]!
    @IBOutlet var valueLabel: [UILabel]!
    @IBOutlet var imgViews: [UIImageView]!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var notificationBadge: UILabel!
    @IBOutlet weak var chatNotificationBadge: UILabel!
    @IBOutlet weak var searchStatusBtn: UIButton!

    // MARK: - Variables
    
    let viewModel = ScoreCardViewModel()
    // MARK: - Life Cycle methods
    /**
    Life Cycle method called when view is loaded.
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting up views and targets and initial info when the view is loaded
        configureView()
        configureCollectionView()
        addTapGesture()
        setUpNavBar()
        setUserInfo()
        
        // Adding observer for Actively looking status change
        NotificationCenter.default.addObserver(self, selector: #selector(searchStatus(notification:)), name: Notification.Name.SearchStatus, object: nil)

    }
    /**
     Life Cycle method called when view is loaded and about to appear on the screen.
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let score = ScoreCard(dictionary: UserDefaults.standard.ScoreCard as NSDictionary)
        viewModel.scoreCard = score
        self.updateUI()
        
        viewModel.getScoreCard(parameters: [:]) { (success, _, message) in
            self.updateUI()
        }
        setNotificationBadges()
        
        searchStatusBtn.tintColor = (UserDefaults.standard.SearchStatus["friendStatus"] as! Int == 1 || UserDefaults.standard.SearchStatus["professionalStatus"] as! Int == 1) ? .green : .red

    }
    /**
    Life cycle method when view has appeared fully on screen
    */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !UserDefaults.standard.isScoreCoachMarkShown {
            UserDefaults.standard.isScoreCoachMarkShown = true
            let vc = InfoCardsViewController.instantiate(fromAppStoryboard: .Main)
            vc.modalPresentationStyle = .fullScreen
            vc.viewModel.screenType = 1
            present(vc, animated: false, completion: nil)
        }
        
        // Screen tracking
        Analytics.logEvent("screen_tracking", parameters: [
            "name": "TabClass" as NSObject,
            "full_text": "ScoreCardViewController" as NSObject
        ])
    }
    // MARK: - Set up essentials
    /**
     Setting up ui of the app and adding targets to different controls that needs to be done after view has loaded
     */
    func configureView() {
        view1.forEach { (i) in
            i.layer.cornerRadius = 12
            i.dropShadow(color: UIColor.SearchColors.shadow, opacity: 1.0, offSet: CGSize(width: 2, height: 2), radius: 12, scale: true)
        }
        userImg.rounded()
        notificationBadge.layer.cornerRadius = notificationBadge.frame.width/2
        notificationBadge.clipsToBounds = true
        chatNotificationBadge.layer.cornerRadius = chatNotificationBadge.frame.width/2
        chatNotificationBadge.clipsToBounds = true
    }
    /**
     Setting up the collection/grid view that is showing at the bottom half of the screen.
     */
    func configureCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    /**
    Adding tap gestures to the horizontally sliding views
    */
    func addTapGesture() {
        view1.forEach { (i) in
            let tap = UITapGestureRecognizer(target: self, action: #selector(ScoreCardViewController.viewTapped(_:)))
            i.addGestureRecognizer(tap)
        }
    }
    /**
    Fetching and updating user's basic details from UserDefaults
    */
    func setUserInfo() {
        let userInfo = UserDefaults.standard.userInfo
        let data = userInfo.data(using: .utf8)
        let json = JSON(data ?? Data())
        let dict = json.dictionaryObject
        if let urlStr = dict?["image_id"] as? String, let url = URL(string: urlStr) {
            userImg.sd_setImage(with: url, completed: nil)
        }
    }
    
    // MARK: - Actions
    /**
     Selector for performing actions when any one of the horizontally sliding views are tapped
     */
    @objc func viewTapped(_ sender: UITapGestureRecognizer) {
        guard let tappedView = sender.view else {
            return
        }
        switch tappedView {
        case view1[0]:
            let vc = ScoreViewController.instantiate(fromAppStoryboard: .Profile)
            let viewModel = ScoreViewModel()
            vc.viewModel = viewModel
            vc.viewModel.userType = 1
            present(vc, animated: true, completion: nil)
        case view1[1]:
            let vc = MedalsViewController.instantiate(fromAppStoryboard: .ScoreCard)
//            vc.medals = parseMedals()
            present(vc, animated: true, completion: nil)
        default:
            let vc = PointsViewController.instantiate(fromAppStoryboard: .Main)
            present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func messageTapped(_ sender: Any) {
        openChatFromNavBar()
    }
    
    @IBAction func infoTapped(_ sender: Any) {
//        openInfoFromNavBar(info: Constants.ScoreInfo.rawValue)
        openVideoPlayer()
    }
    @IBAction func sideMenuTapped(_ sender: Any) {
        if let panel = appDelegate?.window?.rootViewController as? FAPanelController {
            panel.openLeft(animated: true)
        }
    }
    @IBAction func notificationTapped(_ sender: Any) {
        openNotificationFromNavBar()
    }
    @IBAction func pointsBtnTapped(_ sender: Any) {
        openPointsScreenFromNavBar()
    }
    @IBAction func activelyLookingTapped(_ sender: Any) {
        openActivelyLooking()
    }
    /**
    Selector for observing changes in the user's active status for searching friends or professional jobs/works
    */
    @objc func searchStatus(notification: Notification) {
        searchStatusBtn.tintColor = (UserDefaults.standard.SearchStatus["friendStatus"] as! Int == 1 || UserDefaults.standard.SearchStatus["professionalStatus"] as! Int == 1) ? .green : .red
    }

    // MARK: - Methods
    /**
    Updating whole UI after API call or Some other action
    */
    func updateUI() {
        if let value = viewModel.scoreCard?.score {
            valueLabel[0].text = String(format: "%.1f", value)
        }
        if let value = viewModel.scoreCard?.medals_count {
            valueLabel[1].text = "\(value)"
        }
        if let value = viewModel.scoreCard?.tcoins {
            valueLabel[2].text = "\(value)"
        }
        collectionView.reloadData()
    }
    /**
     Set values from API to the collection/grid view that is showing in the bottom half of the screen
     
     - Parameter cell: Instance of the cell from the collectionView
     - Parameter index: Index of the current cell of collectionview
     */
    func setStaticValues(cell: ScoreCardCell, index: Int) {
        switch index {
        case 0:
            cell.titleLbl.text = "Feedback Given"
            cell.valueLbl.text = "\(viewModel.scoreCard?.reviews_count ?? 0)"
            cell.imgView.image = UIImage(named: "Feedback given")
        case 1:
            cell.titleLbl.text = "Feedback Received"
            cell.valueLbl.text = "\(viewModel.scoreCard?.reviewers_count ?? 0)"
            cell.imgView.image = UIImage(named: "Feedback Received")
        case 2:
            cell.titleLbl.text = "Invitations Sent"
            cell.valueLbl.text = "\(viewModel.scoreCard?.invitation_sent_count ?? 0)"
            cell.imgView.image = UIImage(named: "Invites sent")
        case 3:
            cell.titleLbl.text = "Profile Views"
            cell.valueLbl.text = "\(viewModel.scoreCard?.profile_views_count ?? 0)"
            cell.imgView.image = UIImage(named: "vision")
        case 4:
            cell.titleLbl.text = "Appeared in Searches"
            cell.valueLbl.text = "\(viewModel.scoreCard?.search_count ?? 0)"
            cell.imgView.image = UIImage(named: "Icon feather-search")
        case 6:
            cell.titleLbl.text = "Detailed Feedback"
            cell.valueLbl.text = "\(viewModel.scoreCard?.feedback_text_count ?? 0)"
            cell.imgView.image = UIImage(named: "feedback_text")
        default:
            cell.titleLbl.text = "Network"
            cell.valueLbl.text = "\(viewModel.scoreCard?.relationship_count ?? 0)"
            cell.imgView.image = UIImage(named: "noun_relation")
        }
    }
    
}
// MARK: - CollectionView methods
extension ScoreCardViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel.scoreCard != nil {
            return 7
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScoreCardCell", for: indexPath) as! ScoreCardCell
        setStaticValues(cell: cell, index: indexPath.row)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (screenWidth-34)/2
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0,1,2:
            let arr = ["Feedback Given", "Feedback Received", "Invitations Sent"]
            openNetworkVC(screenType: indexPath.row+1, title: arr[indexPath.row])
        case 6:
            let vc = TextualFeedbackViewController.instantiate(fromAppStoryboard: .ScoreCard)
            present(vc, animated: true, completion: nil)
        default:
            print(indexPath)
        }
    }
}
// MARK: - NavBar
extension ScoreCardViewController {
    // Notification Handling- To be moved to a separate class soon
    func setUpNavBar() {
        NotificationCenter.default.addObserver(self, selector: #selector(pushNotificationReceived(_:)), name: Notification.Name.PushReceived, object: nil)
    }
    @objc func pushNotificationReceived(_ notification: Notification) {
        if let type = notification.userInfo?["type"] as? String {
            if type == PushNotificationType.FEEDBACK_UE.rawValue || type == PushNotificationType.FEEDBACK_UE_2.rawValue || type == PushNotificationType.CONTACT_UE.rawValue || type == PushNotificationType.INVITE_UE.rawValue || type == PushNotificationType.PROFILE_UE.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_1.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_2.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_3.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_4.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_5.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_6.rawValue || type == PushNotificationType.UE_FEEDBACK_REMINDER_7.rawValue || type == PushNotificationType.UE_INFO_1.rawValue || type == PushNotificationType.UE_PERSONAL_NEWS_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_2.rawValue || type == PushNotificationType.UE_PROFILE_COMPLETION_1.rawValue || type == PushNotificationType.UE_REINVITE_1.rawValue || type == PushNotificationType.UE_REINVITE_2.rawValue || type == PushNotificationType.CHAT_UNREAD_MESSAGES.rawValue {  }
            else if type == PushNotificationType.CHAT.rawValue {
//                PointsHandler.shared.isChatNotificationBadge = true
//                chatNotificationBadge.isHidden = false
                setNotificationBadges()
            }
            else {
//                PointsHandler.shared.isNotification = true
//                notificationBadge.isHidden = false
                setNotificationBadges()
            }
        }
    }
    /**
    Setting badges on Notificaiton and Chat button to show number of unread notifications and numbers.
    */
    func setNotificationBadges() {
        PointsHandler.shared.getNotificationCount { notificationCountData in
            
            if let notifications = notificationCountData["notification"] as? Int, notifications > 0 {
                PointsHandler.shared.isNotification = true
                self.notificationBadge.isHidden = false
                self.notificationBadge.text = "\(notifications)"
            } else {
                self.notificationBadge.isHidden = PointsHandler.shared.isNotification ? false : true
            }
            if let chat = notificationCountData["chat"] as? Int, chat > 0 {
                PointsHandler.shared.isChatNotificationBadge = true
                self.chatNotificationBadge.isHidden = false
                self.chatNotificationBadge.text = "\(chat)"
            } else {
                self.chatNotificationBadge.isHidden = PointsHandler.shared.isChatNotificationBadge ? false : true
            }
        }
    }
}
