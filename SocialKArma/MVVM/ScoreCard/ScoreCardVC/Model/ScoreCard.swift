/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
import CoreData
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ScoreCard {
	public var network_count : Int?
    public var tcoins : Int?
    public var score : Double?
    public var invitation_sent_count : Int?
    public var medals_count : Int?
    public var reviewers_count : Int?
//    @NSManaged public var medals : Array<Medals>
    public var all_message_count : Int?
    public var reviews_count : Int?
    public var profile_views_count : Int?
    public var relationship_count : Int?
    public var feedback_text_count : Int?
    public var search_count : Int?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let ScoreCard_list = ScoreCard.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of ScoreCard Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ScoreCard]
    {
        var models:[ScoreCard] = []
        for item in array
        {
//            models.append(ScoreCard(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let ScoreCard = ScoreCard(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: ScoreCard Instance.
*/
    
    func update(with dictionary: [String: Any]) {
        network_count = dictionary["network_count"] as? Int ?? 0
        tcoins = dictionary["tcoins"] as? Int ?? 0
        score = dictionary["score"] as? Double ?? 0.0
        invitation_sent_count = dictionary["invitation_sent_count"] as? Int ?? 0
        medals_count = dictionary["medals_count"] as? Int ?? 0
        reviewers_count = dictionary["reviewers_count"] as? Int ?? 0
//        if (dictionary["medals"] != nil) { medals = Medals.modelsFromDictionaryArray(array: dictionary["medals"] as! NSArray) }
        all_message_count = dictionary["all_message_count"] as? Int ?? 0
        reviews_count = dictionary["reviews_count"] as? Int ?? 0
        profile_views_count = dictionary["profile_views_count"] as? Int ?? 0
        relationship_count = dictionary["relationship_count"] as? Int ?? 0
        feedback_text_count = dictionary["feedback_text_count"] as? Int ?? 0
        search_count = dictionary["search_count"] as? Int ?? 0
    }
    

	required public init?(dictionary: NSDictionary) {

        network_count = dictionary["network_count"] as? Int ?? 0
        tcoins = dictionary["tcoins"] as? Int ?? 0
        score = dictionary["score"] as? Double ?? 0.0
        invitation_sent_count = dictionary["invitation_sent_count"] as? Int ?? 0
        medals_count = dictionary["medals_count"] as? Int ?? 0
        reviewers_count = dictionary["reviewers_count"] as? Int ?? 0
//        if (dictionary["medals"] != nil) { medals = Medals.modelsFromDictionaryArray(array: dictionary["medals"] as! NSArray) }
        all_message_count = dictionary["all_message_count"] as? Int ?? 0
        reviews_count = dictionary["reviews_count"] as? Int ?? 0
        profile_views_count = dictionary["profile_views_count"] as? Int ?? 0
        relationship_count = dictionary["relationship_count"] as? Int ?? 0
        feedback_text_count = dictionary["feedback_text_count"] as? Int ?? 0
        search_count = dictionary["search_count"] as? Int ?? 0
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.network_count, forKey: "network_count")
        dictionary.setValue(self.tcoins, forKey: "tcoins")
        dictionary.setValue(self.score, forKey: "score")
        dictionary.setValue(self.invitation_sent_count, forKey: "invitation_sent_count")
        dictionary.setValue(self.medals_count, forKey: "medals_count")
        dictionary.setValue(self.reviewers_count, forKey: "reviewers_count")
        dictionary.setValue(self.all_message_count, forKey: "all_message_count")
        dictionary.setValue(self.reviews_count, forKey: "reviews_count")
        dictionary.setValue(self.profile_views_count, forKey: "profile_views_count")
        dictionary.setValue(self.relationship_count, forKey: "relationship_count")
        dictionary.setValue(self.search_count, forKey: "search_count")

		return dictionary
	}

}
