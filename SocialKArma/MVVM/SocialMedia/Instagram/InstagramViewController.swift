//
//  InstagramViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 31/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import WebKit

class InstagramViewController: UIViewController {
    
    
    @IBOutlet weak var webView: WKWebView!
    let viewModel = InstagramViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        webView.navigationDelegate = self
        loadWebView()
    }
    
    func loadWebView() {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [Instagram.INSTAGRAM_AUTHURL,Instagram.INSTAGRAM_CLIENT_ID,Instagram.INSTAGRAM_REDIRECT_URI, Instagram.INSTAGRAM_SCOPE])
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
        webView.load(urlRequest)
    }
    
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - WebView Delegate
extension InstagramViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if let url = webView.url?.absoluteString, url.contains("access") {
            if let accessToken = webView.url?.absoluteString.components(separatedBy: "=").last {
                Instagram.INSTAGRAM_ACCESS_TOKEN = accessToken
                viewModel.getInstagramProfile(parameters: [:]) { (status, json, str) in
                    DispatchQueue.main.async {
                        self.dismiss(animated: false, completion: {
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                }
            } else {
                print("Failure")
            }
        }
    }
}
/* ["access_token":Instagram.INSTAGRAM_ACCESS_TOKEN]

 */
