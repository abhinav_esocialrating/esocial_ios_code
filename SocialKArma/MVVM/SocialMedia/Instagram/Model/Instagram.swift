//
//  Instagram.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 31/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation

struct Instagram {
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static let INSTAGRAM_CLIENT_ID = "0fac93b2c9b245699724a6b5eca81779"
    static let INSTAGRAM_CLIENTSERCRET = "3a358ed35fc1490891da66554c8c339c"
    static let INSTAGRAM_REDIRECT_URI = "https://www.instagram.com/developer/authentication/"
    static var INSTAGRAM_ACCESS_TOKEN = ""
    static let INSTAGRAM_SCOPE = "basic"
    static let profileURL = "https://api.instagram.com/v1/users/self/?access_token="
}
