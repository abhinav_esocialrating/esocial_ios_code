//
//  InstagramViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 31/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class InstagramViewModel: NSObject {
    func getInstagramProfile(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getInstagramProfile(parameters: parameters, method: .get) { (json) in
            let meta = json["meta"].dictionaryValue
            if meta["code"]?.intValue == 200 {
                let data = json["data"].dictionaryValue
                let counts = data["counts"]?.dictionaryValue
                let followed_by = counts?["followed_by"]?.intValue ?? 0
                completion(true, json, "\(followed_by)")
            } else {
                let message = json["error_message"].stringValue
                let messageString = message
                completion(false, JSON.init(), messageString)
            }
        }
    }
}
/*
 {
 "meta" : {
 "code" : 200
 },
 "data" : {
 "full_name" : "Abhinav Dobhal",
 "counts" : {
 "followed_by" : 123,
 "follows" : 116,
 "media" : 8
 },
 "username" : "abhinavdobhal",
 "is_business" : false,
 "id" : "1596696546",
 "website" : "http:\/\/abhinavdobhal.xyz\/",
 "bio" : "Time is precious, waste it wisely. 😎",
 "profile_picture" : "https:\/\/scontent.cdninstagram.com\/vp\/6855b5e7e89acbf075a9fd5bc2d8525c\/5E0F33E7\/t51.2885-19\/s150x150\/12093304_153634288324210_1024573501_a.jpg?_nc_ht=scontent.cdninstagram.com"
 }
 }
 
 
 {
 "meta" : {
 "code" : 400,
 "error_message" : "Missing client_id or access_token URL parameter.",
 "error_type" : "OAuthParameterException"
 }
 }

 */
