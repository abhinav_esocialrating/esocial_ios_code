//
//  LinkedIn.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 31/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation

struct LinkedIn {
    static let LinkedIn_AUTHURL = "https://www.linkedin.com/oauth/v2/authorization"
    static let LinkedIn_CLIENT_ID = "81ct46ljkg82br"
    static let LinkedIn_CLIENTSERCRET = "9ZtVdPxwQemiteb8"
    static let LinkedIn_REDIRECT_URI = "https://www.linkedin.com/feed/"
    static var LinkedIn_ACCESS_TOKEN = ""
    static var LinkedIn_Code = ""
    static let LinkedIn_SCOPE = "basic"
    static let LinkedIn_ACCESS_TOKEN_URL = "https://www.linkedin.com/oauth/v2/accessToken"
    static let LinkedIn_PROFILE_URL = "https://api.linkedin.com/v2/me"
}
