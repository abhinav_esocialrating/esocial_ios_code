//
//  LinkedInViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 31/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class LinkedInViewModel: NSObject {
    func getLinkedInToken(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getLinkedInToken(parameters: parameters, method: .post) { (json) in
            if json["access_token"].string != "" {
                completion(true, json,  json["access_token"].stringValue)
            } else {
                let message = json["error_message"].stringValue
                let messageString = message
                completion(false, JSON.init(), messageString)
            }
            print(json)
        }
    }
    
    func getLinkedInProfile(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.getLinkedInProfile(parameters: parameters, method: .get) { (json) in
            print(json)
        }
    }
}
/*
 {
 "error_description" : "A required parameter \"client_id\" is missing",
 "error" : "invalid_request"
 }
 */
