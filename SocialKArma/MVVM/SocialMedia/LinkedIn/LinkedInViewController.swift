//
//  LinkedInViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 31/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import WebKit

class LinkedInViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    var viewModel = LinkedInViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        webView.navigationDelegate = self
        loadWebView()

    }
    func loadWebView() {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=code", arguments: [LinkedIn.LinkedIn_AUTHURL,LinkedIn.LinkedIn_CLIENT_ID,LinkedIn.LinkedIn_REDIRECT_URI])
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
        webView.load(urlRequest)
    }
    
    // MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
// MARK: - WebView Delegate
extension LinkedInViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(webView.url as Any)
        if let url = webView.url?.absoluteString, url.contains("code=") {
            if let code = webView.url?.absoluteString.components(separatedBy: "=").last {
                LinkedIn.LinkedIn_Code = code
                let parameters = ["grant_type": "authorization_code", "code": code, "redirect_uri": LinkedIn.LinkedIn_REDIRECT_URI, "client_id": LinkedIn.LinkedIn_CLIENT_ID, "client_secret": LinkedIn.LinkedIn_CLIENTSERCRET]
                viewModel.getLinkedInToken(parameters: parameters) { (result, json, message) in
                    if result {
                        LinkedIn.LinkedIn_ACCESS_TOKEN = message
                        self.viewModel.getLinkedInProfile(parameters: parameters, completion: { (result, json, message) in
                            
                        })
                    }
                }
            } else {
                print("Failure")
            }
        }
    }
}
/*
 DispatchQueue.main.async {
 self.dismiss(animated: false, completion: {
 self.dismiss(animated: true, completion: nil)
 })
 }
*/
