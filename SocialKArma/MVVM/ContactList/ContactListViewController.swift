//
//  ContactListViewController.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 05/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import DropDown

class ContactListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var backImage: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Variables
    let viewModel = ContactListViewModel()
    var sendData: SendData!
    let dropDown = DropDown()

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configreView()
        getContacts()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if tableView.numberOfRows(inSection: 0) == 0 {
            tableView.reloadData()
        }
    }
    func configreView() {
        searchView.setUpBorder(width: 0.4, color: UIColor.darkGray, radius: 8)
        searchTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        searchTF.clearButtonMode = .always
//        searchTF.becomeFirstResponder()
    }
    
    // MARK: - Actions
 
    @IBAction func backTapped(_ sender: Any) {
        if viewModel.isSelectionOn {
            hideSelectionView()
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text!.count > 0 {
            contactSearch(query: textField.text!)
        } else {
            viewModel.contactsSearchedFromRemote = []
            self.tableView.reloadData()
        }
    }
    
    @IBAction func searchIconTapped(_ sender: Any) {
        searchTF.becomeFirstResponder()
    }
    @IBAction func menuTapped(_ sender: Any) {
        view.endEditing(true)
        if viewModel.isSelectionOn {
            inviteContacts()
        } else {
            showDropDown()
        }
    }
    // MARK: - DropDown
    func showDropDown() {
        dropDown.anchorView = menuBtn
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 200
        dropDown.dataSource = viewModel.dropDownData
        dropDown.textFont = UIFont.fontWith(name: .Roboto, face: .Regular, size: 17)
        dropDown.textColor = UIColor.NewTheme.darkGray
        dropDown.selectionAction = { (index: Int, item: String) in
            self.handleDropSelection(index: index, item: item)
        }
        dropDown.show()
    }
    func handleDropSelection(index: Int, item: String) {
        switch index {
        case 0:
            resyncContact(type: 2)
        default:
            print("Unknown")
        }
    }
    // MARK: - Methods
    private func toggleContactSelectionView() {
        viewModel.isSelectionOn = !viewModel.isSelectionOn
        titleLabel.text = viewModel.isSelectionOn ? "\(viewModel.selectedContactIds.count) selected" : "Search Contacts"
        let image = viewModel.isSelectionOn ? "cancel" : "Back-Arrow-icon-White"
        backImage.setImage(UIImage(named: image), for: .normal)
        
        if viewModel.isSelectionOn {
            menuBtn.setImage(nil, for: .normal)
            menuBtn.setTitle("Invite", for: .normal)
        } else {
            menuBtn.setImage(UIImage(named: "menu_3_dot"), for: .normal)
            menuBtn.setTitle(nil, for: .normal)
        }
        
    }
    private func handleContactSelection(type: Int, index: Int) {  // type  1: default  2. searched
        if !viewModel.isSelectionOn {
//            viewModel.isSelectionOn = true
            toggleContactSelectionView()
        }
        if type == 1 {
            if viewModel.selectedContactIds.contains(viewModel.defaultContacts[index].contact_user_id ?? 0) {
                viewModel.selectedContactIds = viewModel.selectedContactIds.filter({ $0 != viewModel.defaultContacts[index].contact_user_id ?? 0 })
            } else {
                viewModel.selectedContactIds.append(viewModel.defaultContacts[index].contact_user_id ?? 0)
            }
        } else {
            if viewModel.selectedContactIds.contains(viewModel.contactsSearchedFromRemote[index].contact_user_id ?? 0) {
               viewModel.selectedContactIds = viewModel.selectedContactIds.filter({ $0 != viewModel.contactsSearchedFromRemote[index].contact_user_id ?? 0 })
            } else {
               viewModel.selectedContactIds.append(viewModel.contactsSearchedFromRemote[index].contact_user_id ?? 0)
            }
        }
        self.tableView.reloadData()
        self.titleLabel.text = "\(viewModel.selectedContactIds.count) selected"
    }
    private func hideSelectionView() {
        toggleContactSelectionView()
        viewModel.selectedContactIds = []
        tableView.reloadData()
    }
    private func handleCellActions(index: Int, action: String) {
        switch action {
        case "request":
            
            let status = searchTF.text != "" ? viewModel.contactsSearchedFromRemote[index].status : viewModel.defaultContacts[index].status
            if status == 3 {
                let id = searchTF.text != "" ? viewModel.contactsSearchedFromRemote[index].contact_user_id : viewModel.defaultContacts[index].contact_user_id
                reviewRequest(id: id ?? 0)
            } else {
                let number = (searchTF.text != "" ? viewModel.contactsSearchedFromRemote[index].mobile_number : viewModel.defaultContacts[index].mobile_number) ?? "9999999999"
                Utilities.inviteContactMessage(phoneNumber: number)
            }
        default:
            AppDebug.shared.consolePrint("")
        }
        
    }
    // MARK: - Web Services
    func getContacts() {
        let params = ["limit": viewModel.limit, "offset": viewModel.offset]
        viewModel.getContacts(parameters: params) { (success, json, message) in
            if success {
                self.tableView.reloadData()
            }
        }
    }
    func contactSearch(query: String) {
        let params = ["query": query]
        viewModel.contactSearch(parameters: params) { (success, json, message) in
            if success {
                self.tableView.reloadData()
            }
        }
    }
    func contactSearchLocal(query: String, number: String) {
        let params = ["query": query, "number": number]
        showHud()
        viewModel.contactSearchLocal(parameters: params) { (success, json, message) in
            if success {
                self.viewModel.getTZCards(parameters: ["id":json.intValue], query: json.intValue) { (success, cardsData, message) in
                    self.hideHud()
                    if success, let _ = cardsData["cards"] {
                        NotificationCenter.default.post(name: Notification.Name.AppNotifications.ReviewBack, object: self, userInfo: cardsData)
                    } else {
                        self.alert(message: "No data found!")
                    }
                }
            } else {
                self.hideHud()
            }
        }
    }
    func inviteContacts() {
        if viewModel.isSelectionOn {
            if viewModel.selectedContactIds.count > 0 {
                showHud()
                viewModel.inviteContacts(parameters: ["contacts": viewModel.selectedContactIds]) { (_, _, message) in
                    self.hideHud()
                    self.showAutoToast(message: message)
                    self.hideSelectionView()
                }
            } else {
                alert(message: "Please select atleast one contact!")
            }
        }
    }
    func reviewRequest(id: Int) {
        view.endEditing(true)
        let params = ["userId": id]
        viewModel.reviewRequest(parameters: params) { (success, _, message) in
            if success {
                self.showAutoToast(message: "Requested Successfully")
            } else {
                self.alert(message: message)
            }
        }
    }
}
// MARK: - TableView methods Remote Search
extension ContactListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchTF.text == "" ? viewModel.defaultContacts.count : viewModel.contactsSearchedFromRemote.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searchTF.text == "" {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ContactListCell.rawValue, for: indexPath) as! ContactListCell
            cell.indexPath = indexPath
            cell.searchedContact = viewModel.defaultContacts[indexPath.row]
            cell.isContactSelected = viewModel.selectedContactIds.contains(viewModel.defaultContacts[indexPath.row].contact_user_id ?? 0)
            cell.requestBtnContainer.isHidden = viewModel.isSelectionOn
            cell.cellLongPressed = { index in
                self.handleContactSelection(type: 1, index: index)
            }
            cell.actions = { (index, action) in
                self.handleCellActions(index: index, action: action)
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ContactListCell.rawValue, for: indexPath) as! ContactListCell
            cell.indexPath = indexPath
            cell.searchedContact = viewModel.contactsSearchedFromRemote[indexPath.row]
            cell.isContactSelected = viewModel.selectedContactIds.contains(viewModel.contactsSearchedFromRemote[indexPath.row].contact_user_id ?? 0)
            cell.requestBtnContainer.isHidden = viewModel.isSelectionOn
            cell.cellLongPressed = { index in
                self.handleContactSelection(type: 2, index: index)
            }
            cell.actions = { (index, action) in
                self.handleCellActions(index: index, action: action)
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if searchTF.text == "" {
            if indexPath.row == (viewModel.defaultContacts.count - 1) && (viewModel.defaultContacts.count / 100) >= 1 {
                viewModel.offset += 1
                getContacts()
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.isSelectionOn {
            handleContactSelection(type: searchTF.text == "" ? 1 : 2, index: indexPath.row)
        } else {
            var contact_user_id = 0
            if searchTF.text! == "" {
                contact_user_id = viewModel.defaultContacts[indexPath.row].contact_user_id ?? Int(UserDefaults.standard.userId)!

            } else {
                contact_user_id = viewModel.contactsSearchedFromRemote[indexPath.row].contact_user_id ?? Int(UserDefaults.standard.userId)!

            }
            showHud()
            viewModel.getTZCards(parameters: ["id":contact_user_id], query: contact_user_id) { (success, cardsData, message) in
                self.hideHud()
                if success, let _ = cardsData["cards"] {
                    NotificationCenter.default.post(name: Notification.Name.AppNotifications.ReviewBack, object: self, userInfo: cardsData)
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.alert(message: "No data found!")
                }
            }
        }
    }
}
