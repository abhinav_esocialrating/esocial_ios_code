/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class ContactSearch {
	public var contact_user_id : Int?
	public var contact_name : String?
	public var image_id : String?
    public var mobile_number : String?
    public var is_business_contact : Int?
    public var status : Int?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let ContactSearch_list = ContactSearch.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of ContactSearch Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ContactSearch]
    {
        var models:[ContactSearch] = []
        for item in array
        {
            models.append(ContactSearch(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let ContactSearch = ContactSearch(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: ContactSearch Instance.
*/
	required public init?(dictionary: NSDictionary) {

		contact_user_id = dictionary["contact_user_id"] as? Int
		contact_name = dictionary["contact_name"] as? String
		image_id = dictionary["image_id"] as? String
        mobile_number = dictionary["mobile_number"] as? String
        is_business_contact = dictionary["is_business_contact"] as? Int
        status = dictionary["status"] as? Int
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.contact_user_id, forKey: "contact_user_id")
		dictionary.setValue(self.contact_name, forKey: "contact_name")
		dictionary.setValue(self.image_id, forKey: "image_id")

		return dictionary
	}

}
