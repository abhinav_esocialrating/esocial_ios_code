//
//  ContactListViewModel.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 05/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContactListViewModel: NSObject {
    var contactsSearchedFromRemote = [ContactSearch]()
    var defaultContacts = [ContactSearch]()
    var dropDownData = ["Resync contacts"]
    var limit = 100
    var offset = 1
    var isSelectionOn = false
    var selectedContactIds = [Int]()
    // MARK: - Web Services
    func contactSearch(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.contactSearch(parameters: parameters, method: .get) { (response) in
            
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    AppDebug.shared.consolePrint(jsonData)
                    self.contactsSearchedFromRemote = []
                    let data = jsonData.value(forKey: "data") as? NSArray
                    if (data?.count ?? 0) > 0 {
                        self.contactsSearchedFromRemote = ContactSearch.modelsFromDictionaryArray(array: data!)
                    }
                } catch {
                
                }
                completion(true, JSON(), "")
            default:
                let json = JSON(response.result.value ?? JSON.init())
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func contactSearchLocal(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.contactSearch(parameters: parameters, method: .get) { (response) in
            
            switch response.response?.statusCode {
            case 200:
                var contact_user_id = 0
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    self.contactsSearchedFromRemote = []
                    let data = jsonData.value(forKey: "data") as? NSArray
                    if (data?.count ?? 0) > 0 {
                        let contactsSearchedFromRemote = ContactSearch.modelsFromDictionaryArray(array: data!)
                        contact_user_id = contactsSearchedFromRemote[0].contact_user_id ?? 0
                    }
                } catch {
                
                }
                completion(true, JSON(contact_user_id), "")
            default:
                let json = JSON(response.result.value ?? JSON.init())
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func getTZCards(parameters: [String:Any], query: Int, completion: @escaping ((_ success: Bool, _ cardsData: [String:Any], _ message: String) -> Void)) {
        NetworkManager.getTZCards(parameters: query != 0 ? parameters : [:], query: "on", method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
            var cards = [TZCard]()
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData["data"] as! NSArray
                    cards = TZCard.modelsFromDictionaryArray(array: data)
                } catch {
                    
                }
                completion(true, ["cards": cards], "")
            default:
                let message = json["message"].stringValue
                completion(false, [:], message)
            }
        }
    }
    func getContacts(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.getContacts(parameters: parameters, method: .get) { (response) in
            
//            AppDebug.shared.debug {
//                let json = JSON(response.result.value ?? JSON.init())
//                AppDebug.shared.consolePrint(json)
//            }
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    let data = jsonData.value(forKey: "data") as? NSArray
                    if (data?.count ?? 0) > 0 {
                        let defaultContacts = ContactSearch.modelsFromDictionaryArray(array: data!)
                        self.defaultContacts.append(contentsOf: defaultContacts)
                    }
                } catch {
                
                }
                completion(true, JSON(), "")
            default:
                let json = JSON(response.result.value ?? JSON.init())
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func inviteContacts(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.inviteContacts(parameters: parameters, method: .post) { (response) in
            
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, JSON(), json["message"].stringValue)
            default:
                let json = JSON(response.result.value ?? JSON.init())
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func reviewRequest(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        NetworkManager.reviewRequest(parameters: parameters, method: .post) { (response) in
            
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)

            switch response.response?.statusCode {
            case 200:
                completion(true, JSON(), json["message"].stringValue)
            default:
                let json = JSON(response.result.value ?? JSON.init())
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
