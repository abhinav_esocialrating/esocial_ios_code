//
//  ContactListCell.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 05/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit

class ContactListCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var avatarLabel: UILabel!
    @IBOutlet weak var requestBtn: UIButton!
    @IBOutlet weak var requestBtnContainer: UIView!
    // MARK: - Variables
    var isContactSelected = false {
        didSet {
            contentView.backgroundColor = isContactSelected ? UIColor.SearchColors.tagBkg : .white
        }
    }
    var searchedContact: ContactSearch! {
        didSet {
            setRemoteValues()
        }
    }
    var contact: PhoneContact! {
        didSet {
            setValues()
        }
    }
    var contactDict: [String:Any]! {
        didSet {
            setValuesFromContactDict()
        }
    }
    var cellLongPressed: ((_ index: Int) -> Void)?
    var actions: ((_ index: Int, _ action: String) -> Void)?
    // MARK: - Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(_:)))
        gesture.minimumPressDuration = 1.0
        contentView.addGestureRecognizer(gesture)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        contactImage.layer.cornerRadius = contactImage.frame.height / 2
        requestBtn.setUpBorder(width: 0.8, color: UIColor.SearchColors.blue, radius: 8)
    }
    func setRemoteValues() {
        nameLabel.text = searchedContact.contact_name
        if let image_id = searchedContact.image_id, image_id != "" {
            contactImage.sd_setImage(with: URL(string: image_id), completed: nil)
        } else {
            contactImage.image = UIImage(named: "avatar-2")
        }
        avatarLabel.text = ""
        numberLabel.text = searchedContact.mobile_number
    }
    func setValues() {
        nameLabel.text = contact.name
//        numberLabel.text = contact.phoneNumber[0]
        contactImage.image = UIImage(named: CommonData.avatars.randomElement() ?? "")
    }
    func setValuesFromContactDict() {
        nameLabel.text = contactDict["name"] as? String
//        numberLabel.text = contact.phoneNumber[0]
        contactImage.image = UIImage(named: CommonData.avatars.randomElement() ?? "")
    }
    // MARK: - Actions
    @objc func longPressed(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        if longPressGestureRecognizer.state == .began {
            if isContactSelected {
                
            } else {
                print("long pressed")
                // callback to controller
                cellLongPressed?(indexPath.row)
                // toggle selection
            }
        }
        
    }
    
    @IBAction func requestTapped(_ sender: Any) {
        actions?(indexPath.row, "request")
    }
    
}
