//
//  NetworkManagerSearch.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 10/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension NetworkManager {
    // MARK: - Search tab
    class func getSearchData(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.search.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    
    class func getSearchFiltered(parameters: Parameters, filter: String, count: Bool, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        var url = ""
        if count {
            url = AppUrl.baseUrl.rawValue + AppUrl.searchFilterCount.rawValue //+ filter
        } else {
            url = AppUrl.baseUrl.rawValue + AppUrl.searchFilter.rawValue //+ filter
        }
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}
// MARK: - notification
extension NetworkManager {
    class func notification(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.notification.rawValue
        if method == .get {
            Alamofire.request(url, method: method, parameters: [:], encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
                completion(response)
            }
        } else {
            Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        }
    }
    class func scorerequestResponse(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.scorerequestResponse.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func refreshFCMToken(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.refreshFCMToken.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getNotificationCount(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.notificationCount.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}
// MARK: - Contacts
extension NetworkManager {
    class func contactSearch(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.contactSearch.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func contactEditName(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.contactEditName.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}
// MARK: - Location search

extension NetworkManager {
    class func searchLocation(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.googlePlacesUrl.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: [:]).responseJSON { (response) in
            completion(response)
        }
    }
    class func sendLocation(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.sendLocation.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}
