//
//  NetworkManagerChat.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 31/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
import Alamofire

extension NetworkManager {
    // MARK: - Search tab
    class func chatUpdateOnlineStatus(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.chatBaseUrl.rawValue + AppUrl.chatUpdateOnlineStatus.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: [:]).responseJSON { (response) in
            completion(response)
        }
    }
    class func chatGetHistory(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.chatBaseUrl.rawValue + AppUrl.chatGetHistory.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: [:]).responseJSON { (response) in
            completion(response)
        }
    }
    class func chatGetRecentChats(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.chatBaseUrl.rawValue + AppUrl.chatGetRecentChats.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: [:]).responseJSON { (response) in
            completion(response)
        }
    }
    class func chatSetReqAction(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.chatBaseUrl.rawValue + AppUrl.chatSetReqAction.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: [:]).responseJSON { (response) in
            completion(response)
        }
    }
    class func updateMessageStatus(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.chatBaseUrl.rawValue + AppUrl.updateMessageStatus.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: [:]).responseJSON { (response) in
            completion(response)
        }
    }
}
