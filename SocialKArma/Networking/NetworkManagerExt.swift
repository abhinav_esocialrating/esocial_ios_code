//
//  NetworkManagerExt.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 07/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

// MARK: - Social Media
extension NetworkManager {
    class func getInstagramProfile(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ json: JSON) -> Void)) {
        let url = Instagram.profileURL + Instagram.INSTAGRAM_ACCESS_TOKEN
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            completion(json)
        }
    }
    
    class func getLinkedInToken(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ json: JSON) -> Void)) {
        let url = LinkedIn.LinkedIn_ACCESS_TOKEN_URL
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            completion(json)
        }
    }
    
    class func getLinkedInProfile(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ json: JSON) -> Void)) {
        let url = LinkedIn.LinkedIn_PROFILE_URL
        let headers = ["Authorization": "Bearer \(LinkedIn.LinkedIn_ACCESS_TOKEN)"]
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            completion(json)
        }
    }
    
}
