//
//  AppURLs.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation

enum AppUrl: String {
    typealias RawValue = String
    
    // MARK:- Base URL
    #if DEBUG
    case baseUrl = "http://staging-api.trustze.com:3000/api/d2/"
//    case baseUrl = "http://staging-api.trustze.com:3000/api/d2/"
    #elseif RELEASE
    case baseUrl = "https://api.trustze.com/api/d2/"
    #endif
    
    #if DEBUG
    case chatBaseUrl = "http://staging-api.trustze.com:4000/"
    #elseif RELEASE
    case chatBaseUrl = "https://chat.trustze.com/"
    #endif
    
    // MARK:- Register
    case signUp = "auth/signup"
    case verifyOTP = "auth/verifyotp"
    case createProfile = "profile/basicDetails"
    case resendOTP = "auth/resendOtp"
    case refreshFCMToken = "/auth/fcmtoken/refresh"
    
    // MARK:- Login
    case login = "auth/login"
    case profilePic = "profile/profilepic"
    case profilePicRemove = "profile/delete"
    case logout = "logout"
    
    // MARK: - Invite Only flow
    case inviteOnlyCards = "config/cards"
    case registerRequest = "auth/request"
    
    // MARK:- System
    case getWorkProfile = "config/config"
    case getStaticConfig = "config/staticConfig"
    case getPopularSkill = "config/popular/skill"
    case getPopularBusinessService = "config/popular/business_service"
    case getCompensationConfig = "config/compensation"

    // MARK:- user-profile
    case lookingFor = "profile/lookingfor"
    case userInfo = "profile/userInfo"
    case inviteLink = "profile/invitelink"
    case about = "profile/about"
    case saveUserAboutInfo = "user/about/save"
    case userEducation = "profile/education"
    case getUserExperience = "profile/experience"
    case skills = "profile/skills"
    case skillsAssessment = "profile/skillsAssessment"
    case interest = "profile/interest"
    case interestAssessment = "profile/interestAssessment"
    case getBusinessInfo = "profile/businessInfo"
    case reportUser = "profile/report"
    case userFeedback = "profile/feedback"
    case certification = "profile/certification"
    case socialAccount = "profile/socialAccount"
    case network = "network"
    case competency = "profile/competency"
    case compensation = "profile/compensation"
    case engagement = "profile/engagement"
    case project = "profile/project"
    case userSkills = "profile/skills/new"
    case profileDetails = "profile/details"
    case searchStatus = "profile/searchStatus"

    // MARK: - ScoreCard
    case scorecard = "profile/scorecard"
    case reviewdonelist = "review/reviewdonelist"
    case reviewerslist = "review/reviewerslist"
    case user_invitation_list = "contact/user_invitation_list"

    // MARK:- User
    case ratingCards = "rating/rate"
    case reviewCards = "review/cards_v2"
    case reviewUser = "review/review"
    case reviewSelf = "review/selfAssessment"
    case reviewSwipe = "review/swipe"
    case postInviteCode = "tzpoints/invite"
    case inviteCount = "tzpoints/invitecount"
    case reviewRequest = "review/request"

    // MARK: - Sugegstions for education and experience
    case formConfig = "config/formConfig"
    case searchConfig = "config/searchConfig"
    case organisationsConfig = "config/organisation"
    case uploadContacts = "contact"
    case addToMasterTable = "config/master"
    case addToCategoryservicesMasterTable = "config/categoryservices"
    
    // MARK: - Search Tab
    case search = "search"
    case searchFilter = "search/"
    case searchFilterCount = "search/filter/"
    
    // MARK: - Notifications
    case notification = "notification"
    case notificationCount = "notification/count"
    
    // MARK: - Request Score
    case requestScore = "profile/requestScore"
    case userReviews = "profile/reviews"
    case scorerequestResponse = "profile/scorerequestResponse"
    
    // MARK: - Contacts
    case contactSearch = "contact/search"
    case contactEditName = "contact/change_contact_name"
    case deleteContact = "contacts"
    case inviteContacts = "contact/inviteAll"
    
    // MARK: - Location Service
    case googlePlacesUrl = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    case sendLocation = "profile/coordinates"
    
    // MARK: - External Links
    case DemoPrivacyPolicy = "http://ourgoodspace.com/privacy-policy"
    case DemoTermsAndConditions = "http://ourgoodspace.com/terms-of-services"
    case DemoFAQs = "ourgoodspace.com/faqs"
    case Contest = "http://trustze.com/contest"
    
    // MARK: - Chat
    case chatUpdateOnlineStatus = "updateOnlineStatus"
    case chatGetHistory = "getChatHistory"
    case chatGetRecentChats = "getRecentChats"
    case chatSetReqAction = "setReqAction"
    case updateMessageStatus = "updateMessageStatus"
    
    // MARK: - Points
    case tzPointsHistory = "tzpoints/history"
    case redeemPoints = "tzpoints/redeem"

    // MARK: - Feedback
    case feedBackQuestions = "feedback/questions"
    case feedBack = "feedback"
    
    // MARK: - Offers
    case offer = "offer"
    case offerMy = "offer/my"
    case offerComment = "offer/comment"
    case offerLike = "offer/like"
    case offerUser = "offer/user"
    case offerFilter = "offer/filter"
}
