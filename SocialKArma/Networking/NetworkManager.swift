//
//  NetworkManager.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 07/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import MobileCoreServices


class NetworkManager {
    static var header: HTTPHeaders = ["device-id": UDID ?? "1272yy2-2828u2-22nj22j", "device-type": "ios", "Content-Type": "application/json"]
    //"Authorization": UserDefaults.standard.Authorization,
    // MARK: - Register
    class func register(requestType: Int, parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let lastPathComponent = requestType == 1 ? AppUrl.signUp.rawValue : AppUrl.login.rawValue
        let url = AppUrl.baseUrl.rawValue + lastPathComponent
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    
    // MARK: - OTP screen
    class func verifyOTP(requestType: Int, parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.verifyOTP.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func resendOTP(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.resendOTP.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    
    // Basic profile
    class func createBasicProfile(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        
        let url = AppUrl.baseUrl.rawValue + AppUrl.createProfile.rawValue
        header["Authorization"] = UserDefaults.standard.Authorization
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
        
    }
    // upload profile pic
    class func uploadProfilePic(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        
        let url = AppUrl.baseUrl.rawValue + AppUrl.profilePic.rawValue
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "multipart/form-data"
        
        let img = parameters["file"] as? UIImage
        let imgData = img?.jpegData(compressionQuality: 0.5) ?? Data()
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imgData, withName: "file",fileName: "profile_pic", mimeType: "image/jpeg")
        }, usingThreshold: UInt64(UInt()), to: url, method: .post, headers: header) { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    completion(response)
                    header["Content-Type"] = "application/json"
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    // remove Profile Picture
    class func removeProfilePic(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/x-www-form-urlencoded"
        let url = AppUrl.baseUrl.rawValue + AppUrl.profilePic.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // Logout
    class func logout(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.logout.rawValue
        header["Authorization"] = UserDefaults.standard.Authorization
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            self.header.removeValue(forKey: UserDefaults.standard.Authorization)
            completion(response)
        }
    }
    // MARK: - Invite Only flow
    class func getInviteOnlyCards(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.inviteOnlyCards.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
            completion(response)
        }
    }
    class func registerRequest(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.registerRequest.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}
// MARK: - Review
extension NetworkManager {
    // Getting Feedback cards
    class func getTZCards(parameters: Parameters, query: String, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Content-Type"] = "application/json"
        header["Authorization"] = UserDefaults.standard.Authorization
        let url = AppUrl.baseUrl.rawValue + AppUrl.reviewCards.rawValue
        if query == "" {
            Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        } else {
            Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
                completion(response)
            }
        }
    }
    // Posting self Assessment/Feedback
    class func getSelfAssessment(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.reviewSelf.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // Post Feedback for one user
    class func rateUser(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.reviewUser.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // Submitting Swipe info for a card: IPN
    class func reviewSwipe(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.reviewSwipe.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // Posting Invite code for self
    class func postInviteCode(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        let url = AppUrl.baseUrl.rawValue + AppUrl.postInviteCode.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // Count of Invites that user has done till now
    class func inviteCount(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        let url = AppUrl.baseUrl.rawValue + AppUrl.inviteCount.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // Making a review request for one user from contact list screen
    class func reviewRequest(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        let url = AppUrl.baseUrl.rawValue + AppUrl.reviewRequest.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // MARK: - Contacts
    // Uploading user's contact to server
    class func uploadContacts(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        let url = AppUrl.baseUrl.rawValue + AppUrl.uploadContacts.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // Getting user's contacts from server in offset of 10/20
    class func getContacts(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        let url = AppUrl.baseUrl.rawValue + AppUrl.uploadContacts.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // Delete contact cards of one user
    class func deleteContactCards(url: String, parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // Inviting multiple contacts
    class func inviteContacts(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        let url = AppUrl.baseUrl.rawValue + AppUrl.inviteContacts.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}
// MARK: - User profile
extension NetworkManager {
    class func getLookingFor(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.lookingFor.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func saveLookingFor(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.lookingFor.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getUserInfo(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.userInfo.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getInviteLink(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.inviteLink.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getAboutMeInfo(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.about.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func saveAboutMeInfo(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.about.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getEducation(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        let url = AppUrl.baseUrl.rawValue + AppUrl.userEducation.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func saveEducation(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        let url = AppUrl.baseUrl.rawValue + AppUrl.userEducation.rawValue
        if method == .delete {
            header["Content-Type"] = "application/x-www-form-urlencoded"
            Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
                header["Content-Type"] = "application/json"
                completion(response)
            }
        } else {
            header["Content-Type"] = "application/json"
            Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        }
    }
    class func getExperience(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.getUserExperience.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func saveExperience(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        let url = AppUrl.baseUrl.rawValue + AppUrl.getUserExperience.rawValue
        if method == .delete {
            header["Content-Type"] = "application/x-www-form-urlencoded"
            Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
                header["Content-Type"] = "application/json"
                completion(response)
            }
        } else {
            Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        }
        
    }
    class func getBusinessInfo(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.getBusinessInfo.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func saveBusinessInfo(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.getBusinessInfo.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func deleteBusinessInfo(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/x-www-form-urlencoded"
        let url = AppUrl.baseUrl.rawValue + AppUrl.getBusinessInfo.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
            header["Content-Type"] = "application/json"
        }
    }

    class func getAllSkills(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.skills.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func saveSkills(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.skillsAssessment.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func deleteSkills(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.skills.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getAllInterests(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.interest.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func saveInterests(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.interestAssessment.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func deleteInterests(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.interest.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func reportUser(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.reportUser.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getUserFeedback(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        var encoding: ParameterEncoding!
        var url: String!
        var params: Parameters!
        if method == .get {
            encoding = URLEncoding.default
            let id = parameters["id"] as! String
            url = AppUrl.baseUrl.rawValue + AppUrl.userFeedback.rawValue + "/" + id
            params = [:]
        } else {
            encoding = JSONEncoding.default
            url = AppUrl.baseUrl.rawValue + AppUrl.userFeedback.rawValue
            params = parameters
        }
        Alamofire.request(url, method: method, parameters: params, encoding: encoding, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getCertification(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.certification.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func saveCertification(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.certification.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func socialAccount(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        var url = ""
        if method == .get {
            url = AppUrl.baseUrl.rawValue + AppUrl.socialAccount.rawValue + "/\(parameters["id"] as! String)"
            Alamofire.request(url, method: method, parameters: [:], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        } else {
            url = AppUrl.baseUrl.rawValue + AppUrl.socialAccount.rawValue
            Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        }
    }
    class func network(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        var url = ""
        if method == .get {
            url = AppUrl.baseUrl.rawValue + AppUrl.network.rawValue + "/\(parameters["id"] as! String)"
            Alamofire.request(url, method: method, parameters: [:], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        } else {
            url = AppUrl.baseUrl.rawValue + AppUrl.network.rawValue
            Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        }
    }
    class func competency(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        var url = ""
        if method == .get {
            url = AppUrl.baseUrl.rawValue + AppUrl.competency.rawValue + "/\(parameters["id"] as! String)"
            Alamofire.request(url, method: method, parameters: [:], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        } else {
            url = AppUrl.baseUrl.rawValue + AppUrl.competency.rawValue
            Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        }
    }
    
    class func saveCompensation(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        var url = AppUrl.baseUrl.rawValue + AppUrl.compensation.rawValue
        if method == .get {
            url += "/\(parameters["id"] as! String)"
            Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
                header["Content-Type"] = "application/json"
                completion(response)
            }
        } else {
            header["Content-Type"] = "application/json"
            Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        }
    }
    class func saveEngagement(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        var url = AppUrl.baseUrl.rawValue + AppUrl.engagement.rawValue
        if method == .get {
            url += "/\(parameters["id"] as! String)"
            Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
                header["Content-Type"] = "application/json"
                completion(response)
            }
        } else {
            header["Content-Type"] = "application/json"
            Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        }
    }
    class func projects(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        var url = AppUrl.baseUrl.rawValue + AppUrl.project.rawValue
        if method == .get {
            header["Content-Type"] = "application/json"
            url += "/\(parameters["id"] as! String)"
            Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        } else if method == .delete {
            header["Content-Type"] = "application/json"
            url += "/\(parameters["user_project_id"] as! Int)"
            Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        } else {
            header["Content-Type"] = "application/json"
            Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                completion(response)
            }
        }
    }
    class func getUserSkills(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.userSkills.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getProfileDetails(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.profileDetails.rawValue + "/\(parameters["id"] as! Int)"
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}



// MARK: - System
extension NetworkManager {
    // Getting purpose list of for joing the app, shown during sign up and profile
    class func getWorkProfile(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.getWorkProfile.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // Getting whole config data from
    class func getStaticConfigData(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.getStaticConfig.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    // Getting list of popular skills and interests
    class func getPopularConfigData(parameters: Parameters, configType: String, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        var url = ""
        if configType.contains("skill") {
            url = AppUrl.baseUrl.rawValue + AppUrl.getPopularSkill.rawValue
        } else {
            url = AppUrl.baseUrl.rawValue + AppUrl.getPopularBusinessService.rawValue
        }
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getCompensationConfig( parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.getCompensationConfig.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}

// MARK: - Sugegstions for education and experience
extension NetworkManager {
    class func getFormConfig(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.formConfig.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getSearchConfig(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.searchConfig.rawValue
        header["Authorization"] = UserDefaults.standard.Authorization
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getOrganisationConfig(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.organisationsConfig.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func addToMasterTable(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.addToMasterTable.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func addToCategoryservicesMasterTable(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        let url = AppUrl.baseUrl.rawValue + AppUrl.addToCategoryservicesMasterTable.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}

// MARK: - Request Score
extension NetworkManager {
    class func requestScore(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.requestScore.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    
    class func getUserReviews(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.userReviews.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    
    class func getScoreValues(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.userReviews.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}
// MARK: - Points
extension NetworkManager {
    class func tzPointsHistory(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.tzPointsHistory.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func redeemPoints(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.redeemPoints.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func changeSearchStatus(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.searchStatus.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: method == .get ? URLEncoding.default : JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}
// MARK: - Feedback
extension NetworkManager {
    class func getFeedback(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.feedBackQuestions.rawValue
//        let url = AppUrl.baseUrl.rawValue + AppUrl.tzPointsHistory.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func postFeedback(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.feedBack.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}
// MARK: - ScoreCard
extension NetworkManager {
    class func getScoreCard(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.scorecard.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getReviewDoneList(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.reviewdonelist.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getReviewersList(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.reviewerslist.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getUserInvitationList(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.user_invitation_list.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
}
// MARK: - Offers
extension NetworkManager {
    class func saveOffer(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.offer.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func filterOffer(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Content-Type"] = "application/json"
        let url = AppUrl.baseUrl.rawValue + AppUrl.offerFilter.rawValue
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
        }
    }
    class func getOffers(parameters: Parameters, isMyOffer: Bool, isOtherUserOffer: Bool, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        var url = ""
        if isMyOffer {
            url = AppUrl.baseUrl.rawValue + AppUrl.offerMy.rawValue
        } else if isOtherUserOffer {
            url = AppUrl.baseUrl.rawValue + AppUrl.offerUser.rawValue + "/\(parameters["otherUserId"] as! Int)"
        } else {
            url = AppUrl.baseUrl.rawValue + AppUrl.offer.rawValue
        }
        if method == .delete {
            url += "/\(parameters["offerId"] as! Int)"
            header["Content-Type"] = nil
        }
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
            header["Content-Type"] = "application/json"
        }
    }
    class func offerDetails(parameters: Parameters, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = nil
        var url = AppUrl.baseUrl.rawValue + AppUrl.offer.rawValue
        url += "/\(parameters["offerId"] as! Int)"
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
            header["Content-Type"] = "application/json"
        }
    }
    class func deleteOffers(parameters: Parameters, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = nil
        var url = AppUrl.baseUrl.rawValue + AppUrl.offer.rawValue
        url += "/\(parameters["offerId"] as! Int)"
        Alamofire.request(url, method: .delete, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(response)
            header["Content-Type"] = "application/json"
        }
    }
    class func offerComment(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        header["Content-Type"] = "application/json"
        var url = AppUrl.baseUrl.rawValue + AppUrl.offerComment.rawValue
        if method == .get {
            url += "/\(parameters["offerId"] as! Int)"
        } else if method == .delete {
            url += "/\(parameters["userCommentId"] as! Int)"
            header["Content-Type"] = ""
        }
        Alamofire.request(url, method: method, parameters: method == .delete ? [:] : parameters, encoding: (method == .post  || method == .put) ? JSONEncoding.default : URLEncoding.default, headers: header).responseJSON { (response) in
            header["Content-Type"] = "application/json"
            completion(response)
        }
    }
    class func likeOffer(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ response: DataResponse<Any>) -> Void)) {
        header["Authorization"] = UserDefaults.standard.Authorization
        
        var url = AppUrl.baseUrl.rawValue + AppUrl.offerLike.rawValue
        if method != .post {
            url += "/\(parameters["offerId"] as! Int)"
            header["Content-Type"] = nil
        }
        else {
            header["Content-Type"] = "application/json"
        }
        Alamofire.request(url, method: method, parameters: method == .post ? parameters : [:], encoding: method == .post ? JSONEncoding.default : URLEncoding.default, headers: header).responseJSON { (response) in
            header["Content-Type"] = "application/json"
            completion(response)
        }
    }
}
