//
//  AppDelegate.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 09/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import FAPanels
import FirebaseCore
import FirebaseMessaging
import UserNotifications
import FirebaseDynamicLinks
import GoogleSignIn
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let reachability = try! Reachability()
    var isVersionAlertShown = false
    var urlDynamic : URL?
    let customURLScheme = "trustze"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        checkForAppStartPoint()
        
        //IQKeyboardManager setup
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.disabledDistanceHandlingClasses = [ChatViewController.self, OfferDetailsViewController.self, OfferCommentViewController.self]
        IQKeyboardManager.shared.disabledToolbarClasses = [ChatViewController.self, OfferDetailsViewController.self, OfferCommentViewController.self]
        IQKeyboardManager.shared.enabledTouchResignedClasses = [ChatViewController.self, OfferDetailsViewController.self, OfferCommentViewController.self]
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
        //Social networking set up
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
//        TWTRTwitter.sharedInstance().start(withConsumerKey: "xxxxxxxxxxxxxxxxxxxxx", consumerSecret: "ssabdavhjdafvdhjavdhjavdahjdvahdvahdvahjd")
        
        //Firebase set up
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = self.customURLScheme
        FirebaseApp.configure()
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
                
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM)
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        Messaging.messaging().delegate = self
        UIApplication.shared.registerForRemoteNotifications()
        
        if let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable:Any] {
            
            print(userInfo)
        } else {            
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.addContactStoreChangeObserver()
            self.getStaticConfig(parameters: [:])
        }
        
        addBackgroundSchedulerForContactSync()
        UIViewController.changeSearchStatus(parameters: [:], method: .get) {_ in }
        return true
    }
    
    // Open URL
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        let appId: String = Settings.appID ?? ""
//        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" {
//            return ApplicationDelegate.shared.application(app, open: url, options: options)
//        }
        if url.scheme != nil && url.scheme!.hasPrefix("whatsapp") {
            return true
        } else if url.scheme != nil && url.scheme == "trustze" {
            let data = url.getQueryParams()
            if let deep_link_id = data["deep_link_id"] as? String, let deep_link_url = URL(string: deep_link_id.removingPercentEncoding ?? "") {
                let invite = deep_link_url["invite"]
                UserDefaults.standard.TZInviteCode = invite ?? ""
            }
        }
        return false
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "SocialKArma")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    // MARK: - Remote notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for notifications: \(error.localizedDescription)")
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
              willPresent notification: UNNotification,
              withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        if let type = userInfo["type"] as? String, type == PushNotificationType.CHAT.rawValue {
            handleNotifications(userInfo: userInfo, type: .willPresent)
        } else {
//            NotificationCenter.default.post(name: Notification.Name.PushReceived, object: self, userInfo: userInfo)
        }
        completionHandler([.alert, .badge, .sound])
    }
    
}

// MARK: - Push Notification
extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        if let token = Messaging.messaging().fcmToken {
            UserDefaults.standard.FirebaseInstanceId = token
            refreshFCMToken(parameters: ["token": token])
        }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification.request.content.userInfo)
        let userInfo = response.notification.request.content.userInfo
        let type = userInfo["type"] as? String
//        handleNotifications(userInfo: userInfo, type: .didReceiveResponse)
        navigateToNotificationScreen(type: type ?? "")
        completionHandler()
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("didReceiveRemoteNotification")
    }
}
// MARK: - Handling Push Notifications
extension AppDelegate {
    func navigateToNotificationScreen(type: String) {
        if let panelVC = appDelegate?.window?.rootViewController as? FAPanelController, let tbc = panelVC.center as? TabBarController {
            
            if let presented = tbc.presentedViewController {
                presented.dismiss(animated: false) {
                    self.openNotificationScreen(tbc: tbc, type: type)
                }
            } else {
                self.openNotificationScreen(tbc: tbc, type: type)
            }
        }
    }
    func openNotificationScreen(tbc: TabBarController, type: String) {
        let vc = NotificationsViewController.instantiate(fromAppStoryboard: .Main)
        vc.isFromPush = true
        vc.notificationType = type
        tbc.present(vc, animated: true, completion: nil)
    }
    func handleNotifications(userInfo: [AnyHashable:Any], type: NotificationHandleType) {
        if type == .didReceiveResponse { } else {
            if type == .willPresent {
                if let panelVC = appDelegate?.window?.rootViewController as? FAPanelController, let tbc = panelVC.center as? TabBarController {   //, tbc.selectedIndex == 2 {
                    
                    var editedUserInfo = userInfo
                    editedUserInfo["notificationHandleType"] = type.rawValue
                    
                    if let presented = tbc.presentedViewController, presented is ChatNavigationController  {
                        
                        NotificationCenter.default.post(name: Notification.Name.PushReceivedChat, object: nil, userInfo: editedUserInfo)
                        
                    } else {
                        NotificationCenter.default.post(name: Notification.Name.PushReceived, object: self, userInfo: userInfo)
                    }
                }
            }
        }
    }
}
// MARK: - Helper methods
extension AppDelegate {
    func checkForAppStartPoint() {
        
//        openOnboardingFlow()
        
        if UserDefaults.standard.Authorization != "" && !UserDefaults.standard.isFormComplete {
            setUpUserDetailsRootView()  // If sign up is not complete
        } else if UserDefaults.standard.Authorization != "" {
            let currentVersionStr = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
            if self.compareNumeric(current: currentVersionStr, store: UserDefaults.standard.minimumAppVersion) ==  .orderedDescending {
                setUpLoginRootView()   // Not logged in
            } else {
                PointsHandler.shared.getSavedPoints()
                setUpSideMenuAndRootView()  // Already logged in
            }
        } else {
            setUpLoginRootView()   // Not logged in
        }
    }
    func setUpSideMenuAndRootView() {
        let leftMenuVC = SideMenuController.instantiate(fromAppStoryboard: .Home)
        let centerVC = TabBarController.instantiate(fromAppStoryboard: .Home)
        let rootController = FAPanelController.instantiate(fromAppStoryboard: .Home)
        rootController.leftPanelPosition = .front
        rootController.configs.bounceOnLeftPanelOpen = false
        rootController.configs.shadowOffset = CGSize.zero
        rootController.configs.shadowOppacity = 0
        rootController.configs.panFromEdge = true
        rootController.configs.minEdgeForLeftPanel = 20.0
//        rootController.configs.canRecognizePanGesture = true
        rootController.center(centerVC).left(leftMenuVC)
        setOnWindow(vc: rootController)
    }
    func setUpLoginRootView() {
        let loginNC = OnboardingNavigationController.instantiate(fromAppStoryboard: .Onboarding)
        setOnWindow(vc: loginNC)
    }
    func openOnboardingFlow() {
        let vc = OnboardingNavigationController.instantiate(fromAppStoryboard: .Onboarding)
//        let nvc = UINavigationController(rootViewController: vc)
//        nvc.isNavigationBarHidden = true
        setOnWindow(vc: vc)
    }
    func setUpUserDetailsRootView() {
        OnboardingProcess.shared.createOnboardingDetailDatasource()
        OnboardingProcess.shared.currentIndex += 1
        let vc = LookingForViewController.instantiate(fromAppStoryboard: .Onboarding)
        let nvc = UINavigationController(rootViewController: vc)
        nvc.isNavigationBarHidden = true
        setOnWindow(vc: nvc)
    }
    func setOnWindow(vc: UIViewController) {
        if let window = window {
            window.rootViewController = vc
            window.makeKeyAndVisible()
        } else {
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = vc
            window?.makeKeyAndVisible()
        }
    }
    func compareNumeric(current: String, store: String) -> ComparisonResult {
        return current.compare(store, options: .numeric)
    }
}
/*
 if let chatList = nvc.viewControllers[0] as? ChannelListViewController, chatList.responds(to: #selector(ChannelListViewController.pushReceivedChat(_:))) {
     NotificationCenter.default.post(name: Notification.Name.PushReceivedChat, object: nil, userInfo: userInfo)
 } else {
     tbc.selectedIndex = 2
 }
 */
