//
//  AppDelegateExtension.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 24/09/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import SwiftyJSON
import FirebaseDynamicLinks
import BackgroundTasks

extension AppDelegate {
    func addContactStoreChangeObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(contactStoreDidChange), name: Notification.Name.CNContactStoreDidChange, object: nil)
    }
    @objc func contactStoreDidChange(_ notification: Notification) {
        UserContacts.sharedInstance.loadContacts()
        print("Contact Store Updated")
    }
}

// MARK: - Background fetch
extension AppDelegate {
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
//        if #available(iOS 13.0, *) {
//        } else {
            periodicallySyncContacts()
//        }
    }
    func addBackgroundSchedulerForContactSync() {
//        if #available(iOS 13.0, *) {
//            BGTaskScheduler.shared.register(forTaskWithIdentifier: "PeriodicContactSyncing", using: DispatchQueue.global()) { (task) in
//                self.periodicallySyncContacts()
//            }
//        } else {
            UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
//        }
    }
    func periodicallySyncContacts() {
        let currentTime = Date.timeIntervalSinceReferenceDate
        if (currentTime - UserDefaults.standard.latestContactSync) >= (60*60*24*7) {
            UserContacts.sharedInstance.loadContactsInBkg()
        }
    }

}
// MARK: - Web Services
extension AppDelegate {
    
    func getStaticConfig(parameters: [String:Any]) {
        NetworkManager.getStaticConfigData(parameters: parameters, method: .get) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint(json)
//            print(json)
            switch response.response?.statusCode {
            case 200:
                let list = json["data"]
                ConfigDataModel.shared.parseData(json: list)
            default:
                _ = json["message"].stringValue
            }
        }
    }
    func refreshFCMToken(parameters: [String:Any]) {
        NetworkManager.refreshFCMToken(parameters: parameters, method: .post) { (response) in
            AppDebug.shared.debug {
                let json = JSON(response.result.value ?? JSON.init())
                print(json)
            }
        }
    }
}
// MARK: - State methods
extension AppDelegate {
    // MARK: - Application state methods
       func applicationWillResignActive(_ application: UIApplication) {
            
       }

       func applicationDidEnterBackground(_ application: UIApplication) {
           SocketIOManager.shared.chatUpdateOnlineStatus(status: 0)
       }

       func applicationWillEnterForeground(_ application: UIApplication) {
            
       }

       func applicationDidBecomeActive(_ application: UIApplication) {
           SocketIOManager.shared.chatUpdateOnlineStatus(status: 1)
       }

       func applicationWillTerminate(_ application: UIApplication) {
           self.saveContext()
       }
}
extension AppDelegate {
    // MARK: - Dynamic Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            // Extracting the invite code
            let url = dynamiclink?.url
//            self.urlDynamic = url
            let data = url?.getQueryParams()
            if let code = data?["invite"] as? String {
                UserDefaults.standard.TZInviteCode = code
            }
            if let urlComponents = URLComponents(string: url?.absoluteString ?? ""), let queryItems = urlComponents.queryItems {
                // for example, we will get the first item name and value:
                _ = queryItems[0].name // encodedMessage
                let value = queryItems[0].value // PD94bWwgdmVyNlPg==
                UserDefaults.standard.TZInviteCode = value ?? ""
            }
        }
        return handled
    }
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        _ = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            // Extracting the invite code
            let url = dynamiclink?.url
            let data = url?.getQueryParams()
            if let code = data?["code"] as? String {
                UserDefaults.standard.TZInviteCode = code
            }
            if let urlComponents = URLComponents(string: url?.absoluteString ?? ""), let queryItems = urlComponents.queryItems {
                // for example, we will get the first item name and value:
                _ = queryItems[0].name // encodedMessage
                _ = queryItems[0].value // PD94bWwgdmVyNlPg==
            }
        }
    }
}
