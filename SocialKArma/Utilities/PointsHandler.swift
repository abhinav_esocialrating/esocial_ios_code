//
//  PointsHandler.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 20/02/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class PointsHandler: NSObject {

    static let shared = PointsHandler()
    private override init() {
    }
    
    var hasChanged: Bool = false
    
    var points: Int = 0
    // For notification badge
    var isNotification = false
    var isChatNotificationBadge = false
    var isFeedbackDetailNotification = false
    
    func flashButton(button: UIButton) {
        if hasChanged {
            hasChanged = false
            button.layer.borderColor = UIColor.clear.cgColor
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = .red
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                button.layer.borderColor = UIColor.NewTheme.darkGray.cgColor
                button.setTitleColor(UIColor.NewTheme.darkGray, for: .normal)
                button.backgroundColor = .clear
            }
        }
    }
    func flipCoin(button: UIButton) {
        if hasChanged {
            hasChanged = false
            let animation = CATransition()
            animation.type = CATransitionType(rawValue: "rotate")
            animation.startProgress = 0
            animation.endProgress = 1.0
            animation.subtype = CATransitionSubtype(rawValue: "fromTop")
            animation.duration = 0.5
            animation.repeatCount = 3

            button.layer.add(animation, forKey: "animation")
        }
    }
    func updateTZPoints(points: Int, changedToBeShown: Bool) {
        // update shared karma points
        self.points = points
        // Update on the disk
        var userInfo = UserDefaults.standard.userInfo
        let data = userInfo.data(using: .utf8)
        var json = JSON(data ?? Data())
        var dict = json.dictionaryObject
        if let savedPoints = dict?["karma_points"] as? Int {
            if savedPoints != points && changedToBeShown {
                hasChanged = true
            }
        }
        dict?["karma_points"] = points
        json = JSON(dict ?? [:])
        userInfo = json.rawString() ?? ""
        UserDefaults.standard.userInfo = userInfo
    }
    func getSavedPoints() {
        let userInfo = UserDefaults.standard.userInfo
        if userInfo != "" {
            let data = userInfo.data(using: .utf8)
            let json = JSON(data ?? Data())
            if let savedPoints = json["karma_points"].int {
                points = savedPoints
            }
        }
    }
    
    // MARK: - Web Service
    func getNotificationCount(completion: @escaping (_ data: [String:Any]) -> Void) {
        NetworkManager.getNotificationCount(parameters: [:], method: .get) { (response) in
            
            switch response.response?.statusCode {
            case 200:
                let json = JSON(response.result.value ?? JSON.init())
                AppDebug.shared.consolePrint(json)
                let list = json["data"].dictionaryObject
                completion(list ?? [:])
            default:
                AppDebug.shared.consolePrint("Do nothing!")
                completion([:])
            }
        }
    }

}
