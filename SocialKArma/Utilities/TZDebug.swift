//
//  TZDebug.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 06/07/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import UIKit

class AppDebug: NSObject {
    // MARK: - Singleton instance
    struct Static {
        fileprivate static var instance: AppDebug?
    }
    class var shared: AppDebug {
        if Static.instance == nil {
            Static.instance = AppDebug()
        }
        return Static.instance!
    }
    func dispose() {
        AppDebug.Static.instance = nil
        print("Disposed Singleton instance")
    }
    
    // MARK: - Methods
    func consolePrint(_ message: Any) {
        debug {
            print(message)
        }
    }
    func debug(task: () -> Void) {
        #if DEBUG
            task()
        #endif
    }
}
