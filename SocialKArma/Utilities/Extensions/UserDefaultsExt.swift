//
//  UserDefaultsExt.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 07/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation

extension UserDefaults {
    var Authorization: String {
        get {
            return UserDefaults.standard.object(forKey: "Authorization") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "Authorization")
        }
    }
    var userId: String {
        get {
            return UserDefaults.standard.object(forKey: "userId") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "userId")
        }
    }
    var userName: String {
        get {
            return UserDefaults.standard.object(forKey: "userName") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "userName")
        }
    }
    var userPhoneNumber: String {
        get {
            return UserDefaults.standard.object(forKey: "userPhoneNumber") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "userPhoneNumber")
        }
    }
    var FirebaseInstanceId: String {
        get {
            return UserDefaults.standard.object(forKey: "FirebaseInstanceId") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "FirebaseInstanceId")
        }
    }
    var TZInviteCode: String {
        get {
            return UserDefaults.standard.object(forKey: "TZInviteCode") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "TZInviteCode")
        }
    }
    var TZInviteLink: String {
        get {
            return UserDefaults.standard.object(forKey: "TZInviteLink") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "TZInviteLink")
        }
    }
    var userInfo: String {
        get {
            return UserDefaults.standard.object(forKey: "userInfo") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "userInfo")
        }
    }
    var isCoachMarkDone: Bool {
        get {
            return UserDefaults.standard.object(forKey: "isCoachMarkDone") as? Bool ?? false
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "isCoachMarkDone")
        }
    }
    var isFormComplete: Bool {
        get {
            return UserDefaults.standard.object(forKey: "isFormComplete") as? Bool ?? false
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "isFormComplete")
        }
    }
    var LocationUpdateTime: Double {
        get {
            return UserDefaults.standard.object(forKey: "LocationUpdateTime") as? Double ?? 0.0
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "LocationUpdateTime")
        }
    }
    var isContactPermission: Bool {
        get {
            return UserDefaults.standard.object(forKey: "isContactPermission") as? Bool ?? false
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "isContactPermission")
        }
    }
    var minimumAppVersion: String {
        get {
            return UserDefaults.standard.object(forKey: "minimumAppVersion") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "minimumAppVersion")
        }
    }
}

// MARK: - Config data
extension UserDefaults {
    var workProfile: String {
        get {
            return UserDefaults.standard.object(forKey: "workProfile") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "workProfile")
        }
    }
    var purpose: String {
        get {
            return UserDefaults.standard.object(forKey: "purpose") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "purpose")
        }
    }
}

// MARK: - Local storage purpose

extension UserDefaults {
    var EducationList: String {
        get {
            return UserDefaults.standard.object(forKey: "EducationList") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "EducationList")
        }
    }
    var ExperienceList: String {
        get {
            return UserDefaults.standard.object(forKey: "ExperienceList") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "ExperienceList")
        }
    }
    var TotalExperience: String {
        get {
            return UserDefaults.standard.object(forKey: "TotalExperience") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "TotalExperience")
        }
    }
    var AboutMe: String {
        get {
            return UserDefaults.standard.object(forKey: "AboutMe") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "AboutMe")
        }
    }
    var ScoreCard: [String:Any] {
        get {
            return UserDefaults.standard.object(forKey: "ScoreCard") as? [String:Any] ?? [:]
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "ScoreCard")
        }
    }
    var GCoins: [String:Any] {
        get {
            return UserDefaults.standard.object(forKey: "GCoins") as? [String:Any] ?? [:]
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "GCoins")
        }
    }
    var TZCards: String {
        get {
            return UserDefaults.standard.object(forKey: "TZCards") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "TZCards")
        }
    }
}
// MARK: - Coach Marks
extension UserDefaults {
    var selfProfileCoachMarks: Bool {
        get {
            return UserDefaults.standard.object(forKey: "selfProfileCoachMarks") as? Bool ?? false
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "selfProfileCoachMarks")
        }
    }
    var othersProfileCoachMarks: Bool {
        get {
            return UserDefaults.standard.object(forKey: "othersProfileCoachMarks") as? Bool ?? false
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "othersProfileCoachMarks")
        }
    }
    var searchCoachMarks: Bool {
        get {
            return UserDefaults.standard.object(forKey: "searchCoachMarks") as? Bool ?? false
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "searchCoachMarks")
        }
    }
}
// MARK: - Coach Mark cards
extension UserDefaults {
    var isFeedbackCoachMarkShown: Bool {
        get {
            return UserDefaults.standard.object(forKey: "isFeedbackCoachMarkShown") as? Bool ?? false
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "isFeedbackCoachMarkShown")
        }
    }
    var isScoreCoachMarkShown: Bool {
        get {
            return UserDefaults.standard.object(forKey: "isScoreCoachMarkShown") as? Bool ?? false
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "isScoreCoachMarkShown")
        }
    }
    var isFindCoachMarkShown: Bool {
        get {
            return UserDefaults.standard.object(forKey: "isFindCoachMarkShown") as? Bool ?? false
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "isFindCoachMarkShown")
        }
    }
    var isProfileCoachMarkShown: Bool {
        get {
            return UserDefaults.standard.object(forKey: "isProfileCoachMarkShown") as? Bool ?? false
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "isProfileCoachMarkShown")
        }
    }
}
// MARK: - Reported Users
extension UserDefaults {
    var reportedUsers: [String] {
        get {
            return UserDefaults.standard.object(forKey: "reportedUsers") as? [String] ?? []
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "reportedUsers")
        }
    }
}
// MARK: - Demo Account for review
extension UserDefaults {
    var isDemoPhoneNumber: Bool {
        get {
            return UserDefaults.standard.object(forKey: "isDemoPhoneNumber") as? Bool ?? false
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "isDemoPhoneNumber")
        }
    }
}
// MARK: - Actively looking / Search status
extension UserDefaults {
    var SearchStatus: [String: Any] {
        get {
            return UserDefaults.standard.object(forKey: "SearchStatus") as? [String: Any] ?? ["friendStatus": 0, "professionalStatus": 0]
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "SearchStatus")
        }
    }
}
// MARK: - Periodic contact syncing
extension UserDefaults {
    var latestContactSync: Double {
        get {
            return UserDefaults.standard.object(forKey: "latestContactSync") as? Double ?? 0.0
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "latestContactSync")
        }
    }
}
// MARK: - Logout case
extension UserDefaults {
    func clearDataOnLogout() {
        UserDefaults.standard.Authorization = ""
        UserDefaults.standard.userId = ""
        UserDefaults.standard.userPhoneNumber = ""
        UserDefaults.standard.userName = ""
        UserDefaults.standard.TZInviteCode = ""
        UserDefaults.standard.TZInviteLink = ""
        UserDefaults.standard.userInfo = ""
//        UserDefaults.standard.isCoachMarkDone = false

        UserDefaults.standard.EducationList = ""
        UserDefaults.standard.ExperienceList = ""
        UserDefaults.standard.TotalExperience = ""
        UserDefaults.standard.AboutMe = ""
        UserDetails.shared.reset()
        PointsHandler.shared.points = 0
        UserDefaults.standard.isContactPermission = false
        
        // Coach mark cards
        UserDefaults.standard.isFeedbackCoachMarkShown = false
        UserDefaults.standard.isScoreCoachMarkShown = false
        UserDefaults.standard.isFindCoachMarkShown = false
        UserDefaults.standard.isProfileCoachMarkShown = false
    }
}
