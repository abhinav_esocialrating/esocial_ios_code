//
//  NotificationExt.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 05/06/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation

extension Notification.Name {
    public struct Points {
        public static let didChange = Notification.Name(rawValue: "Karma_Points_Changed")
    }
    
    public struct AppNotifications {
        public static let ReviewBack = Notification.Name(rawValue: "ReviewBack")
        public static let DidEnterForegroundCheckForContact = Notification.Name(rawValue: "DidEnterForegroundCheckForContact")
        public static let ProfilePicChanged = Notification.Name(rawValue: "ProfilePicChanged")
        public static let NameChanged = Notification.Name(rawValue: "NameChanged")
        public static let SelfReview = Notification.Name(rawValue: "SelfReview")
    }
    public static let PushReceived = Notification.Name(rawValue: "PushReceived")
    public static let PushReceivedChat = Notification.Name(rawValue: "PushReceivedChat")
    public static let PushReceivedScoreReq = Notification.Name(rawValue: "PushReceivedScoreReq")
    public static let NotificationCount = Notification.Name(rawValue: "NotificationCount")
    public static let SearchStatus = Notification.Name(rawValue: "SearchStatus")

    
    public struct Profile {
        public static let AboutMe = Notification.Name(rawValue: "AboutMe")
        public static let LookingFor = Notification.Name(rawValue: "LookingFor")
        public static let Skills = Notification.Name(rawValue: "Skills")
        public static let SkillsWithTypes = Notification.Name(rawValue: "SkillsWithTypes")
        public static let Edu = Notification.Name(rawValue: "Edu")
        public static let feedback_text = Notification.Name(rawValue: "feedback_text")
        public static let socialURLs = Notification.Name(rawValue: "socialURLs")
        public static let relationshipTags = Notification.Name(rawValue: "relationshipTags")
        public static let competency = Notification.Name(rawValue: "competency")
        public static let compensation = Notification.Name(rawValue: "compensation")
        public static let hourlyEngagement = Notification.Name(rawValue: "hourlyEngagement")
        public static let projects = Notification.Name(rawValue: "projects")

    }

}
