//
//  TableCollection.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 21/05/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation

protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {

    // Use if reuse identifier is same as view class name
    static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
    
}
extension UITableView {
    func registerFromXib(name: String) {
        register(UINib(nibName: name, bundle: nil), forCellReuseIdentifier: name)
    }
}

// MARK: - Cells
extension UICollectionView {
    func registerFromXib(name: String) {
        register(UINib(nibName: name, bundle: nil), forCellWithReuseIdentifier: name)
    }
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
        }
        return cell
    }
}
extension UITableViewCell {
    private static var _indexPath = [String:IndexPath]()
    var indexPath: IndexPath {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return UITableViewCell._indexPath[tmpAddress] ?? IndexPath(row: 0, section: 0)
        }
        set(newValue) {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            UITableViewCell._indexPath[tmpAddress] = newValue
        }
    }
}
extension UICollectionViewCell {
    private static var _indexPath = [String:IndexPath]()
    var indexPath: IndexPath {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return UICollectionViewCell._indexPath[tmpAddress] ?? IndexPath(row: 0, section: 0)
        }
        set(newValue) {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            UICollectionViewCell._indexPath[tmpAddress] = newValue
        }
    }
}

extension IndexPath {
    static var zero: IndexPath {
        get {
            return IndexPath(row: 0, section: 0)
        }
    }
}
