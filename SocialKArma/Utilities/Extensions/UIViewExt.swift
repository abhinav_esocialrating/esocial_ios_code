//
//  UIViewExt.swift
//  Classmate_IOS
//
//  Created by Jihad Ismail on 10/30/17.
//  Copyright © 2017 Inova. All rights reserved.
//



import Foundation
import UIKit

extension UIView {
    
// MARK:-  functions
    func setGradient (gradientColors: [CGColor], gradientLocations: [NSNumber]?, startPoint: CGPoint, endPoint: CGPoint ) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        
        if let lLocation  = gradientLocations {
            gradientLayer.locations = lLocation
        }
        
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
        self.layoutIfNeeded()
    }
    func setGradient (gradientColors: [CGColor], gradientLocations: [NSNumber]?, startPoint: CGPoint, endPoint: CGPoint, cornerRadius: CGFloat ) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        
        if let lLocation  = gradientLocations {
            gradientLayer.locations = lLocation
        }
        
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        
        gradientLayer.frame = self.bounds
        gradientLayer.cornerRadius = cornerRadius
        self.layer.insertSublayer(gradientLayer, at: 0)
        self.layoutIfNeeded()
    }
    func createVerticalGradient(colors: [CGColor]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
//        gradientLayer.locations = [0.0, 0.5, 1.0]
        gradientLayer.frame.size = self.bounds.size
        gradientLayer.frame.origin = self.bounds.origin
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    func fadeIn() {
        // Move our fade out code from earlier
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: nil)
    }
    
    
    func fadeOut() {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
        }, completion: nil)
    }
    
    
    
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)
        
    }
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 3
    func applyBorderAndShadow(radius: CGFloat, shadowRadius: CGFloat, shadowKey: String = "") {
        
        let shadowSize : CGFloat = 1.5
        let shadowPath = UIBezierPath(roundedRect: CGRect(x: -shadowSize / 2,
                                                          y: -shadowSize / 2,
                                                          width: frame.size.width + shadowSize,
                                                          height: frame.size.height + shadowSize), cornerRadius: shadowRadius)
        layer.masksToBounds = false
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = 0.25
        layer.shadowPath = shadowPath.cgPath
        layer.shadowRadius = 2
        layer.setValue(shadowKey, forKey: kShadowLayer)
        
        layer.cornerRadius = radius
    }
    
    func createDotedBorder(width: CGFloat, color: UIColor) {
        let viewBorder = CAShapeLayer()
        viewBorder.strokeColor = color.cgColor
        viewBorder.lineDashPattern = [2, 2]
        viewBorder.frame = self.bounds
        viewBorder.fillColor = nil
        viewBorder.path = UIBezierPath(rect: self.bounds).cgPath
        self.layer.addSublayer(viewBorder)
    }
    
    func dashedBorderLayerWithColor(color:CGColor, view : UIView) -> CAShapeLayer {
        
        let  borderLayer = CAShapeLayer()
        borderLayer.name  = "borderLayer"
        let frameSize = view.frame.size
        let shapeRect = CGRect(x:0, y:0, width : frameSize.width, height: frameSize.height)
        
        borderLayer.bounds=shapeRect
        borderLayer.position = CGPoint( x: frameSize.width/2, y:frameSize.height/2)
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = color
        borderLayer.lineWidth = 1
        borderLayer.lineJoin=CAShapeLayerLineJoin.round
        borderLayer.lineDashPattern = NSArray(array: [NSNumber(value: 8),NSNumber(value:4)]) as? [NSNumber]
        
        let path = UIBezierPath.init(roundedRect: shapeRect, cornerRadius: 0)
        
        borderLayer.path = path.cgPath
        
        return borderLayer
        
    }
    
    func setUpBorder(width: CGFloat, color: UIColor, radius: CGFloat) {
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        layer.cornerRadius = radius
    }
    class func attributedText(text: String, font: UIFont, color: UIColor) -> NSMutableAttributedString {
        let attrString = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor: color,
                                                                              NSAttributedString.Key.font: font])
        return attrString
    }
    func createRadialGradient(center: CGPoint) {
        let gradLayer = RadialGradientLayer(center: center, radius: screenWidth + 100, colors: [lightBlue.cgColor, darkBlue.cgColor])
        gradLayer.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        layer.insertSublayer(gradLayer, at: 0)
        gradLayer.setNeedsDisplay()
    }
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    func rounded() {
        self.layer.cornerRadius = self.bounds.size.width / 2.0
        self.clipsToBounds = true
    }
}

extension UIView {
    /**
     Rotate a view by specified degrees

     - parameter angle: angle in degrees
     */
    func rotate(angle: CGFloat) {
        let radians = angle / 180.0 * CGFloat.pi
        let rotation = self.transform.rotated(by: radians)
        self.transform = rotation
    }

}
extension UIView {
    func anchor(top: NSLayoutYAxisAnchor?, paddingTop: CGFloat, bottom: NSLayoutYAxisAnchor?, paddingBottom: CGFloat, left: NSLayoutXAxisAnchor?, paddingLeft: CGFloat, right: NSLayoutXAxisAnchor?, paddingRight: CGFloat, width: CGFloat, height: CGFloat) {
        
        translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
}
public extension UIView {
    
    class func instantiateFromNib<T: UIView>(viewType: T.Type) -> T {
        return Bundle.main.loadNibNamed(String(describing: viewType), owner: nil, options: nil)?.first as! T
    }
    
    class func instantiateFromNib() -> Self {
        return instantiateFromNib(viewType: self)
    }
    
}
