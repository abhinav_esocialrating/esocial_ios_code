//
//  UIViewControllerExt.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 07/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import SwiftyJSON
import FirebaseAnalytics
import FAPanels
import AVKit
import Alamofire

fileprivate var hudKey: UInt8 = 0

extension UIViewController {
    func alertCheckMainThread(message: String) {
        if Thread.isMainThread {
            let alert = UIAlertController(title: "GoodSpace", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "GoodSpace", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    func alert(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "GoodSpace", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func alertWithAction(message: String, handler: @escaping () -> Void) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "GoodSpace", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                handler()
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func alertWithTwoAction(message: String, okHandler: @escaping () -> Void, cancelHandler: @escaping () -> Void) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "GoodSpace", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                okHandler()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                cancelHandler()
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func alertWithTwoActionCustomTitles(message: String, okTitle: String, cancelTitle: String, okHandler: @escaping () -> Void, cancelHandler: @escaping () -> Void) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "GoodSpace", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: okTitle, style: .default, handler: { action in
                okHandler()
            }))
            alert.addAction(UIAlertAction(title: cancelTitle, style: .default, handler: { action in
                cancelHandler()
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func remove() {
        self.children.forEach {
            $0.didMove(toParent: nil)
            $0.view.removeFromSuperview()
            $0.removeFromParent()
        }
    }
    
    // 
    fileprivate var hud: MBProgressHUD?{
        get{
            return objc_getAssociatedObject(self, &hudKey) as? MBProgressHUD
        }
        set{
            objc_setAssociatedObject(self, &hudKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            //            newValue?.contentColor = Appearance.greenColor
            newValue?.bezelView.style = .solidColor
            //            newValue?.bezelView.backgroundColor = ThemeManager.isNightMode() ? Appearance.backgroundViewNight : .white
            newValue?.backgroundColor = .clear
        }
    }
    func showHud(_ message: String = ""){
        self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    func showHudToWindow(_ message: String = "") {
        self.hud = MBProgressHUD.showAdded(to: self.view.window?.rootViewController?.view ?? self.view, animated: true)
    }
    
    func hideHud(){
        DispatchQueue.main.async {
            self.hud?.hide(animated: true)
        }
    }
    
    func openShare() {
        let text = Constants.socialInvite.rawValue + UserDefaults.standard.TZInviteLink

        // set up activity view controller
       let textToShare = [ text ]
       let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
       activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

       // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]
        
        activityViewController.completionWithItemsHandler = { activity, success, items, error in
            if activity != nil {
                Analytics.logEvent(AnalyticsEventShare, parameters: [
                    AnalyticsParameterContentType: "text",
                    AnalyticsParameterItemID: "share"
                ])
            }
        }
        
       self.present(activityViewController, animated: true, completion: nil)
    }
}
// MARK: - SendData object
extension UIViewController {
    func addPointsObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(pointsChanged), name: Notification.Name.CNContactStoreDidChange, object: nil)
    }
    @objc func pointsChanged(notification: Notification) {
        
    }
}


// MARK: - Application specific
extension UIViewController {
    func openNotificationFromNavBar() {
        let vc = NotificationsViewController.instantiate(fromAppStoryboard: .Main)
        let nvc = NotificationNavigationController(rootViewController: vc)
        nvc.isNavigationBarHidden = true
        nvc.modalPresentationStyle = .fullScreen
        present(nvc, animated: true, completion: nil)
    }
    func openPointsScreenFromNavBar() {
        let vc = PointsViewController.instantiate(fromAppStoryboard: .Main)
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    func addNavBar(toView: UIView) {
        toView.addSubview(NavBar.instantiateFromNib())
    }
    func openChatFromNavBar() {
        let vc = ChatNavigationController.instantiate(fromAppStoryboard: .Chat)
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    func openInfoFromNavBar(info: String) {
        let vc = InfoViewController.instantiate(fromAppStoryboard: .MainExt)
        vc.info = info
        vc.removeInfo = {
            self.removeInfoView()
        }
        addChild(vc)
        vc.view.frame = view.frame
        view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    func openVideoPlayer() {
        let vc = VideoViewController.instantiate(fromAppStoryboard: .MainExt)
        let videoURL = URL(string: "https://trustzedev.s3.ap-south-1.amazonaws.com/PromotionalVideos/GOOD-SPACE.mp4")
        let player = AVPlayer(url: videoURL!)
        vc.player = player
        self.present(vc, animated: true) {
            vc.player!.play()
        }
    }
    func openWelcomeScreen() {
        let vc = WekcomeViewController.instantiate(fromAppStoryboard: .Main)
        navigationController?.pushViewController(vc, animated: true)
    }
    func openSocialSignIn() {
        let vc = SocialLoginViewController.instantiate(fromAppStoryboard: .MainExt)
        present(vc, animated: true, completion: nil)
    }
    func removeInfoView() {
        children.forEach { (i) in
            if i is InfoViewController {
                i.willMove(toParent: nil)
                i.view.removeFromSuperview()
                i.removeFromParent()
            }
        }
    }
    func showScoreFromScoreUpdateNotification() {
        if let panelVC = appDelegate?.window?.rootViewController as? FAPanelController, let tbc = panelVC.center as? TabBarController {
            tbc.selectedIndex = 1
        }
    }
    func openNetworkVC(screenType: Int, title: String) {
        let vc = NetworkViewController.instantiate(fromAppStoryboard: .ScoreCard)
        vc.screenType = screenType
        vc.titleText = title
        present(vc, animated: true, completion: nil)
    }
    func compareNumeric(current: String, store: String) -> ComparisonResult {
        return current.compare(store, options: .numeric)
    }
    func openActivelyLooking() {
        let vc = ActivelyLookingViewController.instantiate(fromAppStoryboard: .MainExt)
//        vc.subscriber = { [weak self] in
//            self?.remove()
//        }
        vc.modalPresentationStyle = .fullScreen
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: false, completion: nil)
//        add(vc)
    }
    func redeemPoints(number: String, completion: @escaping (_ success: Bool) -> Void) {  // 1: Points  2: Notifications
        showHud()
        let params = ["code":"POINTS_REDEEM_500", "number":number]
        NetworkManager.redeemPoints(parameters: params, method: .post) { (response) in
            self.hideHud()
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint("redeemPoints")
            AppDebug.shared.consolePrint(json)
            switch response.response?.statusCode {
            case 200:
                self.showAutoToast(message: json["message"].stringValue)
                completion(true)
            default:
                self.alert(message: json["message"].stringValue)
                completion(false)
            }
        }
    }
    func openOtherUserProfile(userId: Int) {
        let vc = ProfileDetailsViewController.instantiate(fromAppStoryboard: .ProfileV2)
        vc.otherUserId = userId
        navigationController?.pushViewController(vc, animated: true)
    }
    func openTermsPage(type: Int) {
        let vc = PolicyViewController.instantiate(fromAppStoryboard: .Main)
        vc.modalPresentationStyle = .fullScreen
        vc.type = type
        present(vc, animated: true, completion: nil)
    }
    // MARK: - Actively looking / Search status
    class func changeSearchStatus(parameters: Parameters, method: HTTPMethod, completion: @escaping (_ success: Bool) -> Void) {  // 1: Points  2: Notifications
        NetworkManager.changeSearchStatus(parameters: parameters, method: method) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            AppDebug.shared.consolePrint("changeSearchStatus")
            AppDebug.shared.consolePrint(json)
            let data = json["data"].dictionaryObject
            if method == .get {
                UserDefaults.standard.SearchStatus = ["friendStatus": data?["actively_friends"] as? Int ?? 0, "professionalStatus": data?["actively_professionals"] as? Int ?? 0]
            } else {
                UserDefaults.standard.SearchStatus = ["friendStatus": parameters["friendStatus"] as? Int ?? 0, "professionalStatus": parameters["professionalStatus"] as? Int ?? 0]
            }
            switch response.response?.statusCode {
            case 200:
                completion(true)
            default:
                completion(false)
            }
        }
    }
}
extension UIViewController {
    
    func showAutoToast(message : String) {
        let toastView = UIView()
        toastView.alpha = 1.0
        toastView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastView.layer.cornerRadius = 10
        toastView.clipsToBounds  =  true
        view.addSubview(toastView)
        toastView.anchor(top: nil, paddingTop: 0, bottom: view.bottomAnchor, paddingBottom: 120, left: view.leftAnchor, paddingLeft: 30, right: view.rightAnchor, paddingRight: 30, width: 0, height: 0)
        
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
        toastView.addSubview(label)
        label.anchor(top: label.superview?.topAnchor, paddingTop: 8, bottom: label.superview?.bottomAnchor, paddingBottom: 8, left: label.superview?.leftAnchor, paddingLeft: 8, right: label.superview?.rightAnchor, paddingRight: 8, width: 0, height: 0)
        label.text = message
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations: {
                 toastView.alpha = 0.0
            }, completion: {(isCompleted) in
                toastView.removeFromSuperview()
            })
        }
    }
    func showAutoToastOnWindowRoot(message : String) {
        let toastView = UIView()
        toastView.alpha = 1.0
        toastView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastView.layer.cornerRadius = 10
        toastView.clipsToBounds  =  true
        let rootView = appDelegate?.window?.rootViewController?.view
        rootView?.addSubview(toastView)
//        view.addSubview(toastView)
        toastView.anchor(top: nil, paddingTop: 0, bottom: rootView?.bottomAnchor, paddingBottom: 120, left: rootView?.leftAnchor, paddingLeft: 30, right: rootView?.rightAnchor, paddingRight: 30, width: 0, height: 0)
        
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.fontWith(name: .Roboto, face: .Regular, size: 15)
        toastView.addSubview(label)
        label.anchor(top: label.superview?.topAnchor, paddingTop: 8, bottom: label.superview?.bottomAnchor, paddingBottom: 8, left: label.superview?.leftAnchor, paddingLeft: 8, right: label.superview?.rightAnchor, paddingRight: 8, width: 0, height: 0)
        label.text = message
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations: {
                 toastView.alpha = 0.0
            }, completion: {(isCompleted) in
                toastView.removeFromSuperview()
            })
        }
    }
}
extension UIViewController {
    func testBadge() {
        NotificationCenter.default.post(name: Notification.Name.PushReceived, object: self)
        if let tabItems = tabBarController?.tabBar.items {
            let tabItem = tabItems[2]
            tabItem.badgeColor = UIColor.red
            tabItem.badgeValue = ""
        }
    }
    /**
            type: 1:  Sign up
            type: 2:  Side menu
     */
    func resyncContact(type: Int) {
        PhoneContacts.requestAccess { (granted) in
            if granted {
                self.alert(message: type == 1 ? Constants.ContactSync.rawValue : Constants.ContactSyncFromMenu.rawValue)
                DispatchQueue.global(qos: .background).async {
                    UserContacts.sharedInstance.loadContactsForced()
                }
            } else {
                self.alertWithTwoAction(message: "Open permissions", okHandler: {
                    let url = URL(string: UIApplication.openSettingsURLString)!
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }) {}
            }
        }
    }
}
