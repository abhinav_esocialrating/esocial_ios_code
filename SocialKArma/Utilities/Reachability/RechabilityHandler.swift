//
//  RechabilityHandler.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 31/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation

class RechabilityHandler: NSObject {
    let reachability = try! Reachability()
    
    static let shared = RechabilityHandler()
    override init(){
        super.init()
        do{
          try reachability.startNotifier()
        }catch{
          print("could not start reachability notifier")
        }
    }
    
    
}
