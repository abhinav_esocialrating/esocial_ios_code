//
//  ImagePicker.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 23/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import TOCropViewController

protocol ImagePickerUploaderDelegate {
    func PickerSuccess(withImage: UIImage)
    func PickerFailed(withError: String)
    func profilePicRemoved(url: String!)
}
extension ImagePickerUploaderDelegate {
    func PickerSuccess(withImage: UIImage, url: URL!) {}
    func profilePicRemoved(url: String!) {}
}

class ImagePicker: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var sourceController = UIViewController()
    var pickerController = UIImagePickerController()
    var selectedImageUrl : NSURL?
    var selectedImage : UIImage?
    var image_uploaded : Bool?
    var imageUrl : String?
    var uploadPath = ""
    var delegate : ImagePickerUploaderDelegate?
    var isCropping = true
    
    func pickImage(sourceController : UIViewController, path : String) -> UIAlertController {
        
        self.sourceController = sourceController
        uploadPath = path
        
        let attributedString = Utilities.attributedText(text: "Add a profile photo\n(This photo will be public)", font: UIFont.fontWith(name: .Roboto, face: .Regular, size: 14), color: UIColor.NewTheme.darkGray)
        let alertView = UIAlertController(title: "", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        alertView.setValue(attributedString, forKey: "attributedTitle")
        alertView.addAction(UIAlertAction(title: "Take Photo", style: UIAlertAction.Style.default, handler: { (action) in
            // Show Camera Picker
            self.showPicker(source: false)
            alertView.dismiss(animated: true, completion: nil)
        }))
        alertView.addAction(UIAlertAction(title: "Choose from Library", style: UIAlertAction.Style.default, handler: { (action) in
            // Show Gallery Picker
            self.showPicker(source: true)
            alertView.dismiss(animated: true, completion: nil)
        }))
//        if selectedImageUrl != nil {
        alertView.addAction(UIAlertAction(title: "Remove", style: UIAlertAction.Style.destructive, handler: { (action) in
            self.removeProfilePic { (success, json, message) in
                self.delegate?.profilePicRemoved(url: json["data"].stringValue)
            }
            alertView.dismiss(animated: true, completion: nil)
        }))
//        }
        
        alertView.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) in
            // Hide Alert
            alertView.dismiss(animated: true, completion: nil)
            self.delegate?.PickerFailed(withError: "Cancel")
        }))
        
        return alertView
    }
    
    func showPicker(source : Bool) {
        pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = false
        
        if source {
            pickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
            sourceController.present(pickerController, animated: true, completion: nil)
        } else {
            if UIImagePickerController.isSourceTypeAvailable( UIImagePickerController.SourceType.camera) {
                pickerController.sourceType = UIImagePickerController.SourceType.camera
                sourceController.present(pickerController, animated: true, completion: nil)
            }
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: false, completion: nil)
//        picker.dismiss(animated: false) {
        DispatchQueue.main.async {
            self.selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            if let image = self.selectedImage {
                if let resizedImage = resizeImage(image:image, targetSize: CGSize(width: 500.0, height: 500.0)){
                    self.selectedImage = resizedImage
                }
            }
            if self.isCropping {
                self.presentCropViewController(image: self.selectedImage!)
            } else {
                self.delegate?.PickerSuccess(withImage: self.selectedImage!)
            }
        }
//        }
    }
        
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.delegate?.PickerFailed(withError: "Cancel")
    }
    
    
}
extension ImagePicker: TOCropViewControllerDelegate {
    func presentCropViewController(image: UIImage) {
        let cropViewController = TOCropViewController(image: image)
        cropViewController.delegate = self
        sourceController.present(cropViewController, animated: true, completion: nil)
    }
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: true, completion: nil)
        self.delegate?.PickerSuccess(withImage: image)
    }
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        cropViewController.dismiss(animated: true, completion: nil)
        self.delegate?.PickerFailed(withError: "Cancel")
    }
}
func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage! {
    let size = image.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}
// MARK: - generate url
extension ImagePicker {
    func startUploadImage()
    {
        let uuid = UUID().uuidString
        let remoteName = "\(uuid).jpeg"
        do{
            if let imageFile = try generateImageUrl(fileName: remoteName) as URL?{
                
                self.delegate?.PickerSuccess(withImage: self.selectedImage!, url: imageFile)
            }
            
        }catch {
            self.delegate?.PickerFailed(withError: "error in creating temp file")
            print("error in creating temp file")
        }
        
    }
    
    func generateImageUrl(fileName: String) throws -> NSURL {
        let fileURL = NSURL(fileURLWithPath: NSTemporaryDirectory().appendingFormat(fileName))
        let data = selectedImage?.jpegData(compressionQuality: 1.0)// UIImageJPEGRepresentation(selectedImage!, 1)
        try data?.write(to: fileURL as URL)
        return fileURL
    }
    
}
// MARK: - Web Services
extension ImagePicker {
    func uploadProfilePic(image: UIImage, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.uploadProfilePic(parameters: ["file": image], method: .post) { (response) in
            
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json, "")
            default:
//                let message = json["message"].stringValue
//                self.alert(message: message)
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
    func removeProfilePic(completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        NetworkManager.removeProfilePic(parameters: [:], method: .delete) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            switch response.response?.statusCode {
            case 200:
                completion(true, json, json["message"].stringValue)
            default:
//                let message = json["message"].stringValue
//                self.alert(message: message)
                let message = json["message"].stringValue
                completion(false, JSON.init(), message)
            }
        }
    }
}
