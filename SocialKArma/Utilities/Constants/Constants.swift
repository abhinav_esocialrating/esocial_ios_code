//
//  Constants.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import UIKit

let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height

let kShadowLayer = "ShadowLayer"

// Appdelegate
let appDelegate = UIApplication.shared.delegate as? AppDelegate
let UDID = UIDevice.current.identifierForVendor?.description


struct CommonData {
    static let experience = ["0-2", "2-5", "5-10", "10-15", "15-20", "20+"]
    static let bloodGroup = ["A+", "A-", "B+", "B-", "O+", "O-", "AB+", "AB-"]
    static let gender = ["Male", "Female"]
    static let genderAll = ["Male", "Female", "Both"]
    static let years = Array(1960...2020)
    static let endYear = Array(1960...2030)
    static let avatars = ["avatar-2", "avatar-3", "avatar-6", "avatar-7", "avatar-11", "avatar-16", "avatar-17", "avatar-18", "avatar-19", "avatar-28"]
    static let months = [1: "January", 2: "February", 3: "March", 4: "April", 5: "May", 6: "June", 7: "July", 8: "August", 9: "September", 10: "October", 11: "November", 12: "December"]
    static let calenderMonths = ["Jan": "01", "Feb": "02" , "Mar" : "03", "Apr": "04", "May": "05", "Jun": "06", "Jul": "07", "Aug": "08", "Sep": "09", "Oct": "10", "Nov": "11", "Dec": "12"]
    static let height = Array(100...250)
    static let weight = Array(30...300)
    static let countries = ["(+91)  India", "(+1)   USA"]
    static let countryCodes = ["+91", "+1"]
    static let emojiCodes: [Int: String] = [12: "👥", 1: "🤝", 7: "🧐", 6: "😇", 2: "👨🏻‍⚕", 10: "🤞", 11: "🙏", 9: "✴", 100: "🙋‍♀️", 101: "✅", 102: "😊", 103: "📜"]  // 100: dear(woman raising hand)  🙋‍♀️,   101: check mark button ✅,  102: 😊 smiling face with smiling eyes
    static let emojiCodess: [Int: String] = [12: "👥", 1: "U+1F91D", 7: "U+1F9D0", 6: "U+1F607", 2: "U+1F468", 10: "U+1F91E", 11: "U+1F64F", 9: "U00012734", 100: "U+1F64B", 101: "U+2705", 102: "U+1F60A", 103: "📜"]
    
    static let filterPlaceholderSize: CGFloat = 14

}

enum FilterTypes {
    case name, similarInterests, dating, serviceProvider, professional, freelancers, employees, others
}
let filterTypes = ["friends", "freelancers", "serviceProviders", "employees", "professionals", "others"]

enum CategoryFilterType: String {
    typealias RawValue = String
    
    case freelancers = "FR"
    case professionals = "PR"
    case serviceProviders = "SP"
    case all = "ALL"
}

// Alert messages
let kAlertUserAlreadyExists = "User already exists"
let kAlertAskForSignIn = "Account already exists. Sign in instead?"
let kAlertAskForMovingToSignIn = "Account already exists. Sign in instead?"
let kAlertUserNotExist = "User does not exist"
let kAlertAskForSignUp = "Account doesn't exist. Sign up instead?"
let kAlertLogout = "Are you sure you want to Logout?"

// Score Request Alert
let kAlertRequestScore = "If you wish to send score request to view the score, it will cost you 2 points. Please ensure you  have sufficient Coins available."
let kAlertRequestSendSuccess = "Request sent successfully"
let kAlertAlreadyRequested = "You already sent request!"
let kAlertAlreadyAccepted = "Your request is already sent accepted!"
let kAlertAlreadyRejected = "Your request is already sent rejected!"
let kAlertAlreadyIgnored = "Your can't send score request to this user!"
let kAlertSomeErrorOccured = "Some error occured!"

// Coach Marks
enum CoachMarksTypes {
    case None
    case InviteAndSearch
    case HighLow
    case SwipeUp
    case Points
}
enum SocialMediaTypes: String {
    typealias rawValue = String
    
    case facebook = "facebook_url"
    case linkedIn = "linkedin_url"
    case twitter = "twitter_url"
    case instagram = "instagram_url"
    
    case facebookPost = "facebookUrl"
    case linkedInPost = "linkedinUrl"
    case twitterPost = "twitterUrl"
    case instagramPost = "instagramUrl"
}
// Home Screen Cards
enum CardType: String {
    typealias RawValue = String
    
    case review = "REVIEW"
    case aux = "AUX"
    case web = "WEB"
}
enum CardSubtype: String {
    typealias RawValue = String
    
    case OVERALL = "OVERALL"
    case FEEDBACK = "FEEDBACK"
    case RELATIONSHIP = "RELATIONSHIP"
}
enum Rc_source: String {
    typealias rawValue = String
    
    case SELF = "SELF"
    case CONTACT = "CONTACT"
}
enum Card_Code: String {
    typealias rawValue = String
    
    case REWARD_SA = "REWARD_SA"
    case REWARD_REVIEW = "REWARD_REVIEW"
    case FIRST_CARD = "FIRST_CARD"
}
enum Card_Template: String {
    typealias rawValue = String
    
    case INFO = "INFO"
    case INFO_1 = "INFO_1"
    case INFO_4 = "INFO_4"
    case INFO_5 = "INFO_5"
    case REWARD = "REWARD"
    case BACKGROUND_IMAGE_ONLY = "BACKGROUND_IMAGE_ONLY"
    case AUX_INFO_1 = "AUX_INFO_1"
    case AUX_INFO_2 = "AUX_INFO_2"
    case AUX_INFO_3 = "AUX_INFO_3"
    case AUX_INFO_4 = "AUX_INFO_4"
    case AUX_INFO_5 = "AUX_INFO_5"
}
enum Card_Action: String {
    typealias rawValue = String
    
    case IGNORE = "IGNORE"
    case POSITIVE = "POSITIVE"
    case NEGATIVE = "NEGATIVE"
}
// Push Notification Handling
enum PushNotificationType: String {
    typealias rawValue = String
    
    case REQUEST_SCORE = "REQUEST_SCORE"
    case REQUEST_SCORE_REJECT = "REQUEST_SCORE_REJECT"
    case REVIEW = "REVIEW"
    case SCORE_UPDATE = "SCORE_UPDATE"
    case FEEDBACK_REQUEST = "FEEDBACK_REQUEST"
    
    case CHAT = "chat"
    
    case FEEDBACK_UE = "FEEDBACK_UE"
    case CONTACT_UE = "CONTACT_UE"
    case PROFILE_UE = "PROFILE_UE"
    case FEEDBACK_UE_2 = "FEEDBACK_UE_2"
    case INVITE_UE = "INVITE_UE"
    
    // To Feedback screen
    case UE_PERSONAL_NEWS = "UE_PERSONAL_NEWS"
    case UE_FEEDBACK_REMINDER_1 = "UE_FEEDBACK_REMINDER_1"
    case UE_FEEDBACK_REMINDER_2 = "UE_FEEDBACK_REMINDER_2"
    case UE_FEEDBACK_REMINDER_4 = "UE_FEEDBACK_REMINDER_4"
    case UE_FEEDBACK_REMINDER_5 = "UE_FEEDBACK_REMINDER_5"
    case UE_FEEDBACK_REMINDER_7 = "UE_FEEDBACK_REMINDER_7"

    // To Profile screen
    case UE_INFO_1 = "UE_INFO_1"
    case UE_PERSONAL_NEWS_2 = "UE_PERSONAL_NEWS_2"
    case UE_PROFILE_COMPLETION_2 = "UE_PROFILE_COMPLETION_2"
    case UE_PROFILE_COMPLETION_1 = "UE_PROFILE_COMPLETION_1"
    case CONTACT_JOINING = "CONTACT_JOINING"
    
    // ScoreCard - Invite Sent List
    case UE_REINVITE_1 = "UE_REINVITE_1"
    case UE_REINVITE_2 = "UE_REINVITE_2"
    
    // ScoreCard - Detailed feedback
    case UE_FEEDBACK_REMINDER_3 = "UE_FEEDBACK_REMINDER_3"
    case UE_FEEDBACK_REMINDER_6 = "UE_FEEDBACK_REMINDER_6"
    
    // Chat
    case CHAT_UNREAD_MESSAGES = "CHAT_UNREAD_MESSAGES"

    // Offers
    case COINS_TO_CASH = "COINS_TO_CASH"
    case OFFER_ACCEPT = "OFFER_ACCEPT"
    case OFFER_REJECT = "OFFER_REJECT"
    case OFFER_LIKE_NOTIFICATION = "OFFER_LIKE"
    case OFFER_COMMENT_ORGANIC = "OFFER_COMMENT_ORGANIC"
    case OFFER_COMMENT_NOTIFICATION = "OFFER_COMMENT"
}
enum NotificationHandleType: String {
    typealias rawValue = String
    
    case willPresent = "willPresent"
    case didReceiveResponse = "didReceiveResponse"
}
// MARK: - Constant Strings
enum Constants: String {
    typealias RawValue = String
    
    // API Keys
    case FirebaseAPIKey = "AIzaSyB8Nfm7CXC0LsnVqNd9cUypvmt3m_Qr0Jg"
    case FirebaseRemoteConfigVersionKey = "ios_version"
    case FirebaseFeedback1Key = "feedback_one"
    case FirebaseFeedback2Key = "feedback_two"
    case FirebaseFeedback3Key = "feeback_three"
    case FirebaseProfile1Key = "profile_one"
    case FirebaseProfile2Key = "profile_two"
    case FirebaseFind1Key = "find_one"
    case FirebaseFind2Key = "find_two"
    case FirebaseScore1Key = "score_one"
    case FirebaseScore2Key = "score_two"
    case FirebaseScore3Key = "score_three"
    
    // Demo Account for review
    static let DemoPhoneNumber = "9999999999"
    static let DemoOTP = "1010"
    static let OTPAlreadyFilled = "Demo account found! Test OTP has already been filled for you."
    static let DemoAccountAPICall = "Multiple API calls are not allowed with the demo account!"

    // Invite
    case socialInvite = "Hey, I am a member of GoodSpace - The Good People's club. I think you are one of the good people in my life and I would like to invite you to become a member of the GoodSpace and give me feedback. Download Now: "
    case whatsappInvite = "I am a member of GoodSpace - The Good People's club. I think you are one of the good people in my life and I would like to invite you to become a member of the GoodSpace and give me feedback \n"
    case shareReview1 = "I think you are one of the good people in my life. I would like to share my feedback about you. You can see it below:"
    case shareReview2 = "And I would be really happy to receive your feedback. "
    
    // Sign up process
    case kAlertInvalidEmail = "Please enter a valid email"
    case kAlertSelectLookingFor = "Please select what you are looking for."
    case kAlertSelectFriendType = "Please select Gender of friends you are interested in."
    
    // Permissions
    case kAlertAskForContactPermission = "GoodSpace uses your contact list to show you people who you can review and without this permission, this app will not function. The contact list is uploaded to our servers for recommending relevant people for reviewing."
    case kAlertContactPermissionDenied = "Without this permission, the app won't function as it is cricital for showing the people who you can review. Please enable it through phone settings."
    case kAlertAskForLocationPermission = "Your location is important for users to find you through search"
    case kAlertLocationPermissionDenied = "Don't you want to be found during search? Please enable permission through phone settings so that you are not left out in search."
    
    // Score screen
    case ScoreInfoSelf = "My self assessment"
    case ScoreInfoOther = "User's self assessment"
    
    // Report abuse
    case ReportUser = "kldskdsl"
    
    // Review Cards
    case ReviewedSuccessfully = "Review submitted successfully"
    
    case ContactSync = "You are almost ready to give your feedback! Please wait while your contacts are syncing"
    case ContactSyncFromMenu = "Please wait while we are resyncing your contacts. This may take few minutes"
    
    // Screen Info
    case FeedbackInfo = "Everyone has some Goodness - Appreciate the Good People in your life."
    case ScoreInfo = "Your Goodness Page  - find supporting scores for Gmedals, Gcoins and much more here!"
    case SearchInfo = "Find good people - Everyone needs to find good people in their lives – this is the place you can find them."
    case ProfileInfo = "Become findable - Make your profile powerful by filling all details. Your GoodSpace profile is publicly visible. We highly recommend that you DO NOT share any sensitive personal information."
    case OffersInfo = "Goodies for you - Offers will appear in this space when your goodness score and number of feedback exceed a threshold value."
    
    // Placeholders in filter screen
    case InterestsPlaceholder = "Swimming, Painting"
    case CategoryPlaceholder = "UI/UX, Copywriting, SEO"
    case SkillsPlaceholder = "Python, Java, C++"
    case ExperiencePlaceholder = "0-2 years, 2-5 years"
    case IndustryPlaceholder = "Consumer Goods, IT"
    case EducationPlaceholder = "Diploma, Graduate"
    case EducationFieldPlaceholder = "Filter people according to the field they studied in"
    case fitnessPlaceholder = "Find people with similar fitness regimens"

}

enum Errors: String {
    typealias RawValue = String

// API Keys
    case LocationAPIError = "Places API failed to get predictions!"
    
    // Card errors
    case TargetUserIdNull = "There is some error with this card. Please swipe this card to ignore and review other users."

    // Network connectivity issues
    case NetworkNotAvailable = "Please connnect to Internet!"
    case NetworkNotAvailableSearch = "Oh no! We can't seem to find your internet connection! Please reconnect your WiFi or Data Plan to resume browsing on GoodSpace. "
}
