//
//  Utilities.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 23/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class Utilities: NSObject {
    class func attributedText(text: String, font: UIFont, color: UIColor) -> NSMutableAttributedString {
        let attrString = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor: color,
                                                                              NSAttributedString.Key.font: font])
        return attrString
    }
    func getExperience(index: Int) -> [Int] {
        var arr = [Int]()
        switch index {
        case 0:
            arr.append(0)
            arr.append(2)
        case 1:
            arr.append(2)
            arr.append(5)
        case 2:
            arr.append(5)
            arr.append(10)
        case 3:
            arr.append(10)
            arr.append(15)
        case 4:
            arr.append(15) //= 15
            arr.append(20) //= 20
        default:
            arr.append(20) //= 20
            arr.append(50) //= 50
        }
        return arr
    }
    /* number: Phone number,
        type: 1: social  2: whatsapp
        name: Invitee's name
    */
    class func inviteUsingWhatsapp(number: String, type: Int, name: String) {
        if UserDefaults.standard.TZInviteLink != "" {
            var inviteUrl = ""
            if type == 1 {
                inviteUrl = Constants.socialInvite.rawValue + UserDefaults.standard.TZInviteLink
            } else {
                inviteUrl = "Dear \(name), " + Constants.whatsappInvite.rawValue + UserDefaults.standard.TZInviteLink
            }
            var components = URLComponents()
            components.scheme = "whatsapp"
            components.host = "send"
            components.path = ""
            let query = URLQueryItem(name: "text", value: inviteUrl)
            var fone = number
            if fone.count > 0 && fone.first == "+" {
                fone.removeFirst()
            }
            let phone = URLQueryItem(name: "phone", value: fone)
            components.queryItems = number == "" ? [query] : [query, phone]
            let dfhk = components.url
            UIApplication.shared.open(dfhk!, options: [:], completionHandler: nil)
        }
    }
    class func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    func updateProfilePicUrl(url: String) {
        var userInfo = UserDefaults.standard.userInfo
        let data = userInfo.data(using: .utf8)
        var json = JSON(data ?? Data())
        var dict = json.dictionaryObject
        dict?["image_id"] = url
        json = JSON(dict ?? [:])
        userInfo = json.rawString() ?? ""
    //    print(userInfo)
        UserDefaults.standard.userInfo = userInfo
        NotificationCenter.default.post(name: Notification.Name.AppNotifications.ProfilePicChanged, object: nil)
    }
    func updateUserName(name: String) {
        var userInfo = UserDefaults.standard.userInfo
        let data = userInfo.data(using: .utf8)
        var json = JSON(data ?? Data())
        var dict = json.dictionaryObject
        dict?["name"] = name
        json = JSON(dict ?? [:])
        userInfo = json.rawString() ?? ""
    //    print(userInfo)
        UserDefaults.standard.userInfo = userInfo
        NotificationCenter.default.post(name: Notification.Name.AppNotifications.NameChanged, object: nil, userInfo: ["name": name])
//        NotificationCenter.default.post(name: Notification.Name.AppNotifications.NameChanged, object: nil)
    }
    class func chatDate(date: String) -> String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(abbreviation: "UTC")
        formatter.locale = Locale.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let datee = formatter.date(from: date)
        
        let dtFormatter = DateFormatter()
        dtFormatter.timeZone = TimeZone.current
        dtFormatter.dateFormat = "dd/MM/yyyy  hh:mm a"
        if let datee = datee {
            let str = dtFormatter.string(from: datee)
            return str
        }
        return ""
    }
    class func chatDateCreation() -> String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(abbreviation: "UTC")
        formatter.locale = Locale.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let str = formatter.string(from: Date())
        return str
    }
    class func dateFromString(dateString dte: String) -> (Date,String) {
        let formatter1 = DateFormatter()
        formatter1.timeZone = TimeZone.init(abbreviation: "UTC")
        formatter1.locale = Locale.current
        formatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let datee = formatter1.date(from: dte)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let str = formatter.string(from: datee ?? Date())
        let date = formatter.date(from: str)
        return (date ?? Date(), str)
    }
    func shouldUpdateLocationInServer() -> Bool {
        let currentTime = Date().timeIntervalSince1970
        if UserDefaults.standard.LocationUpdateTime == 0 {
            UserDefaults.standard.LocationUpdateTime = currentTime
            return true
        } else {
            let diff = currentTime - UserDefaults.standard.LocationUpdateTime
            if diff >= 60*30 {
                UserDefaults.standard.LocationUpdateTime = currentTime
                return true
            }
        }
        return false
    }
    class func getCountryList() {
        var listOfCountries: [String] = []
         
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_US").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            listOfCountries.append(name)
        }
         
        print(listOfCountries)
    }
    class func inviteContactMessage(phoneNumber: String) {
        let url = Constants.socialInvite.rawValue + UserDefaults.standard.TZInviteLink
        var components = URLComponents()
        components.scheme = "whatsapp"
        components.host = "send"
        components.path = ""
        let query = URLQueryItem(name: "text", value: url)
        var fone = phoneNumber
        fone = String(fone.filter { !" \n\t\r".contains($0) })
        if fone.count > 0 && fone.first == "+" {
            fone.removeFirst()
        }
        let phone = URLQueryItem(name: "phone", value: fone)
        components.queryItems = [query, phone]
        let dfhk = components.url
        UIApplication.shared.open(dfhk!, options: [:], completionHandler: nil)
    }
    func days(from date: Date, to: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: to).year ?? 0
    }

}
