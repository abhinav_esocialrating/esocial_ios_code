//
//  UnusedCode.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/10/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation

/*  UserDetailsViewController
fileprivate func setDatePickerForTextfield(textField: UITextField) {
    let datePicker = UIDatePicker()
    datePicker.setDate(Date(), animated: true)
    datePicker.datePickerMode = .date
    datePicker.addTarget(self, action: #selector(datePicked(_:)), for: .valueChanged)
    textField.inputView = datePicker
} */

/* Fetch Contacts
 var allContacts = [PhoneContact]()
 for contact in PhoneContacts.getContacts(filter: filter) {
     allContacts.append(PhoneContact(contact: contact))
 }
 var filterdArray = [PhoneContact]()
 if self.filter == .mail {
     filterdArray = allContacts.filter({ $0.email.count > 0 }) // getting all email
 } else if self.filter == .message {
     filterdArray = allContacts.filter({ $0.phoneNumber.count > 0 })
 } else {
     filterdArray = allContacts
 }

 */

/* search results
 func openFriendsOptions() {
     let vc = FriendsFilterOptionsVC.instantiate(fromAppStoryboard: .Search)
     vc.modalPresentationStyle = .overCurrentContext
     vc.sendData = self
     vc.modalPresentationStyle = .overFullScreen
     present(vc, animated: false, completion: nil)
 }
 func openFilter<U : UIViewController>(viewControllerClass : U.Type) {
     let nvc = UINavigationController.init()
     let vc = U.instantiate(fromAppStoryboard: .Search)
     nvc.setViewControllers([vc], animated: false)
     nvc.isNavigationBarHidden = true
     present(nvc, animated: true, completion: nil)
 }
 
 @IBAction func filterTapped(_ sender: Any) {
     switch currentFilter {
     case 100:
         openFilter(viewControllerClass: NameFilterViewController.self)
     case 4:
         openFilter(viewControllerClass: EmployeeFilterVC.self)
     case 5:
         openFilter(viewControllerClass: OthersFilterVC.self)
     default:
         if friendsFilter == 1 {
             openFilter(viewControllerClass: SimilarInterestsFilterVC.self)
         } else {
             openFilter(viewControllerClass: DatingFilterVC.self)
         }
     }
 }
 func openFriendsOptions() {
     let vc = FriendsFilterOptionsVC.instantiate(fromAppStoryboard: .Search)
     vc.modalPresentationStyle = .overCurrentContext
     vc.sendData = self
     vc.modalPresentationStyle = .overFullScreen
     present(vc, animated: false, completion: nil)
 }
 @IBAction func suggestionsTapped(_ sender: Any) {
     switch (sender as! UIButton).tag {
     case 100:
         openFriendsOptions()
     default:
         print("")
     }
 }

 */

/*  HomeViewController
 viewModel.loadContacts {
     DispatchQueue.main.async {
         self.kolodaView.reloadData()
         self.hideHud()
     }
 }
 
 
 
 // HomeViewModel
 func loadContacts(completion: completionBlock) {
        phoneContacts.removeAll()
        var allContacts = [PhoneContact]()
        for contact in PhoneContacts.getContacts(filter: filter) {
            allContacts.append(PhoneContact(contact: contact))
        }
        
        var filterdArray = [PhoneContact]()
        if self.filter == .mail {
            filterdArray = allContacts.filter({ $0.email.count > 0 }) // getting all email
        } else if self.filter == .message {
            filterdArray = allContacts.filter({ $0.phoneNumber.count > 0 })
        } else {
            filterdArray = allContacts
        }
        phoneContacts.append(contentsOf: filterdArray)
        loadSliderAndRatingValues()
        completion()
    }
 
 func loadSliderAndRatingValues() {
     let dict = [1:1,2:1,3:1,4:1]
     sliderValues = Array(repeating: dict, count: phoneContacts.count)
     ratings = Array(repeating: 0, count: phoneContacts.count)
 }
 
 func getUser(parameters: [String:Any], completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
         
         NetworkManager.getUser(parameters: parameters, method: .get) { (response) in
             let json = JSON(response.result.value ?? JSON.init())
 //            print(json)
             switch response.response?.statusCode {
             case 200:
                 completion(true, json, "")
             default:
                 let message = json["message"].stringValue
                 completion(false, JSON.init(), message)
             }
         }
     }
 */


/*  Contact Rating Cell
 func updateAllSliderValues(rating: Double) {
     let sliders = [slider1, slider2, slider3, slider4, slider5, slider6, slider7, slider8]
     let intValue = Int(rating)
     for (i,slider) in sliders.enumerated() {
         viewModel.sliderValues[indexPath.row][i+1] = intValue
         slider?.selectedMaximum = Float(intValue)
     }
 }

 setAverageRating(floatRating: sliderValue)

 func setAverageRating(floatRating: Float) {
     var total:Double = 0
     for i in viewModel.sliderValues[indexPath.row] {
         total += Double(i.value)
     }
     let average:Double = Double(total / 4)
     viewModel.ratings[indexPath.row] = average//Int(round(average))
 }

 */
/*
 if viewModel.expandedCells.contains(indexPath.row) {
     viewModel.expandedCells = viewModel.expandedCells.filter({$0 != indexPath.row})
 } else {
     viewModel.expandedCells.append(indexPath.row)
 }
 tableView.reloadData()
 */
/*  Creating radial gradient for full screen
 let center = CGPoint(x: screenWidth/2, y: (screenHeight/2)-100)
 let layer = RadialGradientLayer(center: center, radius: screenWidth + 200, colors: [lightBlue.cgColor, darkBlue.cgColor])
 layer.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
 view.layer.insertSublayer(layer, at: 0)
 //        view.layer.insertSublayer(layer, below: view.layer)
 layer.setNeedsDisplay()
 */

/*
extension ChannelListViewController {
    func sendPushNotification(notData: [String: Any]) {
        let url = URL(string: "https://fcm.googleapis.com/fcm/send")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AIzaSyA-e8ZVSU5QcG6LEHkyaV6uW5cDnLb9ruA", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"

        request.httpBody = try? JSONSerialization.data(withJSONObject: notData, options: [])
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error ?? "")
                return
            }

            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print(response ?? "")
            }

            let responseString = String(data: data, encoding: .utf8)
            print(responseString ?? "")
        }
        task.resume()
    }
    /*
    let notifMessage: [String: Any] = [
        "to" : "dTqvBPQHqko:APA91bEbjJkW-fSTok4QQgUWqsT4tI3_hirWRrvaf5sL-avVLCJ_RGrxDsyRFfKW8zvS5tthBBl8z3mDpzTVJtUybTkGx2fxru30DQZdKXf0eVrQ4dqpN-7pLif3yaTSRQHlNuSqshsH",
        "notification" :
            ["title" : "title you want to display", "body": "content you need to display", "badge" : 1, "sound" : "default"]
    ]
    sendPushNotification(notData: notifMessage)
    */
}
*/
/*
//        if let reviewers = searchResult?.reviews {
//            reviewersLabel.text = String(format: "Feedback: %d", reviewers)
//        } else {
//            reviewersLabel.text = "Feedback: 0"
//        }

//        if let medals = searchResult?.medals, medals.count > 0 {
////            medalStack.isHidden = false
//            for (_,j) in medals.enumerated() {
//                if let image_url = j.image_url, image_url != "" {
//
//                    let img = UIImageView()
//                    medalStack.addArrangedSubview(img)
//                    img.anchor(top: nil, paddingTop: 0, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, width: 20, height: 20)
//                    img.sd_setImage(with: URL(string: image_url), completed: nil)
//                }
//            }
//        } else {
////            medalStack.isHidden = true
//        }
*/
