//
//  AppFonts.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 12/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    public enum FontFamily {
        case Roboto
    }
    
    public enum FontFace {
        case Light
        case Bold
        case Thin
        case Regular
        case Extrabld
        case Black
        case Semibold
        case BoldItalic
        case LightItalic
        case MediumItalic
        case Medium
        case BlackItalic
        case Italic
        case ThinItalic
    }
    
    open class func fontWith(name: FontFamily, face: FontFace, size: CGFloat) -> UIFont {
        return UIFont.init(name: "\(name)-\(face)", size: size)!
    }
    
}
