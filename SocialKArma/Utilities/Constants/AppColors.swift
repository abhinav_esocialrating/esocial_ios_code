//
//  AppColors.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 06/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation
import UIKit

// Radial background
let darkBlue = UIColor(red: 0/255, green: 107/255, blue: 180/255, alpha: 1.0) //006bb4
let lightBlue = UIColor(red: 0/255, green: 160/255, blue: 255/255, alpha: 1.0) //00a0ff

let grayBorder = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0)

let defaultBlue = UIColor(red: 0/255, green: 122/255, blue: 1, alpha: 1.0)

let yellow = UIColor(red: 239/255, green: 193/255, blue: 0, alpha: 1.0)// (239,193,0)
let appYellow = UIColor(red: 254/255, green: 235/255, blue: 0, alpha: 1.0)// feeb00

let appGray = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1.0)

let background = UIColor(red: 0/255, green: 107/255, blue: 180/255, alpha: 1.0)

// New card design 21 oct 2019
let cardBlue = UIColor(red: 75/255, green: 152/255, blue: 211/255, alpha: 1.0) // 4b98d3
let lightGray = UIColor(red: 128/255, green: 130/255, blue: 133/255, alpha: 1.0) //(128,130,133) 808285
let darkGray = UIColor(red: 88/255, green: 89/255, blue: 91/255, alpha: 1.0) //(88,89,91)  58595b
let bkgGray = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0)  // f4f4f4


extension UIColor {
    enum NewTheme {
        static var lightBlue: UIColor  { return UIColor(red: 62/255, green: 136/255, blue: 236/255, alpha: 1) }  //3e88ec//(62,136,236)
        static var paleBlue: UIColor { return UIColor(red: 106/255, green: 147/255, blue: 206/255, alpha: 1) }  //6A93CE//(106,147,206)
        static var lightGray: UIColor  { return UIColor(red: 108/255, green: 130/255, blue: 133/255, alpha: 1) } //808285//(128,130,133)
        static var darkGray: UIColor { return UIColor(red: 88/255, green: 89/255, blue: 91/255, alpha: 1) }  //58595b//(88,89,91)
        static var background: UIColor  { return UIColor(red: 250/255, green: 249/255, blue: 248/255, alpha: 1) }  //faf9f8//(250,249,248)
        static var card: UIColor { return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) } //FFFFFF//(255,255,255)
        static var blockedRed: UIColor  { return UIColor(red: 222/255, green: 182/255, blue: 182/255, alpha: 1) }  //deb6b6//(222,182,182)
    }
    enum Misc {
        static var acceptTerms: UIColor  { return UIColor(red: 66/255, green: 143/255, blue: 244/255, alpha: 1) }  //4286F4//(66,134,244)
    }
}



/*
 light blue: 00a0ff
 dark blue :006bb4
 grey:353535
 yellow: efc100
 */
extension UIColor {
    enum ChatColors {
        static var senderBlue1: UIColor  { return UIColor(red: 49/255, green: 96/255, blue: 179/255, alpha: 1) }  //3160B3//(49,96,179)
        static var senderBlue2: UIColor  { return UIColor(red: 10/255, green: 70/255, blue: 134/255, alpha: 1) }  //0A4686//(10,70,134)
        static var senderBlue3: UIColor  { return UIColor(red: 82/255, green: 114/255, blue: 163/255, alpha: 1) }  //5272A3//(82,114,163)
        static var receiverGray: UIColor  { return UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1) }  //efefef//(239,239,239)
    }
}
extension UIColor {
    enum SliderColors {
        static var c1: UIColor  { return UIColor(red: 219/255, green: 21/255, blue: 38/255, alpha: 1) }  //db1526//(219,21,38)
        
        static var c2: UIColor  { return UIColor(red: 222/255, green: 61/255, blue: 26/255, alpha: 1) }  //de3d1a//(222,61,26)
        
        static var c3: UIColor  { return UIColor(red: 239/255, green: 98/255, blue: 11/255, alpha: 1) }  //ef620b//(239,98,11)
        
        static var c4: UIColor  { return UIColor(red: 240/255, green: 130/255, blue: 4/255, alpha: 1) }  //f08204//(240,130,4)
        
        static var c5: UIColor  { return UIColor(red: 253/255, green: 197/255, blue: 38/255, alpha: 1) }  //fdc526//(253,197,38)
        
        static var c6: UIColor  { return UIColor(red: 254/255, green: 225/255, blue: 0/255, alpha: 1) }  //fee100//(254,225,0)
        
        static var c7: UIColor  { return UIColor(red: 186/255, green: 192/255, blue: 13/255, alpha: 1) }  //bac00d//(186,192,13)
        
        static var c8: UIColor  { return UIColor(red: 141/255, green: 192/255, blue: 33/255, alpha: 1) }  //8dc021//(141,192,33)
        
        static var c9: UIColor  { return UIColor(red: 77/255, green: 185/255, blue: 62/255, alpha: 1) }  //4db93e//(77,185,62)
        
        static var c10: UIColor  { return UIColor(red: 12/255, green: 183/255, blue: 93/255, alpha: 1) }  //0cb75d//(12,183,93)
    }
}

extension UIColor {
    enum SearchColors {
        static var blue: UIColor  { return UIColor(red: 58/255, green: 163/255, blue: 211/255, alpha: 1) }  //rgba(58, 163, 211, 1)
        static var tagBkg: UIColor  { return UIColor(red: 203/255, green: 231/255, blue: 244/255, alpha: 1) }  //rgba(203, 231, 244, 1)
        static var shadow: UIColor  { return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.12) }  //rgba(0, 0, 0, 0.12)
    }
    enum ScoreColors {
        static var creditText: UIColor  { return UIColor(red: 92/255, green: 189/255, blue: 33/255, alpha: 1) }  //rgba(92, 189, 33, 1)
        static var creditBkg: UIColor  { return UIColor(red: 203/255, green: 231/255, blue: 244/255, alpha: 0.3) }  //rgba(92, 189, 33, 0.15)
        static var debitText: UIColor  { return UIColor(red: 237/255, green: 91/255, blue: 76/255, alpha: 1) }  //rgba(237, 91, 76, 1)
        static var debitBkg: UIColor  { return UIColor(red: 237/255, green: 91/255, blue: 76/255, alpha: 0.3) }  //rgba(237, 91, 76, 0.15)
    }
}
extension UIColor {
    enum Colors {
        static var blue: UIColor  { return UIColor(red: 39/255, green: 99/255, blue: 252/255, alpha: 1) }  //rgba(39, 99, 252, 1)  #2763FC
        static var tagBkg: UIColor  { return UIColor(red: 203/255, green: 231/255, blue: 244/255, alpha: 1) }  //rgba(203, 231, 244, 1)
        static var shadow: UIColor  { return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.12) }  //rgba(0, 0, 0, 0.12)
    }
}
