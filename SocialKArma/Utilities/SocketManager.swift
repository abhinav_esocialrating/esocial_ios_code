//
//  SocketManager.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 30/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
import SocketIO

protocol SocketIOManagerDelegate {
    func messageSent(chatMessage: Messages)
}

class SocketIOManager: NSObject {
    
    static let shared = SocketIOManager()
    var manager: SocketManager!
    var socket: SocketIOClient!
    var delegate: SocketIOManagerDelegate?
//    let fromUserId = ""
    var toUserId = ""
    // MARK: - Initialize Socket
    private override init() {
        super.init()
    }
    /**
     Function for connecting socket whenever user enters the chat window
     */
    func connectSocket() {
        manager = SocketManager(socketURL: URL(string: AppUrl.chatBaseUrl.rawValue)!, config: ["connectParams": ["userId": UserDefaults.standard.userId, "toUserId": toUserId]])
        socket = manager.defaultSocket
        
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
        }
        socket.on(clientEvent: .error) { (data, ack) in
            print(data)
            //["Tried emitting when not connected"]
        }
        socket.on(clientEvent: .disconnect) { (data, ack) in
            print("socket disconnected")
        }
    }
    // MARK: - Chat Screen
    /**
    Method emitting event for starting chat Socket
     */
    func startChatSocket() {
        socket.on(TZChatEvents.newMsgResp.rawValue) { (data, ack) in
            print(data)
            guard let delegate = self.delegate, let messageDict = data[0] as? NSDictionary else {
                return
            }
            if let message = Messages(dictionary: messageDict) {
                delegate.messageSent(chatMessage: message)
            }
        }
    }
    /**
    Method for emitting event for stopping Socket chat
     */
    func stopChatSocket() {
        socket.off(TZChatEvents.newMsgResp.rawValue)
    }
    /**
    Method for emitting event for sending message
     */
    func emitChatMessage(message: String, messageType: ChatMessageType) {
        print(socket.status)
        socket.emit(TZChatEvents.newMessage.rawValue, with: [["fromUserId": UserDefaults.standard.userId, "toUserId": toUserId, "message": message, "type": messageType.rawValue]]) {
            print("Sent message")
            let messageNew = Messages(dictionary: ["message": message, "fromUserId": Int(UserDefaults.standard.userId) ?? 1, "mdfd": Utilities.chatDateCreation()])
            self.delegate?.messageSent(chatMessage: messageNew!)
        }
    }
    // MARK: - App Start
    /**
    Establish Connection with the remote socket
     */
    func establishConnection() {
        connectSocket()
        socket.connect()
    }
    /**
    Closing Connection with the remote socket
     */
    func closeConnection() {
        socket.disconnect()
    }
    
    // MARK: -  Web Services
    /**
    API calling function for updating status of the user whether he is online or not.
     
     - Parameter status: Status to be updated  0: offline  1: online
     */
    func chatUpdateOnlineStatus(status: Int) {
        let params = ["userId": UserDefaults.standard.userId, "isOnline": status] as [String : Any]
        NetworkManager.chatUpdateOnlineStatus(parameters: params, method: .post) { (response) in
            switch response.response?.statusCode {
            case 200:
                do {
                    let dict = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    print(dict)
                } catch {
                    
                }
            default:
                print("Error chatUpdateOnlineStatus")
            }
        }
    }
    /**
    API calling function for Getting chat history in the offsets of predefined number.
     
     - Parameter params: Parameter dictionary containing message and other info.
     - Parameter completion: Callback to the calling object containg chat history data and error message if any,
     */
    func chatGetHistory(params: [String:Any], completion: @escaping ((_ chatHistory: ChatHistory?, _ data: [Messages], _ error: String) -> Void)) {
        NetworkManager.chatGetHistory(parameters: params, method: .get) { (response) in
            switch response.response?.statusCode {
            case 200:
                do {
                    if let dict = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? NSDictionary {
                        print(dict)
                        let chatHistory = ChatHistory(dictionary: dict)
                        let history = dict["messages"] as! NSArray
                        let messages = Messages.modelsFromDictionaryArray(array: history)
                        completion(chatHistory, messages, "")
                    }
                } catch {
                    print("Catch block")
                }
            default:
                completion(nil, [], "Error block")
                print("Error chatGetHistory")
            }
        }
    }
    /**
    API calling function for Getting chat list of the user whom user has chatted with.
     
     - Parameter params: Parameter dictionary containing message and other info.
     - Parameter completion: Callback to the calling object containg chat list data and error message if any,
     */
    func chatGetRecentChats(params: [String:Any], completion: @escaping ((_ data: [RecentChat], _ error: String) -> Void)) {
        NetworkManager.chatGetRecentChats(parameters: params, method: .get) { (response) in
            switch response.response?.statusCode {
            case 200:
                do {
                    if let dict = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? NSDictionary {
//                        print(dict)
                        let history = dict["messages"] as! NSArray
                        let messages = RecentChat.modelsFromDictionaryArray(array: history)
                        completion(messages, "")
                    }
                } catch {
                    
                }
            default:
                completion([], "Error block")
                print("Error chatGetRecentChats")
            }
        }
    }
    /**
    API calling function for updating the status of the chat request sent by other user.
     
     - Parameter status: status of the action performed by the user i.e., Accept of reject
     - Parameter messageid: message id of the last message sent by the user.
     - Parameter completion: Callback to the calling object containg error message if any, and notifying that the call to remote is complete.
     */
    func chatSetReqAction(status: ChatRequestStatus, messageid: String?, completion: @escaping ((_ error: String) -> Void)) {
        var params = [String:Any]()
        if status == .Blocked || status == .Unblocked {
            params = ["fromUserId": UserDefaults.standard.userId, "toUserId": toUserId, "status": status.rawValue, "messageid": messageid!]
        } else {
            params = ["toUserId": UserDefaults.standard.userId, "fromUserId": toUserId, "status": status.rawValue, "messageid": messageid!,] as [String : Any]  // As chat initiator is other person
        }
        NetworkManager.chatSetReqAction(parameters: params, method: .post) { (response) in
//            var dict = NSDictionary()
            if let data = response.data {
                do {
                    let dict1 = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
                    print(dict1)
//                    dict = dict1
                } catch {
                    
                }
            }
            switch response.response?.statusCode {
            case 200:
                print("success chatSetReqAction")
                completion("")
//                completion(dict["messages"] as! String)
            default:
                print("Error chatSetReqAction")
                completion("Error")
            }
        }
    }
    /**
    API calling function for updating the status of the chat message sent by other user that it has been read.
     
     - Parameter params: Parameter dictionary containing message and other info.
     - Parameter completion: Callback to the calling object containg confirmation data and error message if any, and notifying that the call to remote is complete.
     */
    func updateMessageStatus(params: [String:Any], completion: @escaping ((_ data: String, _ error: String) -> Void)) {
        NetworkManager.updateMessageStatus(parameters: params, method: .post) { (response) in
            switch response.response?.statusCode {
            case 200:
                do {
                    if let dict = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? NSDictionary {
                        print(dict)
                        completion("", "")
                    }
                } catch {
                    print("catch updateMessageStatus")
                }
            default:
                completion("", "Error block")
                print("Error updateMessageStatus")
            }
        }
    }
}
// MARK: - Custom Socket Events
/**
 Enum for differentiating type of event either message sent or received.
 */
enum TZChatEvents: String {
    // Sending message
    case newMessage = "newMessage"
    // Receiving message
    case newMsgResp = "newMsgResp"
}
// MARK: - Actions for chats
/**
Enum for response to the chat request sent by other user
*/
enum ChatRequestStatus: String {
    case Accept = "A"
    case Reject = "R"
    case Ingnored = "I"
    case Blocked = "B"
    case Unblocked = "U"
    case None = "None"
}
/**
Enum for type of message being sent.
*/
enum ChatMessageType: String {
    // If user has accepted the chat request
    case chat = "chat"
    // If chat request is pending
    case request = "request"
}
