//
//  CoreDataHelper.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 18/04/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
import CoreData

class CoreDataHelper {
    
    static let shared = CoreDataHelper()
    var count = 0

    func sync(fetchedContacts contacts: [[String:Any]], completion: (_ success: Bool) -> Void) {
        
        guard let taskContext = appDelegate?.persistentContainer.newBackgroundContext() else {
            return
        }
        taskContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        taskContext.undoManager = nil
        
        taskContext.performAndWait {
            
            let userEntity = NSEntityDescription.entity(forEntityName: "Contacts", in: taskContext)!
            
            for i in contacts {
                let user = NSManagedObject(entity: userEntity, insertInto: taskContext)
                user.setValue(i["name"], forKeyPath: "name")
                user.setValue(i["mobile_number"], forKey: "mobile_number")
                user.setValue(i["country_code"], forKey: "country_code")
            }
            
            if taskContext.hasChanges {
                do {
                    try taskContext.save()
                    completion(true)
                } catch {
                    print("Error: \(error)\nCould not save Core Data context.")
                    completion(false)
                }
                taskContext.reset() // Reset the context to clean up the cache and low the memory footprint.
                
            }
        }
    }
    
    func createData(contacts: [[String:Any]]) {
        
        guard let appDelegate = appDelegate else { return }
                
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Now let’s create an entity and new records.
        let userEntity = NSEntityDescription.entity(forEntityName: "Contacts", in: managedContext)!
        
        for i in contacts {
            let user = NSManagedObject(entity: userEntity, insertInto: managedContext)
            user.setValue(i["name"], forKeyPath: "name")
            user.setValue(i["mobile_number"], forKey: "mobile_number")
            user.setValue(i["country_code"], forKey: "country_code")
        }
        
        do {
            try managedContext.save()
           
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    func createDataInChunks(contacts: [[String:Any]]) {
        guard let appDelegate = appDelegate else { return }
        
        let chunked = contacts.chunked(into: 500)
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        for contactChunk in chunked {
            //Now let’s create an entity and new records.
            let userEntity = NSEntityDescription.entity(forEntityName: "Contacts", in: managedContext)!
            
            for i in contactChunk {
                let user = NSManagedObject(entity: userEntity, insertInto: managedContext)
                user.setValue(i["name"], forKeyPath: "name")
                user.setValue(i["mobile_number"], forKey: "mobile_number")
                user.setValue(i["country_code"], forKey: "country_code")
            }
            
            do {
                try managedContext.save()
               
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
        
    }
    func shouldUploadContacts() -> Bool {
        
        guard let appDelegate = appDelegate else { return false }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contacts")
        var ct = 0
        do
        {
            ct = try managedContext.count(for: fetchRequest)
            print("total count = \(ct)")
        } catch {
            print(error)
        }
        return (ct > 0)
    }
    func retrieveData() -> [[String:Any]] {
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = appDelegate else { return [] }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Prepare the request of type NSFetchRequest  for the entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contacts")
        
        do
        {
            let ct = try managedContext.count(for: fetchRequest)
            print("total count = \(ct)")
        } catch {
            print(error)
        }
        fetchRequest.fetchLimit = 50
        var contactArr = [[String:Any]]()
        do {
            let result = try managedContext.fetch(fetchRequest)
            count = result.count
            for data in result as! [NSManagedObject] {
                var dict = [String:Any]()
                dict["name"] = data.value(forKey: "name") as! String
                dict["mobile_number"] = data.value(forKey: "mobile_number") as! String
                dict["country_code"] = data.value(forKey: "country_code") as! String
                contactArr.append(dict)
            }
        } catch {
            print("Failed")
        }
        return contactArr
    }
    
     func deleteData() {
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contacts")
        fetchRequest.fetchLimit = count
       
        do
        {
            let test = try managedContext.fetch(fetchRequest)
            
            for i in test as! [NSManagedObject] {
                managedContext.delete(i)
            }
            do{
                try managedContext.save()
            }
            catch
            {
                print(error)
            }
            
        }
        catch
        {
            print(error)
        }
    }
    func getCount() -> Int {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return 0 }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "User")
        do
        {
            _ = try managedContext.count(for: fetchRequest)
        } catch {
            print(error)
        }
        return 0
        
    }
     func updateData(){
     
         //As we know that container is set up in the AppDelegates so we need to refer that container.
         guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
                
         //We need to create a context from this container
         let managedContext = appDelegate.persistentContainer.viewContext
         
         let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "User")
         fetchRequest.predicate = NSPredicate(format: "username = %@", "Ankur1")
         do
         {
             let test = try managedContext.fetch(fetchRequest)
    
                 let objectUpdate = test[0] as! NSManagedObject
                 objectUpdate.setValue("newName", forKey: "username")
                 objectUpdate.setValue("newmail", forKey: "email")
                 objectUpdate.setValue("newpassword", forKey: "password")
                 do{
                     try managedContext.save()
                 }
                 catch
                 {
                     print(error)
                 }
             }
         catch
         {
             print(error)
         }
    
     }
}
