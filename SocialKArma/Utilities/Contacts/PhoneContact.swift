//
//  PhoneContact.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 14/08/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import ContactsUI
import libPhoneNumber_iOS

class PhoneContact: NSObject {
    
    var firstName: String?
    var name: String?
    var phoneNumber: [String] = [String]()
    var countryCode: [String] = [String]()
    let phoneUtil = NBPhoneNumberUtil()

    init(contact: CNContact) {
        super.init()
        firstName   = contact.givenName
        name        = contact.givenName + " " + contact.familyName
        if let first = firstName {
            if first == "SPAM" || first == "spam" || first == "Spam" {
                return
            }
        }
        if let name = name {
            if name == "SPAM" || name == "spam" || name == "Spam" {
                return
            }
        }
        for phone in contact.phoneNumbers {
            var phoneStr = phone.value.stringValue
            phoneStr = phoneStr.stripped
            checkForCountryCode(number: phoneStr)
        }
    }
    func isFixedLine(number: String) -> Bool {
        do {
            let phoneNumber: NBPhoneNumber = try phoneUtil.parse(number, defaultRegion: "IN")
            let type = phoneUtil.getNumberType(phoneNumber)
            return type == .FIXED_LINE
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
        return false
    }
    func checkForCountryCode(number: String) {
        do {
            let currentPhoneNumber = try phoneUtil.parseAndKeepRawInput(number, defaultRegion: "IN")
            let type = phoneUtil.getNumberType(currentPhoneNumber)
            switch type {
            case .MOBILE:
                phoneNumber.append(currentPhoneNumber.nationalNumber.stringValue)
                countryCode.append(currentPhoneNumber.countryCode.stringValue)
            case .FIXED_LINE_OR_MOBILE:
                phoneNumber.append(currentPhoneNumber.nationalNumber.stringValue)
                countryCode.append(currentPhoneNumber.countryCode.stringValue)
            default:
                AppDebug.shared.consolePrint("other")
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
    }
    override init() {
        super.init()
    }
}
