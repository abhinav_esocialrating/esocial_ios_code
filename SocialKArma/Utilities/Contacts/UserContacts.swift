//
//  UserContacts.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 07/12/19.
//  Copyright © 2019 Abhinav Dobhal. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserContacts {
    // MARK: - Singleton instance
    struct Static {
        fileprivate static var instance: UserContacts?
    }
    class var sharedInstance: UserContacts {
        if Static.instance == nil {
            Static.instance = UserContacts()
        }
        return Static.instance!
    }
    func dispose() {
        UserContacts.Static.instance = nil
        print("Disposed Singleton instance")
    }
    // MARK: - Variables
    var filter: ContactsFilter = .none
    var phoneContacts = [PhoneContact]()
    var contacts = [[String:Any]]()
    var current = 0   // current point
    var allContactsCount = 0   // count of all contacts
    var chunkSize = 50
    var batchArray = [[[String:Any]]]()
    var isContactSyncInProgress = false
    var isContactSyncDone = false

    // MARK: - Web Services
    func uploadContactList(contactList: [[String:Any]], completion: ((_ success: Bool, _ json: JSON, _ message: String) -> Void)?) {
        let params = ["contact": contactList]
        NetworkManager.uploadContacts(parameters: params, method: .post) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
//            print(json)
            switch response.response?.statusCode {
            case 200:
                AppDebug.shared.consolePrint("Contacts uploaded successfully")
                completion?(true, json, "")
            default:
                let message = json["message"].stringValue
                AppDebug.shared.consolePrint(message)
                completion?(false, JSON.init(), message)
            }
        }
    }
    // MARK: - Methods
    func loadContacts() {
        UserDefaults.standard.latestContactSync = Date.timeIntervalSinceReferenceDate

        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }

        if UserDetails.shared.isComingFromSignIn == 2 {
            UserContacts.sharedInstance.phoneContacts = []
            contacts = PhoneContacts.getContactsAll(filter: .none)
            // Sorting
            UserContacts.sharedInstance.contacts.sort { (p1, p2) -> Bool in
                return ((p1["name"] as? String) ?? "") < ((p2["name"] as? String) ?? "")
            }
            CoreDataHelper.shared.sync(fetchedContacts: self.contacts) { (success) in
                self.uploadToServer()
            }
        } else if UserDetails.shared.isComingFromSignIn == 1 {
            UserContacts.sharedInstance.phoneContacts = []
            contacts = PhoneContacts.getContactsAll(filter: .none)
            // Sorting
            UserContacts.sharedInstance.contacts.sort { (p1, p2) -> Bool in
                return ((p1["name"] as? String) ?? "") < ((p2["name"] as? String) ?? "")
            }
            CoreDataHelper.shared.sync(fetchedContacts: self.contacts) { (success) in
                self.uploadToServer()
            }
        } else if CoreDataHelper.shared.shouldUploadContacts() {
            UserContacts.sharedInstance.uploadToServer()
        } else {
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
    func loadContactsForced() {
        
        UserDefaults.standard.latestContactSync = Date.timeIntervalSinceReferenceDate
        
        contacts = PhoneContacts.getContactsAll(filter: .none)

        // Sorting
        UserContacts.sharedInstance.contacts.sort { (p1, p2) -> Bool in
            return ((p1["name"] as? String) ?? "") < ((p2["name"] as? String) ?? "")
        }
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        CoreDataHelper.shared.sync(fetchedContacts: self.contacts) { (success) in
            self.uploadToServer()
        }
    }
    func loadContactsInBkg() {
        
        PhoneContacts.requestAccess { (granted) in
            UserDefaults.standard.latestContactSync = Date.timeIntervalSinceReferenceDate
            
            self.contacts = PhoneContacts.getContactsAll(filter: .none)
            
            // Sorting
            UserContacts.sharedInstance.contacts.sort { (p1, p2) -> Bool in
                return ((p1["name"] as? String) ?? "") < ((p2["name"] as? String) ?? "")
            }
            CoreDataHelper.shared.sync(fetchedContacts: self.contacts) { (success) in
                self.uploadToServer()
            }
        }
    }
    func uploadToServer() {
        let data = CoreDataHelper.shared.retrieveData()
        if data.count > 0 {
            self.uploadContactList(contactList: data) { (success, _, _) in
                if success {
                    CoreDataHelper.shared.deleteData()
                }
                self.uploadToServer()
            }
        } else {
//            isContactSyncInProgress = false
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
}
extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}

/*  Contact sync old logic chunk

        for contact in PhoneContacts.getContacts(filter: filter) {
            UserContacts.sharedInstance.phoneContacts.append(PhoneContact(contact: contact))
        }
        for contact in UserContacts.sharedInstance.phoneContacts {
            for (j,i) in contact.phoneNumber.enumerated() {
                var dict = [String:Any]()
                dict["name"] = contact.name ?? ""
                dict["mobile_number"] = i.extractDecimalDigits()
                dict["country_code"] = contact.countryCode[j] == "" ? "" : "+\(contact.countryCode[j])"
                contacts.append(dict)
            }
        }

 
         if contactArray.count >= 50 {
             print("chunks.count == 50")
             CoreDataHelper.shared.createData(contacts: contactArray)
             contactArray = []
         }
*/
