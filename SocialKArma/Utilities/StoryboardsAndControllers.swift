//
//  StoryboardsAndControllers.swift
//  Tooprak
//
//  Created by Abhinav Dobhal on 27/05/19.
//

import Foundation
import UIKit

enum AppStoryboard : String {
    
    case Main
    case MainExt
    case Home
    case Profile
    case Search
    case Network
    case Chat
    case ProfileV2
    case ScoreCard
    case Offers
    case Onboarding
    
    var instance : UIStoryboard {
        
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type, function : String = #function, line : Int = #line, file : String = #file) -> T {
        
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
        }
        
        return scene
    }
    
    func initialViewController() -> UIViewController? {
        
        return instance.instantiateInitialViewController()
    }
}

extension UIViewController {
    
    // Not using static as it wont be possible to override to provide custom storyboardID then
    class var storyboardID : String {
        
        return "\(self)"
    }
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
}

// MARK: - TableViewCells
enum TableViewCells: String {
    typealias RawValue = String
    
    // Main
    case TestCell = "TestCell"
    case RatingTableViewCell = "RatingTableViewCell"
    case SliderCell = "SliderCell"
    case MultiColorSliderCell = "MultiColorSliderCell"
    case CountryCell = "CountryCell"
    case CountryListCell = "CountryListCell"
    case OnboardingLookingForCell = "OnboardingLookingForCell"

    // Profile
    case InterestsTableViewCell = "InterestsTableViewCell"
    case ExperienceHeaderCell = "ExperienceHeaderCell"
    case TagTableViewCell = "TagTableViewCell"
    case AboutMeTableViewCell = "AboutMeTableViewCell"
    case TextTableViewCell = "TextTableViewCell"
    case SkillTableViewCell = "SkillTableViewCell"
    case SkillV2TableViewCell = "SkillV2TableViewCell"
    case SkillHeaderTableViewCell = "SkillHeaderTableViewCell"
    case SideMenuCell = "SideMenuCell"
    case ContactListCell = "ContactListCell"
    case EducationListCell = "EducationListCell"
    case NotificationCell = "NotificationCell"
    case BusinessServicesCell = "BusinessServicesCell"
    case BusinessListCell = "BusinessListCell"
    case TagRelationsCell = "TagRelationsCell"
    case LookingForCell = "LookingForCell"
    case SkillTypesCell = "SkillTypesCell"
    case ProjectCell = "ProjectCell"
    case ProfileDetailBasicInfoCell = "ProfileDetailBasicInfoCell"
    case CompetencyTagCell = "CompetencyTagCell"
    case ProfileDetailHeaderCell = "ProfileDetailHeaderCell"

    // Chat
    case ChannelListCell = "ChannelListCell"
    case ChatSenderCell = "ChatSenderCell"
    case ChatReceiverCell = "ChatReceiverCell"
    
    // Score Screen
    case ScoreTableViewCell = "ScoreTableViewCell"
    case ScoreInfoCell = "ScoreInfoCell"
    
    // Points History Screen
    case PointsSummaryCell = "PointsSummaryCell"
    case PointsDetailsCell = "PointsDetailsCell"
    
    // FeedBack screen
    case FeedBackQuestionCell = "FeedBackQuestionCell"
    case FeedbackRatingCell = "FeedbackRatingCell"
    case FeedbackFooterCell = "FeedbackFooterCell"
    case TextFeedbackSuggestionCell = "TextFeedbackSuggestionCell"
    
    
    // ScoreCard
    case NetworkCell = "NetworkCell"
    case ScoreMultiColorSliderCell = "ScoreMultiColorSliderCell"
    case PointsHeaderCell = "PointsHeaderCell"
    case TextualFeedbackCell = "TextualFeedbackCell"
    
    // Offers
    case OffersCell = "OffersCell"
    case OfferCommentsCell = "OfferCommentsCell"
    case LikesCell = "LikesCell"
    case OfferReplyCell = "OfferReplyCell"
}

// MARK: - CollectionViewCells
enum CollectionViewCells: String {
    typealias RawValue = String
    
    case OnBoardingCollectionViewCell = "OnBoardingCollectionViewCell"
    case ContactRatingsCell = "ContactRatingsCell"
    case NetworkCollectionViewCell = "NetworkCollectionViewCell"
    case GroupsCollectionViewCell = "GroupsCollectionViewCell"
    case SearchCategoriesTagCell = "SearchCategoriesTagCell"
    case UserDetailCell = "UserDetailCell"
    case SearchSuggestionsCell = "SearchSuggestionsCell"
    case ReviewContactCell = "ReviewContactCell"
    case RelationshipCell = "RelationshipCell"
    case AuxCardCell = "AuxCardCell"
    case WebCardCell = "WebCardCell"
    case PointsEarnedCell = "PointsEarnedCell"
    case FilterTagCell = "FilterTagCell"
    case ProfileCategoriesCell = "ProfileCategoriesCell"
    // Aux Cards
    case BackgroundImageCell = "BackgroundImageCell"
    case LinkCardCell1 = "LinkCardCell1"
    case LinkCardCell2 = "LinkCardCell2"
    case LinkCardCell3 = "LinkCardCell3"
    case LinkCardCell4 = "LinkCardCell4"
    case LinkCardCell5 = "LinkCardCell5"
    case InfoCardCell = "InfoCardCell"
    
    // Multiple review cards
    case BaseReviewCell = "BaseReviewCell"

    // ScoreCard
    case MedalsCell = "MedalsCell"
    case ScoreCardCell = "ScoreCardCell"
}

// MARK: - Segues
enum Segues: String {
    case CategorySegue = "CategorySegue"
}
