//
//  LocationManager.swift
//  SocialKArma
//
//  Created by Abhinav Dobhal on 08/01/20.
//  Copyright © 2020 Abhinav Dobhal. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON

protocol LocationManagerDelegate {
    func tracingLocation(currentLocation: CLLocation)
    func tracingLocationDidFailWithError(error: NSError)
}
var changedLocationPermission: ((_ allowed: Bool) -> Void)? = nil
var firstTimeSearch: (() -> Void)? = nil
class LocationManager: NSObject, CLLocationManagerDelegate {

    var locationManager: CLLocationManager?
    var lastLocation: CLLocation?
    var delegate: LocationManagerDelegate?
    var isFirst = true
    var isFirstTimeSearch = true
    
    static let shared:LocationManager = {
        let instance = LocationManager()
        return instance
    }()

     override init() {
        super.init()
        self.locationManager = CLLocationManager()
        
        guard let locationManagers=self.locationManager else {
            return
        }
        locationManagers.delegate = self
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined, .restricted, .denied:
            print("No access")
            if locationManagers.responds(to: #selector(locationManagers.requestWhenInUseAuthorization)) {
                locationManagers.requestWhenInUseAuthorization()
            }
        case .authorizedAlways:
            print("authorizedAlways")
            //changedLocationPermission?()
            locationManagers.startUpdatingLocation()
        case .authorizedWhenInUse:
            print("authorizedWhenInUse")
            //changedLocationPermission?()
            locationManagers.startUpdatingLocation()
        @unknown default:
            print("")
        }
        locationManagers.desiredAccuracy = kCLLocationAccuracyBest
//        locationManagers.pausesLocationUpdatesAutomatically = false
        locationManagers.distanceFilter = 0.1
//        locationManagers.allowsBackgroundLocationUpdates = true
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined, .denied:
//            if !isFirst {
            changedLocationPermission?(false)
//            }
//            isFirst = false
            break
        case .authorizedWhenInUse:
            locationManager?.startUpdatingLocation()
            changedLocationPermission?(true)
            break
        case .authorizedAlways:
            locationManager?.startUpdatingLocation()
            changedLocationPermission?(true)
            break
        case .restricted:
            // restricted by e.g. parental controls. User can't enable Location Services
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        updateLocationDidFailWithError(error: error as NSError)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        self.lastLocation = location
        if isFirstTimeSearch {
            isFirstTimeSearch = false
            firstTimeSearch?()
        }
        
        updateLocation(currentLocation: location)
//        stopUpdatingLocation()
        if Utilities().shouldUpdateLocationInServer() {
            getAdressName(coords: location) { (city) in
                let params = ["city": city, "latitude": location.coordinate.latitude, "longitude": location.coordinate.longitude] as [String : Any]
                self.sendLocation(params: params)
            }
        }
//        print(Date().timeIntervalSince1970)
    }
    
    // MARK:- Private function
      private func updateLocation(currentLocation: CLLocation){
          
          guard let delegate = self.delegate else {
              return
          }
          
          delegate.tracingLocation(currentLocation: currentLocation)
      }
      
      private func updateLocationDidFailWithError(error: NSError) {
          
          guard let delegate = self.delegate else {
              return
          }
          
          delegate.tracingLocationDidFailWithError(error: error)
      }

    

    func startUpdatingLocation() {
        print("Starting Location Updates")
        self.locationManager?.startUpdatingLocation()
//        locationManager?.allowsBackgroundLocationUpdates = true
//        self.locationManager?.startMonitoringSignificantLocationChanges()
    }
    
    func stopUpdatingLocation() {
        print("Stop Location Updates")
        self.locationManager?.stopUpdatingLocation()
    }
    
    func startMonitoringSignificantLocationChanges() {
        self.locationManager?.startMonitoringSignificantLocationChanges()
    }
    // MARK: - Geocoding
    func getAdressName(coords: CLLocation, completion: @escaping ((_ city: String) -> Void)) {
        print("reverseGeocodeLocation")
        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
            var adressString : String = ""
            var city = ""
            if error != nil {
                print("Hay un error")
            } else {
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    if place.thoroughfare != nil {
                        adressString = adressString + place.thoroughfare! + ", "
                    }
                    if place.subThoroughfare != nil {
                        adressString = adressString + place.subThoroughfare! + "\n"
                    }
                    if place.locality != nil {
                        adressString = adressString + place.locality! + " - "
                        city = place.locality!
                    }
                    if place.postalCode != nil {
                        adressString = adressString + place.postalCode! + "\n"
                    }
                    if place.subAdministrativeArea != nil {
                        adressString = adressString + place.subAdministrativeArea! + " - "
                    }
                    if place.country != nil {
                      adressString = adressString + place.country!
                    }
                    print(adressString)
                }
            }
            completion(city)
        }
    }
}
// MARK: - Recurring Location service
extension LocationManager {
    func setUpRecurringLocationService() {
        let date = Date().addingTimeInterval(5)
        let timer = Timer(fireAt: date, interval: 60*30, target: self, selector: #selector(callLocationUpdate), userInfo: nil, repeats: true) // Calling API after every 30 min
        RunLoop.main.add(timer, forMode: .common)
    }
    @objc func callLocationUpdate() {
        guard let locationManager = self.locationManager else {
            return
        }
        locationManager.startUpdatingLocation()
//        locationManager.allowsBackgroundLocationUpdates = true
    }
}
// MARK: - Web Services
extension LocationManager {
    func getCities(query: String, completion: @escaping ((_ response: [String], _ cityNames: [String]) -> Void)) {
        let params = ["key": Constants.FirebaseAPIKey.rawValue, "types": "(cities)", "sessiontoken": String.randomString(length: 8), "input": query]
        print(params)
        NetworkManager.searchLocation(parameters: params, method: .get) { (response) in
            var cities = [String]()
            var citiesNames = [String]()
            switch response.response?.statusCode {
            case 200:
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    print(jsonData)
                    let predictions = jsonData["predictions"] as! NSArray
                    predictions.forEach { (predictionAny) in
                        let prediction = predictionAny as! NSDictionary
                        let description = prediction["description"] as! String
                        cities.append(description)
                        let terms = prediction["terms"] as! NSArray
                        let termsParts = terms[0] as? [String:Any]
                        if let value = termsParts?["value"] as? String {
                            citiesNames.append(value)
                        }
                    }
                } catch {
                    print(Errors.LocationAPIError.rawValue)
                }
            default:
                print("")
            }
            completion(cities, citiesNames)
        }
    }
    func sendLocation(params: [String: Any]) {
        NetworkManager.sendLocation(parameters: params, method: .put) { (response) in
            let json = JSON(response.result.value ?? JSON.init())
            print(json)
            switch response.response?.statusCode {
            case 200:
                print("Location update successful!")
            default:
                print("Location update failed!")
            }
        }
    }
}
/*
{
    description = "Ericeira, Portugal";
    id = 714ff024001d0db57f3a11b9f1e1889b6f7e580e;
    "matched_substrings" =             (
                        {
            length = 3;
            offset = 0;
        }
    );
    "place_id" = "ChIJl2_9rQ4nHw0RTKoQDGEMLHQ";
    reference = "ChIJl2_9rQ4nHw0RTKoQDGEMLHQ";
    "structured_formatting" =             {
        "main_text" = Ericeira;
        "main_text_matched_substrings" =                 (
                                {
                length = 3;
                offset = 0;
            }
        );
        "secondary_text" = Portugal;
    };
    terms =             (
                        {
            offset = 0;
            value = Ericeira;
        },
                        {
            offset = 10;
            value = Portugal;
        }
    );
    types =             (
        locality,
        political,
        geocode
    );
}

 
 */
